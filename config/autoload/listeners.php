<?php

declare(strict_types=1);

// 定义监听器
// 实际使用时，配置和注解只需使用其一即可，如果既有注解又有配置，则会造成监听器被多次触发。
// 文档：https://hyperf.wiki/2.0/#/zh-cn/event?id=%e4%bd%bf%e7%94%a8%e4%ba%8b%e4%bb%b6%e7%ae%a1%e7%90%86%e5%99%a8
return [
];
