<?php

declare(strict_types=1);

return [
    'default' => [
        'host' => env('REDIS_HOST', 'localhost'),
        'auth' => env('REDIS_AUTH', null),
        'port' => (int) env('REDIS_PORT', 6379),
        'db' => (int) env('REDIS_DB', 0),
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('REDIS_MAX_IDLE_TIME', 60),
        ],
    ],
    'pod' => [  //pod起来的服务，非稳定
        'host' => env('REDIS_POD_HOST', 'localhost'),
        'auth' => env('REDIS_POD_AUTH', ''),
        'port' => (int) env('REDIS_POD_PORT', 6379),
        'db' => (int) env('REDIS_POD_DB', 0),
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('REDIS_MAX_IDLE_TIME', 60),
        ],
    ],
    'share' => [
        'host' => env('SHARE_REDIS_HOST', 'localhost'),
        'auth' => env('SHARE_REDIS_AUTH', ''),
        'port' => (int) env('SHARE_REDIS_PORT', 6379),
        'db' => (int) env('SHARE_REDIS_DB', 0),
        'pool' => [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float) env('REDIS_MAX_IDLE_TIME', 60),
        ],
    ]
];
