<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
return [
    'point' => [
        'address' => env('POINTS_API_ADDRESS', 'http://point:8080'),
        'host' => env('POINTS_SERVICE_HOST', '127.0.0.1:8080'),
    ],
    'core' => [
        'host' => env('SERVICE_CORE_HOST'),
    ],
    'message' => [
        'host' => env('SERVICE_MESSAGE_HOST', 'localhost:8080')
    ],
    'doctor' => [
        'host' => env('SERVICE_DOCTOR_HOST', 'doctor-newapi:9501')
    ],
    'token' => [
        'host' => env('SERVICE_TOKEN_HOST', 'token:8080')
    ],
    'pharmacy' => [
        'host' => env('SERVICE_PHARMACY_HOST', 'pharmacy:9502')
    ]
];
