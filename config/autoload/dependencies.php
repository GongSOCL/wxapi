<?php

declare(strict_types=1);

return [
    \Hyperf\Contract\StdoutLoggerInterface::class => \App\Logger\StdoutLoggerFactory::class,
    \Hyperf\Guzzle\ClientFactory::class => \Hyperf\Guzzle\ClientFactory::class,
];
