<?php

use App\Constants\DataStatus;

return [
        DataStatus::WECHAT_AGENT => [
            'config' => [   //参考https://www.easywechat.com/docs/5.x/official-account/configuration
                'app_id' => env('AGENT_WECHAT_APP_ID'),
                'secret' => env('AGENT_WECHAT_SECRET'),
                'token' => env('AGENT_WECHAT_TOKEN')
            ],
            'templates' => [
                DataStatus::WX_AGENT_SERVE_APPLY => env('WECHAT_SERVE_APPLY', ''),
                DataStatus::WX_HELP_AUDIT => env('WECHAT_HELP_AUDIT', ''),
                DataStatus::WX_AGENT_VERIFY => env('WECHAT_AGENT_VERIFY_RESULT', ''),
                DataStatus::SUPPLIER_GROUP => env('WECHAT_SUPPLIER_GROUP', ''),
                DataStatus::SUPPLIER_APPLY => env('WECHAT_SUPPLIER_APPLY', ''),
                DataStatus::SUPPLIER_UNBIND => env('WECHAT_SUPPLIER_UNBIND', '')
            ],
            'alias' => 'wxapi'
        ],
        DataStatus::WECHAT_DOCTOR => [
            'config' => [
                'app_id' => env('DOCTOR_WECHAT_APP_ID'),
                'secret' => env('DOCTOR_WECHAT_SECRET'),
                'token' => env('DOCTOR_WECHAT_TOKEN')
            ],
            'templates' => [ //允许的模板
                DataStatus::WX_QUESTIONNAIRE_KEY => env('DOCTOR_WECHAT_QUESTIONNAIRE_TEMPLATE_ID', ''),
            ],
        ],
        DataStatus::WECHAT_AEROGEN => [
            'config' => [
                'app_id' => env('AEROGEN_WECHAT_APP_ID'),
                'secret' => env('AEROGEN_WECHAT_SECRET'),
                'token' => env('AEROGEN_WECHAT_TOKEN')
            ],
            'templates' => [],
        ]
];
