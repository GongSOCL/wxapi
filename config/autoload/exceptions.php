<?php

declare(strict_types=1);

use App\Exception\Handler\GrpcHandler;
use Youyao\Framework\Exception\Handler\GrpcResponseExceptionHandler;

return [
    'handler' => [
        'http' => [
            GrpcResponseExceptionHandler::class,
            \App\Exception\WechatException::class,
            \App\Exception\Handler\BusinessExceptionHandler::class,
            Hyperf\HttpServer\Exception\Handler\HttpExceptionHandler::class,
            \App\Exception\Handler\ValidationExceptionHandler::class,
            App\Exception\Handler\AppExceptionHandler::class,
        ],
        'grpc' => [
            GrpcHandler::class
        ]
    ],
];
