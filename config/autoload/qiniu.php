<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
return [
    'ak' => env('QINIU_ACCESS_KEY'),
    'sk' => env('QINIU_SECRET_KEY'),
    'bucket' => [
        'user' => [
            'name' => 'user',
            'domain' => env('QINIU_BUCKET_USER_DOMAIN'),
        ],
        'static' => [
            'name' => 'static',
            'domain' => env('QINIU_BUCKET_STATIC_DOMAIN'),
        ],
    ],
];
