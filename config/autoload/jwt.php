<?php
declare(strict_types=1);

return [
    'app' => [
        'token_expires' => 7 * 24 * 3600,
        'key' => 'Y\Y"W^o%}z]V$C!_E\rr2MY@&-c#WU>f',
        'alg' => env('JWT_ALG', 'HS256')
    ]
];
