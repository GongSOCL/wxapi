<?php

declare(strict_types=1);

return [
    'scan' => [
        'paths' => [
            BASE_PATH . '/app',
        ],
        'ignore_annotations' => [
            'mixin',
            'OA\Post',
            'OA\RequestBody',
            'OA\MediaType',
            'OA\Schema',
            'OA\Property',
            'OA\Response',
            'OA\Items',
            'param',
            'return',
            'throws',
            'OA\Put',
            'OA\Parameter',
            'OA\Get',
            'OA\Delete'
        ],
    ],
];
