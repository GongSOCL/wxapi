<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddFieldOwneryyid extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //添加owner_yyid字段
            $table->char('owner_yyid',32)->comment('建群者的yyid')->after('yyid');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //
        });
    }
}
