<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddFieldUpdatedat extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //添加updated_at字段
            $table->dateTime('updated_at')->after('created_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //
        });
    }
}
