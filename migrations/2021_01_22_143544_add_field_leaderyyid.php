<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddFieldLeaderyyid extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //添加leader_yyid字段
            $table->char('leader_yyid',32)->comment('当前群主的yyid')->after('owner_yyid');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //
        });
    }
}
