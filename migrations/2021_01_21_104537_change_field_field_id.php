<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class ChangeFieldFieldId extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('t_doctor_openid_userinfo', function (Blueprint $table) {
            // 将field_id字段的改为varchar类型
            $table->string('field_id', 100)->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('t_doctor_openid_userinfo', function (Blueprint $table) {
            //
        });
    }
}
