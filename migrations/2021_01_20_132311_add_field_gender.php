<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddFieldGender extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('t_doctor', function (Blueprint $table) {
            //添加gender字段
            $table->tinyInteger('gender')->default(0)->nullable(true)
                ->comment('1男2女0未知')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('t_doctor', function (Blueprint $table) {
            //
        });
    }
}
