<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddFieldWechattype extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //添加wechat_type字段
            $table->tinyInteger('wechat_type')->default(2)
                ->comment('公众号类型1代表2医生')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //
        });
    }
}
