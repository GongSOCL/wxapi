<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class AddFieldUseropenid extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //添加invitee_openid字段
            $table->string('invitee_openid',255)->default('')
                ->comment('被邀请者微信openid')->after('invitee_yyid');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('t_reps_doctor_inviter', function (Blueprint $table) {
            //
        });
    }
}
