#!/bin/bash

tables=(
't_questionnaire_paper'
't_questionnaire_paper_invitee'
't_questionnaire_test_papers'
't_questionnaire_wechat_invitee'
't_compliance_question'
't_compliance_question_answer'
't_compliance_question_items'
't_compliance_test_paper'
't_compliance_test_paper_items'
'wechat_user'
't_group_member'
)

for tb in ${tables[@]}
do
	echo $tb
	php bin/hyperf.php gen:model  $tb   --path="app/Model/Qa" --inheritance "Model" --uses "App\\Model\\Model" --with-comments -v
done
