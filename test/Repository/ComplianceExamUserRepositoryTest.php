<?php

namespace HyperfTest\Repository;

use App\Repository\ComplianceExamUserRepository;
use App\Repository\UsersRepository;
use PHPUnit\Framework\TestCase;

class ComplianceExamUserRepositoryTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
    }

    public function testGetUserExams()
    {
        $user = UsersRepository::getUserByUid(145);
        $products = 'ABC123';

        $resp = ComplianceExamUserRepository::getUserExams($user, [$products]);
        $this->assertEmpty($resp);
    }
}
