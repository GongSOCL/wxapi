<?php

namespace HyperfTest\Repository;

use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use Hyperf\DbConnection\Db;
use PHPUnit\Framework\TestCase;

class UsersRepositoryTest extends TestCase
{
    protected function setUp()
    {
        Db::beginTransaction();
    }

    protected function tearDown()
    {
        Db::rollBack();
    }

    public function testAddUserAccountFromMobileAndWechat()
    {
        //测试空手机号添加用户
        $wechat = WechatUserRepository::getById(5461);
        $this->assertNotNull($wechat);
        $user = UsersRepository::addUserAccountFromMobileAndWechat($wechat);
        $this->assertNotNull($user);
        echo $user->uid . PHP_EOL;
    }
}
