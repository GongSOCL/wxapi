<?php
declare(strict_types=1);
namespace Repository\NewGroup;

use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Repository\NewGroup\NewDoctorUserinfoRepository;
use PHPUnit\Framework\TestCase;

class NewDoctorUserinfoRepositoryTest extends TestCase
{

    public function testGetRepeatReps1()
    {
        $resp = NewDoctorUserinfoRepository::getRepeatReps1(7169);
        $this->assertNotNull($resp);
        $groupMax = [];
        $groupIds = [];
        $groupIdList = [];
        /** @var NewDoctorOpenidUserinfo $item */
        foreach ($resp as $item) {
            $key = sprintf("%d.%d", $item->user_id, $item->new_doctor_id);
            $groupIdList[$key][] = $item->id;
            if (!isset($groupMax[$key])) {
                $groupMax[$key] = $item->id;
            } else {
                if ($groupMax[$key] < $item->id) {
                    //小于最大id的都要移除掉
                    $groupIds[] = $groupMax[$key];
                    $groupMax[$key] = $item->id;
                } else {
                    $groupIds[] = $item->id;
                }
            }
        }
        print_r($resp->pluck('id')->all());
        print_r($groupIds);
        print_r($groupMax);
        print_r($groupIdList);
        foreach ($groupIdList as $key => $ids) {
            $maxId = $groupMax[$key];
            $filterIds = array_filter($ids, function($val) use($maxId) {
               return $val != $maxId;
            });
            echo "from" . PHP_EOL;
            print_r($filterIds);
            echo $maxId . PHP_EOL;
            echo PHP_EOL;
        }
    }
}
