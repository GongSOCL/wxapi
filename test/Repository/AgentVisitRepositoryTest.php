<?php

namespace Repository;

use App\Repository\AgentVisitRepository;
use App\Repository\UsersRepository;
use PHPUnit\Framework\TestCase;

class AgentVisitRepositoryTest extends TestCase
{

    public function testGetUserVisitData()
    {
        $user = UsersRepository::getUserByUid(145);
        $collect = AgentVisitRepository::getUserVisitData(
            $user,
            '2020-01-01 00:00:00',
            date('Y-m-d H:i:s'),
            '',
            '',
            0,
            1,
            0,
            [1, 2],
            1,
            10
        );
        $this->assertTrue($collect->isEmpty());
        print_r($collect->items());
    }
}
