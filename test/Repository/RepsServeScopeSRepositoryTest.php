<?php

namespace Repository;

use App\Repository\RepsServeScopeSRepository;
use App\Repository\UsersRepository;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class RepsServeScopeSRepositoryTest extends TestCase
{

    public function testGetUserServeSeries()
    {
        $user = UsersRepository::getUserByUid(145);
        $resp = RepsServeScopeSRepository::getUserServeSeries($user);
        $this->assertTrue($resp->isNotEmpty());
    }
}
