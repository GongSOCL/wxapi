<?php

namespace HyperfTest\Repository;

use App\Repository\ComplianceExamRepository;
use App\Repository\UsersRepository;
use PHPUnit\Framework\TestCase;

class ComplianceExamRepositoryTest extends TestCase
{

    public function testGetUserExamsWithPage()
    {
        $user = UsersRepository::getUserByUid(145);
        $resp = ComplianceExamRepository::getUserExamsWithPage($user, ['bbb'], 'aaa', 1, 10);
        $this->assertTrue($resp->isEmpty());
    }
}
