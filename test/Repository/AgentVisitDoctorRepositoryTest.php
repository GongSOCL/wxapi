<?php

namespace HyperfTest\Repository;

use App\Repository\AgentVisitDoctorRepository;
use App\Repository\AgentVisitRepository;
use PHPUnit\Framework\TestCase;
use Repository\AgentVisitRepositoryTest;

class AgentVisitDoctorRepositoryTest extends TestCase
{

    public function testGetDoctors()
    {
        $visit = AgentVisitRepository::getVisitById(1);
        $this->assertNotNull($visit);
        $rep = AgentVisitDoctorRepository::getDoctors($visit);
        $this->assertTrue($rep->isEmpty());
    }
}
