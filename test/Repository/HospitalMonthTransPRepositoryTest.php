<?php

namespace HyperfTest\Repository;

use App\Model\Qa\HospitalMonthTransP;
use App\Repository\DrugProductRepository;
use App\Repository\HospitalMonthTransPRepository;
use App\Repository\YouyaoHospitalRepository;
use PHPUnit\Framework\TestCase;

class HospitalMonthTransPRepositoryTest extends TestCase
{

    public function testGetMonthTopByHospitalAndProducts()
    {
        $hospital = YouyaoHospitalRepository::getHospitalById(7305);
        $productYYID = [
            '1CFA19C5921EFAA41E8A03B3E852B7AC',
            '3CFB4279AC717E90806E1D441546A513'
        ];
        $resp = HospitalMonthTransPRepository::getMonthTopByHospitalAndProducts($hospital, $productYYID);
        $this->assertTrue($resp->isNotEmpty());
        print_r($resp->toArray());
    }

    public function testGetAfterHospitalProductSumAndAvg()
    {
        $hospital = YouyaoHospitalRepository::getHospitalById(7305);
        $product = DrugProductRepository::getById(1);
        $resp = HospitalMonthTransPRepository::getAfterHospitalProductSumAndAvg($hospital, $product, '2019-01');
        $this->assertNotNull($resp);
        var_dump($resp['sum_num']);
        print_r($resp->toArray());
    }
}
