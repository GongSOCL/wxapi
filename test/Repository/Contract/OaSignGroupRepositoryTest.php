<?php

namespace HyperfTest\Repository\Contract;

use App\Helper\Helper;
use App\Model\Qa\OaSignGroup;
use App\Repository\Contract\OaSignGroupRepository;
use App\Repository\UsersRepository;
use App\Service\Contract\ContractService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class OaSignGroupRepositoryTest extends TestCase
{

    protected function setUp()
    {
        Db::beginTransaction();
        parent::setUp();
    }

    protected function tearDown()
    {
        Db::rollBack();;
        parent::tearDown();
    }

    public function testGetRestToGenerateHtml()
    {
        $user = UsersRepository::getUserByUid(10);
        $this->assertNotNull($user);
        Context::set('user', $user);
        $group = OaSignGroupRepository::addOne(uniqid(), '10', '2022-01-01', '2022-12-30', '2023-12-30');
        $this->assertNotNull($group);
        print_r($group->toArray());
        $group->state = 2;
        $group->save();
        [$page, $data] = ContractService::contractGroupList(2);
        $this->assertNotEmpty($data);
        print_r($data);
    }
}
