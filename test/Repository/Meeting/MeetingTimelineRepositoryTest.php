<?php
declare(strict_types=1);
namespace HyperfTest\Repository\Meeting;

use App\Repository\Meeting\MeetingApplyRepository;
use App\Repository\Meeting\MeetingTimelineRepository;
use PHPUnit\Framework\TestCase;

class MeetingTimelineRepositoryTest extends TestCase
{

    public function testGetBetween()
    {
        $resp = MeetingTimelineRepository::getBetween(
            14,
            '2022-07-12 15:37:00',
            '2022-07-31 19:00:00'
        );
        $this->assertTrue($resp->isEmpty());
    }
}
