<?php

namespace HyperfTest\Repository;

use App\Repository\WechatUserRepository;
use PHPUnit\Framework\TestCase;

class WechatUserRepositoryTest extends TestCase
{

    public function testGetWechatInfoByUserMobile()
    {
        $resp = WechatUserRepository::getWechatInfoByUserMobile([
            '18622591705',
            '13482571444'
        ]);
        $this->assertTrue($resp->isNotEmpty());
        print_r($resp->pluck('openid')->all());
    }
}
