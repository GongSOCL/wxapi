<?php

namespace HyperfTest\Repository;

use App\Repository\RepsServeScopeRepository;
use App\Repository\UsersRepository;
use PHPUnit\Framework\TestCase;

class RepsServeScopeRepositoryTest extends TestCase
{

    public function testGetUserServeHospital()
    {
        $user = UsersRepository::getUserByUid(145);
        $this->assertNotNull($user);
        $hospitals = RepsServeScopeRepository::getUserServeHospital($user, "瓜");
        $this->assertGreaterThan(0, $hospitals->count());
        var_dump($hospitals->toArray());
    }
}
