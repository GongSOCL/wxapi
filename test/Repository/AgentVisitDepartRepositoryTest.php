<?php

namespace Repository;

use App\Repository\AgentVisitDepartRepository;
use PHPUnit\Framework\TestCase;

class AgentVisitDepartRepositoryTest extends TestCase
{

    public function testGetVisitDepartsByVisitIds()
    {
        $departs = AgentVisitDepartRepository::getVisitDepartsByVisitIds([
            1, 2, 3
        ]);
        $this->assertTrue($departs->isEmpty());
    }
}
