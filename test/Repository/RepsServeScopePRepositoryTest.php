<?php

namespace HyperfTest\Repository;

use App\Repository\RepsServeScopePRepository;
use App\Repository\UsersRepository;
use App\Repository\YouyaoHospitalRepository;
use PHPUnit\Framework\TestCase;

class RepsServeScopePRepositoryTest extends TestCase
{

    public function testGetByUserAndHospital()
    {
        $user = UsersRepository::getUserByUid(145);
        $hospital = YouyaoHospitalRepository::getHospitalById(530);

        $resp = RepsServeScopePRepository::getByUserAndHospital($user, $hospital);
        $this->assertTrue($resp->isNotEmpty());
    }

    public function testGetUserServeProducts()
    {
        $user = UsersRepository::getUserByUid(145);
        $resp = RepsServeScopePRepository::getUserServeProducts($user);
        $this->assertTrue($resp->isNotEmpty());
        var_dump($resp->count());
    }
}
