<?php

namespace HyperfTest\Repository;

use App\Repository\DrugSkuRepository;
use App\Repository\UsersRepository;
use PHPUnit\Framework\TestCase;

class DrugSkuRepositoryTest extends TestCase
{

    public function testGetUserServeDrugs()
    {
        $user = UsersRepository::getUserByUid(219);
        $this->assertNotNull($user);
        $reps = DrugSkuRepository::getUserServeDrugs($user);
        $this->assertTrue($reps->isNotEmpty());
    }
}
