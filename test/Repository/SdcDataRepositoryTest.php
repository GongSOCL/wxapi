<?php

namespace HyperfTest\Repository;

use App\Model\Qa\Users;
use App\Repository\SdcDataRepository;
use App\Repository\UsersRepository;
use PHPUnit\Framework\TestCase;

class SdcDataRepositoryTest extends TestCase
{

    public function testGetLastDate()
    {
        $resp = SdcDataRepository::getLastDate('2015-11-01', 6);
        $this->assertNotNull($resp);
        var_dump($resp);
    }

    public function testGetDailyTrans()
    {
        $user = UsersRepository::getUserByUid(145);
        $skuYYIDS = [];
        $reps = SdcDataRepository::getDailyTrans($user, $skuYYIDS, '2022-01-25', '2022-01-25');
        $this->assertTrue($reps->isEmpty());
    }
}
