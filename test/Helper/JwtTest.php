<?php

namespace HyperfTest\Helper;

use App\Helper\Jwt;
use App\Repository\UsersRepository;
use Hyperf\Utils\ApplicationContext;
use PHPUnit\Framework\TestCase;

class JwtTest extends TestCase
{

    public function testDecode()
    {
        $user = UsersRepository::getUserByUid(3);
        $this->assertNotNull($user);
        $jwt = ApplicationContext::getContainer()
            ->get(Jwt::class);
        $token = $jwt->generate($user);
        $this->assertNotFalse($token);
        var_dump($token);
        $payload = $jwt->decode($token);
        $this->assertNotFalse($payload);
        var_dump($payload);
    }

    public function testGenerate()
    {
        $key = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ5b3V5YW8iLCJhdWQiOiJ5b3V5YW8tZW1wbG95ZWVzIiwiaWF0IjoxNjMxODU5MDEzLCJleHAiOjE2MzE4NTkwMTMsIm5iZiI6MTYzMTg1OTAxMywic3ViIjo1MzQ2fQ.tbt_NLKC5r_m8xfYOlKEKcRJuLNBSZQFy2LcU7sOiKI";
        $jwt = ApplicationContext::getContainer()
            ->get(Jwt::class);
        $payload = $jwt->decode($key);
        $this->assertNotFalse($payload);
        var_dump($payload);
    }
}
