<?php

namespace HyperfTest\Helper;

use App\Helper\Helper;
use Elasticsearch\Endpoints\Cat\Help;
use PHPUnit\Framework\TestCase;

class HelperTest extends TestCase
{

    public function testCreatePaperShareKey()
    {
        $result = Helper::createPaperShareKey(6, 0, 0, 4938, 2, 0);
        var_dump($result);
        $this->assertNotNull($result);

        $share = Helper::decodeShareKey($result);
        $this->assertNotEmpty($share);
        var_dump($share->serializeToJsonString());

    }

    public function testDownloadFileToTmp()
    {
        $res = Helper::downloadFileToTmp("http://localhost:9199/wxapi-testing/contract-pdf/20220317/34_42_62334c3adb414.pdf");
        fclose($res);
        $this->assertTrue(true);
    }

    public function testEncAes()
    {
        $pwd = uniqid();
        $str = uniqid();
        $enc = Helper::aesEnc($str, $pwd);
        $this->assertNotNull($enc);
        echo $enc . PHP_EOL;

        $dec = Helper::aesDec($enc, $pwd);
        $this->assertNotNull($dec);
        $this->assertEquals($str, $dec);
    }
}
