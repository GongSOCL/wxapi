<?php

namespace Helper;

use App\Helper\Helper;
use App\Helper\Lock;
use Hyperf\Redis\Redis;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Coroutine;
use Hyperf\Utils\Exception\ParallelExecutionException;
use Hyperf\Utils\Parallel;
use PHPUnit\Framework\TestCase;

class LockTest extends TestCase
{
    private $lock;

    protected function setUp()
    {
        $this->lock = ApplicationContext::getContainer()->get(Lock::class);
        parent::setUp();
    }

    public function testAcquire()
    {
        $parallel= new Parallel(10);
        for ($i=0; $i<20; $i++) {
            $randomTime = mt_rand(2,5);
            $parallel->add(function() use ($randomTime) {
                $coroutineId = Coroutine::id();
                $name = 'hospital:5';
                $lock = ApplicationContext::getContainer()->get(Lock::class);
                $identifier = $lock->acquire($name, 15, 10);
                if (!$identifier) {
                  echo "coroutine {$coroutineId} acquire lock fail \n";
                  return;
                } else {
                    echo "coroutine {$coroutineId} acquire lock success \n";
                    echo "{$coroutineId} try lock:" . $lock->tryLock($name) . PHP_EOL;
                }

                \Swoole\Coroutine::sleep($randomTime);

                $releaseResult = $lock->release($name, $identifier);
                if (!$releaseResult) {
                    echo "coroutine {$coroutineId} release lock fail \n";
                } else {
                    echo "coroutine {$coroutineId} release lock success \n";
                }
            });
        }

        try {
            $parallel->wait();
        } catch (ParallelExecutionException  $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    public function testRedis()
    {
       $redis = Helper::getRedis();
//       var_dump(call_user_func_array([$redis,'geoAdd'], ["abc", 15, 37, "p1", 16, 37, "p2"]));
        $res = $redis->georadius('Sicily', 15, 37, 200000, 'm', [
           'WITHDIST'
       ]);
       print_r($res);

        $this->assertTrue(true);
    }
}
