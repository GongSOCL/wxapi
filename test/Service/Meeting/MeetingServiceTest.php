<?php
declare(strict_types=1);
namespace HyperfTest\Service\Meeting;

use App\Service\Meeting\MeetingService;
use Grpc\Livetype\CommonInfo;
use PHPUnit\Framework\TestCase;

class MeetingServiceTest extends TestCase
{

    public function testGetLiveTypeMapByIds()
    {
        $map = MeetingService::getLiveTypeMapByIds([1, 3, 4]);
        /** @var CommonInfo $item */
        foreach ($map as $item) {
            echo sprintf("%d \t %s \t %d \n", $item->getId(), $item->getName(), $item->getMaxTimes());
        }
       foreach ($map as $k => $v) {
           echo sprintf("%d => %s", $k, $v->getName()) . PHP_EOL;
       }
        $this->assertNotEmpty($map);
    }
}
