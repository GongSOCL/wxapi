<?php
declare(strict_types=1);
namespace HyperfTest\Service\Doctor;

use App\Service\Doctor\DoctorInfoService;
use PHPUnit\Framework\TestCase;

class DoctorInfoServiceTest extends TestCase
{

    public function testSearchDoctors()
    {
        $resp = DoctorInfoService::searchDoctors("", 10);
        $this->assertNotEmpty($resp);
    }
}
