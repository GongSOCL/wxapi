<?php

namespace HyperfTest\Service\Doctor;

use App\Service\Doctor\InfoService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;
use PHPUnit\Framework\TestCase;
use Youyao\Framework\Components\Msg\MessageRpcClient;

class InfoServiceTest extends TestCase
{
    protected function setUp()
    {
        Db::connection('default')->beginTransaction();
        parent::setUp();
    }

    protected function tearDown()
    {
        Db::connection('default')->rollBack();
        parent::tearDown();
    }

    public function testMergeDoctor()
    {
        $res = ApplicationContext::getContainer()
            ->get(MessageRpcClient::class)
            ->sendCaptureCode("18221101009", "45678");
        $this->assertTrue(true);
        var_dump($res);
    }

    public function testDeleteDoctors()
    {
        $id = (int) 1;
        InfoService::deleteDoctor($id);
        $this->assertTrue(true);
    }
}
