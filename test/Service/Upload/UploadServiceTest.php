<?php

namespace Service\Upload;

use App\Service\Upload\UploadService;
use PHPUnit\Framework\TestCase;

class UploadServiceTest extends TestCase
{

    public function testAddUpload()
    {
        $res = UploadService::addUpload(
          'user',
            'testing/agent/visit/20220920/H5_BingWallpaper_1663657278051.jpg',
            'H5_BingWallpaper_1663657278051.jpg',
            'https://img.youyao99.com/user/testing/agent/visit/20220920/H5_BingWallpaper_1663657278051.jpg',
            1
        );
        $this->assertNotEmpty($res);
        print_r($res);
    }
}
