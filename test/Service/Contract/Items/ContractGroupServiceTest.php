<?php

namespace HyperfTest\Service\Contract\Items;

use App\Model\Qa\OaSignGroup;
use App\Repository\Contract\OaSignGroupRepository;
use App\Service\Contract\Items\ContractGroupService;
use PHPUnit\Framework\TestCase;

class ContractGroupServiceTest extends TestCase
{

    public function testGeneratePdfContract()
    {
        $group = OaSignGroupRepository::getGroupById(32);
        $this->assertNotNull($group);
        ContractGroupService::generatePdfContract($group, 'http://localhost:9199/wxapi-testing/contract/sign/20220317/145_6233406018955.txt');

        $this->assertTrue(true);
    }
}
