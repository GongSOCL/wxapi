<?php
declare(strict_types=1);
namespace HyperfTest\Service\Contract;

use App\Model\Qa\OaSignGroup;
use App\Model\Qa\OaUserSign;
use App\Repository\Contract\OaUserSignRepository;
use App\Repository\UsersRepository;
use App\Service\Contract\ManageService;
use Co\WaitGroup;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use Hyperf\Utils\Str;
use PHPUnit\Framework\TestCase;

class ManageServiceTest extends TestCase
{

    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(145);
        Context::set('user', $user);
        parent::setUp();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testAddYouyaoAttachment()
    {
        $group = ManageService::addContractGroup(
            Str::random(8),
            145,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);

        $contract = ManageService::addYouyaoAttachment($group->id, Str::random(6), [
            [
                'serve_id' => 40,
                'carryon_num' => mt_rand(1000, 50000),
                'target_num' => mt_rand(1000, 5000)
            ]
        ]);
        $this->assertNotNull($contract);
        print_r($contract->toArray());
    }

    public function testGeneratePdf()
    {

    }

    public function testAddYouyaoMaster()
    {
        $group = ManageService::addContractGroup(
            Str::random(8),
            145,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);

        $contract = ManageService::addYouyaoMaster($group->id, Str::random(6));
        $this->assertNotNull($contract);
        print_r($contract->toArray());
    }

    public function testAddUserAgree()
    {
        $group = ManageService::addContractGroup(
            Str::random(8),
            145,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);

        $contract = ManageService::addUserAgree($group->id, Str::random(6));
        $this->assertNotNull($contract);
        print_r($contract->toArray());
    }

    public function testGenerateHtml1()
    {
        //添加合同组
        $group = ManageService::addContractGroup(
            Str::random(24),
            145,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);

        //添加主合同
        $master  = ManageService::addYouyaoMaster($group->id, Str::random(24));
        $this->assertNotNull($master);

        //生成html模板任务并投递
        ManageService::generateHtml($group->id);
        $this->assertTrue(true);
    }

    public function testGenerateHtml2()
    {
        //添加合同组
        $group = ManageService::addContractGroup(
            Str::random(24),
            145,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);

        //添置附件合同
        $contract = ManageService::addYouyaoAttachment(
          $group->id,
            Str::random(24),
            [
                [
                    'serve_id' => 40,
                    'carryon_num' => mt_rand(1000, 50000),
                    'target_num' => mt_rand(1000, 5000)
                ]
            ]
        );
        $this->assertNotNull($contract);

        ManageService::generateHtml($group->id);
    }

    public function testGenerateHtml3()
    {
        //添加合同组
        $group = ManageService::addContractGroup(
            Str::random(24),
            145,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);

        //添加知情同意书
        $contract = ManageService::addUserAgree($group->id, Str::random(24));
        $this->assertNotNull($contract);

        ManageService::generateHtml($group->id);
    }

    public function testGenerateHtml()
    {
        //添加合同组
        $group = ManageService::addContractGroup(
            Str::random(24),
            145,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);

        //添加主合同
        $master  = ManageService::addYouyaoMaster($group->id, Str::random(24));
        $this->assertNotNull($master);

        //添置附件合同
        $contract = ManageService::addYouyaoAttachment(
            $group->id,
            Str::random(24),
            [
                [
                    'serve_id' => 40,
                    'carryon_num' => mt_rand(1000, 50000),
                    'target_num' => mt_rand(1000, 5000)
                ]
            ]
        );
        $this->assertNotNull($contract);

        //添加知情同意书
        $contract = ManageService::addUserAgree($group->id, Str::random(24));
        $this->assertNotNull($contract);

        ManageService::generateHtml($group->id);

    }

    public function testAddContractGroup()
    {
        $group = ManageService::addContractGroup(
            Str::random(8),
            10,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);
        print_r($group->toArray());
    }

    public function testHtmlGenerated()
    {
        //添置合同组
        $group = ManageService::addContractGroup(
            Str::random(8),
            145,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);

        //添加主合同
        $c1 = ManageService::addYouyaoMaster($group->id, Str::random(12));
        $this->assertInstanceOf(OaUserSign::class,  $c1);
        //添加知情同意书
        $c2 = ManageService::addUserAgree($group->id, Str::random(6));
        $this->assertInstanceOf(OaUserSign::class,  $c2);

        //更新合同已经生成
        $time = time();
        $c1->pdf_link = "http://www.baidu.com";
        $c1->state = OaUserSign::STATE_PDF_GENERATED;
        $c1->updatetime = $time;
        $c1->save();

        $c2->pdf_link = "http://www.baidu.com";
        $c2->state = OaUserSign::STATE_PDF_GENERATED;
        $c2->updatetime = $time;
        $c2->save();

        $wg = new \Swoole\Coroutine\WaitGroup();
        $wg->add(2);
        go(function () use($c1, $wg) {
            ManageService::htmlGenerated($c1->id, true, 'http://www.baidu.com');
            $wg->done();;
        });

        go(function () use($c2, $wg) {
            ManageService::htmlGenerated($c2->id, true, 'http://www.baidu.com');
            $wg->done();
        });
        $wg->wait(300);
        print_r($group->toArray());
    }

    public function testPdfGenerated()
    {
        //添置合同组
        $group = ManageService::addContractGroup(
            Str::random(8),
            145,
            "2022-01-01",
            "2022-12-30"
        );
        $this->assertNotNull($group);

        //添加主合同
        $c1 = ManageService::addYouyaoMaster($group->id, Str::random(12));
        $this->assertInstanceOf(OaUserSign::class,  $c1);

        $group->state = OaSignGroup::STATE_SIGNED;
        $group->updatetime  = date('Y-m-d H:i:s');
        $group->save();

        //添加知情同意书
        $c2 = ManageService::addUserAgree($group->id, Str::random(6));
        $this->assertInstanceOf(OaUserSign::class,  $c2);

        $time = time();
        $c1->state = OaUserSign::STATE_PDF_GENERATED;
        $c1->updatetime = $time;
        $c1->save();

        $c2->pdf_link = "http://www.baidu.com";
        $c2->state = OaUserSign::STATE_PDF_GENERATED;
        $c2->updatetime = $time;
        $c2->save();

        $wg = new \Swoole\Coroutine\WaitGroup();
        $wg->add(2);
        go(function () use($c1, $wg) {
            ManageService::pdfGenerated($c1->id, true, 'http://www.baidu.com');
            $wg->done();
        });
        go(function () use($c2, $wg) {
            ManageService::pdfGenerated($c2->id, true, 'http://www.baidu.com');
            $wg->done();
        });
        $wg->wait(300);
        $this->assertTrue(true);
    }
}
