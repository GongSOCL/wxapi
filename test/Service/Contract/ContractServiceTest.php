<?php
declare(strict_types=1);
namespace HyperfTest\Service\Contract;

use App\Repository\UsersRepository;
use App\Service\Contract\ContractService;
use App\Service\Contract\ManageService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class ContractServiceTest extends TestCase
{

    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(145);
        Context::set('user', $user);
        Db::beginTransaction();
        parent::setUp();
    }

    protected function tearDown()
    {
        Db::commit();;
        parent::tearDown();
    }

    public function testSignContract()
    {
        $img = file_get_contents("http://localhost:9199/wxapi-testing/sign_data.txt");
        ContractService::signContract(34, $img, '62334c2437e20');
        $this->assertTrue(true);
    }

    public function testSendContractWithMail()
    {
        ContractService::sendContractWithMail(34, 'zhenghua.zhao@youyaomedtech.com');
        $this->assertTrue(true);
    }

    public function testGetContractTplInfo()
    {
        $id = 36;
        $info = ContractService::getContractTplInfo($id);
        $this->assertNotEmpty($info);
        echo $info . PHP_EOL;
    }

    public function testContractGroupList()
    {
        [$page, $group, $signCount] = ContractService::contractGroupList(0, 1, 10);
        $this->assertGreaterThan(0, $signCount);
        print_r($page);
        print_r($group);
    }

    public function testGetContractSignedPdf()
    {

    }

    public function testStartContractSign()
    {
        $groupId = 34;
        [$requestId, $nextId]  = ContractService::startContractSign($groupId);
        $this->assertGreaterThan(0, $nextId);
        echo $requestId . PHP_EOL;
        echo $nextId . PHP_EOL;
    }

    public function testGetContractTpl()
    {
        [$content, $nextId] = ContractService::getContractTpl(32, 38, '62333b767bd0f');
        $this->assertNotEmpty($content);
        echo $content . PHP_EOL;
        echo $nextId . PHP_EOL;
    }
}
