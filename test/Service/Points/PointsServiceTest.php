<?php

namespace HyperfTest\Service\Points;

use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Service\Points\PointsService;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class PointsServiceTest extends TestCase
{
    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(145);
        Context::set('user', $user);
    }

    public function testAddQuestionnaireActivityEvent()
    {
        PointsService::addQuestionnaireActivityEvent(
            5346,
            64,
            5462
        );
        $this->assertTrue(true);
    }

    public function testGetUserPoints()
    {
        [$s, $m] = PointsService::getUserPoints();
        $this->assertTrue(true);
        var_dump($s, $m);

    }

    public function testGetExchangeGoodsList()
    {
        [$page, $data] = PointsService::getExchangeGoodsList(1, 10, true);
        $this->assertNotEmpty($data);
        $this->assertGreaterThan(0, $page['pages']);
        print_r($data);
        print_r($page);
    }

    public function testGetUserPointDetails()
    {
        [$page, $data] = PointsService::getUserPointDetails();
        print_r($page);
        $this->assertNotEmpty($data);
        $this->assertGreaterThan(0, $page['pages']);
        print_r($data);
    }

    public function testGetGoodsDetails()
    {
        $YYID = 'zO70KmoJ8bd43nerKYgjDR6wNqlvAkVQ';
        $detail = PointsService::getGoodsDetails($YYID);
        $this->assertNotNull($detail);
        print_r($detail);
    }

    public function testAppExchangeGoods()
    {
        $goodsYYID = 'zO70KmoJ8bd43nerKYgjDR6wNqlvAkVQ';
        $num = 1;
        $wechat = WechatUserRepository::getById(788);
        $this->assertNotNull($wechat);
        PointsService::appExchangeGoods($goodsYYID, $num, $wechat);
        $this->assertTrue(true);
    }
}
