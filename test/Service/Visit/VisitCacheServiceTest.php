<?php

namespace Service\Visit;

use App\Service\Visit\VisitCacheService;
use PHPUnit\Framework\TestCase;

class VisitCacheServiceTest extends TestCase
{

    public function testRefreshCache()
    {
        VisitCacheService::refreshCache(
            1,
            [
                [
                    'id' => 1,
                    'lng' => 1.1111,
                    'lat' => 1.222
                ],
                [
                    'id' => 2,
                    'lng' => 1.1111,
                    'lat' => 1.222
                ],
                [
                    'id' => 3,
                    'lng' => 1.1111,
                    'lat' => 1.222
                ]
            ],
            15000,
            uniqid()
        );
        $this->assertTrue(true);
    }

    public function testCalcGpsDistinct()
    {
        $hospitalGps = [
            [
                'lng' => '117.30609',
                'lat' => '31.896048'
            ],
            [
                'lng' => '117.29057',
                'lat' => '31.857082'
            ],
            [
                'lng' => '117.211845',
                'lat' => '31.870987'
            ],
            [
                'lng' => '117.2678',
                'lat' => '31.847538'
            ]
        ];
        $checkInGps = [
            'lng' => '121.457443',
            'lat' => '31.227626'
        ];
        $checkOutGps = [
            'lng' => '121.448910',
            'lat' => '31.226810'
        ];
        $res = VisitCacheService::calcGpsDistinct($hospitalGps, $checkInGps, $checkOutGps);
        $this->assertNotEmpty($res);
        print_r($res);
    }
}
