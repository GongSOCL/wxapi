<?php

namespace HyperfTest\Service\Visit;

use App\Constants\DataStatus;
use App\Model\Qa\Users;
use App\Repository\AgentVisitConfigRepository;
use App\Repository\AgentVisitRepository;
use App\Repository\UsersRepository;
use App\Service\Visit\VisitService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use Hyperf\Utils\Coroutine;
use Hyperf\Utils\Str;
use Hyperf\Validation\ValidatorFactory;
use PHPUnit\Framework\TestCase;

class VisitServiceTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        //设置登陆用户
        $user = UsersRepository::getUserByUid(145);
        context::set('user', $user);
        //开启事务
        Db::connection("default")->beginTransaction();
    }

    protected function tearDown(): void
    {
        Db::connection('default')->rollBack();
        parent::tearDown();
    }

    public function testList()
    {
        [$page, $data] = VisitService::list(
            '2021-01-01 00:00:00',
            date('Y-m-d H:i:s'),
            '',
            '',
            1,
            1,
            0,
            [],
            1,
            10
        );
        var_dump($data, $page);
        $this->assertTrue(true);
    }

    public function testInfo()
    {
        $data = VisitService::info(1);
        $this->assertNotEmpty($data);
        var_dump($data);
    }

    public function testGetVisitConfig()
    {
        $config = VisitService::getVisitConfig(2);
        $this->assertNotEmpty($config);
        print_r($config);
    }

    public function testCheckIn()
    {
        $visit = VisitService::checkIn(
            1,
            0,
            [1, 2, 3],
            31.23317,
            121.455102,
            'SOHO东海广场',
            0,
            '测试comment',
            DataStatus::COORDINATE_GCJ02
        );
        $this->assertGreaterThan(0, $visit->id);
        print_r($visit->toArray());

        $info  = VisitService::info($visit->id);
        $this->assertNotEmpty($info);
        print_r($info);

        $checkOutInfo = VisitService::getNewCheckOutInfo($visit->id);
        $this->assertNotEmpty($checkOutInfo);
        var_dump($checkOutInfo);
    }

    public function testEditCheckIn()
    {
        $visit = VisitService::checkIn(
            5,
            0,
            [1, 2, 3],
            31.23317,
            121.455102,
            'SOHO东海广场',
            0,
            '测试comment',
            DataStatus::COORDINATE_GCJ02
        );
        $this->assertNotNull($visit);
        $this->assertEquals(0, $visit->hospital_id);
        $info = VisitService::info($visit->id);
        $this->assertEmpty($info['hospital']);
        print_r($info);

        VisitService::editCheckIn(
            $visit->id,
            1,
            530,
            [2,3,4],
            31.23317,
            121.455102,
            'SOHO东海广场',
            0,
            '测试comment',
            DataStatus::COORDINATE_GCJ02
        );
        $this->assertTrue(true);
        $info = VisitService::info($visit->id);
        $this->assertNotEmpty($info['hospital']);
        print_r($info);
    }

    public function testEditCheckOut()
    {
        $visit = VisitService::checkIn(
            1,
            530,
            [1, 2, 3],
            31.23317,
            121.455102,
            'SOHO东海广场',
            0,
            '测试comment'
        );
        $this->assertNotNull($visit);
        $this->assertEquals(530, $visit->hospital_id);
        $info = VisitService::info($visit->id);
        $this->assertNotEmpty($info['hospital']);
        print_r($info);

        VisitService::checkOut(
            $visit->id,
            530,
            1,
            [2],
            [1, 2],
            Str::random(6),
            Str::random(6),
            31.23317,
            121.455102,
            'SOHO东海广场',
            [1,2,3],
            1,
            '测试comment',
            [1, 2]
        );
        $this->assertTrue(true);
        $info = VisitService::info($visit->id);
        $this->assertNotEmpty($info['hospital']);
        print_r($info);

        VisitService::editCheckOut(
           $visit->id,
           530,
           1,
           [2],
            [2,3,5],
            Str::random(6),
            Str::random(6),
            31.23317,
            121.455102,
            'SOHO东海广场',
            [1,2,5],
            1,
            Str::random(6),
            [1, 2],
            DataStatus::COORDINATE_GCJ02
        );
        $info = VisitService::info($visit->id);
        $this->assertNotEmpty($info['hospital']);
        print_r($info);
    }

    public function testValidate()
    {
        $a = ['s' => '测试'];
        $validator = ApplicationContext::getContainer()->get(ValidatorFactory::class)
            ->make($a, [
                's'   => 'string|lt:2',
            ], [
                's.lt' => '字符超长'
            ]);
        $this->assertTrue(true);
        var_dump($validator->getMessageBag()->first());
    }

    public function testGetGroupDoctors()
    {
        $ds = VisitService::getGroupDoctors(530, '');
        $this->assertNotEmpty($ds);
        print_r($ds);
    }

    public function testGetGeoInfo()
    {
        $info = VisitService::getGeoInfo(1);
        $this->assertTrue(true);
        var_dump($info->serializeToJsonString());
    }

    public function testAddCheckInNotice()
    {
        $visit = AgentVisitRepository::getVisitById(1);
        $this->assertNotNull($visit);
        var_dump("start test");
        VisitService::addCheckInNotice($visit);
        $this->assertTrue(true);
    }

    public function testGenerateHospitalRandomGeoPos()
    {
        $resp = VisitService::generateHospitalRandomGeoPos(
            334180
        );
        $this->assertNotEmpty($resp);
        var_dump($resp);
    }
}
