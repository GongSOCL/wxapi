<?php

namespace HyperfTest\Service\Common;

use App\Helper\Helper;
use App\Service\Common\SmsCacheService;
use PHPUnit\Framework\TestCase;

class SmsCacheServiceTest extends TestCase
{

    public function testPreSendSms()
    {
        SmsCacheService::preSendSms('19221175810', uniqid(), Helper::generateNInteger(6));
        $this->assertTrue(true);
    }

    public function testGetPreSendInfo()
    {
        $sid = uniqid();
        SmsCacheService::preSendSms('19221175810', $sid, Helper::generateNInteger(6));
        $this->assertTrue(true);

        $info = SmsCacheService::getPreSendInfo($sid);
        $this->assertNotEmpty($info);
        print_r($info);
    }

    public function testAddRecent()
    {
        SmsCacheService::addRecent('18900000000', '123456');
        $this->assertTrue(true);
    }

    public function testFailRemoveCode()
    {
        SmsCacheService::failRemoveCode("187", '1134');
        $this->assertTrue(true);
    }
}
