<?php

namespace HyperfTest\Service\Common;

use App\Service\Common\AdaptionService;
use PHPUnit\Framework\TestCase;

class AdaptionServiceTest extends TestCase
{

    public function testListFields()
    {
        [$page, $data] = AdaptionService::listFields();
        $this->assertNotEmpty($data);
        print_r($page);

        [$page, $data] = AdaptionService::listFields('abc');
        $this->assertEmpty($data);
    }

    public function testListAdaption()
    {
        [$page, $data] = AdaptionService::listAdaption();
        $this->assertNotEmpty($data);
        print_r($page);

        [$page, $data] = AdaptionService::listAdaption('abc');
        $this->assertEmpty($data);
    }
}
