<?php

namespace HyperfTest\Service\Common;

use App\Service\Common\SmsCacheService;
use App\Service\Common\SmsService;
use PHPUnit\Framework\TestCase;

class SmsServiceTest extends TestCase
{

    public function testSendVerify()
    {
        SmsService::sendVerify('18221175818');
        $this->assertTrue(true);
    }

    public function testIsLock()
    {
        $mobile = "18221171090";
        $isLock = SmsCacheService::isLock($mobile);
        $this->assertTrue($isLock);
        $isLock = SmsCacheService::isLock($mobile);
        $this->assertFalse($isLock);
        $isLock = SmsCacheService::isLock($mobile);
        $this->assertFalse($isLock);
    }
}
