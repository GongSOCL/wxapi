<?php

namespace Service\Common;

use App\Model\Qa\Users;
use App\Repository\UsersRepository;
use App\Service\Common\DepartmentService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use Hyperf\Utils\Str;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use PHPUnit\Framework\TestCase;

class DepartmentServiceTest extends TestCase
{
    protected function setUp()
    {
        //模拟登陆
        $user = UsersRepository::getUserByUid(145);
        $this->assertInstanceOf(Users::class, $user);
        context::set('user', $user);

        Db::beginTransaction();
        parent::setUp();
    }

    protected function tearDown()
    {
        Db::rollBack();
        parent::tearDown();
    }

    public function testListSystemDeparts()
    {
        $keywords = "";
        $lists = DepartmentService::listSystemDeparts($keywords);
        $this->assertNotEmpty($lists);

        $keywords = "科";
        $lists = DepartmentService::listSystemDeparts($keywords);
        $this->assertNotEmpty($lists);

        $keywords = Str::random(6);
        $lists = DepartmentService::listSystemDeparts($keywords);
        $this->assertEmpty($lists);
    }

    public function testGetServeHospitalDeparts()
    {
        //添加数据
        DepartmentService::addServe(1, [
            1, 2, 3, 4
        ]);
        $this->assertTrue(true);

        $serveDeparts = DepartmentService::getServeHospitalDeparts(1, "科");
        $this->assertNotEmpty($serveDeparts);
        $this->assertGreaterThanOrEqual(4, count($serveDeparts));
        print_r($serveDeparts);
    }

    public function testAddServe()
    {
        DepartmentService::addServe(1, [
            1, 2
        ]);
        $this->assertTrue(true);

        DepartmentService::addServe(1, [
            1, 2, 3, 4
        ]);
        $this->assertTrue(true);
    }

    public function testValidDeparts()
    {
        $departIds = [
                'departs' => [0, 1, 2, 3]
        ];
        $validator = ApplicationContext::getContainer()->get(ValidatorFactoryInterface::class)
            ->make(
                $departIds,
                [
                    'departs' => 'array|required',
                    'departs.*' => 'required|integer|gt:0'
                ],
                [
                    'departs.*.required' => '科室不能为空',
                    'departs.*.integer|*.gt' => '科室格式不正确'
                ]
            );

        $this->assertTrue($validator->fails());
    }

    public function testListSystemDepartsWithPage()
    {
        [$page, $data] = DepartmentService::listSystemDepartsWithPage();
        $this->assertNotEmpty($data);
        print_r($page);

        [$page, $data] = DepartmentService::listSystemDepartsWithPage('abc');
        $this->assertEmpty($data);
    }
}
