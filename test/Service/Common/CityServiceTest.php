<?php

namespace HyperfTest\Service\Common;

use App\Model\Qa\Region;
use App\Service\Common\CityService;
use PHPUnit\Framework\TestCase;
use function Swoole\Coroutine\Http\request;

class CityServiceTest extends TestCase
{

    public function testGetNextRegion()
    {
        $province = CityService::getNextRegion(1, Region::LEVEL_COUNTRY);
        $this->assertNotEmpty($province);

        $cities = CityService::getNextRegion(2, Region::LEVEL_PROVINCE);
        $this->assertNotEmpty($cities);

        $districts = CityService::getNextRegion(3, Region::LEVEL_CITY);
        $this->assertNotEmpty($districts);
    }
}
