<?php

namespace HyperfTest\Service\Common;

use App\Exception\BusinessException;
use App\Repository\UsersRepository;
use App\Service\Common\ProductService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class ProductServiceTest extends TestCase
{
    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(145);
        context::set('user', $user);
        Db::beginTransaction();
    }

    protected function tearDown()
    {
       Db::rollBack();
    }

    public function testGetAppProduct()
    {
        $products = ProductService::getAppProduct();
        $this->assertNotEmpty($products);
        print_r($products);
    }

    public function testGetSeriesInfo()
    {
        $info = ProductService::getSeriesInfo(6);
        $this->assertNotEmpty($info);
        print_r($info);
    }

    public function testGetProductManual()
    {
        [$name, $manuals] = ProductService::getProductManual(2);
        $this->assertNotEmpty($manuals);
        $this->assertNotNull($name);
        print_r($manuals);
        var_dump($name);

        $this->expectException(BusinessException::class);
        [$name, $manuals] = ProductService::getProductManual(1000);
    }

    public function testGetProductInterprets()
    {
        $list = ProductService::getProductInterprets(8, 1, 10);
        $this->assertNotEmpty($list);
        print_r($list);
    }

    public function testGetUserServeDrugSeries()
    {
        $list = ProductService::getUserServeDrugSeries();
        $this->assertNotEmpty($list);
        print_r($list);
    }
}
