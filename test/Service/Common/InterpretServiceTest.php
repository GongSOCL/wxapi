<?php

namespace HyperfTest\Service\Common;

use App\Repository\UsersRepository;
use App\Service\Common\InterpretService;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class InterpretServiceTest extends TestCase
{
    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(9);
        Context::set("user", $user);
        parent::setUp();
    }

    public function testInfo()
    {
        $info = InterpretService::info(16);
        $this->assertNotEmpty($info);
        print_r($info);
    }

    public function testGetInterpretComments()
    {
        [$total, $list] = InterpretService::getInterpretComments(8);
        $this->assertNotEmpty($list);
        var_dump($total);
        print_r($list);
    }

    public function testViewInterpret()
    {
        InterpretService::viewInterpret(16);
        $this->assertTrue(true);
    }
}
