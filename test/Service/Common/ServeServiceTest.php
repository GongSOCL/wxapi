<?php

namespace HyperfTest\Service\Common;

use App\Repository\UsersRepository;
use App\Service\Common\ServeService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use Hyperf\Utils\Coroutine;
use PHPUnit\Framework\TestCase;

class ServeServiceTest extends TestCase
{
    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(145);
        Context::set('user', $user);
        Db::beginTransaction();
    }

    protected function tearDown()
    {
        Db::commit();
    }

    public function testSendAuditorServeApply()
    {
    }

    public function testGetServeProductStatistics()
    {
        [$overviewData, $chartData, $lastMonthData, $lastThreeMonthData] =
            ServeService::getServeProductStatistics(44125);
        $this->assertNotEmpty($overviewData);
        print_r(
            $lastThreeMonthData
        );
        print_r($chartData);
    }

    public function testGetServeProductStatistics1()
    {
        $start = new \DateTime('2021-09');
        $end=  clone $start;
        $end->modify("-6 months");
        $interval = new \DateInterval("P1M");
        $period = new \DatePeriod($end, $interval, $start, \DatePeriod::EXCLUDE_START_DATE);
        foreach ($period as $item) {
            echo $item->format('Y-m') . "\t";
        }
        echo PHP_EOL;

        foreach ($period as $item) {
            echo $item->format('Y-m') . "\t";
        }
        $this->assertTrue(true);
    }


    public function testRemove()
    {
        ServeService::remove(44125);
        $this->assertTrue(true);
        \Hyperf\Utils\Coroutine::sleep(5);
    }

    public function testGetServeProductDetailList()
    {
        [$info, $productList] = ServeService::getServeProductDetailList(530);
        $this->assertNotEmpty($productList);
        print_r($info);
        print_r($productList);
    }

    public function testApply()
    {
        $serve = ServeService::apply(44126, 6);
        $this->assertNotNull($serve);
        print_r($serve->toArray());
        $this->assertTrue(true);
        \Hyperf\Utils\Coroutine::sleep(5);
    }

    public function testGetServeList()
    {
        $list = ServeService::getServeList(5346);
        $this->assertNotNull($list);
        $this->assertGreaterThan(0, $list->getPage()->getTotal());
        foreach ($list->getList() as $item) {
            print_r($item->serializeToJsonString());
        }
    }

    public function testGetUserServeHospitalByUid()
    {
        $resp = ServeService::getUserServeHospitalByUid(5388);
        $this->assertGreaterThan(0, $resp->getHospitals()->count());
        var_dump($resp->serializeToJsonString());
    }

    public function testGetUserSpecialHospitalByUid()
    {
        $resp = ServeService::getUserSpecialHospitalByUid(
            5437,
            '医'
        );
        $this->assertNotNull($resp);
        print_r(json_decode($resp->serializeToJsonString()));
    }
}
