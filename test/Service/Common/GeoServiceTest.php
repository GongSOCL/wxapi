<?php

namespace HyperfTest\Service\Common;

use App\Helper\Helper;
use App\Service\Common\BaiduService;
use App\Service\Common\GeoService;
use Hyperf\Utils\ApplicationContext;
use PHPUnit\Framework\TestCase;

class GeoServiceTest extends TestCase
{

    public function testReverseGeo()
    {
        $url = GeoService::reverseGeo(121.455215, 31.233203, 'baidu');
        $this->assertNotEmpty($url);
        $this->assertNotNull($url);
        var_dump($url);
    }
}
