<?php

namespace HyperfTest\Service\Common;

use App\Exception\BusinessException;
use App\Repository\UsersRepository;
use App\Service\Common\CommentService;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use Hyperf\Utils\Str;
use Hyperf\Validation\ValidatorFactory;
use Hyperf\Validation\ValidatorFactoryFactory;
use PHPUnit\Framework\TestCase;

class CommentServiceTest extends TestCase
{

    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(9);
        Context::set("user", $user);
    }

    public function testDeleteComment()
    {
        CommentService::deleteComment(78);
        $this->assertTrue(true);
    }

    public function testAddInterceptComment()
    {
        $cm = CommentService::addInterceptComment(16, Str::random(32), 0);
        $this->assertNotNull($cm);
        var_dump($cm->id);
        print_r($cm->toArray());

        $cm = CommentService::addInterceptComment(16, Str::random(32), $cm->id);
        $this->assertNotNull($cm);
        var_dump($cm->id);
        print_r($cm->toArray());

        $this->expectException(BusinessException::class);
        $cm = CommentService::addInterceptComment(16, Str::random(32), $cm->id);
    }

    public function testAddInterceptComment1()
    {
        $s = 'hyperf 验证器的使用';
        $validator = ApplicationContext::getContainer()->get(ValidatorFactory::class)
            ->make([
                's' => $s,
            ], [
                's'   => 'required|string|max:13',
            ], [
                's.required' => '字符串不能为空',
                's.string' => '字符串格式不正确',
                's.lt' => '字符串超长'
            ]);
        var_dump($validator->errors()->first());
        $this->assertTrue($validator->errors()->isEmpty());
    }
}
