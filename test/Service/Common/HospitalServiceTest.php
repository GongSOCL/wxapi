<?php

namespace HyperfTest\Service\Common;

use App\Model\Qa\YouyaoHospital;
use App\Repository\UsersRepository;
use App\Service\Common\HospitalService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class HospitalServiceTest extends TestCase
{

    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(145);
        Context::set('user', $user);
        Db::beginTransaction();
    }

    protected function tearDown()
    {
       Db::rollBack();
    }

    public function testSearchHospitals(){
        [$page, $data] = HospitalService::searchHospitals(493, 508, [
            YouyaoHospital::HOSPITAL_LEVEL_TWO,
            YouyaoHospital::HOSPITAL_LEVEL_THREE
        ],1, 10, 6, 0, '');
        print_r($page);
        print_r($data);
        $this->assertNotEmpty($data);
    }
}
