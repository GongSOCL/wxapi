<?php

namespace HyperfTest\Service\Compliance;

use App\Model\Qa\PaperQuestionItem;
use App\Repository\PaperQuestionItemRepository;
use App\Service\Compliance\QuestionService;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use PHPUnit\Framework\TestCase;

class QuestionServiceTest extends TestCase
{

    public function testGetLogicQuestionAndAnswersByQuestionId()
    {
        /** @var Collection $answers */
        [$question, $answers] = QuestionService::getLogicQuestionAndAnswersByQuestionId(104);
        $this->assertInstanceOf(Collection::class, $answers);
        $this->assertTrue($answers->isNotEmpty());
    }
}
