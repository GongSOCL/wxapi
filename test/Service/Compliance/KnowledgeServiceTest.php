<?php

namespace Service\Compliance;

use App\Repository\UsersRepository;
use App\Service\Compliance\KnowledgeService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class KnowledgeServiceTest extends TestCase
{

    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(145);
        context::set('user', $user);
        Db::beginTransaction();
        parent::setUp();
    }

    protected function tearDown()
    {
        Db::rollBack();
        parent::tearDown();
    }

    public function testTopDirs()
    {
        $dirs = KnowledgeService::topDirs();
        $this->assertNotEmpty($dirs);
        var_dump($dirs);
    }

    public function testSearch()
    {
        $list = KnowledgeService::search("第三");
        $this->assertNotEmpty($list);
        print_r($list);
    }

    public function testTreeView()
    {
        $list = KnowledgeService::treeView(5);
        $this->assertNotEmpty($list);
        print_r($list);
    }
}
