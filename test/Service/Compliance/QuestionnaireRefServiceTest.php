<?php

namespace HyperfTest\Service\Compliance;

use App\Repository\UsersRepository;
use App\Service\Compliance\QuestionnaireRefService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class QuestionnaireRefServiceTest extends TestCase
{
    private $conn;
    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(5346);
        $this->assertNotNull($user);
        Context::set('user', $user);

        Db::beginTransaction();
    }

    protected function tearDown()
    {
        Db::rollBack();
    }

    public function testGetDoctorWechatInfo()
    {

    }

    public function testAnswerQuestion()
    {

    }

    public function testAddPaperInvitee()
    {

    }

    public function testGetAppPaperAvailableWechatDoctor()
    {
        [$page, $data] = QuestionnaireRefService::getAppPaperAvailableWechatDoctor(1);
        $this->assertNotEmpty($data);
        print_r($data);
        print_r($page);
    }

    public function testGetWechatPaperAvailableWechatDoctor()
    {
        [$page, $data] = QuestionnaireRefService::getWechatPaperAvailableWechatDoctor('3CFB4279AC717E90806E1D441546A522');
        $this->assertNotEmpty($data);
        print_r($data);
        print_r($page);
    }

    public function testGetAppAgentInvitees()
    {
        $rest = QuestionnaireRefService::getAppAgentInvitees(1);
        $this->assertNotEmpty($rest);
        $this->assertArrayHasKey('list', $rest);
        $this->assertNotEmpty($rest['list']);
        print_r($rest);

        $rest = QuestionnaireRefService::getAppAgentInvitees(89);
        $this->assertNotEmpty($rest);
        $this->assertArrayHasKey('list', $rest);
        $this->assertEmpty($rest['list']);
    }

    public function testGetWechatAgentInvitees()
    {
        $rest = QuestionnaireRefService::getWechatAgentInvitees('3CFB4279AC717E90806E1D441546A522');
        $this->assertNotEmpty($rest);
        $this->assertArrayHasKey('list', $rest);
        $this->assertNotEmpty($rest['list']);
        print_r($rest);

        $rest = QuestionnaireRefService::getWechatAgentInvitees('908161019DC911EB90EF5C3A4501FF93');
        $this->assertNotEmpty($rest);
        $this->assertArrayHasKey('list', $rest);
        $this->assertEmpty($rest['list']);
    }

    public function testReInviteDoctor()
    {

    }

    public function testGetPaperInvitees()
    {

    }

    public function testStartQuestionnaire()
    {

    }

    public function testGetQuestionItemInfo()
    {

    }

    public function testNoticeToAnswer()
    {
        QuestionnaireRefService::appNoticeToAnswer(1, 3);
    }

    public function testInviteDoctor()
    {
        QuestionnaireRefService::appInviteDoctor(1, 10);
        $this->assertTrue(true);
    }

    public function testGetNextQuestionItem()
    {

    }

    public function testCacheQuestionAnswer()
    {

    }

    public function testGetAgentsPapers()
    {
        //测试进行中的问卷
        $papers = QuestionnaireRefService::appGetAgentsPapers(true);
        $this->assertNotEmpty($papers);
        print_r($papers);

        //测试已结束问卷
        $papers = QuestionnaireRefService::appGetAgentsPapers(false);
        $this->assertEmpty($papers);
    }

    public function testGetAgentsPapers1()
    {
        //测试进行中的问卷
        $papers = QuestionnaireRefService::wechatGetAgentsPapers(true);
        $this->assertNotEmpty($papers);
        print_r($papers);

        //测试已结束问卷
        $papers = QuestionnaireRefService::wechatGetAgentsPapers(false);
        $this->assertEmpty($papers);
    }
}
