<?php

namespace HyperfTest\Service\Compliance;

use App\Helper\Helper;
use App\Service\Compliance\ExamCacheService;
use App\Service\Compliance\ExamService;
use PHPUnit\Framework\TestCase;

class ExamCacheServiceTest extends TestCase
{

    public function testCacheExamQuestionList()
    {
        $requestId = uniqid();
        ExamCacheService::cacheExamQuestionList($requestId, [1, 10, 6, 9, 5], 7200);
        $this->assertTrue(true);
    }

    public function testCheckRequestAndQuestion()
    {
        $requestId = '618b853c83fec';
        $questionId = 10;
        ExamCacheService::checkRequestAndQuestion($requestId, $questionId);
        $this->assertTrue(true);
    }

    public function testGetNextQuestionId()
    {
        $requestId = '618b853c83fec';
        $questionId = 5;
        $next = ExamCacheService::getNextQuestionId($requestId, $questionId);
        $this->assertTrue(true);
        $this->assertEmpty($questionId);
    }

    public function testCheckFinished()
    {
        $requestId = "618b9db5958a8";
        $isFinished = ExamCacheService::checkFinished($requestId);
        $this->assertFalse($isFinished);
    }

    public function testClearOldRequests()
    {
        $key = "exam:user:5:5";
        $requestId = "";
        ExamCacheService::clearOldRequests(23, 145, $requestId);
    }
}
