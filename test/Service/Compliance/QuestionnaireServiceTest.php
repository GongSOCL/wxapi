<?php

namespace HyperfTest\Service\Compliance;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Service\Compliance\QuestionnaireService;
use PHPUnit\Framework\TestCase;
use Youyao\Framework\ErrCode;

class QuestionnaireServiceTest extends TestCase
{

    public function dataProviderForAvailableDoctor(): array
    {
        return [
            ['abc', 'abc', 'abc', false, true],
        ];
    }
}
