<?php
declare(strict_types=1);
namespace Service\Group\Doctor;

use App\Service\Group\Doctor\DoctorGroupService;
use Hyperf\Utils\ApplicationContext;
use PHPUnit\Framework\TestCase;

class DoctorGroupServiceTest extends TestCase
{

    public function testGetServingAgentByDoctorRelatedHospital()
    {
        $resp = ApplicationContext::getContainer()
            ->get(DoctorGroupService::class)
            ->getServingAgentByDoctorRelatedHospital(20);
        $this->assertGreaterThan(0, $resp->getAgents()->count());
        var_dump($resp->serializeToJsonString());
    }

    public function testGetAgentOwnGroupDoctor()
    {
        $res = ApplicationContext::getContainer()
            ->get(DoctorGroupService::class)
            ->getAgentOwnGroupDoctor(20);
        $this->assertGreaterThan(0, $res->getAgents()->count());
        var_dump($res->serializeToJsonString());
    }

    public function testCheckAgentOwnerDoctorInGroup()
    {
        $res = ApplicationContext::getContainer()
            ->get(DoctorGroupService::class)
            ->checkAgentOwnerDoctorInGroup(5388, 20);
        $this->assertTrue($res);
    }
}
