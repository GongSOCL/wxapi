<?php

namespace HyperfTest\Service\User;

use App\Helper\Helper;
use App\Repository\UsersRepository;
use App\Service\User\UserService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{

    protected function setUp()
    {
        $user = UsersRepository::getUserByUid(10);
        $this->assertNotNull($user);
        Context::set('user', $user);
        Db::beginTransaction();
    }

    protected function tearDown()
    {
        Db::rollBack();
        parent::tearDown();
    }

    public function testAddVerify()
    {
        UserService::addVerify(
            'la archer',
                42,
            2,
            10,
            [
                //department_id
                1, 2
            ],
            [
                //hospital_id
                1, 2
            ],
            [
              // serve_company
                [
                    'company_id' => 2,
                    'field_id' => [1, 3],
                    'adaption_id' => [22, 24],
                    'product_name' => '测试产品1号',
                ],
                [
                    'company_id' => 2,
                    'field_id' => [2, 4],
                    'adaption_id' => [23, 25],
                    'product_name' => '测试产品2号',
                ],
            ],
            [1, 2],
            ''
        );
        $this->assertTrue(true);

        $info = UserService::getVerifyInfo();
        $this->assertNotEmpty($info);
        print_r($info);
    }

    public function testGetVerifyInfo()
    {
        $info = UserService::getVerifyInfo();
        $this->assertNotEmpty($info);
        print_r($info);
    }

    public function testListUsersWithPage()
    {
        $data = UserService::listUsersWithPage(2, '', 1, 1, 10);
        $this->assertNotNull($data);
        $this->assertGreaterThan(0, $data->getPage()->getPages());
        foreach ($data->getUsers() as $item) {
            var_dump($item->serializeToJsonString());
        }
    }

    public function testGetAdminUserVerifyInfo()
    {
        $resp = UserService::getAdminUserVerifyInfo(5340);
        $this->assertNotEmpty($resp->getCompany());
        var_dump($resp->serializeToJsonString());
    }

    public function testAuthByMobileAndWechat()
    {
        [$user, $wechat] = UserService::authByMobileAndWechat('18221175821', 'abc123');
        $this->assertNotNull($user);
        $this->assertNotNull($wechat);
    }

    public function testAddHelp()
    {
        UserService::addHelp(4, "测试纠错", 'admin@admin.com');
        $this->assertTrue(true);
    }

    public function testUpdateUserName()
    {
        $name = '张三';
        UserService::updateUserName($name);
        $user = Helper::getLoginUser();
        $this->assertNotNull($user);
        $user->refresh();
        $this->assertEquals($name, $user->name);
    }

    public function testVerifyCancel()
    {
        UserService::verifyCancel(145);
        $this->assertTrue(true);
    }

    public function testGetUserServeHospitalDoctors()
    {
        $resp = UserService::getUserServeHospitalDoctors(5388, true);
        $this->assertGreaterThan(0, $resp->getDoctors()->count());
        var_dump($resp->serializeToJsonString());
    }

    public function testGetPharmacyAgentInviteInfo()
    {
        $link = UserService::getPharmacyAgentInviteInfo();
        $this->assertNotNull($link);
        var_dump($link);
    }

    public function testGetPharmacySalesAndTopicRecordLink()
    {
        $res = UserService::getPharmacySalesAndTopicRecordLink();
        $this->assertNotNull($res);
        print_r($res);
    }
}
