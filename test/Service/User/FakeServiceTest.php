<?php

namespace HyperfTest\Service\User;

use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Service\User\FakeService;
use PHPUnit\Framework\TestCase;

class FakeServiceTest extends TestCase
{

    public function testCacheWechatInfo()
    {
        $token = FakeService::cacheWechatInfo(1, 1);
        $this->assertNotEmpty($token);

        [$uid, $wid] = FakeService::getCacheInfoByCode($token);
        echo $uid . PHP_EOL;
        echo $wid . PHP_EOL;
        $this->assertTrue(true);

        $this->expectException(BusinessException::class);
        FakeService::getCacheInfoByCode(uniqid());
    }

    public function testGetToken()
    {
        $pwd = FakeService::getEncPwd();
        $encUid = Helper::aesEnc(strval(145), $pwd);
        echo $encUid;
        $token = FakeService::getToken($encUid);
        $this->assertNotNull($token);
        echo $token . PHP_EOL;

//        $info = FakeService::auth($token);
//        $this->assertNotNull($info);
//        print_r($info);
    }
}
