#!/usr/bin/env php
<?php

ini_set('display_errors', 'on');
ini_set('display_startup_errors', 'on');

error_reporting(E_ALL);
date_default_timezone_set('Asia/Shanghai');

! defined('BASE_PATH') && define('BASE_PATH', dirname(__DIR__, 1));
$swooleVersion = swoole_version();
[$major, $minor, $patch] = explode('.', $swooleVersion);
if($major < 4 || $minor < 6 || $patch < 0) {
    $flags = SWOOLE_HOOK_ALL | SWOOLE_HOOK_CURL;
} else {
    $flags = SWOOLE_HOOK_ALL | SWOOLE_HOOK_NATIVE_CURL;
}
! defined('SWOOLE_HOOK_FLAGS') && define('SWOOLE_HOOK_FLAGS', $flags);

require BASE_PATH . '/vendor/autoload.php';

// Self-called anonymous function that creates its own scope and keep the global namespace clean.
(function () {
    Hyperf\Di\ClassLoader::init();
    /** @var \Psr\Container\ContainerInterface $container */
    $container = require BASE_PATH . '/config/container.php';

    $application = $container->get(\Hyperf\Contract\ApplicationInterface::class);
    $application->run();
})();
