## 项目概述

* 产品名称：优药微信平台 Api 接口
* 项目代号：wxapi
* 项目地址：http://yy-git.youyao99.com/youyao/wxapi

## 运行环境要求

- PHP >= 7.2
- Swoole PHP 扩展 >= 4.5，并关闭了 Short Name
- OpenSSL PHP 扩展
- JSON PHP 扩展
- PDO PHP 扩展
- Redis PHP 扩展
- Protobuf PHP 扩展

## 开发环境部署/安装

推荐使用 [Docker](https://hyperf.wiki/2.0/#/zh-cn/quick-start/install?id=docker-%e4%b8%8b%e5%bc%80%e5%8f%91) 安装

### 基础安装

#### 1. 克隆源代码

克隆 `http://yy-git.youyao99.com/youyao/wxapi.git` 源代码到本地：

    > git clone http://yy-git.youyao99.com/youyao/wxapi.git

#### 2. 安装依赖包

```
cd wxapi
composer install -vvv
```

#### 3. 修改 `.env` 配置

```dotenv
DB_DRIVER=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=hyperf
DB_USERNAME=root
DB_PASSWORD=
DB_CHARSET=utf8mb4
DB_COLLATION=utf8mb4_unicode_ci
DB_PREFIX=

REDIS_HOST=localhost
REDIS_AUTH=(null)
REDIS_PORT=6379
REDIS_DB=0
```

#### 4. 启动服务
```
php bin/hyperf.php start
```
