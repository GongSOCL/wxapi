<?php
declare(strict_types=1);

namespace App\Request\User;


use Hyperf\Validation\Request\FormRequest;

class GetPharmacyDoctorQrcodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'doctor_id' => 'required|integer|gt:0',
        ];
    }

    public function messages(): array
    {
        return [
            'doctor_id.*'=>'doctor_id数据格式不对'
        ];
    }
}