<?php

declare(strict_types=1);

namespace App\Request\App\Common;

use Hyperf\Validation\Request\FormRequest;

class SmsVerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'mobile' => 'required|regex:/^1[3456789][0-9]{9}$/'
        ];
    }

    public function messages(): array
    {
        return [
            'mobile.required' => '手机号不能为空',
            'mobile.regex' => '手机号格式不正确'
        ];
    }
}
