<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Request\App\Feedback;

use Youyao\Framework\FormRequest;

class FeedbackAddRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'platform' => 'required|numeric',
            'content' => 'required',
            'picurl' => '',
            'contact_information' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'platform.required' => 'platform是必须的',
            'platform.numeric' => 'platform必须是数字',
            'content.required' => '反馈内容是必须的',
            'contact_information.required' => '；联系方式是必须的'
        ];
    }
}
