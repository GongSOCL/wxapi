<?php

declare(strict_types=1);

namespace App\Request\App\Serve;

use Hyperf\Validation\Request\FormRequest;

class ServeApplyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'hospital_id' => 'required|integer|gt:0',
            'series_id' => 'required|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'hospital_id.required' => '医院id不能为空',
            'hospital_id.integer' => '医院id格式不正确',
            'hospital_id.gt' => '医院id格式不正确,',
            'series_id.required' => '药品id不能为空',
            'series_id.integer' => '药品id格式不正确',
            'series_id.gt' => '药品id格式不正确,'
        ];
    }
}
