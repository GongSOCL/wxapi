<?php

declare(strict_types=1);

namespace App\Request\App\Transaction;

use Hyperf\Validation\Request\FormRequest;

class MsgDailyTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'msg_date' => 'required|string|date_format:Y-m-d',
            'order_date' => 'required|string|date_format:Y-m-d',
            'msg_id' => 'nullable|integer|gte:0'
        ];
    }

    public function messages(): array
    {
        return [
            'msg_date.required' => '日期格式不正确',
            'msg_date.string' => '日期格式不正确',
            'msg_date.date_format' => '日期格式不正确',
            'order_date.required' => '日期格式不正确',
            'order_date.string' => '日期格式不正确',
            'order_date.date_format' => '日期格式不正确',
            'msg_id.integer' => '消息id格式不正确',
            'msg_id.gte' => '消息id格式不正确'
        ];
    }
}
