<?php

declare(strict_types=1);

namespace App\Request\App\Hospital;

use Hyperf\Validation\Request\FormRequest;

class AddServeDepartsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'hospital_id' => 'required|integer|gt:0',
            'depart_ids' => 'required|array',
            'depart_ids.*' => 'integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'hospital_id.required' => '医院不能为空',
            'hospital_id.integer|hospital_id.gt' => '医院参数格式不正确',
            'depart_ids.required' => '科室不能为空',
            'depart_ids.array' => '科室格式不正确',
            'depart_ids.*.integer' => '科室格式不正确',
            'depart_ids.*.gt' => '科室格式不正确'
        ];
    }
}
