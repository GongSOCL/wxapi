<?php

declare(strict_types=1);

namespace App\Request\App\Hospital;

use Hyperf\Validation\Request\FormRequest;

class HospitalInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'series_id' => 'nullable|integer|gte:0'
        ];
    }

    public function messages(): array
    {
        return [
            'series_id.integer' => '系列药品参数格式不正确',
            'series_id.gte' => '系列药品参数格式不正确'
        ];
    }
}
