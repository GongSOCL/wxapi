<?php

declare(strict_types=1);

namespace App\Request\App\Product;

use Hyperf\Validation\Request\FormRequest;

class AddCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'resource_id' => 'required|integer|gt:0',
            'parent_comment_id' => 'nullable|integer|gte:0',
            'comment' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'resource_id.required' => '资源不能为空',
            'resource_id.integer' => '资源格式不正确',
            'resource_id.gt' => '资源格式不正确',
            'parent_comment_id.integer' => '父级评论格式不正确',
            'parent_comment_id.e' => '父级评论格式不正确',
            'comment.required' => '评论内容不能为空',
            'comment.string' => '评论内容格式不正确',
        ];
    }
}
