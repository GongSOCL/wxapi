<?php

declare(strict_types=1);

namespace App\Request\App\Contract;

use Hyperf\Validation\Request\FormRequest;

class GetContractTplRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'request_id' => 'required|string',
            'contract_id' => 'required|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'request_id.required' => '请求非法',
            'request_id.string' => '请求非法',
            'contract_id.required' => '合同id不能为空',
            'contract_id.integer' => '格式id格式不正确',
            'contract_id.gt' => '格式id格式不正确'
        ];
    }
}
