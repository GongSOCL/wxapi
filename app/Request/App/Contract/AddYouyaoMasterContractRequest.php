<?php

declare(strict_types=1);

namespace App\Request\App\Contract;

use Hyperf\Validation\Request\FormRequest;

class AddYouyaoMasterContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'group_id' => 'required|integer|gt:0',
            'title' => 'required|string|max:64'
        ];
    }
}
