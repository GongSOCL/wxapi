<?php

declare(strict_types=1);

namespace App\Request\App\Contract;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class ContractCallbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'id' => 'required|integer|gt:0',
            'link' => 'nullable|string',
            'is_success' => [
                'required',
                'integer',
                Rule::in([0, 1])
            ]
        ];
    }
}
