<?php

declare(strict_types=1);

namespace App\Request\App\Contract;

use Hyperf\Validation\Request\FormRequest;

class MailContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'mail' => 'required|email'
        ];
    }

    public function messages(): array
    {
        return [
            'mail.required' => '邮箱不能为空',
            'mail.email' => '邮箱格式不正确'
        ];
    }
}
