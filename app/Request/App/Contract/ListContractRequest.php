<?php

declare(strict_types=1);

namespace App\Request\App\Contract;

use Hyperf\Validation\Request\FormRequest;

class ListContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'status' => 'required|integer|gte:0',
            'current' => 'nullable|integer|gt:0',
            'limit' => 'nullable|integer|gt:0',
            'msg_id' => 'nullable|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'status.required' => '搜索状态不正确',
            'status.integer' => '搜索状态格式不正确',
            'status.gte' => '搜索状态格式不正确',
            'current.integer' => '当前页格式不正确',
            'limit.integer' => '每页返回条数格式不正确',
            'current.gt' => '当前页格式不正确',
            'limit.gt' => '每页返回条数格式不正确',
            'msg_id.integer' => '消息id格式不正确',
            'msg_id.gt' => '消息id格式不正确'
        ];
    }
}
