<?php

declare(strict_types=1);

namespace App\Request\App\Contract;

use Hyperf\Validation\Request\FormRequest;

class SignContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'sign_img' => 'required|string',
            'request_id' => 'required|string',
            'code' => 'required|regex:/\d{6}/'
        ];
    }

    public function messages(): array
    {
        return [
          'sign_img.required' => '签名不能为空',
          'sign_img.string' => '签名格式不正确',
          'request_id.required' => '请求非法',
          'request_id.string' => '请求非法',
            'code.required' => '验证码不能为空',
            'code.regex' => '验证码格式不正确'
        ];
    }
}
