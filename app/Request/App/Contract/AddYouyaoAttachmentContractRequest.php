<?php

declare(strict_types=1);

namespace App\Request\App\Contract;

use Hyperf\Validation\Request\FormRequest;

class AddYouyaoAttachmentContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'group_id' => 'required|integer|gt:0',
            'title' => 'required|string|max:64',
            'fee_unit' => 'required|numeric',
            'serves' => 'required|array',
            'serves.*.serve_id' => 'required|integer|gt:0',
            'serves.*.carryon_num' => 'required|integer|gte:0',
            'serves.*.target' => 'required|array',
            'serves.*.target.*.month' => 'required|regex:/^\d{4}-\d{2}$/',
            'serves.*.target.*.num' => 'required|integer|gt:0'
        ];
    }
}
