<?php

declare(strict_types=1);

namespace App\Request\App\Questionnaire;

use Hyperf\Validation\Request\FormRequest;

class InviteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'doctor_id' => 'required|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'doctor_id.required' => '医生id不能为空',
            'doctor_id.integer' => '医生id格式不正确',
            'doctor_id.gt' => '医生id格式不正确'
        ];
    }
}
