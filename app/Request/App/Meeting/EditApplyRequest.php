<?php

declare(strict_types=1);

namespace App\Request\App\Meeting;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;
use phpDocumentor\Reflection\Types\Nullable;

class EditApplyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'manager_id' => 'required|integer|gt:0',
            'compere_id' => 'required|integer|gt:0',
            'zone_id' => 'required|integer|gt:0',
            'province_id' => 'required|integer|gt:0',
            'hospital_id' => 'required|integer|gt:0',
            'depart_id' => 'required|integer|gt:0',
            'chairman' => 'nullable|array',
            'chairman.name' => 'string|max:64',
            'chairman.hospital_id' => 'integer|gt:0',
            'chairman.depart_id' => 'integer|gt:0',
            'chairman.level_id' => 'integer|gt:0',
            'speaker' => 'required|array',
            'speaker.*.id' => 'required|integer|gte:0',
            'speaker.*.name' => 'required|string|max:64',
            'speaker.*.hospital_id' => 'required|integer|gt:0',
            'speaker.*.depart_id' => 'required|integer|gt:0',
            'speaker.*.level_id' => 'required|integer|gt:0',
            'speaker.*.project' => 'required|string|max:64',
            'range' => [
                'required',
                'integer',
                Rule::in([1, 2])
            ]
        ];
    }

    public function messages(): array
    {
        return [
            'manager_id.required' => '会议管理员不能为空',
            'manager_id.integer' => '会议管理员格式不正确',
            'manager_id.gt' => '会议管理员格式不正确',
            'compere_id.required' => '会议主持人不能为空',
            'compere_id.integer' => '会议主持人格式不正确',
            'compere_id.gt' => '会议主持人格式不正确',
            'zone_id.required' => '会议所属大区不能为空',
            'zone_id.integer' => '会议所属大区格式不正确',
            'zone_id.gt' => '会议所属大区格式不正确',
            'province_id.required' => '会议所属省份不能为空',
            'province_id.integer' => '会议所属省份格式不正确',
            'province_id.gt' => '会议所属省份格式不正确',
            'hospital_id.required' => '会议所属医院不能为空',
            'hospital_id.integer' => '会议所属医院格式不正确',
            'hospital_id.gt' => '会议所属医院格式不正确',
            'depart_id.required' => '会议所属科室不能为空',
            'depart_id.integer' => '会议所属科室格式不正确',
            'depart_id.gt' => '会议所属科室格式不正确',
            'chairman.array' => '主席讲者格式不正确',
            'chairman.name.string' => '主席讲者姓名不能为空',
            'chairman.name.max' => '主席讲者最多允许63个字',
            'chairman.hospital_id.integer' => '主席讲者所属医院不能为空',
            'chairman.hospital_id.gt' => '主席讲者所属医院格式不正确',
            'chairman.depart_id.integer' => '主席讲者所属科室不能为空',
            'chairman.depart_id.gt' => '主席讲者所属科室格式不正确',
            'chairman.level_id.integer' => '主席讲者级别不能为空',
            'chairman.level_id.gt' => '主席讲者级别格式不正确',
            'speaker.required' => '会议讲者不能为空',
            'speaker.array' => '会议讲者格式不正确',
            'speaker.*.id.required' => '主席讲者ID不能为空',
            'speaker.*.id.integer' => '主席讲者ID格式不正确',
            'speaker.*.id.gte' => '主席讲者ID格式不正确',
            'speaker.*.name.required' => '主席讲者姓名不能为空',
            'speaker.*.name.string' => '主席讲者姓名格式不正确',
            'speaker.*.name.max' => '主席讲者最多允许63个字',
            'speaker.*.hospital_id.required' => '主席讲者所属医院不能为空',
            'speaker.*.hospital_id.integer' => '主席讲者所属医院格式不正确',
            'speaker.*.hospital_id.gt' => '主席讲者所属医院格式不正确',
            'speaker.*.depart_id.required' => '主席讲者所属科室不能为空',
            'speaker.*.depart_id.integer' => '主席讲者所属科室格式不正确',
            'speaker.*.depart_id.gt' => '主席讲者所属科室格式不正确',
            'speaker.*.level_id.required' => '主席讲者级别不能为空',
            'speaker.*.level_id.integer' => '主席讲者级别格式不正确',
            'speaker.*.level_id.gt' => '主席讲者级别格式不正确',
            'range.required' => '会议区域不能为空',
            'range.integer' => '会议区域格式不正确',
            'range.in' => '会议区域格式不正确',
            'speaker.*.project.required' => '讲者课题不能为空',
            'speaker.*.project.string' => '讲者课题格式不正确',
            'speaker.*.project.max' => '讲者课题最多允许64个字'
        ];
    }
}
