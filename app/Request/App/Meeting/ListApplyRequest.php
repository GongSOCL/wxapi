<?php

declare(strict_types=1);

namespace App\Request\App\Meeting;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class ListApplyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'zone_id' => 'nullable|integer|gte:0',
            'scope' => [
                'nullable',
                'integer',
                Rule::in([1, 2])
            ],
            'status' => [
                'nullable',
                'integer',
                Rule::in([0, 1, 2, 3])
            ],
            'current' => 'nullable|integer|gte:0',
            'limit' => 'nullable|integer|gte:0'
        ];
    }

    public function messages(): array
    {
        return [
            'zone_id.integer' => '大区id格式不正确',
            'zone_id.gte' => '大区id格式不正确',
            'scope.integer' => '搜索范围不存在格式不正确',
            'scope.in' => '搜索范围不存在格式不正确',
            'status.integer' => '申请状态格式不正确',
            'status.in' => '申请状态格式不存在格式不正确',
            'current.integer' => '当前页格式不正确',
            'current.gte' => '当前页格式不正确',
            'limit.integer' => '返回条数格式不正确',
            'limit.gte' => '返回条数格式不正确',
        ];
    }
}
