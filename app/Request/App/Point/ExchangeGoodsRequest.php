<?php

declare(strict_types=1);

namespace App\Request\App\Point;

use Hyperf\Validation\Request\FormRequest;

class ExchangeGoodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'goods_yyid' => 'required|string',
            'num' => 'required|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'goods_yyid.required' => '商品不存在',
            'goods_yyid.string' => '商品格式不正确',
            'num.required' => '兑换数量不能为空',
            'num.integer' => '兑换数量格式不正确',
            'num.gt' => '兑换数量格式不正确'
        ];
    }
}
