<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Request\App\Login;

use Youyao\Framework\FormRequest;

class LoginForCheckRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'phone' => 'required',
            'password' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'phone.required' => '账号不能为空',
            'password.required' => '密码不能为空',
        ];
    }
}
