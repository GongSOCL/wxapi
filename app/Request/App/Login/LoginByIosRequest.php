<?php

declare(strict_types=1);

namespace App\Request\App\Login;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class LoginByIosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'iosid' => 'required',
            'familyName' => '',
        ];
    }

    public function messages(): array
    {
        return [
            'iosid.required' => 'iosid不能为空'
        ];
    }
}
