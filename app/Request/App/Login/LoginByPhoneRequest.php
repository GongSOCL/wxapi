<?php

declare(strict_types=1);

namespace App\Request\App\Login;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class LoginByPhoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'mobile' => 'required|regex:/^1[3456789][0-9]{9}$/',
            'code' => 'required|regex:/\d{6}/'
        ];
    }

    public function messages(): array
    {
        return [
            'mobile.required' => '手机号不能为空',
            'mobile.regex' => '手机号格式不正确',
            'code.required' => '验证码不能为空',
            'code.regex' => '验证码格式不正确'
        ];
    }
}
