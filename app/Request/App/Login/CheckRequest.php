<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/doctor-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Request\App\Login;

use Youyao\Framework\FormRequest;

class CheckRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'version' => 'required',
            'channel' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'version.required' => 'version不能为空',
            'channel.required' => 'channel不能为空'
        ];
    }
}
