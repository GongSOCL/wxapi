<?php

declare(strict_types=1);

namespace App\Request\App\Login;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class IosBindRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'iosid' => ''
        ];
    }

    public function messages(): array
    {
        return [
        ];
    }
}
