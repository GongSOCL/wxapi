<?php

declare(strict_types=1);

namespace App\Request\App\Visit;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;
use function foo\func;

class FakeHospitalPositionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $hospitalId = $this->query('hospital_id');
        $id = $this->query('id');
        return [
            'hospital_id' => [
                Rule::requiredIf(function () use($id) {
                    return is_null($id) || $id == 0;
                }),
                'integer',
                'gte:0'
            ],
            'id' => [
                Rule::requiredIf(function () use($hospitalId) {
                    return is_null($hospitalId) || $hospitalId == 0;
                })
            ]
        ];
    }

    public function messages(): array
    {
        return [
            'hospital_id.required_if' => '医院id不能为空',
            'hospital_id.integer' => '医院id格式不正确',
            'hospital_id.gte' => '医院id格式不正确',
            'id.required_if' => '拜访id不能为空',
            'id.integer' => '拜访id格式不正确',
            'id.gte' => '拜访id格式不正确'
        ];
    }
}
