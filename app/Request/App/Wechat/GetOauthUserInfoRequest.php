<?php

declare(strict_types=1);

namespace App\Request\App\Wechat;

use Hyperf\Validation\Request\FormRequest;

class GetOauthUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'code' => 'required|string',
            'state' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'code.required' => '授权码不能为空',
            'code.string' => '授权码格式不正确',
            'state.required' => '授权参数不能为空'
        ];
    }
}
