<?php

declare(strict_types=1);

namespace App\Request\App\Wechat;

use Hyperf\Validation\Request\FormRequest;

class GetJsSignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'url' => 'required|url',
            'apis' => 'array|nullable',
            'apis.*' => 'string'
        ];
    }

    public function messages(): array
    {
        return [
            'url.required' => '微信授权回跳地址不能为空',
            'url.url' => '微信授权回跳地址不正确',
            'apis.array' => '微信功能格式不正确',
            'apis.*.string' => '微信功能格式不正确'
        ];
    }
}
