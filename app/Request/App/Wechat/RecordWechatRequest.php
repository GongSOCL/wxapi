<?php

declare(strict_types=1);

namespace App\Request\App\Wechat;

use Hyperf\Validation\Request\FormRequest;

class RecordWechatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'openid' => 'required|string',
            'nickname' => 'required|string',
            'sex' => 'nullable|integer|gte:0',
            'province' => 'nullable|string',
            'city' => 'nullable|string',
            'country' => 'string|nullable',
//            'headimgurl' => 'required|url',
            'headimgurl' => 'url',
            'unionid' => 'required|string|nullable'
        ];
    }

    public function messages(): array
    {
        return [
            'openid.required' => '微信授权信息不能为空',
            'openid.string' => '微信授权信息格式不正确',
            'nickname.required' => '微信呢称不能为空',
            'nickname.string' => '微信呢称格式不正确',
            'sex.integer' => '性别格式不正确',
            'sex.gte' => '性别格式不正确',
            'province.string' => '省份格式不正确',
            'city.string' => '城市格式不正确',
            'country.string' => '国家格式不正确',
//            'headimgurl.required' => '微信头像不能为空',
            'headimgurl.url' => '微信头像地址格式不正确',
            'unionid.string' => '微信授权信息格式不正确',
            'unionid.required' => '微信授权信息不能为空'
        ];
    }
}
