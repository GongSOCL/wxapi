<?php

declare(strict_types=1);

namespace App\Request\App\Visits;

use App\Constants\DataStatus;
use App\Model\Qa\AgentVisit;
use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class CheckOutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $visitType = (int) $this->post('visit_type');
        $isTypeRequired = function () use ($visitType) {
            return in_array($visitType, [
                AgentVisit::VISIT_TYPE_TO_FACE,
                AgentVisit::VISIT_TYPE_ONLINE,
                AgentVisit::VISIT_TYPE_EXTERNAL_MEETING
            ]);
        };
        $isNextRequired = function () use ($visitType) {
            return in_array($visitType, [
                AgentVisit::VISIT_TYPE_TO_FACE,
                AgentVisit::VISIT_TYPE_ONLINE
            ]);
        };
        return [
            'hospital_id' => 'required|integer|gte:0',
            'visit_type' => [
                'required',
                'integer',
                Rule::in([1,2,3,4,5]),
            ],
            'series_ids' => [
                Rule::requiredIf($isTypeRequired),
                'array'
            ],
            'series_ids.*' => 'integer|gt:0',
            'doctor_ids' => 'nullable|array',
            'doctor_ids.*' => 'integer|gt:0',
            'result' => [
                'string',
                Rule::requiredIf($isTypeRequired)
            ],
            'next' => [
                'string',
                Rule::requiredIf($isNextRequired)
            ],
            'lng' => [
                'required',
                'regex:/(?:[0-9]|[1-9][0-9]|1[0-7][0-9]|180)\.([0-9]+)/'
            ],
            'lat' => [
                'required',
                'regex:/(?:[0-9]|[1-8][0-9]|90)\.([0-9]+)/'
            ],
            'coordinate_type' => [
                'nullable',
                'string',
                Rule::in([DataStatus::COORDINATE_GPS, DataStatus::COORDINATE_GCJ02])
            ],
            'position' => 'required|string|max:128',
            'pics' => 'required|array',
            'pics.*' => 'integer|gt:0',
            'geo_opt' => 'integer|gte:0|nullable',
            'geo_comment' => 'string|nullable|max:64',
            'is_collaborative' => [
                'nullable',
                'integer',
                Rule::in([0, 1, 2]),
                "required_if:visit_type,1"
            ],
            'check_out_time' => 'nullable|date',
            'is_fake_geo' => [
                'nullable',
                'integer',
                Rule::in([0, 1])
            ]
        ];
    }

    public function messages(): array
    {
        return [
            'date.required' => '拜访日期不能为空',
            'date.string' => '拜访日期格式不正确',
            'hospital_id.required' => '拜访医院不能为空',
            'hospital_id.integer' => '拜访医院格式不正确',
            'hospital_id.gt' => '拜访医院格式不正确',
            'doctor_ids.array' => '医生格式不正确',
            'doctor_ids.*.integer' => '医生格式不正确',
            'doctor_ids.*.gt' => '医生格式不正确',
            'series_ids.required_if' => '请至少选择一种药品',
            'series_ids.array' => '药品格式不正确',
            'series_ids.*.integer' => '药品格式不正确',
            'series_ids.*.gt' => '药品格式不正确',
            'visit_type.required' => '拜访类型不能为空',
            'visit_type.integer' => '拜访类型不存在',
            'visit_type.in' => '拜访类型不存在',
            'main_purpose.required' => '主要事宜不能为空',
            'main_purpose.string' => '主要事宜格式不正确',
            'result.string' => '拜访结果格式不正确',
            'result.required_if' => '拜访结果不能为空',
            'next.string' => '后续跟进事项格式不正确',
            'next.required_if' => '后续计划不能为空',
            'comment.string' => '备注格式不正确',
            'lng.required_if' => '当面拜访经度不能为空',
            'lng.regex' => '当面拜访经度格式不正确',
            'lat.required_if' => '当面拜访纬度不能为空',
            'lat.regex' => '当面拜访纬度格式不正确',
            'position.required_if' => '当面拜访地理位置不能为空',
            'pics.array' => '照片列表格式不正确',
            'pics.*.integer' => '上传图片参数格式不正确',
            'pics.*.gt' => '上传图片参数格式不正确',
            'geo_comment.max' => '超出范围原因超出长度限制，最长允许64个字',
            'coordinate_type.string' => '坐标体系格式不正确',
            'coordinate_type.in' => '暂不支持该坐标系',
            'position.max' => '定位位置超长，最大允许128个字',
            'is_collaborative.integer' => '是否协访参数格式不正确',
            'is_collaborative.in' => '是否协访参数格式不正确',
            'is_collaborative.required_in' => '面对面拜访是否协访必选',
            'check_out_time.date' => '签出时间格式不正确'
        ];
    }
}
