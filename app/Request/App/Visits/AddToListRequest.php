<?php

declare(strict_types=1);

namespace App\Request\App\Visits;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class AddToListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'doctor_ids' => 'required'
        ];
    }

    public function messages(): array
    {
        return [];
    }
}
