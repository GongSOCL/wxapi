<?php

declare(strict_types=1);

namespace App\Request\App\Visits;

use Hyperf\Validation\Request\FormRequest;

class AgentVisitListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'check_in_start_at' => 'nullable|string',
            'check_in_end_at' => 'nullable|string',
            'check_out_start_at' => 'nullable|string',
            'check_out_end_at' => 'nullable|string',
            'hospital_id' => 'nullable|integer|gte:0',
            'office_id' => 'nullable|array',
            'office_id.*' => 'integer|gt:0',
            'current' => 'nullable|integer|gt:0',
            'limit' => 'nullable|integer|gt:0',
            'check_type' => 'integer|gte:0|nullable',
            'visit_type' => 'integer|gte:0|nullable',
            'create_start_at' => 'nullable|string',
            'create_end_at' => 'nullable|string',
        ];
    }

    public function messages(): array
    {
        return [
            'check_in_start_at.string' => '签入开始时间格式不正确',
            'check_in_end_at.string' => '签入结束时间格式不正确',
            'check_out_start_at.string' => '签出开始时间格式不正确',
            'check_out_end_at.string' => '签出结束时间格式不正确',
            'hospital_id.integer' => '医院搜索条件不正确',
            'hospital_id.gte' => '医院搜索条件不正确',
            'office_id.array' => '科室搜索条件不正确',
            'office_id.*.integer' => '科室搜索条件不正确',
            'office_id.*.gt' => '科室搜索条件不正确',
            'current.integer|current.gt' => '当前页格式不正确',
            'limit.integer|limit.gt' => '每页返回条数格式不正确',
            'check_type.integer|check_type.gte' => '搜索类型格式不正确',
            'visit_type.integer|visit_type.gte' => '拜访类型格式不正确'
        ];
    }
}
