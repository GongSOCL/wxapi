<?php

declare(strict_types=1);

namespace App\Request\App\Exam;

use Hyperf\Validation\Request\FormRequest;

class FinishExamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'request_id' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'request_id.required' => '请求非法',
            'request_id.string' => '请求非法'
        ];
    }
}
