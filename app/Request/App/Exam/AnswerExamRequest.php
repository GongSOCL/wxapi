<?php

declare(strict_types=1);

namespace App\Request\App\Exam;

use Hyperf\Validation\Request\FormRequest;

class AnswerExamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'question_id' => 'required|integer|gt:0',
            'request_id' => 'required|string',
            'option_ids' => 'nullable|array',
            'option_ids.*' => 'integer|gt:0',
            'content' => 'nullable|string'
        ];
    }

    public function messages(): array
    {
        return [
            'question_id.required' => '试题id不能为空',
            'question_id.integer' => '试题不存在或格式不正确',
            'question_id.gt' => '试题不存在或格式不正确',
            'option_ids|array' => '选项id格式不正确',
            'option_ids.*.integer' => '选项id格式不正确',
            'option_ids.*.gt' => '选项id格式不正确',
            'content.string' => '填空内容格式不正确'
        ];
    }
}
