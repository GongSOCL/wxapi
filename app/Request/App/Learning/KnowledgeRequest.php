<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Learning;

use App\Request\FormRequest;

class KnowledgeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'type' => 'required|numeric'
        ];
    }

    public function messages(): array
    {
        return [
            'type.required' => 'type是必须的',
            'type.numeric' => 'type含有非法字符'
        ];
    }
}
