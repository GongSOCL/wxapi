<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Learning;

use App\Request\FormRequest;

class LearningRecordRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'type' => 'required|numeric',
            'plan_id' => 'required|numeric',
            'mid' => 'required|numeric',
            'learn_time' => 'required|numeric',
        ];
    }

    public function messages(): array
    {
        return [
            'plan_id.required' => '计划id是必须的',
            'mid.required' => '资料id是必须的',
            'learn_time.required' => '阅读时长是必须的',
            'type.numeric' => 'type含有非法字符'
        ];
    }
}
