<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\MorningShare;

use App\Request\FormRequest;

class RankRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'rank_type' => 'numeric',
            'limit' => 'numeric'
        ];
    }

    public function messages(): array
    {
        return [];
    }
}
