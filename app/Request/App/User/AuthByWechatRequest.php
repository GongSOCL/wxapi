<?php

declare(strict_types=1);

namespace App\Request\App\User;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class AuthByWechatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'openid' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'openid.required' => '微信信息不能为空',
            'openid.string' => '微信信息格式不正确'
        ];
    }
}
