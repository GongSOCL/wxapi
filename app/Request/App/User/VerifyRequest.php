<?php

declare(strict_types=1);

namespace App\Request\App\User;

use Hyperf\Validation\Request\FormRequest;

class VerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:32',
            'hospital_ids' => 'nullable|array',
            'hospital_ids.*' => 'integer|gt:0',
            'province_id' => 'required|integer|gt:0',
            'company_id' => 'required|integer|gt:0',
            'working_age' => 'required|integer|gt:0',
            'department_ids' => 'required|array',
            'department_ids.*' => 'integer|gt:0',
            'serve_company' => 'nullable|array',
            'serve_company.*' => 'nullable|array',
            'serve_company.*.company_id' => 'required|integer|gt:0',
            'serve_company.*.field_id' => 'required|array',
            'serve_company.*.field_id.*' => 'required|integer|gt:0',
            'serve_company.*.adaption_id' => 'required|array',
            'serve_company.*.adaption_id.*' => 'required|integer|gt:0',
            'serve_company.*.product_name' => 'required|string',
            'direction' => 'nullable|integer|gt:0',
            'submitted_role_type' => 'integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => '真实姓名不能为空',
            'name.string' => '真实姓名格式不正确',
            'hospital_ids.array' => '医院参数格式错误',
            'hospital_ids.*.integer' => '医院参数格式错误',
            'hospital_ids.*.gt' => '医院参数格式错误',
            'province_id.required' => '可服务城市不能为空',
            'province_id.gt' => '可服务城市参数格式错误',
            'province_id.integer' => '可服务城市参数格式错误',
            'company_id.integer' => '服务公司参数格式错误',
            'company_id.gt' => '服务公司参数格式错误',
            'company_id.required' => '服务公司不能为空',
            'working_age.integer' => '服务年限参数格式错误',
            'working_age.gt' => '服务年限参数格式错误',
            'working_age.required' => '服务年限不能为空',
            'department_ids.array' => '熟悉科室参数格式错误',
            'department_ids.*.integer' => '熟悉科室参数格式错误',
            'department_ids.*.gt' => '熟悉科室参数格式错误',
            'serve_company.array' => '熟悉科室参数格式错误',
            'serve_company.*.array' => '熟悉科室参数格式错误',
            'serve_company.*.company_id.required' => '公司名称不能为空',
            'serve_company.*.company_id.integer' => '公司名称参数格式错误',
            'serve_company.*.company_id.gt' => '公司名称参数格式错误',
            'serve_company.*.field_id.required' => '治疗领域不能为空',
            'serve_company.*.field_id.array' => '治疗领域格式错误',
            'serve_company.*.field_id.*.required' => '治疗领域不能为空',
            'serve_company.*.field_id.*.integer' => '治疗领域参数格式错误',
            'serve_company.*.field_id.*.gt' => '治疗领域参数格式错误',
            'serve_company.*.adaption_id.required' => '适应症不能为空',
            'serve_company.*.adaption_id.array' => '适应症格式错误',
            'serve_company.*.adaption_id.*.required' => '适应症不能为空',
            'serve_company.*.adaption_id.*.integer' => '适应症参数格式错误',
            'serve_company.*.adaption_id.*.gt' => '适应症参数格式错误',
            'serve_company.*.product_name.required' => '产品名称不能为空',
            'serve_company.*.product_name.string' => '产品名称参数格式错误',
            'direction.integer' => '服务方向参数格式错误',
            'direction.gt' => '服务方向参数格式错误',
            'submitted_role_type.gt' => '用户类型参数格式错误',
        ];
    }
}
