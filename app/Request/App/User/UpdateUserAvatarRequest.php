<?php

declare(strict_types=1);

namespace App\Request\App\User;

use Hyperf\Validation\Request\FormRequest;

class UpdateUserAvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
                'avatar_id' => 'required|integer|gt:0'
        ];
    }

    public function messages(): array
    {
        return [
            'avatar_id.required' => '头像上传id不能为空',
            'avatar_id.integer' => '上传头像格式不正确',
            'avatar_id.gt' => '上传头像格式不正确'
        ];
    }
}
