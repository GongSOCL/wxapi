<?php

declare(strict_types=1);

namespace App\Request\App\User;

use Hyperf\Validation\Request\FormRequest;

class UpdateUserNameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:80'
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => '呢称不能为空',
            'name.max' => '呢称超长，最多允许80个字',
        ];
    }
}
