<?php

declare(strict_types=1);

namespace App\Request\App\User;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class HelpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'content' => 'required|string',
            'type' => [
                'required',
                'integer',
                Rule::in([1, 2, 3, 4])
            ],
            'email' => 'nullable|string'
        ];
    }

    public function messages(): array
    {
        return [
            'content.required' => '求助内容不能为空',
            'content.string' => '求助内容格式不正确',
            'type.required' => '求助类型不能为空',
            'type.integer' => '求助类型格式不正确',
            'type.in' => '求助类型不存在',
            'email.string' => '邮箱格式不正确'
        ];
    }
}
