<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Group\Work;

use App\Request\FormRequest;

class PresetUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'nid' => '',
        ];
    }

    public function messages(): array
    {
        return [];
    }
}
