<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Group\Work;

use App\Request\FormRequest;

class MemberUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'nid' => 'numeric',
            'true_name' => "required",
            'gender' => '',
            'mobile_num' => 'nullable|regex:/^1[3456789][0-9]{9}$/',
            'hospital_id' => 'required',
            'depart_id' => 'required',
            'position' => '',
            'job_title' => '',
            'field_id' => '',
            'kol_level' => 'required',
            'clinic_rota' => ''
        ];
    }

    public function messages(): array
    {
        return [
            'true_name.required' => '姓名不能为空',
//            'true_name.regex' => '姓名只能是2-6个中文',
        ];
    }
}
