<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Group\Work;

use App\Request\FormRequest;

class DelMemberRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'group_id' => 'required|numeric',
            'member_ids' => 'required|numeric'
        ];
    }

    public function messages(): array
    {
        return [
            'group_id.required' => 'group_id是必须的',
            'group_id.numeric' => 'group_id含有非法字符',
            'member_ids.required' => 'member_ids是必须的',
            'member_ids.numeric' => 'member_ids含有非法字符',
        ];
    }
}
