<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Group\Work;

use App\Request\FormRequest;

class StaticMemberRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'depart_id' => 'required|numeric',
            'page' => '',
            'page_size' => ''
        ];
    }

    public function messages(): array
    {
        return [
            'depart_id.required' => 'depart_id是必须的',
            'depart_id.numeric' => 'depart_id含有非法字符'
        ];
    }
}
