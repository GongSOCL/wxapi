<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Group\Work;

use App\Request\FormRequest;

class VoteCancelRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'vote_id' => 'required|numeric'
        ];
    }

    public function messages(): array
    {
        return [
            'vote_id.required' => 'vote_id是必须的',
            'vote_id.numeric' => 'vote_id含有非法字符'
        ];
    }
}
