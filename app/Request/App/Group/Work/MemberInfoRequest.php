<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Group\Work;

use App\Request\FormRequest;

class MemberInfoRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'n_id' => 'numeric'
        ];
    }

    public function messages(): array
    {
        return [
            'n_id.numeric' => 'n_id含有非法字符'
        ];
    }
}
