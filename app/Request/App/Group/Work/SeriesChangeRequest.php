<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Group\Work;

use App\Request\FormRequest;

class SeriesChangeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'series_id' => 'required|numeric',
            'group_id' => 'required|numeric'
        ];
    }

    public function messages(): array
    {
        return [
            'series_id.required' => 'series_id是必须的',
            'series_id.numeric' => 'series_id含有非法字符',
            'group_id.required' => 'group_id是必须的',
            'group_id.numeric' => 'group_id含有非法字符',
        ];
    }
}
