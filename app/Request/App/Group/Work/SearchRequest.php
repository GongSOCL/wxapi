<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Group\Work;

use App\Request\FormRequest;

class SearchRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'key_word' => 'alpha_dash',
            'page' => 'numeric',
            'page_size' => 'numeric'
        ];
    }

    public function messages(): array
    {
        return [
            'key_word.alpha_dash' => 'key_word含有非法字符',
            'page.numeric' => '页码必须是数字',
            'page_size.numeric' => '页面数据必须是数字'
        ];
    }
}
