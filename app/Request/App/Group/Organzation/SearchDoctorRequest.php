<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\App\Group\Organzation;

use App\Request\FormRequest;

class SearchDoctorRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'keyword' => '',
            'page' => 'numeric',
            'page_size' => 'numeric'
        ];
    }

    public function messages(): array
    {
        return [];
    }
}
