<?php

declare(strict_types=1);

namespace App\Request\Fake;

use Hyperf\Validation\Request\FormRequest;

class GetCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'uid' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'uid.required' => '用户不能为空',
            'uid.string' => '用户参数格式不正确'
        ];
    }
}
