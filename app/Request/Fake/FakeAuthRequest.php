<?php

declare(strict_types=1);

namespace App\Request\Fake;

use Hyperf\Validation\Request\FormRequest;

class FakeAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'code' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'code.required' => '登陆令牌不能为空',
            'code.string' => '登陆令牌格式不正确'
        ];
    }
}
