<?php

declare(strict_types=1);

namespace App\Request\Visit;

use Hyperf\Validation\Request\FormRequest;

class AgentVisiDoctorListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'hospital_id' => 'required|integer|gt:0',
            'keywords' => 'nullable|string'
        ];
    }

    public function messages(): array
    {
        return [
            'hospital_id.integer' => '医院搜索条件不正确',
            'hospital_id.gt' => '医院搜索条件不正确',
            'keywords.string' => '关键字搜索条件格式不正确'
        ];
    }
}
