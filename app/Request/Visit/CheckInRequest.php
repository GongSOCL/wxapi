<?php

declare(strict_types=1);

namespace App\Request\Visit;

use App\Model\Qa\AgentVisit;
use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class CheckInRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'visit_type' => [
                'required',
                'integer',
                Rule::in([
                    AgentVisit::VISIT_TYPE_TO_FACE,
                    AgentVisit::VISIT_TYPE_ONLINE,
                    AgentVisit::VISIT_TYPE_INTERNAL_MEETING,
                    AgentVisit::VISIT_TYPE_EXTERNAL_MEETING,
                    AgentVisit::VISIT_TYPE_ADMINISTRATIVE_WORK
                ])
            ],
            'hospital_id' => 'nullable|integer|gte:0',
            'pics' => 'required|string',
            'lng' => [
                'required',
                'regex:/(?:[0-9]|[1-9][0-9]|1[0-7][0-9]|180)\.([0-9]+)/'
            ],
            'lat' => [
                'required',
                'regex:/(?:[0-9]|[1-8][0-9]|90)\.([0-9]+)/'
            ],
            'position' => 'required|string',
            'geo_opt' => 'integer|nullable',
            'geo_comment' => 'string|nullable|max:64',
            'special_id' => 'nullable|integer|gte:0',
            'special_comment' => [
                'string',
                'max:256',
                'required_if:special_id,>,0'
            ]
        ];
    }

    public function messages(): array
    {
        return [
           'pics.required' => '图片尚未上传或未上传完成',
            'lng.required' => '地理位置经度不能为空',
            'lng.regex' => '地理位置经度格式不正确',
            'lat.required' => '地理位置纬度不能为空',
            'lat.regex' => '地理位置纬度格式不正确',
            'geo_comment.max' => '超出范围原因超出长度限制，最长允许64个字',
            'special_id.integer' => '异常原因选项格式不正确',
            'special_id.gte' => '异常原因选项格式不正确',
            'special_comment.string' => '异常原因备注格式不正确',
            'special_comment.max' => '异常原因备注过长，最多允许256个字符',
            'special_comment.required_if' => '异常原因备注不能为空',
        ];
    }
}
