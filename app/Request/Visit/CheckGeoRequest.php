<?php

declare(strict_types=1);

namespace App\Request\Visit;

use Hyperf\Validation\Request\FormRequest;

class CheckGeoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'hospital_id' => 'required|integer|gt:0',
            'lng' => [
                'required',
                'regex:/(?:[0-9]|[1-9][0-9]|1[0-7][0-9]|180)\.([0-9]+)/'
            ],
            'lat' => [
                'required',
                'regex:/(?:[0-9]|[1-8][0-9]|90)\.([0-9]+)/'
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'hospital_id.required' => '医院id不能为空',
            'hospital_id.integer' => '医院参数格式不正确',
            'hospital_id.gt' => '医院参数格式不正确',
            'lng.required' => '经度不能为空',
            'lng.regex' => '经度格式不正确',
            'lat.required_if' => '纬度不能为空',
            'lat.regex' => '纬度格式不正确',
        ];
    }
}
