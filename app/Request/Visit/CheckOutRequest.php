<?php

declare(strict_types=1);

namespace App\Request\Visit;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class CheckOutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'hospital_id' => 'required|integer|gte:0',
            'visit_type' => [
                'required',
                'integer',
                Rule::in([1,2,3,4,5]),
            ],
            'depart_ids' => 'nullable|string',
            'series_ids' => 'nullable|string',
            'doctor_ids' => 'nullable|string',
            'result' => 'string|nullable',
            'next' => 'string|nullable',
            'lng' => [
                'required',
                'regex:/(?:[0-9]|[1-9][0-9]|1[0-7][0-9]|180)\.([0-9]+)/'
            ],
            'lat' => [
                'required',
                'regex:/(?:[0-9]|[1-8][0-9]|90)\.([0-9]+)/'
            ],
            'position' => 'required|string',
            'pics' => 'required|string',
            'geo_opt' => 'integer|gte:0|nullable',
            'geo_comment' => 'string|nullable|max:64'
        ];
    }

    public function messages(): array
    {
        return [
            'date.required' => '拜访日期不能为空',
            'date.string' => '拜访日期格式不正确',
            'hospital_id.required' => '拜访医院不能为空',
            'hospital_id.integer' => '拜访医院格式不正确',
            'hospital_id.gt' => '拜访医院格式不正确',
            'office_id.required' => '拜访科室不能为空',
            'office_id.integer' => '拜访科室格式不正确',
            'doctor_ids.string' => '医生格式不正确',
            'series_ids.required' => '产品不能为空',
            'visit_type.required' => '拜访类型不能为空',
            'visit_type.integer' => '拜访类型不存在',
            'visit_type.in' => '拜访类型不存在',
            'main_purpose.required' => '主要事宜不能为空',
            'main_purpose.string' => '主要事宜格式不正确',
            'result.required' => '拜访结果不能为空',
            'result.string' => '拜访结果格式不正确',
            'next.string' => '后续跟进事项格式不正确',
            'comment.string' => '备注格式不正确',
            'lng.required_if' => '当面拜访经度不能为空',
            'lng.regex' => '当面拜访经度格式不正确',
            'lat.required_if' => '当面拜访纬度不能为空',
            'lat.regex' => '当面拜访纬度格式不正确',
            'position.required_if' => '当面拜访地理位置不能为空',
            'pics.required_if' => '当面拜访拍照不能为空',
            'geo_comment.max' => '超出范围原因超出长度限制，最长允许64个字'
        ];
    }
}
