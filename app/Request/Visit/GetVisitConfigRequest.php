<?php

declare(strict_types=1);

namespace App\Request\Visit;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class GetVisitConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                'integer',
                Rule::in([1, 2, 3])
            ]
        ];
    }

    public function messages(): array
    {
        return [
            'type.required' => '类型不能为空',
            'type.integer' => '类型参数格式不正确',
            'type.in' => '类型参数格式不正确'
        ];
    }
}
