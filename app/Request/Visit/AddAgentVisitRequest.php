<?php

declare(strict_types=1);

namespace App\Request\Visit;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class AddAgentVisitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'date' => 'required|string',
            'hospital_id' => 'required|integer|gt:0',
            'office_id' => 'required|integer|gt:0',
            'series_ids' => 'required|string',
//            'series_ids.*' => 'integer|gt:0',
            'visit_type' => [
                'required',
                'integer',
                Rule::in([1,2,3,4]),
            ],
            'main_purpose' => 'required|string',
            'result' => 'required|string',
            'next' => 'string|nullable',
            'comment' => 'string|nullable',
            'lng' => [
                'required_if:visit_type,1',
                'regex:/(?:[0-9]|[1-9][0-9]|1[0-7][0-9]|180)\.([0-9]{5,6})/'
            ],
            'lat' => [
                'required_if:visit_type,1',
                'regex:/(?:[0-9]|[1-8][0-9]|90)\.([0-9]{5,6})/'
            ],
            'position' => 'required_if:visit_type,1|string',
            'pics' => 'required_if:visit_type,1|string',
//            'pics.*' => 'required_if:visit_type,1|integer'
        ];
    }

    public function messages(): array
    {
        return [
            'date.required' => '拜访日期不能为空',
            'date.string' => '拜访日期格式不正确',
            'hospital_id.required' => '拜访医院不能为空',
            'hospital_id.integer|hospital_id.gt' => '拜访医院格式不正确',
            'office_id.required' => '拜访科室不能为空',
            'office_id.integer|hospital_id.gt' => '拜访科室格式不正确',
            'series_ids.required' => '产品不能为空',
            'series_ids.array' => '产品格式不正确',
//            'series_ids.*.integer' => '产品格式不正确',
//            'series_ids.*.gt' => '产品格式不正确',
            'visit_type.required' => '拜访类型不能为空',
            'visit_type.integer|visit_type.in' => '拜访类型不存在',
            'main_purpose.required' => '主要事宜不能为空',
            'main_purpose.string' => '主要事宜格式不正确',
            'result.required' => '拜访结果不能为空',
            'result.string' => '拜访结果格式不正确',
            'next.string' => '后续跟进事项格式不正确',
            'comment.string' => '备注格式不正确',
            'lng.required_if' => '当面拜访经度不能为空',
            'lng.regex' => '当面拜访经度格式不正确',
            'lat.required_if' => '当面拜访纬度不能为空',
            'lat.regex' => '当面拜访纬度格式不正确',
            'position.required_if' => '当面拜访地理位置不能为空',
            'pics.required_if' => '当面拜访拍照不能为空',
//            'pics.array' => '当面拜访拍照格式不正确',
//            'pics.*.integer' => '拍照图片参数格式不正确',
//            'pics.*.required_if' => '当面拜访至少需要上传一张图片'
        ];
    }
}
