<?php

declare(strict_types=1);

namespace App\Request\Visit;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class AgentVisiDoctorAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'check' => 'required',
            'name' => 'required|string',
            'mobile' => 'nullable|regex:/^1[3456789][0-9]{9}$/',
            'hospital_id' => 'required|integer|gt:0',
            'depart_id' => 'required|integer|gt:0',
            'sex' => [
                'nullable',
                'integer',
                Rule::in([1, 2])
            ],
            'position' => 'nullable|string',
            'job_title' => 'nullable|string',
            'field_id' => 'nullable|integer|gte:0',
            'clinic_rota' => 'nullable|string'
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => '姓名不能为空',
            'name.string' => '姓名格式不正确',
            'mobile.required' => '手机号不能为空',
            'mobile.regex' => '手机号格式不正确',
            'hospital_id.integer' => '医院搜索条件不正确',
            'hospital_id.gt' => '医院搜索条件不正确',
            'depart_id.integer' => '科室搜索条件不正确',
            'depart_id.gt' => '科室搜索条件不正确',
            'sex.integer|sex.in' => '性别格式不正确',
            'position.string' => '级别格式不正确',
            'job_title.string' => '职称格式不正确',
            'field_id.integer' => '擅长领域格式不正确',
            'field_id.gte' => '擅长领域格式不正确',
            'clinic_rota.string' => '门诊时间格式不正确'
        ];
    }
}
