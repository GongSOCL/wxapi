<?php

declare(strict_types=1);

namespace App\Request\Doctor;

use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class EditDoctorInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'sex' => [
                'nullable',
                'integer',
                Rule::in([0, 1, 2])
            ],
            'mobile' => 'string|required|regex:/^1[3456789][0-9]{9}$/',
            'hospital_id' => 'required|integer|gt:0',
            'depart_id' => 'required|integer|gt:0',
            'job_title' => 'nullable|string',
            'position'  => 'nullable|string',
            'field_id' => 'nullable|integer|gte:0',
            'clinic_rota' => 'nullable|string'
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => '用户名不能为空',
            'name.string' => '用户名格式不正确',
            'sex.in' => '性别格式不正确',
            'sex.integer' => '性别格式不正确',
            'mobile.string' => '手机号格式不正确',
            'mobile.required' => '手机号不能为空',
            'hospital_id.required' => '未选择医院',
            'hospital_id.integer' => '医院格式不正确',
            'hospital_id.gt' => '医院格式不正确',
            'depart_id.required' => '未选择部门',
            'depart_id.integer' => '部门格式不正确',
            'depart_id.gt' => '部门格式不正确',
            'job_title.string' => '职称格式不正确',
            'position.string' => '级别格式不正确',
            'field_id.integer' => '擅长领域格式不正确',
            'field_id.gte' => '擅长领域格式不正确',
            'clinic_rota.string' => '门诊情况格式不正确',
            'mobile.regex' => '手机号格式不正确'
        ];
    }
}
