<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */

namespace App\Request\Doctor;

use Youyao\Framework\FormRequest;

class IdcardRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_yyid' => 'required',
            'user_token' => 'required',
            'idcard_front' => 'required',
            'idcard_back' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'idcard_front.required' => 'idcard_front不能为空',
            'idcard_back.required' => 'idcard_back不能为空'
        ];
    }
}
