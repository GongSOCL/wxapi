<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\Doctor;

use App\Constants\DataStatus;
use App\Request\FormRequest;
use Hyperf\Grpc\StatusCode;
use Hyperf\Validation\Rule;

class QrcodeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'wechat_type' => [
                'required',
                'integer',
                Rule::in([DataStatus::WECHAT_AGENT, DataStatus::WECHAT_DOCTOR])
            ],
            'sense_str' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'wechat_type.required' => '公众号类型不能为空',
            'wechat_type.in' => '该类型公众号未开放',
            'sense_str.required' => '二维码参数不能为空'
        ];
    }
}
