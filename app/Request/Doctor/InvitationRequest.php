<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\Doctor;

use App\Request\FormRequest;

class InvitationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_yyid' => 'required',
            'user_token' => 'required',
            'user_type' => 'numeric',
            'is_auth' => 'numeric',
            'page' => 'numeric|min:1',
            'page_size' => 'numeric|min:1',
        ];
    }
}
