<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\Doctor;

use App\Request\FormRequest;

class PreInfoRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_yyid' => 'required',
            'user_token' => 'required',
            'mobile_num' => 'required|regex:/^1[0-9]{10}$/'
        ];
    }
}
