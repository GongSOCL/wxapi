<?php

declare(strict_types=1);

namespace App\Request\Doctor;

use Hyperf\Validation\Request\FormRequest;

class MergeDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'agree' => 'required',
        ];
    }

    public function messages(): array
    {
        return [];
    }
}
