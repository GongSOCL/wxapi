<?php

declare(strict_types=1);

namespace App\Request\Doctor;

use Hyperf\Validation\Request\FormRequest;

class SearchValidMergeDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'mobile' => [
                'required',
                'string',
                'regex:/^1(3\d|4[5-9]|5[0-35-9]|6[2567]|7[0-8]|8\d|9[0-35-9])\d{8}$/'
            ]
        ];
    }

    public function messages(): array
    {
        return [
            'mobile.required' => '手机号不能为空',
            'mobile.string' => '手机号格式不正确',
            'mobile.regex' => '手机号格式不正确'
        ];
    }
}
