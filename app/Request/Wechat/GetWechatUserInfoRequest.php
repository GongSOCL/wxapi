<?php

declare(strict_types=1);

namespace App\Request\Wechat;

use App\Constants\DataStatus;
use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class GetWechatUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'wechat_type' => [
                'required',
                'integer',
                Rule::in([DataStatus::WECHAT_DOCTOR, DataStatus::WECHAT_AGENT])
            ],
            'openid' => [
                'required',
                'string'
            ]
        ];
    }

    public function messages(): array
    {
        return [
            'wechat_type.required' => '公众号类型不能为空',
            'wechat_type.in' => '该类型公众号未开放',
            'openid.required' => '微信授权信息不能为空'
        ];
    }
}
