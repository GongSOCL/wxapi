<?php

declare(strict_types=1);

namespace App\Request\Wechat;

use App\Constants\DataStatus;
use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class OauthUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                Rule::in([DataStatus::WECHAT_AGENT, DataStatus::WECHAT_DOCTOR])
            ],
            'code' => 'required|string',
            'state' => 'required|string'
        ];
    }

    public function messages(): array
    {
        return [
            'type.required' => '公众号类型不能为空',
            'type.in' => '该类型公众号未开放',
            'code.required' => '授权码不能为空',
            'code.string' => '授权码格式不正确',
            'state.required' => '授权参数不能为空'
        ];
    }
}
