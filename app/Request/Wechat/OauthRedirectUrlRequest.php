<?php

declare(strict_types=1);

namespace App\Request\Wechat;

use App\Constants\DataStatus;
use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class OauthRedirectUrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                Rule::in([DataStatus::WECHAT_AGENT, DataStatus::WECHAT_DOCTOR])
            ],
            'is_silent' => [
                'integer',
                'nullable',
                Rule::in([1, 0])
            ],
            'state' => 'string|nullable',
            'redirect' => 'required|url'
        ];
    }

    public function messages(): array
    {
        return [
            'type.required' => '公众号类型不能为空',
            'type.in' => '该类型公众号未开放',
            'is_silent.integer' => '公众号授权类型不正确',
            'is_silent.in' => '公众号授权类型不正确',
            'redirect.required' => '公众号授权跳转地址不能为空',
            'redirect.url' => '公众号授权跳转地址不正确'
        ];
    }
}
