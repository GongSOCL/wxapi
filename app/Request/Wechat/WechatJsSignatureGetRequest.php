<?php

declare(strict_types=1);

namespace App\Request\Wechat;

use App\Constants\DataStatus;
use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class WechatJsSignatureGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                Rule::in([DataStatus::WECHAT_AGENT, DataStatus::WECHAT_DOCTOR])
            ],
            'url' => 'required|url',
            'apis' => 'string|nullable'
        ];
    }

    public function messages(): array
    {
        return [
            'type.required' => '公众号类型不能为空',
            'type.in' => '该类型公众号未开放',
            'url.required' => '微信授权回跳地址不能为空',
            'url.url' => '微信授权回跳地址不正确',
            'apis.string' => '微信功能格式不正确'
        ];
    }
}
