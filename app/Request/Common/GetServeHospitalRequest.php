<?php

declare(strict_types=1);

namespace App\Request\Common;

use Hyperf\Validation\Request\FormRequest;

class GetServeHospitalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'keywords' => 'string|nullable'
        ];
    }

    public function messages(): array
    {
        return [
            'keywords.string' => '搜索关键字格式不正确'
        ];
    }
}
