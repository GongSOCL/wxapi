<?php

declare(strict_types=1);

namespace App\Request\Common;

use Hyperf\Validation\Request\FormRequest;

class ServeHospitalDepartSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'hospital_id' => 'integer|nullable',
            'keywords' => 'string|nullable'
        ];
    }

    public function messages(): array
    {
        return [
            'hospital_id.integer' => '医院id参数格式不正确',
            'keywords.string' => '搜索关键字参数格式不正确'
        ];
    }
}
