<?php

declare(strict_types=1);

namespace App\Request\Common;

use App\Constants\DataStatus;
use Hyperf\Validation\Request\FormRequest;
use Hyperf\Validation\Rule;

class ReverseGeoRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'lng' => 'required|string',
            'lat' => 'required|string',
            'coordinate_type' => [
                'nullable',
                'string',
                Rule::in([DataStatus::COORDINATE_GPS, DataStatus::COORDINATE_GCJ02])
            ]
        ];
    }

    public function messages(): array
    {
        return [
            'lng.required' => '经度不能为空',
            'lat.required' => '纬度不能为空',
            'coordinate_type.string' => '坐标体系格式不正确',
            'coordinate_type.in' => '暂不支持该坐标系'
        ];
    }
}
