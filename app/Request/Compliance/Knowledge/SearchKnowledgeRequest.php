<?php

declare(strict_types=1);

namespace App\Request\Compliance\Knowledge;

use Hyperf\Validation\Request\FormRequest;

class SearchKnowledgeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'keywords' => 'string|required'
        ];
    }

    public function messages(): array
    {
        return [
            'keywords.required' => '搜索关键字不能为空'
        ];
    }
}
