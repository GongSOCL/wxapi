<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Request\Compliance\Compliance;

use App\Request\FormRequest;

class AgreeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'openid' => 'required|string|max:64',
            'user_yyid' => 'required|alpha_num|max:32',
            'user_token' => 'required|max:64',
            'exam_yyid' => 'required|max:64',
        ];
    }
}
