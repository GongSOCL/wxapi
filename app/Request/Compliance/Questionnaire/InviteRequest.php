<?php

declare(strict_types=1);

namespace App\Request\Compliance\Questionnaire;

use Hyperf\Validation\Request\FormRequest;

class InviteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'paper_yyid' => 'required',
            'openid' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'paper_yyid.required' => '问卷不能为空',
            'openid.required' => '微信授权信息不能为空'
        ];
    }
}
