<?php

declare(strict_types=1);

namespace App\Request\Compliance\Questionnaire;

use Hyperf\Validation\Request\FormRequest;

class StartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'openid' => 'required',
            'paper_yyid' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'openid.required' => '微信授权信息不能为空',
            'paper_yyid.required' => '问卷不能为空'
        ];
    }
}
