<?php

declare(strict_types=1);

namespace App\Request\Compliance\Questionnaire;

use Hyperf\Validation\Request\FormRequest;

class GetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'openid' => 'required',
            'paper_yyid' => 'required',
            'question_yyid' => 'required',
            'pos' => 'integer'
        ];
    }

    public function messages(): array
    {
        return [
            'openid.required' => '微信授权信息不能为空',
            'paper_yyid.required' => '问卷不能为空',
            'question_yyid.required' => '试题不能为空',
            'pos.integer' => '题目编号格式不正确'
        ];
    }
}
