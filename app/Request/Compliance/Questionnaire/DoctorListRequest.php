<?php

declare(strict_types=1);

namespace App\Request\Compliance\Questionnaire;

use Hyperf\Validation\Request\FormRequest;

class DoctorListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'paper_yyid' => 'required|string',
            'keywords' => 'string|nullable'
        ];
    }

    public function messages(): array
    {
        return [
            'paper_yyid.required' => '问卷不能为空',
            'paper_yyid.string' => '问卷格式不正确',
            'keywords.string' => '搜索关键字格式不正确'
        ];
    }
}
