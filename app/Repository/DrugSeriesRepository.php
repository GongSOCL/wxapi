<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DrugSeries;
use Hyperf\Utils\Collection;

class DrugSeriesRepository
{
    public static function getByIds(array $seriesIds): Collection
    {
        return DrugSeries::whereIn('id', $seriesIds)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }
    /**
     * @param $drugIds
     * @return \Hyperf\Utils\Collection
     */
    public static function getVisitDrugInfo($drugIds)
    {
        return DrugSeries::select(['id', 'series_name as name'])
            ->whereIn('id', $drugIds)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function getById($id): ?DrugSeries
    {
        return DrugSeries::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getValidSeriesByYyids(array $yyids): Collection
    {
        return DrugSeries::whereIn("yyid", $yyids)
            ->where("status", DataStatus::REGULAR)
            ->get();
    }

    /**
     * @param $yyid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getByYyid($yyid)
    {
        return DrugSeries::where([
            'yyid' => $yyid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getByYYIDS(array $seriesYYIDS)
    {
        return DrugSeries::whereIn('yyid', $seriesYYIDS)
            ->get();
    }

    /**
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getAvailableSeries()
    {
        return DrugSeries::where('status', DataStatus::REGULAR)->orderBy('sort', 'desc')->get();
    }

    /**
     * @param $name
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getSeriesByDrugName($name)
    {
        return DrugSeries::where([
            'series_name' => $name,
            'status' => DataStatus::REGULAR
        ])->first();
    }
}
