<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\UserSession;

class UserSessionRepository
{
    /**
     * 获取用户登陆session
     * @param $userYYID
     * @return UserSession|null
     */
    public static function getUserSession($userYYID)
    {
        return UserSession::where([
            'yyid'=> $userYYID,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * 删除用户session
     * @param $userYYID
     * @return int|mixed
     */
    public static function deleteUserSession($userYYID)
    {
        return UserSession::where('yyid', $userYYID)->delete();
    }
}
