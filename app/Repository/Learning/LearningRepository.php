<?php
declare(strict_types=1);
namespace App\Repository\Learning;

use App\Constants\DataStatus;
use App\Helper\Helper;
use App\Model\Qa\DrugSeries;
use App\Model\Qa\Group;
use App\Model\Qa\LearningMaterialsDistribution;
use App\Model\Qa\LearningPlan;
use App\Model\Qa\NewKnowledge;
use App\Model\Qa\NewKnowledgeUserPower;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\TComplianceDirSeries;
use Hyperf\Database\Query\Expression;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Validation\Rules\In;

class LearningRepository
{

    public function getKnowledgeList($where, $keywords, $pid)
    {


        $userId     = Helper::getLoginUser()->uid;
        $userYYId     = Helper::getLoginUser()->yyid;
        var_dump($userId. '   用户id ----------------------- ');
        # 用户目录权限 获取当前用户拥有权限的所有目录ID
        $Dirs = NewKnowledgeUserPower::where('uid', $userId)->select('dir_id')->get();
        $DirIds = $Dirs->isNotEmpty()?$Dirs->toArray():[];
//        var_dump('用户文档id');
//        var_dump($DirIds);
//
//        # 产品目录权限 查询该用户在哪些产品下 获取产品的id，再通过 TComplianceDirSeries获取所有目录id
//
//        # 用户yyid 找 服务产品yyid
        $RepsServeScopeS = RepsServeScopeS::where('user_yyid', $userYYId)->select('series_yyid')->get();
        $RepsServeScopeYYIDS = $RepsServeScopeS->isNotEmpty()?$RepsServeScopeS->toArray():[];
        var_dump('产品yyid');
        var_dump($RepsServeScopeYYIDS);

//
//        # 产品yyid 找到对应的产品id
        $DrugSeries = DrugSeries::whereIn('yyid', $RepsServeScopeYYIDS)->select('id')->get();
        $DrugSeriesIds = $DrugSeries->isNotEmpty()?$DrugSeries->toArray():[];
//        # 产品id 找目录id
        $TComplianceDirSeries = TComplianceDirSeries::whereIn('series_id', $DrugSeriesIds)->select('dir_id')->get();
        $TComplianceDirSeries = $TComplianceDirSeries->isNotEmpty()?$TComplianceDirSeries->toArray():[];
        var_dump('产品对应权限目录');
        var_dump($TComplianceDirSeries);

        $UniqueDirIds = array_merge($TComplianceDirSeries, $DirIds);
        $UniqueDirIds = array_unique(array_column($UniqueDirIds, 'dir_id'));
        # 目录id 找到对 的目录 ----- 先找到对应目录下的所有文件
        var_dump('$UniqueDirIds');
        var_dump($UniqueDirIds);
        $field = ["id","pid","title","type","file_type","file_address","introduce","sort","created_at"];
        if ($keywords) {
            if ($pid) {
                $UniqueDirIds = [$pid];
            }
            $NewKnowledges = NewKnowledge::where(function ($query) use ($UniqueDirIds, $where) {
                $query->whereIn('id', $UniqueDirIds)
                    ->where($where);
            })
                ->orWhere(function ($query) use ($UniqueDirIds, $where) {
                    $query->whereIn('pid', $UniqueDirIds)
                        ->where($where);
                })->select()->get($field);
        } else {
//            $NewKnowledges = NewKnowledge::where(function ($query) use ($UniqueDirIds){
//                $query->whereIn('id', $UniqueDirIds)
//                    ->where([
//                        ['is_deleted' ,'=', 0],
//                        ['status' ,'=', 1],
//                        ['type' ,'=', 1]
//                    ]);
//            })
//                ->orWhere(function ($query) use ($UniqueDirIds) {
//                    $query->whereIn('pid', $UniqueDirIds)
//                        ->where([
//                            ['is_deleted' ,'=', 0],
//                            ['status' ,'=', 1],
//                            ['type' ,'=', 1]
//                        ]);
//                })->select()
//                ->get($field);

            $NewKnowledges = NewKnowledge::where([
                ['pid' ,'=', 0],
                ['is_deleted' ,'=', 0],
                ['status' ,'=', 1],
                ['type' ,'=', 1]])
                ->whereIn('id', $UniqueDirIds)
                ->get($field);
        }
        $NewKnowledges= $NewKnowledges->isNotEmpty()?$NewKnowledges->toArray():[];

        return $NewKnowledges;
    }


    /**
     * @param $id
     * @param $type
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public function getKnowledgeInfo($id, $type)
    {
        $field = [
            "id",
            "pid",
            "title",
            "type",
            "file_type",
            "file_address",
            "introduce",
            "sort",
            "created_at",
            "video_pass_time",
            "video_total_time"
        ];

        if ($type == 1) {
            $info = NewKnowledge::where('pid', $id)->select($field)->get();
            return $info->isNotEmpty() ? $info->toArray() : [];
        } else {
            $info = NewKnowledge::where('id', $id)->where('type', $type)->select($field)->first();
            return $info ? $info->toArray() : [];
        }
    }


    /**
     * 该用户下的所有学习计划
     * @param $uid
     * @return array
     */
    public function getPlanFinishList($uid)
    {
        $where = [['user_id',$uid],['is_deleted',0]];
        $PlanListLearining = LearningMaterialsDistribution::where($where)
            ->where(function ($query) {
                $query->where('status', 1)
                    ->orWhere(function ($query) {
                        $query->where('status', 2);
                    });
            })
            ->select(Db::raw("GROUP_CONCAT(distinct l_id SEPARATOR ',') as lear_id"))
            ->get();

        $PlanAll = LearningMaterialsDistribution::where($where)
            ->select(
                Db::raw("GROUP_CONCAT(distinct l_id SEPARATOR ',') as lear_id")
            )->get();
        $Learining = $PlanListLearining->isNotEmpty() ? $PlanListLearining->toArray() : [];
        $All= $PlanAll->isNotEmpty() ? $PlanAll->toArray() : [];
        return ['learining'=>$Learining[0],'all'=>$All[0]];
    }


    /**
     * 用户计划状态
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getPlanStatus($uid, $id, $mid)
    {
        return LearningMaterialsDistribution::where(
            [
                ['l_id',$id],
                ['user_id',$uid],
                ['m_id',$mid],
                ['status',3],
                ['is_deleted',0]
            ]
        )->first();
    }

    /**
     * 用户计划信息 判断是否可全部完成
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getPlans($uid, $id, $mid)
    {
        $LearningMaterialsDistribution = LearningMaterialsDistribution::where(
            [
                ['l_id',$id],
                ['user_id',$uid],
                ['m_id','<>',$mid],
                ['is_deleted',0]
            ]
        )
            ->select('status', 'lr_st')
            ->get();
        return $LearningMaterialsDistribution->isNotEmpty()?$LearningMaterialsDistribution:[];
    }

    /**
     *
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getPlansByMid($uid, $id, $mid)
    {
        return LearningMaterialsDistribution::where([['l_id',$id],['user_id',$uid],['m_id','=',$mid],['is_deleted',0]])
            ->whereNotIn('lr_st', [4,6])
            ->select('status')
            ->first();
    }

    /**
     * 用户 更新完成
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function updatePlanByUidPlanId($uid, $id, $mid, $status)
    {
        if ($status == 1) {
            LearningMaterialsDistribution::where([['l_id',$id],['user_id',$uid],['m_id',$mid],['is_deleted',0]])
                ->update(['status'=>3]);

            # 全部完成的时候 更新  learning_materials_distribution 中的 lr_st 和 finish_day 计划完成时间
            return LearningMaterialsDistribution::where([['l_id',$id],['user_id',$uid],['is_deleted',0]])
                ->update(['lr_st'=>6, 'finish_day'=>date('Y-m-d H:i:s')]);
        } else {
            return LearningMaterialsDistribution::where([['l_id',$id],['user_id',$uid],['m_id',$mid],['is_deleted',0]])
                ->update(['status'=>3]);
        }
    }


    /**
     * 用户计划详情
     * @param $where
     * @return array
     */
    public function getplanInfo($where)
    {
        $PlanAll = LearningMaterialsDistribution::where($where)->get();
        return $PlanAll->isNotEmpty() ? $PlanAll->toArray() : [];
    }
    /**
     * 用户计划详情
     * @param $where
     * @return array
     */
    public function getplanMaterialInfo($where)
    {
        $PlanAll =  LearningMaterialsDistribution::from('learning_materials_distribution as a')
            ->leftJoin('new_knowledge as b', 'a.m_id', '=', 'b.id')
            ->select(
                'b.id',
                'b.title',
                'b.file_type',
                'b.type',
                'b.sort',
                'b.file_address',
                'b.introduce',
                'b.video_pass_time',
                'b.video_total_time',
                'a.status',
                'a.status as a_status',
                'a.l_id'
            )
            ->where($where)
            ->get();
        return $PlanAll->isNotEmpty() ? $PlanAll->toArray() : [];
    }


    /**
     * 用户计划描述
     * @param $where
     * @return array
     */
    public function getplanDescribe($id)
    {
        $info = LearningPlan::where('id', $id)->first();
        return $info?$info->toArray():[];
    }

    /**
     * 用户计划列表
     * @param $where
     * @return array
     */
    public function getPlanList($where)
    {
        $PlanAll =  LearningMaterialsDistribution::from('learning_materials_distribution as a')
            ->leftJoin('learning_plan as b', 'a.l_id', '=', 'b.id')
            ->select('b.*', Db::raw("GROUP_CONCAT(a.status SEPARATOR ',') as stu"), "a.lr_st", "a.exam_id as aexam_id")
            ->groupBy(['b.id'])
            ->where($where)
            ->where('b.status', 1)
            ->where('b.is_deleted', 0)
            ->orderBy('b.created_at', 'desc')
            ->get();
        return $PlanAll->isNotEmpty() ? $PlanAll->toArray() : [];
    }


    /**
     * 获取对应资料状态
     * @param $userId
     * @param $plan_id
     * @param $mid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getMaterialStatus($userId, $plan_id, $mid)
    {
        return LearningMaterialsDistribution::where(
            [
                ['l_id',$plan_id],
                ['user_id',$userId],
                ['m_id',$mid]
            ]
        )->select('status')->first();
    }


    /**
     * 当阅读时 更新该用户 该计划的阅读状态
     * @param $learn_id
     * @param $userId
     * @return int
     */
    public function updateLRSTByUidPlanId($id, $learn_id, $userId)
    {
        LearningMaterialsDistribution::where([['l_id',$learn_id],['user_id',$userId],['is_deleted',0]])
            ->whereNotIn('lr_st', [1,4,6])   # 1计划进行中 4过期 6完成
            ->update(['lr_st'=>1]);

        # 更新资料状态
        return LearningMaterialsDistribution::where(
            [
                ['l_id',$learn_id],
                ['user_id',$userId],
                ['m_id',$id],
                ['is_deleted',0]
            ]
        )
            ->where('status', 1)
            ->update(['status'=>2]);
    }
}
