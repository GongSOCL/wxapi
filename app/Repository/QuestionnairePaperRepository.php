<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\QuestionnairePaper;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class QuestionnairePaperRepository
{
    /**
     * 根据问卷yyid获取问卷
     *
     * @param $paperYYID
     * @return QuestionnairePaper|null
     */
    public static function getPaperByYyid($paperYYID)
    {
        return QuestionnairePaper::where([
            'yyid' => $paperYYID,
        ])->where('status', '!=', QuestionnairePaper::STATUS_DELETED)
            ->first();
    }

    /**
     * 获取用户进行中的开放邀请的问卷
     *
     * @param $userId
     * @return array|Collection
     */
    public static function getUserProcessingInviteAnswerPapersIds($userId)
    {
        $now = date('Y-m-d H:i:s');
        $result = Db::table('t_questionnaire_paper')
            ->join(
                't_questionnaire_paper_invitee',
                't_questionnaire_paper_invitee.paper_id',
                '=',
                't_questionnaire_paper.id'
            )
            ->where([
                't_questionnaire_paper.platform' => QuestionnairePaper::PLATFORM_YOUYAO,
                't_questionnaire_paper.status' => QuestionnairePaper::STATUS_PUBLISHED,
                't_questionnaire_paper.is_invite_answer' => QuestionnairePaper::INVITE_ANSWER_YES,
                't_questionnaire_paper_invitee.status' => DataStatus::REGULAR,
                't_questionnaire_paper_invitee.invitee_id' => $userId,
            ])->whereRaw(
                "((t_questionnaire_paper.expire_time is null) or (t_questionnaire_paper.expire_time > '$now'))"
            )
            ->select('t_questionnaire_paper.id')
            ->get();

        $ids = [];
        if ($result && $result->isNotEmpty()) {
            $ids = $result->pluck('id')->toArray();
        }

        return $ids;
    }

    /**
     * 获取用户已结束的开放邀请的问卷
     *
     * @param $userId
     * @return array
     */
    public static function getUserFinishedInviteAnswerPapersIds($userId)
    {
        $now = date('Y-m-d H:i:s');
        $result = Db::table('t_questionnaire_paper')
            ->join(
                "t_questionnaire_paper_invitee",
                "t_questionnaire_paper_invitee.paper_id",
                "=",
                "t_questionnaire_paper.id"
            )
            ->where(['t_questionnaire_paper.status' => QuestionnairePaper::STATUS_PUBLISHED,
                't_questionnaire_paper.platform' => QuestionnairePaper::PLATFORM_YOUYAO,
                't_questionnaire_paper.is_invite_answer' => QuestionnairePaper::INVITE_ANSWER_YES,
                't_questionnaire_paper_invitee.status' => DataStatus::REGULAR,
                't_questionnaire_paper_invitee.invitee_id' => $userId,
            ])->whereRaw(
                "(t_questionnaire_paper.expire_time is not null and t_questionnaire_paper.expire_time <= '$now')"
            )
            ->select('t_questionnaire_paper.id')
            ->get();

        $ids = [];
        if ($result && $result->isNotEmpty()) {
            $ids = $result->pluck('id')->toArray();
        }

        return $ids;
    }

    /**
     * 根据问卷id列表获取问卷详情
     *
     * @param $ids
     * @return Collection
     */
    public static function getPapersByIds($ids): Collection
    {
        return QuestionnairePaper::whereIn('id', $ids)
            ->where('status', '!=', QuestionnairePaper::STATUS_DELETED)
            ->orderByDesc('id')
            ->get();
    }

    /**
     * 完成一单问卷后更新统计数据
     *
     * @param QuestionnairePaper $paper
     * @return int
     */
    public static function finishOne(QuestionnairePaper $paper)
    {
        return QuestionnairePaper::where([
            'id' => $paper->id,
            'status' => QuestionnairePaper::STATUS_PUBLISHED
        ])->increment('finished_quota', 1, [
            'modify_time' => date("Y-m-d H:i:s")
        ]);
    }

    /**
     * 获取同名问卷
     * @param $paperName
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getPaperByName($paperName)
    {
        return QuestionnairePaper::where('name', $paperName)
            ->where('status', '!=', QuestionnairePaper::STATUS_DELETED)
            ->first();
    }

    /**
     * 添加问卷
     * @param $paperName
     * @param $totalQuota
     * @param int $inviteExpireHours
     * @param bool $isWrongTips
     * @param bool $isPublish
     * @param float $cash
     * @param int $points
     * @param bool $isInviteToAnswer
     * @return QuestionnairePaper
     */
    public static function addOneQuestionnaire(
        $paperName,
        $totalQuota,
        $inviteExpireHours = 0,
        $isWrongTips = true,
        $isPublish = true,
        $cash = 0.00,
        $points = 0,
        $isInviteToAnswer = true
    ): QuestionnairePaper {
        $now = date('Y-m-d H:i:s');
        $o = new QuestionnairePaper();
        $o->yyid = Db::raw("replace(upper(uuid()),'-','')");
        $o->name = $paperName;
        $o->wrong_tips = $isWrongTips;
        $o->publish_time = $now;
        $o->total_quota = $totalQuota;
        $o->finished_quota = 0;
        $o->status = $isPublish ? QuestionnairePaper::STATUS_PUBLISHED : QuestionnairePaper::STATUS_NEW;
        $o->cash = $cash;
        $o->points = $points;
        $o->invite_expire_hour = $inviteExpireHours;
        $o->is_invite_answer = $isInviteToAnswer ? 1 : 0;
        $o->created_time = $o->modify_time = $now;
        $o->save();

        $o->refresh();

        return $o;
    }

    /**
     * 根据问卷id获取问卷详情
     * @param $id
     * @return QuestionnairePaper|null
     */
    public static function getPaperById($id)
    {
        return QuestionnairePaper::where('id', $id)
            ->where('status', '!=', QuestionnairePaper::STATUS_DELETED)
            ->first();
    }
}
