<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\Company;
use App\Model\Qa\UserCompanyChange;
use App\Model\Qa\Users;
use Carbon\Carbon;

class UserCompanyChangeRepository
{

    public static function addChange(Users $user, Company $oldCompany, Company $newCompany, $operatorId)
    {
        $o = new UserCompanyChange();
        $o->user_id = $user->uid;
        $o->before_company_id = $oldCompany->id;
        $o->after_company_id = $newCompany->id;
        $o->operator_id = $operatorId;
        $o->created_at = $o->updated_at = Carbon::now();
        $o->save();
    }
}