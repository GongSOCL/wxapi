<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\UploadQiniu;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;
use Hyperf\Utils\Collection;

class UploadQiniuRepository
{
    /**
     * @param $id
     * @return Builder|Model|object|null
     */
    public static function getById($id): ?UploadQiniu
    {
        return UploadQiniu::where('id', $id)
            ->first();
    }


    public static function getByIds(array $ids): Collection
    {
        return UploadQiniu::whereIn('id', $ids)
            ->where('status', '=', UploadQiniu::STATUS_NORMAL)
            ->get();
    }

    /**
     * @param $picIds
     * @return Collection
     */
    public static function getVisitPicInfo($picIds)
    {
        return UploadQiniu::select(['id', 'file_name as name', 'url'])
            ->whereIn('id', $picIds)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    /**
     * 预删除文件
     * @param UploadQiniu $uploadItem
     * @return bool
     */
    public static function preDeleteFile(UploadQiniu $uploadItem): bool
    {
        return UploadQiniu::where('id', $uploadItem->id)
            ->where('status', UploadQiniu::STATUS_NORMAL)
            ->update([
                'status' => UploadQiniu::STATUS_PRE_DELETE,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }

    //标记已经已经删除
    public static function delFile(UploadQiniu $uploadItem)
    {
        return UploadQiniu::where('id', $uploadItem->id)
            ->update([
                'status' => UploadQiniu::STATUS_DELETED,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }

    public static function createWechatLoginoutHeadimg($uid, $headimgurl)
    {
        return UploadQiniu::insertGetId([
            'bucket' => 'wechat',
            'file_name' => 'id_' . $uid,
            'file_key' => 'id_' . $uid,
            'url' => $headimgurl,
            'status' => DataStatus::REGULAR,
            'created_time' => date('Y-m-d H:i:s'),
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }
}
