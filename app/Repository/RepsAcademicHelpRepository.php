<?php
declare(strict_types=1);
namespace App\Repository;

use App\Helper\Helper;
use App\Model\Qa\RepsAcademicHelp;
use App\Model\Qa\RepsServeScopeP;
use App\Model\Qa\Users;

class RepsAcademicHelpRepository
{
    
    public static function addHelp(Users $user, $type, $content, $email = ""): RepsAcademicHelp
    {
        $o = new RepsAcademicHelp();
        $o->yyid = Helper::yyidGenerator();
        $o->user_yyid = $user->yyid;
        $o->type = $type;
        $o->content = $content;
        $o->email = (string)$email;
        $o->ask_time = $o->created_time = $o->modify_time = date('Y-m-d H:i:s');
        $o->save();

        return $o;
    }

    public static function getById($helpId)
    {
        return RepsAcademicHelp::where('id', $helpId)
            ->first();
    }
}
