<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\MorningShare;
use App\Model\Qa\MorningShareCount;
use App\Model\Qa\MorningShareDepartment;
use App\Model\Qa\MorningShareType;

class MorningShareRepository
{
    /**
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getType()
    {
        return MorningShareType::where('is_deleted', DataStatus::DELETE)
            ->select('id as type_id', 'title as type')
            ->get();
    }

    /**
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getDepartment()
    {
        return MorningShareDepartment::where('is_deleted', DataStatus::DELETE)
            ->select('id as department_id', 'title as department')
            ->get();
    }

    /**
     * @param $where
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function getList($where, $page, $pageSize)
    {
        return MorningShare::from('t_morning_share as ms')
            ->join('t_morning_share_department as msd', 'ms.department_id', '=', 'msd.id')
            ->join('t_morning_share_type as mst', 'ms.type_id', '=', 'mst.id')
            ->where($where)
            ->select(['ms.*', 'msd.title as department', 'mst.title as type'])
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->latest('ms.created_at')
            ->get();
    }

    /**
     * @param $where
     * @return int
     */
    public static function getListCount($where)
    {
        return MorningShare::from('t_morning_share as ms')
            ->join('t_morning_share_department as msd', 'ms.department_id', '=', 'msd.id')
            ->join('t_morning_share_type as mst', 'ms.type_id', '=', 'mst.id')
            ->where($where)->count();
    }

    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getDetailById($id)
    {
        return MorningShare::from('t_morning_share as ms')
            ->join('t_morning_share_department as msd', 'ms.department_id', '=', 'msd.id')
            ->join('t_morning_share_type as mst', 'ms.type_id', '=', 'mst.id')
            ->where('ms.id', $id)
            ->select(['ms.*', 'msd.title as department', 'mst.title as type'])
            ->first();
    }

    /**
     * @param $picId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getCountByPicId($picId)
    {
        return MorningShareCount::where('pic_id', $picId)
            ->groupBy(['user_id', 'share_user_id', 'share_user_type'])
            ->get();
    }

    /**
     * @param $id
     * @param $picId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function checkShareStatus($id, $picId)
    {
        return MorningShareCount::where([
            'share_user_id' => $id,
            'pic_id' => $picId
        ])->first();
    }

    /**
     * @param $id
     * @param $picId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getCountByUserId($id, $picId)
    {
        return MorningShareCount::where([
            'pic_id' => $picId,
            'share_user_id' => $id
        ])->groupBy(['user_id', 'share_user_id', 'share_user_type'])->get();
    }

    /**
     * @return \Hyperf\Utils\Collection
     */
    public static function getCountRank()
    {
        return MorningShareCount::select(['user_id', 'pic_id', 'share_user_id'])
            ->groupBy(['user_id', 'share_user_id', 'share_user_type'])
            ->get();
    }

    /**
     * @param $id
     * @return \Hyperf\Utils\Collection
     */
    public static function getCountRankByUserId($id)
    {
        return MorningShareCount::select(['user_id', 'pic_id', 'share_user_id'])
            ->where('share_user_id', $id)
            ->groupBy(['user_id', 'share_user_id', 'share_user_type'])
            ->get();
    }

    /**
     * @return \Hyperf\Utils\Collection
     */
    public static function getPicId()
    {
        return MorningShare::select('id as pic_id')
            ->where([
                'status' => DataStatus::REGULAR,
                'is_deleted' => DataStatus::DELETE
            ])->get();
    }

    /**
     * @param $id
     * @return \Hyperf\Utils\Collection
     */
    public static function getPicIdByUserId($id)
    {
        return MorningShareCount::select('pic_id')
            ->where('share_user_id', $id)
            ->groupBy(['pic_id'])
            ->get();
    }
}
