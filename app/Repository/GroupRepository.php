<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\Group;
use Hyperf\Database\Query\Expression;
use Hyperf\Di\Annotation\Inject;

class GroupRepository
{
    /**
     * @Inject
     * @var Group
     */
    private $groupModel;

    public function createRepsGroup($leaderId, $groupName): int
    {
        $now = date('Y-m-d H:i:s');

        $data = [];
        $data['leader_id'] = $leaderId;
        $data['group_name'] = $groupName;
        $data['group_type'] = Group::GROUP_TYPE_REPS;
        $data['created_user_id'] = $leaderId;
        $data['status'] = DataStatus::REGULAR;
        $data['created_time'] = $now;
        $data['modify_time'] = $now;

        return $this->groupModel->newQuery()->insertGetId($data);
    }

    /**
     * 根据id获取群组信息
     *
     * @param $gid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getById($gid)
    {
        return Group::where(['id' => $gid, 'status' => DataStatus::REGULAR])
            ->first();
    }


    /**
     * 根据yyid获取group
     * @param $groupYyid
     * @param $groupType
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getGroupByYyid($groupYyid, $groupType)
    {
        return $this->groupModel::query()
            ->where([
                'yyid' => $groupYyid,
                'group_type' => $groupType,
                'status' => Group::STATUS_NORMAL
            ])->first();
    }

    /**
     * 获取所有群
     * @param $yyid
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public function getGroups($yyid, $type)
    {
        return $this->groupModel::query()
            ->select('id', 'yyid', 'leader_yyid', 'group_name', 'group_type', 'created_user_yyid', 'status')
            ->where(
                [
                    'leader_yyid' => $yyid,
                    'group_type' => $type,
                    'status' => Group::STATUS_NORMAL
                ]
            )
            ->latest()
            ->get();
    }

    /**
     * 判断是否同名群
     * @param $yyid
     * @param $groupName
     * @param $type
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getSameGroup($yyid, $groupName, $type, $groupYyid)
    {
        if ($groupYyid) {
            //重命名的时候判断 要排除本身
            return $this->groupModel::query()
                ->where(
                    [
                        'leader_yyid' => $yyid,
                        'group_name' => $groupName,
                        'group_type' => $type,
                        'status' => Group::STATUS_NORMAL
                    ]
                )->where('yyid', '<>', $groupYyid)->first();
        } else {
            //创建新群组的时候判断
            return $this->groupModel::query()
                ->where(
                    [
                        'leader_yyid' => $yyid,
                        'group_name' => $groupName,
                        'group_type' => $type,
                        'status' => Group::STATUS_NORMAL
                    ]
                )->first();
        }
    }

    /**
     * 创建群组
     * @param $yyid
     * @param $groupName
     * @param $type
     * @return int
     */
    public function createGroup($yyid, $groupName, $type)
    {

        $data = [
            'yyid' => new Expression("replace(upper(uuid()),'-','')"),
            'leader_yyid' => $yyid,
            'group_name' => $groupName,
            'group_type' => $type,
            'created_user_yyid' => $yyid,
            'status' => Group::STATUS_NORMAL,
            'created_time' => date('Y-m-d H:i:s', time()),
            'modify_time' => date('Y-m-d H:i:s', time())
        ];
        return $this->groupModel::query()->insertGetId($data);
    }


    /**
     * 删除组
     * @param $groupYyid
     * @param $type
     * @return int
     */
    public function removeGroupByYyid($groupYyid, $type)
    {
        return $this->groupModel::query()
            ->where(
                [
                    'yyid' => $groupYyid,
                    'group_type' => $type
                ]
            )->update(
                [
                    'status' => Group::STATUS_DELETE,
                    'modify_time' => date('Y-m-d H:i:s')
                ]
            );
    }

    /**
     * 重命名
     * @param $groupYyid
     * @param $groupName
     * @param $type
     * @return int
     */
    public function renameGroup($groupYyid, $groupName, $type)
    {
        return $this->groupModel::query()
            ->where(
                [
                    'yyid' => $groupYyid,
                    'group_type' => $type
                ]
            )->update(
                [
                    'group_name' => $groupName,
                    'modify_time' => date('Y-m-d H:i:s')
                ]
            );
    }
}
