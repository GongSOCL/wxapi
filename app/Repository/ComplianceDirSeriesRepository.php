<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ComplianceDirSeries;
use Hyperf\Database\Model\Collection;

class ComplianceDirSeriesRepository
{
    /**
     * 获取根据权限配置的子目录id
     * @param $seriesIds
     * @return Collection|array
     */
    public static function getDirIdsBySeriesIds($seriesIds): Collection
    {
        return ComplianceDirSeries::where('status', DataStatus::REGULAR)
            ->whereIn("series_id", $seriesIds)
            ->get();
    }
}
