<?php
declare(strict_types=1);

namespace App\Repository\LiveInvite;

use App\Constants\DataStatus;
use App\Model\Core\Live;
use App\Model\Core\LiveParticipator;
use App\Model\Doctor\WechatUser;
use App\Model\Qa\Agent;
use App\Model\Qa\LiveInvite;
use App\Model\Qa\LiveInviteDetail;
use App\Model\Qa\LiveInviteHistory;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Qa\YouyaoHospital;

class LiveInviteRepository
{
    /**
     * @param $where
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getLives($where, $page, $pageSize)
    {
        return Live::where($where)
            ->select(['id', 'title', 'live_state', 'thumbnail_img', 'start_time'])
            ->latest()
            ->offset($pageSize * ($page - 1))
            ->limit($pageSize)
            ->get();
    }

    /**
     * @param $where
     * @return int
     */
    public static function getLivesCount($where)
    {
        return Live::where($where)->count();
    }

    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getLiveInfoById($id)
    {
        return Live::where('id', $id)->first();
    }

    /**
     * @param $where
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function getDoctors($where, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where($where)
            ->select([
                'ndou.id',
                'ndu.nickname',
                'ndu.true_name',
                'ndu.headimgurl',
                'ndu.hospital_name',
                'ndu.depart_name',
                'ndu.position',
                'ndu.unionid',
                'is_subscribed as is_subscribe'
            ])
            ->groupBy(['ndou.new_doctor_id'])
            ->latest('ndou.created_at')
            ->offset($pageSize * ($page - 1))
            ->limit($pageSize)
            ->get();
    }

    /**
     * @param $where
     * @return int
     */
    public static function getDoctorsCount($where)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where($where)->groupBy(['ndou.new_doctor_id'])->get()->count();
    }

    /**
     * @param $where
     * @param $doctorIds
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function getOrgDoctors($where, $doctorIds, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where($where)
            ->whereIn('nid', $doctorIds)
            ->select([
                'ndou.id',
                'ndu.nickname',
                'ndu.true_name',
                'ndu.headimgurl',
                'ndu.hospital_name',
                'ndu.depart_name',
                'ndu.position',
                'ndu.unionid',
                'is_subscribed as is_subscribe'
            ])
            ->groupBy(['ndou.new_doctor_id'])
            ->latest('ndou.created_at')
            ->offset($pageSize * ($page - 1))
            ->limit($pageSize)
            ->get();
    }

    /**
     * @param $where
     * @param $doctorIds
     * @return int
     */
    public static function getOrgDoctorsCount($where, $doctorIds)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where($where)->whereIn('nid', $doctorIds)->groupBy(['ndou.new_doctor_id'])->get()->count();
    }

    /**
     * @param $userId
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getInviteList($userId, $page, $pageSize)
    {
        return LiveInvite::where('user_id', $userId)
            ->where('status', DataStatus::REGULAR)
            ->latest()
            ->offset($pageSize * ($page - 1))
            ->limit($pageSize)
            ->get();
    }

    /**
     * @param $userId
     * @return int
     */
    public static function getInviteListCount($userId)
    {
        return LiveInvite::where('user_id', $userId)
            ->where('status', DataStatus::REGULAR)
            ->count();
    }

    /**
     * @param $id
     * @return int
     */
    public static function getInviteDetailCount($id)
    {
        return LiveInviteDetail::where([
            'live_invite_id' => $id,
            'status' => DataStatus::REGULAR
        ])->count();
    }

    /**
     * @param $inviteId
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function getInviteDetail($inviteId, $page, $pageSize)
    {
        return LiveInviteDetail::from('live_invite_detail as lid')
            ->leftJoin('new_doctor_openid_userinfo as ndou', 'lid.doctor_info_id', '=', 'ndou.id')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where('live_invite_id', $inviteId)
            ->where('lid.status', DataStatus::REGULAR)
            ->select([
                'ndou.id',
                'nickname',
                'true_name',
                'headimgurl',
                'hospital_name',
                'depart_name',
                'position',
                'is_read'
            ])
            ->latest('lid.created_at')
            ->offset($pageSize * ($page - 1))
            ->limit($pageSize)
            ->get();
    }

    /**
     * @param $userId
     * @param $liveId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getInviteInfo($userId, $liveId)
    {
        return LiveInvite::where([
            'user_id' => $userId,
            'live_id' => $liveId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $userId
     * @param $liveId
     * @return int
     */
    public static function createInviteInfo($userId, $liveId)
    {
        return LiveInvite::insertGetId([
            'user_id' => $userId,
            'live_id' => $liveId,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param $inviteId
     * @param $oneId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getInviteDetailInfo($inviteId, $oneId)
    {
        return LiveInviteDetail::where([
            'live_invite_id' => $inviteId,
            'doctor_info_id' => $oneId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getDoctorInfo($id)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where('id', $id)->first();
    }

    /**
     * @param $inviteId
     * @param $doctorInfo
     */
    public static function createInviteDetailInfo($inviteId, $doctorInfo)
    {
        LiveInviteDetail::insert([
            'live_invite_id' => $inviteId,
            'doctor_info_id' => $doctorInfo->id,
            'doctor_openid' => $doctorInfo->openid,
            'doctor_unionid' => $doctorInfo->unionid,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param $userId
     * @param $inviteId
     * @param $doctorInfoid
     * @return int
     */
    public static function createInviteHistory($userId, $inviteId, $doctorInfoid)
    {
        return LiveInviteHistory::insertGetId([
            'user_id' => $userId,
            'live_invite_id' => $inviteId,
            'doctor_info_id' => $doctorInfoid,
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * @param $userId
     * @param $inviteId
     * @param $doctorInfoid
     * @return int
     */
    public static function getMaxInviteTimes($userId, $inviteId, $doctorInfoid)
    {
        return LiveInviteHistory::where([
            'user_id' => $userId,
            'live_invite_id' => $inviteId,
            'doctor_info_id' => $doctorInfoid,
        ])->count();
    }

    /**
     * @param $userId
     * @return \Hyperf\Database\Model\Model|\Hyperf\Database\Query\Builder|object|null
     */
    public static function getAgentInfo($userId)
    {
        return Agent::from('t_agent as a')
            ->join('t_users as u', 'a.u_yyid', '=', 'u.yyid')
            ->select(['u.name','a.truename'])
            ->where('u.uid', $userId)
            ->first();
    }

    /**
     * @param $liveId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getLiveChairman($liveId)
    {
        return LiveParticipator::where([
            'live_id' => $liveId,
            'user_type' => 1,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $liveId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getLiveSpeakers($liveId)
    {
        return LiveParticipator::where([
            'live_id' => $liveId,
            'user_type' => 2,
            'status' => DataStatus::REGULAR
        ])->get();
    }

    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getHospitalInfo($id)
    {
        return YouyaoHospital::where('id', $id)->first();
    }

    /**
     * @param $unionid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getDoctorWechat($unionid)
    {
        return WechatUser::where('unionid', $unionid)->first();
    }

    /**
     * @param $userId
     * @param $inviteId
     * @param $doctorInfoId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getInviteHistory($userId, $inviteId, $doctorInfoId)
    {
        return LiveInviteHistory::where([
            'user_id' => $userId,
            'live_invite_id' => $inviteId,
            'doctor_info_id' => $doctorInfoId
        ])->first();
    }
}
