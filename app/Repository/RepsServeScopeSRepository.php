<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\DrugSeries;
use App\Model\Qa\DrugSku;
use App\Model\Qa\RepsServeScope;
use App\Model\Qa\RepsServeScopeP;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class RepsServeScopeSRepository
{
    /**
     * 获取用户服务医院相关记录
     * @param Users $user
     * @param YouyaoHospital $hospital
     * @return Collection
     */
    public static function getUserServeHospitalRecord(Users $user, YouyaoHospital $hospital): Collection
    {
        return RepsServeScopeS::where([
            'user_yyid' => $user->yyid,
            'hospital_yyid' => $hospital->yyid,
            'status' => DataStatus::REGULAR
        ])->get();
    }

    public static function isSeriesApplied(Users $user, YouyaoHospital $hospital, DrugSeries $series): bool
    {
        return RepsServeScopeS::where([
            'user_yyid' => $user->yyid,
            'hospital_yyid' => $hospital->yyid,
        ])->whereIn('status', [
            0, 1,2,3,5
        ])->exists();
    }

    public static function getHospitalSeriesServe(YouyaoHospital $hospital, DrugSeries $series): ?RepsServeScopeS
    {
        return RepsServeScopeS::where([
            'hospital_yyid' => $hospital->yyid,
            'series_yyid' => $series->yyid
        ])->whereIn('status', [
            1, 5, 6, 0
        ])->first();
    }

    public static function addServe(Users $user, YouyaoHospital $hospital, DrugSeries $series): RepsServeScopeS
    {
        $now = date('Y-m-d H:i:s');
        $o = new RepsServeScopeS();
        $o->yyid = Helper::generateYYID();
        $o->user_yyid = $user->yyid;
        $o->series_yyid = $series->yyid;
        $o->hospital_yyid = $hospital->yyid;
        $o->admin_id = 0;
        $o->agreed_time = $now;
        $o->status = 0;
        $o->created_time = $o->modify_time = $now;
        if (!$o->save()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "代理申请添加失败");
        }
        $o->refresh();

        return $o;
    }

    public static function getById($serveId): ?RepsServeScopeS
    {
        return RepsServeScopeS::where('id', $serveId)
            ->first();
    }

    /**
     * 申请解绑服务
     * @param RepsServeScopeS $serve
     * @return bool|int
     */
    public static function removeServe(RepsServeScopeS $serve)
    {
        return RepsServeScopeS::where([
            'id' => $serve->id,
        ])->update([
            'status' => 5,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getByServeProduct(RepsServeScopeP $serveProduct): ?RepsServeScopeS
    {
        return RepsServeScopeS::where([
            'user_yyid' => $serveProduct->user_yyid,
            'series_yyid' => $serveProduct->series_yyid,
            'hospital_yyid' => $serveProduct->hospital_yyid
        ])->orderByDesc('id')
            ->first();
    }

    public static function getServingBySeriesAndHospitals(DrugSeries $series, array $hospitalYYIDS): Collection
    {
        return RepsServeScopeS::where(['series_yyid' => $series->yyid])
            ->whereIn('hospital_yyid', $hospitalYYIDS)
            ->whereIn('status', [1, 5, 6, 0])
            ->select([Db::raw("distinct hospital_yyid")])
            ->get();
    }

    public static function getServeList(
        Users $users = null,
        YouyaoHospital $hospital = null,
        DrugSeries $series = null,
        $start = "",
        $end = "",
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        $query = RepsServeScopeS::query();
        if ($users) {
            $query->where('user_yyid', $users->yyid);
        }
        if ($hospital) {
            $query->where('hospital_yyid', $hospital->yyid);
        }
        if ($series) {
            $query->where('series_yyid', $series->yyid);
        }
        if ($start) {
            $query->where("created_time", ">=", $start);
        }
        if ($end) {
            $query->where("created_time", "<=", $end);
        }

        return $query->paginate($limit, ['*'], '', $current);
    }

    /**
     * @param RepsServeScopeS $serve
     * @param bool $isOk
     * @return bool|int
     */
    public static function auditServe(RepsServeScopeS $serve, bool $isOk)
    {
        return RepsServeScopeS::where([
            'id' => $serve->id
        ])->update([
            'status' => $isOk ? 1 : 2,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }

    public static function unbindAudit(RepsServeScopeS $serve, bool $isOk)
    {
        return RepsServeScopeS::where([
            'id' => $serve->id
        ])->update([
            'status' => $isOk ? 4 : 6,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getServeHospitalNum($userYyid)
    {
        return RepsServeScopeS::where('status', 1)
            ->select([
                'user_yyid',
                Db::raw('count(DISTINCT hospital_yyid) as hospital_num')
            ])->groupBy(['user_yyid'])->having('user_yyid', $userYyid)->first();
    }

    /**
     * 获取用户服务的药品系统
     * @param Users $user
     * @return Collection
     */
    public static function getUserServeSeries(Users $user): Collection
    {
        $query = Db::table("t_reps_serve_scope_s")
            ->join(
                "t_drug_series",
                't_drug_series.yyid',
                '=',
                't_reps_serve_scope_s.series_yyid'
            )->where([
                't_reps_serve_scope_s.user_yyid' => $user->yyid,
                't_reps_serve_scope_s.status' => DataStatus::REGULAR,
                't_drug_series.status' => DataStatus::REGULAR
            ])->select(["t_drug_series.*"]);

        return $query->get();
    }

    public static function cancelUserServer(Users $user)
    {
        return RepsServeScope::where([
            'user_yyid' => $user->yyid
        ])->delete();
    }

    /**
     * 我服务的医院
     * @param $agentYyid
     * @return Collection
     */
    public static function getDrugstoreListByAgentYyid($agentYyid)
    {
        return RepsServeScope::from('t_reps_serve_scope_s as t_rss')
            ->leftJoin('t_youyao_hospital as t_yh', 't_rss.hospital_yyid', '=', 't_yh.yyid')
            ->select([
                't_yh.id as drugstore_id',
                't_yh.hospital_name as drugstore_name'
            ])->where([
                't_rss.user_yyid' => $agentYyid,
                't_rss.status' => DataStatus::REGULAR
//                't_yh.org_type' => YouyaoHospital::ORG_TYPE_DRUGSTORE
            ])->get();
    }

    /**
     * 我服务的产品
     * @param $agentYyid
     * @return Collection
     */
    public static function getProductListByAgentYyid($agentYyid)
    {
        return RepsServeScope::from('t_reps_serve_scope_s as t_rss')
            ->leftJoin('t_drug_product as t_dp', 't_rss.series_yyid', '=', 't_dp.series_yyid')
            ->select([
                't_dp.id as drug_id',
                't_dp.name as drug_name'
            ])
            ->where([
                't_rss.user_yyid' => $agentYyid,
                't_rss.status' => DataStatus::REGULAR,
                't_dp.status' => DataStatus::REGULAR
            ])
            ->distinct()
            ->get();
    }

    /**
     * 判断该用户是否代理了某种产品
     * @param $userYyid
     * @param $seriesYyid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getInfoByUserAndSeries($userYyid, $seriesYyid)
    {
        return RepsServeScopeS::where([
            'user_yyid' => $userYyid,
            'series_yyid' => $seriesYyid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * 根据reps_serve_scope_s.id获取所有的drug_sku
     * @param array $ids
     * @return Collection
     */
    public static function getDrugSkuByRepsIds(array $ids): Collection
    {
        $sub = RepsServeScopeS::query()
            ->join("t_reps_serve_scope as trss", function ($query) {
                $query->where('trss.user_yyid', '=', Db::raw('t_reps_serve_scope_s.user_yyid'))
                   ->where('trss.hospital_yyid', Db::raw('t_reps_serve_scope_s.hospital_yyid'))
                   ->where('trss.status', Db::raw('t_reps_serve_scope_s.status'));
            })->whereIn('t_reps_serve_scope_s.id', $ids)
            ->select([Db::raw("distinct trss.drug_yyid as drug_yyid")]);

        return DrugSku::query()
            ->joinSub($sub, "sb", "t_drug_sku.yyid", "=", "sb.drug_yyid")
            ->select([Db::raw("t_drug_sku.*")])
            ->get();
    }

    public static function getByIds(array $serveIds): Collection
    {
        return RepsServeScopeS::query()
            ->whereIn('id', $serveIds)
            ->get();
    }

    public static function getUserServeHospitals(Users $user): Collection
    {
        $sub = RepsServeScopeS::query()
            ->join('t_youyao_hospital as tyh', 'tyh.yyid', '=', 't_reps_serve_scope_s.hospital_yyid')
            ->where('t_reps_serve_scope_s.user_yyid', $user->yyid)
            ->whereIn('t_reps_serve_scope_s.status', [1, 5, 6])
            ->where('tyh.status', DataStatus::REGULAR)
            ->select([Db::raw('distinct(tyh.id) as hid')]);

        return YouyaoHospital::query()
            ->joinSub($sub, 'sb', 't_youyao_hospital.id', '=', 'sb.hid')
            ->select(['t_youyao_hospital.*'])
            ->get();
    }

    public static function isUserServeHospital(Users $users, YouyaoHospital $hospital): bool
    {
        return RepsServeScopeS::query()
            ->where('user_yyid', $users->yyid)
            ->where('hospital_yyid', $hospital->yyid)
            ->where('status', [1, 5, 6])
            ->exists();
    }

    public static function getServeByHospital(YouyaoHospital $hospital): Collection
    {
        return RepsServeScopeS::query()
            ->where('hospital_yyid', $hospital->yyid)
            ->where('status', [1, 5, 6])
            ->get();
    }
}
