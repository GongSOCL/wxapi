<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\Company;
use App\Model\Qa\Users;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Collection;

class CompanyRepository
{
    /**
     * @Inject
     * @var Company
     */
    private $companyModel;

    /**
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return LengthAwarePaginatorInterface
     */
    public static function listCompany($keywords = "", $current = 1, $limit = 10): LengthAwarePaginatorInterface
    {
        $query = Company::query()->where(['status' => 1]); // 状态等于1
        if ($keywords) {
            $query->where("name", "like", "%{$keywords}%");
        }

        return $query->paginate($limit, ['*'], '', $current);
    }

    public static function getById($companyId): ?Company
    {
        return Company::where([
            'id' => $companyId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getByIds(array $companyIds): Collection
    {
        return Company::whereIn('id', $companyIds)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    /**
     * @param $name
     */
    public static function getByName($name): ?Company
    {
        return Company::where('name', $name)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }

    public static function addByName($name): Company
    {
        $o = new Company();
        $o->name = $name;
        $o->yyid = Helper::yyidGenerator();
        $o->tel_num ="";
        $o->address = "";
        $o->introduction = "";
        $o->email = "";
        $o->flag = 2;
        $o->status = DataStatus::REGULAR;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');

        if (!$o->save()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "公司创建失败");
        }

        $o->refresh();
        return $o;
    }

    public static function getUserCompany(Users $users)
    {
    }

    public static function getByYYIDS($validCompanyYYIDS): Collection
    {
        return Company::whereIn('yyid', $validCompanyYYIDS)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function searchCompanyWithLimit($keywords = '', $limit = 5)
    {
        return Company::query()
            ->when($keywords, function ($q) use($keywords) {
                $q->where('name', 'like', "%{$keywords}%");
            })->where('status', DataStatus::REGULAR)
            ->limit($limit)
            ->get();
    }

    public function getUid($yyId): array
    {
        return $this->companyModel::all();
    }

    public static function getByYYID($yyid): ?Company
    {
        return Company::where([
            'yyid' => $yyid,
            'status' => DataStatus::REGULAR
        ])->first();
    }
}
