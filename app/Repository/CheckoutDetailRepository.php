<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Qa\DealerCheckoutDetail;
use App\Model\Qa\TDealerDoctorVisitDetail;

class CheckoutDetailRepository
{
    /**
     * @param $companyYyid
     * @param $staffCode
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getCheckoutDetailInfo($companyYyid, $staffCode)
    {
        return DealerCheckoutDetail::where([
            'company_yyid' => $companyYyid,
            'staff_code' => $staffCode
        ])->get();
    }

    /**
     * @param $userId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getLatestMonth($userId)
    {
        return TDealerDoctorVisitDetail::where('user_id', $userId)
            ->groupBy(['month'])
            ->orderBy('id', 'desc')
            ->first();
    }

    /**
     * @param $userId
     * @param $eMonth
     * @return \Hyperf\Utils\Collection
     */
    public static function getDeparts($userId, $eMonth)
    {
        return TDealerDoctorVisitDetail::where('user_id', $userId)
            ->where('month', $eMonth)
            ->groupBy(['department'])
            ->orderBy('id', 'asc')
            ->pluck('department');
    }

    /**
     * @param $userId
     * @param $department
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getDoctorVisitDetailInfo($userId, $department)
    {
        return TDealerDoctorVisitDetail::where([
            'user_id' => $userId,
            'department' => $department
        ])->orderBy('id', 'desc')->first();
    }
}
