<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DealerHospitalGps;
use App\Model\Qa\YouyaoHospital;
use Hyperf\Utils\Collection;

class DealerHospitalGpsRepository
{
    /**
     * @param YouyaoHospital $hospital
     * @return Collection
     */
    public static function getUserHospitalGps(YouyaoHospital $hospital): Collection
    {
        return DealerHospitalGps::where([
                'company_yyid' => $hospital->company_yyid,
                'hospital_yyid' => $hospital->yyid,
                'status'=> DataStatus::REGULAR
            ])->get();
    }

    public static function getGpsByIds(array $ids): Collection
    {
        return DealerHospitalGps::whereIn('id', $ids)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }
}
