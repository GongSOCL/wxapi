<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\RepsFamiliarHospital;
use App\Model\Qa\Users;
use http\Client\Curl\User;
use Hyperf\Utils\Collection;

class RepsFamiliarHospitalRepository
{

    /**
     * 获取用户熟悉的医院
     * @param Users $users
     * @return Collection
     */
    public static function getUserFamiliarHospitals(Users $users): Collection
    {
        return RepsFamiliarHospital::where([
            'rep_yyid' => $users->yyid,
            'status' => DataStatus::REGULAR
        ])->get();
    }

    /**
     * 批量添加用户熟悉的医院
     * @param Users $users
     * @param array $hospitalYYIDS
     */
    public static function batchAddUserFamiliarHospitals(Users $users, array $hospitalYYIDS): bool
    {
        $data = [];
        $now = date('Y-m-d H:i:s');
        foreach ($hospitalYYIDS as $hospitalYYID) {
            $data[] = [
                'rep_yyid' => $users->yyid,
                'hospital_yyid' => $hospitalYYID,
                'status' => DataStatus::REGULAR,
                'created_time' => $now,
                'modify_time' => $now
            ];
        }

        return RepsFamiliarHospital::insert($data);
    }

    public static function batchDelFamiliarHospitals(Users $users, array $hospitalYYIDS)
    {
        return RepsFamiliarHospital::where([
            'rep_yyid' => $users->yyid,
            'status' => DataStatus::REGULAR
        ])->whereIn('hospital_yyid', $hospitalYYIDS)
            ->update([
                'status' => DataStatus::DELETE,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }
}
