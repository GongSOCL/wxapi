<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DealerHospitalC;
use App\Model\Qa\DrugSeries;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Core\ProvinceArea;
use App\Model\Qa\SystemDepart;
use App\Model\Qa\YouyaoHospital;
use Hyperf\Di\Annotation\Inject;

class HospitalRepository
{
    /**
     * @Inject
     * @var YouyaoHospital
     */
    private $hospitalModel;


    /**
     * @param $keyword
     * @param $pyyid
     * @param $cyyid
     * @param $level
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getHospitalInfo($keyword, $pyyid, $cyyid, $level, $page, $pageSize)
    {
        $params = $this->getParams($keyword, $pyyid, $cyyid, $level);
        if (empty($params[1])) {
            return $this->hospitalModel::query()
                ->select('id', 'yyid', 'pro', 'city', 'hospital_name', 'alias_name', 'addr', 'level', 'method', 'url')
                ->where($params[0])
                ->offset($pageSize*($page-1))
                ->limit($pageSize)
                ->get();
        } else {
            return $this->hospitalModel::query()
                ->select('id', 'yyid', 'pro', 'city', 'hospital_name', 'alias_name', 'addr', 'level', 'method', 'url')
                ->where($params[0])
                ->where($params[1])
                ->offset($pageSize*($page-1))
                ->limit($pageSize)
                ->get();
        }
    }


    /**
     * @param $keyword
     * @param $pyyid
     * @param $cyyid
     * @param $level
     * @return int
     */
    public function getHospitalCount($keyword, $pyyid, $cyyid, $level)
    {
        $params = $this->getParams($keyword, $pyyid, $cyyid, $level);
        if (empty($params[1])) {
            return $this->hospitalModel::query()->where($params[0])->count();
        } else {
            return $this->hospitalModel::query()->where($params[0])->where($params[1])->count();
        }
    }


    /**
     * 构筑查询条件
     * @param $keyword
     * @param $pyyid
     * @param $cyyid
     * @param $level
     * @return array[]
     */
    public function getParams($keyword, $pyyid, $cyyid, $level)
    {
        $where = [];
        $map = [];
        $where['status'] = $this->hospitalModel::HOSPITAL_NORMAL;
        if ($pyyid!=='') {
            $where['pro_yyid'] = $pyyid;
        }
        if ($cyyid!=='') {
            $where['city_yyid'] = $cyyid;
        }
        if ($keyword!=='') {
            $map[] = ['hospital_name','like','%'.$keyword.'%'];
        }
        switch ($level) {
            case 1:
                $map[] = ['level','like','一级%'];
                break;
            case 2:
                $map[] = ['level','like','二级%'];
                break;
            case 3:
                $map[] = ['level','like','三级%'];
                break;
        }
        return [$where,$map];
    }

    public static function getHospitalCInfo($hospitalYyid)
    {
        return DealerHospitalC::where([
            'hospital_yyid' => $hospitalYyid
        ])->first();
    }

    public static function getArea($pro)
    {
        return ProvinceArea::where('province', 'like', '%'.substr($pro, 0, 6).'%')->first();
    }

    public static function getAllDepart()
    {
        return SystemDepart::query()
            ->select(['id', 'name'])
            ->where([
                'status' => DataStatus::REGULAR
            ])
            ->get();
    }

    public static function getAllDoctor()
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select(['id', 'true_name'])
            ->get();
    }

    public static function getSeriesInfo()
    {
        return DrugSeries::query()
            ->select(['id', 'series_name'])
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getHospitalById($id)
    {
        return YouyaoHospital::where('id', $id)->first();
    }

    /**
     * @param $yyId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getHospitalByYyId($yyId)
    {
        return YouyaoHospital::where('yyid', $yyId)->first();
    }
}
