<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Helper\Helper;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Qa\NewDoctorUserinfo;
use Hyperf\Utils\Collection;

class DoctorOpenidUserinfoRepository
{

    public static function getWechatDoctorWithMobile($mobile, $excludeId = 0): Collection
    {
        $loginUser = Helper::getLoginUser();
        $query = NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'mobile_num' =>  $mobile,
                'status' => DataStatus::REGULAR,
                'user_id' => $loginUser->uid
            ])->whereRaw("(openid = '' or openid is null)");

        if ($excludeId) {
            $query = $query->where("id", '!=', $excludeId);
        }
        return $query->get();
    }


    /**
     * 通过yyid获取openid
     * @param $num
     * @return object
     */
    public function getPreInfo($num)
    {
        return NewDoctorUserinfo::query()
            ->select(
                'true_name',
                'gender',
                'hospital_yyid',
                'hospital_name',
                'depart_id',
                'depart_name',
                'job_title',
                'position',
                'field_id'
            )
            ->where(
                [
                    'mobile_num' => $num,
                //                    'status' => DataStatus::REGULAR
                ]
            )
            ->first();
    }


    /**
     * 查找用户信息
     *
     * @param $openid
     * @param int $wechatType
     * @return NewDoctorUserinfo|null
     */
    public static function getDoctorInfo($openid, $wechatType = DataStatus::WECHAT_DOCTOR): NewDoctorUserinfo
    {
        return NewDoctorUserinfo::where([
                'openid' => $openid,
                'wechat_type' => $wechatType
            ])
//            ->orderByDesc('id')
            ->first();
    }

    /**
     * 添加医生信息
     *
     * @param $wxinfo
     * @param int $wechatType
     */
    public static function addDoctorOpenidUserInfo($wxinfo, $wechatType = DataStatus::WECHAT_AGENT)
    {
        return NewDoctorUserinfo::insertGetId([
            'wechat_type' => $wechatType,
            'openid' => $wxinfo['openid'],
            'unionid' => $wxinfo['unionid'],
            'nickname' => array_key_exists('nickname', $wxinfo) ? $wxinfo['nickname'] : '',
            'headimgurl' => array_key_exists('headimgurl', $wxinfo) ? $wxinfo['headimgurl'] : '',
            'gender' => $wxinfo['sex'],
            'source_type' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getDoctorReps($uid, $newid)
    {
        return NewDoctorOpenidUserinfo::where([
            'user_id' => $uid,
            'new_doctor_id' => $newid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function addDoctorReps($uid, $newid)
    {
        NewDoctorOpenidUserinfo::insert([
            'user_id' => $uid,
            'new_doctor_id' => $newid,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function findUpdateOrAddDoctorWechat($uid, $wxinfo, $wechatType = DataStatus::WECHAT_AGENT)
    {
        $doctorInfo = self::getDoctorInfo($wxinfo['openid'], $wechatType);
        if ($doctorInfo) {
            if ($wxinfo['nickname']) {
                $doctorInfo->nickname = $wxinfo['nickname'];
            }
            if ($wxinfo['sex']) {
                $doctorInfo->gender = $wxinfo['sex'];
            }
            if ($wxinfo['headimgurl']) {
                $doctorInfo->headimgurl = $wxinfo['headimgurl'];
            }
            $doctorInfo->save();
            $newid = $doctorInfo->id;
        } else {
            $newid = self::addDoctorOpenidUserInfo($wxinfo, $wechatType);
        }
        $reps = self::getDoctorReps($uid, $newid);
        if (!$reps) {
            self::addDoctorReps($uid, $newid);
        }
    }



    public static function getInfoById($id): ?NewDoctorUserinfo
    {
        $resp = NewDoctorOpenidUserinfo::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();

        return NewDoctorUserinfo::where('nid', $resp->new_doctor_id)->first();
    }

    /**
     * @param $id
     * @return bool|int
     */
    public static function deleteInfoById($id)
    {
        return NewDoctorUserinfo::query()
            ->where([
            'nid' => $id
        ])->delete();
    }

    public static function deleteDoctorResp($id)
    {
        NewDoctorOpenidUserinfo::where('id', $id)
            ->update([
                'status' => DataStatus::DELETE,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * @param $mobile
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getDoctorByMobile($mobile)
    {
        return NewDoctorUserinfo::where('mobile_num', $mobile)->get();
    }

    public static function getDoctorByHospital($hospitalYyid, $departId, $name)
    {
        return NewDoctorUserinfo::where([
            'hospital_yyid' => $hospitalYyid,
            'depart_id' => $departId,
            'true_name' => $name
        ])->get();
    }
}
