<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\Company;
use App\Model\Qa\DepartmentImport;

class DepartmentImportRepository
{
    public static function getLastActiveImport(Company $company): ?DepartmentImport
    {
        return DepartmentImport::query()
            ->where('company_id', $company->id)
            ->where('is_active', DataStatus::REGULAR)
            ->first();
    }
}
