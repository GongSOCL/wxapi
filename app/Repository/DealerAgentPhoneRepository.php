<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DealerAgentPhone;

class DealerAgentPhoneRepository
{
    /**
     * @param $companyYyid
     * @param $phone
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getStaffInfoByPhone($companyYyid, $phone)
    {
        return DealerAgentPhone::where([
            'company_yyid' => $companyYyid,
            'phone_number' => '+86-'.$phone,
            'status' => DataStatus::REGULAR
        ])->first();
    }
}
