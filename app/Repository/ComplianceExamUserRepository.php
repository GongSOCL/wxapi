<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ComplianceExamUser;
use App\Model\Qa\Users;
use Hyperf\Database\Model\Builder;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class ComplianceExamUserRepository
{

    /**
     * 获取用户考试yyid列表
     * @param Users $user
     * @param array $productYYIDS
     * @return Collection
     */
    public static function getUserExams(Users $user, array $productYYIDS = []): Collection
    {
        $query = ComplianceExamUser::query()
            ->where('status', DataStatus::REGULAR);
        if (empty($productYYIDS)) {
            $query->where('user_yyid', $user->yyid);
        } else {
            $query->where(function ($query) use ($user, $productYYIDS) {
                /** @var Builder $query */
                $query = $query->where('user_yyid', $user->yyid);
                if (!empty($productYYIDS)) {
                    $query->orWhereIn('product_yyid', $productYYIDS);
                }
                return $query;
            });
        }

        return $query->select([Db::raw("distinct exam_yyid")])
            ->get();
    }

    /**
     * 检查用户是否分配了考试
     * @param Users $user
     * @param ComplianceExam $exam
     * @param array $productYYIDS
     * @return bool
     */
    public static function checkUserExam(Users $user, ComplianceExam $exam, array $productYYIDS = []): bool
    {
        return ComplianceExamUser::query()
            ->where([
               'exam_yyid' => $exam->yyid,
               'status' => DataStatus::REGULAR
            ])->where(function ($query) use ($user, $productYYIDS) {
                /** @var Builder $query */
                $query = $query->where('user_yyid', $user->yyid);
                if (!empty($productYYIDS)) {
                    $query->orWhereIn('product_yyid', $productYYIDS);
                }
                return $query;
            })->exists();
    }
}
