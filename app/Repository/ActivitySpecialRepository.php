<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\Activity;
use App\Model\Qa\ActivitySpecial;
use Hyperf\DbConnection\Db;

class ActivitySpecialRepository
{
    public static function getSpecialFromActivityAndSpecialId(Activity $activity, $specialId, $tableName)
    {
        return ActivitySpecial::where([
            'activity_id' => $activity->id,
            's_value' => $specialId,
            'event_key' => $activity->event_key,
            's_table_name' => $tableName,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * 添加特殊积分事件
     *
     * @param Activity $activity
     * @param $specialId
     * @param $name
     * @param $sType
     * @param $tableName
     * @param $startTime
     * @param string $endTime
     * @param float $cash
     * @param int $points
     * @param int $numPreDay
     * @param int $numTotal
     * @param int $createUserId
     * @param string $desc
     * @return ActivitySpecial
     */
    public static function addActivitySpecial(
        Activity $activity,
        $specialId,
        $name,
        $sType,
        $tableName,
        $startTime,
        $endTime = '',
        $cash = 0.00,
        $points = 0,
        $numPreDay = 0,
        $numTotal = 0,
        $createUserId = 0,
        $desc = ''
    ): ActivitySpecial {
        $o = new ActivitySpecial();
        $o->activity_id = $activity->id;
        $o->name = $name;
        $o->event_key = $activity->event_key;
        $o->s_type = $sType;
        $o->s_value = $specialId;
        $o->s_table_name = $tableName;
        $o->start_time = $startTime;
        $o->end_time = $endTime ? $endTime : null;
        $o->money_point = $cash;
        $o->service_point = $points;
        $o->num_pre_day = $numPreDay;
        $o->num_total = $numTotal;
        $o->created_user_id = $createUserId;
        $o->status = DataStatus::REGULAR;
        $o->point_desc = $desc;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');
        $o->save();

        return $o;
    }
}
