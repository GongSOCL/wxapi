<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Qa\DrugProduct;
use App\Model\Qa\HospitalMonthTransP;
use App\Model\Qa\YouyaoHospital;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class HospitalMonthTransPRepository
{
    public static function getMonthTopByHospitalAndProduct(YouyaoHospital $hospital, DrugProduct $product)
    {
        return HospitalMonthTransP::where([
            'hospital_yyid' => $hospital->yyid,
            'product_yyid' => $product->yyid
        ])->select([
            'product_yyid',
            Db::raw("max(month) as max_month"),
            Db::raw("max(num) as max_num")
        ])->first();
    }

    /**
     * 根据医院和药品统计药品最后一月及最大销量数量
     * @param array $hospitalYYID
     * @param array $productYYID
     * @return HospitalMonthTransP|\Hyperf\Database\Model\Model|\Hyperf\Database\Query\Builder|object|null
     */
    public static function getByProductAndHospitals(DrugProduct $product, array $hospitalYYID)
    {
        return HospitalMonthTransP::where('product_yyid', $product->yyid)
        ->whereIn('hospital_yyid', $hospitalYYID)
        ->select([
            'product_yyid',
            Db::raw("max(month) as max_month"),
            Db::raw("max(num) as max_num")
        ])->first();
    }

    /**
     * 获取最大月份数据
     * @param YouyaoHospital $hospital
     * @param array $productYYID
     */
    public static function getMonthTopByHospitalAndProducts(YouyaoHospital $hospital, array $productYYID): Collection
    {
        return HospitalMonthTransP::where([
            'hospital_yyid' => $hospital->yyid,
        ])->whereIn('product_yyid', $productYYID)
            ->select([
                'product_yyid',
                Db::raw("max(month) as max_month"),
                Db::raw("max(num) as max_num")
            ])
            ->groupBy('product_yyid')
            ->get();
    }

    /**
     * 获取某个时间点以来平均销售及总销售
     * @param YouyaoHospital $hospital
     * @param DrugProduct $productItem
     * @param string $lastYear
     * @return HospitalMonthTransP|\Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|\Hyperf\Database\Query\Builder|object|null
     */
    public static function getAfterHospitalProductSumAndAvg(
        YouyaoHospital $hospital,
        DrugProduct $productItem,
        string $lastYear
    ) {
        return HospitalMonthTransP::where([
            'hospital_yyid' => $hospital->yyid,
            'product_yyid' => $productItem->yyid
        ])->where('month', '>', $lastYear)
            ->select([
                Db::raw('avg(num) as avg_num'),
                Db::raw('sum(num) as sum_num')
            ])->first();
    }

    /**
     * @param YouyaoHospital $hospital
     * @param DrugProduct $productItem
     * @param array $date
     * @return Collection
     */
    public static function getMonthRangeDataByHospitalAndProduct(
        YouyaoHospital $hospital,
        DrugProduct $productItem,
        array $date
    ): Collection {
        return HospitalMonthTransP::where([
            'hospital_yyid' => $hospital->yyid,
            'product_yyid' => $productItem->yyid
        ])->whereIn('month', $date)
            ->get();
    }

    /**
     * @param YouyaoHospital $hospital
     * @param DrugProduct $productItem
     * @param array $date
     * @return Collection
     */
    public static function getMonthRangeDataByHospitalsAndProduct(
        array $hospitalYYIDS,
        DrugProduct $productItem,
        array $date
    ): Collection {
        return HospitalMonthTransP::where([
            'product_yyid' => $productItem->yyid
        ])->whereIn('hospital_yyid', $hospitalYYIDS)
            ->whereIn('month', $date)
            ->select(['month', Db::raw('sum(num) as num')])
            ->groupBy('month')
            ->get();
    }

    /**
     * @param array $hospitalYYIDS
     * @param DrugProduct $product
     * @param string $month
     * @return Collection
     */
    public static function getDataByMonthProductAndHospitals(
        array $hospitalYYIDS,
        DrugProduct $product,
        string $month
    ): Collection {
        return HospitalMonthTransP::where([
            'product_yyid' => $product->yyid,
            'month' => $month
        ])->whereIn('hospital_yyid', $hospitalYYIDS)
            ->get();
    }

    /**
     * 获取最近的n条记录
     * @param YouyaoHospital $hospital
     * @param DrugProduct $product
     * @param int $limit
     * @return Collection
     */
    public static function getLastServeRecord(YouyaoHospital $hospital, DrugProduct $product, int $limit): Collection
    {
        return HospitalMonthTransP::where([
            'hospital_yyid' => $hospital->yyid,
            'product_yyid' => $product->yyid
        ])->orderByDesc('month')
            ->limit($limit)
            ->get();
    }

    public static function getTotalByHospitalAndProducts(YouyaoHospital $hospital, DrugProduct $product, array $dates)
    {
        return HospitalMonthTransP::where([
            'hospital_yyid' => $hospital->yyid,
            'product_yyid' => $product->yyid
        ])->whereIn('month', $dates)
            ->select([Db::raw('sum(num) as total')])
            ->first();
    }

    public static function getLastServeHospitalsProductRecord(array $hospitalYYIDS, $product, int $limit)
    {
        return HospitalMonthTransP::where([
            'product_yyid' => $product->yyid
        ])->whereIn('hospital_yyid', $hospitalYYIDS)
            ->select(["month", Db::raw("sum(num) as num")])
            ->groupBy(["month"])
            ->orderByDesc('month')
            ->limit($limit)
            ->get();
    }

    public static function getTotalByHospitalsAndProducts(array $hospitalYYIDS, DrugProduct $product, array $dates)
    {
        return HospitalMonthTransP::where([
            'product_yyid' => $product->yyid
        ])->whereIn('hospital_yyid', $hospitalYYIDS)
            ->whereIn('month', $dates)
            ->select([Db::raw('sum(num) as total')])
            ->first();
    }

    public static function getHospitalsByRanks(
        DrugProduct $product,
        array $hospitalYYIDS,
        $month,
        $current,
        $limit
    ): LengthAwarePaginatorInterface {
        return HospitalMonthTransP::where('product_yyid', $product->yyid)
            ->where('month', $month)
            ->whereIn('hospital_yyid', $hospitalYYIDS)
            ->select(['hospital_yyid', Db::raw("sum(num) as sum")])
            ->groupBy('hospital_yyid')
            ->orderByDesc('sum')
            ->paginate($limit, ['*'], '', $current);
    }
}
