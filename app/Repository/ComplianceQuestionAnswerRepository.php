<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ComplianceQuestionAnswer;

class ComplianceQuestionAnswerRepository
{

    public static function getByQuestionYYIDS(array $questionIds)
    {
        return ComplianceQuestionAnswer::whereIn('question_item_yyid', $questionIds)
            ->where('status', DataStatus::REGULAR)
            ->orderBy('question_item_yyid')
            ->get();
    }
}
