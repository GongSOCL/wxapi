<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DrugProduct;
use App\Model\Qa\RepsServeScopeP;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;
use Hyperf\Utils\Collection;
use InvalidArgumentException;
use RuntimeException;

class RepsServeScopePRepository
{
    /**
     * 获取用户医院下服务的所有产品
     * @param Users $user
     * @param YouyaoHospital $hospital
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getByUserAndHospital(Users $user, YouyaoHospital $hospital)
    {
        return RepsServeScopeP::query()
            ->join("t_reps_serve_scope_s as trss", function ($join) {
                $join->on("trss.series_yyid", "=", "t_reps_serve_scope_p.series_yyid")
                ->on("trss.user_yyid", "=", "t_reps_serve_scope_p.user_yyid")
                ->on("trss.hospital_yyid", "=", "t_reps_serve_scope_p.hospital_yyid")
                ->on('trss.status', '=', 't_reps_serve_scope_p.status');
            })->select(["t_reps_serve_scope_p.*"])
            ->where([
                "t_reps_serve_scope_p.user_yyid" => $user->yyid,
                't_reps_serve_scope_p.hospital_yyid' => $hospital->yyid
            ])->whereIn("trss.status", [0, 1, 5, 6])
            ->groupBy(["t_reps_serve_scope_p.product_yyid"])
            ->get();
    }

    public static function getById(int $id): ?RepsServeScopeP
    {
        return RepsServeScopeP::where('id', $id)
            ->first();
    }

    public static function getUserServeProducts(Users $users): Collection
    {
        return RepsServeScopeP::query()
            ->join('t_drug_product as tdp', 'tdp.yyid', '=', 't_reps_serve_scope_p.product_yyid')
            ->where([
                't_reps_serve_scope_p.user_yyid' => $users->yyid,
                'tdp.status' => DataStatus::REGULAR,
            ])->whereIn('t_reps_serve_scope_p.status', [1, 5])
            ->select(['tdp.*'])
            ->get();
    }

    public static function getUserServeList(Users $user, DrugProduct $product = null): Collection
    {
        $where = [
            'user_yyid' => $user->yyid
        ];
        if ($product) {
            $where['product_yyid'] = $product->yyid;
        }

        return RepsServeScopeP::where($where)
            ->whereIn('status', [1, 5, 6])
            ->get();
    }

    /**
     * 检查用户是否服务某产品
     * @param Users $user
     * @param DrugProduct $product
     * @param YouyaoHospital|null $hospital
     * @return bool
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public static function checkUserServeProduct(
        Users $user,
        DrugProduct $product,
        YouyaoHospital $hospital = null
    ): bool {
        $query = RepsServeScopeP::where([
            'user_yyid' => $user->yyid,
            'product_yyid' => $product->yyid
        ])->whereIn('status', [1, 5, 6]);
        if ($hospital) {
            $query = $query->where('hospital_yyid', $hospital->yyid);
        }

        return $query->exists();
    }

    public static function checkUserServeHospitals(Users $user, YouyaoHospital $hospital): bool
    {
        return RepsServeScopeP::where([
            'user_yyid' => $user->yyid,
            'hospital_yyid' => $hospital->yyid
        ])->whereIn('status', [1, 5, 6])
        ->exists();
    }
}
