<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\UserAsset;
use Grpc\Point\UserType;
use Hyperf\Di\Annotation\Inject;

class UserAssetRepository
{
    /**
     * @Inject
     * @var UserAsset
     */
    private $userAssetModel;

    /**
     * 通过uid获取用户的消费积分值和服务积分值
     * @param $uid
     * @return object
     */
    public function getloginInfo($uid, $isAgent = true)
    {
        $userType = $isAgent ? UserType::USER_TYPE_AGENT : UserType::USER_TYPE_DOCTOR;
        return $this->userAssetModel::query()
            ->select('money_point', 'service_point')
            ->where('uid', $uid)
            ->where('user_role_type', $userType)
            ->first();
    }
}
