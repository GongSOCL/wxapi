<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Repository\Login;

use App\Model\Qa\TVersionForCheck;

class LoginForVersionRepository
{
    /**
     * @param $version
     * @param $channel
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getCheckStatus($version, $channel)
    {
        return TVersionForCheck::where('app_version', $version)
            ->where('channel', 'like', '%' . $channel . '%')->first();
    }

    /**
     * @param $phone
     * @param $password
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function loginForCheck($phone, $password)
    {
        return TVersionForCheck::where([
            'phone' => $phone,
            'psd' => $password
        ])->first();
    }

    /**
     * @param $phone
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getPwdByPhone($phone)
    {
        return TVersionForCheck::where([
            'phone' => $phone,
        ])->first();
    }
}
