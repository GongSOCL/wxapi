<?php
declare(strict_types=1);

namespace App\Repository\Login;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Kernel\Uuid\Uuid;
use App\Model\Qa\UserBindIosLog;
use App\Model\Qa\UserBindPhoneLog;
use App\Model\Qa\UserBindWechatLog;
use App\Model\Qa\UserLoginLog;
use App\Model\Qa\Users;
use App\Model\Qa\WechatUser;
use Hyperf\Utils\ApplicationContext;

class LoginRepository
{
    /**
     * @param $mobile
     * @return Users|null
     */
    public static function getUserByPhone($mobile)
    {
        return Users::where([
            'mobile_num' => $mobile,
            'v_status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getUserByUnionid($unionid, $wechatType)
    {
        return WechatUser::where('unionid', $unionid)
            ->where('wechat_type', $wechatType)
            ->first();
    }

    public static function getUsersByUnionid($unionid)
    {
        return WechatUser::where('unionid', $unionid)
            ->get();
    }

    public static function getUsersByUnionidWithoutNull($unionid)
    {
        return WechatUser::where('unionid', $unionid)
            ->whereIn('wechat_type', [DataStatus::WECHAT_AGENT, DataStatus::WECHAT_APP])
            ->whereRaw("(u_yyid != '' or u_yyid is not null) ")
            ->orderBy('id', 'desc')
            ->limit(1)
            ->first();
    }

    public static function updateUserLoginInfo($userId, $ip, $platform, $isFirst)
    {
        return UserLoginLog::insertGetId([
            'user_id' => $userId,
            'platform' => $platform,
            'login_time' => date('Y-m-d H:i:s', time()),
            'login_ip' => $ip,
            'is_first_login' => $isFirst,
            'created_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    public static function createAccount($mobile, $user = null)
    {
        $yyid = ApplicationContext::getContainer()
            ->get(Uuid::class)
            ->generate();
        $nowTime = time();
        $now = date('Y-m-d H:i:s');
        $o = new Users();
        $o->yyid = $yyid;
        $o->mobile_num = $mobile;
        $o->mobile_verified = $mobile ? DataStatus::REGULAR : DataStatus::DELETE;
        $o->name = $user && $user->nickname ? $user->nickname : '用户_'.Helper::getRandomString(8);
        $o->pass = "";
        $o->mail = "";
        $o->mail_verified = DataStatus::DELETE;
        $o->company_yyid = "";
        $o->region_yyid = "";
        $o->wechat = "";
        $o->weibo = "";
        $o->tengqq = "";
        $o->tel_num = $mobile;
        $o->access = time();
        $o->login = time();
        $o->picture = "";
        $o->v_status = DataStatus::REGULAR;
        $o->event = "";
        $o->v_date = $nowTime;
        $o->user_type = DataStatus::REGULAR;
        $o->is_doctor = DataStatus::DELETE;
        $o->is_agent = DataStatus::DELETE;
        $o->client_ip = "";
        $o->last_client_ip = "";
        $o->created = $nowTime;
        $o->update_date = $now;

        if (!$o->save()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户添加失败");
        }

        return $o;
    }

    public static function updateWechatBindState($id, $state)
    {
        Users::where('uid', $id)->update([
            'is_bind_wechat' => $state,
            'update_date' => date('Y-m-d H:i:s', time())
        ]);
    }

    public static function updateIosBindState($id, $state)
    {
        Users::where('uid', $id)->update([
            'is_bind_ios' => $state,
            'update_date' => date('Y-m-d H:i:s', time())
        ]);
    }

    public static function bindPhoneToUser($id, $mobile)
    {
        return Users::where('uid', $id)->update([
            'mobile_num' => $mobile,
            'mobile_verified' => DataStatus::REGULAR,
            'update_date' => date('Y-m-d H:i:s', time())
        ]);
    }

    public static function bindPhoneHistory($id, $mobile)
    {
        UserBindPhoneLog::insertGetId([
            'user_id' => $id,
            'phone' => $mobile,
            'created_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    public static function getByYyid($yyid)
    {
        return WechatUser::where('u_yyid', $yyid)
            ->get();
    }

    public static function removeBindUserYyid($ids)
    {
        WechatUser::whereIn('id', $ids)->update([
            'u_yyid' => ''
        ]);
    }

    public static function bindWechatHistory($id, $unionid, $state)
    {
        UserBindWechatLog::insertGetId([
            'user_id' => $id,
            'unionid' => $unionid,
            'status' => $state,
            'created_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    public static function bindIosHistory($id, $iosid, $state)
    {
        UserBindIosLog::insertGetId([
            'user_id' => $id,
            'ios_id' => $iosid,
            'status' => $state,
            'created_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    public static function getOneWechatInfoByUnionId($unionId, $wechatType)
    {
        return WechatUser::where('unionid', $unionId)
            ->where('wechat_type', $wechatType)
            ->whereRaw("u_yyid != ''")
            ->whereRaw("u_yyid is not null")
            ->orderBy('id', 'desc')
            ->limit(1)
            ->first();
    }
}
