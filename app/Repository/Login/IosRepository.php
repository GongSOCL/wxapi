<?php
declare(strict_types=1);

namespace App\Repository\Login;

use App\Model\Qa\UserIosInfo;

class IosRepository
{
    public static function getUserByIosid($iosid)
    {
        return UserIosInfo::where('ios_id', $iosid)->first();
    }

    public static function getUserByUserId($userid)
    {
        return UserIosInfo::where('user_id', $userid)->first();
    }

    public static function createIosUser($iosid, $familyName, $userid = 0)
    {
        return UserIosInfo::insertGetId([
            'user_id' => $userid,
            'ios_id' => $iosid,
            'familyName' => $familyName,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function updateInfoByIosid($iosid, $familyName)
    {
        return UserIosInfo::where('ios_id', $iosid)->update([
            'familyName' => $familyName,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function updateUserIdByIosid($userid, $iosid)
    {
        UserIosInfo::where('ios_id', $iosid)->update([
            'user_id' => $userid,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function removeBindUserId($id)
    {
        UserIosInfo::where('id', $id)->update([
            'user_id' => 0
        ]);
    }

    public static function removeBindUserIdByUserId($userid)
    {
        UserIosInfo::where('user_id', $userid)->update([
            'user_id' => 0
        ]);
    }
}
