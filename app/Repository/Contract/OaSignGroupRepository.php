<?php
declare(strict_types=1);

namespace App\Repository\Contract;

use App\Constants\DataStatus;
use App\Model\Qa\OaSignGroup;
use App\Model\Qa\OaUserSign;
use App\Model\Qa\Users;
use Hyperf\Contract\LengthAwarePaginatorInterface;

class OaSignGroupRepository
{

    public static function getGroupById(int $id): ?OaSignGroup
    {
        return OaSignGroup::query()
            ->where('id', $id)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }

    public static function getRestToGenerateHtml(OaSignGroup $group): int
    {
        return $group->contracts()->where('state', OaUserSign::STATE_TO_GENERATE_HTML)->count();
    }

    public static function getRestToGeneratePdf(OaSignGroup $group): int
    {
        return $group->contracts()->where('state', OaUserSign::STATE_SIGNED)->count();
    }

    public static function htmlGenerateFinished(OaSignGroup $group)
    {
        return OaSignGroup::where('id', $group->id)
            ->where('state', OaSignGroup::STATE_TO_GENERATE_HTML)
            ->update([
                'state' => OaSignGroup::STATE_TO_SIGN,
                'updatetime' => date('Y-m-d H:i:s')
            ]);
    }

    public static function pdfGenerateFinished(OaSignGroup $group)
    {
        return OaSignGroup::where('id', $group->id)
            ->where('state', OaSignGroup::STATE_SIGNED)
            ->update([
                'state' => OaSignGroup::STATE_PDF_GENERATED,
                'updatetime' => date('Y-m-d H:i:s')
            ]);
    }

    public static function markSigned(OaSignGroup $group): int
    {
        return OaSignGroup::query()
            ->where('id', $group->id)
            ->where('state', OaSignGroup::STATE_TO_SIGN)
            ->update([
                'state' => OaSignGroup::STATE_SIGNED,
                'updatetime' => date('Y-m-d H:i:s')
            ]);
    }

    public static function getContractByStateWithGroup(
        Users $users,
        $state = 0,
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        $query = OaSignGroup::query()
            ->where('user_id', $users->uid)
            ->where('status', DataStatus::REGULAR);
        switch ($state) {
            case 0:
                //未签署
                $query->where('state', OaSignGroup::STATE_TO_SIGN);
                break;
            case 1:
                //已生效
                $query = $query->whereIn('state', [
                    OaSignGroup::STATE_PDF_GENERATED,
                    OaSignGroup::STATE_SIGNED
                ]);
                break;
            case 2:
                //已过期
                $query = $query->where('sign_end', '<', date('Y-m-d'));
                break;
        }

        return $query->orderBy('id')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function countToSigned(Users $user): int
    {
        return OaSignGroup::where([
            'user_id' => $user->uid,
            'status' => DataStatus::REGULAR,
            'state' => OaSignGroup::STATE_TO_SIGN
        ])->count();
    }

    public static function addOne($title, $userId, $start, $end, $signEnd = ""): OaSignGroup
    {
        $o = new OaSignGroup();
        $o->title = $title;
        $o->user_id = $userId;
        $o->start = $start;
        $o->end = $end;
        $o->sign_end = $signEnd ? $signEnd : $end;
        $o->state = OaSignGroup::STATE_TO_GENERATE_HTML;
        $o->createtime = $o->updatetime = $o->deletetime = date('Y-m-d H:i:s');
        $o->save();
        return $o;
    }
}
