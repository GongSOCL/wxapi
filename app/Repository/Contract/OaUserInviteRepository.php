<?php
declare(strict_types=1);
namespace App\Repository\Contract;

use App\Model\Qa\Company;
use App\Model\Qa\OaUserInvite;

class OaUserInviteRepository
{

    public static function getUserInfo(int $user_id, int $company_id): ?OaUserInvite
    {
        return OaUserInvite::query()
            ->where('user_id', $user_id)
            ->where('company_id', $company_id)
            ->where('status', '2')
            ->orderByDesc('id')
            ->first();
    }

    public static function updateUserCompany(OaUserInvite $inviteInfo, Company $company)
    {
        OaUserInvite::query()
            ->where('id', $inviteInfo->id)
            ->update([
                'company_id' => $company->id,
                'updatetime' => time()
            ]);
    }
}
