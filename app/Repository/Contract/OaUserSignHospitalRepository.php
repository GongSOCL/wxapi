<?php
declare(strict_types=1);
namespace App\Repository\Contract;

use App\Constants\DataStatus;
use App\Model\Qa\OaUserSign;
use App\Model\Qa\OaUserSignHospital;
use Hyperf\Utils\Collection;

class OaUserSignHospitalRepository
{

    /**
     * @param OaUserSign $contract
     * @return Collection
     */
    public static function getSignRecords(OaUserSign $contract): Collection
    {
        return OaUserSignHospital::query()
            ->where('user_sign_id', $contract->id)
            ->get();
    }

    /**
     * 批量添加
     * @param array $repsServes
     * @return bool
     */
    public static function batchAdd(array $repsServes): bool
    {
        return OaUserSignHospital::insert($repsServes);
    }
}
