<?php
declare(strict_types=1);

namespace App\Repository\Contract;

use App\Constants\DataStatus;
use App\Model\Qa\OaSignTemplate;

class OaSignTemplateRepository
{

    public static function getById(int $id): ?OaSignTemplate
    {
        return OaSignTemplate::query()
            ->where('id', $id)
            ->first();
    }

    public static function getByAlias($alias): ?OaSignTemplate
    {
        return OaSignTemplate::query()
            ->where('tpl_alias', $alias)
            ->first();
    }
}
