<?php
declare(strict_types=1);
namespace App\Repository\Contract;

use App\Constants\DataStatus;
use App\Model\Qa\OaUserSign;
use App\Model\Qa\OaUserSignHospital;
use App\Model\Qa\OaUserSignHospitalTarget;
use Hyperf\Utils\Collection;

class OaUserSignHospitalTargetRepository
{

    public static function batchAdd(array $hospitalTargets): bool
    {
        return OaUserSignHospitalTarget::insert($hospitalTargets);
    }

    public static function getByContract(OaUserSign $contract): Collection
    {
        return OaUserSignHospital::query()
            ->join(
                'oa_user_sign_hospital_target as ousht',
                'ousht.sign_hospital_yyid',
                '=',
                'oa_user_sign_hospital.yyid'
            )
            ->where('user_sign_id', $contract->id)
                ->where('ousht.status', DataStatus::REGULAR)
            ->select(['ousht.*'])
            ->get();
    }
}
