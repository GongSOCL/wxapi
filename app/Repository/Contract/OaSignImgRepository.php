<?php
declare(strict_types=1);

namespace App\Repository\Contract;

use App\Constants\DataStatus;
use App\Model\Qa\OaSignGroup;
use App\Model\Qa\OaSignImg;

class OaSignImgRepository
{

    public static function findUpdateOrAdd(OaSignGroup $group, string $link, $type = OaSignImg::TYPE_NAME): OaSignImg
    {
        /** @var OaSignImg $img */
        $img = OaSignImg::query()
            ->where('sign_group_id', $group->id)
            ->where('img_type', $type)
            ->first();
        if (!$img) {
            return self::addImg($group, $link, $type);
        } else {
            $img->img_link = $link;
            $img->file_hash = md5($link);
            $img->updatetime = date('Y-m-d H:i:s');
            $img->save();
            $img->refresh();
            return $img;
        }
    }

    public static function addImg(OaSignGroup $group, string $link, $type = OaSignImg::TYPE_NAME): OaSignImg
    {
        $o = new OaSignImg();
        $o->sign_group_id = $group->id;
        $o->user_id = $group->user_id;
        $o->img_type = $type;
        $o->img_link = $link;
        $o->file_hash = md5($link);
        $o->createtime = $o->updatetime = $o->deletetime = date('Y-m-d H:i:s');
        $o->save();
        return $o;
    }

    public static function getGroupLink($groupId, $type = OaSignImg::TYPE_NAME): ?OaSignImg
    {
        return OaSignImg::query()
            ->where('sign_group_id', $groupId)
            ->where('img_type', $type)
            ->orderByDesc('id')
            ->first();
    }
}
