<?php
declare(strict_types=1);
namespace App\Repository\Contract;

use App\Constants\DataStatus;
use App\Model\Qa\OaUserSign;
use App\Model\Qa\OaYouyaoAttachmentContract;

class OaYouyaoAttachmentContractRepository
{
    public static function getContractAttachmentInfo(OaUserSign $contract): ?OaYouyaoAttachmentContract
    {
        return OaYouyaoAttachmentContract::query()
            ->where('sign_id', $contract->id)
            ->orderByDesc('id')
            ->first();
    }

    public static function addOne(
        OaUserSign $contract,
        $percent,
        $feeUnit,
        $settlementDate = ""
    ): OaYouyaoAttachmentContract {
        $o = new OaYouyaoAttachmentContract();
        $o->sign_id = $contract->id;
        $o->startdate = $contract->startdate;
        $o->enddate = $contract->enddate;
        if ($settlementDate) {
            $o->settlement_date =$settlementDate;
        } else {
            $o->settlement_date = $contract->enddate;
        }
        $o->fee_unit = $feeUnit;
        $o->percent = $percent;
        $o->createtime = $o->updatetime = date('Y-m-d H:i:s');
        $o->save();

        return $o;
    }
}
