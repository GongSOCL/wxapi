<?php
declare(strict_types=1);
namespace App\Repository\Contract;

use App\Constants\DataStatus;
use App\Model\Qa\Company;
use App\Model\Qa\OaSignGroup;
use App\Model\Qa\OaSignTemplate;
use App\Model\Qa\OaUserSign;
use App\Model\Qa\Users;
use Hyperf\Utils\Collection;

class OaUserSignRepository
{

    public static function markGroupSigned(OaSignGroup $group): int
    {
        return OaUserSign::query()
            ->where('group_id', $group->id)
            ->where('state', OaUserSign::STATE_TO_SIGN)
            ->update([
                'state' => OaUserSign::STATE_SIGNED,
                'signdate' => date('Y-m-d'),
                'updatetime' => time()
            ]);
    }

    public static function getById($id): ?OaUserSign
    {
        return OaUserSign::query()
            ->where('id', $id)
            ->first();
    }

    public static function getByGroupIds(array $groupIds): Collection
    {
        return OaUserSign::query()
            ->whereIn('group_id', $groupIds)
            ->orderBy('id')
            ->get();
    }

    public static function getGroupNextSign(Users $user, OaSignGroup $group, int $currentId = 0): ?OaUserSign
    {
        return OaUserSign::query()
            ->where('user_id', $user->uid)
            ->where('group_id', $group->id)
            ->where('id', ">", $currentId)
            ->orderBy('id')
            ->first();
    }

    public static function addOne(
        OaSignGroup $group,
        OaSignTemplate $template,
        Company $userCompany,
        $title,
        $contractNo,
        $type
    ): OaUserSign {
        $o = new OaUserSign();
        $o->user_id = $group->user_id;
        $o->title = $title;
        $o->contractno = $contractNo;
        $o->group_id = $group->id;
        $o->startdate = $group->start;
        $o->enddate = $group->end;
        $o->type = $type;
        $o->template_id = $template->id;
        $o->admin_id = 0;
        $o->company_id = $userCompany->id;
        $o->link = $o->pdf_link = "";
        $o->state = OaUserSign::STATE_TO_GENERATE_HTML;
        $o->createtime = $o->updatetime = $o->deletetime = time();
        $o->save();

        return $o;
    }
}
