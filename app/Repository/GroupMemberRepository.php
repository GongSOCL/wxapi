<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\Group;
use App\Model\Qa\GroupMember;
use App\Constants\DataStatus;
use Hyperf\Database\Query\Expression;

class GroupMemberRepository
{

    /**
     * 获取用户某一类型微信好友
     * @param $userYyid
     * @param int $followerType
     */
    public static function getUserFollower($userYyid, $followerType = DataStatus::USER_TYPE_DOCTOR)
    {
        return GroupMember::where([
            'inviter_yyid'  => $userYyid,
            'group_yyid' => '',
            'user_type' => $followerType,
            'status' => DataStatus::REGULAR
        ])->get();
    }

    /**
     * 设置取消关注
     * @param $openid
     */
    public static function batchSetUnsubscribe($openid, $wechatType = DataStatus::WECHAT_AGENT)
    {
        GroupMember::where([
            'user_openid' => $openid,
            'wechat_type' => $wechatType
        ])->update([
            'is_subscribe' => DataStatus::DELETE,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * 查找或者更新邀请关系
     * @param string $agentYYID
     * @param array $wxinfo
     * @param bool $isAutoConfirmd  是否自动确认好友关系,通过群组邀请进来的需要确认，通过个人中心邀请进来的自动确认
     * @param int $wechatType
     * @return GroupMember
     */
    public static function findOrUpdateRelation(
        $agentYYID,
        $wxinfo,
        $userYYID = '',
        $isAutoConfirmd = false,
        $userType = DataStatus::USER_TYPE_DOCTOR,
        $wechatType = DataStatus::WECHAT_AGENT
    ) {
        $wechatRelation = self::getRelationByYYIDAndOpenid($agentYYID, $wxinfo['openid'], $userType, $wechatType);
        if ($wechatRelation) {
            //已经成为好友的就不用再变更了,其它状态
            $isAutoConfirmd = $wechatRelation->status == GroupMember::STATUS_NORMAL ? true : $isAutoConfirmd;
            self::updateInviteRelationOk($agentYYID, $wxinfo['openid'], $isAutoConfirmd, $userType, $wechatType, '');
        } else {
            $wechatRelation = self::addInvitRelation(
                $agentYYID,
                $wxinfo,
                $userYYID,
                $isAutoConfirmd,
                $userType,
                $wechatType
            );
        }

        return $wechatRelation;
    }

    public static function updateInviteRelationOk(
        $inviterYYID,
        $openid,
        $isAutoConfirmd = false,
        $userType = DataStatus::USER_TYPE_DOCTOR,
        $wechatType = DataStatus::WECHAT_AGENT,
        $groupYYID = ''
    ) {
        GroupMember::where([
            'inviter_yyid' => $inviterYYID,
            'user_openid' => $openid,
            'user_type' => $userType,
            'group_yyid' => ''
        ])->update([
            'status' => $isAutoConfirmd ? GroupMember::STATUS_NORMAL : GroupMember::STATUS_JOIN,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }

    public static function addInvitRelation(
        $inviterYYID,
        $wxinfo,
        $userYyid = '',
        $isAutoConfirmd = false,
        $userType = DataStatus::USER_TYPE_DOCTOR,
        $wechatType = DataStatus::WECHAT_AGENT
    ) {
        $o = new GroupMember();
        $o->yyid =  new Expression("replace(upper(uuid()),'-','')");
        $o->owner_yyid = strval($inviterYYID);
        $o->leader_yyid = strval($inviterYYID);
        $o->group_yyid = '';
        $o->user_openid = strval($wxinfo['openid']);
        $o->user_yyid = strval($userYyid);
        $o->inviter_yyid = strval($inviterYYID);
        $o->is_subscribe = 1;
        $o->user_type = $userType;
        $o->status = $isAutoConfirmd ? GroupMember::STATUS_NORMAL : GroupMember::STATUS_JOIN;
        $o->wechat_type = $wechatType;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');

        $o->save();
        return $o;
    }

    /**
     * 查找第一条好友邀请关系
     *
     * @param $agentYYID
     * @param $userOpenid
     * @param $wechatType
     * @return GroupMember
     */
    public static function getRelationByYYIDAndOpenid($agentYYID, $userOpenid, $userType, $wechatType, $groupId = '')
    {
        return GroupMember::where([
            'inviter_yyid' => $agentYYID,
            'user_openid' => $userOpenid,
            'user_type' => $userType,
            'wechat_type' => $wechatType,
            'group_yyid' => $groupId
        ])->first();
    }

    public static function batchSetSubscribed($openid, $wechatType = DataStatus::WECHAT_AGENT)
    {
        GroupMember::where([
            'user_openid' => $openid,
            'wechat_type' => $wechatType
        ])->update([
            'is_subscribe' => DataStatus::REGULAR,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }


    /**
     * 根据好友关系添加组邀请关系
     *
     * @param GroupMember $frientRelation
     * @param Group $group
     * @param int $wechatType
     * @return GroupMember|\Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function findOrAddGroupRelation(
        $openid,
        GroupMember $frientRelation,
        Group $group,
        $isAutoConfirmed = false,
        $wechatType = DataStatus::WECHAT_AGENT
    ) {
        $member = GroupMember::where('group_yyid', $group->yyid)
//            ->where('user_yyid', $frientRelation->user_yyid)
            ->where('user_openid', $openid)
            ->where('user_type', $frientRelation->user_type)
            ->where('wechat_type', $wechatType)
            ->whereIn('status', [GroupMember::STATUS_NORMAL, GroupMember::STATUS_JOIN])
            ->first();
//        $member = self::getRelationByYYIDAndOpenid($frientRelation->inviter_yyid,$frientRelation->user_openid,$frientRelation->user_type,$wechatType,$group->id);

        if (empty($member)) {
            $member = self::cloneWithFriendRelation($frientRelation, $group->yyid, $isAutoConfirmed);
        }
        return $member;
    }

    private static function cloneWithFriendRelation(GroupMember $member, $groupId, $isAutoConfirmed = false)
    {
        $o = new GroupMember();
        $o->yyid         = new Expression("replace(upper(uuid()),'-','')");
        $o->owner_yyid   = $member->owner_yyid;
        $o->leader_yyid  = $member->leader_yyid;
        $o->group_yyid   = $groupId;
        $o->user_openid  = $member->user_openid;
        $o->user_yyid    = $member->user_yyid;
        $o->inviter_yyid = $member->inviter_yyid;
        $o->is_subscribe = DataStatus::REGULAR;
        $o->user_type    = $member->user_type;
        $o->status       = $isAutoConfirmed ? GroupMember::STATUS_NORMAL : GroupMember::STATUS_JOIN;
        $o->created_time = $o->modify_time =  date('Y-m-d H:i:s');
        $o->save();

        return $o;
    }

    public static function clearFriendRelation($inviterYYID, $clearOpenId, $wechatType = DataStatus::WECHAT_DOCTOR)
    {
        GroupMember::where([
                'inviter_yyid' => $inviterYYID,
                'user_openid' => $clearOpenId,
                'wechat_type' => $wechatType,
            ])->where('status', '!=', DataStatus::DELETE)
            ->update([
                'status' => DataStatus::DELETE,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }
}
