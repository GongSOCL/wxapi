<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ItemsFraction;
use Hyperf\DbConnection\Db;

class ItemsFractionRepository
{
    /**
     * 获取指定考试下所有题目的总分
     * @param $examid
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getExamsfractionByid($examid)
    {
        return Db::table('t_compliance_exam')
            ->where(['status'=>DataStatus::REGULAR,'id'=>$examid])
            ->select(["pass_condition"])->first();
    }

    /**
     * 获取指定考试下 答对题目的分数和
     * @param $exid
     * @param $itemsId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getExamsfractionByItemsId($exid, $itemsId)
    {
        return ItemsFraction::where(['status'=>DataStatus::REGULAR,'exam_id'=>$exid])
            ->whereIn('item_id', $itemsId)
            ->groupBy('exam_id')
            ->select([Db::raw("sum(fraction) as items_fraction")])->get();
    }
}
