<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\PaperQuestionItem;

class PaperQuestionItemRepository
{
    public static function getQuestionItemById($questionId): ?PaperQuestionItem
    {
        return PaperQuestionItem::where('id', $questionId)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }
}
