<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\AgentVisit;
use App\Model\Qa\AgentVisitDepart;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class AgentVisitDepartRepository
{

    public static function getVisitDepartsByVisitIds(array $visitIds): Collection
    {
        return Db::table('t_agent_visit_depart')
            ->join(
                't_system_depart',
                't_system_depart.id',
                '=',
                't_agent_visit_depart.depart_id'
            )->where([
                't_agent_visit_depart.status' => DataStatus::REGULAR,
                't_system_depart.status'      => DataStatus::REGULAR
            ])->whereIn('t_agent_visit_depart.visit_id', $visitIds)
            ->select(['t_agent_visit_depart.visit_id', 't_system_depart.*'])
            ->groupBy(['t_agent_visit_depart.visit_id', 't_system_depart.id'])
            ->get();
    }

    /**
     * @param AgentVisit $visit
     * @return Collection
     */
    public static function getVisitDeparts(AgentVisit $visit): Collection
    {
        return AgentVisitDepart::where([
            'visit_id' => $visit->id,
            'status' => DataStatus::REGULAR
        ])->get();
    }

    /**
     * @param $newCheckOutId
     * @param $departIds
     * @return bool
     */
    public static function addVisitDeparts($newCheckOutId, $departIds): bool
    {
        $data = [];
        foreach ($departIds as $key => $value) {
            $data[$key]['visit_id'] = $newCheckOutId;
            $data[$key]['depart_id'] = $value;
            $data[$key]['status'] = DataStatus::REGULAR;
            $data[$key]['created_time'] = date('Y-m-d H:i:s', time());
            $data[$key]['modify_time'] = date('Y-m-d H:i:s', time());
        }

        return AgentVisitDepart::insert($data);
    }

    public static function batchDeleteDeparts(AgentVisit $visit, array $delDepartIds): int
    {
        return AgentVisitDepart::where([
            'visit_id' => $visit->id,
            'status' => DataStatus::REGULAR
        ])->whereIn('depart_id', $delDepartIds)
            ->update([
                'status' => DataStatus::DELETE,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }
}
