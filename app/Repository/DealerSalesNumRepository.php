<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DealerSalesNum;

class DealerSalesNumRepository
{
    /**
     * @param $companyYyid
     * @param $staffCode
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getLastMonth($companyYyid, $staffCode)
    {
        return DealerSalesNum::query()
            ->select('month')
            ->distinct()
            ->where([
                'company_yyid' => $companyYyid,
                'staff_code' => $staffCode,
                'status' => DataStatus::REGULAR
            ])
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @param $companyYyid
     * @param $month
     * @param $staffCode
     * @return \Hyperf\Utils\Collection
     */
    public static function getReportInfo($companyYyid, $month, $staffCode)
    {
        return DealerSalesNum::select([
                'id','month','staff_code','product_id','sku_id',
                'month_target','month_sales','month_ach','ly_sales','month_gr',
                'qtd_target','qtd_sales','qtd_ach','qtd_ly_sales','qtd_gr',
                'ytd_target','ytd_sales','ytd_ach','ytd_ly_sales','ytd_gr'
            ])
            ->where([
                'month' => $month,
                'company_yyid' => $companyYyid,
                'staff_code' => $staffCode,
                'status' => DataStatus::REGULAR
            ])->get();
    }
}
