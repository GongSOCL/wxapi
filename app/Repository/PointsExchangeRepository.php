<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ExchangeGoods;
use App\Service\Points\PointRpcService;
use App\Service\Points\PointsService;
use Grpc\Point\GoodsDetail;
use Hyperf\Di\Annotation\Inject;

class PointsExchangeRepository
{
    /**
     * @Inject
     * @var ExchangeGoods
     */
    private $exchangeGoodsModel;

    /**
     * 获取积分兑换列表.
     * @param $uid
     * @param $page
     * @param $pageSize
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getGoodsList($uid, $page, $pageSize, $isAgent = true): array
    {
        $userType = $isAgent ? DataStatus::USER_TYPE_AGENT : DataStatus::USER_TYPE_DOCTOR;
        $reply = PointRpcService::fetchExchangeGoodsList($uid, $page, $pageSize, $userType);

        $list = [];
        /** @var GoodsDetail $obj */
        foreach ($reply->getList() as $obj) {
            $list[] = [
                'goods_yyid' => $obj->getYyid(),
                'goods_name' => $obj->getGoodsName(),
                'goods_pic' => $obj->getGoodsPic(),
                'money_point' => $obj->getMoneyPoint(),
                'service_point' => $obj->getServicePoint(),
                'goods_type' => $obj->getGoodsType(),
                'desc' => $obj->getDesc(),
            ];
        }

        return [
            'page' => $reply->getPage(),
            'page_size' => $reply->getPageSize(),
            'total' => $reply->getTotal(),
            'list' => $list,
        ];
    }
}
