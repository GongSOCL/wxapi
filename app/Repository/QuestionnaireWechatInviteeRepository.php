<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\QuestionnairePaper;
use App\Model\Qa\QuestionnaireWechatInvitee;
use App\Model\Qa\WechatUser;
use App\Model\Qa\Users;
use Hyperf\Database\Model\Collection;
use Hyperf\DbConnection\Db;

class QuestionnaireWechatInviteeRepository
{

    /**
     * 获取某个openid问卷邀请记录
     * @param $paperId
     * @param $openId
     * @param $invitorId
     * @param int $userType
     * @return QuestionnaireWechatInvitee|null
     */
    public static function getInviteRecord(
        $paperId,
        $openId,
        $invitorId,
        $userType = DataStatus::USER_TYPE_DOCTOR
    ): ?QuestionnaireWechatInvitee {
          return QuestionnaireWechatInvitee::where([
              'paper_id' => $paperId,
              'wechat_open_id' => $openId,
              'invitee_type' => $userType,
              'status' => DataStatus::REGULAR,
              'invitor_id' => $invitorId
          ])->orderByDesc('id')
            ->first();
    }


    /**
     * 获取问卷实际占用的额度
     *
     * @param array $paperIds
     * @param Users $users
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getPaperValidUsedQuota(array $paperIds, Users $users)
    {
        $now = date("Y-m-d H:i:s");
        //总量 - 已完成 - 已邀请末过期的就是实际可用量
        $query = QuestionnaireWechatInvitee::where([
            'invitor_id' => $users->uid,
            'status' => DataStatus::REGULAR,
        ])->whereIn('paper_id', $paperIds);

        //已经结束或者未过期的邀请
        $expressing = sprintf(
            "((invite_status=%d) or (invite_status in (%d,%d,%d, %d) and last_expire_time > '%s'))",
            QuestionnaireWechatInvitee::INVITE_STATUS_FINISHED,
            QuestionnaireWechatInvitee::INVITE_STATUS_NEW,
            QuestionnaireWechatInvitee::INVITE_STATUS_INVITED,
            QuestionnaireWechatInvitee::INVITE_STATUS_REINVITED,
            QuestionnaireWechatInvitee::INVITE_STATUS_START,
            $now
        );

        return $query->whereRaw($expressing)
            ->groupBy("paper_id")
            ->select("paper_id", Db::raw("count(*) as cnt"))
            ->get();
    }

    /**
     * 获取某一份问卷被邀请人列表
     * @param QuestionnairePaper $paper
     * @param $invitorID
     * @return Collection
     */
    public static function getUserPaperInviteeList(QuestionnairePaper $paper, $invitorID): Collection
    {
        return $paper->wechatInvitees()
            ->where(['invitor_id' => $invitorID])
            ->get();
    }


    public static function addInvitee(
        QuestionnairePaper  $paper,
        Users $invitor,
        $openId,
        $inviteeType = DataStatus::USER_TYPE_DOCTOR,
        $unionId = ""
    ): QuestionnaireWechatInvitee {
        $a = new \DateTime();
        $now = date("Y-m-d H:i:s");
        $o = new QuestionnaireWechatInvitee();
        $o->paper_id = $paper['id'];
        $o->invitor_id = $invitor['uid'];
        $o->wechat_open_id = $openId;
        $o->wechat_union_id = $unionId;
        $o->invitee_type= $inviteeType;
        $o->last_invite_time = $a->format("Y-m-d H:i:s");
        $o->last_expire_time = $a->modify("+{$paper['invite_expire_hour']} hours")->format("Y-m-d H:i:s");
        $o->invite_status = QuestionnaireWechatInvitee::INVITE_STATUS_NEW;
        $o->status = DataStatus::REGULAR;
        $o->created_time = $o->modify_time = $now;
        try {
            $o->save();
        } catch (\Exception $e) {
        }
        $o->save();

        return $o;
    }

    /**
     * 标记状态为已邀请并修改计时起始点为当前时间点
     *
     * @param QuestionnaireWechatInvitee $invitee
     * @param QuestionnairePaper $paper
     * @return int
     */
    public static function markInvited(QuestionnaireWechatInvitee  $invitee, QuestionnairePaper $paper): int
    {
        $expireHours = $paper->invite_expire_hour;
        $a = new \DateTime();
        $now = $a->format('Y-m-d H:i:s');
        $end = $a->modify("+{$expireHours} hours")->format("Y-m-d H:i:s");
        return QuestionnaireWechatInvitee::where([
            'id' => $invitee->id,
            'status' => DataStatus::REGULAR,
            'invite_status' => QuestionnaireWechatInvitee::INVITE_STATUS_NEW
        ])->update([
            'invite_status' => QuestionnaireWechatInvitee::INVITE_STATUS_INVITED,
            'last_invite_time' => $now,
            'last_expire_time' => $end,
            'modify_time' => $now,
        ]);
    }

    public static function cancelInvite(QuestionnaireWechatInvitee  $invitee)
    {
        QuestionnaireWechatInvitee::where('id', $invitee->id)
            ->update([
                'status' => DataStatus::DELETE,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * 获取某个openid某份问卷是否有被某人邀请参与回答
     *
     * @param $paperId
     * @param $invitorId
     * @param $openId
     * @param int $inviteeUserType
     * @return QuestionnaireWechatInvitee|null
     */
    public static function checkInviteRelation(
        $paperId,
        $invitorId,
        $openId,
        $inviteeUserType = DataStatus::USER_TYPE_DOCTOR
    ) {
        return QuestionnaireWechatInvitee::where([
            'paper_id' => $paperId,
            'invitor_id' => $invitorId,
            'wechat_open_id' => $openId,
            'invitee_type' => $inviteeUserType,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * 标记已重新邀请
     * @param QuestionnaireWechatInvitee $invitee
     * @param QuestionnairePaper $paper
     * @return int|string
     */
    public static function markReInvited(QuestionnaireWechatInvitee $invitee, QuestionnairePaper $paper)
    {
        $a = new \DateTime();
        $now = $a->format("Y-m-d H:i:s");
        $expireHours = $paper->invite_expire_hour;
        $end =$a->modify("+{$expireHours} hours")->format("Y-m-d H:i:s");

        return QuestionnaireWechatInvitee::where([
            'id' => $invitee->id,
            'status' => DataStatus::REGULAR
        ])->whereIn('invite_status', [
            QuestionnaireWechatInvitee::INVITE_STATUS_INVITED,
            QuestionnaireWechatInvitee::INVITE_STATUS_REINVITED,
            QuestionnaireWechatInvitee::INVITE_STATUS_START
        ])->update([
            'invite_status' => QuestionnaireWechatInvitee::INVITE_STATUS_REINVITED,
            'modify_time' => $now,
            'last_invite_time' => $now,
            'last_expire_time' => $end,
        ]);
    }

    /**
     * 更新数据库表示完成答题
     * @param QuestionnaireWechatInvitee $invitee
     * @return int
     */
    public static function setFinished(QuestionnaireWechatInvitee $invitee): int
    {
        return QuestionnaireWechatInvitee::where([
            'id' => $invitee->id,
            'status' => DataStatus::REGULAR,
            'invite_status' => QuestionnaireWechatInvitee::INVITE_STATUS_START
        ])->update([
            'invite_status' => QuestionnaireWechatInvitee::INVITE_STATUS_FINISHED,
            'modify_time' => date("Y-m-d H:i:s")
        ]);
    }

    /**
     * 开始问卷
     * @param QuestionnaireWechatInvitee $invitee
     */
    public static function setStart(QuestionnaireWechatInvitee $invitee)
    {
        QuestionnaireWechatInvitee::where('id', $invitee->id)
            ->whereIn(
                'invite_status',
                [
                    QuestionnaireWechatInvitee::INVITE_STATUS_INVITED,
                    QuestionnaireWechatInvitee::INVITE_STATUS_REINVITED
                ]
            )
            ->update([
            'invite_status' => QuestionnaireWechatInvitee::INVITE_STATUS_START,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * 根据id查找邀请记录
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getInviteeById($id): ?QuestionnaireWechatInvitee
    {
        return QuestionnaireWechatInvitee::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();
    }


    /**
     * 获取问卷实际占用的额度
     *
     * @param QuestionnairePaper $paper
     * @param $invitorId
     * @return int
     */
    public static function getPaperValidQuota(QuestionnairePaper $paper, $invitorId): int
    {
        $now = date("Y-m-d H:i:s");
        //总量 - 已完成 - 已邀请末过期的就是实际可用量
        $query = QuestionnaireWechatInvitee::where([
            'invitor_id' => $invitorId,
            'status' => DataStatus::REGULAR,
            'paper_id' => $paper->id
        ]);

        //已经结束或者未过期的邀请
        $expressing = sprintf(
            "((invite_status=%d) or (invite_status in (%d,%d,%d, %d) and last_expire_time > '%s'))",
            QuestionnaireWechatInvitee::INVITE_STATUS_FINISHED,
            QuestionnaireWechatInvitee::INVITE_STATUS_NEW,
            QuestionnaireWechatInvitee::INVITE_STATUS_INVITED,
            QuestionnaireWechatInvitee::INVITE_STATUS_REINVITED,
            QuestionnaireWechatInvitee::INVITE_STATUS_START,
            $now
        );

        return $query->whereRaw($expressing)
            ->select()
            ->count();
    }
}
