<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Helper\Helper;
use App\Model\Qa\AgentFamiliarCompany;
use App\Model\Qa\Users;
use Hyperf\Utils\Collection;

class AgentFamiliarCompanyRepository
{
    public static function clearUserFamiliarCompany(Users $users)
    {
        return AgentFamiliarCompany::where([
            'user_yyid' => $users->yyid,
            'status' => DataStatus::REGULAR
        ])->update([
            'status' => DataStatus::DELETE,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }
    public static function batchAddUserFamiliarCompany(Users $users, array $cms): bool
    {
        $data = [];
        $yyid = Helper::yyidGenerator();
        $now = date('Y-m-d H:i:s');
        foreach ($cms as $item) {
            $data[] = [
                'yyid' => $yyid,
                'user_yyid' => $users->yyid,
                'company_name' => $item['company_name'],
                'company_adaptation' => implode(',', $item['adaption_id']),
                'company_territory' => implode(',', $item['field_id']),
                'drug_name' => $item['product'],
                'status' => DataStatus::REGULAR,
                'created_time' => $now,
                'modify_time' => $now
            ];
        }

        return AgentFamiliarCompany::insert($data);
    }

    public static function getUserFamiliarCompanies(Users $user): Collection
    {
        return AgentFamiliarCompany::where([
            'user_yyid' => $user->yyid,
            'status' => DataStatus::REGULAR
        ])->get();
    }
}
