<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\Agent;
use App\Model\Qa\AgentVisit;
use App\Model\Qa\AgentVisitDoctor;
use App\Model\Qa\AgentVisitDrugSeries;
use App\Model\Qa\DrugSeries;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\Supplier;
use App\Model\Qa\SupplierHospitalProduct;
use App\Model\Qa\SupplierMember;
use App\Model\Qa\SupplierMemberDetail;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;
use Hyperf\DbConnection\Db;

class SupplierRepository
{
    /**
     * @param $userYyid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getMemberRole($userYyid)
    {
        return Agent::where('u_yyid', $userYyid)->first();
    }

    /**
     * @param $userId
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function getGroupList($userId, $page, $pageSize)
    {
        return SupplierMember::from('t_supplier_member as sm')
            ->join('t_agent as a', 'sm.u_yyid', '=', 'a.u_yyid')
            ->join('wechat_user as wu', 'sm.u_yyid', '=', 'wu.u_yyid')
            ->select(['sm.id as member_id', 'a.truename as true_name', 'wu.headimgurl as heading', 'sm.is_leader'])
            ->where(function ($query) use ($userId) {
                $query->where('sm.user_id', $userId)
                    ->where('sm.status', SupplierMember::STATUS_1)
                    ->where('sm.is_deleted', DataStatus::DELETE)
                    ->where('wu.wechat_type', DataStatus::WECHAT_AGENT);
            })
            ->orWhere(function ($query) use ($userId) {
                $query->where('sm.pid', $userId)
                    ->where('sm.status', SupplierMember::STATUS_1)
                    ->where('sm.is_deleted', DataStatus::DELETE)
                    ->where('wu.wechat_type', DataStatus::WECHAT_AGENT);
            })
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->orderBy('sm.is_leader', 'desc')
            ->orderBy('sm.id', 'desc')
            ->get();
    }

    /**
     * @param $userId
     * @return int
     */
    public static function getGroupListCount($userId)
    {
        return SupplierMember::from('t_supplier_member as sm')
            ->join('t_agent as a', 'sm.u_yyid', '=', 'a.u_yyid')
            ->join('wechat_user as wu', 'sm.u_yyid', '=', 'wu.u_yyid')
            ->where(function ($query) use ($userId) {
                $query->where('sm.user_id', $userId)
                    ->where('sm.status', SupplierMember::STATUS_1)
                    ->where('sm.is_deleted', DataStatus::DELETE)
                    ->where('wu.wechat_type', DataStatus::WECHAT_AGENT);
            })
            ->orWhere(function ($query) use ($userId) {
                $query->where('sm.pid', $userId)
                    ->where('sm.status', SupplierMember::STATUS_1)
                    ->where('sm.is_deleted', DataStatus::DELETE)
                    ->where('wu.wechat_type', DataStatus::WECHAT_AGENT);
            })->count();
    }

    /**
     * @param $memberId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getMenberBindHospitalCount($memberId)
    {
        return SupplierMemberDetail::where([
            'member_id' => $memberId,
            'status' => SupplierMemberDetail::STATUS_2,
            'is_deleted' => DataStatus::DELETE
        ])->select('hospital_id')->groupBy(['hospital_id'])->get();
    }

    /**
     * @param $userId
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function getJoinList($userId, $page, $pageSize)
    {
        return SupplierMember::from('t_supplier_member as sm')
            ->join('t_agent as a', 'sm.u_yyid', '=', 'a.u_yyid')
            ->join('wechat_user as wu', 'sm.u_yyid', '=', 'wu.u_yyid')
            ->select(['sm.id as member_id', 'a.truename as true_name', 'wu.headimgurl as heading', 'sm.created_at'])
            ->where('sm.pid', $userId)
            ->where('sm.status', SupplierMember::STATUS_0)
            ->where('sm.is_deleted', DataStatus::DELETE)
            ->where('wu.wechat_type', DataStatus::WECHAT_AGENT)
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->orderBy('sm.id', 'desc')
            ->get();
    }

    /**
     * @param $userId
     * @return int
     */
    public static function getJoinListCount($userId)
    {
        return SupplierMember::from('t_supplier_member as sm')
            ->join('t_agent as a', 'sm.u_yyid', '=', 'a.u_yyid')
            ->join('wechat_user as wu', 'sm.u_yyid', '=', 'wu.u_yyid')
            ->where('sm.pid', $userId)
            ->where('sm.status', SupplierMember::STATUS_0)
            ->where('sm.is_deleted', DataStatus::DELETE)
            ->where('wu.wechat_type', DataStatus::WECHAT_AGENT)
            ->count();
    }

    /**
     * @param $memberId
     */
    public static function cancelBindMember($memberId)
    {
        SupplierMemberDetail::where([
            'member_id' => $memberId,
            'type' => SupplierMemberDetail::TYPE_2,
        ])->update([
            'status' => SupplierMemberDetail::STATUS_4,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param $memberId
     */
    public static function removeMember($memberId)
    {
        SupplierMember::where('id', $memberId)->update([
            'status' => SupplierMember::STATUS_2,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param $yyid
     * @param $supplier_id
     * @param $is_supplier
     * @param $is_lead
     */
    public static function updateSupplierRole($yyid, $supplier_id, $is_supplier, $is_lead)
    {
        Agent::where('u_yyid', $yyid)->update([
            'supplier_id' => $supplier_id,
            'is_supplier' => $is_supplier,
            'is_supplier_leader' => $is_lead
        ]);
    }

    /**
     * @param $yyids
     */
    public static function updateSupplierRoles($yyids)
    {
        Agent::whereIn('u_yyid', $yyids)->update([
            'supplier_id' => 0,
            'is_supplier' => 0,
            'is_supplier_leader' => 0
        ]);
    }

    /**
     * @param $userId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getMemberInfo($userId)
    {
        return SupplierMember::where(function ($query) use ($userId) {
            $query->where('pid', $userId)->where('status', SupplierMember::STATUS_1);
        })->orWhere(function ($query) use ($userId) {
            $query->where('user_id', $userId)->where('status', SupplierMember::STATUS_1);
        })->get();
    }

    public static function getMembersInfo($userId, $supplierId)
    {
        return SupplierMember::where('pid', $userId)
            ->where('supplier_id', $supplierId)
            ->where('is_leader', SupplierMember::NOT_LEADER)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    /**
     * @param $members
     */
    public static function cancelBindMembers($members, $supplierId)
    {
        SupplierMemberDetail::where('supplier_id', $supplierId)
            ->whereIn('member_id', $members)
            ->update([
                'status' => SupplierMemberDetail::STATUS_4,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * @param $supplierId
     */
    public static function dismissGroup($supplierId)
    {
        SupplierMember::where('supplier_id', $supplierId)
            ->update([
                'status' => SupplierMember::STATUS_2,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * @param $userId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getOneMemberInfoByUserId($userId)
    {
        return SupplierMember::where([
            'user_id' => $userId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getOneMemberInfoById($id)
    {
        return SupplierMember::where('id', $id)->first();
    }

    /**
     * @param $supplierId
     * @param $yyid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getOneMemberInfoByYyid($supplierId, $yyid)
    {
        return SupplierMember::where([
            'supplier_id' => $supplierId,
            'u_yyid' => $yyid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getMemberInfoByUserid($supplierId, $userId)
    {
        return SupplierMember::where([
            'supplier_id' => $supplierId,
            'is_leader' => SupplierMember::IS_LEADER,
            'user_id' => $userId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $memberId
     * @return \Hyperf\Utils\Collection
     */
    public static function getMemberDetail($memberId)
    {
        return SupplierMemberDetail::from('t_supplier_member_detail as smd')
            ->join('t_drug_series as dp', 'smd.product_id', '=', 'dp.id')
            ->join('t_youyao_hospital as yh', 'smd.hospital_id', '=', 'yh.id')
            ->select(['smd.id as bind_id', 'dp.series_name as product_name', 'yh.hospital_name'])
            ->where([
                'smd.member_id' => $memberId,
                'smd.status' => SupplierMemberDetail::STATUS_2,
                'smd.is_deleted' => DataStatus::DELETE
            ])->orderBy('smd.id', 'desc')->get();
    }

    /**
     * @param $bindId
     */
    public static function unbindMember($bindId)
    {
        SupplierMemberDetail::whereIn('id', $bindId)->update([
            'status' => SupplierMemberDetail::STATUS_4,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param $supplierId
     * @param $occupyId
     * @return \Hyperf\Utils\Collection
     */
    public static function getSupplierHospitalProduct($supplierId, $occupyId)
    {
        return SupplierHospitalProduct::whereNotIn('id', $occupyId)
            ->where([
                'supplier_id' => $supplierId,
                'is_deleted' => DataStatus::DELETE
            ])->latest()->get();
    }

    public static function searchSupplierHospitalProduct($supplierId, $hid, $pid, $occupyId)
    {
        return SupplierHospitalProduct::where(function ($query) use ($supplierId, $hid, $occupyId) {
            $query->whereNotIn('id', $occupyId)
                ->where('supplier_id', $supplierId)
                ->where('is_deleted', DataStatus::DELETE)
                ->whereIn('hospital_id', $hid);
        })->orWhere(function ($query) use ($supplierId, $pid, $occupyId) {
            $query->whereNotIn('id', $occupyId)
                ->where('supplier_id', $supplierId)
                ->where('is_deleted', DataStatus::DELETE)
                ->whereIn('product_id', $pid);
        })->latest()->get();
    }

    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getProduct($id)
    {
        return DrugSeries::where('id', $id)->first();
    }

    /**
     * @param $diffIds
     * @return \Hyperf\Utils\Collection
     */
    public static function getHospitals($diffIds)
    {
        return YouyaoHospital::whereIn('id', $diffIds)
            ->select(['id as hospital_id', 'yyid', 'hospital_name'])
            ->get();
    }

    /**
     * @param $supplierId
     * @param $id
     * @param $userId
     * @param $productId
     * @param $hospitalId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getMember($supplierId, $id, $userId, $productId, $hospitalId)
    {
        return SupplierMemberDetail::where([
            'supplier_id' => $supplierId,
            'member_id' => $id,
            'user_id' => $userId,
            'hospital_id' => $hospitalId,
            'product_id' => $productId
        ])->whereIn('status', [1, 2])->first();
    }

    /**
     * @param $supplierId
     * @param $id
     * @param $userId
     * @param $productId
     * @param $hospitalId
     * @return int
     */
    public static function bindMember($supplierId, $id, $userId, $productId, $hospitalId)
    {
        return SupplierMemberDetail::insertGetId([
            'supplier_id' => $supplierId,
            'member_id' => $id,
            'user_id' => $userId,
            'hospital_id' => $hospitalId,
            'product_id' => $productId,
            'type' => SupplierMemberDetail::TYPE_2,
            'status' => SupplierMemberDetail::STATUS_2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getSupplierInfoById($id)
    {
        return Supplier::where('id', $id)->first();
    }

    /**
     * @param $user
     * @param $hospital
     * @param $product
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getHospitalServe($user, $hospital, $product)
    {
        return RepsServeScopeS::where([
            'user_yyid' => $user->yyid,
            'series_yyid' => $product->yyid,
            'hospital_yyid' => $hospital->yyid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param Users $user
     * @param YouyaoHospital $hospital
     * @param DrugSeries $series
     * @param int $adminId
     * @param int $status
     * @return RepsServeScopeS
     */
    public static function addServe(
        Users $user,
        YouyaoHospital $hospital,
        DrugSeries $series,
        $adminId = 0,
        $status = 0
    ): RepsServeScopeS {
        $now = date('Y-m-d H:i:s');
        $o = new RepsServeScopeS();
        $o->yyid = Helper::generateYYID();
        $o->user_yyid = $user->yyid;
        $o->series_yyid = $series->yyid;
        $o->hospital_yyid = $hospital->yyid;
        $o->admin_id = $adminId;
        $o->agreed_time = $now;
        $o->status = $status;
        $o->created_time = $o->modify_time = $now;
        if (!$o->save()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "代理申请添加失败");
        }
        $o->refresh();

        return $o;
    }

    /**
     * @param $id
     * @return \Hyperf\Utils\Collection
     */
    public static function getMemberDetailById($id)
    {
        return SupplierMemberDetail::whereIn('id', $id)->get();
    }

    /**
     * @param $memberId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getMemberDetailByMemberId($memberId)
    {
        return SupplierMemberDetail::where([
            'member_id' => $memberId,
            'type' => SupplierMemberDetail::TYPE_2,
            'status' => SupplierMemberDetail::STATUS_2,
            'is_deleted' => DataStatus::DELETE
        ])->get();
    }

    /**
     * @param $members
     * @return \Hyperf\Utils\Collection
     */
    public static function getMemberDetailByMemberIds($members)
    {
        return SupplierMemberDetail::whereIn('member_id', $members)
            ->whereIn('status', [1, 2])
            ->where('is_deleted', DataStatus::DELETE)
            ->get();
    }

    /**
     * @param $user
     * @param $hospital
     * @param $product
     */
    public static function cancelServe($user, $hospital, $product)
    {
        RepsServeScopeS::where([
            'user_yyid' => $user->yyid,
            'series_yyid' => $product->yyid,
            'hospital_yyid' => $hospital->yyid,
            'status' => DataStatus::REGULAR
        ])->update([
            'status' => 4,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }

    ////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $supplierId
     * @param $userId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getSupplierMemberBySupplierId($supplierId, $userId)
    {
        return SupplierMember::where([
            'supplier_id' => $supplierId,
            'user_id' => $userId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $supplierId
     * @param $userId
     * @param $inviteAid
     * @param $userYyid
     * @return int
     */
    public static function addJoinMember($supplierId, $userId, $inviteAid, $userYyid)
    {
        return SupplierMember::insertGetId([
            'supplier_id' => $supplierId,
            'is_leader' => SupplierMember::NOT_LEADER,
            'user_id' => $userId,
            'pid' => $inviteAid,
            'u_yyid' => $userYyid,
            'status' => DataStatus::DELETE,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getOneMemberInfo($supplierId, $userId)
    {
        return SupplierMember::where([
            'supplier_id' => $supplierId,
            'user_id' => $userId,
            'is_deleted' => DataStatus::DELETE
        ])->whereIn('status', [DataStatus::REGULAR, 0])->first();
    }

    public static function memberVerifyOk($id, $status)
    {
        SupplierMember::where('id', $id)->update([
            'status' => $status,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param $userId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getMemberInfoInGroup($userId)
    {
        return SupplierMember::where(function ($query) use ($userId) {
                $query->where('user_id', $userId)
                    ->where('status', SupplierMember::STATUS_1)
                    ->where('is_deleted', DataStatus::DELETE);
        })
            ->orWhere(function ($query) use ($userId) {
                $query->where('pid', $userId)
                    ->where('status', SupplierMember::STATUS_1)
                    ->where('is_deleted', DataStatus::DELETE);
            })
            ->get();
    }

    /**
     * @param $userId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getVisitInfoByUserId($userId, $date)
    {
        return AgentVisit::where([
            ['user_id', '=', $userId],
            ['check_in_time', 'like', $date.'%'],
            ['status', '=', DataStatus::REGULAR]
        ])->get();
    }

    /**
     * @param $userIds
     * @param $date
     * @return \Hyperf\Utils\Collection
     */
    public static function getVisitInfosByUserId($userIds, $date)
    {
        return AgentVisit::whereIn('user_id', $userIds)->where([
            ['check_in_time', 'like', $date.'%'],
            ['status', '=', DataStatus::REGULAR]
        ])->select('id', 'user_id', 'hospital_id')->get();
    }

    /**
     * @param $id
     * @return YouyaoHospital|null
     */
    public static function getHospitalById($id): ?YouyaoHospital
    {
        return YouyaoHospital::where('id', $id)->first();
    }

    public static function getVisitDetailByUserIds($ids, $date)
    {
        return self::publicModel()
            ->whereIn('user_id', $ids)
            ->where('check_in_time', 'like', $date.'%')
            ->groupBy(['tav.id'])
            ->orderBy('tav.id', 'desc')
            ->get();
    }

    /**
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Query\Builder
     */
    public static function publicModel()
    {
        return AgentVisit::query()
            ->from('t_agent_visit as tav')
            ->leftJoin(
                't_users as tu',
                'tav.user_id',
                '=',
                'tu.uid'
            )
            ->leftJoin(
                't_company as tc',
                'tu.company_yyid',
                '=',
                'tc.yyid'
            )
            ->leftJoin(
                't_agent_visit_depart as tavd',
                'tav.id',
                '=',
                'tavd.visit_id'
            )
            ->leftJoin(
                't_agent_visit_geo as tavg',
                'tav.check_in_geo',
                '=',
                'tavg.id'
            )
            ->leftJoin(
                't_agent_visit_geo as tavg_1',
                'tav.check_out_geo',
                '=',
                'tavg_1.id'
            )
            ->leftJoin(
                't_agent_visit_config as tavc',
                'tav.check_in_geo_opt',
                '=',
                'tavc.id'
            )
            ->leftJoin(
                't_agent_visit_config as tavc_1',
                'tav.check_out_geo_opt',
                '=',
                'tavc_1.id'
            )
            ->leftJoin(
                't_agent_visit_config as tavc_2',
                'tav.visit_special_id',
                '=',
                'tavc_2.id'
            )
            ->leftJoin(
                't_agent_visit_doctor as tavo',
                'tav.id',
                '=',
                'tavo.visit_id'
            )
            ->select(['tav.id','tav.user_id','tc.name as company_name','tu.mobile_num','tu.company_yyid',
                'tav.hospital_id','visit_type','check_in_time','check_out_time',
                Db::raw("GROUP_CONCAT(tavd.depart_id SEPARATOR ';') as depart_id"),
                'tavg.amap_longitude as check_in_longitude','tavg.amap_latitude as check_in_latitude',
                'tavg.sign_pos as check_in_pos','tavc.config_name as check_in_range_reason',
                'tavg_1.amap_longitude as check_out_longitude','tavg_1.amap_latitude as check_out_latitude',
                'tavg_1.sign_pos as check_out_pos','tavc_1.config_name as check_out_range_reason',
                'tavc_2.config_name as visit_special_reason','check_in_geo_comment','check_out_geo_comment',
                'visit_state','visit_state_comment','visit_special_comment','is_collaborative',
                Db::raw("GROUP_CONCAT(tavo.doctor_id SEPARATOR ';') as doctor_id"),
                'visit_result','next_visit_purpose']);
    }

    public static function getSeriesByVisitId($id)
    {
        return AgentVisitDrugSeries::query()->where('visit_id', $id)->get();
    }

    public static function getOccupiedHospital($seriesId, $hospitalIDS)
    {
        return SupplierMemberDetail::where('product_id', $seriesId)
            ->whereIn('hospital_id', $hospitalIDS)
            ->whereIn('status', [SupplierMemberDetail::STATUS_1, SupplierMemberDetail::STATUS_2])
            ->where('is_deleted', DataStatus::DELETE)
            ->get();
    }

    public static function getAgentInfo($inviteAid)
    {
        return Agent::from('t_agent as a')
            ->join('t_users as u', 'a.u_yyid', '=', 'u.yyid')
            ->select('u.uid')
            ->where('a.aid', $inviteAid)
            ->first();
    }

    public static function getMemberDetailByUserId($supplierId, $memberId, $uid, $hospitalId, $seriesId, $type)
    {
        return SupplierMemberDetail::where([
            'supplier_id' => $supplierId,
            'member_id' => $memberId,
            'user_id' => $uid,
            'hospital_id' => $hospitalId,
            'product_id' => $seriesId,
            'type' => $type,
            'is_deleted' => DataStatus::DELETE
        ])->first();
    }

    public static function addSupplierMemberDetail($supplierId, $memberId, $uid, $hospitalId, $seriesId)
    {
        return SupplierMemberDetail::insert([
            'supplier_id' => $supplierId,
            'member_id' => $memberId,
            'user_id' => $uid,
            'hospital_id' => $hospitalId,
            'product_id' => $seriesId,
            'type' => SupplierMemberDetail::TYPE_1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getSupplierByPhone($phone)
    {
        return Supplier::where([
            'admin_phone' => $phone,
            'is_deleted' => DataStatus::DELETE
        ])->first();
    }

    public static function getSupplierLeader($supplierId)
    {
        return SupplierMember::where([
            'supplier_id' => $supplierId,
            'is_leader' => 1,
            'status' => DataStatus::REGULAR,
            'is_deleted' => DataStatus::DELETE
        ])->first();
    }

    public static function updateLeaderInfo($leadId, $user)
    {
        SupplierMember::where('id', $leadId)->update([
            'user_id' => $user->uid,
            'u_yyid' => $user->yyid,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param $id
     * @return int
     */
    public static function getVisitCountById($id)
    {
        return AgentVisitDoctor::where('visit_id', $id)
            ->where('status', DataStatus::REGULAR)
            ->count();
    }

    public static function getUserAgent($yyid)
    {
        return Users::from('t_users as tu')
            ->join('t_agent as ta', 'tu.yyid', '=', 'ta.u_yyid')
            ->where('tu.yyid', $yyid)
            ->select('tu.*', 'supplier_id', 'is_supplier', 'real_role_type')
            ->first();
    }

    public static function getHospitalByName($keyword)
    {
        return YouyaoHospital::where('hospital_name', 'like', '%' . $keyword . '%')
            ->limit(100)
            ->pluck('id');
    }

    public static function getProductByName($keyword)
    {
        return DrugSeries::where('series_name', 'like', '%' . $keyword . '%')
            ->where('status', DataStatus::REGULAR)
            ->pluck('id');
    }

    public static function getMemberDetails($supplierId, $id, $userId)
    {
        return SupplierMemberDetail::where([
            'supplier_id' => $supplierId,
            'member_id' => $id,
            'user_id' => $userId
        ])->whereIn('status', [1, 2])->get();
    }

    public static function getOccupyHospital($supplierId, $item)
    {
        return SupplierHospitalProduct::where([
            'supplier_id' => $supplierId,
            'hospital_id' => $item->hospital_id,
            'product_id' => $item->product_id,
            'is_deleted' => DataStatus::DELETE
        ])->first();
    }

    public static function deleteSupplier($id)
    {
        Supplier::where('id', $id)->update([
            'is_deleted' => DataStatus::REGULAR,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getDetailByHospital($supplierId, $item)
    {
        return SupplierMemberDetail::where([
            'supplier_id' => $supplierId,
            'hospital_id' => $item['hospital_id'],
            'product_id' => $item['product_id'],
            'is_deleted' => DataStatus::DELETE
        ])->whereIn('status', [1, 2])->pluck('id');
    }

    public static function removeHospitalProduct($supplierId, $hospitalId, $productId)
    {
        SupplierHospitalProduct::where([
            'supplier_id' => $supplierId,
            'hospital_id' => $hospitalId,
            'product_id' => $productId
        ])->update([
            'is_deleted' => DataStatus::REGULAR,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
