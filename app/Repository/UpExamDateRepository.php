<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ItemsFraction;
use App\Model\Qa\UpExamDate;
use Hyperf\DbConnection\Db;

class UpExamDateRepository
{
    /**
     * 获取指定考试下所有题目的总分
     * @param $examid
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getUpExamDate($examids)
    {
        return UpExamDate::where(
            [
                ['status','=',DataStatus::REGULAR],
                ['up_exam_end','!=',null],
                ['up_exam_start','!=',null]
            ]
        )
            ->whereIn('exam_id', $examids)
            ->select('exam_id', 'up_exam_start', 'up_exam_end', 'up_exam_time', 'up_exam_num')->get();
    }
}
