<?php


namespace App\Repository;

use App\Model\Qa\CompanyMonthPoint;

class CompanyMonthPointRepository
{

    public static function getLastMonth(): ?CompanyMonthPoint
    {
        return CompanyMonthPoint::query()
            ->orderByDesc("month")
            ->first();
    }
}
