<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Repository;

use App\Model\Qa\ComplianceAgreement;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

class ComplianceAgreementRepository
{
    /**
     * @return null|Builder|ComplianceAgreement|Model|object
     */
    public function getByYyid(string $yyid): ?ComplianceAgreement
    {
        return ComplianceAgreement::query()
            ->where('yyid', $yyid)
            ->where('status', 1)
            ->first();
    }
}
