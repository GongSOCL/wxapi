<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\AgentVisit;
use App\Model\Qa\AgentVisitGeo;
use App\Model\Qa\Users;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\DbConnection\Db;

class AgentVisitRepository
{
    public static function getUserVisitData(
        Users $user,
        $checkInStartAt = "",
        $checkInEndAt = "",
        $checkOutStartAt = "",
        $checkOutEndAt = "",
        $createStartAt = "",
        $createEndAt = "",
        $checkType = 0,
        $visitType = 0,
        $hospitalId = 0,
        array $departIds = [],
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        $query = Db::table('t_agent_visit')
            ->selectRaw("t_agent_visit.*");
        if ($departIds) {
            $query = $query->join('t_agent_visit_depart', function ($join) use ($departIds) {
                $where = ['t_agent_visit_depart.status' => DataStatus::REGULAR];

                $join = $join->on(
                    't_agent_visit_depart.visit_id',
                    '=',
                    't_agent_visit.id'
                )->where($where);
                if (!empty($departIds)) {
                    $join->whereIn('t_agent_visit_depart.depart_id', $departIds);
                }
            });
        } else {
            $query = $query->leftJoin('t_agent_visit_depart', function ($join) {
                $join = $join->on(
                    't_agent_visit_depart.visit_id',
                    '=',
                    't_agent_visit.id'
                )->where(['t_agent_visit_depart.status' => DataStatus::REGULAR]);
            });
        }

        AgentVisit::class;
        $query = $query->where([
                't_agent_visit.user_id' => $user->uid,
                't_agent_visit.status'  => DataStatus::REGULAR,
            ])->when($createStartAt != "", function ($query) use ($createStartAt) {
                $query->where('t_agent_visit.created_time', '>=', $createStartAt);
            })->when($createEndAt != '', function ($query) use ($createEndAt) {
                $query->where('t_agent_visit.created_time', '<=', $createEndAt);
            });
        if ($checkInStartAt) {
            $query = $query->where('t_agent_visit.check_in_time', '>=', $checkInStartAt);
        }
        if ($checkInEndAt) {
            $query = $query->where('t_agent_visit.check_in_time', '<=', $checkInEndAt);
        }
        if ($checkOutStartAt) {
            $query = $query->where('t_agent_visit.check_out_time', '>=', $checkOutStartAt);
        }
        if ($checkOutEndAt) {
            $query = $query->where('t_agent_visit.check_out_time', '<=', $checkOutEndAt);
        }
        if ($checkType) {
            $query = $query->where('t_agent_visit.check_status', $checkType);
        }
        if ($visitType) {
            $query = $query->where('t_agent_visit.visit_type', $visitType);
        }
        if ($hospitalId) {
            $query = $query->where('t_agent_visit.hospital_id', $hospitalId);
        }

        return $query->limit($limit)
            ->groupBy(['t_agent_visit.id'])
            ->orderByDesc('t_agent_visit.id')
            ->paginate($limit, ['t_agent_visit.*'], '', $current);
    }

    /**
     * @param $visitId
     * @return AgentVisit|null
     */
    public static function getVisitById($visitId): ?AgentVisit
    {
        return AgentVisit::where([
            'id' => $visitId,
            'status' => DataStatus::REGULAR
        ])->first();
    }


    public static function addCheckIn(
        Users $users,
        $visitType,
        $hospitalId,
        AgentVisitGeo $geo,
        $geoOpt = 0,
        $geoComment = "",
        $checkResult = false,
        $specialId = 0,
        $specialComment = "",
        $checkInTime = ""
    ): AgentVisit {
        $now = date('Y-m-d H:i:s');
        $specialId = (int)$specialId;
        $o = new AgentVisit();
        $o->user_id = $users->uid;
        $o->visit_type = $visitType;
        $o->hospital_id = $hospitalId;
        $o->check_in_geo = $geo->id;
        $o->check_in_geo_opt = $geoOpt;
        $o->check_in_geo_comment = $geoComment;
        $o->status = DataStatus::REGULAR;
        $o->check_status = AgentVisit::CHECK_STATUS_CHECK_IN;
        $o->check_in_time = $checkInTime ? $checkInTime : $now;
        $o->created_time = $o->modify_time =  $now;
        $o->check_in_geo_over = $checkResult ? 0 : 1;
        $o->visit_special_id = $specialId;
        $o->visit_special_comment = $specialId > 0 ? (string)$specialComment : '';
        $o->is_collaborative = 0;
        $o->is_supplement = $checkInTime ? 1 : 0;
        $o->save();

        return $o;
    }

    /**
     * @param AgentVisit $visit
     * @param AgentVisitGeo|null $geo
     * @param string $result
     * @param string $next
     * @param int $geoOpt
     * @param string $geoComment
     * @param bool $geoCheckResult
     * @return int
     */
    public static function checkOut(
        AgentVisit $visit,
        AgentVisitGeo $geo = null,
        $result = "",
        $next = "",
        $geoOpt = 0,
        $geoComment = "",
        $geoCheckResult = false,
        $isCollaborative = 0,
        $checkOutTime = ""
    ): int {
        $now = date('Y-m-d H:i:s');
        return AgentVisit::where([
            'id' => $visit->id,
            'check_status' => AgentVisit::CHECK_STATUS_CHECK_IN,
            'status' => DataStatus::REGULAR
        ])->update([
            'visit_result' => $result,
            'next_visit_purpose' => $next,
            'check_out_time' => $checkOutTime ? $checkOutTime : $now,
            'check_status' => AgentVisit::CHECK_STATUS_CHECK_OUT,
            'check_out_geo_opt' => $geoOpt,
            'check_out_geo_comment' => $geoComment,
            'check_out_geo' => $geo ? $geo->id : 0,
            'modify_time' => $now,
            'check_out_geo_over' => $geoCheckResult ? 0 : 1,
            'is_collaborative' => $isCollaborative
        ]);
    }

    /**
     * @param AgentVisit $visit
     * @param $visitType
     * @param $hospitalId
     * @param AgentVisitGeo $geo
     * @param false $checkResult
     * @param int $geoOpt
     * @param string $geoComment
     * @return int
     */
    public static function editCheckIn(
        AgentVisit $visit,
        $visitType,
        $hospitalId,
        AgentVisitGeo $geo,
        $checkResult = false,
        $geoOpt = 0,
        $geoComment = ""
    ): int {
        $now = date('Y-m-d H:i:s');
        return AgentVisit::where([
            'id' => $visit->id,
            'check_status' => AgentVisit::CHECK_STATUS_CHECK_IN,
            'status' => DataStatus::REGULAR
        ])->update([
            'visit_type' => $visitType,
            'hospital_id' => $hospitalId,
            'check_in_geo_opt' => $geoOpt,
            'check_in_geo_comment' => $geoComment,
            'check_in_geo' => $geo ? $geo->id : 0,
            'modify_time' => $now,
            'check_in_geo_over' => $checkResult ? 0 : 1
        ]);
    }

    /**
     * 编辑签出.
     *
     * @param AgentVisit $visit
     * @param AgentVisitGeo $geo
     * @param string $result
     * @param string $next
     * @param int $geoOpt
     * @param string $geoComment
     * @param false $geoCheckResult
     * @return int
     */
    public static function editCheckOut(
        AgentVisit $visit,
        AgentVisitGeo $geo,
        $result = "",
        $next = "",
        $geoOpt = 0,
        $geoComment = "",
        $geoCheckResult = false,
        $isCollaborative = 0
    ): int {
        return AgentVisit::where([
            'id' => $visit->id,
            'check_status' => AgentVisit::CHECK_STATUS_CHECK_OUT,
            'status' => DataStatus::REGULAR
        ])->update([
            'visit_result' => $result,
            'next_visit_purpose' => $next,
            'check_out_geo_opt' => $geoOpt,
            'check_out_geo_comment' => $geoComment,
            'check_out_geo' => $geo ? $geo->id : 0,
            'modify_time' => date('Y-m-d H:i:s'),
            'check_out_geo_over' => $geoCheckResult ? 0 : 1,
            'is_collaborative' => $isCollaborative
        ]);
    }
}
