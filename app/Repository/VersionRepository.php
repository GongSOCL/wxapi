<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Qa\YouyaoAppVersion;

class VersionRepository
{
    /**
     * @param $osType
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getVersion($osType)
    {
        return YouyaoAppVersion::where([
            'os_type' => $osType==1 ? 'Android' : 'IOS'
        ])->latest()->first();
    }
}
