<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\Agent;
use App\Model\Qa\Company;
use App\Model\Qa\Region;
use App\Model\Qa\Users;
use Hyperf\Utils\Collection;

class AgentRepository
{
    /**
     * @param $yyid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getAgentName($yyid)
    {
        return Agent::where('u_yyid', $yyid)->first();
    }

    public static function findByUser(Users $users): ?Agent
    {
        return Agent::where('u_yyid', $users->yyid)
            ->first();
    }

    public static function addUserInfoFromVerify(
        Users $user,
        $trueName,
        Region $province,
        $workingAge,
        $direction = [],
        $departIds = [],
        $supplierInfo = [],
        $inviteAid = 0,
        $roleType = 0
    ): Agent {
        $o = new Agent();
        $o->yyid = Helper::yyidGenerator();
        $o->u_yyid = $user->yyid;
        $o->truename = $trueName;
        $o->address = "";
        $o->serve_direction = implode(',', $direction);
        $o->expertise_ids = "";
        $o->familiar_offices = implode(',', $departIds);
        $o->working_age = $workingAge;
        $o->service_region = $province->yyid;
        $o->created = time();
        $o->update_date = date('Y-m-d H:i:s');
        $o->status = DataStatus::DELETE;
        $o->supplier_id = $supplierInfo['supplier_id'];
        $o->is_supplier = $supplierInfo['isSupplier'];
        $o->is_supplier_leader = $supplierInfo['isSupplierLeader'];
        $o->invite_aid = $inviteAid;
        $o->submitted_role_type = $roleType ? $roleType : 0;

        if (!$o->save()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "代表认证信息添加失败");
        }
        return $o;
    }

    public static function updateUserInfoFromVerify(
        Users $user,
        $trueName,
        Region $province,
        $workingAge,
        $direction = [],
        $departIds = [],
        $supplierInfo = [],
        $inviteAid = 0,
        $roleType = 0
    ) {
        return Agent::where('u_yyid', $user->yyid)
            ->update([
                'truename' => $trueName,
                'serve_direction' => implode(',', $direction),
                'address' => '',
                'expertise_ids' => '',
                'familiar_offices' => implode(',', $departIds),
                'working_age' => $workingAge,
                'service_region' => $province->yyid,
                'supplier_id' => $supplierInfo['supplier_id'],
                'is_supplier' => $supplierInfo['isSupplier'],
                'is_supplier_leader' => $supplierInfo['isSupplierLeader'],
                'invite_aid' => $inviteAid,
                'submitted_role_type' => $roleType ? $roleType : 0
            ]);
    }

    public static function getAgentByUser(Users $users): ?Agent
    {
        return Agent::where([
            'u_yyid' => $users->yyid
        ])->first();
    }

    public static function agree(Agent $agent, $updateData, $realRole)
    {
        $updateData['status'] = DataStatus::REGULAR;
        $updateData['real_role_type'] = $realRole;
        $updateData['agreement_time'] = date('Y-m-d H:i:s');
        return Agent::where('aid', $agent->aid)
            ->update($updateData);
    }

    public static function getMultiUserAgentInfo($userYYIDS): Collection
    {
        return Agent::whereIn('u_yyid', $userYYIDS)
            ->where('status', DataStatus::REGULAR)
            ->groupBy('u_yyid')
            ->select(['u_yyid', 'truename'])
            ->get();
    }
}
