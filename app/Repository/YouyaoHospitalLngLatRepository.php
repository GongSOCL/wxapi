<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\YouyaoHospital;
use App\Model\Qa\YouyaoHospitalLngLat;

class YouyaoHospitalLngLatRepository
{
    public static function getHospitalGeo(YouyaoHospital $hospital): ?YouyaoHospitalLngLat
    {
        return YouyaoHospitalLngLat::query()
            ->where('hospital_id', $hospital->id)
            ->where('status', '1')
            ->first();
    }

    public static function checkHospitalPointExists($hospitalId): bool
    {
        return YouyaoHospitalLngLat::query()
            ->where('hospital_id', $hospitalId)
            ->where('status', '1')
            ->exists();
    }
}