<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\Company;
use App\Model\Qa\VisitSupplementCompany;
use Carbon\Carbon;
use Hyperf\Contract\LengthAwarePaginatorInterface;

class VisitSupplementCompanyRepository
{
    public static function checkCompanyIsOk($companyId): bool
    {
        return VisitSupplementCompany::query()
            ->where('company_id', $companyId)
            ->where('status', DataStatus::REGULAR)
            ->exists();
    }

    public static function getCompanyWithPage($keywords = "", $current = 1, $limit = 10): LengthAwarePaginatorInterface
    {
        return VisitSupplementCompany::query()
            ->join('t_company as c', 'c.id', '=', 't_visit_supplement_company.company_id')
            ->where('t_visit_supplement_company.status', DataStatus::REGULAR)
            ->when($keywords != "", function ($query) use ($keywords) {
                $query->where('c.name', 'like', "%{$keywords}%");
            })->orderByDesc('t_visit_supplement_company.id')
            ->paginate($limit, ['t_visit_supplement_company.id', 'c.id as company_id', 'c.name'], '', $current);
    }

    public static function addCompanySupplement($companyId)
    {
        $o = new VisitSupplementCompany();
        $o->company_id = $companyId;
        $o->status = DataStatus::REGULAR;
        $o->created_at = $o->updated_at = Carbon::now();
        $o->save();
    }

    public static function delCompanySupplement($id): int
    {
        return VisitSupplementCompany::query()
            ->where('id', $id)
            ->where('status', DataStatus::REGULAR)
            ->update([
                'status' => DataStatus::DELETE,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function getById($id): ?VisitSupplementCompany
    {
        return VisitSupplementCompany::query()
            ->where('id', $id)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }
}
