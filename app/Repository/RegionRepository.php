<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\Region;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Collection;
use function Swoole\Coroutine\Http\get;

class RegionRepository
{
    /**
     * @Inject
     * @var Region
     */
    private $regionModel;

    public static function getByYYID($yyid): ?Region
    {
        return Region::where([
            'yyid' => $yyid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * 通过yyid获取级联地区信息
     * @param $regionyyid
     * @return object
     */
    public function getRegion($regionyyid)
    {
        return $this->regionModel::query()
            ->from('t_region as ra')
            ->leftJoin('t_region as rb', 'ra.pid_yyid', '=', 'rb.yyid')
            ->leftJoin('t_region as rc', 'rb.pid_yyid', '=', 'rc.yyid')
            ->select('ra.region_name as area', 'rb.region_name as city', 'rc.region_name as province')
            ->where('ra.yyid', $regionyyid)
            ->first();
    }

    /**
     * 根据id获取区域
     * @param $id
     * @return Region|null
     */
    public static function getById($id): ?Region
    {
        return Region::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * 获取下一级区域
     * @param $id
     * @return Collection
     */
    public static function getNextOne($id): Collection
    {
        return Region::where([
            'pid' => $id,
            'status' => DataStatus::REGULAR
        ])->get();
    }
}
