<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DrugProduct;
use App\Model\Qa\RepsTransactionP;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;
use InvalidArgumentException;

class RepsTransactionPRepository
{

    public static function getTransHospitals(
        DrugProduct $product,
        array $hospitalYYIDS,
        string $keywords = ""
    ): Collection {
        $query = RepsTransactionP::query()
            ->join("t_youyao_hospital as h", 'h.yyid', '=', 't_reps_transaction_p.hospital_yyid')
            ->where('t_reps_transaction_p.product_yyid', $product->yyid)
            ->whereIn('t_reps_transaction_p.hospital_yyid', $hospitalYYIDS);
        if ($keywords) {
            $query = $query->where("h.hospital_name", "like", "%{$keywords}%");
        }

        return $query->select(["h.*"])->get();
    }

    /**
     * 获取最近的流向数据
     * @param int $limit
     */
    public static function getLastTransMonths(int $limit): Collection
    {
        return RepsTransactionP::query()
            ->where('status', DataStatus::REGULAR)
            ->orderByDesc('transaction_month')
            ->select(['transaction_month'])
            ->groupBy(['transaction_month'])
            ->limit($limit)
            ->get();
    }

    /**
     * 检查对应月份是否已经结算了
     * @param mixed $month
     * @return bool
     * @throws InvalidArgumentException
     */
    public static function checkMonthTransed($month): bool
    {
        return RepsTransactionP::query()
            ->where([
                'transaction_month' => $month,
                'status' => DataStatus::REGULAR
            ])->exists();
    }

    /**
     * 获取单个月份流向数据
     * @param DrugProduct $product
     * @param mixed $month
     * @param array $hospitalYYIDS
     * @return Collection
     * @throws InvalidArgumentException
     */
    public static function getTransactionsMonthList(DrugProduct $product, $month, array $hospitalYYIDS): Collection
    {
        return RepsTransactionP::query()
            ->where([
                'product_yyid' => $product->yyid,
                'transaction_month' => $month,
                'status' => DataStatus::REGULAR
            ])->whereIn('hospital_yyid', $hospitalYYIDS)
            ->groupBy(['transaction_date', 'hospital_yyid'])
            ->select(['transaction_date', 'hospital_yyid', Db::raw("sum(num) as num")])
            ->orderByDesc('transaction_date')
            ->get();
    }

    /**
     * 获取多个月份流向数据
     * @param DrugProduct $product
     * @param array $months
     * @param array $hospitalYYIDS
     * @return Collection
     * @throws InvalidArgumentException
     */
    public static function getTransactionsMontshList(
        DrugProduct $product,
        array $months,
        array $hospitalYYIDS
    ): Collection {
        return RepsTransactionP::query()
            ->where([
                'product_yyid' => $product->yyid,
                'status' => DataStatus::REGULAR
            ])->whereIn('transaction_month', $months)
            ->whereIn('hospital_yyid', $hospitalYYIDS)
            ->groupBy(['transaction_month', 'transaction_date', 'hospital_yyid'])
            ->select(['transaction_month', 'transaction_date', 'hospital_yyid', Db::raw("sum(num) as num")])
            ->orderByDesc('transaction_date')
            ->get();
    }
}
