<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DealerHospitalSale;

class DealerHospitalSalesRepository
{
    /**
     * @param $companyYyid
     * @param $staffCode
     * @return \Hyperf\Utils\Collection
     */
    public static function getReportInfo($companyYyid, $staffCode)
    {
        return DealerHospitalSale::select([
                'id','type','month','staff_code','hospital_id','product_id','sku_id',
                'month_target','month_sales','month_ach','ly_sales','month_gr','eff_call_num'
            ])
            ->where([
                'company_yyid' => $companyYyid,
                'staff_code' => $staffCode,
                'status' => DataStatus::REGULAR
                ])
            ->orderBy('id', 'desc')
            ->get();
    }
}
