<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DrugProduct;
use App\Model\Qa\DrugSku;
use App\Model\Qa\RepsServeScope;
use App\Model\Qa\Users;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class DrugSkuRepository
{
    public static function getUserServeDrugs(Users $users): Collection
    {
        $sub = DrugSku::query()
            ->join("t_reps_serve_scope as trss", "trss.drug_yyid", "=", "t_drug_sku.yyid")
            ->where('t_drug_sku.status', DataStatus::REGULAR)
            ->where('trss.user_yyid', $users->yyid)
            ->whereIn("trss.status", [
                RepsServeScope::STATUS_ACCESS,
                RepsServeScope::STATUS_REMOVE_APPLY,
                RepsServeScope::STATUS_REMOVE_FAILED
            ])->select([Db::raw('distinct t_drug_sku.id')]);

        return DrugSku::query()
            ->joinSub($sub, 'sb', 'sb.id', '=', 't_drug_sku.id')
            ->get();
    }

    public static function getFromUserAndProduct(
        Users $users,
        DrugProduct $product,
        array $hospitalYYID = []
    ): Collection {
        $sub = DrugSku::query()
            ->join("t_reps_serve_scope as trss", "trss.drug_yyid", "=", "t_drug_sku.yyid")
            ->where('t_drug_sku.status', DataStatus::REGULAR)
            ->where('t_drug_sku.product_yyid', $product->yyid)
            ->where('trss.user_yyid', $users->yyid)
            ->whereIn("trss.status", [
                RepsServeScope::STATUS_ACCESS,
                RepsServeScope::STATUS_REMOVE_APPLY,
                RepsServeScope::STATUS_REMOVE_FAILED
            ])->select([Db::raw('distinct t_drug_sku.id')]);
        if (!empty($hospitalYYID)) {
            $sub = $sub->whereIn('trss.hospital_yyid', $hospitalYYID);
        }

        return DrugSku::query()
            ->joinSub($sub, 'sb', 'sb.id', '=', 't_drug_sku.id')
            ->get();
    }
}
