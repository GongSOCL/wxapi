<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ExamCode;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ComplianceQuestionItem;
use App\Model\Qa\ExaminationResult;
use App\Model\Qa\TComplianceExamRandItem;
use App\Model\Qa\TComplianceQuestionItemsFraction;
use App\Model\Qa\TComplianceTestExamFraction;
use App\Model\Qa\UpExamDate;
use App\Model\Qa\Users;
use Hyperf\DbConnection\Db;

class ExaminationResultRepository
{
    /**
     * 获取指定考试下所有题目的总分
     * @param $examid
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function updateStatusByexamId($examPassed, $userYYid, $examid, $status)
    {
        if ($status == 4) {
            # 查询考试重考次数，结束时间， 结束后 查补考
            $examData = ComplianceExam::where([
                'id' => $examid,
                'status' => DataStatus::REGULAR
            ])->first()->toArray();
            # 判断考试时间是否结束

            # 查询是否有补考
            $upExam = UpExamDate::where(
                [
                    ['status','=',DataStatus::REGULAR],
                    ['exam_id','=',$examid],
                    ['up_exam_end','!=',null],
                    ['up_exam_start','!=',null]
                ]
            )->first();

            $examUserInfo = ExaminationResult::where(['exam_id'=>$examid,'user_yyid'=>$userYYid])->first();
            $upStatusUpExam = 0;
            $ExamNum = 0;
            if (!empty($examData['exam_end'])) {
                if ($examData['exam_end'] >= date('Y-m-d H:i:s')) {   # 考试未结束时
                    if ($examUserInfo['status'] == 3) {
                        $upExam = $upExam->toArray();
                        $ExamNum = $examUserInfo['exam_num'];
                        $upStatusUpExam = $examUserInfo['up_exam'] + 1;
                        if (($upExam['up_exam_num']) == $upStatusUpExam) {
                            $statusRes = 4;
                        } else {
                            $statusRes = 3;
                        }
                    } else {
                        if ($examData['exam_num'] && $examData['exam_num'] > 0) {
                            $ExamNum = $examUserInfo['exam_num'] + 1;
                            if ($upExam) {
                                if (($examData['exam_num']) == $ExamNum) {
//                                    if($upExam->exam_num > 0){
//                                        $statusRes = 3; # 开启补考
//                                    }else{
//                                        $statusRes = 4;
//                                    }
                                    $statusRes = 3; # 开启补考
                                } else {
                                    $statusRes = 1;
                                }
                            } else {  # 没补考 且 考试次数用完
                                if (($examData['exam_num']) == $ExamNum) {
                                    $statusRes = 4;
                                } else {
                                    $statusRes = 1;
                                }
                            }
                        } elseif (!$examData['exam_num']) {
                            if ($examUserInfo['exam_num'] == 0) {
                                $statusRes = 1;
                            } else {
                                $statusRes = 4;
                            }
                        }
                    }

                    return ExaminationResult::where(['exam_id'=>$examid,'user_yyid'=>$userYYid])
                        ->update([
                            'fraction' => $examPassed['fraction'],
                            'rights_num' => $examPassed['nums'],
                            'status' => $statusRes,   # 正常考试时间范围内
                            'up_exam' => $upStatusUpExam,
                            'exam_num' => $ExamNum,
                            'updated_at' => date('Y-m-d H:i:s', time())
                        ]);
                } else {
                    # 考试结束 判断补考
                    if ($upExam) {
                        $upExam = $upExam->toArray();
                        # 查询是否已经补考
                        $where  = " (status = 3 or status =1 ) and exam_id = ".$examid;
                        $upEcamUser = ExaminationResult::whereRaw($where)->first();
                        if ($upEcamUser) {
                            $upEcamUser = $upEcamUser->toArray();
                            # 补考次数  补考次数等于补考完次数 = 未通过
                            if ($upExam['up_exam_num'] && $upExam['up_exam_num'] > 0) {
                                $upExamNum = $upEcamUser['up_exam'] + 1;
                                if ($upExam['up_exam_num'] == $upExamNum) {
                                    $statusRes = 4; # 终止 未通过
                                } else {
                                    $statusRes = 3;
                                }
                            } else { # 无限补考
                                # 已经补考 还为通过 更新 up_exam 加一、 up_exam_num
                                if ($upEcamUser) {
                                    $upExamNum = $upEcamUser['up_exam']+1;
                                } else {
                                    $upExamNum = 0; # 需要补考 但还没考
                                }
                                $statusRes = 3;
                            }
                        } else {
                            $upExamNum = 0;
                            $statusRes = 3;
                        }

                        return ExaminationResult::where(['exam_id'=>$examid,'user_yyid'=>$userYYid])
                            ->update([
                                'fraction' => $examPassed['fraction'],
                                'rights_num' => $examPassed['nums'],
                                'status' => $statusRes,   # 有补考 则状态3
                                'up_exam' => $upExamNum,
                                'updated_at' => date('Y-m-d H:i:s', time())
                            ]);
                    } else {
                        # 没补考 直接写入不通过状态
                        return ExaminationResult::where(
                            [
                                'status'=>DataStatus::REGULAR,
                                'exam_id'=>$examid,
                                'user_yyid'=>$userYYid
                            ]
                        )
                            ->update([
                                'fraction' => $examPassed['fraction'],
                                'rights_num' => $examPassed['nums'],
                                'status' => $status,
                                'updated_at' => date('Y-m-d H:i:s', time())
                            ]);
                    }
                }
            } else {
                # 无限考试 判断考试次数 【无限时 不会有补考】
                if ($examData['exam_num']) {
                    $ExamNum = $examUserInfo['exam_num'] + 1;
                    if (($examData['exam_num']) == $ExamNum) {
                        $statusRes = 4;
                    } else {
                        $statusRes = 1;
                    }
                } else {
                    $statusRes = 3;
                }

                return ExaminationResult::where(['exam_id'=>$examid,'user_yyid'=>$userYYid])
                    ->update([
                        'fraction' => $examPassed['fraction'],
                        'status' => $statusRes,
                        'up_exam' => 0,
                        'exam_num' => $ExamNum,
                        'updated_at' => date('Y-m-d H:i:s', time())
                    ]);
            }
        } else {
            # 查询是否已经补考
            $upEcamUser = ExaminationResult::where(['status'=>3,'exam_id'=>$examid])->first();
            $status = 2;
            $upStatusUpExam = 0;
            if ($upEcamUser) {
                $upEcamUser = $upEcamUser->toArray();
                $upStatusUpExam = $upEcamUser['up_exam']+1;
                $status = 6;
            }
            # 更新通过状态
            return ExaminationResult::where(['exam_id'=>$examid,'user_yyid'=>$userYYid])
                ->update([
                    'fraction' => $examPassed['fraction'],
                    'status' => $status,
                    'up_exam' => $upStatusUpExam,
                    'updated_at' => date('Y-m-d H:i:s', time())
                ]);
        }
    }


    public static function getUserStatus($examids, $userYYid)
    {
        return ExaminationResult::where(['user_yyid'=>$userYYid])
            ->whereIn('exam_id', $examids)
            ->get();
    }


    /**
     * 获取每道题的分数
     * @param $examid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getTestExamFraction($examid)
    {
        return TComplianceTestExamFraction::where(['status'=>DataStatus::REGULAR,'exam_id'=>$examid])
            ->select(["item_id","question_bank_id","fraction","type"])->get();
    }

    /**
     * 获取每道题的分数
     * @param $examid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getTestPaperQuestionItemFraction($examid, $rightData)
    {
        return Db::table('t_compliance_question_items as a')
            ->join("t_compliance_question as b", "a.question_yyid", "=", "b.yyid")
            ->join("t_compliance_test_exam_fraction as c", "b.id", "=", "c.question_bank_id")
            ->whereIn('a.id', $rightData)
            ->where('c.exam_id', $examid)
            ->select('c.fraction')
            ->get();
    }

    /**
     * @param $examid
     * @param $rightData
     * @return \Hyperf\Utils\Collection
     */
    public static function getTestPaperItemFraction($examid, $rightData)
    {
        return Db::table('t_compliance_question_items as a')
            ->join("t_compliance_question as b", "a.question_yyid", "=", "b.yyid")
            ->join("t_compliance_question_items_fraction as c", "b.id", "=", "c.question_bank_id")
            ->whereIn('a.id', $rightData)
            ->select('c.fraction')
            ->get();
    }


    /**
     * @param $user
     * @param $examIds
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getUpExamStatus($user, $examIds)
    {
        return ExaminationResult::where('exam_num', '>', 0)
            ->where(function ($query) use ($user) {
                $query->where('user_yyid', $user->uid)
                    ->orWhere(function ($query) use ($user) {
                        $query->where('user_yyid', $user->yyid);
                    });
            })
            ->whereIn('exam_id', $examIds)
            ->select('exam_id', 'exam_num')
            ->get();
    }

    /**
     * @param $examIds
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getUpExam($examIds)
    {
        return UpExamDate::where(
            [
                ['status','=',DataStatus::REGULAR],
                ['exam_id','=',$examIds],
                ['up_exam_end','!=',null],
                ['up_exam_start','!=',null]
            ]
        )
            ->first();
    }

    /**
     * @param $exam
     * @param $user
     * @param $test_id
     * @param $questionItemId
     */
    public static function addExamRandItem($exam, $user, $test_id, $questionItemId)
    {
        $randItem = TComplianceExamRandItem::where([
            ['status','=',DataStatus::REGULAR],
            ['test_id','=',$test_id],
            ['exam_id','=',$exam->id],
            ['uid','=',$user->uid],
            ['platform','=',ExamCode::YOUYAO],

        ])->first();

        if (!$randItem) {
            $insertData = [];
            foreach ($questionItemId as $k => $item_id) {
                $insertData[$k]['uid'] = $user->uid;
                $insertData[$k]['exam_id'] = $exam->id;
                $insertData[$k]['test_id'] = $test_id;
                $insertData[$k]['item_id'] = $item_id;
                $insertData[$k]['platform'] = ExamCode::YOUYAO;
                $insertData[$k]['created_at'] = date('Y-m-d H:i:s');
            }
            TComplianceExamRandItem::insert($insertData);
        }
    }

    public static function getExamRandItem($exam, $user, $test_id)
    {
        return TComplianceExamRandItem::where([
            ['status','=',DataStatus::REGULAR],
            ['test_id','=',$test_id],
            ['exam_id','=',$exam->id],
            ['uid','=',$user->uid],
        ])->get();
    }
}
