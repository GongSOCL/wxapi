<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DrugManual;
use App\Model\Qa\DrugProduct;
use Hyperf\Utils\Collection;

class DrugManualRepository
{
    public static function getProductManuals(DrugProduct $product): Collection
    {
        return DrugManual::where([
            'product_yyid' => $product->yyid,
            'status' => DataStatus::REGULAR
        ])->orderBy("manual_rank", "ASC")
            ->get();
    }
}
