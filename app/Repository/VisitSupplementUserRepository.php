<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\Users;
use App\Model\Qa\VisitSupplementUser;
use Carbon\Carbon;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\DbConnection\Db;
use Swoole\FastCGI\Record\Data;

class VisitSupplementUserRepository
{
    public static function checkUserIsOk($userId): bool
    {
        return VisitSupplementUser::query()
            ->where('user_id', $userId)
            ->where('status', DataStatus::REGULAR)
            ->exists();
    }

    public static function listSupplementUserWithPage(
        $keywords = "",
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        return VisitSupplementUser::query()
            ->join('t_users as tu', 'tu.uid', '=', 't_visit_supplement_users.user_id')
            ->where('t_visit_supplement_users.status', DataStatus::REGULAR)
            ->when($keywords != "", function ($query) use ($keywords) {
                if (preg_match('/^\d+$/', $keywords)) {
                    $query->where('mobile_num', 'like', "%{$keywords}%");
                } else {
                    $query->where('true_name', 'like', "%{$keywords}%");
                }
            })->where('tu.v_status', DataStatus::REGULAR)
            ->paginate($limit, ['t_visit_supplement_users.id', 'tu.uid', 'tu.name', 'tu.true_name', 'tu.mobile_num'], '', $current);
    }

    public static function getById($id): ?VisitSupplementUser
    {
        return VisitSupplementUser::query()
            ->where('id', $id)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }

    public static function delById(int $id): int
    {
        return VisitSupplementUser::query()
            ->where('id', $id)
            ->where('status', DataStatus::REGULAR)
            ->update([
                'status' => DataStatus::DELETE,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function addUser($userId): VisitSupplementUser
    {
        $o = new VisitSupplementUser();
        $o->user_id = $userId;
        $o->status = DataStatus::REGULAR;
        $o->created_at = $o->updated_at = Carbon::now();
        $o->save();

        return $o;
    }

    public static function filterUserBySearch($keywords = '', $limit = 5)
    {
        $query = Users::query()
            ->leftJoin('t_visit_supplement_users as tvsu', function ($join) {
                $join->on('tvsu.user_id', '=', 't_users.uid')
                    ->on('tvsu.status', '=', Db::raw("1"));
            });
        if ($keywords) {
            if (preg_match('/^\d+$/', $keywords) == 1) {
                $query = $query->where('t_users.mobile_num', 'like', "{$keywords}%");
            } else {
                $query = $query->where('t_users.true_name', 'like', "%{$keywords}%");
            }
        }

        return $query->limit($limit)
            ->select(['t_users.*', 'tvsu.user_id'])
            ->havingRaw("tvsu.user_id is null")
            ->get();
    }
}
