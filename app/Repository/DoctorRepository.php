<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\Doctor;
use Hyperf\Database\Query\Expression;
use Hyperf\Di\Annotation\Inject;

class DoctorRepository
{
    /**
     * @Inject
     * @var Doctor
     */
    private $doctorModel;


    /**
     * 创建一条记录
     * @param $params
     * @param $hName
     * @param $dname
     */
    public function createOneInfo($params, $hName)
    {
        $data = [
            'yyid' => new Expression("replace(upper(uuid()),'-','')"),
            'u_yyid' => $params['user_yyid'],
            'name' => $params['true_name'],
            'job_title' => $params['job_title'],
            'position' => $params['position'],
            'gender' => $params['gender'],
            'expertise_ids' => $params['field_id'],
            'hospital_name' => $hName,
            'h_yyid' => $params['hospital_yyid'],
            'depart_name' => $params['depart_id'],
            'created_time' => date('Y-m-d H:i:s'),
            'modify_time' => date('Y-m-d H:i:s')
        ];
        $this->doctorModel::query()->insertGetId($data);
    }


    /**
     * @param $yyid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getVerifyInfo($yyid)
    {
        return $this->doctorModel::query()
            ->select(
                'id',
                'yyid',
                'u_yyid',
                'name as true_name',
                'gender',
                'job_title',
                'position',
                'expertise_ids as field_id',
                'hospital_name',
                'depart_name as depart_id',
                'status'
            )
            ->where('u_yyid', $yyid)
            ->first();
    }
}
