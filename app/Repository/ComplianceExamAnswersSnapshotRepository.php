<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ExamCode;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ComplianceExamAnswersSnapshot;
use App\Model\Qa\Users;
use Hyperf\Utils\Collection;

class ComplianceExamAnswersSnapshotRepository
{
    /**
     * 获取用户考试回顾记录
     * @param Users $users
     * @param ComplianceExam $exam
     * @return Collection
     */
    public static function getUserExamSnapshot(Users $users, ComplianceExam $exam): Collection
    {
        return ComplianceExamAnswersSnapshot::where('exam_id', $exam->id)
            ->where('user_id', $users->uid)
            ->where('platform', ExamCode::YOUYAO)
            ->get();
    }

    public static function clearExam(ComplianceExam $exam, Users $user)
    {
        return ComplianceExamAnswersSnapshot::where('exam_id', $exam->id)
            ->where('user_id', $user->uid)
            ->where('platform', ExamCode::YOUYAO)
            ->delete();
    }
}
