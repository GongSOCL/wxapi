<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DrugInterpret;
use App\Model\Qa\DrugProduct;
use Hyperf\Contract\LengthAwarePaginatorInterface;

class DrugInterpretRepository
{
    public static function getProductDrugInterprets(
        DrugProduct $product,
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        return DrugInterpret::where([
            'product_yyid' => $product->yyid,
            'status' => DataStatus::REGULAR
        ])->orderBy("rank", "ASC")
            ->paginate($limit, ['*'], '', $current);
    }

    public static function getById($id): ?DrugInterpret
    {
        return DrugInterpret::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();
    }
}
