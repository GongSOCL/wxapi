<?php


namespace App\Repository;

use App\Model\Qa\DrugSeries;
use App\Model\Qa\HospitalTransactionStatus;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class HospitalTransactionStatusRepository
{
    public static function getHospitalsBySeriesAndInterval(DrugSeries $series, $start, $end): Collection
    {
        return HospitalTransactionStatus::where([
            'series_yyid' => $series->yyid,
        ])->get();
    }

    public static function filterValidHospitalsBySeriesAndHospitals(
        DrugSeries $series,
        string $start,
        string $end,
        array $hospitalYYIDS
    ) {
        return HospitalTransactionStatus::where('series_yyid', $series->yyid)
            ->whereBetween('transaction_date', [$start, $end])
            ->whereIn('hospital_yyid', $hospitalYYIDS)
            ->select([Db::raw("distinct hospital_yyid")])
            ->get();
    }
}
