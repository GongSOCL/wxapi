<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\AgentVisit;
use App\Model\Qa\AgentVisitGeo;
use Hyperf\Utils\Collection;

class AgentVisitGeoRepository
{
    public static function addGeoPos(
        $lnt,
        $lat,
        $amapLng,
        $amapLat,
        $position,
        $coordinateType = DataStatus::COORDINATE_GPS,
        $isFakeGeo = false
    ): AgentVisitGeo {
        $o = new AgentVisitGeo();
        $o->longitude = $lnt;
        $o->latitude = $lat;
        $o->amap_latitude = $amapLat;
        $o->amap_longitude = $amapLng;
        $o->origin_coordinate_type = $coordinateType;
        $o->sign_pos = $position;
        $o->status = DataStatus::REGULAR;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');
        $o->is_fake_geo = $isFakeGeo ? 1 : 0;
        $o->save();

        return $o;
    }

    public static function editGeoPos(
        AgentVisitGeo $geo,
        $lnt,
        $lat,
        $amapLng,
        $amapLat,
        $position,
        $coordinateType = DataStatus::COORDINATE_GPS,
        $isFakeGeo = false
    ): bool {
        $geo->longitude = $lnt;
        $geo->latitude = $lat;
        $geo->amap_longitude = $amapLng;
        $geo->amap_latitude = $amapLat;
        $geo->sign_pos = $position;
        $geo->origin_coordinate_type = $coordinateType;
        $geo->modify_time = date('Y-m-d H:i:s');
        $geo->is_fake_geo = $isFakeGeo ? 1 : 0;
        return $geo->save();
    }

    public static function getGeoByIds(array $ids): Collection
    {
        return AgentVisitGeo::query()
            ->whereIn('id', $ids)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }
}
