<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ComplianceQuestionItem;
use Hyperf\Utils\Collection;

class ComplianceQuestionItemsRepository
{
    /**
     * 根据题库yyid获取问题列表
     *
     * @param array $questionYYIDS
     */
    public static function getItemsByQuestionYYIDS(array $questionYYIDS, $examItem)
    {
        if ($examItem) {
            # 随机出题 题库出题
            $qi = [];
            foreach ($examItem as $k => $item) {
                # 随机取N题
                $qi[] =  ComplianceQuestionItem::where('question_yyid', $item['yyid'])
                   ->inRandomOrder()
                   ->limit($item['item_num'])
                   ->get()
                ->toArray();
            }

            # 拼二维
            $qis = [];
            foreach ($qi as $items) {
                foreach ($items as $itm) {
                    array_push($qis, $itm);
                }
            }
            return $qis;
        } else {
            return ComplianceQuestionItem::whereIn('question_yyid', $questionYYIDS)
                ->where('status', DataStatus::REGULAR)
                ->orderBy('sort')
                ->get()
                ->toArray();
        }
    }

    /**
     * 通过yyid获取问题列表
     * @param array $YYIDS
     * @return \Hyperf\Utils\Collection
     */
    public static function getItemsByYYIDS(array $YYIDS)
    {
        return ComplianceQuestionItem::whereIn('yyid', $YYIDS)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    /**
     * 获取id获取试题内容
     * @param $itemYYID
     * @return ComplianceQuestionItem|null
     */
    public static function getItemByYYID($itemYYID)
    {
        return ComplianceQuestionItem::where('yyid', $itemYYID)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }

    public static function getItemsById($questionId): ?ComplianceQuestionItem
    {
        return ComplianceQuestionItem::where('id', $questionId)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }

    public static function getItemsByIds(array $questionIds): Collection
    {
        return ComplianceQuestionItem::whereIn('id', $questionIds)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }
}
