<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\Company;
use App\Model\Qa\DepartmentImport;
use App\Model\Qa\DepartmentUser;
use App\Model\Qa\ImportUser;
use App\Model\Qa\OrgBindingUser;
use App\Model\Qa\Users;
use Carbon\Carbon;
use Swoole\Http\Status;

class DepartmentUserRepository
{
    public static function batchUpdateAccountByImport(Company $company, Users $users, $userUniq): int
    {
        return DepartmentUser::query()
            ->where('company_id', $company->id)
            ->where('user_uniq', $userUniq)
            ->where('account_uid', 0)
            ->update([
                'account_uid' => $users->uid,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function getByCompanyAndUserUniq(Company $company, string $userUniq)
    {
        return DepartmentUser::query()
            ->where('company_id', $company->id)
            ->where('user_uniq', $userUniq)
            ->where('account_uid', 0)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }
}
