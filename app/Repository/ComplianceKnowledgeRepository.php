<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ComplianceKnowledge;
use Hyperf\Database\Model\Collection;

class ComplianceKnowledgeRepository
{

    public static function searchKnowledge($keywords, array $dirYYIDs): Collection
    {
        return ComplianceKnowledge::where('status', DataStatus::REGULAR)
            ->whereIn('dir_yyid', $dirYYIDs)
            ->where('title', 'like', "%{$keywords}%")
            ->get();
    }

    public static function getDirsKnowledge(array $subDirYYIDS): Collection
    {
        return ComplianceKnowledge::where('status', DataStatus::REGULAR)
            ->whereIn('dir_yyid', $subDirYYIDS)
            ->get();
    }

    public static function getById(int $id): ?ComplianceKnowledge
    {
        return ComplianceKnowledge::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();
    }
}
