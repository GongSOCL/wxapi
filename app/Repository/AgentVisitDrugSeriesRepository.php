<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\AgentVisit;
use App\Model\Qa\AgentVisitDrugSeries;
use Hyperf\Database\Model\Builder;
use Hyperf\Utils\Collection;
use Swoole\FastCGI\Record\Data;

class AgentVisitDrugSeriesRepository
{
    /**
     * 批量添加拜访记录关联药品.
     *
     * @param AgentVisit $visit
     * @param array $seriesIds
     * @return bool
     */
    public static function batchAddVisitSeries(AgentVisit $visit, array $seriesIds): bool
    {
        $data = [];
        $now = date('Y-m-d H:i:s');
        foreach ($seriesIds as $seriesId) {
            $data[] = [
               'visit_id' => $visit->id,
               'series_id' => $seriesId,
               'status' => DataStatus::REGULAR,
               'created_time' => $now,
               'modify_time' => $now
            ];
        }

        return AgentVisitDrugSeries::insert($data);
    }

    public static function getVisitDrugSeries(AgentVisit $visit): Collection
    {
        return AgentVisitDrugSeries::where([
            'visit_id' => $visit->id,
            'status' => DataStatus::REGULAR
        ])->get();
    }

    public static function batchDeleteVisitDrugs(AgentVisit $visit, array $seriesIds): int
    {
        return AgentVisitDrugSeries::where([
            'visit_id' => $visit->id,
            'status' => DataStatus::REGULAR
        ])->whereIn('series_id', $seriesIds)
            ->update([
                'status' => DataStatus::DELETE,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * @param $newCheckOutId
     * @param $yids
     * @return bool
     */
    public static function addVisitDrugs($newCheckOutId, $yids): bool
    {
        $data = [];
        foreach ($yids as $key => $value) {
            $data[$key]['visit_id'] = $newCheckOutId;
            $data[$key]['series_id'] = $value;
            $data[$key]['status'] = DataStatus::REGULAR;
            $data[$key]['created_time'] = date('Y-m-d H:i:s', time());
            $data[$key]['modify_time'] = date('Y-m-d H:i:s', time());
        }

        return AgentVisitDrugSeries::insert($data);
    }
}
