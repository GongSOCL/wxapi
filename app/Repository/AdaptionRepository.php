<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\Adaption;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;
use const OpenTracing\Tags\DATABASE_STATEMENT;

class AdaptionRepository
{
    public static function add($name, $nameEn, $parentId = 0, $id = 0): Adaption
    {
        $o = new Adaption();
        $o->name = $name;
        $o->name_en = $nameEn;
        $o->status = DataStatus::REGULAR;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');
        $o->parent_id = $parentId;
        if ($id > 0) {
            $o->id = $id;
        }
        $o->save();
        return $o;
    }

    /**
     * @param int $field_id
     * @return Adaption|null
     */
    public static function getById(int $field_id): ?Adaption
    {
        return Adaption::where([
            'id' => $field_id,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getFields($keywords, $current, $limit): LengthAwarePaginatorInterface
    {
        $query = Adaption::where("parent_id", 0);
        if ($keywords) {
            $query->where("name", "like", "%{$keywords}%");
        }

        return $query->paginate($limit, ['*'], '', $current);
    }

    public static function getAdaptions($keywords, $current, $limit): LengthAwarePaginatorInterface
    {
        $query = Adaption::where("parent_id", '>', 0);
        if ($keywords) {
            $query->where("name", "like", "%{$keywords}%");
        }

        return $query->paginate($limit, ['*'], '', $current);
    }

    public static function getFieldByIds(array $ids): Collection
    {
        return Adaption::whereIn('id', $ids)
            ->where("parent_id", 0)
            ->get();
    }

    public static function getAdaptionByIds(array $ids): Collection
    {
        return Adaption::whereIn('id', $ids)
            ->where("parent_id", '>', 0)
            ->get();
    }

    public static function getByFieldsYYIDS(array $yyids): Collection
    {
        return Adaption::whereIn('yyid', $yyids)
            ->where('parent_id', 0)
            ->get();
    }

    public static function getByAdaptionsYYIDS(array $yyids): Collection
    {
        return Adaption::whereIn('yyid', $yyids)
            ->where('parent_id', '!=', 0)
            ->get();
    }
}
