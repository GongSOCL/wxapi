<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\Company;
use App\Model\Qa\DepartmentImport;
use App\Model\Qa\OrgBindingUser;
use App\Model\Qa\Users;
use Carbon\Carbon;

class OrgBindingUserRepository
{
    public static function getUserBind(Users $user, Company $company): ?OrgBindingUser
    {
        return OrgBindingUser::query()
            ->where('company_id', $company->id)
            ->where('bind_user_id', $user->uid)
            ->first();
    }

    public static function getByDingUserId(int $company_id, string $user_id): ?OrgBindingUser
    {
        return OrgBindingUser::query()
            ->where('company_id', $company_id)
            ->where('ding_user_id', $user_id)
            ->first();
    }

    public static function addBinding(
        Users $users,
        Company $company,
        DepartmentImport $import,
        $dingUserId
    ): OrgBindingUser {
        $o =new OrgBindingUser();
        $o->company_id = $company->id;
        $o->ding_user_id = $dingUserId;
        $o->bind_mobile = $users->mobile_num;
        $o->bind_user_id = $users->uid;
        $o->import_id = $import->id;
        $o->created_at = $o->updated_at = Carbon::now();
        $o->save();
        $o->refresh();

        return $o;
    }

    public static function deleteByUser(Users $user)
    {
        $company = $user->company;
        return OrgBindingUser::where('bind_user_id', $user->uid)
            ->when(!is_null($company), function ($query) use ($company) {
                $query->where('company_id', $company->id);
            })->delete();
    }
}
