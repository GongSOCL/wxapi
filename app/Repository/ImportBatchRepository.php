<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Qa\ImportBatch;

class ImportBatchRepository
{

    public static function getNewest(): ImportBatch
    {
        return ImportBatch::query()
            ->orderByDesc('id')
            ->first();
    }
}
