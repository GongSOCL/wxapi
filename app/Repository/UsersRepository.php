<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Kernel\Uuid\Uuid;
use App\Model\Qa\Agent;
use App\Model\Qa\Company;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\UploadQiniu;
use App\Model\Qa\Users;
use App\Model\Qa\WechatUser;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\Database\Model\Model;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use Overtrue\Socialite\User;
use Youyao\Framework\ErrCode;

class UsersRepository
{
    /**
     * @Inject
     * @var Users
     */
    private $userModel;

    /**
     * @param $mobile
     * @return Users|null
     */
    public static function getUserByMobile($mobile): ?Users
    {
        return Users::where('mobile_num', $mobile)
            ->first();
    }

    /**
     * 根据手机号和微信添加用户
     * @param WechatUser $wechatUser
     * @param string $mobile
     * @return Users
     */
    public static function addUserAccountFromMobileAndWechat(WechatUser $wechatUser, string $mobile = ""): Users
    {
        $yyid = ApplicationContext::getContainer()
            ->get(Uuid::class)
            ->generate();
        $nowTime = time();
        $now = date('Y-m-d H:i:s');
        $o = new Users();
        $o->yyid = $yyid;
        $o->mobile_num = $mobile;
        $o->mobile_verified = $mobile ? DataStatus::REGULAR : DataStatus::DELETE;
        $o->name = $wechatUser->nickname;
        $o->pass = "";
        $o->mail = "";
        $o->mail_verified = DataStatus::DELETE;
        $o->company_yyid = "";
        $o->region_yyid = "";
        $o->wechat = "";
        $o->weibo = "";
        $o->tengqq = "";
        $o->tel_num = $mobile;
        $o->access = time();
        $o->login = time();
        $o->picture = "";
        $o->v_status = DataStatus::REGULAR;
        $o->event = "";
        $o->v_date = $nowTime;
        $o->user_type = DataStatus::REGULAR;
        $o->is_doctor = DataStatus::DELETE;
        $o->is_agent = DataStatus::DELETE;
        $o->client_ip = "";
        $o->last_client_ip = "";
        $o->created = $nowTime;
        $o->update_date = $now;

        if (!$o->save()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户添加失败");
        }

        return $o;
    }

    public static function getUserByYYIDs(array $yyids): Collection
    {
        return Users::whereIn('yyid', $yyids)
            ->get();
    }

    public static function getUserWithWechat(array $userYYIDS): Collection
    {
        return Users::whereIn('yyid', $userYYIDS)
            ->leftJoin("wechat_user as wu", 'wu.u_yyid', '=', 't_users.yyid')
            ->select([
                't_users.yyid',
                't_users.name',
                'wu.nickname',
                'wu.headimgurl'
            ])->get();
    }

    public static function updateMobile(Users $user, $mobile): bool
    {
        $user->mobile_num = $mobile;
        $user->mobile_verified = DataStatus::REGULAR;
        $user->update_date = date('Y-m-d H:i:s');
        return $user->save();
    }

    /**
     * 更新用户app头像
     * @param Users $user
     * @param string $avatar
     * @return bool
     */
    public static function updateAvatar(Users $user, UploadQiniu $upload): bool
    {
        $aTimes = $user->headimg_change_times;
        $user->avatar_id = $upload->id;
        $user->headimg_change_times = $aTimes + 1;
        $user->update_date = date('Y-m-d H:i:s');

        return $user->save();
    }

    public static function updateCompany(Users $user, Company $company): bool
    {
        $user->company_yyid = $company->yyid;
        $user->update_date = date('Y-m-d H:i:s');
        return $user->save();
    }

    /**
     * 更新用户代表状态
     * @param Users $user
     * @param $agentStatus
     * @return bool|int
     */
    public static function updateUserAgent(Users $user, $agentStatus, $trueName= '')
    {
        $data = [
            'is_agent' =>  $agentStatus,
            'update_date' => date('Y-m-d H:i:s')
        ];
        if ($trueName) {
            $data['true_name'] = $trueName;
        }

        return Users::where([
            'uid' => $user->uid,
            'v_status' => DataStatus::REGULAR
        ])->update($data);
    }

    public static function getUserBYMobiles(array $mobiles): Collection
    {
        return Users::whereIn('mobile', $mobiles)
            ->where('v_status', DataStatus::REGULAR)
            ->get();
    }

    public static function getUsersWithPage(
        string $keywords = "",
        Company $company = null,
        $agentStatus = -1,
        int $current = 1,
        int $limit = 10,
        $submittedType = 0,
        $realType = 0
    ): LengthAwarePaginatorInterface {
        $query = Users::query()->from('t_users as tu')
            ->leftJoin('t_agent as ta', 'tu.yyid', '=', 'ta.u_yyid')
            ->select(['tu.*', 'truename', 'submitted_role_type', 'real_role_type']);
        if ($company) {
            $query = $query->where('company_yyid', $company->yyid);
        }
        if ($submittedType) {
            $query = $query->where('submitted_role_type', $submittedType);
        }
        if ($realType) {
            $query = $query->where('real_role_type', $realType);
        }
        if ($keywords) {
            if (preg_match('/^\d+$/', $keywords) == 1) {
                $query = $query->where('mobile_num', 'like', "%{$keywords}%");
            } else {
                $query = $query->where('truename', 'like', "%{$keywords}%");
            }
        }

        if ($agentStatus != -1) {
            $query = $query->where('is_agent', $agentStatus);
        }

        return $query->orderByDesc('uid')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function verify(Users $user, $isOk = true)
    {
        return Users::where([
            'uid' => $user->uid,
        ])->update([
           'is_agent' => $isOk ? 2 : 4,
           'update_date' => date('Y-m-d H:i:s')
        ]);
    }

    public static function updateName(Users $user, string $name): bool
    {
        $nTimes = $user->name_change_times;
        $user->name = $name;
        $user->name_change_times = $nTimes + 1;
        $user->update_date = date('Y-m-d H:i:s');
        return $user->save();
    }

    public static function getUserByUids(array $userIds)
    {
        return Users::query()
            ->whereIn('uid', $userIds)
            ->get();
    }

    public static function searchUserWithLimit(string $keywords = "", $limit = 10): Collection
    {
        $query = Users::query()
            ->where('v_status', DataStatus::REGULAR);
        if ($keywords) {
            if (preg_match('/^\d+$/', $keywords)) {
                $query->where('mobile_num', 'like', "%{$keywords}%");
            } else {
                $query->where('name', 'like', "%{$keywords}%");
            }
        }

        return $query->limit($limit)
            ->get();
    }

    /**
     * 通过yyid获取uid
     * @param $yyid
     * @return object
     */
    public function getUid($yyid)
    {
        return $this->userModel::query()
            ->select('uid')
            ->where('yyid', $yyid)
            ->first();
    }

    /**
     * 通过yyid获取个人信息
     * @param $yyid
     * @return array|mixed
     */
    public function getPersonInfo($yyid)
    {
        return $this->userModel::query()
            ->from('t_users as u')
            ->leftJoin('t_company as c', 'u.company_yyid', '=', 'c.yyid')
            ->leftJoin('wechat_user as we', 'u.yyid', '=', 'we.u_yyid')
            ->leftJoin('t_user_assets as tua', 'u.uid', '=', 'tua.uid')
            ->select(
                'access',
                'c.name as company_name',
                'company_yyid',
                'is_agent',
                'is_doctor',
                'login',
                'mail',
                'mail_verified',
                'mobile_num',
                'mobile_verified',
                'u.name',
                'headimgurl',
                'pass',
                'picture',
                'region_yyid',
                'u.tel_num',
                'tengqq',
                'u.uid',
                'user_type',
                'v_date',
                'v_status',
                'wechat',
                'weibo',
                'u.yyid',
                Db::raw("coalesce(money_point, 0) as money_point"),
                Db::raw('coalesce(service_point, 0) as service_point'),
                'is_upload_idcard'
            )
            ->where(
                [
                    'u.yyid' => $yyid,
                    'wechat_type' => DataStatus::WECHAT_DOCTOR
                ]
            )
            ->first();
    }

    /**
     * 获取标志is_doctor
     * @param $yyid
     * @return object
     */
    public function getIsDoctor($yyid)
    {
        return $this->userModel::query()
            ->select('is_doctor')
            ->where('yyid', $yyid)
            ->first();
    }


    /**
     * 修改认证标志is_doctor = 1
     * @param $yyid
     */
    public function changeIsDoctor($yyid)
    {
        $isDoctor = $this->userModel::query()
            ->where('yyid', $yyid)
            ->first();
        $isDoctor->is_doctor = 1;
        $isDoctor->update_date = date('Y-m-d H:i:s');
        $isDoctor->save();
    }


    /**
     * 修改is_upload_idcard字段为1
     * @param $yyid
     */
    public function changeIsUploadIdcard($yyid)
    {
        $isUploadIdcard = $this->userModel::query()
            ->where('yyid', $yyid)
            ->first();
        $isUploadIdcard->is_upload_idcard = 1;
        $isUploadIdcard->update_date = date('Y-m-d H:i:s');
        $isUploadIdcard->save();
    }

    /**
     *根据用户yyid获取用户信息
     * @param $yyid
     * @return Users|null
     */
    public static function getUserByYYID($yyid)
    {
        return Users::where('yyid', $yyid)
            ->where('v_status', DataStatus::REGULAR)
            ->first();
    }

    /**
     * 根据uid获取用户信息
     *
     * @param $uid
     * @return Users|null
     */
    public static function getUserByUid($uid)
    {
        $query = Users::where([
            'uid' => $uid,
            'v_status' => DataStatus::REGULAR
        ]);
        return $query->first();
    }

    /**
     * @param $yyid
     * @param $email
     */
    public static function updateEmail($yyid, $email)
    {
        Users::where('yyid', $yyid)->update([
            'mail' => $email
        ]);
    }

    public static function cancelVerify(Users $user)
    {
        return Users::where([
            'uid' => $user->uid,
        ])->update([
            'is_agent' => 0,
            'update_date' => date('Y-m-d H:i:s')
        ]);
    }

    public static function updateRoleType(Users $user)
    {
        Agent::where('u_yyid', $user->yyid)->update([
            'supplier_id' => 0,
            'is_supplier' => 0,
            'is_supplier_leader' => 0,
            'real_role_type' => 0,
            'update_date' => date('Y-m-d H:i:s')
        ]);
    }

    public static function deleteAccount($id)
    {
        return Users::where('uid', $id)->update([
            'v_status' => DataStatus::DELETE,
            'update_date' => date('Y-m-d H:i:s')
        ]);
    }

    public static function updateHeadimg($id, $avatarId)
    {
        Users::where('uid', $id)->update([
            'avatar_id' => $avatarId,
            'update_date' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getUserMapByUids(array $uids): array
    {
        $user = self::getUserByUids($uids);
        return $user->keyBy('uid')->all();
    }

    public static function alertChange($user)
    {
        $user->is_alert = 1;
        return $user->save();
    }

    /**
     * @param $yyid
     * @return Collection
     */
    public static function getServiceProduct($yyid)
    {
        return RepsServeScopeS::from('t_reps_serve_scope_s as ts')
            ->join('t_drug_series as td', 'ts.series_yyid', '=', 'td.yyid')
            ->where([
                'ts.user_yyid' => $yyid,
                'ts.status' => DataStatus::REGULAR
            ])->select(['td.id', 'series_name'])->groupBy(['ts.series_yyid'])->get();
    }

    /**
     * @param $userId
     * @return Model|\Hyperf\Database\Query\Builder|object|null
     */
    public static function getUserAgent($userId)
    {
        return Users::from('t_users as tu')
            ->join('t_agent as ta', 'tu.yyid', '=', 'ta.u_yyid')
            ->where('uid', $userId)
            ->first();
    }

    /**
     * @param $userIds
     * @return Collection
     */
    public static function getUserAgents($userIds)
    {
        return Users::from('t_users as tu')
            ->join('t_agent as ta', 'tu.yyid', '=', 'ta.u_yyid')
            ->whereIn('uid', $userIds)
            ->select('uid', 'truename')
            ->get();
    }
}
