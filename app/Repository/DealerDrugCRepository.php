<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Qa\DealerDrugC;

class DealerDrugCRepository
{
    /**
     * @param $companyYyid
     * @param $productId
     * @param $skuId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getSkuInfo($companyYyid, $productId, $skuId)
    {
        return DealerDrugC::where([
            'company_yyid' => $companyYyid,
            'product_id' => $productId,
            'sku_id' => $skuId
            ])
            ->orderBy('id', 'desc')
            ->first();
    }

    /**
     * @param $companyYyid
     * @param $productId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getProInfo($companyYyid, $productId)
    {
        return DealerDrugC::where([
            'company_yyid' => $companyYyid,
            'product_id' => $productId
            ])
            ->orderBy('id', 'desc')
            ->first();
    }
}
