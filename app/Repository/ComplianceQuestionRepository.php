<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ComplianceQuestion;
use Elasticsearch\Endpoints\Indices\DataStreamsStats;

class ComplianceQuestionRepository
{
    /**
     * 根据yyid获取所有的题库
     * @param array $YYIDS
     */
    public static function getQuestionsByYYIDS(array $YYIDS)
    {
        return ComplianceQuestion::whereIn('yyid', $YYIDS)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function getByYYID($YYID)
    {
        return  ComplianceQuestion::where('yyid', $YYID)
            ->whereValid()
            ->first();
    }
}
