<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\Msg;
use App\Model\Qa\RepsMsg;
use App\Model\Qa\Users;
use Hyperf\DbConnection\Db;

class RepsMsgRepository
{
    /**
     * 添加消息接收人关联关系
     *
     * @param Msg $msg
     * @param Users $user
     * @param int $memberId
     * @return RepsMsg
     */
    public static function addMsgReps(Msg $msg, Users $user, $memberId = 0): RepsMsg
    {
        $o = new RepsMsg();
        $o->yyid = Db::raw("replace(upper(uuid()),'-','')");
        $o->user_yyid = $user->yyid;
        $o->msg_yyid = $msg->yyid;
        $o->member_id = $memberId;
        $o->is_read = 0;
        $o->read_time = $o->deleted_time = $o->created_time = $o->modify_time = date('Y-m-d H:i:s');
        $o->save();

        return $o;
    }


    /**
     * 条件参数
     * @param $userType
     * @return array
     */
    public static function getUserTypeCondition($userType)
    {
        if ($userType==1) {
            $whereIn =[
                Msg::MSG_FLOW_DATE_UPDATED,
                Msg::MSG_VOTE_NOTICE ,
                Msg::MSG_REWARD_POINT,
                Msg::MSG_CONSUMPUTION_POINT ,
                Msg::MSG_INVITE_TO_GROUP
            ];
        } elseif ($userType==2) {
            $whereIn = [
                Msg::MSG_REWARD_POINT,
                Msg::MSG_CONSUMPUTION_POINT ,
                Msg::MSG_QUESTIONNAIRE_INVITE
            ];
        }

        return $whereIn;
    }

    /**
     * 获取提醒消息详情同时is_read+1
     * @param $yyid
     * @param $userType
     * @return \Hyperf\Database\Model\Model|\Hyperf\Database\Query\Builder|object|null
     */
    public static function getRemindMsg($yyid, $userType)
    {

        //获取消息提醒 最近的一条消息
        $whereIn = self::getUserTypeCondition($userType);
        return RepsMsg::from('t_reps_msg as rm')
            ->leftJoin('t_msg as m', 'rm.msg_yyid', '=', 'm.yyid')
            ->select(
                'rm.id',
                'msg_yyid',
                'is_read',
                'content',
                'title',
                'sub_title',
                'msg_type',
                'link',
                'm.created_time'
            )
            ->where('rm.user_yyid', $yyid)
            ->where('m.status', Msg::STATUS_AUDITED)
            ->where('rm.is_deleted', RepsMsg::NOT_DELETED)
            ->whereIn('m.msg_type', $whereIn)
            ->orderBy('m.created_time', 'desc')
            ->first();
    }


    /**
     * 根据msgyyid获取消息详情同时is_read+1
     * @param $yyid
     * @param $msgYyid
     * @return \Hyperf\Database\Model\Model|\Hyperf\Database\Query\Builder|object|null
     */
    public static function getOneMsgDetail($yyid, $msgYyid)
    {
        //根据msgyyid获取消息详情
        $msg =  RepsMsg::from('t_reps_msg as rm')
            ->leftJoin('t_msg as m', 'rm.msg_yyid', '=', 'm.yyid')
            ->select(
                'rm.id',
                'msg_yyid',
                'is_read',
                'content',
                'title',
                'sub_title',
                'msg_type',
                'link',
                'm.created_time'
            )
            ->where('rm.user_yyid', $yyid)
            ->where('rm.msg_yyid', $msgYyid)
            ->where('m.status', Msg::STATUS_AUDITED)
            ->where('rm.is_deleted', RepsMsg::NOT_DELETED)
            ->first();

        if ($msg) {
            $msg->is_read = $msg->is_read + 1;
            $msg->modify_time = date('Y-m-d H:i:s');
            $msg->save();
            return $msg;
        }
        return null;
    }

    /**
     * 未读消息的数量
     * @param $yyid
     * @param $userType
     * @return int
     */
    public static function getUnReadCount($yyid, $userType)
    {
        $whereIn = self::getUserTypeCondition($userType);
        return RepsMsg::query()
            ->from('t_reps_msg as rm')
            ->leftJoin('t_msg as m', 'm.yyid', '=', 'rm.msg_yyid')
            ->where('rm.user_yyid', $yyid)
            ->where('is_read', 0)
            ->whereIn('m.msg_type', $whereIn)
            ->where('m.status', Msg::STATUS_AUDITED)
            ->where('rm.is_deleted', RepsMsg::NOT_DELETED)
            ->count();
    }
}
