<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Qa\HospitalStarP;
use App\Model\Qa\YouyaoHospital;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class HospitalStarPRepository
{
    public static function getStarByHospitalAndProducts(YouyaoHospital $hospital, array $productYYIDS): Collection
    {
        $subQuery = HospitalStarP::where('hospital_yyid', $hospital->yyid)
            ->whereIn('product_yyid', $productYYIDS)
            ->select(['product_yyid', Db::raw("max(id) as mid")])
            ->groupBy('product_yyid');


        return HospitalStarP::query()
            ->joinSub($subQuery, 'b', function ($join) {
                $join->on("t_hospital_star_p.id", "=", 'b.mid')
                    ->on("t_hospital_star_p.product_yyid", "=", 'b.product_yyid');
            })->get();
    }
}
