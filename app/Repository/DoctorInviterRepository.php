<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\RepsDoctorInviter;
use Hyperf\Database\Query\Expression;
use Hyperf\Di\Annotation\Inject;

class DoctorInviterRepository
{
    /**
     * @Inject
     * @var RepsDoctorInviter
     */
    private $repsDoctorInviterModel;


    /**
     * 获取邀请列表
     * @param $yyid
     * @param $type
     * @param $isAuth
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getInvitationList($yyid, $type, $isAuth, $page, $pageSize)
    {
        $where = $this->getParams($yyid, $type, $isAuth);

        return $this->repsDoctorInviterModel::query()
            ->from('t_reps_doctor_inviter as g')
            ->leftJoin('wechat_user as we', 'g.invitee_openid', '=', 'we.openid')
            ->leftJoin('t_users as u', 'g.invitee_yyid', '=', 'u.yyid')
            ->select(
                'g.yyid as member_yyid',
                'u.yyid as user_yyid',
                'we.openid as user_openid',
                'headimgurl',
                'nickname',
                'name',
                'g.user_type as user_type',
                'u.user_type as is_auth',
                'g.status',
                'g.created_at'
            )
            ->where($where)
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->orderBy('g.created_at', 'desc')
            ->get();
    }


    /**
     * 获取总条数
     * @param $yyid
     * @param $type
     * @param $isAuth
     * @return int
     */
    public function getInvitationCount($yyid, $type, $isAuth)
    {
        $where = $this->getParams($yyid, $type, $isAuth);

        return $this->repsDoctorInviterModel::query()
            ->from('t_reps_doctor_inviter as g')
            ->leftJoin('wechat_user as we', 'g.invitee_openid', '=', 'we.openid')
            ->leftJoin('t_users as u', 'g.invitee_yyid', '=', 'u.yyid')
            ->where($where)
            ->count();
    }

    /**
     * 构筑参数
     * @param $yyId
     * @param $type
     * @param $isAuth
     * @return array
     */
    public function getParams($yyid, $type, $isAuth)
    {
        $where = [];
        $where['inviter_yyid'] = $yyid;
        $where['status'] = $this->repsDoctorInviterModel::STATUS_COMMON;
        if ($type!==0&&$isAuth!==0) {
            $where['g.user_type'] = $type;
            $where['u.user_type'] = $isAuth;
        } elseif ($type==0&&$isAuth!==0) {
            $where['u.user_type'] = $isAuth;
        } elseif ($type!==0&&$isAuth==0) {
            $where['g.user_type'] = $type;
        }
        return $where;
    }

    /**
     * 邀请
     * @param $inviteeId
     * @param $inviterId
     * @param $type
     * @return int
     */
    public function inviteDoctor($inviteeId, $inviterId, $inviteeYyid, $type)
    {
        $data = [
            'yyid' => new Expression("replace(upper(uuid()),'-','')"),
            'inviter_yyid' => $inviterId,
            'invitee_yyid' => $inviteeYyid,
            'invitee_openid' => $inviteeId,
            'is_subscribe' => $this->repsDoctorInviterModel::ALREADY_SUBSCRIBE,
            'user_type' => $type,
            'status' => $this->repsDoctorInviterModel::STATUS_JOINING,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ];
        return $this->repsDoctorInviterModel::query()->insertGetId($data);
    }


    /**
     * 查找好友邀请关系
     * @param $inviterYyid
     * @param $inviteeOpenid
     * @param $wechatType
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getExistsRelation($inviterYyid, $inviteeOpenid, $wechatType)
    {
        return RepsDoctorInviter::where(
            [
                    'inviter_yyid' => $inviterYyid,
                    'invitee_openid' => $inviteeOpenid,
                    'wechat_type' => $wechatType,
                    'group_yyid' => ''
                ]
        )->orderBy('created_at', 'desc')->first();
    }

    /**
     * 更新好友关系
     * @param $inviterYyid
     * @param $inviteeOpenid
     * @param $userType
     * @param $wechatType
     */
    public static function updateRelation($inviterYyid, $inviteeOpenid, $userType, $wechatType, $status)
    {
        RepsDoctorInviter::where(
            [
                'inviter_yyid' => $inviterYyid,
                'invitee_openid' => $inviteeOpenid,
                'user_type' => $userType,
                'wechat_type' => $wechatType,
                'group_yyid' => ''
            ]
        )->update(
            [
                'is_subscribe' => 1,
                'status' => $status,
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );
    }

    /**
     * 添加好友关系
     * @param $inviterYyid
     * @param $inviteeYyid
     * @param $inviteeOpenid
     * @param $userType
     * @param $wechatType
     * @param $status
     */
    public static function addRelation($inviterYyid, $inviteeYyid, $inviteeOpenid, $userType, $wechatType, $status)
    {
        $data = [
            'yyid' =>  new Expression("replace(upper(uuid()),'-','')"),
            'inviter_yyid' => $inviterYyid,
            'invitee_yyid' => $inviteeYyid,
            'invitee_openid' => $inviteeOpenid,
            'group_yyid' => '',
            'is_subscribe' => 1,
            'user_type' => $userType,
            'status' => $status,
            'wechat_type' => $wechatType,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        RepsDoctorInviter::insert($data);
    }
}
