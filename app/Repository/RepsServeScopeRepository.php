<?php
declare(strict_types=1);


namespace App\Repository;

use App\Model\Qa\RepsServeScope;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class RepsServeScopeRepository
{
    /**
     * 获取用户服务医院列表.
     *
     * @param $provinceYYID
     * @param $cityYYID
     * @param $level
     * @param Users $user
     * @param $page
     * @param $pageSize
     * @param $keywords
     * @return Collection
     */
    public static function getUserServeHospital(
        $provinceYYID,
        $cityYYID,
        $level,
        Users $user,
        $page,
        $pageSize,
        $keywords = ""
    ): Collection {
        $query = Db::table('t_reps_serve_scope_s')
            ->join('t_youyao_hospital', 't_youyao_hospital.yyid', '=', 't_reps_serve_scope_s.hospital_yyid')
            ->where([
                't_reps_serve_scope_s.user_yyid' => $user->yyid,
                't_reps_serve_scope_s.status' => RepsServeScope::STATUS_ACCESS
            ]);
        if ($keywords) {
            $query = $query->where('t_youyao_hospital.hospital_name', 'like', "%{$keywords}%");
        }
        if ($provinceYYID) {
            $query = $query->where('t_youyao_hospital.pro_yyid', $provinceYYID);
        }
        if ($cityYYID) {
            $query = $query->where('t_youyao_hospital.city_yyid', $cityYYID);
        }

        if (!empty($level)) {
            if (in_array(YouyaoHospital::HOSPITAL_LEVEL_THREE, $level)) {
                $query->where('level', 'like', "%三%", 'or');
            }

            if (in_array(YouyaoHospital::HOSPITAL_LEVEL_TWO, $level)) {
                $query->where('level', 'like', "%二%");
            }

            if (in_array(YouyaoHospital::HOSPITAL_LEVEL_ONE, $level)) {
                $query->where('level', 'like', "%一%");
            }

            if (in_array(YouyaoHospital::HOSPITAL_LEVEL_UNKNOWN, $level)) {
                $query->whereRaw("(level = '' or level is null)");
            }
        }

        if (!empty($level)) {
            $query = $query->whereIn('t_youyao_hospital.level', $level);
        }
        return $query->select('t_youyao_hospital.*')
            ->groupBy('t_youyao_hospital.id')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function getUserServeHospitalCount(
        $provinceYYID,
        $cityYYID,
        $level,
        Users $user,
        $keywords = ""
    ) {
        $query = Db::table('t_reps_serve_scope_s')
            ->join(
                't_youyao_hospital',
                't_youyao_hospital.yyid',
                '=',
                't_reps_serve_scope_s.hospital_yyid'
            )
            ->where([
                't_reps_serve_scope_s.user_yyid' => $user->yyid,
                't_reps_serve_scope_s.status' => RepsServeScope::STATUS_ACCESS
            ]);
        if ($keywords) {
            $query = $query->where('t_youyao_hospital.hospital_name', 'like', "%{$keywords}%");
        }
        if ($provinceYYID) {
            $query = $query->where('t_youyao_hospital.pro_yyid', $provinceYYID);
        }
        if ($cityYYID) {
            $query = $query->where('t_youyao_hospital.city_yyid', $cityYYID);
        }

        if (!empty($level)) {
            if (in_array(YouyaoHospital::HOSPITAL_LEVEL_THREE, $level)) {
                $query->where('level', 'like', "%三%", 'or');
            }

            if (in_array(YouyaoHospital::HOSPITAL_LEVEL_TWO, $level)) {
                $query->where('level', 'like', "%二%");
            }

            if (in_array(YouyaoHospital::HOSPITAL_LEVEL_ONE, $level)) {
                $query->where('level', 'like', "%一%");
            }

            if (in_array(YouyaoHospital::HOSPITAL_LEVEL_UNKNOWN, $level)) {
                $query->whereRaw("(level = '' or level is null)");
            }
        }

        if (!empty($level)) {
            $query = $query->whereIn('t_youyao_hospital.level', $level);
        }
        return $query->select('t_youyao_hospital.*')
            ->groupBy('t_youyao_hospital.id')
            ->get();
    }

    /**
     * 获取代表服务药品系列.
     *
     * @param Users $user
     * @return Collection
     */
    public static function getUserServeDrugSeries(Users $user, YouyaoHospital $hospital = null): Collection
    {
        return Db::table('t_reps_serve_scope_s')
            ->join('t_drug_series', 't_drug_series.yyid', '=', 't_reps_serve_scope_s.series_yyid')
            ->where([
                't_reps_serve_scope_s.user_yyid' => $user->yyid,
                't_reps_serve_scope_s.status' => RepsServeScope::STATUS_ACCESS
            ])->when($hospital, function($q) use($hospital) {
                $q->where('t_reps_serve_scope_s.hospital_yyid', $hospital->yyid);
            })->select('t_drug_series.*')
            ->get();
    }
}
