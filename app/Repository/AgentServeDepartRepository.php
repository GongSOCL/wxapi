<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\AgentServeDepart;
use App\Model\Qa\Users;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class AgentServeDepartRepository
{
    /**
     * @param Users $users
     * @param int $hospitalId
     * @param string $keywords
     * @return Collection
     */
    public static function getUserServeDeparts(Users $users, $hospitalId = 0, $keywords = ""): Collection
    {
        $query = Db::table('t_agent_serve_depart')
           ->join('t_system_depart', 't_system_depart.id', '=', 't_agent_serve_depart.depart_id')
           ->where([
               't_agent_serve_depart.user_id' => $users->uid,
               't_system_depart.status' => DataStatus::REGULAR,
               't_agent_serve_depart.status' => DataStatus::REGULAR
           ]);
        if ($hospitalId) {
            $query = $query->where('t_agent_serve_depart.hospital_id', $hospitalId);
        }
        if ($keywords) {
            $query = $query->where('t_system_depart.name', 'like', "%{$keywords}%");
        }

        return $query->select(["t_system_depart.*"])
           ->orderBy('t_system_depart.id')
           ->groupBy(['t_system_depart.id'])
           ->get();
    }

    public static function filterServeDeparts(Users $users, $hospitalId, array $departIds): Collection
    {
        return AgentServeDepart::where([
            'user_id' => $users->uid,
            'hospital_id' => $hospitalId,
            'status' => DataStatus::REGULAR
        ])->whereIn('depart_id', $departIds)
            ->get();
    }

    public static function batchAddUserHospitalDeparts(Users $user, $hospitalId, $departIds): bool
    {
        $data = [];
        $now = date('Y-m-d H:i:s');
        foreach ($departIds as $departId) {
            $data[] = [
                'user_id' => $user->uid,
                'hospital_id' => $hospitalId,
                'depart_id' => $departId,
                'status' => DataStatus::REGULAR,
                'created_time' => $now,
                'modify_time' => $now
            ];
        }

        return AgentServeDepart::insert($data);
    }
}
