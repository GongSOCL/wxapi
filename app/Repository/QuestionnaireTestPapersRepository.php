<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\QuestionnairePaper;
use App\Model\Qa\QuestionnaireTestPaper;
use Hyperf\DbConnection\Db;

class QuestionnaireTestPapersRepository
{
    /**
     * 获取问卷关联的所有试卷id
     * @param QuestionnairePaper $paper
     * @return array
     */
    public static function getAllTestPaperIds(QuestionnairePaper $paper)
    {
        return $paper->testPapers()
            ->where(['status' => DataStatus::REGULAR])
            ->pluck('test_paper_id')
            ->all();
    }

    /**
     * 批量关联问卷-试卷关系
     * @param QuestionnairePaper $paper
     * @param array $testPaperIds
     */
    public static function batchAddTestPaper(QuestionnairePaper $paper, array $testPaperIds)
    {
        $insertData = [];
        $now = date('Y-m-d H:i:s');
        foreach ($testPaperIds as $testPaperId) {
            $insertData[] = [
                'questionnaire_paper_id' => $paper->id,
                'test_paper_id' => $testPaperId,
                'status' => DataStatus::REGULAR,
                'created_time' => $now,
                'modify_time' => $now
            ];
        }

        QuestionnaireTestPaper::getQuery()->insert($insertData);
    }
}
