<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DealerDrugC;
use App\Model\Qa\DealerVisit;

class DealerVisitRepository
{
    /**
     * @param $companyYyid
     * @param $staffCode
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getLastMonth($companyYyid, $staffCode)
    {
        return DealerVisit::query()
            ->select('month')
            ->distinct()
            ->where([
                'company_yyid' => $companyYyid,
                'staff_code' => $staffCode,
                'status' => DataStatus::REGULAR
            ])
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @param $companyYyid
     * @param $month
     * @param $staffCode
     * @return \Hyperf\Utils\Collection
     */
    public static function getReportInfo($companyYyid, $month, $staffCode)
    {
        return DealerVisit::select([
                'id','product_id','ins_class','of_tgt_ins',
                'of_ins_visited','coverage','of_calls','frequency',
                'base_num','base_standard_num','base_rage','vacant_days',
                'dif','dif_percent','working_hours'])
            ->where([
                'month' => $month,
                'company_yyid' => $companyYyid,
                'staff_code' => $staffCode,
                'status' => DataStatus::REGULAR
            ])
            ->get();
    }
}
