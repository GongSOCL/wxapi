<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\AgentVisit;
use App\Model\Qa\AgentVisitDoctor;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class AgentVisitDoctorRepository
{

    /**
     * 批量添加代表拜访医生关联关系.
     * @param AgentVisit $visit
     * @param array $doctorOpenidUserInfoIds
     * @return bool
     */
    public static function batchAddVisitDoctors(AgentVisit $visit, array $doctorOpenidUserInfoIds): bool
    {
        $data = [];
        $now = date('Y-m-d H:i:s');
        foreach ($doctorOpenidUserInfoIds as $id) {
            $data[] = [
                'visit_id' => $visit->id,
                'doctor_id' => $id,
                'status' => DataStatus::REGULAR,
                'created_time' => $now,
                'modify_time' => $now
            ];
        }

        return AgentVisitDoctor::insert($data);
    }

    /**
     * 批量删除代表医生关联关系.
     * @param AgentVisit $visit
     * @param array $doctorOpenidUserInfoIds
     * @return bool
     */
    public static function batchRemoteVisitDoctors(AgentVisit $visit, array $doctorOpenidUserInfoIds)
    {
        return AgentVisitDoctor::where([
            'visit_id' => $visit->id,
            'status' => DataStatus::REGULAR
        ])->whereIn('doctor_id', $doctorOpenidUserInfoIds)
            ->update([
               'status' => DataStatus::DELETE,
               'modify_time' => date('Y-m-d H:i:s')
            ]);
    }

    public static function getVisitDoctors(AgentVisit $visit): Collection
    {
        return AgentVisitDoctor::where([
            'visit_id' => $visit->id,
            'status' => DataStatus::REGULAR
        ])->get();
    }

    public static function getDoctors(AgentVisit $visit): Collection
    {
        return Db::table("t_agent_visit_doctor as tavd")
            ->join("new_doctor_openid_userinfo as tdou", "tdou.id", "=", "tavd.doctor_id")
            ->join('new_doctor_userinfo as ndu', 'tdou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                "tavd.visit_id" => $visit->id,
                "tavd.status" => DataStatus::REGULAR
            ])->select([
                'tdou.id',
                'ndu.nickname',
                'ndu.true_name'
            ])
            ->get();
    }

    public static function getVisitsAllDoctors(array $visitIds): Collection
    {
        return Db::table("t_agent_visit_doctor as tavd")
            ->join("new_doctor_openid_userinfo as tdou", "tdou.id", "=", "tavd.doctor_id")
            ->join('new_doctor_userinfo as ndu', 'tdou.new_doctor_id', '=', 'ndu.nid')
            ->where("tavd.status", DataStatus::REGULAR)
            ->whereIn('tavd.visit_id', $visitIds)
            ->select([
                "tavd.visit_id",
                "tdou.id",
                'ndu.nickname',
                'ndu.true_name'
            ])
            ->get();
    }

    public static function changeDoctorToNew(array $oldDoctorIds, $newiestDoctorId): int
    {
        return AgentVisitDoctor::query()
            ->whereIn('doctor_id', $oldDoctorIds)
            ->where('status', DataStatus::REGULAR)
            ->update([
                'doctor_id' => $newiestDoctorId,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }
}
