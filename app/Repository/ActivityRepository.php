<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\Point;
use App\Model\Qa\Activity;

class ActivityRepository
{
    /**
     * 通过id获取单条信息
     * @param $id
     * @return object
     */
    public static function getActivity($id)
    {
        return Activity::query()
            ->where('id', $id)
            ->first();
    }

    /**
     * 获取问卷活动
     * @return Activity|null
     */
    public static function getQuestionnaireActivity()
    {
        return Activity::where([
            'event_key' => Point::QUESTIONNAIRE_EVENT_KEY,
            'exist_special' => DataStatus::REGULAR,
            'status' => DataStatus::REGULAR
        ])->first();
    }
}
