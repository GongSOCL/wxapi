<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\Users;
use App\Model\Qa\WechatUser;
use Hyperf\Di\Annotation\Inject;
use App\Constants\DataStatus;
use Hyperf\Utils\Collection;

class WechatUserRepository
{
    /**
     * @Inject
     * @var WechatUser
     */
    private $wechatUserModel;

    /**
     * 根据openid获取微信列表
     *
     * @param $openIds
     * @param int $wechatType
     * @param string $indexBy
     * @return \Hyperf\Utils\Collection
     */
    public static function getListByOpenids($openIds, $wechatType = DataStatus::WECHAT_DOCTOR, $indexBy = '')
    {
        $subQuery = WechatUser::whereIn('openid', $openIds)
                ->where('wechat_type', $wechatType)
                ->orderBy('id', 'desc');


        $result = WechatUser::fromSub($subQuery, 'temp')
            ->groupBy('openid')
            ->orderBy('id', 'desc')
            ->get();

        //
        if ($result && $result->isNotEmpty() && $indexBy != '') {
            return $result->keyBy($indexBy);
        } else {
            return $result;
        }
    }

    /**
     * @param $openIds
     * @param string $keywords
     * @param int $wechatType
     * @param string $indexBy
     * @return \Hyperf\Utils\Collection
     */
    public static function getListWithFuzzyName(
        $openIds,
        $keywords = '',
        $wechatType = DataStatus::WECHAT_DOCTOR,
        $indexBy = ''
    ) {
        $subQuery = WechatUser::whereIn('openid', $openIds)
            ->where('wechat_type', $wechatType)
            ->orderBy('id', 'desc');
        if ($keywords) {
            $subQuery = $subQuery->where('nickname', 'like', "%{$keywords}%");
        }

        $result = WechatUser::fromSub($subQuery, 'temp')
            ->groupBy('openid')
            ->orderBy('id', 'desc')
            ->get();

        //
        if ($result && $result->isNotEmpty() && $indexBy != '') {
            return $result->keyBy($indexBy);
        } else {
            return $result;
        }
    }

    /**
     * @param string $openid
     * @param int $wechatType
     * @return WechatUser|null
     */
    public static function getOneWechatInfo($openid, $wechatType = DataStatus::WECHAT_DOCTOR): ?WechatUser
    {
        return WechatUser::where('openid', $openid)
            ->where('wechat_type', $wechatType)
            ->orderBy('id', 'desc')
            ->limit(1)
            ->first();
    }

    /**
     * @param string $openid
     * @param int $wechatType
     * @return WechatUser|null
     */
    public static function getOneWechatInfoByUnionId(
        $unionId,
        $wechatType = DataStatus::WECHAT_AGENT,
        $forceBind = false
    ): ?WechatUser {
        $query = WechatUser::where('unionid', $unionId)
            ->where('wechat_type', $wechatType);

        if ($forceBind) {
            $query = $query->whereRaw("(u_yyid != '' or u_yyid is not null) ");
        }

        return $query->orderBy('id', 'desc')
            ->limit(1)
            ->first();
    }

    /**
     * @param string $openid
     * @param int $wechatType
     * @return WechatUser|null
     */
    public static function getByUnionIdExcludeType(
        $unionId,
        $wechatType = DataStatus::WECHAT_AGENT,
        $onlyBind = false
    ): ?WechatUser {
        $query = WechatUser::where('unionid', $unionId)
            ->where('wechat_type', '=', $wechatType);
        if ($onlyBind) {
            $query = $query->whereRaw("(u_yyid != '' and u_yyid is not null)");
        }

        return $query->orderBy('id', 'desc')
            ->limit(1)
            ->first();
    }

    /**
     * @param string $userYYID
     * @param int $wechatType
     * @return WechatUser|null
     */
    public static function getOneWechatInfoByUserYYID($userYYID, $wechatType = DataStatus::WECHAT_DOCTOR): ?WechatUser
    {
        return WechatUser::where('u_yyid', $userYYID)
            ->where('wechat_type', $wechatType)
            ->orderBy('id', 'desc')
            ->limit(1)
            ->first();
    }

    public static function getMultiUsersWechat(array $userYYIDS, $wechatType = DataStatus::WECHAT_DOCTOR): Collection
    {
        return WechatUser::whereIn('u_yyid', $userYYIDS)
            ->where('wechat_type', $wechatType)
            ->groupBy('u_yyid')
            ->orderBy('id', 'desc')
            ->get();
    }

    public static function unsubscribe($unionId, $wechatType)
    {
        WechatUser::where('unionid', $unionId)
            ->where('wechat_type', $wechatType)
            ->update([
                'subscribe' => DataStatus::DELETE,
            ]);
    }

    /**
     * 查找更新或添加微信信息
     * @param array $wxInfo
     * @param $wechatType
     * @return WechatUser
     */
    public static function findUpdateOrAddSubscribe(array $wxInfo, $wechatType): WechatUser
    {
        $wechat = self::getOneWechatInfo($wxInfo['openid'], $wechatType);
        if ($wechat) {
            $wechat = self::updateWechatInfoFromWx($wechat, $wxInfo);
        } else {
            $wechat = self::addWechatInfoFromWx($wxInfo, $wechatType);
        }

        return $wechat;
    }

    /**
     * 从公众号获取的微信信息更新用户记录
     *
     * @param WechatUser $wechatUser
     * @param array $wxInfo
     * @return WechatUser
     */
    public static function updateWechatInfoFromWx(WechatUser $wechatUser, array $wxInfo): WechatUser
    {
        //微信接口调整了
        //https://developers.weixin.qq.com/community/develop/doc/00028edbe3c58081e7cc834705b801?blockType=1&page=2#comment-list
        //!!Funck you wechat
        if (isset($wxInfo['nickname']) && $wxInfo['nickname']) {
            $wechatUser->nickname = $wxInfo['nickname'];
        }
        if (isset($wxInfoaw['headimgurl']) && $wxInfo['headimgurl']) {
            $wechatUser->headimgurl = $wxInfo['headimgurl'];
        }
        $wechatUser->sex = $wxInfo['sex'] ?: 0;
        $wechatUser->country = $wxInfo['country'];
        $wechatUser->province = $wxInfo['province'];
        $wechatUser->city = $wxInfo['city'];
        $wechatUser->subscribe = $wxInfo['subscribe'] ? $wxInfo['subscribe'] : 0;
        $wechatUser->subscribe_scene = $wxInfo['subscribe_scene'];
        $wechatUser->subscribe_time = $wxInfo['subscribe_time'];
        $wechatUser->unionid = $wxInfo['unionid'] ?? '';
        $wechatUser->save();

        return $wechatUser;
    }

    public static function updateWechatInfoFromOauthRawInfo(WechatUser $wechatUser, array $raw): WechatUser
    {
        if (isset($raw['nickname']) && $raw['nickname']) {
            $wechatUser->nickname = $raw['nickname'];
        }
        if (isset($raw['sex']) && $raw['sex']) {
            $wechatUser->sex = $raw['sex'];
        }
        if (isset($raw['city']) && $raw['city']) {
            $wechatUser->city = $raw['city'];
        }
        if (isset($raw['province']) && $raw['province']) {
            $wechatUser->province = $raw['province'];
        }
        if (isset($raw['country']) && $raw['country']) {
            $wechatUser->country = $raw['country'];
        }
        if (isset($raw['headimgurl']) && $raw['headimgurl']) {
            $wechatUser->headimgurl = $raw['headimgurl'];
        }
        $wechatUser->save();

        return $wechatUser;
    }

    public static function addWechatInfoFromWx(array $wxInfo, $wechatType, $userYYID = ''): WechatUser
    {
        $o = new WechatUser();
        $o->u_yyid = $userYYID;
        $o->openid = $wxInfo['openid'];
        $o->unionid = $wxInfo['unionid'] ?? '';
        $o->nickname = $wxInfo['nickname'];
        $o->headimgurl = $wxInfo['headimgurl'];
        $o->subscribe = DataStatus::REGULAR;
        $o->subscribe_time = $wxInfo['subscribe_time'];
        $o->remark = $wxInfo['remark'];
        $o->subscribe_scene = $wxInfo['subscribe_scene'];
        $o->qr_scene = $wxInfo['qr_scene'];
        $o->qr_scene_str = $wxInfo['qr_scene_str'];
        $o->created_at = date('Y-m-d H:i:s');
        $o->sex = $wxInfo['sex'] ?: 0;
        $o->wechat_type = $wechatType;
        $o->save();

        return $o;
    }

    public static function addWechatInfoFromOauthRawInfo(
        array $raw,
        $wechatType,
        $userYYID = '',
        $subScribe = false
    ): WechatUser {
        $o = new WechatUser();
        $o->u_yyid = $userYYID;
        $o->openid = $raw['openid'];
        $o->unionid = isset($raw['unionid']) && $raw['unionid'] ? $raw['unionid'] : '';
        $o->nickname = $raw['nickname'] ?? '';
        $o->headimgurl = $raw['headimgurl'] ?? '';
        $o->subscribe = $subScribe ? DataStatus::REGULAR : DataStatus::DELETE;
        $o->subscribe_time = '';
        $o->remark = '';
        $o->subscribe_scene = '';
        $o->qr_scene = '';
        $o->qr_scene_str = '';
        $o->created_at = date('Y-m-d H:i:s');
        $o->wechat_type = $wechatType;
        $o->province = $raw['province'] ?? '';
        $o->city = $raw['city'] ?? '';
        $o->country = $raw['country'] ?? '';
        //微信接口调整了
        //https://developers.weixin.qq.com/community/develop/doc/00028edbe3c58081e7cc834705b801?blockType=1&page=2#comment-list
        //!!Funck you wechat
        $o->sex = $raw['sex'] ?? DataStatus::DELETE;
        $o->save();

        return $o;
    }

    public static function getById(int $id)
    {
        return WechatUser::where('id', $id)
            ->first();
    }

    public static function bindUserByOpenId(string $openid, Users $user)
    {
        WechatUser::where('openid', $openid)
            ->whereRaw("(u_yyid = '' or u_yyid is null)")
            ->update([
                'u_yyid' => $user->yyid
            ]);
    }

    public static function bindUserByUnionId(string $unionId, Users $user)
    {
        WechatUser::where('unionid', $unionId)
            ->whereRaw("(u_yyid = '' or u_yyid is null)")
            ->update([
                'u_yyid' => $user->yyid
            ]);
    }

    /**
     * 获取用户所有微信记录
     * @param array $userYYIDS
     * @return Collection
     */
    public static function getUserAllWechat(array $userYYIDS): Collection
    {
        return WechatUser::whereIn('u_yyid', $userYYIDS)
            ->get();
    }

    public static function getByYyid($yyid)
    {
        return WechatUser::where('u_yyid', $yyid)
            ->first();
    }

    public static function getYouyaoByYyid($yyid)
    {
        return WechatUser::where('u_yyid', $yyid)
            ->where('wechat_type', DataStatus::WECHAT_AGENT)
            ->first();
    }

    public static function getByYyids($yyids)
    {
        return WechatUser::whereIn('u_yyid', $yyids)
            ->where('wechat_type', DataStatus::WECHAT_AGENT)
            ->get();
    }

    /**
     * 通过openid判断有没有yyid（注册）
     * @param $inviteeId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getYyidByOpenid($inviteeId)
    {
        return $this->wechatUserModel::query()
            ->select('u_yyid')
            ->where('openid', $inviteeId)
            ->first();
    }

    /**
     * 根据另一个微信号绑定当前微信号的用户
     * @param WechatUser $wechat
     * @param Users $user
     * @return bool|int
     */
    public static function bindWechatUser(WechatUser $wechat, Users $user)
    {
        return WechatUser::where('id', $wechat->id)
            ->update([
                'u_yyid' => $user->yyid
            ]);
    }

    /**
     * 根据手机号批量获取用户微信信息
     * @param array $mobiles
     * @param int $wechatType
     * @return Collection
     */
    public static function getWechatInfoByUserMobile(array $mobiles, $wechatType = DataStatus::WECHAT_AGENT): Collection
    {
        return WechatUser::query()
            ->join('t_users as tu', 'tu.yyid', '=', 'u_yyid')
            ->whereIn('tu.mobile_num', $mobiles)
            ->where('wechat_user.wechat_type', $wechatType)
            ->select(['wechat_user.*'])
            ->groupBy('wechat_user.u_yyid')
            ->orderByDesc('wechat_user.id')
            ->get();
    }

    /**
     * @param $unionid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getByUnionid($unionid)
    {
        return WechatUser::where('unionid', $unionid)->first();
    }

    public static function bindWechatUserByAnthorWechatUser($wechatUser, $bindWechat)
    {
        return WechatUser::where('id', $wechatUser->id)
            ->update([
                'u_yyid' => $bindWechat->u_yyid
            ]);
    }

    public static function removeBindUserIdByUserId($yyid)
    {
        WechatUser::where('u_yyid', $yyid)->update([
            'u_yyid' => ''
        ]);
    }
}
