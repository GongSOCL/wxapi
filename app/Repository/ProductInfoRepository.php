<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ProductInfo;
use Hyperf\Utils\Collection;

class ProductInfoRepository
{
    public static function addOne($seriesId, $type, $val): ProductInfo
    {
        $o = new ProductInfo();
        $o->series_id = $seriesId;
        $o->info_type = $type;
        $o->content = $val;
        $o->status = DataStatus::REGULAR;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');
        $o->save();

        return $o;
    }

    public static function getBySeriesId($seriesId): Collection
    {
        return ProductInfo::where([
            'series_id' => $seriesId,
            'status' => DataStatus::REGULAR
        ])->get();
    }
}
