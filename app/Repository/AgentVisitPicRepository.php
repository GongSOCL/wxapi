<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\AgentVisit;
use App\Model\Qa\AgentVisitPic;
use Hyperf\Utils\Collection;

class AgentVisitPicRepository
{
    /**
     * 批量添加上传关系.
     * @param AgentVisit $visit
     * @param array $uploadIds
     * @param int $picType
     * @return bool
     */
    public static function batchAddVisitUploads(
        AgentVisit $visit,
        array $uploadIds,
        $picType = AgentVisitPic::PIC_TYPE_CHECK_IN
    ): bool {
        $data = [];
        $now = date('Y-m-d H:i:s');
        foreach ($uploadIds as $uploadId) {
            $data[] = [
                'visit_id' => $visit->id,
                'pic_type' => $picType,
                'upload_id' => $uploadId,
                'status' => DataStatus::REGULAR,
                'created_time' => $now,
                'modify_time' => $now
            ];
        }

        return AgentVisitPic::insert($data);
    }

    /**
     * 批量清除上传关系.
     * @param AgentVisit $visit
     * @param $uploadIds
     * @return int
     */
    public static function batchDeleteVisitUpload(AgentVisit $visit, $uploadIds): int
    {
        return AgentVisitPic::where([
            'visit_id' => $visit->id,
            'status' => DataStatus::REGULAR
        ])->whereIn('upload_id', $uploadIds)
            ->update([
                'status' => DataStatus::DELETE,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * @param AgentVisit $visit
     * @param int $type
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getVisitUpload(AgentVisit $visit, $type = 0): Collection
    {
        $query = AgentVisitPic::where([
            'visit_id' => $visit->id,
            'status' => DataStatus::REGULAR
        ]);
        if ($type) {
            $query = $query->where('pic_type', $type);
        }
        return $query->get();
    }
}
