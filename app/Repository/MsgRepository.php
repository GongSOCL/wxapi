<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\Msg;
use App\Model\Qa\RepsMsg;
use App\Model\Qa\Users;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Context;

class MsgRepository
{
    /**
     * 获取消息列表
     * @param $yyid
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection|\Hyperf\Database\Query\Builder[]|\Hyperf\Utils\Collection
     */
    public static function getMsgList($yyid, $userType, $page, $pageSize)
    {
        $whereIn = self::getUserTypeCondition($userType);
        return Msg::query()
            ->from('t_msg as m')
            ->leftJoin('t_reps_msg as rm', 'm.yyid', '=', 'rm.msg_yyid')
            ->select(
                'msg_yyid',
                'is_read',
                'content',
                'title',
                'sub_title',
                'msg_type',
                'link',
                Db::raw("right(link,32) as paper_yyid"),
                'm.created_time'
            )
            ->where('rm.user_yyid', $yyid)
            ->where('m.status', Msg::STATUS_AUDITED)
            ->where('rm.is_deleted', RepsMsg::NOT_DELETED)
            ->whereIn('m.msg_type', $whereIn)
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->orderBy('m.created_time', 'desc')
            ->get();
    }

    /**
     * @param $yyid
     * @param $userType
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection|\Hyperf\Database\Query\Builder[]|\Hyperf\Utils\Collection
     */
    public static function getMsgListWithoutPage($yyid, $userType)
    {
        $whereIn = self::getUserTypeCondition($userType);
        return Msg::query()
            ->from('t_msg as m')
            ->leftJoin('t_reps_msg as rm', 'm.yyid', '=', 'rm.msg_yyid')
            ->select(
                'msg_yyid',
                'is_read',
                'content',
                'title',
                'sub_title',
                'msg_type',
                'link',
                Db::raw("right(link,32) as paper_yyid"),
                'm.created_time'
            )
            ->where('rm.user_yyid', $yyid)
            ->where('m.status', Msg::STATUS_AUDITED)
            ->where('rm.is_deleted', RepsMsg::NOT_DELETED)
            ->whereIn('m.msg_type', $whereIn)
            ->orderBy('m.created_time', 'desc')
            ->get();
    }


    /**
     * 获取消息总数量
     * @param $yyid
     * @return int
     */
    public static function getMsgCount($yyid, $userType)
    {
        $whereIn = self::getUserTypeCondition($userType);
        return Msg::query()
            ->from('t_msg as m')
            ->leftJoin('t_reps_msg as rm', 'm.yyid', '=', 'rm.msg_yyid')
            ->where('rm.user_yyid', $yyid)
            ->where('m.status', Msg::STATUS_AUDITED)
            ->where('rm.is_deleted', RepsMsg::NOT_DELETED)
            ->whereIn('m.msg_type', $whereIn)
            ->count();
    }


    /**
     * 条件参数
     * @param $userType
     * @return array
     */
    public static function getUserTypeCondition($userType)
    {
        if ($userType==1) {
            $whereIn =[
                Msg::MSG_FLOW_DATE_UPDATED,
                Msg::MSG_VOTE_NOTICE ,
                Msg::MSG_REWARD_POINT,
                Msg::MSG_CONSUMPUTION_POINT ,
                Msg::MSG_INVITE_TO_GROUP
            ];
        } elseif ($userType==2) {
            $whereIn = [
                Msg::MSG_REWARD_POINT,
                Msg::MSG_CONSUMPUTION_POINT ,
                Msg::MSG_QUESTIONNAIRE_INVITE
            ];
        }

        return $whereIn;
    }

    /**
     * @param $title
     * @param $msgType
     * @param string $subTitle
     * @param string $content
     * @param string $link
     * @param $sendType
     * @param false $isFeedBack
     * @param string $start
     * @param string $end
     * @param false $needAudit
     * @return Msg
     */

    public static function addMsg(
        $title,
        $msgType,
        $subTitle = '',
        $content = '',
        $link = '',
        $sendType = Msg::SEND_TYPE_USER,
        $isFeedBack = false,
        $start = '',
        $end = '',
        $needAudit = false
    ): Msg {
        $user = context::get('user');
        $msg = new Msg();
        $msg->yyid = Db::raw("replace(upper(uuid()),'-','')");
        $msg->title = $title;
        $msg->sub_title = $subTitle;
        $msg->content = $content;
        $msg->link = $link;
        $msg->msg_type = $msgType;
        $msg->send_type = $sendType;
        $msg->is_feedback = $isFeedBack ? 1 : 0;
        $msg->start_time = $start;
        $msg->end_time = $end;
        $msg->status = $needAudit ? 0 : 1;
        $msg->start_time = $msg->modify_time = date('Y-m-d H:i:s');
        $msg->create_user_id = $user && $user instanceof Users ? $user->uid : 0;
        $msg->save();
        $msg->refresh();
        return $msg;
    }
}
