<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\RepsComment;
use App\Model\Qa\Users;
use Hyperf\Utils\Collection;
use Youyao\Framework\ErrCode;
use const OpenTracing\Tags\DATABASE_STATEMENT;

class RepsCommentRepository
{
    public static function getResourceComments($resourceYYID): Collection
    {
        return RepsComment::where('resource_yyid', $resourceYYID)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function addComment($resourceYYID, Users $users, $content, $parentYYID = "", $type = 2): RepsComment
    {
        $o = new RepsComment();
        $o->yyid = Helper::yyidGenerator();
        $o->resource_yyid = $resourceYYID;
        $o->user_yyid = $users->yyid;
        $o->content = $content;
        $o->comment_pyyid = $parentYYID;
        $o->type = $type;
        $o->status = DataStatus::REGULAR;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');
        if (!$o->save()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "评论添加失败，请重试");
        }

        $o->refresh();
        return $o;
    }

    public static function getCommentById($id): ?RepsComment
    {
        return RepsComment::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function deleteParentComment(RepsComment $comment)
    {
        return RepsComment::where('status', DataStatus::REGULAR)
            ->whereRaw("(id = {$comment->id} or comment_pyyid='{$comment->yyid}')")
            ->update([
               'status' => DataStatus::DELETE,
               'modify_time' =>  date('Y-m-d H:i:s')
            ]);
    }

    public static function deleteSubComment(RepsComment $comment)
    {
        return RepsComment::where([
                'id' => $comment->id,
                'status' => DataStatus::REGULAR
            ])->update([
                'status' => DataStatus::DELETE,
                'modify_time' =>  date('Y-m-d H:i:s')
            ]);
    }
}
