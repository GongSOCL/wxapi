<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Qa\DrugSeries;
use App\Model\Qa\SalesTargetMonth;
use App\Model\Qa\YouyaoHospital;
use Hyperf\DbConnection\Db;

class SalesTargetMonthRepository
{

    public static function getByHospitalAndProduct(
        YouyaoHospital $hospital,
        DrugSeries $series,
        string $date
    ): ?SalesTargetMonth {
        return SalesTargetMonth::where([
            'hospital_yyid' => $hospital->yyid,
            'drug_yyid'     => $series->yyid,
            'month' => $date
        ])->first();
    }

    public static function getTotalByHospitalAndSeries(YouyaoHospital $hospital, DrugSeries $series, array $dates)
    {
        return SalesTargetMonth::where([
            'hospital_yyid' => $hospital->yyid,
            'drug_yyid'     => $series->yyid,
        ])->whereIn('month', $dates)
            ->select([Db::raw('sum(target) as total')])
            ->first();
    }

    public static function getTotalByHospitalsAndSeries(array $hospitalYYIDS, string $seriesYYID, array $dates)
    {
        return SalesTargetMonth::where([
            'drug_yyid'     => $seriesYYID,
        ])->where('hospital_yyid', $hospitalYYIDS)
            ->whereIn('month', $dates)
            ->select([Db::raw('sum(target) as total')])
            ->first();
    }

    public static function getByHospitalsAndProduct(
        array $hospitalYYIDS,
        string $seriesYYID,
        string $date
    ): ?SalesTargetMonth {
        return SalesTargetMonth::where([
            'drug_yyid'     => $seriesYYID,
            'month' => $date
        ])->whereIn('hospital_yyid', $hospitalYYIDS)->first();
    }
}
