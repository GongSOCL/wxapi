<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DealerHospital;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;
use Swoole\FastCGI\Record\Data;

class DealerHospitalRepository
{
    public static function getUserDealerHospital(Users $users, YouyaoHospital $hospital): ?DealerHospital
    {
        return DealerHospital::where([
            'dealer_yyid' => $users->company_yyid,
            'hospital_yyid' => $hospital->yyid,
            'status' => DataStatus::REGULAR
        ])->first();
    }
}
