<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Qa\WechatUserSnsapi;

class WechatUserSnsapiRepository
{
    public static function addFromOauthUserInfo($oauthUserInfo): WechatUserSnsapi
    {
        $o = new WechatUserSnsapi();
//        'openid' => $res2['openid'],
//        'nickname' => isset($res2['nickname'])?$res2['nickname']:'',
//        'sex' => isset($res2['sex'])?$res2['sex']:'',
//        'language' => isset($res2['language'])?$res2['language']:'',
//        'city' => isset($res2['city'])?$res2['city']:'',
//        'province' =>isset($res2['province'])?$res2['province']:'',
//        'country' => isset($res2['country'])?$res2['country']:'',
//        'headimgurl' => isset($res2['headimgurl'])?$res2['headimgurl']:'',
//        'privilege' => isset($res2['privilege'])?$res2['privilege']:'',
//        'scope' => $scope,
//        'redirct_url' => $redirectUrl,
        $o->openid = $oauthUserInfo['openid'];
        $o->nickname = $oauthUserInfo['nickname'];
        $o->sex = $oauthUserInfo['sex'];
        $o->language = $oauthUserInfo['language'];
        $o->city = $oauthUserInfo['city'];
        $o->province = $oauthUserInfo['province'];
        $o->country = $oauthUserInfo['country'];
        $o->headimgurl = $oauthUserInfo['headimgurl'];
        $o->privilege = $oauthUserInfo['privilege'];
        $o->redirct_url = '';
        $o->scope = '';

        $o->save();

        return $o;
    }

    public static function addWechatData($oauthUserInfo, $state)
    {
    }

    private static function getRedirectUrlByState($state, $openid)
    {
        $redirectUrl = env('WECHAT_OAUTH_REDIRECT_URL');
        if ($state == "rp") {
            $redirectUrl = "http://rp.youyao99.com?openid=" . $openid;
        } elseif ($state == "activity") {
            $redirectUrl .= "/act_dialog_r.php?version="
                . time() . "&a_v_flag=1&_t=1&t="
                . time() . "&openid=" . $openid
                . "#/activity?openid=" . $openid;
        } elseif ($state == "regist") {
            $redirectUrl .= "/act_dialog_r.php?version="
                . time()
                . "&a_v_flag=1&openid="
                . $openid . "#/activity/register?openid="
                . $openid;
        } elseif ($state == "rank") {
            $redirectUrl .= "/act_dialog_r.php?version="
                . time() . "&a_v_flag=2&openid="
                . $openid . "#/activity/rank_list?openid="
                . $openid;
        } elseif ($state == "youyaolive") {
            $redirectUrl .= "/template/live/live_list.php?version="
                . time() . "&a_v_flag=2&openid="
                . $openid . "#/live/index?openid="
                . $openid;
        } elseif ($state == "youyaolivegkk") {
            $redirectUrl .= "/template/live/live_list.php?version="
                . time() . "&a_v_flag=2&openid="
                . $openid . "#/live/pub_class?openid="
                . $openid;
        } else {
            if (strpos(($state), "v2wx")) {
                $redirectUrl = $redirectUrl . "" . ($state);
            } elseif (strpos(($state), "apibeta")) {
                $redirectUrl = ($state);
            } elseif (strpos(($state), "v2api")) {
                $redirectUrl = ($state);
            } elseif (strpos(($state), "weiv2")) {
                $redirectUrl = ($state);
            } else {
                $redirectUrl = $redirectUrl . "/login.html?version=" . time() . "&backUrl=" . urlencode($state);
            }

            Logger::info(__FILE__, __CLASS__, __LINE__, $redirectUrl);
            return strpos(($state), "?") ?
                ($redirectUrl . "&openid=" . $openid) :
                ($redirectUrl . "?openid=" . $openid);
        }
    }
}
