<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Repository;

use App\Constants\DataStatus;
use App\Service\Points\PointRpcService;
use App\Service\Points\PointsService;
use Grpc\Point\PointDetail;

class PointDetailRepository
{
    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetchList($uid, $page = 1, $pageSize = 20, $type = 0, $fetchService = true, $isAgent = true)
    {
        $userType = $isAgent ? DataStatus::USER_TYPE_AGENT : DataStatus::USER_TYPE_DOCTOR;
        $reply = PointRpcService::fetchPointDetails($uid, $page, $pageSize, (int) $type, $fetchService, $userType);

        $data = [
            'page' => $reply->getPage(),
            'page_size' => $reply->getPageSize(),
            'total' => $reply->getTotal(),
        ];

        $list = [];
        /** @var PointDetail $obj */
        foreach ($reply->getList() as $obj) {
            $createdAt = strtotime($obj->getCreatedAt());
            $point = $fetchService ? $obj->getServicePoint() : $obj->getMoneyPoint();
            $list[] = [
                'detail_yyid' => $obj->getYyid(),
                'type' => $obj->getType(),
                'status' => $obj->getStatus(),
                'desc' => $obj->getDesc(),
                'point' => $obj->getType() == 1 ? $point : -$point,
                'date' => date('Y-m-d', $createdAt),
                'month' => date('n', $createdAt),
                'year_month' => date('Ym', $createdAt),
            ];
        }

        $data['details'] = $list;

        return $data;
    }
}
