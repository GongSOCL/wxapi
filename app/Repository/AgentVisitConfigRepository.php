<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\AgentVisitConfig;
use Hyperf\Utils\Collection;

class AgentVisitConfigRepository
{
    public static function getConfigsByType($type, $visitId = 0): Collection
    {
        $query = AgentVisitConfig::where([
            'config_type' => $type,
            'status' => DataStatus::REGULAR
        ]);
        if ($visitId > 0) {
            $query->whereIn('visit_id', [0, $visitId]);
        }

        return $query->orderBy('config_order')
            ->get();
    }

    public static function getById($id): ?AgentVisitConfig
    {
        return AgentVisitConfig::query()
            ->where('id', $id)
            ->first();
    }
}
