<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DrugProduct;
use Hyperf\Utils\Collection;

class DrugProductRepository
{
    public static function getShowProducts(): Collection
    {
        return DrugProduct::where([
            'is_show' => DataStatus::REGULAR,
            'status' => DataStatus::REGULAR
        ])->orderBy('sort')
            ->get();
    }

    public static function getById($id): ?DrugProduct
    {
        return DrugProduct::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getByYYIDS(array $productYYIDS): Collection
    {
        return DrugProduct::whereIn('yyid', $productYYIDS)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function getByYYID($yyid): ?DrugProduct
    {
        return DrugProduct::where([
            'yyid' => $yyid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getByIds(array $id): Collection
    {
        return DrugProduct::whereIn('id', $id)
            ->get();
    }

    public static function getProductMapByIds(array $ids): array
    {
        $products = self::getByIds($ids);
        return $products->keyBy('id')->all();
    }
}
