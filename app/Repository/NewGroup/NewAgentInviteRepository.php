<?php
declare(strict_types=1);
namespace App\Repository\NewGroup;

use App\Constants\DataStatus;
use App\Model\Qa\NewAgentInvite;
use App\Model\Qa\NewDoctorOpenidUserinfo;

class NewAgentInviteRepository
{
    public static function searchDoctorGroup($userId, $keyword, $userType, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select([
                'ndu.nid',
                'ndou.id as r_id',
                'openid',
                'ndu.unionid as user_unionid',
                'ndu.true_name',
                'ndu.hospital_name',
                'ndu.depart_name',
                'ndou.id as link_doctor_id',
                'nickname',
                'headimgurl',
                'is_subscribed as is_subscribe'
            ])
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->where($keyword)
            ->latest('ndou.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function searchDoctorGroupCount($userId, $keyword, $userType)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'user_id' => $userId,
                'wechat_type' => $userType,
                'status' => DataStatus::REGULAR
            ])
            ->where($keyword)
            ->count();
    }

    public static function searchUnsubscribeGroup($userId, $keyword, $userType, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select([
                'ndu.nid',
                'ndou.id as r_id',
                'openid',
                'ndu.unionid as user_unionid',
                'ndu.true_name',
                'ndu.hospital_name',
                'ndu.depart_name',
                'ndou.id as link_doctor_id',
                'nickname',
                'headimgurl',
                'is_subscribed as is_subscribe'
            ])
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->where(function($query) {
                $query->whereRaw('is_subscribed is null')
                    ->orWhere('is_subscribed', 0);
            })
            ->where($keyword)
            ->latest('ndou.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function searchUnsubscribeGroupCount($userId, $keyword, $userType)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->where(function($query) {
                $query->whereRaw('is_subscribed is null')
                    ->orWhere('is_subscribed', 0);
            })
            ->where($keyword)
            ->count();
    }

    public static function searchSubscribeGroup($userId, $keyword, $userType, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select([
                'ndu.nid',
                'ndou.id as r_id',
                'openid',
                'ndu.unionid as user_unionid',
                'ndu.true_name',
                'ndu.hospital_name',
                'ndu.depart_name',
                'ndou.id as link_doctor_id',
                'nickname',
                'headimgurl',
                'is_subscribed as is_subscribe'
            ])
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'is_subscribed' => 1,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->where($keyword)
            ->latest('ndou.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function searchSubscribeGroupCount($userId, $keyword, $userType)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'ndu.is_subscribed' => 1,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->where($keyword)
            ->count();
    }

    public static function getMyDoctorGroup($userId, $userType, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select([
                'ndu.nid',
                'ndou.id as r_id',
                'openid',
                'ndu.unionid as user_unionid',
                'ndu.true_name',
                'ndu.hospital_name',
                'ndu.depart_name',
                'ndou.id as link_doctor_id',
                'nickname',
                'headimgurl',
                'is_subscribed as is_subscribe',
            ])
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->latest('ndou.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function getMyDoctorGroupCount($userId, $userType)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'user_id' => $userId,
                'wechat_type' => $userType,
                'status' => DataStatus::REGULAR
            ])->count();
    }

    public static function getUnsubscribeGroup($userId, $userType, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select([
                'ndu.nid',
                'ndou.id as r_id',
                'openid',
                'ndu.unionid as user_unionid',
                'ndu.true_name',
                'ndu.hospital_name',
                'ndu.depart_name',
                'ndou.id as link_doctor_id',
                'nickname',
                'headimgurl',
                'is_subscribed as is_subscribe'
            ])
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->where(function ($query) {
                $query->whereRaw('is_subscribed is null')
                    ->orWhere('is_subscribed', 0);
            })
            ->latest('ndou.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function getUnsubscribeGroupCount($userId, $userType)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->where(function ($query) {
                $query->whereRaw('is_subscribed is null')
                    ->orWhere('is_subscribed', 0);
            })->count();
    }

    public static function getSubscribeGroup($userId, $userType, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select([
                'ndu.nid',
                'ndou.id as r_id',
                'openid',
                'ndu.unionid as user_unionid',
                'ndu.true_name',
                'ndu.hospital_name',
                'ndu.depart_name',
                'ndou.id as link_doctor_id',
                'nickname',
                'headimgurl',
                'is_subscribed as is_subscribe'
            ])
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'is_subscribed' => 1,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->latest('ndou.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function getSubscribeGroupCount($userId, $userType)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'ndou.user_id' => $userId,
                'ndu.wechat_type' => $userType,
                'ndu.is_subscribed' => 1,
                'ndou.status' => DataStatus::REGULAR
            ])->count();
    }

    /**
     * 我的好友关系 $userType=1表示代表 2表示医生
     * @param $userId
     * @param $userType
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getMyUserAgents($userId, $userType)
    {
        return NewAgentInvite::where([
            'inviter_id' => $userId,
            'user_type' => $userType,
            'status' => NewAgentInvite::NORMAL
        ])->get();
    }

    /**
     * 查找好友关系
     * @param $inviterId
     * @param $wxinfo
     * @param $userType
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function findRelation($inviterId, $wxinfo, $userType)
    {
        return NewAgentInvite::where([
            'inviter_id' => $inviterId,
            'user_unionid' => $wxinfo['unionid'],
            'user_type' => $userType
        ])->first();
    }

    /**
     * 更新好友关系的status
     * @param $inviterId
     * @param $wxinfo
     * @param $userType
     */
    public static function updateRelationStatus($inviterId, $wxinfo, $userType)
    {
        NewAgentInvite::where([
            'inviter_id' => $inviterId,
            'user_unionid' => $wxinfo['unionid'],
            'user_type' => $userType
        ])->update([
            'status' => NewAgentInvite::NORMAL,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * 添加好友关系
     * @param $inviterId
     * @param $wxinfo
     * @param $userType
     */
    public static function addRelation($inviterId, $wxinfo, $userType)
    {
        NewAgentInvite::insertGetId([
            'inviter_id' => $inviterId,
            'user_openid' => $wxinfo['openid'],
            'user_unionid' => $wxinfo['unionid'],
            'is_subscribe' => NewAgentInvite::IS_SUBSCRIBE,
            'user_type' => $userType,
            'status' => NewAgentInvite::NORMAL,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $userId
     * @param $userType
     * @return \Hyperf\Utils\Collection
     */
    public static function getBindingDoctorsList($userId, $userType, $status = NewAgentInvite::NORMAL)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select([
                'id as r_id',
                'unionid as user_unionid',
                'true_name',
                'hospital_name',
                'depart_name',
                'id as link_doctor_id'
            ])
            ->where([
                'user_id' => $userId,
                'wechat_type' => $userType,
                'status' => $status
            ])->get();
    }

    /**
     * @param $inviterId
     * @param $unionId
     * @param $userType
     * @param $subscribe
     */
    public static function changeSubscribe($inviterId, $unionId, $userType, $subscribe)
    {
        NewAgentInvite::where([
            'inviter_id' => $inviterId,
            'user_unionid' => $unionId,
            'user_type' => $userType,
            'status' => DataStatus::REGULAR
        ])->update([
            'is_subscribe' => $subscribe,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param $id
     */
    public static function deleteDoctor($id)
    {
        NewDoctorOpenidUserinfo::where('id', $id)->update([
            'status' => DataStatus::DELETE,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
