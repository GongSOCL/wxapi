<?php
declare(strict_types=1);
namespace App\Repository\NewGroup;

use App\Constants\DataStatus;
use App\Model\Doctor\WechatUser;
use App\Model\Qa\NewGroupMember;

class NewGroupMemberRepository
{
    /**
     * 获取我加入的群
     * @param $unionid
     * @param $groupType
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getJoinGroupInfo($unionid, $groupType)
    {
        return NewGroupMember::where([
            'user_unionid' => $unionid,
            'group_type' => $groupType,
            'status' => NewGroupMember::NORMAL,
        ])->latest()->get();
    }

    /**
     * 获取待加入该组的人数
     * @param $groupId
     * @param $userType
     * @return int
     */
    public static function getWorkGroupJoinMemberNum($groupId, $groupType)
    {
        return NewGroupMember::where([
            ['group_id', '=', $groupId],
            ['group_type', '=', $groupType],
            ['status', '=', NewGroupMember::JOIN]
        ])->count();
    }

    /**
     * 获取群组人员信息
     * @param $groupId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getGroupMembers($groupId)
    {
        return NewGroupMember::where([
            'group_id' => $groupId,
            'status' => NewGroupMember::NORMAL
        ])->latest()->get();
    }

    /**
     * 判断组员是否在别的群代理该产品
     * @param $seriesId
     * @param $unionIds
     * @param $groupId
     * @return \Hyperf\Utils\Collection
     */
    public static function filterUserIdsBySeriesChange($seriesId, $unionIds, $groupId)
    {
        return NewGroupMember::from('new_group_member as ngm')
            ->join('new_group as ng', 'ng.id', '=', 'ngm.group_id')
            ->whereIn('ngm.user_unionid', $unionIds)
            ->where([
                ['ngm.status', '=', NewGroupMember::NORMAL],
                ['ngm.group_id', '<>', $groupId],
                ['ng.series_id', '=', $seriesId]
            ])->distinct()->get();
    }

    /**
     * 判断是否在群里
     * @param $unionId
     * @param $groupId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getByUserIdAndGroupId($unionId, $groupId)
    {
        return NewGroupMember::where([
            'user_unionid' => $unionId,
            'group_id' => $groupId,
            'status' => NewGroupMember::NORMAL
        ])->first();
    }

    /**
     * 把前群主加入组员
     * @param $groupType
     * @param $groupId
     * @param $wxinfo
     * @return int
     */
    public static function createOneMember($groupType, $groupId, $wxinfo)
    {
        $insertData = [
            'group_id'     => $groupId,
            'group_type'   => $groupType,
            'user_unionid'  => $wxinfo->unionid,
            'inviter_id' => 0,
            'status'       => NewGroupMember::NORMAL,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at'  => date('Y-m-d H:i:s', time()),
        ];

        return NewGroupMember::insertGetId($insertData);
    }

    /**
     * 根据主键删除
     * @param $id
     * @return int
     */
    public static function deleteMemberById($id)
    {
        return NewGroupMember::where('id', $id)
            ->update([
                'status' => NewGroupMember::DELETE,
                'updated_at' => date('Y-m-d H:i:s', time())
            ]);
    }

    public static function deleteMembersByIds($id)
    {
        return NewGroupMember::whereIn('id', $id)
            ->update([
                'status' => NewGroupMember::DELETE,
                'updated_at' => date('Y-m-d H:i:s', time())
            ]);
    }

    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getInfoById($id)
    {
        return NewGroupMember::where('id', $id)->first();
    }

    /**
     * 删除成员
     * @param $groupId
     * @return int
     */
    public static function deleteGroupMembers($groupId)
    {
        return NewGroupMember::where('group_id', $groupId)->update([
            'status' => NewGroupMember::DELETE,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * 待加入列表
     * @param $groupId
     * @param $groupType
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getGroupJoinList($groupId, $groupType)
    {
        return NewGroupMember::where([
            'group_id' => $groupId,
            'group_type' => $groupType,
            'status' => NewGroupMember::JOIN
        ])->get();
    }

    /**
     * 获取待加入和已加入
     * @param $groupId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getGroupMembersByStatus($groupId)
    {
        return NewGroupMember::where('group_id', $groupId)
            ->whereIn('status', [NewGroupMember::JOIN, NewGroupMember::NORMAL])
            ->get();
    }

    /**
     * 组员在其他组代理该药品
     * @param $seriesId
     * @param $unionids
     * @param $groupType
     * @return \Hyperf\Utils\Collection
     */
    public static function filterUserIdsBySeries($seriesId, $unionids, $groupType)
    {
        return NewGroupMember::from('new_group_member as ngm')
            ->join('new_group as ng', 'ng.id', '=', 'ngm.group_id')
            ->whereIn('ngm.user_unionid', $unionids)
            ->where([
                ['ngm.group_type', '=', $groupType],
                ['ngm.status', '=', NewGroupMember::NORMAL],
                ['ng.series_id', '=', $seriesId]
            ])
            ->distinct()
            ->get();
    }

    /**
     * 批准入群
     * @param $groupId
     * @param $approve
     * @param $memberIds
     */
    public static function approveMember($groupId, $approve, $memberIds)
    {
        NewGroupMember::whereIn('id', $memberIds)
            ->where('group_id', $groupId)
            ->update([
                'status' => $approve == 1 ? NewGroupMember::NORMAL : NewGroupMember::REJECT,
                'updated_at' => date('Y-m-d H:i:s', time())
            ]);
    }

    /**
     * @param $memberIds
     * @return \Hyperf\Utils\Collection
     */
    public static function getGroupMembersByIds($memberIds)
    {
        return NewGroupMember::whereIn('id', $memberIds)->get();
    }

    /**
     * 删除成员
     * @param $groupId
     * @param $memberIds
     */
    public static function removeMember($memberIds)
    {
        NewGroupMember::whereIn('id', $memberIds)
            ->where('status', NewGroupMember::NORMAL)->update([
                'status' => NewGroupMember::DELETE,
                'updated_at' => date('Y-m-d H:i:s', time())
            ]);
    }

    /**
     * @param $groupId
     * @param $groupType
     * @param $wxinfo
     * @param $inviterId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function findGroupRelation($groupId, $groupType, $wxinfo, $inviterId)
    {
        return NewGroupMember::where([
            'group_id' => $groupId,
            'group_type' => $groupType,
            'user_unionid' => $wxinfo['unionid']
        ])->whereIn('status', [NewGroupMember::NORMAL, NewGroupMember::JOIN])->first();
    }

    public static function getGroupDoctorsListByGroupId($userId, $groupId, $groupType, $userType, $page, $pageSize)
    {
        return NewGroupMember::from('new_group_member as ngm')
            ->join('new_doctor_userinfo as ndu', 'ngm.new_doctor_id', '=', 'ndu.nid')
            ->join('new_doctor_openid_userinfo as ndou', 'ndu.nid', '=', 'ndou.new_doctor_id')
            ->select([
                'ndou.id as r_id',
                'ndu.nid',
                'ngm.id as member_id',
                'ngm.group_id as group_id',
                'openid',
                'ngm.user_unionid',
                'true_name',
                'hospital_name',
                'depart_name',
                'nickname',
                'headimgurl',
                'is_subscribed as is_subscribe'
            ])
            ->where([
                'ngm.group_id' => $groupId,
                'ngm.group_type' => $groupType,
                'ndu.wechat_type' => $userType,
                'ndou.user_id' => $userId,
                'ndou.status' => DataStatus::REGULAR,
                'ngm.status' => NewGroupMember::NORMAL
            ])
            ->latest('ngm.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function getGroupDoctorsListByGroupIdCount($userId, $groupId, $groupType, $userType)
    {
        return NewGroupMember::from('new_group_member as ngm')
            ->join('new_doctor_userinfo as ndu', 'ngm.new_doctor_id', '=', 'ndu.nid')
            ->join('new_doctor_openid_userinfo as ndou', 'ndu.nid', '=', 'ndou.new_doctor_id')
            ->where([
                'ngm.group_id' => $groupId,
                'ngm.group_type' => $groupType,
                'ndu.wechat_type' => $userType,
                'ndou.user_id' => $userId,
                'ndou.status' => DataStatus::REGULAR,
                'ngm.status' => NewGroupMember::NORMAL
            ])
            ->count();
    }

    public static function searchGroupDoctorsListByGroupId($userId, $keyword, $groupId, $groupType, $userType, $page, $pageSize)
    {
        return NewGroupMember::from('new_group_member as ngm')
            ->join('new_doctor_userinfo as ndu', 'ngm.new_doctor_id', '=', 'ndu.nid')
            ->join('new_doctor_openid_userinfo as ndou', 'ndu.nid', '=', 'ndou.new_doctor_id')
            ->select([
                'ndou.id as r_id',
                'ndu.nid',
                'ngm.id as member_id',
                'ngm.group_id as group_id',
                'openid',
                'ngm.user_unionid',
                'true_name',
                'hospital_name',
                'depart_name',
                'nickname',
                'headimgurl',
                'is_subscribed as is_subscribe'
            ])
            ->where([
                'ngm.group_id' => $groupId,
                'ngm.group_type' => $groupType,
                'ndu.wechat_type' => $userType,
                'ndou.user_id' => $userId,
                'ndou.status' => DataStatus::REGULAR,
                'ngm.status' => NewGroupMember::NORMAL
            ])
            ->where($keyword)
            ->latest('ngm.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function searchGroupDoctorsListByGroupIdCount($userId, $keyword, $groupId, $groupType, $userType)
    {
        return NewGroupMember::from('new_group_member as ngm')
            ->join('new_doctor_userinfo as ndu', 'ngm.new_doctor_id', '=', 'ndu.nid')
            ->join('new_doctor_openid_userinfo as ndou', 'ndu.nid', '=', 'ndou.new_doctor_id')
            ->where([
                'ngm.group_id' => $groupId,
                'ngm.group_type' => $groupType,
                'ndu.wechat_type' => $userType,
                'ndou.user_id' => $userId,
                'ndou.status' => DataStatus::REGULAR,
                'ngm.status' => NewGroupMember::NORMAL
            ])
            ->where($keyword)
            ->count();
    }

    /**
     * @param $inviterId
     * @param $newDoctorId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getInGroup($inviterId, $newDoctorId)
    {
        return NewGroupMember::where([
            'inviter_id' => $inviterId,
            'new_doctor_id' => $newDoctorId,
            'group_type' => NewGroupMember::DOCTOR,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $groupId
     * @param $userUnionid
     * @param $inviterId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
//    public static function getRepeatMember($groupId, $userUnionid, $inviterId)
//    {
//        return NewGroupMember::where([
//            'group_id' => $groupId,
//            'group_type' => NewGroupMember::DOCTOR,
//            'user_unionid' => $userUnionid,
//            'inviter_id' => $inviterId
//        ])->whereIn('status', [NewGroupMember::NORMAL, NewGroupMember::JOIN])->first();
//    }


    ///////////////////////////////////////////////////////////////////////////
    public static function getRepeatMember($groupId, $newDoctorId, $userId)
    {
        return NewGroupMember::where([
            'group_id' => $groupId,
            'group_type' => NewGroupMember::DOCTOR,
            'new_doctor_id' => $newDoctorId,
            'inviter_id' => $userId
        ])->whereIn('status', [NewGroupMember::NORMAL, NewGroupMember::JOIN])->first();
    }

    public static function addMember(
        $newDoctorId,
        $groupId,
        $userId,
        $groupType,
        $status = NewGroupMember::NORMAL
    ) {
        return NewGroupMember::insertGetId([
            'group_id' => $groupId,
            'group_type' => $groupType,
            'new_doctor_id' => $newDoctorId,
            'inviter_id' => $userId,
            'status' => $status,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $newDoctorId
     * @param $inviterId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getOtherMember($newDoctorId, $inviterId)
    {
        return NewGroupMember::where([
            'group_type' => NewGroupMember::DOCTOR,
            'new_doctor_id' => $newDoctorId,
            'inviter_id' => $inviterId
        ])->whereIn('status', [NewGroupMember::NORMAL, NewGroupMember::JOIN])->get();
    }

    /**
     * @param $nid
     * @param $userId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getDoctorGroups($nid, $userId)
    {
        return NewGroupMember::where([
            'group_type' => NewGroupMember::DOCTOR,
            'new_doctor_id' => $nid,
            'inviter_id' => $userId,
        ])
            ->whereIn('status', [NewGroupMember::NORMAL, NewGroupMember::JOIN])
            ->groupBy(['group_id'])
            ->get();
    }

    public static function getWechatUserByOpenid($openid)
    {
        return WechatUser::where('openid', $openid)->first();
    }
}
