<?php
declare(strict_types=1);
namespace App\Repository\NewGroup;

use App\Constants\DataStatus;
use App\Model\Qa\DepartmentUser;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Qa\YouyaoDepartment;

class OrganizationRepository
{
    /**
     * @param $userId
     * @return \Hyperf\Utils\Collection
     */
    public static function getOrgLevel($userId)
    {
        return DepartmentUser::from('t_department_user as tdu')
            ->join('t_youyao_department as tyd', 'tdu.dept_id', '=', 'tyd.id')
            ->select(['dept_id', 'path', 'level'])
            ->where([
                'account_uid' => $userId,
                'tdu.status' => DataStatus::REGULAR,
                'tyd.status' => DataStatus::REGULAR
            ])
            ->get();
    }

    /**
     * @param $dv
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getDepById($dv)
    {
        return YouyaoDepartment::where('id', $dv)->first();
    }

    /**
     * @param $depIds
     * @return \Hyperf\Utils\Collection
     */
    public static function getDepsByIds($depIds)
    {
        return YouyaoDepartment::whereIn('id', $depIds)
            ->where('status', DataStatus::REGULAR)
            ->select(['id as department_id', 'name as department_name', 'level'])
            ->orderBy('id', 'asc')
            ->get();
    }

    /**
     * @param $departId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getSubDepartByParentsId($departId)
    {
        return YouyaoDepartment::where('parent_id', $departId)
            ->where('status', DataStatus::REGULAR)
            ->select(['id as dept_id'])
            ->orderBy('id', 'asc')
            ->get();
    }

    /**
     * @param $departId
     * @param $userId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getDepartByDeptId($departId, $userId)
    {
        return DepartmentUser::where([
            'dept_id' => $departId,
            'account_uid' => $userId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $departId
     * @return \Hyperf\Utils\Collection
     */
    public static function getDepartmentAgent($departId)
    {
        return DepartmentUser::from('t_department_user as tdu')
            ->join('t_users as tu', 'tdu.account_uid', '=', 'tu.uid')
            ->join('t_agent as ta', 'tu.yyid', '=', 'ta.u_yyid')
            ->select(['tu.uid', 'tu.yyid', 'tu.name', 'ta.truename', 'tdu.title', 'tdu.is_leader'])
            ->where([
                ['tdu.dept_id', '=', $departId],
                ['tdu.account_uid', '<>', 0],
                ['tdu.status', '=', DataStatus::REGULAR]
            ])
            ->get();
    }

    /**
     * @param $agentId
     * @return int
     */
    public static function getSubscribeDoctorInAgent($agentId)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'ndou.user_id' => $agentId,
                'ndu.is_subscribed' => 1,
                'ndou.status' => DataStatus::REGULAR
            ])->count();
    }

    /**
     * @param $agentId
     * @return int
     */
    public static function getUnsubscribeDoctorInAgent($agentId)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'ndou.user_id' => $agentId,
                'ndu.is_subscribed' => 0,
                'ndou.status' => DataStatus::REGULAR
            ])->count();
    }

    /**
     * @param $uid
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function getDoctorInAgent($uid, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select(
                [
                    'ndou.id as r_id',
                    'nid as doctor_id',
                    'nickname',
                    'true_name',
                    'headimgurl',
                    'hospital_name',
                    'depart_name',
                    'is_subscribed'
                ]
            )
            ->where([
                'ndou.user_id' => $uid,
                'ndou.status' => DataStatus::REGULAR
            ])
            ->latest('ndou.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    /**
     * @param $uid
     * @return int
     */
    public static function getDoctorCountInAgent($uid)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'ndou.user_id' => $uid,
                'ndou.status' => DataStatus::REGULAR
            ])->count();
    }

    /**
     * @param $keyword
     * @param $doctorIds
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function searchDoctorInAgent($keyword, $doctorIds, $page, $pageSize)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select(
                [
                    'ndou.id as r_id',
                    'nid as doctor_id',
                    'nickname',
                    'true_name',
                    'headimgurl',
                    'hospital_name',
                    'depart_name',
                    'is_subscribed'
                ]
            )
            ->whereIn('nid', $doctorIds)
            ->where('true_name', 'like', '%' . $keyword . '%')
            ->where('ndou.status', DataStatus::REGULAR)
            ->groupBy(['nid'])
            ->latest('ndou.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    /**
     * @param $keyword
     * @param $doctorIds
     * @return \Hyperf\Utils\Collection
     */
    public static function searchDoctorCountInAgent($keyword, $doctorIds)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->whereIn('nid', $doctorIds)
            ->where('true_name', 'like', '%' . $keyword . '%')
            ->where('ndou.status', DataStatus::REGULAR)
            ->groupBy(['nid'])
            ->get();
    }



    public static function getAllDepartment()
    {
        return YouyaoDepartment::all();
    }

    public static function resetLevel($id, $level)
    {
        YouyaoDepartment::where('id', $id)->update([
            'level' => $level
        ]);
    }
}
