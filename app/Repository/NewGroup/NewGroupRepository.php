<?php
declare(strict_types=1);
namespace App\Repository\NewGroup;

use App\Constants\DataStatus;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Qa\NewDoctorUserinfo;
use App\Model\Qa\NewGroup;
use App\Model\Qa\RepsServeScopeS;

class NewGroupRepository
{
    /**
     * 获取我是组长的群
     * @param $userId
     * @param $groupType
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getMyGroupInfo($userId, $groupType)
    {
        return NewGroup::where([
            'leader_id' => $userId,
            'group_type' => $groupType,
            'status' => NewGroup::NORMAL
        ])->latest()->get();
    }

    /**
     * @param $userId
     * @param $groupType
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getMyGroupInfoByGroupName($userId, $groupType)
    {
        return NewGroup::where([
            'leader_id' => $userId,
            'group_type' => $groupType,
            'status' => NewGroup::NORMAL
        ])->latest()->get();
    }

    /**
     * 获取群组列表
     * @param $groupIds
     * @param $keyWord
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function getGroupInfo(
        $groupIds,
        $keyWord,
        $page,
        $pageSize
    ) {
        return NewGroup::from('new_group as g')
            ->leftJoin('t_drug_series as tds', 'g.series_id', '=', 'tds.id')
            ->whereIn('g.id', $groupIds)
            ->where([
                ['g.group_name', 'like', '%' . $keyWord . '%'],
                ['g.status', '=', NewGroup::NORMAL]
            ])
            ->select(['g.id as group_id','group_name','notice','series_id','series_name'])
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->orderBy('g.created_at', 'desc')
            ->get();
    }

    /**
     * 获取群组列表个数
     * @param $groupIds
     * @param $keyWord
     * @return int
     */
    public static function getGroupCount($groupIds, $keyWord)
    {
        return NewGroup::from('new_group as g')
            ->leftJoin('t_drug_series as tds', 'g.series_id', '=', 'tds.id')
            ->whereIn('g.id', $groupIds)
            ->where([
                ['g.group_name', 'like', '%' . $keyWord . '%'],
                ['g.status', '=', NewGroup::NORMAL]
            ])
            ->count();
    }

    /**
     * 根据id获取组信息
     * @param $groupId
     * @param $groupType
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getGroupInfoById($groupId, $groupType)
    {
        return NewGroup::where([
            'id' => $groupId,
            'group_type' => $groupType,
            'status' => NewGroup::NORMAL
        ])->first();
    }

    /**
     * 判断重命名
     * @param $userId
     * @param $groupName
     * @param $groupType
     * @param string $groupId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function sameGroupName($userId, $groupName, $groupType, $groupId = '')
    {
        if ($groupId) {
            //重命名的时候判断 要排除本身
            return NewGroup::where([
                'leader_id' => $userId,
                'group_name' => $groupName,
                'group_type' => $groupType,
                'status' => NewGroup::NORMAL
            ])->where('id', '<>', $groupId)->first();
        } else {
            //创建新群组的时候判断
            return NewGroup::where([
                'leader_id' => $userId,
                'group_name' => $groupName,
                'group_type' => $groupType,
                'status' => NewGroup::NORMAL
            ])->first();
        }
    }

    /**
     * 创建群组
     * @param $userId
     * @param $groupName
     * @param $groupType
     * @return int
     */
    public static function createGroup($userId, $groupName, $groupType)
    {
        return NewGroup::insertGetId([
            'leader_id' => $userId,
            'group_name' => $groupName,
            'group_type' => $groupType,
            'created_id' => $userId,
            'status' => NewGroup::NORMAL,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * 重命名
     * @param $groupId
     * @param $groupName
     * @param $groupType
     * @return int
     */
    public static function renameGroup($groupId, $groupName, $groupType)
    {
        return NewGroup::where([
            'id' => $groupId,
            'group_type' => $groupType
        ])->update([
            'group_name' => $groupName,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * 改公告
     * @param $groupId
     * @param $content
     * @return int
     */
    public static function changeNotice($groupId, $content)
    {
        return NewGroup::where('id', $groupId)->update([
            'notice' => $content,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * 改产品
     * @param $groupId
     * @param $seriesId
     * @return int
     */
    public static function changeSeries($groupId, $seriesId)
    {
        return NewGroup::where('id', $groupId)->update([
            'series_id' => $seriesId,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * 改群主
     * @param $groupId
     * @param $newLeaderId
     * @return int
     */
    public static function changeLeader($groupId, $newLeaderId)
    {
        return NewGroup::where('id', $groupId)->update([
            'leader_id' => $newLeaderId,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * 删除群
     * @param $userId
     * @param $groupId
     * @return int
     */
    public static function deleteGroup($userId, $groupId)
    {
        return NewGroup::where([
            'id' => $groupId,
            'leader_id' => $userId
        ])->update([
            'status' => NewGroup::DELETE,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $groupId
     * @param $tax
     * @return int
     */
    public static function changeTax($groupId, $tax)
    {
        return NewGroup::where('id', $groupId)->update([
            'tax_point' => $tax
        ]);
    }

    public static function getMyServerHospitals($yyid, $page, $pageSize)
    {
        return RepsServeScopeS::from('t_reps_serve_scope_s as trs')
            ->join('t_youyao_hospital as tyh', 'trs.hospital_yyid', '=', 'tyh.yyid')
            ->select(['tyh.id as hospital_id', 'hospital_yyid', 'hospital_name'])
            ->where([
                'trs.user_yyid' => $yyid,
                'trs.status' => DataStatus::REGULAR
            ])
            ->groupBy('tyh.id')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function getMyServerHospitalsCount($yyid)
    {
        return RepsServeScopeS::from('t_reps_serve_scope_s as trs')
            ->join('t_youyao_hospital as tyh', 'trs.hospital_yyid', '=', 'tyh.yyid')
            ->where([
                'trs.user_yyid' => $yyid,
                'trs.status' => DataStatus::REGULAR
            ])
            ->groupBy('tyh.id')
            ->get();
    }

    public static function getMyServerHospitalsWithoutPage($yyid)
    {
        return RepsServeScopeS::from('t_reps_serve_scope_s as trs')
            ->join('t_youyao_hospital as tyh', 'trs.hospital_yyid', '=', 'tyh.yyid')
            ->select(['tyh.id as hospital_id', 'hospital_yyid', 'hospital_name'])
            ->where([
                'trs.user_yyid' => $yyid,
                'trs.status' => DataStatus::REGULAR
            ])
            ->get();
    }

    public static function getMyDoctorsCount($hospitalYyids)
    {
        return NewDoctorUserinfo::whereIn('hospital_yyid', $hospitalYyids)->count();
    }

    public static function getOcDoctorsCount($userId, $hospitalYyids)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'ndou.user_id' => $userId,
                'ndou.status' => DataStatus::REGULAR
            ])->whereIn('hospital_yyid', $hospitalYyids)->count();
    }
}
