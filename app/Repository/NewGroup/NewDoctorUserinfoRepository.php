<?php
declare(strict_types=1);
namespace App\Repository\NewGroup;

use App\Constants\DataStatus;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Qa\NewDoctorUserinfo;
use App\Model\Qa\NewGroupMember;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\Users;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\DbConnection\Db;

class NewDoctorUserinfoRepository
{
    /**
     * @param $id
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getDoctorReps($id)
    {
        return NewDoctorOpenidUserinfo::where('id', $id)->first();
    }

    /**
     * @param $mobile
     * @param $nid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getSameScanDoctor($mobile, $nid)
    {
        return NewDoctorUserinfo::where('mobile_num', $mobile)
            ->whereRaw("openid <> ''")
            ->where('nid', '<>', $nid)
            ->first();
    }

    /**
     * @param $mobile
     * @param $nid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getSameAddDoctor($mobile, $nid)
    {
        return NewDoctorUserinfo::where('mobile_num', $mobile)
            ->where('openid', '')
            ->where('nid', '<>', $nid)
            ->latest()
            ->first();
    }

    /**
     * @param $nid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getShareDoctorInfo($nid)
    {
        return NewDoctorUserinfo::where('nid', $nid)->first();
    }

    /**
     * @param $trueName
     * @param $gender
     * @param $mobile
     * @param $hospitalYyid
     * @param $hospitalName
     * @param $departId
     * @param $departName
     * @param $jobTitle
     * @param $position
     * @param $fieldId
     * @param $kolLevel
     * @param $clinicRota
     * @return int
     */
    public static function createDoctor(
        $trueName,
        $gender,
        $mobile,
        $hospitalYyid,
        $hospitalName,
        $departId,
        $departName,
        $jobTitle,
        $position,
        $fieldId,
        $kolLevel,
        $clinicRota
    ) {
        return NewDoctorUserinfo::insertGetId([
            'wechat_type' => DataStatus::USER_TYPE_DOCTOR,
            'true_name' => $trueName,
            'gender' => $gender,
            'mobile_num' => $mobile,
            'hospital_yyid' => $hospitalYyid,
            'hospital_name' => $hospitalName,
            'depart_id' => $departId,
            'depart_name' => $departName,
            'job_title' => $jobTitle,
            'position' => $position,
            'field_id' => $fieldId,
            'kol_level' => $kolLevel,
            'clinic_rota' => $clinicRota,
            'source_type' => 1,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $nid
     * @param $trueName
     * @param $gender
     * @param $mobile
     * @param $hospitalYyid
     * @param $hospitalName
     * @param $departId
     * @param $departName
     * @param $jobTitle
     * @param $position
     * @param $fieldId
     * @param $kolLevel
     * @param $clinicRota
     * @return int
     */
    public static function updateDoctorInfo(
        $nid,
        $trueName,
        $gender,
        $mobile,
        $hospitalYyid,
        $hospitalName,
        $departId,
        $departName,
        $jobTitle,
        $position,
        $fieldId,
        $kolLevel,
        $clinicRota
    ) {
        NewDoctorUserinfo::where('nid', $nid)->update([
            'true_name' => $trueName,
            'gender' => $gender,
            'mobile_num' => $mobile,
            'hospital_yyid' => $hospitalYyid,
            'hospital_name' => $hospitalName,
            'depart_id' => $departId,
            'depart_name' => $departName,
            'job_title' => $jobTitle,
            'position' => $position,
            'field_id' => $fieldId,
            'kol_level' => $kolLevel,
            'clinic_rota' => $clinicRota,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $nid
     * @param $mobile
     */
    public static function updateDoctorPhone($nid, $mobile)
    {
        NewDoctorUserinfo::where('nid', $nid)->update([
            'mobile_num' => $mobile,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $nid
     * @param $rInfo
     */
    public static function mergeInfo($nid, $rInfo)
    {
        NewDoctorUserinfo::where('nid', $nid)->update([
            'true_name' => $rInfo->true_name,
            'gender' => $rInfo->gender,
            'mobile_num' => $rInfo->mobile_num,
            'hospital_yyid' => $rInfo->hospital_yyid,
            'hospital_name' => $rInfo->hospital_name,
            'depart_id' => $rInfo->depart_id,
            'depart_name' => $rInfo->depart_name,
            'job_title' => $rInfo->job_title,
            'position' => $rInfo->position,
            'field_id' => $rInfo->field_id,
            'clinic_type' => $rInfo->clinic_type,
            'clinic_rota' => $rInfo->clinic_rota,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $relatedId
     * @param $newDoctorId
     */
    public static function updateReps($relatedId, $newDoctorId)
    {
        NewDoctorOpenidUserinfo::where([
            'new_doctor_id' => $relatedId
        ])->update([
            'new_doctor_id' => $newDoctorId,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $newDoctorId
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getRepeatReps($newDoctorId)
    {
        return NewDoctorOpenidUserinfo::where('new_doctor_id', $newDoctorId)
            ->groupBy(['user_id', 'new_doctor_id'])
            ->havingRaw("count(*)>1")
            ->get();
    }

    public static function getRepeatReps1($newDoctorId)
    {
        //查询重复的组合
        $sub = NewDoctorOpenidUserinfo::query()
            ->where('new_doctor_id', $newDoctorId)
            ->where('status', DataStatus::REGULAR)
            ->select(['user_id', 'new_doctor_id', Db::raw('count(*) as cnt')])
            ->groupBy(['user_id', 'new_doctor_id'])
            ->having('cnt', '>', 1);

        return NewDoctorOpenidUserinfo::query()
            ->joinSub($sub, 'sb', function ($query) {
                $query->on('new_doctor_openid_userinfo.user_id', '=', 'sb.user_id')
                    ->on('new_doctor_openid_userinfo.new_doctor_id', '=', 'sb.new_doctor_id');
            })->where('new_doctor_openid_userinfo.status', DataStatus::REGULAR)
            ->select(['new_doctor_openid_userinfo.*'])
            ->get();
    }

    /**
     * @param $repeat
     */
    public static function deleteRepeatReps($relatedIds)
    {
        NewDoctorOpenidUserinfo::whereIn('id', $relatedIds)->update([
            'status' => DataStatus::DELETE,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $relatedId
     */
    public static function deleteDoctor($relatedId)
    {
        NewDoctorUserinfo::where('nid', $relatedId)->delete();
    }

    /**
     * @param $hospitalYyid
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getNewDoctorsByHospital($hospitalYyid)
    {
        return NewDoctorUserinfo::where('hospital_yyid', $hospitalYyid)->get();
    }

    /**
     * @param $userId
     * @param $nid
     */
    public static function createDoctorReps($userId, $nid)
    {
        NewDoctorOpenidUserinfo::insert([
            'user_id' => $userId,
            'new_doctor_id' => $nid,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $userId
     * @param $nids
     */
    public static function deleteDoctorsReps($userId, $nids)
    {
        NewDoctorOpenidUserinfo::where('user_id', $userId)->whereIn('new_doctor_id', $nids)->update([
            'status' => DataStatus::DELETE,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    public static function getAgentServeHospitalDoctors(
        Users $user,
        $withWechat = true,
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        $sub = RepsServeScopeS::query()
            ->join('t_youyao_hospital as tyh', 'tyh.yyid', '=', 't_reps_serve_scope_s.hospital_yyid')
            ->join('new_doctor_userinfo as ndu', 'ndu.hospital_yyid', '=', 'tyh.yyid')
            ->whereIn('t_reps_serve_scope_s.status', [1, 5, 6])
            ->where('tyh.status', DataStatus::REGULAR)
            ->when($withWechat, function ($query) {
                $query->where('ndu.unionid', '!=', '');
            })->select([Db::raw('distinct ndu.nid as ndid')]);
        return NewDoctorUserinfo::query()
            ->joinSub($sub, 'sb', 'sb.ndid', '=', 'new_doctor_userinfo.nid')
            ->select(['new_doctor_userinfo.*'])
            ->paginate($limit, ['*'], '', $current);
    }

    public static function getDoctorById($doctorId): ?NewDoctorUserinfo
    {
        return NewDoctorUserinfo::query()
            ->where('nid', $doctorId)
            ->first();
    }

    public static function getMyDoctors($where, $page, $pageSize)
    {
        return NewDoctorUserinfo::where($where)
            ->select(['nid', 'openid', 'nickname', 'true_name', 'headimgurl', 'hospital_yyid', 'hospital_name',
                'depart_id', 'depart_name', 'is_subscribed', 'kol_level'])
            ->latest()
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    public static function getMyDoctorsCount($where)
    {
        return NewDoctorUserinfo::where($where)->count();
    }

    public static function getMyDoctorInGroup($uid, $nid)
    {
        return NewDoctorOpenidUserinfo::where([
            'user_id' => $uid,
            'new_doctor_id' => $nid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getMyDoctorsInGroup($uid, $nid)
    {
        return NewGroupMember::where([
            'inviter_id' => $uid,
            'new_doctor_id' => $nid,
            'status' => DataStatus::REGULAR
        ])->get();
    }

    public static function checkSame($trueName, $hospitalYyid)
    {
        return NewDoctorUserinfo::where([
            'true_name' => $trueName,
            'hospital_yyid' => $hospitalYyid
        ])->first();
    }
}
