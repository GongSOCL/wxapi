<?php
declare(strict_types=1);
namespace App\Repository\NewGroup;

use App\Constants\DataStatus;
use App\Model\Qa\DrugProduct;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Qa\NewGroup;
use App\Model\Qa\NewGroupDepartMember;
use App\Model\Qa\NewGroupIsAuto;
use App\Model\Qa\NewGroupMember;
use App\Model\Qa\NewGroupStaticDepartment;
use App\Model\Qa\NewRepsGroupDepartment;
use App\Model\Qa\RepsServeScopeS;
use Swoole\Http\Status;

class NewGroupStaticRepository
{
    /**
     * @param $userYyid
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getServerReps($userYyid)
    {
        return RepsServeScopeS::where([
            'user_yyid' => $userYyid,
            'status' => DataStatus::REGULAR
        ])->get();
    }

    /**
     * @param $seriesYyids
     * @return \Hyperf\Utils\Collection
     */
    public static function getProducts($seriesYyids)
    {
        return DrugProduct::whereIn('series_yyid', $seriesYyids)->get();
    }

    /**
     * @param $productIds
     * @return \Hyperf\Utils\Collection
     */
    public static function getStaticDeparts($productIds)
    {
        return NewGroupStaticDepartment::from('new_group_static_department as ngsd')
            ->join('new_reps_group_department as nrgd', 'nrgd.group_department_id', '=', 'ngsd.id')
            ->where('ngsd.status', DataStatus::REGULAR)
            ->where('nrgd.status', DataStatus::REGULAR)
            ->whereIn('product_id', $productIds)
            ->select(['ngsd.id as depart_id', 'name', 'img'])
            ->groupBy(['ngsd.id'])
            ->orderBy('rank', 'desc')
            ->get();
    }

    /**
     * @param $userId
     * @param $departId
     * @return int
     */
    public static function getStaticMembersCount($userId, $departId)
    {
        return NewGroupDepartMember::where([
            'user_id' => $userId,
            'group_department_id' => $departId,
            'status' => DataStatus::REGULAR
        ])->count();
    }

    /**
     * @param $userId
     * @param $departId
     * @param $page
     * @param $pageSize
     * @return \Hyperf\Utils\Collection
     */
    public static function getStaticMembersByUserId($userId, $departId, $page, $pageSize)
    {
        return NewGroupDepartMember::from('new_group_depart_member as ngdm')
            ->join('new_doctor_userinfo as ndu', 'ngdm.new_doctor_id', '=', 'ndu.nid')
            ->select([
                'ngdm.id as s_id',
                'ndu.nid',
                'nickname',
                'true_name',
                'hospital_name',
                'depart_name',
                'headimgurl',
                'is_subscribed as is_subscribe'
            ])
            ->where([
                'ngdm.user_id' => $userId,
                'ngdm.group_department_id' => $departId,
                'ngdm.status' => DataStatus::REGULAR
            ])
            ->latest('ngdm.created_at')
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    /**
     * @param $userId
     * @param $departId
     * @return int
     */
    public static function getStaticMembersByUserIdCount($userId, $departId)
    {
        return NewGroupDepartMember::from('new_group_depart_member as ngdm')
            ->join('new_doctor_userinfo as ndu', 'ngdm.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'ngdm.user_id' => $userId,
                'ngdm.group_department_id' => $departId,
                'ngdm.status' => DataStatus::REGULAR
            ])->count();
    }

    /**
     * @param $userId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function isAutoGroup($userId)
    {
        return NewGroupIsAuto::where([
            'user_id' => $userId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $userId
     */
    public static function createAutoFlag($userId)
    {
        NewGroupIsAuto::insert([
            'user_id' => $userId,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $userId
     */
    public static function deleteAutoGroup($userId)
    {
        NewGroupDepartMember::where('user_id', $userId)->update([
            'status' => DataStatus::DELETE,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $userId
     * @return \Hyperf\Utils\Collection
     */
    public static function getMyDoctors($userId)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'user_id' => $userId,
                'status' => DataStatus::REGULAR
            ])->get();
    }

    /**
     * @param $depart
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getStaticDepartById($depart)
    {
        return NewRepsGroupDepartment::where([
            'department_id' => $depart,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $depart
     * @param $userId
     * @param $doctorId
     */
    public static function deleteStaticDoctor($depart, $userId, $doctorId)
    {
        NewGroupDepartMember::where([
            'department_id' => $depart,
            'user_id' => $userId,
            'new_doctor_id' => $doctorId,
        ])->update([
            'status' => DataStatus::DELETE,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $userId
     * @param $doctorId
     */
    public static function removeStaticDoctor($userId, $doctorId)
    {
        NewGroupDepartMember::where([
            'user_id' => $userId,
            'new_doctor_id' => $doctorId,
        ])->update([
            'status' => DataStatus::DELETE,
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $groupDepartmentId
     * @param $depart
     * @param $userId
     * @param $doctorId
     */
    public static function createStaticDoctor($groupDepartmentId, $depart, $userId, $doctorId)
    {
        NewGroupDepartMember::insert([
            'group_department_id' => $groupDepartmentId,
            'department_id' => $depart,
            'user_id' => $userId,
            'new_doctor_id' => $doctorId,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $groupDepartmentId
     * @param $depart
     * @param $userId
     * @param $doctorId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getAutoReps($groupDepartmentId, $depart, $userId, $doctorId)
    {
        return NewGroupDepartMember::where([
            'group_department_id' => $groupDepartmentId,
            'department_id' => $depart,
            'user_id' => $userId,
            'new_doctor_id' => $doctorId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $userId
     * @param $nid
     * @return \Hyperf\Database\Model\Builder[]|\Hyperf\Database\Model\Collection
     */
    public static function getInGroupList($userId, $nid)
    {
        return NewGroupMember::where([
            'group_type' => NewGroupMember::DOCTOR,
            'new_doctor_id' => $nid,
            'inviter_id' => $userId,
        ])->whereIn('status', [NewGroupMember::NORMAL, NewGroupMember::JOIN])
            ->groupBy(['group_id'])
            ->get();
    }

    /**
     * @param $groupIds
     * @return \Hyperf\Utils\Collection
     */
    public static function getGroupName($groupIds)
    {
        return NewGroup::whereIn('id', $groupIds)->select(['id', 'group_name'])->get();
    }
}
