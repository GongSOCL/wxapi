<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\UserIdentity;
use Hyperf\Database\Query\Expression;
use Hyperf\Di\Annotation\Inject;

class UserIdentifyRepository
{
    /**
     * @Inject
     * @var UserIdentity
     */
    private $userIdentifyModel;


    /**
     * @param $yyid
     * @param $front
     * @param $back
     */
    public function create(
        $front,
        $back,
        $name,
        $sex,
        $ethnicity,
        $birthDate,
        $address,
        $idNumber,
        $issueAuthority,
        $validPeriod,
        $mobile,
        $info
    ) {

        $data = [
//            'yyid'                  => new Expression("replace(upper(uuid()),'-','')"),
//            'user_yyid'             => $yyid,
            'uid'             => $info->uid,
            'idcard_front_location' => $front,
            'idcard_back_location'  => $back,
            'realname'      => $name,
            'id_code'       => $idNumber,
            'gender'        => $sex,
            'nationality'   => $ethnicity,
            'birth_date'    => $birthDate,
            'address'       => $address,
            'mobile'        => $mobile,
            'platform'      => 1,   # 平台选择 1优药 2优佳医 4优能汇
            'due_date'      => trim(strstr($validPeriod, '-'), '-'),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ];
        $this->userIdentifyModel->insert($data);
    }
}
