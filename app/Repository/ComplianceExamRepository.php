<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ExaminationResult;
use App\Model\Qa\LearningMaterialsDistribution;
use App\Model\Qa\LearningPlan;
use App\Model\Qa\Users;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\Database\Query\Builder;
use Hyperf\DbConnection\Db;
use Hyperf\Paginator\LengthAwarePaginator;

class ComplianceExamRepository
{
    public static function filterExams(
        $keywords = "",
        $current = 1,
        $limit = 10,
        array $rangeyyids = [],
        $allStatus = true,
        $isExpire = false
    ): LengthAwarePaginator {
        $query = ComplianceExam::query()
            ->where('platform', ComplianceExam::PLATFORM_YOUYAO)
            ->where('status', DataStatus::REGULAR);
        if (!empty($rangeyyids)) {
            $query = $query->whereIn("yyid", $rangeyyids);
        }

        if ($keywords) {
            $query = $query->where("name", "like", "%${keywords}%");
        }


        if (!$allStatus) {
            $now = date('Y-m-d H:i:s');
            if ($isExpire) {    //已过期
                $query = $query->whereRaw("(exam_end is not null and exam_end <= '$now')");
            } else {    //未过期
                $query = $query->whereRaw("((exam_end is null) or (exam_end is not null and exam_end > '$now'))");
            }
        }

        return $query->orderByDesc('id')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function getById(int $id): ?ComplianceExam
    {
        return ComplianceExam::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();
    }


    /**
     * @param string $yyid
     * @return ComplianceExam|\Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getByYyid(string $yyid)
    {
        return ComplianceExam::query()->where('yyid', $yyid)->first();
    }

    /**
     * 带分页获取用户的所有考试
     * @param Users $users
     * @param array $productYYIDS
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return LengthAwarePaginatorInterface
     */
    public static function getUserExamsWithPage(
        Users $users,
        array $productYYIDS,
        string $keywords = "",
        int $current = 1,
        int $limit = 10
    ): LengthAwarePaginatorInterface {
        $subQuery = Db::table('t_compliance_exam as tce')
            ->select(['tce.id'])
            ->orderByDesc('tce.id')
            ->join('t_compliance_exam_user as tceu', 'tceu.exam_yyid', '=', 'tce.yyid')
            ->where('tce.platform', ComplianceExam::PLATFORM_YOUYAO)
            ->where('tceu.status', DataStatus::REGULAR)
            ->where('tce.status', DataStatus::REGULAR)
            ->where(function (Builder $query) use ($users, $productYYIDS) {
                $query->where('tceu.user_yyid', $users->yyid);
                if (!empty($productYYIDS)) {
                    $query->orWhereIn('tceu.product_yyid', $productYYIDS);
                }
            });
        if ($keywords) {
            $subQuery = $subQuery->where('tce.name', 'like', "%{$keywords}%");
        }

        return ComplianceExam::query()
            ->whereIn('id', $subQuery)
            ->orderByDesc('id')
            ->paginate($limit, ['*'], '', $current);
    }


    /**
     * 获取计划信息
     * @param $exam_type
     * @param $exam_id
     * @param $uid
     * @return array
     */
    public static function getLearnPlans($exam_type, $exam_id, $uid)
    {
        $where = [
            ['a.status',1],
            ['b.status',1],
            ['b.is_deleted',0],
            ['d.exam_id',$exam_id],
            ['b.user_id',$uid],
            ['c.user_id',$uid]
        ];

        $field = [
            'a.name',
            'a.date_type',
            'a.dt_off',
            'a.dt_on',
            'b.one_day',
            'b.last_day',
            'b.exam_start',
            'b.plan_id',
            'c.sdt',
            'c.employment_date'
        ];

        $LearningPlanInfo = LearningPlan::from('learning_plan as a')
            ->join('learning_plan_exams as d', 'a.id', '=', 'd.plan_id')
            ->join('learning_plan_msg_node as b', 'a.id', '=', 'b.plan_id')
            ->join('learning_materials_distribution as c', 'a.id', '=', 'c.l_id')
            ->where($where)
            ->select($field)
            ->first();

        return $LearningPlanInfo?$LearningPlanInfo->toArray():[];
    }


    /**
     * 获取计划状态【用户】
     * @param $exam_id
     * @param $uid
     * @return array
     */
    public static function getUserLearnPLanStatus($plan_id, $uid)
    {
        $field = ["lr_st as plan_status",'finish_day'];
        $where = [['is_deleted',0],['l_id',$plan_id],['user_id',$uid]];
        $LearningPlanStatus =  LearningMaterialsDistribution::where($where)
            ->select($field)
            ->first();
        return $LearningPlanStatus?$LearningPlanStatus->toArray():[];
    }


    /**
     * 获取当前用户考试状态
     * @param $exam_id
     * @param $yyid
     * @return array
     */
    public static function getExamStatus($exam_id, $yyid)
    {
        $where = [['exam_id',$exam_id],['user_yyid',$yyid],['is_deleted',0]];
        $field = ['status','exam_num'];
        $ExamInfo = ExaminationResult::where($where)
            ->select($field)
            ->first();
        return $ExamInfo?$ExamInfo->toArray():[];
    }

    /**
     * 获取当前用户 关联考试下的计划状态
     * @param $exam_id
     * @param $uid
     * @return array
     */
    public static function getLearnPlanInfo($exam_id, $uid)
    {
        $where = [
            ['d.exam_id',$exam_id],
            ['b.user_id',$uid],
            ['a.status',1],
            ['a.is_deleted',0],
            ['b.status','>=',1],
            ['b.is_deleted',0]
        ];
        $field = [
            'a.id',
            'a.dt_off',
            'a.date_type',
            'a.dt_on',
            'a.name',
            Db::raw("GROUP_CONCAT(b.lr_st SEPARATOR ',') as plan_status")
        ];
        $LearningPlanInfo = LearningPlan::from('learning_plan as a')
            ->join('learning_materials_distribution as b', 'b.l_id', '=', 'a.id')
            ->join('learning_plan_exams as d', 'a.id', '=', 'd.plan_id')
            ->where($where)
            ->select($field)
            ->first();
        return $LearningPlanInfo?$LearningPlanInfo->toArray():[];
    }

    /**
     * 获取当前用户 关联考试下的计划信息
     * @param $exam_id
     * @param $uid
     * @return array
     */
    public static function getPLanAndUserInfo($plan_id, $uid)
    {
        $where = [
            ['a.id',$plan_id],
            ['b.user_id',$uid],
            ['a.status',1],
            ['a.is_deleted',0],
            ['b.status','>=',1],
            ['b.is_deleted',0]
        ];
        $field = ['a.dt_off','a.dt_on','a.date_type','b.employment_date'];
        $PLanAndUserInfo = LearningPlan::from('learning_plan as a')
            ->join('learning_materials_distribution as b', 'b.l_id', '=', 'a.id')
            ->where($where)
            ->select($field)
            ->first();
        return $PLanAndUserInfo?$PLanAndUserInfo->toArray():[];
    }
}
