<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ComplianceKnowledgeDir;
use Hyperf\Utils\Collection;

class ComplianceKnowledgeDirRepository
{

    public static function getDirById($dirId)
    {
        return ComplianceKnowledgeDir::where([
            'id' => $dirId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getSubDirById(array $dirIds): Collection
    {
        return ComplianceKnowledgeDir::whereIn('id', $dirIds)
            ->where('status', DataStatus::REGULAR)
            ->whereRaw("p_yyid is not null")
            ->get();
    }

    public static function getParentDirs(array $yyids): Collection
    {
        return ComplianceKnowledgeDir::whereIn('yyid', $yyids)
        ->where('status', DataStatus::REGULAR)
        ->get();
    }
}
