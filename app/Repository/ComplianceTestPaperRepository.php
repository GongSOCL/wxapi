<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\ComplianceTestPaper;

class ComplianceTestPaperRepository
{
    /**
     * 根据id获取所有的试卷
     * @param $testPaperIds
     * @return \Hyperf\Utils\Collection
     */
    public static function getTestPapersByIds($testPaperIds)
    {
        return ComplianceTestPaper::whereIn('id', $testPaperIds)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    /**
     * @param string $yyid
     * @return ComplianceTestPaper|null
     */
    public static function getByYYID(string $yyid): ?ComplianceTestPaper
    {
        return ComplianceTestPaper::where('yyid', $yyid)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }
}
