<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\YouyaoHospital;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Collection;

class YouyaoHospitalRepository
{
    /**
     * @Inject
     * @var YouyaoHospital
     */
    private $youyaoHospitalModel;

    public static function getHospitalByIds(array $hospitalIds): Collection
    {
        return YouyaoHospital::whereIn('id', $hospitalIds)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    /**
     * @param string $provinceYYID
     * @param string $cityYYID
     * @param array $level
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return LengthAwarePaginatorInterface
     */
    public static function searchHospital(
        $provinceYYID = "",
        $cityYYID = "",
        $level = [],
        $keywords = "",
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        $query = YouyaoHospital::query()
                ->where('status', DataStatus::REGULAR);
        if ($provinceYYID) {
            $query->where('pro_yyid', $provinceYYID);
        }

        if ($cityYYID) {
            $query->where('city_yyid', $cityYYID);
        }

        if (!empty($level)) {
            $q = function ($query) use ($level) {
                if (in_array(YouyaoHospital::HOSPITAL_LEVEL_THREE, $level)) {
                    $query->where('level', 'like', "%三%", 'or');
                }

                if (in_array(YouyaoHospital::HOSPITAL_LEVEL_TWO, $level)) {
                    $query->orWhere('level', 'like', "%二%");
                }

                if (in_array(YouyaoHospital::HOSPITAL_LEVEL_ONE, $level)) {
                    $query->orWhere('level', 'like', "%一%");
                }

                if (in_array(YouyaoHospital::HOSPITAL_LEVEL_UNKNOWN, $level)) {
                    $query->orWhereRaw("(level = '' or level is null)");
                }
            };

            $query->where($q);
        }

        if ($keywords) {
            $query = $query->where('hospital_name', 'like', "%{$keywords}%");
        }

        return $query->paginate($limit, ['*'], '', $current);
    }

    public static function updateStatusNormal(string $hospitalYYID)
    {
        YouyaoHospital::where('yyid', $hospitalYYID)
            ->update([
                'status' => DataStatus::REGULAR,
                'modify_time' => date('Y-m-d H:i:s')
            ]);
    }

    public static function getHospitalMapByIds(array $ids): array
    {
        $resp = self::getHospitalByIds($ids);
        return $resp->keyBy('id')->all();
    }

    public static function getIdByYyids(array $yyids): Collection
    {
        return YouyaoHospital::query()
            ->whereIn('yyid', $yyids)
            ->select(['id', 'yyid'])
            ->get();
    }

    /**
     * 通过医院yyid查询该医院是否存在
     * @param $yyid
     * @return object
     */
    public function checkHosptial($yyid)
    {
        return $this->youyaoHospitalModel::query()
            ->select('yyid', 'hospital_name', 'alias_name')
            ->where('yyid', $yyid)
            ->first();
    }

    public static function getHospitalById($hospitalId): ?YouyaoHospital
    {
        return YouyaoHospital::where([
            'id' => $hospitalId,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getHospitalByYYID($YYID): ?YouyaoHospital
    {
        return YouyaoHospital::where([
            'yyid' => $YYID,
            'status' => YouyaoHospital::HOSPITAL_NORMAL
        ])->orderByDesc("id")
            ->first();
    }

    public static function getHospitalByYYIDs(array $YYIDs): Collection
    {
        return YouyaoHospital::whereIn('yyid', $YYIDs)
        ->where('status', YouyaoHospital::HOSPITAL_NORMAL)
            ->orderByDesc("id")
            ->get();
    }
}
