<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\Department;
use Hyperf\Di\Annotation\Inject;

class DepartmentRepository
{
    /**
     * @Inject
     * @var Department
     */
    private $departmentModel;

    /**
     * 通过科室yyid验证科室是否存在
     * @param $hyyId
     * @param $dyyid
     * @return object
     */
    public function checkDepartment($hyyId, $dyyid)
    {
        return $this->departmentModel::query()
            ->select('yyid', 'name')
            ->where(['h_yyid' => $hyyId,'id' => $dyyid])
            ->first();
    }

    /**
     * 根据医院id获取科室信息
     * @param $hyyid
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getDepartmentInfo($hyyid, $page, $pageSize)
    {
        return $this->departmentModel::query()
            ->select('id', 'yyid', 'name', 'h_yyid')
            ->where(['h_yyid' => $hyyid,'status'=>$this->departmentModel::DEPARTMENT_SHOW])
            ->offset($pageSize*($page-1))
            ->limit($pageSize)
            ->get();
    }

    /**
     * @param $hyyid
     * @return int
     */
    public function getDepartmentCount($hyyid)
    {
        return $this->departmentModel::query()
            ->where(['h_yyid' => $hyyid,'status'=>$this->departmentModel::DEPARTMENT_SHOW])
            ->count();
    }
}
