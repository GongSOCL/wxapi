<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\TRepsAgentStore;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;

/**
 * 用于药店关联，指定代表与药店或医院关系
 * Class TRepsAgentStoreRepository
 *
 * @package App\Repository
 */
class TRepsAgentStoreRepository
{

    public static function getUserServeHospitals($userId, $keywords = '')
    {
        return TRepsAgentStore::query()
            ->from("t_reps_agent_store as r")
            ->select(['h.id', 'h.hospital_name', 'r.sort'])
            ->distinct()
            ->join(
                "t_youyao_hospital as h",
                'r.hospital_id',
                '=',
                'h.id'
            )
            ->where('r.user_id', $userId)
            ->where('h.status', DataStatus::REGULAR)
            ->whereIn("r.status", [1, 5, 6])
            ->when(
                $keywords != '',
                function ($q) use ($keywords) {
                    $q->where('h.hospital_name', 'like', "%{$keywords}%");
                }
            )->orderByDesc('r.sort')
            ->get();
    }

    public static function isUserServeHospital($userId, $hospitalId): bool
    {
        return TRepsAgentStore::query()
            ->where('user_id', $userId)
            ->where('hospital_id', $hospitalId)
            ->where('status', [1, 5, 6])
            ->exists();
    }

    public static function getStoreAgents($storeId)
    {
        return TRepsAgentStore::query()
            ->from("t_reps_agent_store as r")
            ->select(['u.uid', 'u.name'])
            ->distinct()
            ->join(
                "t_users as u",
                'r.user_id',
                '=',
                'u.uid'
            )
            ->where('r.hospital_id', $storeId)
            ->where('u.v_status', DataStatus::REGULAR)
            ->whereIn("r.status", [1, 5, 6])
            ->get();
    }
}
