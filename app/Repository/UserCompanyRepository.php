<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\Company;
use App\Model\Qa\UserCompany;
use App\Model\Qa\Users;
use Carbon\Carbon;

class UserCompanyRepository
{
    public static function findAddOrUpdate(Users $users, Company $company): UserCompany
    {
        $exists = UserCompany::query()
            ->where('company_id', $company->id)
            ->where('user_id', $users->uid)
            ->first();
        if ($exists) {
            return $exists;
        }
        $o = new UserCompany();
        $o->user_id = $users->uid;
        $o->company_id = $company->id;
        $o->created_at = $o->updated_at = Carbon::now();
        $o->save();
        $o->refresh();

        return $o;
    }
}
