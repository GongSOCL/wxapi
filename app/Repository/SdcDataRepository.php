<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DealerHospital;
use App\Model\Qa\DrugProduct;
use App\Model\Qa\RepsServeScope;
use App\Model\Qa\RepsServeScopeP;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\SdcData;
use App\Model\Qa\Users;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;
use InvalidArgumentException;

class SdcDataRepository
{

    public static function getLastDate($lastDate, $limit = 6)
    {
        $sql = <<<eof
 SELECT order_month as order_month
        FROM t_reps_serve_scope s 
        LEFT JOIN t_sdc_data sdc ON (sdc.hospital_yyid=s.hospital_yyid and sdc.drug_yyid=s.drug_yyid)
        WHERE (sdc.order_date >= ?)
        AND (s.status='1' or s.status='5' or s.status='6')
        GROUP BY order_month
        ORDER BY order_month DESC limit ?; 
eof;

        return Db::select($sql, [$lastDate, $limit]);
    }

    /**
     * 获取单个月份流向数据
     * @param mixed $month
     * @param array $skuYYIDS
     * @return Collection
     */
    public static function getTransactionMonthData(
        $month,
        array $skuYYIDS,
        Users $user,
        array $hospitalYYIDS = []
    ): Collection {
        $query = Db::table("t_reps_serve_scope as s")
            ->leftJoin("t_sdc_data as sdc", function ($join) {
                $join->on("sdc.hospital_yyid", '=', "s.hospital_yyid")
                    ->on('sdc.drug_yyid', '=', 's.drug_yyid');
            })->leftJoin("t_youyao_hospital as tyh", "tyh.yyid", "=", "sdc.hospital_yyid")
            ->whereIn('s.status', [
                RepsServeScope::STATUS_ACCESS,
                RepsServeScope::STATUS_REMOVE_APPLY,
                RepsServeScope::STATUS_REMOVE_FAILED
            ])->where("s.user_yyid", $user->yyid)
            ->whereIn('s.drug_yyid', $skuYYIDS)
            ->where('tyh.status', DataStatus::REGULAR)
            ->where('sdc.order_month', $month);
        if (!empty($hospitalYYIDS)) {
            $query = $query->whereIn('s.hospital_yyid', $hospitalYYIDS);
        }
        return $query->select(
            [
                'sdc.order_date',
                Db::raw('tyh.yyid as hospital_yyid'),
                Db::raw("sum(sdc.order_num) as order_num")
            ]
        )
            ->groupBy(['sdc.order_date', 'tyh.yyid'])
            ->get();
    }


    /**
     * 获取多个月份流向数据
     * @param array $months
     * @param array $skuYYID
     * @return Collection
     */
    public static function getTransactionMultiMonthData(
        array $months,
        array $skuYYID,
        Users $user,
        array $hospitalYYID = []
    ): Collection {
        $query = Db::table("t_reps_serve_scope as s")
            ->leftJoin("t_sdc_data as sdc", function ($join) {
                $join->on("sdc.hospital_yyid", '=', "s.hospital_yyid")
                    ->on('sdc.drug_yyid', '=', 's.drug_yyid');
            })->leftJoin("t_youyao_hospital as tyh", "tyh.yyid", "=", "sdc.hospital_yyid")
            ->whereIn('s.status', [
                RepsServeScope::STATUS_ACCESS,
                RepsServeScope::STATUS_REMOVE_APPLY,
                RepsServeScope::STATUS_REMOVE_FAILED
            ])->where("s.user_yyid", $user->yyid)
            ->whereIn('s.drug_yyid', $skuYYID)
            ->where('tyh.status', DataStatus::REGULAR)
            ->whereIn('sdc.order_month', $months);
        if (!empty($hospitalYYID)) {
            $query = $query->whereIn('s.hospital_yyid', $hospitalYYID);
        }
           return $query->select(
               [
                   'sdc.order_month',
                   'sdc.order_date',
                   Db::raw('tyh.yyid as hospital_yyid'),
                   Db::raw("sum(sdc.order_num) as order_num")
               ]
           )
                ->groupBy(['sdc.order_month', 'sdc.order_date', 'tyh.yyid'])
                ->get();
    }

    public static function getDailyTrans(Users $users, array $skuYYID, $orderDate, $createDate): Collection
    {
        $a = new \DateTime($createDate);
        $start = $a->format('Y-m-d 00:00:00');
        $end = $a->modify("+1 day")->format('y-m-d 00:00:00');

        return Db::table("t_reps_serve_scope as s")
            ->leftJoin("t_sdc_data as sdc", function ($join) {
                $join->on("sdc.hospital_yyid", '=', "s.hospital_yyid")
                    ->on('sdc.drug_yyid', '=', 's.drug_yyid');
            })->leftJoin("t_youyao_hospital as tyh", "tyh.yyid", "=", "sdc.hospital_yyid")
            ->whereIn('s.status', [
                RepsServeScope::STATUS_ACCESS,
                RepsServeScope::STATUS_REMOVE_APPLY,
                RepsServeScope::STATUS_REMOVE_FAILED
            ])->where("s.user_yyid", $users->yyid)
            ->whereIn('s.drug_yyid', $skuYYID)
            ->where('sdc.order_date', $orderDate)
            ->whereBetween('sdc.created_time', [$start, $end])
            ->groupBy(['sdc.drug_yyid', 'tyh.yyid'])
            ->select(['sdc.drug_yyid', 'tyh.hospital_name', Db::raw("sum(sdc.order_num) as order_num")])
            ->orderByDesc('sdc.order_num')
            ->get();
    }
}
