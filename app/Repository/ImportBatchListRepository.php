<?php
declare(strict_types=1);

namespace App\Repository;

use App\Model\Qa\ImportBatch;
use App\Model\Qa\ImportBatchList;
use Hyperf\DbConnection\Db;

class ImportBatchListRepository
{

    public static function getMaxDate(ImportBatch $batch)
    {
        return ImportBatchList::where('batch_yyid', $batch->yyid)
            ->select([Db::raw("max(transaction_date) as transaction_date")])
            ->first();
    }
}
