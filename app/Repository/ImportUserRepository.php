<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\DepartmentImport;
use App\Model\Qa\ImportUser;
use App\Model\Qa\OrgBindingUser;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;

class ImportUserRepository
{
    public static function getImportByMobile(DepartmentImport $import, $mobile): ?ImportUser
    {
        return ImportUser::query()
            ->where('import_id', $import->id)
            ->where('mobile', $mobile)
            ->first();
    }

    public static function updateImportUserAccount(DepartmentImport $import, OrgBindingUser $bindings): int
    {
        return ImportUser::query()
            ->where('import_id', $import->id)
            ->where('user_unique', $bindings->ding_user_id)
            ->where('account_uid', 0)
            ->update([
                'account_uid' => $bindings->bind_user_id,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function getUnBindByMobile(DepartmentImport $import, string $mobile_num): ?ImportUser
    {
        return ImportUser::query()
            ->where('import_id', $import->id)
            ->where('mobile', $mobile_num)
            ->where('account_uid', 0)
            ->first();
    }
}
