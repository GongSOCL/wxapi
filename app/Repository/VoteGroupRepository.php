<?php
declare(strict_types=1);
namespace App\Repository;

use App\Model\Qa\NewGroupVote;
use App\Model\Qa\NewGroupVoteDetail;

class VoteGroupRepository
{
    /**
     * @param $groupId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getByGroupId($groupId)
    {
        return NewGroupVote::where('group_id', $groupId)->orderBy('id', 'desc')->first();
    }

    /**
     * @param $voteId
     * @param $userId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function hasVoted($voteId, $userId)
    {
        return NewGroupVoteDetail::where([
            'vote_id' => $voteId,
            'user_id' => $userId
        ])->first();
    }

    /**
     * @param $groupid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getProcessingVoteByGroupYyid($groupid)
    {
        return NewGroupVote::where([
            'group_id' => $groupid,
            'status' => NewGroupVote::STATUS_PROCESSING
        ])->orderBy('id', 'desc')->first();
    }

    /**
     * @param $groupId
     * @param $userId
     * @param $fromTax
     * @param $tax
     * @return int
     */
    public static function addVote(
        $groupId,
        $userId,
        $fromTax,
        $tax
    ) {
        $data = [];
        $data['group_id'] = $groupId;
        $data['up_yyid'] = $userId;
        $data['from_tax'] = $fromTax;
        $data['to_tax'] = $tax;
        $data['start_time'] = date('Y-m-d H:i:s', time());
        $data['end_time'] = date('Y-m-d H:i:s', strtotime('+7 days'));
        $data['status'] = NewGroupVote::STATUS_PROCESSING;
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $data['updated_at'] = date('Y-m-d H:i:s', time());

        return NewGroupVote::insertGetId($data);
    }

    /**
     * @param $voteYyid
     * @return int
     */
    public static function getVoteCount($voteId)
    {
        return NewGroupVoteDetail::where('vote_id', $voteId)->count();
    }

    /**
     * @param $voteId
     * @param $status
     * @return int
     */
    public static function getVoteCountByStatus($voteId, $status)
    {
        return NewGroupVoteDetail::where([
            'vote_id' => $voteId,
            'status' => $status
        ])->count();
    }

    /**
     * @param $groupId
     * @param $voteId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getNowVote($groupId, $voteId)
    {
        return NewGroupVote::where([
            'group_id' => $groupId,
            'id' => $voteId,
            'status' => NewGroupVote::STATUS_PROCESSING
        ])->first();
    }

    /**
     * @param $groupId
     * @param $voteId
     * @param $userId
     * @param $agree
     * @return int
     */
    public static function memberVote($groupId, $voteId, $userId, $agree)
    {
        $data['group_id'] = $groupId;
        $data['vote_id'] = $voteId;
        $data['user_id'] = $userId;
        $data['status'] = $agree;
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $data['updated_at'] = date('Y-m-d H:i:s', time());

        return NewGroupVoteDetail::insertGetId($data);
    }

    /**
     * @param $voteId
     * @param $status
     */
    public static function updateVoteStatus($voteId, $status)
    {
        NewGroupVote::where('id', $voteId)->update([
            'status' => $status,
            'end_time' => date('Y-m-d H:i:s', time())
        ]);
    }

    /**
     * @param $voteDetailId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getVoteDetailById($voteDetailId)
    {
        return NewGroupVoteDetail::where('id', $voteDetailId)->first();
    }

    /**
     * @param $voteId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getVoteById($voteId)
    {
        return NewGroupVote::where('id', $voteId)->first();
    }
}
