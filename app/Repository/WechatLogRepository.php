<?php
declare(strict_types=1);
namespace App\Repository;

use App\Helper\Helper;
use App\Model\Qa\WechatLog;

class WechatLogRepository
{
    /**
     * 记录日志到数据库
     *
     * @param array $msg
     * @param $wechatType
     * @return WechatLog
     */
    public static function log(array $msg, $wechatType): WechatLog
    {
        $o = new WechatLog();
        $o->ToUserName = $msg['ToUserName'] ?? '';
        $o->FromUserName = $msg['FromUserName'] ?? '';
        $o->MsgType = $msg['MsgType'] ?? '';
        $o->MsgId = $msg['MsgId'] ?? 0;
        $o->MediaId = $msg['MediaId'] ?? '';
        $o->PicUrl = $msg['PicUrl'] ?? '';
        $o->Format = $msg['Format'] ?? '';
        $o->Recognition = $msg['Recognition'] ?? '';
        $o->ThumbMediaId = $msg['ThumbMediaId'] ?? '';
        $o->Title = $msg['Title'] ?? '';
        $o->Description = $msg['Description'] ?? '';
        $o->Url = $msg['Url'] ?? '';
        $o->Event = $msg['Event'] ?? '';
        $o->EventKey = $msg['EventKey'] ?? '';
        $o->Content = $msg['Content'] ?? '';
        $o->Ticket= $msg['Ticket'] ?? '';
        $o->wechat_type = $wechatType;
        $o->CreateTime = $msg['CreateTime'] ?? 0;
        $saveResult = $o->save();
        Helper::getLogger()->info('after_wechat_save_log', [
            'msg' => $msg,
            'save_result' => $saveResult
        ]);
        return $o;
    }
}
