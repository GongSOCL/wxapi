<?php
declare(strict_types=1);
namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\QuestionnairePaper;
use App\Model\Qa\QuestionnairePaperInvitee;
use App\Model\Qa\Users;
use Hyperf\Utils\Collection;

class QuestionnairePaperInviteeRepository
{
    /**
     * 根据问卷id获取用户关联的问卷
     *
     * @param Users $users
     * @param array $paperIds
     * @param int $userType
     * @return QuestionnairePaperInvitee[]|Collection
     */
    public static function getUserPapersByPaperIds(
        Users $users,
        array $paperIds,
        $userType = DataStatus::USER_TYPE_AGENT
    ): Collection {
        return QuestionnairePaperInvitee::where([
            'invitee_id' => $users->uid,
            'invitee_type' => $userType,
            'status' => DataStatus::REGULAR
        ])->whereIn('paper_id', $paperIds)
            ->get();
    }


    /**
     * 检查用户是否参与了开放邀请的问卷
     *
     * @param QuestionnairePaper $paper
     * @param $userID
     * @param int $userType
     * @return QuestionnairePaperInvitee|null
     */
    public static function checkUserInvited(
        QuestionnairePaper $paper,
        $userID,
        $userType = DataStatus::USER_TYPE_AGENT
    ): ?QuestionnairePaperInvitee {
        return $paper->invitee()
            ->where([
                'invitee_id' => $userID,
                'invitee_type' => $userType,
            ])->first();
    }

    /**
     * 完成一单问卷后更新邀请者统计数据
     *
     * @param QuestionnairePaperInvitee $invitee
     * @return int
     */
    public static function finishOne(QuestionnairePaperInvitee $invitee)
    {
        return QuestionnairePaperInvitee::where([
            'id' => $invitee->id,
            'status' => DataStatus::REGULAR
        ])->increment('invited', 1, [
            'modify_time' => date("Y-m-d H:i:s")
        ]);
    }

    /**
     * 添加问卷代表邀请记录
     * @param QuestionnairePaper $paper
     * @param $inviteeId
     * @param $quota
     * @param int $inviteeUserType
     * @return QuestionnairePaperInvitee
     */
    public static function addPaperInviteRecord(
        QuestionnairePaper $paper,
        $inviteeId,
        $quota,
        $inviteeUserType = DataStatus::USER_TYPE_AGENT
    ): QuestionnairePaperInvitee {
        $o = new QuestionnairePaperInvitee();
        $o->paper_id = $paper->id;
        $o->invitee_id = $inviteeId;
        $o->invitee_type = $inviteeUserType;
        $o->invite_quota = $quota;
        $o->invited = 0;
        $o->status = DataStatus::REGULAR;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');
        $o->save();

        return $o;
    }

    /**
     * 获取问卷已经分配的配额
     * @param QuestionnairePaper $paper
     * @return int
     */
    public static function getPaperUsedQuote(QuestionnairePaper $paper): int
    {
        $res = $paper->invitee()->sum('invite_quota');
        $res = is_numeric($res) ? intval($res) : $res;

        return $res;
    }
}
