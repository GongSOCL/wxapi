<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\SystemDepart;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\Utils\Collection;

class SystemDepartRepository
{
    /**
     * @param string $keywords
     * @return Collection
     */
    public static function searchSystemDeparts($keywords = ""): Collection
    {
        $query = SystemDepart::where([
            'pid' => 0,
            'status' => DataStatus::REGULAR
        ]);
        if ($keywords) {
            $query = $query->where('name', 'like', "%{$keywords}%");
        }

        return $query->orderBy('id')
            ->get();
    }

    /**
     * @param string $keywords
     */
    public static function searchSystemDepartsWithPage(
        $keywords = "",
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        $query = SystemDepart::where([
            'pid' => 0,
            'status' => DataStatus::REGULAR
        ]);
        if ($keywords) {
            $query = $query->where('name', 'like', "%{$keywords}%");
        }

        return $query->orderBy('id')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function getByIds($visitDepartIds): Collection
    {
        return SystemDepart::whereIn('id', $visitDepartIds)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function getById($departId): ?SystemDepart
    {
        return SystemDepart::where('id', $departId)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }

    /**
     * @param $departIds
     * @return Collection
     */
    public static function getVisitDepartsInfo($departIds)
    {
        return SystemDepart::select(['id', 'name'])
            ->whereIn('id', $departIds)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function getDepartMapByIds(array $ids): array
    {
        $depart = self::getByIds($ids);
        return $depart->keyBy('id')->all();
    }
}
