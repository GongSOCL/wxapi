<?php


namespace App\Repository;

use App\Helper\Helper;
use App\Model\Qa\RefuseRemark;
use App\Model\Qa\Users;

class RefuseRemarkRepository
{
    public static function getUserRefuseMsg(Users $users, $type = 1): ?RefuseRemark
    {
        return RefuseRemark::where('resource_yyid', $users->yyid)
            ->where('type', $type)
            ->orderByDesc("id")
            ->first();
    }

    public static function addVerifyRefuse(Users $user, $reason, $adminId): RefuseRemark
    {
        $o = new RefuseRemark();
        $o->yyid = Helper::yyidGenerator();
        $o->resource_yyid =$user->yyid;
        $o->content = $reason;
        $o->admin_id = $adminId;
        $o->type = 1;
        $o->status = 1;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');

        $o->save();
        return $o;
    }
}
