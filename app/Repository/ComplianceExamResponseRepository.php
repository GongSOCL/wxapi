<?php


namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ExamCode;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ComplianceExamResponse;
use App\Model\Qa\Users;
use Hyperf\Utils\Collection;

class ComplianceExamResponseRepository
{

    public static function getExamResponse(ComplianceExam $exam, Users $user): Collection
    {
        return ComplianceExamResponse::where('exam_yyid', $exam->yyid)
            ->where('uid', $user->uid)
            ->where('platform', ExamCode::YOUYAO)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }
}
