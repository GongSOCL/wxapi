<?php
declare(strict_types=1);


namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\DealerHospitalC;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;

class DealerHospitalCRepository
{
    public static function getUserCompanyHospital(Users $users, YouyaoHospital $hospital): ?DealerHospitalC
    {
        return DealerHospitalC::where([
            'company_yyid' => $users->company_yyid,
            'hospital_yyid' => $hospital->yyid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $companyYyid
     * @param $hospitalNo
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getHospitalInfo($companyYyid, $hospitalNo)
    {
        return DealerHospitalC::where([
            'company_yyid' => $companyYyid,
            'hospital_no' => $hospitalNo,
            ])
            ->orderBy('id', 'desc')
            ->first();
    }
}
