<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Repository;

use App\Constants\DataStatus;
use App\Helper\Helper;
use App\Model\Qa\ComplianceAgreement;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ComplianceUserAgreement;
use App\Model\Qa\Users;
use Carbon\Carbon;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Model;

class ComplianceUserAgreementRepository
{
    /**
     * 检查用户是否已经签署过协议
     * @param Users $user
     * @param ComplianceExam $exam
     * @return bool
     */
    public static function checkUserExamAgreed(Users $user, ComplianceExam $exam): bool
    {
        return ComplianceUserAgreement::where([
            'user_yyid' => $user->yyid,
            'exam_yyid' => $exam->agreement_yyid,
            'status' => DataStatus::REGULAR
        ])->exists();
    }

    public static function addExamAgree(
        ComplianceExam $exam,
        Users $user,
        ComplianceAgreement $agreement,
        Carbon $time,
        $pdfUrl,
        $yyid = ''
    ): ComplianceUserAgreement {
        $o = new ComplianceUserAgreement();
        $o->yyid = $yyid ?: Helper::yyidGenerator();
        $o->exam_yyid = $exam->yyid;
        $o->agreement_yyid = $agreement->yyid;
        $o->user_yyid = $user->yyid;
        $o->agree_pdf = $pdfUrl;
        $o->status = DataStatus::REGULAR;
        $o->created_time = $o->modify_time = $o->agree_time = $time->format('Y-m-d H:i:s');
        $o->save();

        $o->refresh();
        return $o;
    }

    /**
     * @return null|Builder|ComplianceUserAgreement|Model|object
     */
    public function getUserAgree(string $userYyid, string $agreementYyid)
    {
        return ComplianceUserAgreement::query()->where('user_yyid', $userYyid)
            ->where('agreement_yyid', $agreementYyid)
            ->where('status', 1)
            ->first();
    }

    /**
     * @param ComplianceUserAgreement $agreement
     * @return int
     */
    public function addUserAgree(ComplianceUserAgreement $agreement)
    {
        return ComplianceUserAgreement::query()->insertGetId($agreement->getAttributes());
    }

    /**
     * 获取用户最新考试记录
     * @param ComplianceExam $exam
     * @param Users $user
     * @return ComplianceUserAgreement|null
     */
    public static function getUserExamAgreement(ComplianceExam $exam, Users $user): ?ComplianceUserAgreement
    {
        return ComplianceUserAgreement::where([
            'user_yyid' => $user->yyid,
            'exam_yyid' => $exam->yyid,
            'status' => DataStatus::REGULAR
        ])->orderByDesc('id')
            ->first();
    }
}
