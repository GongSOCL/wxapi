<?php


namespace App\Repository;

use App\Helper\Helper;
use App\Model\Qa\ResourceNum;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class ResourceNumRepository
{
    public static function getReadNums(array $resourceYYIDS): Collection
    {
        return ResourceNum::whereIn('resource_yyid', $resourceYYIDS)
            ->selectRaw("resource_yyid, sum(read_num) as read_num")
            ->groupBy("resource_yyid")
            ->get();
    }

    public static function getReadNum(string $yyid)
    {
        return ResourceNum::where('resource_yyid', $yyid)
            ->sum('read_num');
    }

    public static function findByResourceYYID($yyid): ?ResourceNum
    {
        return ResourceNum::where('resource_yyid', $yyid)
            ->first();
    }

    public static function addResourceRead(
        $yyid,
        $readNum = 1,
        $commentNum = 0,
        $type = ResourceNum::TYPE_INTERPRET
    ): ResourceNum {
        $o = new ResourceNum();
        $o->yyid = Helper::yyidGenerator();
        $o->resource_yyid = $yyid;
        $o->type = $type;
        $o->read_num = $readNum;
        $o->share_num = 0;
        $o->share_click_num = 0;
        $o->fav_num = 0;
        $o->comment_num = $commentNum;
        $o->save();

        return $o;
    }

    /**
     * @param $yyid
     * @param int $type
     * @param int $step
     * @return bool|int
     */
    public static function incrReadNum($yyid, $type = ResourceNum::TYPE_INTERPRET, $step = 1)
    {
        return ResourceNum::where([
            'resource_yyid' => $yyid,
            'type' => $type
        ])->update([
            'read_num' => Db::raw("read_num + {$step}")
        ]);
    }
}
