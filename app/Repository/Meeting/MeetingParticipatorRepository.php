<?php
declare(strict_types=1);
namespace App\Repository\Meeting;

use App\Constants\DataStatus;
use App\Model\Qa\MeetingApply;
use App\Model\Qa\MeetingParticipator;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class MeetingParticipatorRepository
{

    public static function addUser(
        MeetingApply $apply,
        $name,
        $hospitalId,
        $departId,
        $levelId,
        $type = MeetingParticipator::TYPE_CHAIRMAN
    ): MeetingParticipator {
        $o = new MeetingParticipator();
        $o->meeting_id = $apply->id;
        $o->name = $name;
        $o->hospital_id = $hospitalId;
        $o->depart_id = $departId;
        $o->level_id = $levelId;
        $o->status = DataStatus::REGULAR;
        $o->created_at =$o->updated_at = Carbon::now();
        $o->user_type = $type;
        $o->save();
        $o->refresh();

        return $o;
    }

    public static function batchAdd(MeetingApply $apply, array $users, $type = MeetingParticipator::TYPE_SPEAKER): bool
    {
        $data = [];
        $now = date('Y-m-d H:i:s');
        foreach ($users as $user) {
            $data[] = [
                'meeting_id' => $apply->id,
                'name' => $user['name'],
                'hospital_id' => $user['hospital_id'],
                'depart_id' => $user['depart_id'],
                'level_id' => $user['level_id'],
                'project' => $user['project'],
                'status' => DataStatus::REGULAR,
                'created_at' => $now,
                'updated_at' => $now,
                'user_type' => $type
            ];
        }

        return MeetingParticipator::insert($data);
    }

    public static function getApplyParticipator(MeetingApply $apply, $type = 0): Collection
    {
        return MeetingParticipator::query()
            ->where('meeting_id', $apply->id)
            ->when($type > 0, function ($query) use ($type) {
                return $query->where('user_type', $type);
            })->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function deleteByIds($delSpIds): int
    {
        return MeetingParticipator::query()
            ->whereIn('id', $delSpIds)
           ->where('status', DataStatus::REGULAR)
            ->update([
                'status' => DataStatus::DELETE,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function getById(int $id): ?MeetingParticipator
    {
        return  MeetingParticipator::query()
        ->where('id', $id)
        ->where('status', DataStatus::REGULAR)
        ->first();
    }

    public static function updateParticipator(
        MeetingParticipator $user,
        $name,
        $hospital_id,
        $depart_id,
        $level_id
    ): MeetingParticipator {
        $user->name = $name;
        $user->hospital_id = $hospital_id;
        $user->depart_id = $depart_id;
        $user->level_id = $level_id;
        $user->updated_at = Carbon::now();
        $user->save();

        return $user;
    }

    public static function getCountMapByApplyIds(array $meetingIds)
    {
        $resp = MeetingParticipator::query()
            ->whereIn('meeting_id', $meetingIds)
            ->where('status', DataStatus::REGULAR)
            ->select(['meeting_id', Db::raw('count(*) as cnt')])
            ->groupBy(['meeting_id'])
            ->get();
        return $resp->reduce(function ($carry, $item) {
            $carry[$item->meeting_id] = $item->cnt;
            return $carry;
        }, []);
    }
}
