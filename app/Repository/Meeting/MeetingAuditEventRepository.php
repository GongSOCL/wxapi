<?php
declare(strict_types=1);
namespace App\Repository\Meeting;

use App\Model\Qa\MeetingApply;
use App\Model\Qa\MeetingAuditEvent;
use Carbon\Carbon;

class MeetingAuditEventRepository
{

    public static function addApplyEvent(
        MeetingApply $apply,
        $eventType,
        $uid = 0,
        $platform = MeetingAuditEvent::PLATFORM_WXAPI
    ): MeetingAuditEvent {
        $o = new MeetingAuditEvent();
        $o->meeting_id = $apply->id;
        $o->user_id = $uid;
        $o->platform = $platform;
        $o->event_type = $eventType;
        $o->created_at = $o->updated_at = Carbon::now();
        $o->save();

        return $o;
    }
}
