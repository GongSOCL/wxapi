<?php
declare(strict_types=1);
namespace App\Repository\Meeting;

use App\Constants\DataStatus;
use App\Model\Qa\MeetingApply;
use App\Model\Qa\MeetingParticipator;
use App\Model\Qa\MeetingTimeline;
use App\Model\Qa\Users;
use Carbon\Carbon;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\DbConnection\Db;

class MeetingApplyRepository
{

    public static function listApply(
        $zoneId = 0,
        $uid = 0,
        $status = 0,
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        return MeetingApply::query()
            ->where('status', DataStatus::REGULAR)
            ->when($status > 0, function ($query) use ($status) {
                $query->where('state', $status);
            })->when($zoneId > 0, function ($query) use ($zoneId) {
                $query->where('zone_id', $zoneId);
            })->when($uid > 0, function ($query) use ($uid) {
                $query->where('applicant_id', $uid);
            })->orderByDesc('id')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function addApply(
        Users $users,
        $mobile,
        $name,
        $managerId,
        $compereId,
        $meetingType,
        $subType,
        $categoryId,
        $zoneId,
        $provinceId,
        $hospitalId,
        $departId,
        $productId,
        $rangeType,
        $chairmanId = 0,
        $minTime = ""
    ): MeetingApply {
        $o = new MeetingApply();
        $o->name = $name;
        $o->applicant_id =$users->uid;
        $o->applicant_mobile  = $mobile;
        $o->manager_id = $managerId;
        $o->compere_id =$compereId;
        $o->meeting_type = $meetingType;
        $o->meeting_sub_type = $subType;
        $o->meeting_category = $categoryId;
        $o->range_type = $rangeType;
        $o->state = MeetingApply::STATE_NEW;
        $o->zone_id = $zoneId;
        $o->province_id = $provinceId;
        $o->hospital_id = $hospitalId;
        $o->depart_id = $departId;
        $o->series_id = $productId;
        $o->chairman_id  = $chairmanId;
        $o->reject_reason = "";
        $o->status = DataStatus::REGULAR;
        $o->created_at = $o->updated_at = Carbon::now();
        if ($minTime) {
            $o->start = $minTime;
        }
        $o->save();
        $o->refresh();

        return $o;
    }

    public static function setChairman(MeetingApply $apply, MeetingParticipator $chairman = null): int
    {
        return MeetingApply::query()
            ->where('id', $apply->id)
            ->update([
                'chairman_id' => $chairman ?  $chairman->id : 0,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function getById($id): ?MeetingApply
    {
        return MeetingApply::query()
            ->where('id', $id)
            ->where('status', DataStatus::REGULAR)
            ->first();
    }

    public static function updateApplyBase(
        MeetingApply $apply,
        $name,
        $mobile,
        $managerId,
        $compereId,
        $meetingType,
        $meetingSubType,
        $categoryId,
        $zoneId,
        $provinceId,
        $hospitalId,
        $departId,
        $productId,
        $rangeType,
        $chairmanId = 0,
        $state = MeetingApply::STATE_NEW,
        $minTime = ""
    ): MeetingApply {
        $apply->name = $name;
        $apply->applicant_mobile = $mobile;
        $apply->meeting_type = $meetingType;
        $apply->meeting_sub_type = $meetingSubType;
        $apply->meeting_category = $categoryId;
        $apply->series_id = $productId;
        $apply->manager_id = $managerId;
        $apply->compere_id = $compereId;
        $apply->range_type = $rangeType;
        $apply->zone_id =$zoneId;
        $apply->province_id = $provinceId;
        $apply->hospital_id = $hospitalId;
        $apply->depart_id = $departId;
        $apply->chairman_id = $chairmanId;
        $apply->updated_at = Carbon::now();
        $apply->state = $state;
        if ($minTime) {
            $apply->start = $minTime;
        }
        $apply->save();

        return $apply;
    }

    public static function listWithTimeLimit(
        $applicantId = 0,
        $zoneId = 0,
        $status = 0,
        $applyStart = "",
        $applyEnd = "",
        $meetingStart = "",
        $meetingEnd = "",
        $current = 1,
        $limit = 10,
        $sortStartOrder = 0
    ): LengthAwarePaginatorInterface {
        $sb = MeetingApply::query()
            ->join("t_meeting_timeline as tmt", 'tmt.meeting_id', '=', 't_meeting_apply.id')
            ->when($applicantId > 0, function ($query) use ($applicantId) {
                   $query->where('t_meeting_apply.applicant_id', $applicantId);
            })->when($zoneId > 0, function ($query) use ($zoneId) {
                $query->where('t_meeting_apply.zone_id', $zoneId);
            })->when($status > 0, function ($query) use ($status) {
                $query->where('t_meeting_apply.state', $status);
            })->when($applyStart != "" || $applyEnd != "", function ($query) use ($applyStart, $applyEnd) {
                if ($applyStart != "") {
                    $query->where('t_meeting_apply.created_at', '>=', $applyStart);
                }
                if ($applyEnd != "") {
                    $query->where('t_meeting_apply.created_at', '<', $applyEnd);
                }
            })->when($meetingStart != "" || $meetingEnd != "", function ($query) use ($meetingStart, $meetingEnd) {
                if ($meetingStart != "") {
                    $query->where("tmt.start", ">=", $meetingStart);
                }
                if ($meetingEnd != "") {
                    $query->where('tmt.start', "<", $meetingEnd);
                }
            })->where('tmt.status', DataStatus::REGULAR)
            ->where('t_meeting_apply.status', DataStatus::REGULAR)
            ->select([Db::raw("distinct(t_meeting_apply.id) as sid")]);
        $query = MeetingApply::query()
            ->joinSub($sb, 'sb', 'sb.sid', 'id');
        if ($sortStartOrder == 1 || $sortStartOrder == -1) {
            $query->orderBy('start', $sortStartOrder == 1 ? 'asc' : 'desc');
        }
        return $query->orderByDesc('t_meeting_apply.id')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function listApplyWithCond(
        $applicantId = 0,
        $zoneId = 0,
        $status = 0,
        $applyStart = "",
        $applyEnd = "",
        $current = 1,
        $limit = 10,
        $sortStartOrder = 0
    ): LengthAwarePaginatorInterface {
        $query = MeetingApply::query()
            ->when($applicantId > 0, function ($query) use ($applicantId) {
                $query->where('applicant_id', $applicantId);
            })->when($zoneId > 0, function ($query) use ($zoneId) {
                $query->where('zone_id', $zoneId);
            })->when($status > 0, function ($query) use ($status) {
                $query->where('state', $status);
            })->when($applyStart != "" || $applyEnd != "", function ($query) use ($applyStart, $applyEnd) {
                if ($applyStart != "") {
                    $query->where('created_at', '>=', $applyStart);
                }
                if ($applyEnd != "") {
                    $query->where('created_at', '<', $applyEnd);
                }
            })->where('t_meeting_apply.status', DataStatus::REGULAR);
        if ($sortStartOrder == 1 || $sortStartOrder == -1) {
            $query->orderBy('start', $sortStartOrder == 1 ? 'asc' : 'desc');
        }
        return $query->orderByDesc('id')
            ->paginate($limit, ['*'], '', $current);
    }

    public static function auditSuccess(MeetingApply $apply): int
    {
        return MeetingApply::query()
            ->where('id', $apply->id)
            ->where('status', DataStatus::REGULAR)
            ->where('state', MeetingApply::STATE_NEW)
            ->update([
                'state' => MeetingApply::STATE_SUCCESS,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function auditFail(MeetingApply $apply, string $reason): int
    {
        return MeetingApply::query()
            ->where('id', $apply->id)
            ->where('status', DataStatus::REGULAR)
            ->where('state', MeetingApply::STATE_NEW)
            ->update([
                'reject_reason' => $reason,
                'state' => MeetingApply::STATE_FAIL,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function cancelApply(MeetingApply $apply)
    {
        return MeetingApply::where('id', $apply->id)
            ->where('status', DataStatus::REGULAR)
            ->where('state', '!=', MeetingApply::STATE_CANCEL)
            ->update([
                'state' => MeetingApply::STATE_CANCEL,
                'updated_at' => Carbon::now()
            ]);
    }
}
