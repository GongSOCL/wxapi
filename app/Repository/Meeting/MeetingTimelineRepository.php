<?php

namespace App\Repository\Meeting;

use App\Constants\DataStatus;
use App\Model\Qa\MeetingApply;
use App\Model\Qa\MeetingTimeline;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;

class MeetingTimelineRepository
{

    public static function getMultiMeetingStartEnd(array $meetingIds): Collection
    {
        return MeetingTimeline::query()
            ->whereIn('meeting_id', $meetingIds)
            ->where('status', DataStatus::REGULAR)
            ->select([
                'meeting_id',
                Db::raw('min(start) as start'),
                Db::raw('max(end) as end')
            ])
            ->groupBy(['meeting_id'])
            ->get();
    }

    public static function batchAdd(MeetingApply $apply, array $time): bool
    {
        $data = [];

        $now = date('Y-m-d H:i:s');
        foreach ($time as $t) {
            $data[] = [
                'meeting_id' => $apply->id,
                'start' => $t['start'],
                'end' => $t['end'],
                'meeting_type' => $apply->meeting_category,
                'status' => DataStatus::REGULAR,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        return MeetingTimeline::insert($data);
    }

    public static function getApplyTimes(MeetingApply $apply): Collection
    {
        return MeetingTimeline::query()
            ->where('meeting_id', $apply->id)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function deleteByIds(array $delTimeIds): int
    {
        return MeetingTimeline::query()
            ->whereIn('meeting_id', $delTimeIds)
            ->where('status', DataStatus::REGULAR)
            ->update([
                'status' => DataStatus::DELETE,
                'updated_at' => Carbon::now()
            ]);
    }

    public static function getBetween($type, $min, $max): Collection
    {
        $s1 = MeetingTimeline::query()
            ->where('meeting_type', $type)
            ->where('status', DataStatus::REGULAR)
            ->where('start', '<=', $min)
            ->where('end', '>=', $min);
        $s2 = MeetingTimeline::query()
            ->where('meeting_type', $type)
            ->where('status', DataStatus::REGULAR)
            ->where('start', '<=', $max)
            ->where('end', '>=', $max);
        $s3 = MeetingTimeline::query()
            ->where('meeting_type', $type)
            ->where('status', DataStatus::REGULAR)
            ->where('start', '>=', $min)
            ->where('end', '<=', $max);
        $s4 = MeetingTimeline::query()
            ->where('meeting_type', $type)
            ->where('status', DataStatus::REGULAR)
            ->where('start', '<=', $min)
            ->where('end', '>=', $max);
        return $s1->union($s2)
            ->union($s3)
            ->union($s4)
            ->get();
    }

    public static function getApplyMinStart(MeetingApply $apply)
    {
        return MeetingTimeline::query()
            ->where('meeting_id', $apply->id)
            ->where('status', DataStatus::REGULAR)
            ->select([Db::raw('min(start) as start')])
            ->first();
    }

    public static function updateApplyCategory(MeetingApply $apply): int
    {
        return MeetingTimeline::query()
            ->where('meeting_id', $apply->id)
            ->where('status', DataStatus::REGULAR)
            ->update([
               'meeting_type' => $apply->meeting_category,
                'updated_at' => Carbon::now()
            ]);
    }
}
