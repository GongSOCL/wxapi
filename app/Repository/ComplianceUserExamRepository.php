<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Repository;

use App\Constants\DataStatus;
use App\Constants\ExamCode;
use App\Helper\Helper;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ComplianceUserExam;
use App\Model\Qa\Users;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Collection;
use InvalidArgumentException;
use const OpenTracing\Tags\DATABASE_STATEMENT;

class ComplianceUserExamRepository
{
    public static function getUserExamLastRecord(Users $users, array $examYYIDS): Collection
    {
        $sub = ComplianceUserExam::where('uid', $users->uid)
            ->where('platform', ExamCode::YOUYAO)
            ->whereIn('exam_yyid', $examYYIDS)
            ->groupBy('exam_yyid')
            ->select(['exam_yyid', Db::raw("max(id) as mid")]);

        return ComplianceUserExam::query()
        ->joinSub($sub, "sb", "id", "=", "sb.mid")
        ->select(['t_compliance_user_exam.*'])
        ->get();
    }

    /**
     * 获取用户考试参与最终记录
     * @param Users $users
     * @param ComplianceExam $exam
     * @return ComplianceUserExam|null
     */
    public static function getExamLast(Users $users, ComplianceExam $exam): ?ComplianceUserExam
    {
        return ComplianceUserExam::where('platform', ExamCode::YOUYAO)
        ->where('exam_yyid', $exam->yyid)
        ->where('uid', $users->uid)->orderByDesc('id')
            ->limit(1)
            ->first();
    }

    public static function clearUserExams(ComplianceExam $exam, Users $users)
    {
        ComplianceUserExam::where([
            ['platform', ExamCode::YOUYAO],
            ['uid', $users->uid],
            ['exam_yyid', $exam->yyid]
        ])->update([
            'status' => DataStatus::DELETE,
            'modify_time' => date('Y-m-d H:i:s')
        ]);
    }

    public static function addUserExam(
        ComplianceExam $exam,
        Users $users,
        bool $isPassed = true,
        $openId = ''
    ): ComplianceUserExam {
        $now = date('Y-m-d H:i:s');
        $o = new ComplianceUserExam;
        $o->exam_yyid = $exam->yyid;
        $o->uid = $users->uid;
        $o->platform = ExamCode::YOUYAO;
        $o->openid = $openId;
        $o->yyid = Helper::yyidGenerator();
        $o->status = $isPassed ? DataStatus::REGULAR : DataStatus::DELETE;
        $o->pass_time = $now;
        $o->created_time = $o->modify_time = $now;
        $o->save();

        return $o;
    }


    /**
     * @return null|ComplianceUserExam|\Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object
     */
    public function getPassUserExam(string $examYyid, int $uid)
    {
        return ComplianceUserExam::query()->where('exam_yyid', $examYyid)
            ->where('uid', $uid)->where('status', 1)->where('platform', ExamCode::YOUYAO)
            ->orderBy('id', 'desc')->first();
    }


    /**
     * 过滤出用户参与过的考试
     * @param Users $users
     * @param bool $isPased
     * @return Collection
     * @throws InvalidArgumentException
     */
    public static function filterUserPartedExams(Users $users, $isPased = false): Collection
    {
        $query = ComplianceUserExam::query()
            ->where([
                'uid' => $users->uid,
                'platform', ExamCode::YOUYAO
            ]);
        if ($isPased) {
            $query =$query->where('status', 1);
        }

        return $query->select([Db::raw("distinct exam_yyid")])
            ->get();
    }

    /**
     * 根据状态获取用户参与过的问卷
     * @param Users $users
     * @param bool $isPassed
     * @return Collection
     */
    public static function filterExamsByLastStatus(Users $users, bool $isPassed = true): Collection
    {
        $sub  = ComplianceUserExam::query()
            ->where('uid', $users->uid)
            ->where('platform', ExamCode::YOUYAO)
            ->select(['exam_yyid', Db::raw('max(id) as mid')])
            ->groupBy(['exam_yyid']);

        $query = ComplianceUserExam::query()
            ->joinSub($sub, "cue", "id", '=', "cue.mid");
        if ($isPassed) {
            $query = $query->where('status', DataStatus::REGULAR);
        } else {
            $query = $query->where('status', DataStatus::DELETE);
        }

        return $query->get();
    }
}
