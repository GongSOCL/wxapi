<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\NewAgentInvite;
use App\Model\Qa\Users;

class NewAgentInviteRepository
{

    public static function getUserInviteRelations(
        Users $agent,
        $unionId,
        string $openId = "",
        $userType = DataStatus::USER_TYPE_DOCTOR
    ): ?NewAgentInvite {
        $query = NewAgentInvite::query()
            ->where('inviter_id', $agent->uid)
            ->where('user_unionid', $unionId)
            ->where('user_type', $userType);
        if ($openId) {
            $query = $query->where('user_openid', $openId);
        }

        return $query->first();
    }
}
