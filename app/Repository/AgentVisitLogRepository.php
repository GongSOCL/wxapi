<?php

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\AgentVisit;
use App\Model\Qa\AgentVisitLog;

class AgentVisitLogRepository
{
    public static function addLog(
        $visitId,
        $userId,
        $ua,
        $ip,
        $operateType,
        $latitude,
        $longitude,
        $deviceType
    ) {
        $o = new AgentVisitLog();
        $o->visit_id = $visitId;
        $o->user_id = $userId;
        $o->ua = $ua;
        $o->ip = $ip;
        $o->operate_type = $operateType;
        $o->amap_latitude = $latitude;
        $o->amap_longitude = $longitude;
        $o->device_type = $deviceType;
        $o->status = DataStatus::REGULAR;
        $o->created_time = $o->modify_time = date('Y-m-d H:i:s');
        $o->save();
    }

    public static function clearLog(string $end)
    {
        AgentVisitLog::query()
            ->where('created_time', '<=', $end)
            ->delete();
    }
}
