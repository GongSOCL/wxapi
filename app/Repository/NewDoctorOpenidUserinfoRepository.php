<?php
declare(strict_types=1);

namespace App\Repository;

use App\Constants\DataStatus;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Qa\NewDoctorUserinfo;
use App\Model\Qa\NewInviteQrcode;
use App\Model\Qa\Users;
use Hyperf\Contract\LengthAwarePaginatorInterface;
use Hyperf\Utils\Collection;

class NewDoctorOpenidUserinfoRepository
{
    public static function searchAgentDoctorWithPage(
        Users $agent,
        $keywords = "",
        $current = 1,
        $limit = 10
    ): LengthAwarePaginatorInterface {
        $query = NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where([
                'user_id' => $agent->uid,
                'status' => DataStatus::REGULAR
            ]);
        if ($keywords) {
            $query = $query->whereRaw("(nickname like '%{$keywords}%' or true_name like '%{$keywords}%')");
        }

        return $query->paginate($limit, ['*'], '', $current);
    }

    public static function getRepsInfo($id)
    {
        return NewDoctorOpenidUserinfo::where([
            'id' => $id,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    public static function getInfoById($id): ?NewDoctorUserinfo
    {
        return NewDoctorUserinfo::where('nid', $id)->first();
    }

    public static function getDoctorInfo(Users $agent, string $doctorOpenid): ?NewDoctorUserinfo
    {
        $resp = NewDoctorOpenidUserinfo::where([
            'user_id' => $agent->uid,
            'status' => DataStatus::REGULAR
        ])->get();

        if ($resp->isNotEmpty()) {
            $nids = array_column($resp->toArray(), 'new_doctor_id');
        } else {
            $nids = [ -1 ];
        }

        return NewDoctorUserinfo::whereIn('nid', $nids)
            ->where('openid', $doctorOpenid)->first();
    }

    public static function getAgentDoctors(Users $agent, $hospitalYYID = "", $departId = 0, $keywords = "", $page = 1, $pageSize = 10): Collection
    {
        $where = [
            "user_id" => $agent->uid,
            "status" => DataStatus::REGULAR
        ];
        if ($hospitalYYID) {
            $where['hospital_yyid'] = $hospitalYYID;
        }
        if ($departId) {
            $where['depart_id'] = $departId;
        }

        $query = NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where($where);
        if ($keywords) {
            $query = $query->whereRaw("(true_name like '%{$keywords}%' or nickname like '%{$keywords}%')");
        }
        return $query->latest('ndou.created_at')->offset($pageSize*($page-1))->limit($pageSize)->get();
    }

    public static function getAgentDoctorsCount(Users $agent, $hospitalYYID = "", $departId = 0, $keywords = "")
    {
        $where = [
            "user_id" => $agent->uid,
            "status" => DataStatus::REGULAR
        ];
        if ($hospitalYYID) {
            $where['hospital_yyid'] = $hospitalYYID;
        }
        if ($departId) {
            $where['depart_id'] = $departId;
        }

        $query = NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where($where);
        if ($keywords) {
            $query = $query->whereRaw("(true_name like '%{$keywords}%' or nickname like '%{$keywords}%')");
        }
        return $query->count();
    }

    public static function getInfoByIds(array $ids): Collection
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->whereIn('id', $ids)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function checkUserOwnDoctor($userId, $doctorId): bool
    {
        return NewDoctorOpenidUserinfo::query()
            ->where('user_id', $userId)
            ->where('new_doctor_id', $doctorId)
            ->where('status', DataStatus::REGULAR)
            ->exists();
    }

    public static function getRelatedToDoctor(NewDoctorUserinfo $doctor): Collection
    {
        return NewDoctorOpenidUserinfo::query()
            ->where('new_doctor_id', $doctor->nid)
            ->where('status', DataStatus::REGULAR)
            ->get();
    }

    public static function getAgentDoctorsWithPage(
        Users $users,
        $current = 1,
        $limit = 10,
        $showOnlyWithWechat = false
    ): LengthAwarePaginatorInterface {
        return NewDoctorOpenidUserinfo::query()
            ->join('new_doctor_userinfo as ndu', 'ndu.nid', '=', 'new_doctor_openid_userinfo.new_doctor_id')
            ->where('new_doctor_openid_userinfo.user_id', $users->uid)
            ->where('new_doctor_openid_userinfo.status', 1)
            ->when($showOnlyWithWechat, function ($query) {
                $query->where('ndu.unionid', '!=', '');
            })->paginate($limit, ['ndu.*'], '', $current);
    }

    public static function getDoctorDeparts($userId, $hospitalYyid)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->select(['depart_id', 'depart_name'])
            ->where('user_id', $userId)
            ->where('hospital_yyid', $hospitalYyid)
            ->where('status', DataStatus::REGULAR)
            ->groupBy(['depart_id'])
            ->latest('ndu.created_at')
            ->get();
    }

    public static function getDoctorsById($doctorId)
    {
        return NewDoctorOpenidUserinfo::from('new_doctor_openid_userinfo as ndou')
            ->join('new_doctor_userinfo as ndu', 'ndou.new_doctor_id', '=', 'ndu.nid')
            ->where('id', $doctorId)
            ->first();
    }

    /**
     * @param $aid
     * @param $userId
     * @param $nid
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public static function getQrcodeRecode($aid, $userId, $nid)
    {
        return NewInviteQrcode::where([
            'activity_id' => $aid,
            'user_id' => $userId,
            'nid' => $nid,
            'status' => DataStatus::REGULAR
        ])->first();
    }

    /**
     * @param $aid
     * @param $userId
     * @param $nid
     * @param $key
     * @return int
     */
    public static function createQrcodeRecode($aid, $userId, $nid, $key)
    {
        return NewInviteQrcode::insertGetId([
            'activity_id' => $aid,
            'event_key' => $key,
            'user_id' => $userId,
            'nid' => $nid,
            'created_at' => date('Y-m-d H:i:s', time()),
        ]);
    }

    /**
     * @param $userId
     * @param $nid
     * @return int
     */
    public static function updateQrcodeRecode($userId, $nid)
    {
        return NewInviteQrcode::where([
            'user_id' => $userId,
            'nid' => $nid
        ])->update([
            'status' => DataStatus::DELETE
        ]);
    }
}
