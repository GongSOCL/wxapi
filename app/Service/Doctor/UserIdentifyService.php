<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Doctor;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Constants\Code\ErrorCodeDoctor;
use App\Controller\Upload\UploadController;
use App\Exception\BusinessException;
use App\Exception\DoctorException;
use App\Helper\Helper;
use App\Repository\UserIdentifyRepository;
use App\Repository\UsersRepository;
use App\Service\ALiYun\OCRAPIService;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

class UserIdentifyService
{

    /**
     * @Inject()
     * @var UploadController
     */
    private $upload;

    /**
     * @Inject()
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @Inject()
     * @var UserIdentifyRepository
     */
    private $userIdentityRepository;

    /**
     * @Inject()
     * @var OCRAPIService
     */
    private $OCRAPIService;

    /**
     * @param $yyid
     * @param $file
     * @return bool|string
     */
    public function uploadIdcard($yyid, $file)
    {
        $userInfo = UsersRepository::getUserByYYID($yyid);
        $fileName = $this->upload->upload(DataStatus::UPLOAD_URI_IDCARD, $file, $userInfo->uid);
        if (!$fileName) {
            throw new BusinessException(ErrorCode::UPLOAD_ERROR);
        }

        $res = $this->OCRAPIService::main(DataStatus::UPLOAD_URI_IDCARD.$userInfo->uid.'/'.$fileName, $fileName);

        return ['file_name'=>$fileName,'user_info'=>$res];
    }

    /**
     * @param $yyid
     * @param $front
     * @param $back
     * @return array
     */
    public function idcardSubmit(
        $front,
        $back,
        $name,
        $sex,
        $ethnicity,
        $birthDate,
        $address,
        $idNumber,
        $issueAuthority,
        $validPeriod,
        $mobile,
        $is_upload_idcard
    ) {
        if ($is_upload_idcard  != 2) {
            //判断是否有数据
            if (!$front||!$back) {
                throw new DoctorException(ErrorCodeDoctor::DOCTOR_UPLOAD_INFO_MISS);
            }
            $yyid   = Helper::getLoginUser()->yyid;

            $info = UsersRepository::getUserByYYID($yyid);

            if (!$info) {
                throw new DoctorException(ErrorCodeDoctor::DOCTOR_USER_NOT_EXISTS);
            }
            if ($info->is_upload_idcard==1) {
                throw new DoctorException(ErrorCodeDoctor::DOCTOR_IDCARD_ALREADY_UPLOADED);
            }
        }


        Db::beginTransaction();
        try {
            if ($is_upload_idcard  != 2) {
                //修改t_users表is_upload_idcard字段为1
                $this->usersRepository->changeIsUploadIdcard($yyid);
            }
            //向t_user_identity表插入一条记录
            $this->userIdentityRepository->create(
                $front,
                $back,
                $name,
                $sex,
                $ethnicity,
                $birthDate,
                $address,
                $idNumber,
                $issueAuthority,
                $validPeriod,
                $mobile,
                $info
            );

            Db::commit();
            return ['yyid' => $yyid];
        } catch (BusinessException $exception) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::UPLOAD_ERROR);
        }
    }
}
