<?php


namespace App\Service\Doctor;

use Grpc\DoctorInfo\TEMPLATE_MSG_TYPE;
use Hyperf\Utils\ApplicationContext;

class DoctorWechatBusinessService
{
    /**
     * 问卷医生重新邀请模板
     *
     * @param $receiverOpenId
     * @param $url
     * @param $invitorName
     * @param $paperName
     * @param $start
     * @param $expire
     */
    public static function sendQuestionnaireWechatReInvite(
        $receiverOpenId,
        $url,
        $invitorName,
        $paperName,
        $start,
        $expire
    ) {
        $data = [
            'first' => [
                'value' => sprintf("%s邀请您重新参加问卷", $invitorName)
            ],
            'keyword1' => [
                'value' => $paperName
            ],
            'keyword2' => [
                'value' => $start
            ],
            'keyword3' => [
                'value' => $expire
            ],
            'remark' => [
                'value' => '问卷详细内容请点此信息进入查询'
            ]
        ];

        ApplicationContext::getContainer()
            ->get(DoctorInfoRpcService::class)
            ->sendTemplateMsg(
                $receiverOpenId,
                TEMPLATE_MSG_TYPE::TYPE_QUESTIONNAIRE_RE_INVITE,
                $data,
                $url
            );
    }

    /**
     * 问卷医生邀请模板消息
     * @param $receiverOpenId
     * @param $url
     * @param $invitorName
     * @param $paperName
     * @param $start
     * @param $expire
     */
    public static function sendQuestionnaireWechatInviteMsg(
        $receiverOpenId,
        $url,
        $invitorName,
        $paperName,
        $start,
        $expire
    ) {
        $data = [
            'first' => [
                'value' => sprintf("尊敬的用户，%s诚邀您参与一份问卷调查，参与问卷有惊喜！", $invitorName)
            ],
            'keyword1' => [
                'value' => $paperName
            ],
            'keyword2' => [
                'value' => $start
            ],
            'keyword3' => [
                'value' => $expire
            ],
            'remark' => [
                'value' => '点击开始问卷作答'
            ]
        ];

        ApplicationContext::getContainer()
            ->get(DoctorInfoRpcService::class)
            ->sendTemplateMsg(
                $receiverOpenId,
                TEMPLATE_MSG_TYPE::TYPE_QUESTIONNAIRE_INVITE,
                $data,
                $url
            );
    }

    /**
     * 问卷医生通知答题
     * @param $receiverOpenId
     * @param $url
     * @param $invitorName
     * @param $paperName
     * @param $start
     * @param $expire
     */
    public static function sendQuestionnaireWechatNoticeToAnswer(
        $receiverOpenId,
        $url,
        $invitorName,
        $paperName,
        $start,
        $expire
    ) {
        $data = [
            'first' => [
                'value' => sprintf("%s提醒您参加问卷", $invitorName)
            ],
            'keyword1' => [
                'value' => $paperName
            ],
            'keyword2' => [
                'value' => $start
            ],
            'keyword3' => [
                'value' => $expire
            ],
            'remark' => [
                'value' => '问卷详细内容请点此信息进入查询'
            ]
        ];

        ApplicationContext::getContainer()
            ->get(DoctorInfoRpcService::class)
            ->sendTemplateMsg(
                $receiverOpenId,
                TEMPLATE_MSG_TYPE::TYPE_QUESTIONNAIRE_NOTICE,
                $data,
                $url
            );
    }
}
