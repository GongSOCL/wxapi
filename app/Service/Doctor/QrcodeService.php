<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Doctor;

use App\Constants\DataStatus;
use App\Constants\Code\ErrorCodeDoctor;
use App\Exception\DoctorException;
use App\Repository\ActivityRepository;
use App\Repository\UsersRepository;
use App\Service\Wechat\WechatBusiness;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Di\Container;

class QrcodeService
{

    /**
     * @Inject()
     * @var Container
     */
    private $container;

    /**
     * @Inject()
     * @var WechatBusiness
     */
    private $wechatBusiness;

    /**
     * @Inject()
     * @var UsersRepository
     */
    private $userRepository;
    /**
     * @param $inviteYyid
     * @param $inviteeType
     * @return array
     */
    public function getQrcode($inviteYyid, $inviteeType)
    {
        //根据$inviteeType生成qrcode 1是代表公众号 邀请代表  2是医生公众号 邀请医生

        //判断微信参数是否配置
        $config = $this->container->get(ConfigInterface::class);
        if (!$config->has('wechat')) {
            throw new \UnexpectedValueException("微信参数尚未配置");
        }
        $wechatConfig = $config->get('wechat', []);
        //判断是否有效的$inviteeType
        $wxTypeConfig = isset($wechatConfig[$inviteeType]) ? $wechatConfig[$inviteeType] : [];
        if (!$wxTypeConfig) {
            throw new DoctorException(ErrorCodeDoctor::DOCTOR_INVITE_TYPE_NOT_EXISTS);
        }
        //获取行为信息 来源：t_activity
        $activityData = ActivityRepository::getActivity(DataStatus::ACTIVITY_ID_QRCODE_DOCTOR_INVITE);
        //根据yyid获取uid
        $inviteId = $this->userRepository->getUid($inviteYyid);
        if ($activityData) {
            $inviteData = $activityData->id.'#'.$activityData->event_key.'_'.$inviteId->uid.'_';
        }

        $url = $this->wechatBusiness->getQrcode($inviteData, $inviteeType);
        return ['url' => $url];
    }
}
