<?php
declare(strict_types=1);

namespace App\Service\Doctor;

use Grpc\DoctorInfo\DoctorInfoClient;
use Grpc\DoctorInfo\DoctorWechatInfoReply;
use Grpc\DoctorInfo\DoctorWechatInfoRequest;
use Grpc\DoctorInfo\IdsRequest;
use Grpc\DoctorInfo\SearchDoctorReply;
use Grpc\DoctorInfo\SearchDoctorRequest;
use Grpc\DoctorInfo\TemplateMsgRequest;
use Hyperf\Utils\ApplicationContext;

class DoctorInfoRpcService
{
    /**
     * @param $openId
     * @return DoctorWechatInfoReply
     */
    public function getWechatInfos(array $openIds, $keywords = ""): DoctorWechatInfoReply
    {
        $request = new DoctorWechatInfoRequest();
        $request->setOpenids($openIds);
        $request->setKeywords($keywords);

        $resp = $this->getClient()->getDoctorWechatInfo($request);
        return $resp;
    }

    public function sendTemplateMsg($openId, $type, array $data, $url = "")
    {
        $request = new TemplateMsgRequest();
        $request->setOpenid($openId);
        $request->setMsgType($type);
        $request->setMsgData(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        if ($url) {
            $request->setUrl($url);
        }

        $this->getClient()->sendWechatTemplateMsg($request);
    }

    private function getClient()
    {
        return ApplicationContext::getContainer()
            ->get(DoctorInfoClient::class);
    }

    public function searchDoctors($keyword = "", $limit = 10): SearchDoctorReply
    {
        $req = new SearchDoctorRequest();
        $req->setKeywords($keyword);
        $req->setLimit($limit);
        return self::getClient()->searchDoctors($req);
    }

    public function getByIds(array $ids): SearchDoctorReply
    {
        $req = new IdsRequest();
        $req->setId($ids);
        return self::getClient()->getDoctorByIds($req);
    }
}
