<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Doctor;

use App\Constants\Code\ErrorCodeDoctor;
use App\Constants\DataStatus;
use App\Exception\DoctorException;
use App\Model\Qa\RepsDoctorInviter;
use App\Model\Qa\WechatUser;
use App\Repository\DoctorInviterRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use Hyperf\Di\Annotation\Inject;

class DoctorInviterService
{
    /**
     * @Inject()
     * @var DoctorInviterRepository
     */
    private $doctorInviterRepository;

    /**
     * @Inject()
     * @var WechatUserRepository
     */
    private $wechatUserRepository;

    /**
     * @param $yyid
     * @param $type
     * @param $isAuth
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getInvitationList($yyid, $type, $isAuth, $page, $pageSize)
    {
        //获取邀请列表
        $invitationList = $this->doctorInviterRepository->getInvitationList(
            $yyid,
            $type,
            $isAuth,
            $page,
            $pageSize
        );
        foreach ($invitationList as &$item) {
            if (empty($item['name'])) {
                $item['name'] = $item['nickname'];
            }
        }

        //获取总条数
        $invitationCount = $this->doctorInviterRepository->getInvitationCount(
            $yyid,
            $type,
            $isAuth
        );


        $data = [
            'list' => $invitationList,
            'page' => $page,
            'page_size' => $pageSize,
            'total' => $invitationCount
        ];

        return $data;
    }

    /**
     * 邀请医生（加入我的邀请名单）
     * @param $inviteeId
     * @param $inviterId
     * @return int|mixed
     */
    public function inviteDoctor($inviteeId, $inviterId)
    {
        //暂时用不到
        return;
        //判断有没有注册 有注册的话拿取yyid
//        $yyidInfo = $this->wechatUserRepository->getYyidByOpenid($inviteeId);
//
//        //邀请添加一条信息到reps_doctor_inviter表
//        $inviteDoctor = $this->doctorInviterRepository->inviteDoctor($inviteeId,$inviterId,$yyidInfo->u_yyid,$type);
//        if(!$inviteDoctor) {
//            throw new DoctorException(ErrorCodeDoctor::FAIL_TO_INVITE_DOCTOR);
//        }
//        return $inviteDoctor;
    }


    /**
     * 扫码回调个人中心医生邀请医生事件
     *
     * @param $uid
     * @param array $wxInfo
     * @param WechatUser|null $wechatUser
     * @param int $wechatType
     */
    public static function processDoctorInviteEvent(
        $uid,
        array $wxInfo,
        WechatUser $wechatUser = null,
        $wechatType = DataStatus::WECHAT_DOCTOR
    ) {
        //获取邀请者的yyid 没有为空
        $user = UsersRepository::getUserByUid($uid);
        $inviterYyid = $user ? $user->yyid : '';
        $inviteeYyid = $wechatUser ? $wechatUser->u_yyid : '';

        //判断是否有好友关系 如果没有好友关系 或者好友关系已删除（status==0） 好友关系已拒绝（status==3） 需要添加好友关系
        //status==2待加入的时候要不要更新为1还不确定
        //status==1时只更新订阅信息
        $hasRelation = DoctorInviterRepository::getExistsRelation(
            $inviterYyid,
            $wxInfo['openid'],
            $wechatType
        );
        if ($hasRelation) {
            if ($hasRelation->status==0||$hasRelation->status==3) {
                //现在默认都是医生 user_type==2  status直接变成1
                DoctorInviterRepository::addRelation(
                    $inviterYyid,
                    $inviteeYyid,
                    $wxInfo['openid'],
                    DataStatus::USER_TYPE_DOCTOR,
                    $wechatType,
                    RepsDoctorInviter::STATUS_COMMON
                );
            } elseif ($hasRelation->status==2) {
                DoctorInviterRepository::updateRelation(
                    $inviterYyid,
                    $wxInfo['openid'],
                    DataStatus::USER_TYPE_DOCTOR,
                    $wechatType,
                    RepsDoctorInviter::STATUS_COMMON
                );
            }
        } else {
            //现在默认都是医生 user_type==2  status直接变成1
            DoctorInviterRepository::addRelation(
                $inviterYyid,
                $inviteeYyid,
                $wxInfo['openid'],
                DataStatus::USER_TYPE_DOCTOR,
                $wechatType,
                RepsDoctorInviter::STATUS_COMMON
            );
        }
    }
}
