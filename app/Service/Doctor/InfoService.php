<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Doctor;

use App\Constants\Code\ErrorCodeDoctor;
use App\Constants\Code\ErrorCodeGroup;
use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Exception\DoctorException;
use App\Exception\GroupException;
use App\Helper\Helper;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Qa\NewDoctorUserinfo;
use App\Repository\AdaptionRepository;
use App\Repository\AgentVisitDoctorRepository;
use App\Repository\DoctorOpenidUserinfoRepository;
use App\Repository\GroupMemberRepository;
use App\Repository\NewDoctorOpenidUserinfoRepository;
use App\Repository\NewGroup\NewDoctorUserinfoRepository;
use App\Repository\RegionRepository;
use App\Repository\SystemDepartRepository;
use App\Repository\UsersRepository;
use App\Repository\YouyaoHospitalRepository;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use function foo\func;

class InfoService
{

    /**
     * @Inject()
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * 编辑医生信息.
     *
     * @param $id
     * @param $name
     * @param $mobile
     * @param $hospitalId
     * @param $departId
     * @param int $sex
     * @param string $jobTitle
     * @param string $position
     * @param int $fieldId
     * @param string $clinicRota
     */
    public static function editDoctorInfo(
        $id,
        $name,
        $mobile,
        $hospitalId,
        $departId,
        $sex = 0,
        $jobTitle = "",
        $position = "",
        $fieldId = 0,
        $clinicRota = ""
    ) {
        $doctor = DoctorOpenidUserinfoRepository::getInfoById($id);
        if (!$doctor) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医生信息不存在");
        }

        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }

        $depart = SystemDepartRepository::getById($departId);
        if (!$depart) {
            throw new BusinessException(ErrorCode::DEPART_NOT_EXISTS);
        }

        $doctor->true_name = $name;
        $doctor->mobile_num = $mobile;
        $doctor->hospital_yyid = $hospital->yyid;
        $doctor->hospital_name = $hospital->hospital_name;
        $doctor->depart_id = $depart->id;
        $doctor->depart_name = $depart->name;
        $doctor->gender = $sex;
        $doctor->job_title = $jobTitle;
        $doctor->position = $position;
        $doctor->field_id = (string)$fieldId;
        $doctor->clinic_rota = $clinicRota;
        $doctor->updated_at = date('Y-m-d H:i:s');
        if (!$doctor->save()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医生信息更新失败");
        }
    }

    public static function getValidDoctorToMerge(int $id, $mobile): array
    {
        $resp = NewDoctorUserinfoRepository::getDoctorReps($id);
        $currentInfo = NewDoctorUserinfoRepository::getShareDoctorInfo($resp->new_doctor_id);
        //根据当前本条信息有没有微信信息来判断
        if ($currentInfo->openid) {
            // 判断手机号在扫过码的医生中存不存在重复
            $repeatScanDoctors = NewDoctorUserinfoRepository::getSameScanDoctor($mobile, $currentInfo->nid);
            if ($repeatScanDoctors) {
                throw new GroupException(ErrorCodeGroup::DOCTOR_PHONE_ALREADY_USED);
            }
            // 判断手机号在拜访手动添加的医生中存不存在重复
            $repeatAddDoctors = NewDoctorUserinfoRepository::getSameAddDoctor($mobile, $currentInfo->nid);
            if ($repeatAddDoctors) {
                $repeatAddDoctors = $repeatAddDoctors ? $repeatAddDoctors->toArray() : [];
                if ($repeatAddDoctors['field_id']) {
                    $repeatAddDoctors['field_name'] = AdaptionRepository::getById(
                        (int)$repeatAddDoctors['field_id']
                    )->name;
                } else {
                    $repeatAddDoctors['field_name'] = '';
                }
                return $repeatAddDoctors;
            }
        }
        return [];
    }

    public static function mergeDoctor(int $id, int $relatedId, $agree = 1)
    {
        $reps = NewDoctorUserinfoRepository::getDoctorReps($id);
        $rInfo = NewDoctorUserinfoRepository::getShareDoctorInfo($relatedId);
        if ($agree==1) {
            // 合并
            Db::beginTransaction();
            try {
                NewDoctorUserinfoRepository::mergeInfo($reps->new_doctor_id, $rInfo);
                NewDoctorUserinfoRepository::updateReps($relatedId, $reps->new_doctor_id);
                self::clearRepeatedAgentDoctors($reps->new_doctor_id);
                NewDoctorUserinfoRepository::deleteDoctor($relatedId);
                Db::commit();
            } catch (\Exception $exception) {
                Db::rollBack();
                throw $exception;
            }
        } else {
            // 不合并 只修改手机号
            NewDoctorUserinfoRepository::updateDoctorPhone($reps->new_doctor_id, $rInfo->mobile_num);
        }
    }

    private static function updateMergeInfo(NewDoctorUserinfo $doctor, NewDoctorUserinfo $mergeDoctor)
    {
        $doctor->true_name = $mergeDoctor->true_name;
        $doctor->gender = $mergeDoctor->gender;
        if ($mergeDoctor->user_id) {
            $doctor->user_id = $mergeDoctor->user_id;
        }
        if ($mergeDoctor->mobile_num) {
            $doctor->mobile_num = $mergeDoctor->mobile_num;
        }
        if ($mergeDoctor->hospital_name) {
            $doctor->hospital_name = $mergeDoctor->hospital_name;
        }
        if ($mergeDoctor->hospital_yyid) {
            $doctor->hospital_yyid = $mergeDoctor->hospital_yyid;
        }
        $doctor->depart_name = $mergeDoctor->depart_name;
        $doctor->depart_id = $mergeDoctor->depart_id;
        $doctor->job_title = $mergeDoctor->job_title;
        $doctor->position = $mergeDoctor->position;
        $doctor->field_id = $mergeDoctor->field_id;
        $doctor->clinic_type = $mergeDoctor->clinic_type;
        $doctor->clinic_rota = $mergeDoctor->clinic_rota;
        $doctor->updated_at = date('Y-m-d H:i:s');
        if (!$doctor->save()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合并医生信息更新失败");
        }
    }

    private static function clearRepeatedAgentDoctors($newDoctorId)
    {
        // 处理重复数据
        $repeatResp = NewDoctorUserinfoRepository::getRepeatReps1($newDoctorId);
        // 清除重复关系
        if ($repeatResp->isEmpty()) {
            return;
        }
        $groupMax = [];
        $groupIds = [];
        $groupIdList = [];
        /** @var NewDoctorOpenidUserinfo $item */
        foreach ($repeatResp as $item) {
            $key = sprintf("%d.%d", $item->user_id, $item->new_doctor_id);
            $groupIdList[$key][] = $item->id;
            if (!isset($groupMax[$key])) {
                $groupMax[$key] = $item->id;
            } else {
                if ($groupMax[$key] < $item->id) {
                    //小于最大id的都要移除掉
                    $groupIds[] = $groupMax[$key];
                    $groupMax[$key] = $item->id;
                } else {
                    $groupIds[] = $item->id;
                }
            }
        }

        //批量删除那些
        if (!empty($groupIds)) {
            //根据id全量删除对应关系
            NewDoctorUserinfoRepository::deleteRepeatReps($groupIds);
        }

        //其它的删除掉
        foreach ($groupIdList as $key => $ids) {
            $maxId = $groupMax[$key];
            $filterIds = array_filter($ids, function ($val) use ($maxId) {
                return $val != $maxId;
            });
            if (!empty($filterIds)) {
                AgentVisitDoctorRepository::changeDoctorToNew($filterIds, $maxId);
            }
        }
    }


    /**
     * @param $yyid
     * @return array
     */
    public function getPersonInfo($yyid)
    {
        //获取个人信息
        $personObject = $this->usersRepository->getPersonInfo($yyid);
        $personInfo = $personObject ? $personObject->toArray() : [];
        if (!$personInfo) {
            throw new DoctorException(ErrorCodeDoctor::DOCTOR_USER_NOT_EXISTS);
        }
        $personInfo['uid'] = Helper::generateHashId($personInfo['uid']);

        //获取级联地区信息
        if (isset($personInfo['region_yyid'])&&$personInfo['region_yyid']) {
            $regionObj = $this->regionRepository->getRegion($personInfo['region_yyid']);
            $region = $regionObj ? $regionObj->toArray() : [];
            $personInfo['region_name'] = trim(implode(',', $region), ',');
        } else {
            $personInfo['region_name'] = '';
        }

        return $personInfo;
    }

    private static function buildDoctorInfo(NewDoctorUserinfo $doctor)
    {
        $hospitalInfo = [];
        if ($doctor->hospital_yyid != "") {
            $hospital = YouyaoHospitalRepository::getHospitalByYYID($doctor->hospital_yyid);
            $hospital && $hospitalInfo = [
                'id' => $hospital->id,
                'name' => $hospital->hospital_name
            ];
        }
        $departInfo = [];
        if ($doctor->depart_id) {
            $depart = SystemDepartRepository::getById($doctor->depart_id);
            $depart && $departInfo = [
                'id' => $depart->id,
                'name' => $depart->name
            ];
        }

        $fieldInfo = [];
        if ($doctor->field_id > 0) {
            $field = AdaptionRepository::getById((int)$doctor->field_id);
            if ($field) {
                $fieldInfo = [
                    'id' => $field->id,
                    'name' => $field->name
                ];
            }
        }

        return [
            'name' => $doctor->true_name ?: ($doctor->nickname ?: ''),
            'sex' => $doctor->gender,
            'mobile' => $doctor->mobile_num,
            'hospital' => $hospitalInfo,
            'depart' => $departInfo,
            'job_title' => $doctor->job_title,
            'position' => $doctor->position,
            'field_id' => $doctor->field_id,
            'field' => $fieldInfo,
            'clinic_type' => $doctor->clinic_type,
            'clinic_rota' => (string)$doctor->clinic_rota
        ];
    }

    public static function getInfo($id): array
    {
        $doctor = DoctorOpenidUserinfoRepository::getInfoById($id);
        if (!$doctor) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医生信息不存在");
        }

        return self::buildDoctorInfo($doctor);
    }

    /**
     * @Inject()
     * @var RegionRepository
     */
    private $regionRepository;

    public static function deleteDoctor(int $id)
    {
        //根据id从doctor_openid_userinfo中获取医生
        $doctorInfo = DoctorOpenidUserinfoRepository::getInfoById($id);
        if (!$doctorInfo) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医生信息不存在");
        }

        //删除医生记录
        DoctorOpenidUserinfoRepository::deleteDoctorResp($id);

        //删除医生和代表的好友关系
        if ($doctorInfo->openid && $doctorInfo->user_yyid) {
            GroupMemberRepository::clearFriendRelation(
                $doctorInfo->user_yyid,
                $doctorInfo->openid,
                DataStatus::WECHAT_DOCTOR
            );
        }
    }
}
