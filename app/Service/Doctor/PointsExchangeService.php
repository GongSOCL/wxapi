<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Doctor;

use App\Model\Qa\Users;
use App\Repository\PointsExchangeRepository;
use App\Repository\UserAssetRepository;
use App\Service\Points\PointsService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Arr;

class PointsExchangeService
{
    /**
     * @Inject
     * @var UserAssetRepository
     */
    private $userAssetRepository;

    /**
     * @Inject
     * @var PointsExchangeRepository
     */
    private $pointsExchangeRepository;
}
