<?php


namespace App\Service\Doctor;

use Grpc\DoctorInfo\DoctorWechatInfoItem;
use Grpc\DoctorInfo\UserInfoItem;
use Hyperf\Utils\ApplicationContext;

class DoctorInfoService
{
    /**
     * 根据单个openid获取医生微信信息
     *
     * @param $openId
     * @param string $keywords
     * @return array
     */
    public static function getWechatInfoByOpenId($openId, $keywords = ''): array
    {
        $reply = self::getWechatInfos([$openId], $keywords);
        if ($reply->getList()->count() == 0) {
            return [];
        }

        $item = $reply->getList()[0];
        return [
            'id' => $item->getId(),
            'uid' => $item->getDoctorId(),
            'openid' => $item->getOpenid(),
            'unionid' => '',
            'nickname' => $item->getName(),
            'headimgurl' => $item->getHeadimg(),
            'sex' => $item->getSex(),
            'country' => $item->getCity(),
            'city' => $item->getCity(),
            'phone' => $item->getPhone(),
            'user_verified' => $item->getUserVerified()
        ];
    }

    private static function getWechatInfos(array $openIds, $keywords = '')
    {
        $rpc = ApplicationContext::getContainer()
            ->get(DoctorInfoRpcService::class);

        return $rpc->getWechatInfos($openIds, $keywords);
    }

    /**
     * 根据多个openid获取微信信息
     * @param array $openIds
     * @param string $keywords
     * @return array
     */
    public static function getWechatInfoByOpenIds(array $openIds, $keywords = ""): array
    {
        $reply = self::getWechatInfos($openIds, $keywords);

        $data = [];
        /** @var DoctorWechatInfoItem $item */
        foreach ($reply->getList() as $item) {
            $data[] = [
                'id' => $item->getId(),
                'uid' => $item->getDoctorId(),
                'openid' => $item->getOpenid(),
                'unionid' => '',
                'nickname' => $item->getName(),
                'headimgurl' => $item->getHeadimg(),
                'sex' => $item->getSex(),
                'country' => $item->getCity(),
                'city' => $item->getCity(),
                'phone' => $item->getPhone(),
                'user_verified' => $item->getUserVerified()
            ];
        }
        return $data;
    }

    public static function searchDoctors($keywords = "", $limit = 10): array
    {
        $rpc = new DoctorInfoRpcService();
        $resp = $rpc->searchDoctors($keywords, $limit);
        $data = [];
        /** @var UserInfoItem $user */
        foreach ($resp->getUsers() as $user) {
            $trueName = $user->getTruename();
            $data[] = [
                'id' => $user->getId(),
                'name' => $trueName ? $trueName : $user->getName()
            ];
        }

        return $data;
    }
}
