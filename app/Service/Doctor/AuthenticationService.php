<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Doctor;

use App\Constants\Code\ErrorCodeDoctor;
use App\Exception\DoctorException;
use App\Repository\DoctorOpenidUserinfoRepository;
use App\Repository\DoctorRepository;
use App\Repository\UsersRepository;
use App\Repository\YouyaoHospitalRepository;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

class AuthenticationService
{
    /**
     * @Inject()
     * @var DoctorOpenidUserinfoRepository
     */
    private $doctorOpenidUserinfoRepository;

    /**
     * @Inject()
     * @var YouyaoHospitalRepository
     */
    private $youyaoHospitalRepository;

    /**
     * @Inject()
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @Inject()
     * @var DoctorRepository
     */
    private $doctorRepository;


    /**
     * @param $num
     * @return array
     */
    public function getPreInfo($num)
    {
        //获取预留信息
        $preInfo = $this->doctorOpenidUserinfoRepository->getPreInfo($num);

        return $preInfo ? $preInfo->toArray() : [];
    }

    /**
     * @param $yyid
     * @return array
     */
    public function getVerifyInfo($yyid)
    {
        //获取认证信息
        $verifyInfo = $this->doctorRepository->getVerifyInfo($yyid);

        return $verifyInfo ? $verifyInfo->toArray() : [];
    }

    /**
     * 认证
     * @param $params
     * @return array
     */
    public function qualify($params)
    {
        //验证是否在认证中
        $isDoctor = $this->usersRepository->getIsDoctor($params['user_yyid']);
        if (!$isDoctor) {
            throw new DoctorException(ErrorCodeDoctor::DOCTOR_USER_NOT_EXISTS);
        }
        if ($isDoctor->is_doctor==1) {
            throw new DoctorException(ErrorCodeDoctor::DOCTOR_QUALIFYINFO_ALREADY_SUBMIT);
        }

        //验证医院是否存在
        $hosptialInfo = $this->youyaoHospitalRepository->checkHosptial($params['hospital_yyid']);
        if (!$hosptialInfo) {
            throw new DoctorException(ErrorCodeDoctor::DOCTOR_HOSPITAL_NOT_EXISTS);
        }

        //验证科室是否存在
//        $departmentInfo = $this->departmentRepository->checkDepartment($params['hospital_yyid'],$params['depart_id']);
//        if(!$departmentInfo) throw new BusinessException(ErrorCode::DOCTOR_DEPARTMENT_NOT_EXISTS);

        //提交认证资料
        try {
            Db::transaction(function () use ($params, $hosptialInfo) {
                //修改t_users表is_doctor字段为1
                $this->usersRepository->changeIsDoctor($params['user_yyid']);

                //向t_doctor表插入一条记录
                $this->doctorRepository->createOneInfo($params, $hosptialInfo['hospital_name']);
            });

            return [];
        } catch (DoctorException $exception) {
            throw new DoctorException(ErrorCodeDoctor::DOCTOR_FAIL_TO_SUBMIT_QUALIFYINFO);
        }
    }
}
