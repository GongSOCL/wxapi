<?php
declare(strict_types=1);

namespace App\Service\MorningShare;

use App\Constants\DataStatus;
use App\Helper\Helper;
use App\Repository\MorningShareRepository;

class MorningShareService
{
    /**
     * @return array
     */
    public function getType()
    {
        $data = MorningShareRepository::getType();
        return $data->isNotEmpty() ? $data->toArray() : [];
    }

    /**
     * @return array
     */
    public function getDepartment()
    {
        $data = MorningShareRepository::getDepartment();
        return $data->isNotEmpty() ? $data->toArray() : [];
    }

    /**
     * @param $id
     * @param $title
     * @param $typeId
     * @param $departmentId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getMonpic($id, $title, $typeId, $departmentId, $page, $pageSize)
    {
        $where = $this->getWhere($title, $typeId, $departmentId);
        $data = MorningShareRepository::getList($where, $page, $pageSize);
        $count = MorningShareRepository::getListCount($where);

        $data = $data->isNotEmpty() ? $data->toArray() : [];

        if ($data) {
            foreach ($data as $key => $value) {
                $data[$key]['count'] = $this->getCountByPicId($value['id']);
                $data[$key]['is_share'] = $this->checkMyStatus($id, $value['id']);
                $data[$key]['my_count'] = $this->getCountByUserId($id, $value['id']);
                $data[$key]['time'] = $this->getTimeByPicId($value['id']);
            }
        }

        return [
            'list' => $data,
            'total' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $id
     * @param $picId
     * @return array
     */
    public function getDetailById($id, $picId)
    {
        $data = MorningShareRepository::getDetailById($picId);
        $data = $data ? $data->toArray() : [];

        $data['count'] = $this->getCountByPicId($picId);
        $data['is_share'] = $this->checkMyStatus($id, $picId);
        $data['my_count'] = $this->getCountByUserId($id, $picId);
        $data['time'] = $this->getTimeByPicId($picId);

        return $data;
    }

    /**
     * @param $id
     * @param $rankType
     * @param $limit
     * @return array
     */
    public function getCountRank($id, $rankType, $limit)
    {
        $data = [];
        if ($rankType==1) {
            $res = MorningShareRepository::getCountRank();
        } else {
            $res = MorningShareRepository::getCountRankByUserId($id);
        }

        $list = [];
        if ($res->isNotEmpty()) {
            foreach ($res->toArray() as $value) {
                $list[$value['pic_id']][] = $value;
            }
            foreach ($list as $k => $v) {
                $data[$k]['pic_id'] = $k;
                $data[$k]['count'] = count($v);
            }
        }

        //排序
        $data = array_slice(Helper::arraySort($data, 'count', 'desc'), 0, $limit);

        foreach ($data as $key => $item) {
            $detail = MorningShareRepository::getDetailById($item['pic_id']);
            $data[$key]['title'] = $detail->title;
            $data[$key]['department'] = $detail->department;
            $data[$key]['type'] = $detail->type;
            $data[$key]['img'] = $detail->img;
        }

        return $data;
    }

    /**
     * @param $id
     * @param $rankType
     * @param $limit
     * @return array
     */
    public function getTimeRank($id, $rankType, $limit)
    {
        $data = [];
        if ($rankType==1) {
            $res = MorningShareRepository::getPicId();
        } else {
            $res = MorningShareRepository::getPicIdByUserId($id);
        }

        if ($res->isNotEmpty()) {
            foreach ($res->toArray() as $key => $value) {
                $data[$key]['pic_id'] = $value['pic_id'];
                $redis = new ShareRedisService($value['pic_id']);
                $redisCount = $redis->getHeartBeatCount();
                $data[$key]['sec'] = $redisCount * 5;
            }
        }

        //排序
        $data = array_slice(Helper::arraySort($data, 'sec', 'desc'), 0, $limit);

        foreach ($data as $key => $item) {
            $detail = MorningShareRepository::getDetailById($item['pic_id']);
            $data[$key]['title'] = $detail->title;
            $data[$key]['department'] = $detail->department;
            $data[$key]['type'] = $detail->type;
            $data[$key]['img'] = $detail->img;
            $data[$key]['time'] = Helper::timeFormat($data[$key]['sec']);
        }

        return $data;
    }

    /**
     * @param $title
     * @param $typeId
     * @param $departmentId
     * @return array
     */
    public function getWhere($title, $typeId, $departmentId)
    {
        $where = [];
        if ($title) {
            $where[] = ['ms.title', 'like', '%' . $title . '%'];
        }
        if ($typeId) {
            $where[] = ['type_id', '=', $typeId];
        }
        if ($departmentId) {
            $where[] = ['department_id', '=', $departmentId];
        }
        $where[] = ['status', '=', DataStatus::REGULAR];
        $where[] = ['ms.is_deleted', '=', DataStatus::DELETE];
        return $where;
    }

    /**
     * @param $picId
     * @return int
     */
    public function getCountByPicId($picId)
    {
        return MorningShareRepository::getCountByPicId($picId)->count();
    }

    /**
     * @param $id
     * @param $picId
     * @return int
     */
    public function checkMyStatus($id, $picId)
    {
        $res = MorningShareRepository::checkShareStatus($id, $picId);

        return $res ? 1 : 0;
    }

    /**
     * @param $id
     * @param $picId
     * @return int
     */
    public function getCountByUserId($id, $picId)
    {
        return MorningShareRepository::getCountByUserId($id, $picId)->count();
    }

    /**
     * @param $id
     * @return string
     */
    public function getTimeByPicId($id)
    {
        $redis = new ShareRedisService($id);
        $redisCount = $redis->getHeartBeatCount();

        return Helper::timeFormat($redisCount * 5);
    }
}
