<?php
declare(strict_types=1);

namespace App\Service\MorningShare;

use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;

class ShareRedisService
{
    private $redis;

    private $picShareTimeKey;

    public function __construct($picId)
    {
        $container = ApplicationContext::getContainer();
        $this->redis = $container->get(RedisFactory::class)->get('share');
        $this->picShareTimeKey = 'share:picstaytime_'.$picId;
    }

    /**
     * @return int
     */
    public function getHeartBeatCount()
    {
        $redis = $this->redis;
        return (int)$redis->lLen($this->picShareTimeKey);
    }
}
