<?php
declare(strict_types=1);

namespace App\Service\Feedback;

use Grpc\Feedback\AddFeedbackReply;
use Grpc\Feedback\AddFeedbackRequest;
use Youyao\Framework\Client\CoreClient;

class FeedbackRpcService extends CoreClient
{
    /**
     * @param AddFeedbackRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function addFeedback(AddFeedbackRequest $argument)
    {
        return $this->sendRequest($argument, [AddFeedbackReply::class, 'decode']);
    }
}
