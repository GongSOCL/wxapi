<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Points;

use App\Constants\DataStatus;
use App\Helper\Helper;
use App\Model\Qa\Msg;
use App\Model\Qa\Users;
use App\Model\Qa\WechatUser;
use App\Service\Msg\MsgService;
use Grpc\Point\GoodsDetail;
use Grpc\Point\PointDetail;
use Grpc\Point\UserType;
use Grpc\Questionnaire\USER_TYPE;
use GuzzleHttp\Exception\GuzzleException;

class PointsService
{
    /**
     * 添加问卷积分事件(添加积分事件的门面方法，参数集中到一个地方维护了).
     * @param $uid
     * @param $paperId
     * @param int $wechatUserId
     * @param bool $isAgent
     * @throws GuzzleException
     */
    public static function addQuestionnaireActivityEvent($uid, $paperId, $wechatUserId = 0, $isAgent = false)
    {
        $userType = $isAgent ? USER_TYPE::USER_TYPE_AGENT : USER_TYPE::USER_TYPE_DOCTOR;
        PointRpcService::activity('question_answer', 5, $uid, $wechatUserId, $paperId, $userType);
    }

    /**
     * 消耗积分兑换商品
     * @param Users $user
     * @param WechatUser $wechatUser
     * @param mixed $goodsYyid
     * @param mixed $goodsNum
     * @param bool $isAgent
     * @return array
     */
    public static function exchangeGoods(
        Users $user,
        WechatUser $wechatUser,
        $goodsYyid,
        $goodsNum,
        $isAgent = false
    ): array {
        $userType = $isAgent ? USER_TYPE::USER_TYPE_AGENT : USER_TYPE::USER_TYPE_DOCTOR;
        $reply = PointRpcService::exchangeGoods($user, $wechatUser, $goodsYyid, $goodsNum, $userType);

        $exchangeResult = [
            'money_point' => $reply->getMoneyPoint(),
            'service_point' => $reply->getServicePoint(),
            'desc' => $reply->getDesc(),
            'yyid' => $reply->getYyid(),
        ];

        self::sendExchangeGoodsPointMsg(
            $reply->getDesc(),
            $goodsNum,
            $reply->getMoneyPoint(),
            $reply->getServicePoint()
        );

        return $exchangeResult;
    }

    /**
     * 发送兑换商品站内信
     * @param $desc
     * @param $num
     * @param int $moneyPoint
     * @param int $servicePoint
     */
    public static function sendExchangeGoodsPointMsg($desc, $num, $moneyPoint = 0, $servicePoint = 0)
    {
        $content = sprintf("您已成功兑换%s商品，数量为%d", $desc, $num);
        if ($moneyPoint > 0) {
            $content .= sprintf("，使用%s消息积分", $moneyPoint);
        }
        if ($servicePoint > 0) {
            $content .= sprintf("，使用%s服务积分", $servicePoint);
        }

        //添加msg记录
        $now = date('Y-m-d H:i:s');
        $user  = Helper::getLoginUser();
        MsgService::addUserMsg(
            '消耗积分',
            Msg::MSG_CONSUMPUTION_POINT,
            $user,
            $content,
            '',
            '',
            Msg::SEND_TYPE_USER,
            false,
            $now,
            $now,
            false
        );
    }

    /**
     * 获取登陆用户积分信息
     * @return float[]
     */
    public static function getUserPoints($groupId = 0): array
    {
        if ($groupId == 0) {
            $groupResp = PointRpcService::getDefaultGroup();
            $groupId = $groupResp->getInfo()->getId();
        }

        $user = Helper::getLoginUser();
        $resp = PointRpcService::getUserAssets(
            (int)$user->uid,
            UserType::USER_TYPE_AGENT,
            $groupId
        );
        $servicePoint = $resp->getServicePoint();
        $moneyPoint = $resp->getMoneyPoint();

        return [$servicePoint + 0.0, $moneyPoint + 0.0];
    }

    /**
     * @throws GuzzleException
     */
    public static function getExchangeGoodsList($current = 1, $limit = 10, $isAgentApp = true, $groupId = 0): array
    {
        if ($groupId == 0) {
            $groupResp = PointRpcService::getDefaultGroup();
            $groupId = $groupResp->getInfo()->getId();
        }

        $user = Helper::getLoginUser();
        $resp = PointRpcService::fetchExchangeGoodsList(
            $user->uid,
            $current,
            $limit,
            DataStatus::USER_TYPE_AGENT,
            $isAgentApp
        , $groupId);
        $data = [];
        $pages = (int)ceil($resp->getTotal()/$limit);
        $pages = $pages < 1 ? 1 : $pages;
        $page = [
            'total' => $resp->getTotal(),
            'current' => $resp->getPage(),
            'limit' => $resp->getPageSize(),
            'pages' => $pages
        ];

        if ($resp->getTotal() > 0) {
            /** @var GoodsDetail $item */
            foreach ($resp->getList() as $item) {
                $data[] = [
                    'yyid' => $item->getYyid(),
                    'name' => $item->getGoodsName(),
                    'cost_money_point' => $item->getMoneyPoint(),
                    'cost_service_point' => $item->getServicePoint(),
                    'desc' => $item->getDesc(),
                    'pic' => $item->getGoodsPic(),
                    'is_unlimited' => $item->getIsUnlimited() ? 1 : 0,
                    'rest' => $item->getRestNum()
                ];
            }
        }

        return [$page, $data];
    }

    public static function getUserPointDetails(int $current = 1, int $limit = 10, int $type = 0, $pointType = 1, $groupId = 0): array
    {
        if ($groupId == 0) {
            $groupResp = PointRpcService::getDefaultGroup();
            $groupId = $groupResp->getInfo()->getId();
        }
        $user = Helper::getLoginUser();
        $resp = PointRpcService::fetchPointDetails($user->uid, $current, $limit, $type, $pointType == 1,
            UserType::USER_TYPE_AGENT,
            $groupId
        );

        $pages = (int)ceil($resp->getTotal()/$limit);
        $pages = $pages < 1 ? 1 : $pages;
        $page = [
            'total' => $resp->getTotal(),
            'current' => $resp->getPage(),
            'limit' => $resp->getPageSize(),
            'pages' => $pages
        ];
        $data = [];

        if ($resp->getTotal() > 0) {

            /** @var PointDetail $item */
            foreach ($resp->getList() as $item) {
                $p = $pointType == 1 ? $item->getServicePoint() : $item->getMoneyPoint();
                $data[] = [
                    'desc' => $item->getDesc(),
                    'status' => self::getStatusDesc($item->getStatus(), $item->getType()),
                    'date' => Helper::formatInputDate($item->getCreatedAt(), "Y-m-d"),
                    'points' =>self::getPointDesc($p, $item->getStatus(), $item->getType()),
                ];
            }
        }

        return [$page, $data];
    }

    private static function getPointDesc($point, $status, $type): string
    {
        if ($type == 3) {   //目前撤回不区分状态， 没有取消撤回操作
            return sprintf("-%.2f", $point);
        } else {
            switch ($status) {
                case 2:
                    return sprintf("+%.2f", $point);
                default:
                    return sprintf("-%.2f", $point);
            }
        }

    }

    private static function getStatusDesc($status, $type): string
    {
        if ($type == 3) {   //目前撤回不区分状态， 没有取消撤回操作
            return '发放撤销';
        } else {
            switch ($status) {
                case 1:
                    return "申请中";
                case 2:
                    return "已到帐";
                case 3:
                    return "已兑换";
                case 4:
                    return "兑换失败";
                default:
                    return "未知";
            }
        }

    }

    public static function getGoodsDetails(string $yyid): array
    {
        $resp = PointRpcService::getGoods($yyid);

        return [
            'yyid' => $resp->getYyid(),
            'name' => $resp->getGoodsName(),
            'cost_money_point' => $resp->getMoneyPoint(),
            'cost_service_point' => $resp->getServicePoint(),
            'desc' => $resp->getDesc(),
            'pic' => $resp->getGoodsPic(),
            'is_unlimited' => $resp->getIsUnlimited() ? 1 : 0,
            'rest' => $resp->getRestNum()
        ];
    }

    /**
     * app兑换商品
     * @param $goodsYYID
     * @param $goodsNum
     * @param WechatUser $wechatInfo
     */
    public static function appExchangeGoods($goodsYYID, $goodsNum, WechatUser $wechatInfo)
    {
        $user = Helper::getLoginUser();

        $resp = PointRpcService::exchangeGoods(
            $user,
            $wechatInfo,
            $goodsYYID,
            $goodsNum
        );

        self::sendExchangeGoodsPointMsg(
            $resp->getDesc(),
            $goodsNum,
            $resp->getMoneyPoint(),
            $resp->getServicePoint()
        );
    }
}
