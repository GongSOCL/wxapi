<?php
declare(strict_types=1);

namespace App\Service\Points;

use App\Constants\DataStatus;
use App\Model\Qa\Msg;
use App\Model\Qa\Users;
use App\Model\Qa\WechatUser;
use App\Service\Msg\MsgService;
use Grpc\Point\AddActivityPointReq;
use Grpc\Point\AssetsDetail;
use Grpc\Point\BindWechatReq;
use Grpc\Point\DetailListReply;
use Grpc\Point\ExchangeGoodsReq;
use Grpc\Point\GetDefaultGroupRequest;
use Grpc\Point\GetDetailListReq;
use Grpc\Point\GetGoodsListReq;
use Grpc\Point\GoodsDetail;
use Grpc\Point\GoodsListReply;
use Grpc\Point\GoodsYYIDRequest;
use Grpc\Point\PointClient;
use Grpc\Point\PointDetail;
use Grpc\Point\PointPlatform;
use Grpc\Point\UidReq;
use Grpc\Point\UserType;
use GuzzleHttp\Exception\GuzzleException;
use Hyperf\Utils\ApplicationContext;

class PointRpcService
{
    /**
     * 添加积分事件.
     * @param string $key 事件编号
     * @param string $type 特例的类型 1 表示地域 2表示 角色 3表示产品 4表示医院 5表示特定数据 问卷跟页面浏览都传 5
     * @param string $uid 用户 id
     * @param int $wechatUserId
     * @param string $val 特例数值
     * @throws GuzzleException
     */
    public static function activity(
        $key = '',
        $type = '',
        $uid = '',
        $wechatUserId = 0,
        $val = '',
        $userType = UserType::USER_TYPE_AGENT,
        $groupId = 0
    ) {
        $uType = $userType == DataStatus::USER_TYPE_AGENT ?
            UserType::USER_TYPE_AGENT : UserType::USER_TYPE_DOCTOR;
        $req = new AddActivityPointReq();
        $req->setUid(intval($uid));
        $req->setKey($key);
        $req->setType($type);
        $req->setVal(strval($val));
        $req->setWechatUserID($wechatUserId ? intval($wechatUserId) : 0);
        $req->setUserType($uType);
        $req->setGroupId($groupId);

        self::getClient()->addActivityPoint($req);
    }

    /**
     * 消耗积分兑换商品
     * @param Users $user
     * @param WechatUser $wechatUser
     * @param mixed $goodsYyid
     * @param mixed $goodsNum
     * @param int $userType
     * @return PointDetail
     */
    public static function exchangeGoods(
        Users $user,
        WechatUser $wechatUser,
        $goodsYyid,
        $goodsNum,
        $userType = UserType::USER_TYPE_AGENT
    ): PointDetail {
        $uType = $userType == DataStatus::USER_TYPE_AGENT ? UserType::USER_TYPE_AGENT : UserType::USER_TYPE_DOCTOR;
        $req = new ExchangeGoodsReq();
        $req->setUid($user->uid);
        $req->setGoodsNum($goodsNum);
        $req->setGoodsYyid($goodsYyid);
        $req->setWechatUserID($wechatUser->id);
        $req->setUserType($uType);

        return self::getClient()->exchangeGoods($req);
    }

    /**
     * 积分兑换商品列表.
     * @param mixed $uid
     * @param mixed $page
     * @param mixed $pageSize
     * @throws GuzzleException
     */
    public static function fetchExchangeGoodsList(
        $uid,
        $page,
        $pageSize,
        $userType = DataStatus::USER_TYPE_AGENT,
        $isAgentApp = true
    , $groupId = 0): GoodsListReply {
        $uType = $userType == DataStatus::USER_TYPE_AGENT ? UserType::USER_TYPE_AGENT : UserType::USER_TYPE_DOCTOR;
        $req = new GetGoodsListReq();
        $req->setUid($uid);
        $req->setPage($page);
        $req->setPageSize($pageSize);
        $req->setUserType($uType);
        $req->setPlatform(PointPlatform::PLATFORM_NEW_AGENT);
        $req->setGroupId($groupId);

        return self::getClient()->getGoodsList($req);
    }

    /**
     * 获取积分明细列表.
     * @param mixed $uid
     * @param mixed $page
     * @param mixed $pageSize
     * @param bool $fetchService
     * @throws GuzzleException
     */
    public static function fetchPointDetails(
        $uid,
        $page,
        $pageSize,
        int $type,
        $fetchService = true,
        $userType = DataStatus::USER_TYPE_AGENT,
        $groupId = 0
    ): DetailListReply {
        $uType = $userType == DataStatus::USER_TYPE_AGENT ?
            UserType::USER_TYPE_AGENT : UserType::USER_TYPE_DOCTOR;
        $req = new GetDetailListReq();
        $req->setUid($uid);
        $req->setPage($page);
        $req->setPageSize($pageSize);
        $req->setType($type);
        $req->setFetchService($fetchService);
        $req->setUserType($uType);
        $req->setGroupId($groupId);

        return self::getClient()->getDetailList($req);
    }

    /**
     * @return mixed|PointClient
     */
    protected static function getClient(): PointClient
    {
        return ApplicationContext::getContainer()->get(PointClient::class);
    }

    /**
     * 获取用户积分信息
     * @param $uid
     * @param int $userType
     * @return AssetsDetail
     */
    public static function getUserAssets($uid, $userType = DataStatus::USER_TYPE_AGENT, $groupId = 0): AssetsDetail
    {
        $uType = $userType == DataStatus::USER_TYPE_AGENT ? UserType::USER_TYPE_AGENT : UserType::USER_TYPE_DOCTOR;
        $request = new UidReq();
        $request->setUid($uid);
        $request->setUserType($uType);
        $request->setGroupId($groupId);

        return self::getClient()->getUserAssets($request);
    }

    /**
     * 获取商品详情
     * @param string $yyid
     * @return GoodsDetail
     */
    public static function getGoods(string $yyid): GoodsDetail
    {
        $req = new GoodsYYIDRequest();
        $req->setYyid($yyid);

        return self::getClient()->getGoods($req);
    }

    public static function bindWechat(Users $user, WechatUser $wechatUser, $userType = DataStatus::USER_TYPE_AGENT)
    {
        $uType = $userType == DataStatus::USER_TYPE_AGENT ? UserType::USER_TYPE_AGENT : UserType::USER_TYPE_DOCTOR;
        $request = new BindWechatReq();
        $request->setUid($user->uid);
        $request->setWechatUserID($wechatUser->id);
        $request->setUserType($uType);

        $client = self::getClient();
        $client->bindWechat($request);
    }

    public static function getDefaultGroup($platform = PointPlatform::PLATFORM_NEW_AGENT)
    {
        $req = new GetDefaultGroupRequest();
        $req->setPlatform($platform);
        return self::getClient()->getDefaultGroup($req);
    }

}
