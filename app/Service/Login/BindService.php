<?php
declare(strict_types=1);
namespace App\Service\Login;

use App\Constants\Auth;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\UserLoginLog;
use App\Model\Qa\Users;
use App\Repository\Login\IosRepository;
use App\Repository\Login\LoginForVersionRepository;
use App\Repository\Login\LoginRepository;
use App\Repository\OrgBindingUserRepository;
use App\Repository\UploadQiniuRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Service\Common\SmsService;
use Hyperf\DbConnection\Db;

class BindService
{
    public static function bindPhoneNumber($id, $mobile, $code, $unionid, $iosid, $ip)
    {
        //验证码判断
        if (!SmsService::verify($mobile, $code)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "验证码不正确");
        }

        if ($id!=0) {
            //有id 已生成账号 修改手机号
            $user = self::userChangePhone($id, $mobile);
        } else {
            // 没有id 账号还没生成 绑定手机号
            if ($unionid) {
                $user = self::wechatBindPhone($unionid, $mobile, $ip);
            } elseif ($iosid) {
                $user = self::iosBindPhone($iosid, $mobile, $ip);
            } else {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "参数id，unionid和iosid至少一个不能为空");
            }
        }

        // 判断手机号是否为供应商群主
        LoginService::checkSupplier($mobile, $user);

        return [
            'auth_status' => Auth::AUTH_STATUS_SUCCESS,
            'token' => LoginService::generateAuthInfo($user)
        ];
    }

    public static function iosBindPhone($iosid, $mobile, $ip)
    {
        $iosUser = IosRepository::getUserByIosid($iosid);
        if (!$iosUser) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "ios信息不存在");
        }
        $userInfo = LoginRepository::getUserByPhone($mobile);
        if ($userInfo) {
            //判断是不是已经有ios绑定
            $otherInfo = IosRepository::getUserByUserId($userInfo->uid);
            if ($otherInfo) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该苹果账号已被另一个账号绑定，请先解绑");
            }
            Db::beginTransaction();
            try {
                LoginRepository::updateIosBindState($userInfo->uid, Users::IS_BIND_IOS_1);
                IosRepository::updateUserIdByIosid($userInfo->uid, $iosid);
                LoginRepository::bindPhoneHistory($userInfo->uid, $mobile);
                LoginService::notFirstLogin($userInfo, $ip);
                Db::commit();
                return $userInfo;
            } catch (\Exception $exception) {
                Db::rollBack();
                throw $exception;
            }
        } else {
            Db::beginTransaction();
            try {
                $userModel = LoginRepository::createAccount($mobile);
                LoginRepository::updateIosBindState($userModel->uid, Users::IS_BIND_IOS_1);
                IosRepository::updateUserIdByIosid($userModel->uid, $iosid);
                LoginRepository::bindPhoneHistory($userModel->uid, $mobile);
                LoginService::notFirstLogin($userModel, $ip, UserLoginLog::IS_FIRST_LOGIN);
                Db::commit();
                return $userModel;
            } catch (\Exception $exception) {
                Db::rollBack();
                throw $exception;
            }
        }
    }

    public static function wechatBindPhone($unionid, $mobile, $ip)
    {
        //根据设备类型获取当前登录平台
        $authRole = Helper::getAuthRole();
        //根据当前登录平台获取用户类型（1-h5 3-app）
        $wechatType = Helper::getWechatTypeByRole($authRole);
        //获取微信用户
        $wechatUser = LoginRepository::getUserByUnionid($unionid, $wechatType);

        if (!$wechatUser) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "未绑定微信，请重新授权");
        }

        $userInfo = LoginRepository::getUserByPhone($mobile);
        if ($userInfo) {
            //判断是不是已经有微信绑定
//            $otherInfo = WechatUserRepository::getByYyid($userInfo->yyid);
            if ($wechatUser->u_yyid!=''&&
                $wechatUser->u_yyid!=$userInfo->yyid) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "手机号已被另一个账号绑定，请先解绑");
            }
            Db::beginTransaction();
            try {
                //修改绑定状态
                LoginRepository::updateWechatBindState($userInfo->uid, Users::IS_BIND_WECHAT_1);
                //修改wechat表u_yyid
                WechatUserRepository::bindUserByUnionId($unionid, $userInfo);
                //绑定手机记录
                LoginRepository::bindPhoneHistory($userInfo->uid, $mobile);
                //登录log记录
                LoginService::notFirstLogin($userInfo, $ip);
                Db::commit();
                return $userInfo;
            } catch (\Exception $exception) {
                Db::rollBack();
                throw $exception;
            }
        } else {
            Db::beginTransaction();
            try {
                //新增用户
                $userModel = LoginRepository::createAccount($mobile, $wechatUser);
                //修改绑定状态
                LoginRepository::updateWechatBindState($userModel->uid, Users::IS_BIND_WECHAT_1);
                //修改wechat表u_yyid
                WechatUserRepository::bindUserByUnionId($unionid, $userModel);
                //绑定手机记录
                LoginRepository::bindPhoneHistory($userModel->uid, $mobile);
                //登录log记录
                LoginService::notFirstLogin($userModel, $ip, UserLoginLog::IS_FIRST_LOGIN);
                Db::commit();
                return $userModel;
            } catch (\Exception $exception) {
                Db::rollBack();
                throw $exception;
            }
        }
    }

    public static function userChangePhone($id, $mobile)
    {
        //判断修改手机号和原手机号是否相同
        $userModel = UsersRepository::getUserByUid($id);
        if ($userModel&&
            $userModel->mobile_num&&
            $userModel->mobile_num==$mobile) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "不能和原手机号相同");
        }
        //判断要绑定的手机号是否已被别人占用
        $userInfo = LoginRepository::getUserByPhone($mobile);
        if ($userInfo) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "手机号已被另一个账号绑定，请先解绑");
        }
        Db::beginTransaction();
        try {
            LoginRepository::bindPhoneToUser($id, $mobile);
            LoginRepository::bindPhoneHistory($id, $mobile);
            Db::commit();
            return $userModel;
        } catch (\Exception $e) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "手机号绑定失败");
        }
    }

    public static function bindwechat($user, $unionid)
    {
        // 获取当前状态
        $userInfo = UsersRepository::getUserByUid($user->uid);
        if (!$userInfo) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户不存在");
        }
        if ($userInfo->is_bind_wechat==Users::IS_BIND_WECHAT_1) {
            // 解绑
            Db::beginTransaction();
            try {
                $wechatOne = WechatUserRepository::getByYyid($userInfo->yyid);
                //处理头像
                if ($userInfo->avatar_id <= 0) {
                    //解绑微信后 保留头像
                    $avatarId = UploadQiniuRepository::createWechatLoginoutHeadimg($user->uid, $wechatOne->headimgurl);
                    UsersRepository::updateHeadimg($user->uid, $avatarId);
                }
                // user表is_bind_wechat修改
                LoginRepository::updateWechatBindState($user->uid, Users::IS_BIND_WECHAT_0);
                $wechatInfo = LoginRepository::getByYyid($userInfo->yyid);
                // wechat表清空user_id
                LoginRepository::removeBindUserYyid(array_column($wechatInfo->toArray(), 'id'));
                // 历史表记录绑定历史
                LoginRepository::bindWechatHistory($user->uid, $wechatOne->unionid, Users::IS_BIND_WECHAT_0);
                Db::commit();
                return Users::IS_BIND_WECHAT_0;
            } catch (\Exception $e) {
                Db::rollBack();
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "微信解绑失败");
            }
        } else {
            //查找微信是否被绑定过
            $wechatInfo = LoginRepository::getUsersByUnionid($unionid);
            if ($wechatInfo->isEmpty()) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "未绑定微信，请重新授权");
            }
            $yyids = array_column($wechatInfo->toArray(), 'u_yyid');
            foreach ($yyids as $yyid) {
                if ($yyid) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该微信已被另一个账号绑定，请先解绑");
                }
            }
            // 绑定
            Db::beginTransaction();
            try {
                // user表is_bind_wechat修改
                LoginRepository::updateWechatBindState($user->uid, Users::IS_BIND_WECHAT_1);
                // wechat表写入user_id
                WechatUserRepository::bindUserByUnionId($unionid, $user);
                // 历史表记录绑定历史
                LoginRepository::bindWechatHistory($user->uid, $unionid, Users::IS_BIND_WECHAT_1);
                Db::commit();
                return Users::IS_BIND_WECHAT_1;
            } catch (\Exception $e) {
                Db::rollBack();
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "微信绑定失败");
            }
        }
    }

    public static function bindIos($id, $iosid)
    {
        // 获取当前状态
        $userInfo = UsersRepository::getUserByUid($id);
        if (!$userInfo) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户不存在");
        }
        if ($userInfo->is_bind_ios==Users::IS_BIND_IOS_1) {
            // 解绑
            Db::beginTransaction();
            try {
                // user表is_bind_ios修改
                LoginRepository::updateIosBindState($id, Users::IS_BIND_IOS_0);
                $iosInfo = IosRepository::getUserByUserId($id);
                // iosuser表清空user_id
                IosRepository::removeBindUserId($iosInfo->id);
                // 历史表记录绑定历史
                LoginRepository::bindIosHistory($id, $iosInfo->ios_id, Users::IS_BIND_IOS_0);
                Db::commit();
                return Users::IS_BIND_IOS_0;
            } catch (\Exception $e) {
                Db::rollBack();
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "ios解绑失败");
            }
        } else {
            //查找ios是否被绑定过
            $iosInfo = IosRepository::getUserByIosid($iosid);
            if (!$iosInfo) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "未绑定ios，请重新授权");
            }
            if ($iosInfo->user_id!=0) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该ios已被另一个账号绑定，请先解绑");
            }
            // 绑定
            Db::beginTransaction();
            try {
                // user表is_bind_ios修改
                LoginRepository::updateIosBindState($id, Users::IS_BIND_IOS_1);
                // iosuser表写入user_id
                IosRepository::updateUserIdByIosid($id, $iosid);
                // 历史表记录绑定历史
                LoginRepository::bindIosHistory($id, $iosid, Users::IS_BIND_IOS_1);
                Db::commit();
                return Users::IS_BIND_IOS_1;
            } catch (\Exception $e) {
                Db::rollBack();
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "ios绑定失败");
            }
        }
    }

    public static function deleteAccount($user)
    {
        Db::beginTransaction();
        try {
            // user表is_delete修改
            UsersRepository::deleteAccount($user->uid);
            // wechatuser表清除user_id
            WechatUserRepository::removeBindUserIdByUserId($user->yyid);
            // iosuser表清除user_id
            IosRepository::removeBindUserIdByUserId($user->uid);
            //清除组织架构里面绑定的人员
            OrgBindingUserRepository::deleteByUser($user);
            Db::commit();
            return '账号已注销';
        } catch (\Exception $e) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "账号注销失败");
        }
    }

    public static function checkBindState($unionid)
    {
        $wechatInfo = LoginRepository::getUsersByUnionid($unionid);
        if ($wechatInfo->isEmpty()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "未绑定微信，请重新授权");
        }
        $bindInfo = LoginRepository::getUsersByUnionidWithoutNull($unionid);
        if ($bindInfo) {
            $user = UsersRepository::getUserByYYID($bindInfo->u_yyid);
            return [
                'auth_status' => Auth::AUTH_STATUS_SUCCESS,
                'token' => LoginService::generateAuthInfo($user)
            ];
        } else {
            return [
                'auth_status' => Auth::AUTH_STATUS_BIND_MOBILE,
                'token' => ''
            ];
        }
    }

    /**
     * @param $version
     * @param $channel
     * @return array
     */
    public static function getLoginForCheckStatus($version, $channel)
    {
        $data = LoginForVersionRepository::getCheckStatus($version, $channel);
        if (!$data) {
            throw new BusinessException(ErrorCode::VERSION_NOT_EXIST);
        }
        return ['status' => $data->status];
    }

    /**
     * @param $phone
     * @param $password
     * @return array
     */
    public static function loginForCheck($phone, $password)
    {
        $check = LoginForVersionRepository::loginForCheck($phone, $password);
        $user = UsersRepository::getUserByUid($check->user_id);
        if (!$user) {
            throw new BusinessException(ErrorCode::DOCTOR_USER_NOT_EXISTS);
        }
        return [
            'auth_status' => Auth::AUTH_STATUS_SUCCESS,
            'token' => LoginService::generateAuthInfo($user)
        ];
    }
}
