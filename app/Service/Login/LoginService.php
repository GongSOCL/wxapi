<?php
declare(strict_types=1);
namespace App\Service\Login;

use App\Constants\Auth;
use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\Jwt;
use App\Model\Qa\UserLoginLog;
use App\Model\Qa\Users;
use App\Repository\Login\IosRepository;
use App\Repository\Login\LoginRepository;
use App\Repository\SupplierRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Service\Common\SmsService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;

class LoginService
{
    public static function authByPhone($phone, $code, $ip)
    {
        //验证码判断
        if (!SmsService::verify($phone, $code)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "验证码不正确");
        }

        // 根据手机号搜索账号
        $userInfo = LoginRepository::getUserByPhone($phone);
        if ($userInfo) {
            // 刷新登录信息
            self::notFirstLogin($userInfo, $ip);
            // 判断手机号是否为供应商群主
            self::checkSupplier($phone, $userInfo);
            return [
                'auth_status' => Auth::AUTH_STATUS_SUCCESS,
                'token' => self::generateAuthInfo($userInfo)
            ];
        } else {
            //账号不存在 新建账号
            Db::beginTransaction();
            try {
                $user = LoginRepository::createAccount($phone);
                self::notFirstLogin($user, $ip, UserLoginLog::IS_FIRST_LOGIN);
                // 判断手机号是否为供应商群主
                self::checkSupplier($phone, $user);
                Db::commit();
                return [
                    'auth_status' => Auth::AUTH_STATUS_SUCCESS,
                    'token' => self::generateAuthInfo($user)
                ];
            } catch (\Exception $exception) {
                Db::rollBack();
                throw $exception;
            }
        }
    }

    public static function checkSupplier($phone, $user)
    {
        $supplierInfo = SupplierRepository::getSupplierByPhone($phone);
        if ($supplierInfo) {
            $leadInfo = SupplierRepository::getSupplierLeader($supplierInfo->id);
            if ($leadInfo) {
                // 更新id和yyid
                SupplierRepository::updateLeaderInfo($leadInfo->id, $user);
            }
        }
    }

    public static function authByWechat($openid, $ip)
    {
        //根据设备类型获取当前登录平台
        $authRole = Helper::getAuthRole();
        //根据当前登录平台获取用户类型（1-h5 3-app）
        $wechatType = Helper::getWechatTypeByRole($authRole);
        //获取微信用户
        $wechatUser = WechatUserRepository::getOneWechatInfo($openid, $wechatType);

        if (!$wechatUser) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "未绑定微信，请重新授权");
        }

        if ($wechatUser->u_yyid) {
            //不是第一次登录，刷新登录数据
            $userInfo = UsersRepository::getUserByYYID($wechatUser->u_yyid);
            if ($userInfo) {
                // user表is_bind_wechat修改
                LoginRepository::updateWechatBindState($userInfo->uid, Users::IS_BIND_WECHAT_1);
                self::notFirstLogin($userInfo, $ip);
                return [
                    'auth_status' => Auth::AUTH_STATUS_SUCCESS,
                    'token' => self::generateAuthInfo($userInfo)
                ];
            } else {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "账号逻辑错误");
            }
        } else {
            //如果WeChatType为1（h5） 检查该openid的类型3（app） 有没有绑定用户
            //如果 WeChatType为3（app） 检查该openid的类型1（h5） 有没有绑定用户
            if ($wechatUser->unionid) {
                $relateType = self::getRelatedWechatType($wechatType);
                $bindWechat = LoginRepository::getOneWechatInfoByUnionId(
                    $wechatUser->unionid,
                    $relateType
                );
                if ($bindWechat) {
                    // 如果另一个人wechattype类型也绑定了用户手机 将该类型绑定yyid
                    WechatUserRepository::bindWechatUserByAnthorWechatUser($wechatUser, $bindWechat);
                    self::notFirstLogin(
                        UsersRepository::getUserByYYID($bindWechat->u_yyid),
                        $ip,
                        UserLoginLog::IS_FIRST_LOGIN
                    );
                    return [
                        'auth_status' => Auth::AUTH_STATUS_SUCCESS,
                        'token' => self::generateAuthInfo(UsersRepository::getUserByYYID($bindWechat->u_yyid))
                    ];
                } else {
                    // 如果另一个wechattype类型也没有绑定手机 返回提示去绑定手机
                    return [
                        'auth_status' => Auth::AUTH_STATUS_BIND_MOBILE,
                        'token' => ''
                    ];
                }
            } else {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "未绑定微信，请重新授权");
            }
        }
    }

    public static function appIosAuthInfo($iosid, $familyName)
    {
        if (IosRepository::getUserByIosid($iosid)) {
            return IosRepository::updateInfoByIosid($iosid, $familyName);
        } else {
            return IosRepository::createIosUser($iosid, $familyName);
        }
    }

    public static function loginByIos($iosid, $familyName, $ip)
    {
        $iosUser = IosRepository::getUserByIosid($iosid);
        if (!$iosUser) {
            IosRepository::createIosUser($iosid, $familyName);
            return [
                'auth_status' => Auth::AUTH_STATUS_BIND_MOBILE,
                'token' => ''
            ];
        }

        if ($iosUser->user_id&&$iosUser->user_id!=0) {
            $userInfo = UsersRepository::getUserByUid($iosUser->user_id);
            if ($userInfo) {
                self::notFirstLogin($userInfo, $ip);
                return [
                    'auth_status' => Auth::AUTH_STATUS_SUCCESS,
                    'token' => self::generateAuthInfo($userInfo)
                ];
            } else {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "账号逻辑错误");
            }
        } else {
            return [
                'auth_status' => Auth::AUTH_STATUS_BIND_MOBILE,
                'token' => ''
            ];
        }
    }

    public static function notFirstLogin($userInfo, $ip, $firstLogin = UserLoginLog::NOT_FIRST_LOGIN)
    {
        //根据设备类型获取当前登录平台
        $authRole = Helper::getAuthRole();
        //更新登陆时间和上次登录ip
        if (!LoginRepository::updateUserLoginInfo($userInfo->uid, $ip, $authRole, $firstLogin)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "更新登录信息失败");
        }
    }

    public static function generateAuthInfo(Users $user = null)
    {
        $token = "";
        if ($user) {
            $token = ApplicationContext::getContainer()
                ->get(Jwt::class)
                ->generate($user);
            if (!$token) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "微信登陆失败");
            }
        }
        return $token;
    }

    public static function getRelatedWechatType($wechatType): int
    {
        switch ($wechatType) {
            case DataStatus::WECHAT_AGENT:
                return DataStatus::WECHAT_APP;
            case DataStatus::WECHAT_APP:
                return DataStatus::WECHAT_AGENT;
            default:
                throw new BusinessException(ErrorCode::SERVER_ERROR, "暂时不不支持该类型微信");
        }
    }

    public static function checkMobileBind()
    {
        $user = Helper::getLoginUser();
        if (!($user->mobile_num && $user->mobile_verified == 1)) {
            return DataStatus::DELETE;
        } else {
            return DataStatus::REGULAR;
        }
    }
}
