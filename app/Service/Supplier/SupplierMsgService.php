<?php
declare(strict_types=1);

namespace App\Service\Supplier;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\Msg;
use App\Model\Qa\SupplierMember;
use App\Repository\AgentRepository;
use App\Repository\SupplierRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Service\Msg\MsgService;
use App\Service\Wechat\WechatBusiness;
use Grpc\Questionnaire\USER_TYPE;
use Hyperf\Utils\ApplicationContext;

class SupplierMsgService
{
    public static function sendJoinMsg($memberInfo, $userInfo)
    {
        //供应商信息 拿取供应商名称
        $supplierInfo = SupplierRepository::getSupplierInfoById($memberInfo->supplier_id);
        //当前成员的认证信息 用于获取成员的真名
        $agentInfo = AgentRepository::getAgentByUser($userInfo);
        //发给群主的信息
        $contentLeader = sprintf("%s已成功进入%s群", $agentInfo->truename, $supplierInfo->title);
        //发给成员本人的信息
        $contentMember = sprintf("恭喜您已成功进入%s群", $supplierInfo->title);
        //群主的微信信息
        $leaderWechatInfo = WechatUserRepository::getYouyaoByYyid(
            UsersRepository::getUserByUid($memberInfo->pid)->yyid
        );
        //该成员的微信信息
        $memberWechatInfo = WechatUserRepository::getYouyaoByYyid($memberInfo->u_yyid);

        //发送组长模板消息
        ApplicationContext::getContainer()
            ->get(WechatBusiness::class)
            ->sendSupplierGroupMsg(
                $contentLeader,
                $supplierInfo->title.'群',
                '已入群',
                '',
                [$leaderWechatInfo->openid]
            );

        //发送组员模板消息
        ApplicationContext::getContainer()
            ->get(WechatBusiness::class)
            ->sendSupplierGroupMsg(
                $contentMember,
                $supplierInfo->title.'群',
                '已入群',
                '',
                [$memberWechatInfo->openid]
            );

        //发送站内消息
        self::sendInOrOutGroupMsg(
            $memberInfo->pid,   //群组长的user_id
            [$userInfo->uid],   //该成员的user_id
            $contentLeader,
            $contentMember
        );
    }

    public static function sendRemoveMsg($oneInfo, $yyid, $memberId, $userId)
    {
        //供应商信息 拿取供应商名称
        $supplierInfo = SupplierRepository::getSupplierInfoById($oneInfo->supplier_id);
        //当前成员的认证信息 用于获取成员的真名
        $agentInfo = AgentRepository::getAgentName($yyid);
        //群主的微信信息
        $leaderWechatInfo = WechatUserRepository::getYouyaoByYyid(
            UsersRepository::getUserByUid($oneInfo->pid)->yyid
        );
        //该成员的微信信息
        $memberWechatInfo = WechatUserRepository::getYouyaoByYyid($yyid);
        if ($memberId==0) {
            //自己退出
            $contentLeader = sprintf("%s已成功退出%s群，其群内所有终端绑定关系即时失效，请知晓", $agentInfo->truename, $supplierInfo->title);
            $contentMember = sprintf("您已成功退出%s群，您群内的终端绑定关系即时失效，请知晓", $supplierInfo->title);
        } else {
            //组长移除
            $contentLeader = sprintf("%s已成功移出%s群，其群内所有终端绑定关系即时失效，请知晓", $agentInfo->truename, $supplierInfo->title);
            $contentMember = sprintf("您已被移出%s群，您群内的终端绑定关系即时失效，请知晓", $supplierInfo->title);
        }

        //发送组长模板消息
        ApplicationContext::getContainer()
            ->get(WechatBusiness::class)
            ->sendSupplierGroupMsg(
                $contentLeader,
                $supplierInfo->title.'群',
                '已出群',
                '',
                [$leaderWechatInfo->openid]
            );

        //发送组员模板消息
        ApplicationContext::getContainer()
            ->get(WechatBusiness::class)
            ->sendSupplierGroupMsg(
                $contentMember,
                $supplierInfo->title.'群',
                '已出群',
                '',
                [$memberWechatInfo->openid]
            );

        //发送站内消息
        self::sendInOrOutGroupMsg(
            $oneInfo->pid,  //组长的user_id
            [$userId],      //当前被移除的成员的user_id
            $contentLeader,
            $contentMember
        );
    }

    public static function sendDismissMsg($userId, $supplierId)
    {
        //组员的member信息
        $memberInfo = SupplierRepository::getMembersInfo($userId, $supplierId);
        //供应商信息
        $supplierInfo = SupplierRepository::getSupplierInfoById($supplierId);

        $contentLeader = sprintf("您已成功解散%s群，群内所有终端绑定关系即时失效，请知晓", $supplierInfo->title);
        $contentMember = sprintf("群主已解散%s群，您群内的终端绑定关系即时失效，请知晓", $supplierInfo->title);

        //群主的微信信息
        $leaderWechatInfo = WechatUserRepository::getYouyaoByYyid(
            UsersRepository::getUserByUid($userId)->yyid
        );
        //发送组长模板消息
        ApplicationContext::getContainer()
            ->get(WechatBusiness::class)
            ->sendSupplierGroupMsg(
                $contentLeader,
                $supplierInfo->title.'群',
                '已解散',
                '',
                [$leaderWechatInfo->openid]
            );
        if ($memberInfo->isNotEmpty()) {
            //该成员的微信信息
            $memberWechatInfo = WechatUserRepository::getByYyids(
                array_column(
                    UsersRepository::getUserByUids(
                        array_column($memberInfo->toArray(), 'user_id')
                    )->toArray(),
                    'yyid'
                )
            );

            //发送组员模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendSupplierGroupMsg(
                    $contentMember,
                    $supplierInfo->title.'群',
                    '已解散',
                    '',
                    array_unique(array_column($memberWechatInfo->toArray(), 'openid'))
                );

            //发送站内消息
            self::sendInOrOutGroupMsg(
                $userId,  //群主的userid
                array_column($memberInfo->toArray(), 'user_id'),
                $contentLeader,
                $contentMember
            );
        }
    }

    public static function sendBindMsg($oneInfo, $hospitalId, $memberId)
    {
        if ($oneInfo->is_leader==SupplierMember::NOT_LEADER) {
            //群主的认证信息用于获取真名
            $leadUserInfo = UsersRepository::getUserByUid($oneInfo->pid);
            $leaderInfo = AgentRepository::getAgentName($leadUserInfo->yyid);

            //组员的认证信息用于获取真名
            $memberInfo = AgentRepository::getAgentName($oneInfo->u_yyid);

            if ($memberId==0) {
                //自己加入
                $contentLeader = sprintf("%s的终端绑定已生效，点击查看详情（点击后跳转群主页）", $memberInfo->truename);
                $contentMember = sprintf("您的终端绑定已生效，点击查看详情（点击后跳转成员页）");
            } else {
                //组长分配
                $contentLeader = sprintf("%s的终端分配已成功，点击查看详情（点击后跳转群主页）", $memberInfo->truename);
                $contentMember = sprintf("%s已给您分配了终端，点击查看详情（点击后跳转成员页）", $leaderInfo->truename);
            }
            $links = SupplierMsgService::getBindLinks();

            //群主的微信信息
            $leaderWechatInfo = WechatUserRepository::getYouyaoByYyid($leadUserInfo->yyid);
            //该成员的微信信息
            $memberWechatInfo = WechatUserRepository::getYouyaoByYyid($oneInfo->u_yyid);

            //医院名称
            $hInfo = YouyaoHospitalRepository::getHospitalById($hospitalId);

            $date = date('Y-m-d', time());

            //发送组长模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendSupplierApplyMsg(
                    $contentLeader,
                    $hInfo->hospital_name,
                    $date,
                    '点击查看详情（点击后跳转群主页）',
                    [$leaderWechatInfo->openid],
                    $links['linkLeader']
                );

            //发送组员模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendSupplierApplyMsg(
                    $contentMember,
                    $hInfo->hospital_name,
                    $date,
                    '点击查看详情（点击后跳转成员页）',
                    [$memberWechatInfo->openid],
                    $links['linkMember']
                );

            //发送站内消息
            self::sendInOrOutGroupMsg(
                $oneInfo->pid,
                [$oneInfo->user_id],
                $contentLeader,
                $contentMember,
                $links['linkLeader'],
                $links['linkMember']
            );
        } else {
            //群主自己给自己绑定
            $memberInfo = AgentRepository::getAgentName($oneInfo->u_yyid);

            //自己加入
            $contentLeader = sprintf("%s的终端绑定已生效，点击查看详情（点击后跳转群主页）", $memberInfo->truename);

            $links = SupplierMsgService::getBindLinks();

            //该成员的微信信息
            $memberWechatInfo = WechatUserRepository::getYouyaoByYyid($oneInfo->u_yyid);

            //医院名称
            $hInfo = YouyaoHospitalRepository::getHospitalById($hospitalId);

            $date = date('Y-m-d', time());

            //发送组长模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendSupplierApplyMsg(
                    $contentLeader,
                    $hInfo->hospital_name,
                    $date,
                    '点击查看详情（点击后跳转群主页）',
                    [$memberWechatInfo->openid],
                    $links['linkLeader']
                );

            //发送站内消息
            self::sendInOrOutGroupMsg(
                $oneInfo->user_id,
                [],
                $contentLeader,
                '',
                $links['linkLeader']
            );
        }
    }

    public static function sendUnbindMsg($userYyid, $info, $userId)
    {
        //用于判断是组长解除还是自己解除
        $agentInfo = AgentRepository::getAgentName($userYyid);
        if ($agentInfo&&$agentInfo->is_supplier_leader==1) {
            //组长解除 $agentInfo是组长的认证信息
            //$userYyid $userId都是组长的
            //组员的认证信息
            $mInfo = SupplierRepository::getOneMemberInfoById($info[0]['member_id']);
            $memberInfo = AgentRepository::getAgentName($mInfo->u_yyid);
            $contentLeader = sprintf("%s的终端解除已成功，点击查看详情（点击后跳转群主页）", $memberInfo->truename);
            $contentMember = sprintf("%s解除了您的终端绑定", $agentInfo->truename);
            $links = SupplierMsgService::getBindLinks();

            //群主的微信信息
            $leaderWechatInfo = WechatUserRepository::getYouyaoByYyid($userYyid);
            //该成员的微信信息
            $memberWechatInfo = WechatUserRepository::getYouyaoByYyid($mInfo->u_yyid);

            $date = date('Y-m-d', time());

            //发送组长模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendSupplierUnbindMsg(
                    $contentLeader,
                    $date,
                    $agentInfo->truename,
                    '点击查看详情（点击后跳转群主页）',
                    [$leaderWechatInfo->openid],
                    $links['linkLeader']
                );

            //发送组员模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendSupplierUnbindMsg(
                    $contentMember,
                    $date,
                    $agentInfo->truename,
                    '',
                    [$memberWechatInfo->openid]
                );

            //发送站内消息
            SupplierMsgService::sendInOrOutGroupMsg(
                $userId,
                [$info[0]['user_id']],
                $contentLeader,
                $contentMember,
                $links['linkLeader']
            );
        } elseif ($agentInfo&&$agentInfo->is_supplier_leader==0) {
            //组员自己解除 $agentInfo是组员的认证信息
            //$userYyid $userId都是组员的
            //组员的member信息
            $memberInfo = SupplierRepository::getOneMemberInfoById($info[0]['member_id']);
            $contentLeader = sprintf("%s的终端绑定已解除，点击查看详情（点击后跳转群主页）", $agentInfo->truename);
            $contentMember = sprintf("您的终端绑定已解除");
            $links = SupplierMsgService::getBindLinks();

            //群主的微信信息
            $leaderWechatInfo = WechatUserRepository::getYouyaoByYyid(
                UsersRepository::getUserByUid($memberInfo->pid)->yyid
            );
            //该成员的微信信息
            $memberWechatInfo = WechatUserRepository::getYouyaoByYyid($userYyid);

            $date = date('Y-m-d', time());

            //发送组长模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendSupplierUnbindMsg(
                    $contentLeader,
                    $date,
                    $agentInfo->truename,
                    '点击查看详情（点击后跳转群主页）',
                    [$leaderWechatInfo->openid],
                    $links['linkLeader']
                );

            //发送组员模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendSupplierUnbindMsg(
                    $contentMember,
                    $date,
                    $agentInfo->truename,
                    '',
                    [$memberWechatInfo->openid]
                );

            //发送站内消息
            SupplierMsgService::sendInOrOutGroupMsg(
                $memberInfo->pid,
                [$userId],
                $contentLeader,
                $contentMember,
                $links['linkLeader']
            );
        } else {
            throw new BusinessException(ErrorCode::SUPPLIER_VERIFY_DATA_WORING);
        }
    }

    /**
     * @param $leaderId
     * @param array $memberIds
     * @param $contentLeader
     * @param $contentMember
     * @param string $linkLeader
     * @param string $linkMember
     */
    public static function sendInOrOutGroupMsg(
        $leaderId,
        array $memberIds,
        $contentLeader,
        $contentMember,
        $linkLeader = '',
        $linkMember = ''
    ) {
        //发送组长消息
        MsgService::sendInsideMsg(
            $leaderId,
            USER_TYPE::USER_TYPE_AGENT,
            '服务商群消息',
            $contentLeader,
            Msg::MSG_SUPPLIER_GROUP,
            $contentLeader,
            $linkLeader
        );

        foreach ($memberIds as $memberId) {
            //发送组员消息
            MsgService::sendInsideMsg(
                $memberId,
                USER_TYPE::USER_TYPE_AGENT,
                '服务商群消息',
                $contentMember,
                Msg::MSG_SUPPLIER_GROUP,
                $contentMember,
                $linkMember
            );
        }
    }

    public static function getBindLinks()
    {
        $env = env('APP_ENV', 'local');
        switch ($env) {
            case 'testing':
                $leaderLink = 'http://wx.qa.youyao99.com/uniapp/pages/supplier/memberList';
                $memberLink = 'http://wx.qa.youyao99.com/uniapp/pages/supplier/memberInfo';
                break;
            case 'staging':
                $leaderLink = 'http://wx.qa2.youyao99.com/uniapp/pages/supplier/memberList';
                $memberLink = 'http://wx.qa2.youyao99.com/uniapp/pages/supplier/memberInfo';
                break;
            case 'prod':
                $leaderLink = 'http://wx.youyao99.com/uniapp/pages/supplier/memberList';
                $memberLink = 'http://wx.youyao99.com/uniapp/pages/supplier/memberInfo';
                break;
            default:
                $leaderLink = '';
                $memberLink = '';
                break;
        }

        return [
            'linkLeader' => $leaderLink,
            'linkMember' => $memberLink
        ];
    }
}
