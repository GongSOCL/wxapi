<?php
declare(strict_types=1);

namespace App\Service\Supplier;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\SupplierMember;
use App\Repository\AgentRepository;
use App\Repository\DrugSeriesRepository;
use App\Repository\HospitalRepository;
use App\Repository\SupplierRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Service\Wechat\WechatBusiness;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

class SupplierService
{
    /**
     * @Inject()
     * @var WechatBusiness
     */
    protected $wechatBusiness;

    /**
     * @param $userYyid
     * @return int[]
     */
    public function getMemberRole($userYyid)
    {
        $agentInfo = SupplierRepository::getMemberRole($userYyid);
        $sid = $agentInfo ? $agentInfo->supplier_id : 0;
        if (!$agentInfo) {
            return ['role' => 5, 'supplier_id' => $sid];
        }
        if ($agentInfo&&$agentInfo->supplier_id!=0&&$agentInfo->is_supplier_leader==1) {
            return ['role' => 1, 'supplier_id' => $sid];
        }
        if ($agentInfo&&$agentInfo->supplier_id!=0&&$agentInfo->is_supplier_leader==0) {
            return ['role' => 2, 'supplier_id' => $sid];
        }
        return ['role' => 4, 'supplier_id' => $sid];
    }

    /**
     * @param $userId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getGroupList($userId, $page, $pageSize)
    {
        $data = SupplierRepository::getGroupList($userId, $page, $pageSize);
        $count = SupplierRepository::getGroupListCount($userId);

        $list = [];
        if ($data->isNotEmpty()) {
            $list = $data->toArray();
            foreach ($list as $key => $value) {
                $list[$key]['hospital_count'] = SupplierRepository::getMenberBindHospitalCount($value['member_id'])
                    ->count();
            }
        }

        return [
            'list' => $list,
            'count' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $userId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getJoinList($userId, $page, $pageSize)
    {
        $data = SupplierRepository::getJoinList($userId, $page, $pageSize);
        $count = SupplierRepository::getJoinListCount($userId);

        return [
            'list' => $data->isNotEmpty() ? $data->toArray() : [],
            'count' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $memberId
     */
    public function join($memberId)
    {
        //判断用户是否已经审核通过
        $memberInfo = SupplierRepository::getOneMemberInfoById($memberId);
        $supplierInfo = SupplierRepository::getSupplierInfoById($memberInfo->supplier_id);
        $userInfo = SupplierRepository::getUserAgent($memberInfo->u_yyid);
        if ($userInfo&&$userInfo->is_agent==2&&$userInfo->supplier_id!=0) {
            //判断是否是供应商成员
            $memberExist = SupplierRepository::getMemberInfo($userInfo->uid);
            if ($memberExist->isNotEmpty()) {
                throw new BusinessException(ErrorCode::MEMBER_EXIXTS);
            }
            //判断组长组员公司是否一致
            $leaderInfo = UsersRepository::getUserByUid($memberInfo->pid);
            if ($userInfo->company_yyid!=$leaderInfo->company_yyid) {
                throw new BusinessException(ErrorCode::NOT_SAME_COMPANY);
            }
            //判断组长组员的结算标识是否一致
            if ($supplierInfo->role_type!=$userInfo->real_role_type) {
                throw new BusinessException(ErrorCode::ROLE_TYPE_NOT_SAME);
            }
            SupplierRepository::memberVerifyOk($memberId, SupplierMember::STATUS_1);
            //发送模板消息 发送站内消息
            SupplierMsgService::sendJoinMsg($memberInfo, $userInfo);
        } else {
            throw new BusinessException(ErrorCode::USER_DONT_VERIFY);
        }
    }

    /**
     * @param $userId
     * @param $memberId
     * @throws \Exception
     */
    public function removeMember($userId, $memberId)
    {
        $memberId0 = $memberId;
        if ($memberId==0) {
            $oneInfo = SupplierRepository::getOneMemberInfoByUserId($userId);
            $memberId = $oneInfo->id;
        } else {
            $oneInfo = SupplierRepository::getOneMemberInfoById($memberId);
            $userId = $oneInfo->user_id;
        }
        $yyid = $oneInfo->u_yyid;
        Db::beginTransaction();
        try {
            //发送模板消息 发送站内消息
            SupplierMsgService::sendRemoveMsg(
                $oneInfo,  //当前被移除成员的member信息
                $yyid,     //当前被移除成员的yyid
                $memberId0, //判断是移除还是自己退出
                $userId
            );  //当前被移除成员的userid

            //解除server_s个人绑定的医院
            $bindInfo = SupplierRepository::getMemberDetailByMemberId($memberId);
            if ($bindInfo->isNotEmpty()) {
                foreach ($bindInfo->toArray() as $info) {
                    $user = UsersRepository::getUserByUid($info['user_id']);
                    $hospital = SupplierRepository::getHospitalById($info['hospital_id']);
                    $product = DrugSeriesRepository::getById($info['product_id']);
                    // 解除个人绑定关系
                    SupplierRepository::cancelServe($user, $hospital, $product);
                }
            }
            //认证信息agent去除供应商身份
            SupplierRepository::updateSupplierRole($yyid, 0, 0, 0);
            SupplierRepository::cancelBindMember($memberId);
            SupplierRepository::removeMember($memberId);

            Db::commit();
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }

    public function dismissGroup($userId)
    {
        Db::beginTransaction();
        try {
            // 获取该组member
            $memberInfo = SupplierRepository::getMemberInfo($userId);
            if ($memberInfo->isNotEmpty()) {
                $supplierId = $memberInfo->toArray()[0]['supplier_id'];
                $yyids = array_column($memberInfo->toArray(), 'u_yyid');
                $members = array_column($memberInfo->toArray(), 'id');

                //发送模板消息 发送站内消息 user_id群主的userid
                SupplierMsgService::sendDismissMsg($userId, $supplierId);

                //解除server_s个人绑定的医院
                $bindInfo = SupplierRepository::getMemberDetailByMemberIds($members);
                if ($bindInfo->isNotEmpty()) {
                    foreach ($bindInfo->toArray() as $info) {
                        $user = UsersRepository::getUserByUid($info['user_id']);
                        $hospital = SupplierRepository::getHospitalById($info['hospital_id']);
                        $product = DrugSeriesRepository::getById($info['product_id']);
                        // 解除个人绑定关系
                        SupplierRepository::cancelServe($user, $hospital, $product);
                    }
                }
                //认证信息去除供应商身份
                SupplierRepository::updateSupplierRoles($yyids);
                SupplierRepository::cancelBindMembers($members, $supplierId);
                SupplierRepository::dismissGroup($supplierId);
                SupplierRepository::deleteSupplier($supplierId);
            }
            Db::commit();
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $userYyid
     * @param $userId
     * @return array
     */
    public function getLeaderCode($userYyid, $userId)
    {
        $agentInfo = SupplierRepository::getMemberRole($userYyid);
        $supplierInfo = SupplierRepository::getOneMemberInfoByUserId($userId);
        $aid = $agentInfo ? $agentInfo->aid : 0;
        $sid = $supplierInfo ? $supplierInfo->supplier_id : 0;
        return [
            'supplier_id' => $sid,
            'invite_aid' => $aid
        ];
    }

    /**
     * @param $userYyid
     * @param $userId
     * @param $supplierId
     * @param $aid
     * @return array
     * @throws \Exception
     */
    public function addJoinList($userYyid, $userId, $supplierId, $aid)
    {
        Db::beginTransaction();
        try {
            $inviteInfo = SupplierRepository::getAgentInfo($aid);
            $supplierInfo = SupplierRepository::getSupplierInfoById($supplierId);
            $memberInfo = SupplierRepository::getOneMemberInfo($supplierId, $userId);
            if (!$memberInfo) {
                // 同时添加供应商待审核成员信息
                $mid = SupplierRepository::addJoinMember($supplierId, $userId, $inviteInfo->uid, $userYyid);
                //认证信息agent添加供应商身份
                SupplierRepository::updateSupplierRole($userYyid, $supplierId, $supplierInfo->type, 0);
            } else {
                $mid = $memberInfo->id;
            }

            Db::commit();

            return ['member' => $mid];
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $userId
     * @param $memberId
     * @return array
     */
    public function getMemberDetail($userId, $memberId)
    {
        if ($memberId==0) {
            $oneInfo = SupplierRepository::getOneMemberInfoByUserId($userId);
            $memberId = $oneInfo ? $oneInfo->id : 0;
        } else {
            $oneInfo = SupplierRepository::getOneMemberInfoById($memberId);
        }

        $a = null;
        $w = null;
        $s = null;
        if ($oneInfo) {
            $userYyid = $oneInfo->u_yyid;
            $supplierId = $oneInfo->supplier_id;
            $a = AgentRepository::getAgentName($userYyid);
            $w = WechatUserRepository::getYouyaoByYyid($userYyid);
            $s = SupplierRepository::getSupplierInfoById($supplierId);
        }

        $detailInfo = SupplierRepository::getMemberDetail($memberId);

        return [
            'true_name' => $a ? $a->truename : '',
            'heading' => $w ? $w->headimgurl : '',
            'supplier_title' => $s ? $s->title : '',
            'bind_info' => $detailInfo->isNotEmpty() ? $detailInfo->toArray() : []
        ];
    }

    /**
     * @param $bindId
     * @param $userId
     * @param $userYyid
     * @return mixed
     * @throws \Exception
     */
    public function unbindMember($bindId, $userId, $userYyid)
    {
        Db::beginTransaction();
        try {
            $allIds = [];
            $info = SupplierRepository::getMemberDetailById($bindId);
            if ($info->isNotEmpty()) {
                $info = $info->toArray();
                $memberInfo = SupplierRepository::getOneMemberInfoById($info[0]['member_id']);
                $supplierId = $memberInfo->supplier_id;
                if ($memberInfo->is_leader==SupplierMember::IS_LEADER) {
                    foreach ($info as $item) {
                        $detailForMember = SupplierRepository::getDetailByHospital($supplierId, $item);
                        if ($detailForMember->isNotEmpty()) {
                            foreach ($detailForMember->toArray() as $did) {
                                array_push($allIds, $did);
                            }
                        }
                    }
                    $allInfo = SupplierRepository::getMemberDetailById($allIds);
                    // 同时解除供应商下的医院
                    foreach ($allInfo as $one) {
                        SupplierRepository::removeHospitalProduct($supplierId, $one['hospital_id'], $one['product_id']);
                    }
                } else {
                    $allIds = $bindId;
                    $allInfo = $info;
                }
                foreach ($allInfo as $in) {
                    $user = UsersRepository::getUserByUid($in['user_id']);
                    $hospital = SupplierRepository::getHospitalById($in['hospital_id']);
                    $product = DrugSeriesRepository::getById($in['product_id']);

                    // 解除个人绑定关系
                    SupplierRepository::cancelServe($user, $hospital, $product);
                }

                // 解除供应商绑定关系
                SupplierRepository::unbindMember($allIds);

                //发送模板消息 发送站内消息
                SupplierMsgService::sendUnbindMsg(
                    $userYyid,  //当前操作人员的yyid
                    $allInfo,     //当前操作的memberdetail信息
                    $userId
                );  //当前操作的userid
            }
            Db::commit();
            return $allIds;
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $memberId
     * @param $keyword
     * @return array
     */
    public function hospitalProductForBind($memberId, $keyword)
    {
        $occupyId = [];
        $data = [];
        // 获取供应商
        $info = SupplierRepository::getOneMemberInfoById($memberId);
        if (!$info) {
            throw new BusinessException(ErrorCode::SUPPLIER_NOT_EXISTS);
        }

        // 获取当前我绑定的医院和药品
        $occupy = SupplierRepository::getMemberDetails($info->supplier_id, $info->id, $info->user_id);
        if ($occupy->isNotEmpty()) {
            foreach ($occupy as $item) {
                $hAndP = SupplierRepository::getOccupyHospital($info->supplier_id, $item);
                if ($hAndP) {
                    array_push($occupyId, $hAndP->id);
                }
            }
        }

        if ($keyword) {
            $hospital = SupplierRepository::getHospitalByName($keyword);
            $hid = $hospital->isNotEmpty() ? $hospital->toArray() : [];
            $product = SupplierRepository::getProductByName($keyword);
            $pid = $product->isNotEmpty() ? $product->toArray() : [];
            $hospitalProduct = SupplierRepository::searchSupplierHospitalProduct(
                $info->supplier_id,
                $hid,
                $pid,
                $occupyId);
        } else {
            $hospitalProduct = SupplierRepository::getSupplierHospitalProduct($info->supplier_id, $occupyId);
        }

        if ($hospitalProduct->isNotEmpty()) {
            foreach ($hospitalProduct->toArray() as $key => $value) {
                $data[$key]['hospital_id'] = $value['hospital_id'];
                $data[$key]['hospital_name'] = HospitalRepository::getHospitalById($value['hospital_id'])
                    ->hospital_name;
                $data[$key]['product_id'] = $value['product_id'];
                $data[$key]['product_name'] = SupplierRepository::getProduct($value['product_id'])->series_name;
            }
        }

        return $data;
    }

    /**
     * @param $userId
     * @param $memberId
     * @param $productId
     * @param $hospitalId
     */
    public function bindMember($userId, $memberId, $productId, $hospitalId)
    {
        if ($memberId==0) {
            $oneInfo = SupplierRepository::getOneMemberInfoByUserId($userId);
        } else {
            $oneInfo = SupplierRepository::getOneMemberInfoById($memberId);
        }
        if (!$oneInfo) {
            throw new BusinessException(ErrorCode::SUPPLIER_NOT_EXISTS);
        }
        Db::beginTransaction();
        try {
            $supplierExist = SupplierRepository::getMember(
                $oneInfo->supplier_id,
                $oneInfo->id,
                $oneInfo->user_id,
                $productId,
                $hospitalId
            );

            if (!$supplierExist) {
                SupplierRepository::bindMember(
                    $oneInfo->supplier_id,
                    $oneInfo->id,
                    $oneInfo->user_id,
                    $productId,
                    $hospitalId
                );
            }

            $user = UsersRepository::getUserByUid($oneInfo->user_id);
            $hospital = SupplierRepository::getHospitalById($hospitalId);
            $product = DrugSeriesRepository::getById($productId);

            $exist = SupplierRepository::getHospitalServe($user, $hospital, $product);

            if (!$exist) {
                //绑定至个人
                SupplierRepository::addServe(
                    $user,
                    $hospital,
                    $product,
                    888,
                    1
                );
            }

            //发送模板消息 发送站内消息
            SupplierMsgService::sendBindMsg(
                $oneInfo,  //当前绑定的成员的member信息
                $hospitalId,
                $memberId
            ); //判断组长分配还是组员自己申请

            Db::commit();
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }
}
