<?php
declare(strict_types=1);

namespace App\Service\Supplier;

use App\Constants\Code\ErrorCodeDoctor;
use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Exception\DoctorException;
use App\Kernel\Minio\Minio;
use App\Model\Qa\SupplierMember;
use App\Repository\HospitalRepository;
use App\Repository\SupplierRepository;
use App\Repository\UsersRepository;
use Grpc\Msg\MSG_BIZ_TYPE;
use Grpc\Msg\MSG_PRIORITY;
use Hyperf\Utils\ApplicationContext;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Youyao\Framework\Components\Msg\MessageRpcClient;

class ReportService
{
    /**
     * @param $supplierId
     * @param $userId
     * @param $date
     * @return array|mixed
     */
    public function getReport($supplierId, $userId, $date)
    {
        // 判断用户是否为管理员
        $supplierMember = SupplierRepository::getSupplierMemberBySupplierId($supplierId, $userId);
        if (!$supplierMember) {
            throw new BusinessException(ErrorCode::USER_IS_NOT_SUPPLIER);
        }
        if ($supplierMember->is_leader==SupplierMember::IS_LEADER) {
            // 组长查看全部成员
            $members = SupplierRepository::getMemberInfoInGroup($userId);
            $ids = array_unique(array_column($members->toArray(), 'user_id'));
            return $this->getMembersReport($ids, $date);
        } else {
            // 成员个人
            return $this->getOneReport($userId, $date);
        }
    }

    /**
     * @param $userId
     * @param $date
     * @return mixed
     */
    public function getOneReport($userId, $date)
    {
        $userInfo = UsersRepository::getUserAgent($userId);
        $data['true_name'] = $userInfo->truename;
        $data['h'] = 0;
        $data['s'] = 0;
        $data['b'] = 0;
        $data['e'] = 0;
        $data['type_1'] = 0;
        $data['type_2'] = 0;
        $data['type_3'] = 0;
        $data['type_4'] = 0;
        $data['type_5'] = 0;
        $visitInfo = SupplierRepository::getVisitInfoByUserId($userId, $date);
        if ($visitInfo->isNotEmpty()) {
            foreach ($visitInfo->toArray() as $item) {
                $doctorCount = SupplierRepository::getVisitCountById($item['id']);
                if ($doctorCount==0) {
                    $doctorCount = 1;
                }
                if ($item['visit_type']==1) {
                    $data['type_1'] += $doctorCount;
                }
                if ($item['visit_type']==2) {
                    $data['type_2'] += $doctorCount;
                }
                if ($item['visit_type']==3) {
                    $data['type_3'] += $doctorCount;
                }
                if ($item['visit_type']==4) {
                    $data['type_4'] += $doctorCount;
                }
                if ($item['visit_type']==5) {
                    $data['type_5'] += $doctorCount;
                }
                $hospInfo = SupplierRepository::getHospitalById($item['hospital_id']);
                if ($hospInfo&&$hospInfo->org_type==1) {
                    $data['h'] += $doctorCount;
                } elseif ($hospInfo&&$hospInfo->org_type==2) {
                    $data['s'] += $doctorCount;
                } elseif ($hospInfo&&$hospInfo->org_type==3) {
                    $data['b'] += $doctorCount;
                } else {
                    $data['e'] += $doctorCount;
                }
            }
        }

        return $data;
    }

    /**
     * @param $ids
     * @param $date
     * @return array
     */
    public function getMembersReport($ids, $date)
    {
        $usersInfo = [];
        $usersModel = UsersRepository::getUserAgents($ids);
        if ($usersModel->isNotEmpty()) {
            foreach ($usersModel->toArray() as $row) {
                $usersInfo[$row['uid']] = $row;
            }
        }
        $visitsInfo = [];
        $visitsModel = SupplierRepository::getVisitInfosByUserId($ids, $date);
        if ($visitsModel->isNotEmpty()) {
            foreach ($visitsModel->toArray() as $row) {
                $visitsInfo[$row['user_id']][] = $row;
            }
        }

        $hVisit = 0;
        $sVisit = 0;
        $bVisit = 0;
        $eVisit = 0;
        $data = [];
        foreach ($ids as $k => $id) {
            $data[$k]['true_name'] = $usersInfo[$id]['truename'];
            $data[$k]['h'] = 0;
            $data[$k]['s'] = 0;
            $data[$k]['b'] = 0;
            $data[$k]['e'] = 0;
            if (array_key_exists($id, $visitsInfo)&&count($visitsInfo[$id])>0) {
                foreach ($visitsInfo[$id] as $item) {
                    $doctorCount = SupplierRepository::getVisitCountById($item['id']);
                    if ($doctorCount==0) {
                        $doctorCount = 1;
                    }
                    $hospInfo = SupplierRepository::getHospitalById($item['hospital_id']);
                    if ($hospInfo&&$hospInfo->org_type==1) {
                        $data[$k]['h'] += $doctorCount;
                        $hVisit += $doctorCount;
                    } elseif ($hospInfo&&$hospInfo->org_type==2) {
                        $data[$k]['s'] += $doctorCount;
                        $sVisit += $doctorCount;
                    } elseif ($hospInfo&&$hospInfo->org_type==3) {
                        $data[$k]['b'] += $doctorCount;
                        $bVisit += $doctorCount;
                    } else {
                        $data[$k]['e'] += $doctorCount;
                        $eVisit += $doctorCount;
                    }
                }
            }
        }

        return [
            'total' => $hVisit + $sVisit + $bVisit + $eVisit,
            'h' => $hVisit,
            's' => $sVisit,
            'b' => $bVisit,
            'e' => $eVisit,
            'list' => $data
        ];
    }

    public function exportReport($supplierId, $userId, $date, $email)
    {
        // 判断用户是否为管理员
        $supplierMember = SupplierRepository::getSupplierMemberBySupplierId($supplierId, $userId);
        if (!$supplierMember) {
            throw new BusinessException(ErrorCode::USER_IS_NOT_SUPPLIER);
        }
        if ($supplierMember->is_leader==SupplierMember::IS_LEADER) {
            // 组长查看全部成员
            $members = SupplierRepository::getMemberInfoInGroup($userId);
            $ids = array_unique(array_column($members->toArray(), 'user_id'));
            return $this->exportMembersReport($supplierMember->supplier_id, $ids, $date, $email);
        } else {
            // 成员个人
            return $this->exportMembersReport($supplierMember->supplier_id, [$userId], $date, $email);
        }
    }

    public function exportMembersReport($supplierId, $ids, $date, $email)
    {
        // 供应商信息相同
        $supplierInfo = SupplierRepository::getSupplierInfoById($supplierId);
        //科室信息
        $departArr = $this->getAllDepartInfo();
        $doctorArr = $this->getAllDoctorInfo();
        $drugArr = $this->getDrugInfo();

        $visitInfo = SupplierRepository::getVisitDetailByUserIds($ids, $date);
        if ($visitInfo->isEmpty()) {
            throw new BusinessException(ErrorCode::EMPTY_SUPPLIER_VISIT_INFO);
        }
        $visitInfo = $visitInfo->toArray();
        foreach ($visitInfo as $k => $item) {
            // 用户信息
            $userInfo = UsersRepository::getUserByUid($item['user_id']);
            $visitInfo[$k]['t_n'] = SupplierRepository::getMemberRole($userInfo->yyid)->truename;
            // 供应商信息
            $visitInfo[$k]['s_id'] = $supplierId;
            $visitInfo[$k]['s_n'] = $supplierInfo->title;
            //医院信息
            $hospInfo = SupplierRepository::getHospitalById($item['hospital_id']);
            if ($hospInfo) {
                $hospitalCInfo = HospitalRepository::getHospitalCInfo($hospInfo->yyid);
                if ($hospitalCInfo) {
                    $visitInfo[$k]['h_o'] = $hospitalCInfo->hospital_no;
                    $visitInfo[$k]['h_n'] = $hospitalCInfo->hospital_name_cn;
                } else {
                    $visitInfo[$k]['h_o'] = $hospInfo->id;
                    $visitInfo[$k]['h_n'] = $hospInfo->hospital_name;
                }
            } else {
                $visitInfo[$k]['h_o'] = '';
                $visitInfo[$k]['h_n'] = '';
            }
            $visitInfo[$k]['h_t'] = $hospInfo->org_type ?? 0;
            $visitInfo[$k]['h_l'] = $hospInfo->level ?? '';
            $visitInfo[$k]['h_p'] = $hospInfo->pro ?? '';
            $visitInfo[$k]['h_c'] = $hospInfo->city ?? '';
            $area = HospitalRepository::getArea($visitInfo[$k]['h_p']);
            $visitInfo[$k]['h_a'] = $area ? $area->area : '';
            //科室
            if ($item['depart_id']) {
                $visitInfo[$k]['d'] = trim(
                    implode(
                        '，',
                        $this->matchDepartInfo($departArr, $item['depart_id'])
                    ),
                    '，'
                );
            } else {
                $visitInfo[$k]['d'] = '';
            }
            //医生
            if ($item['doctor_id']) {
                $visitInfo[$k]['do'] = trim(
                    implode(
                        '，',
                        $this->matchDoctorInfo($doctorArr, $item['doctor_id'])
                    ),
                    '，'
                );
            } else {
                $visitInfo[$k]['do'] = '';
            }
            //产品
            $visitInfo[$k]['se'] = trim(
                implode(
                    '，',
                    $this->matchDrugInfo($drugArr, $item['id'])
                ),
                '，'
            );
            //时长
            if ($item['check_in_time']&&$item['check_out_time']) {
                $visitInfo[$k]['last_time'] = round((
                    strtotime($item['check_out_time']) - strtotime($item['check_in_time'])) /60);
            } else {
                $visitInfo[$k]['last_time'] = '';
            }

            switch ($visitInfo[$k]['h_t']) {
                case 1:
                    $visitInfo[$k]['h_t_n'] = '医院拜访';
                    break;
                case 2:
                    $visitInfo[$k]['h_t_n'] = '药店拜访';
                    break;
                case 3:
                    $visitInfo[$k]['h_t_n'] = '商业拜访';
                    break;
                default:
                    $visitInfo[$k]['h_t_n'] = '';
                    break;
            }

            switch ($item['visit_type']) {
                case 1:
                    $visitInfo[$k]['v_t_n'] = '面对面拜访';
                    break;
                case 2:
                    $visitInfo[$k]['v_t_n'] = '线上拜访';
                    break;
                case 3:
                    $visitInfo[$k]['v_t_n'] = '内部会议';
                    break;
                case 4:
                    $visitInfo[$k]['v_t_n'] = '外部会议';
                    break;
                case 5:
                    $visitInfo[$k]['v_t_n'] = '行政类工作';
                    break;
                default:
                    $visitInfo[$k]['v_t_n'] = '';
                    break;
            }

            if ($item['is_collaborative']===1) {
                $visitInfo[$k]['i_c_c'] = '是';
            } elseif ($item['is_collaborative']===2) {
                $visitInfo[$k]['i_c_c'] = '否';
            } else {
                $visitInfo[$k]['i_c_c'] = '未选择';
            }
        }

        //生成文件
        $file = $this->createMemberExcel($visitInfo);
        //发送邮件
        if ($file['fileUri']) {
            go(function () use ($file, $email) {
                self::sendCheckout($file, $email);
            });
            return ['res' => '邮件发送成功'];
        } else {
            throw new DoctorException(ErrorCodeDoctor::NO_REPORT);
        }
    }

    private static function sendCheckout(array $file, $email): void
    {
        //上传文件到minio存储
        $uploadPath = sprintf("/checkout/%s/%s_%s.xlsx", date('Ymd'), $file['fileName'], uniqid());
        self::uploadFileToMinio($file['fileUri'], $uploadPath, $file['fileName'] . ".xlsx");
        $url = Minio::createPublicBucketFileLink(env('S3_BUCKET'), $uploadPath);

        //发送邮件消息
        self::sendMailMsg(
            $email,
            '服务商拜访报表',
            '服务商拜访报表，详情见附件',
            [
                [$file['fileName'] . ".xlsx", $url]
            ]
        );
    }

    private static function sendMailMsg($mail, $subject, $body, array $attachment)
    {
        //上传文件
        ApplicationContext::getContainer()
            ->get(MessageRpcClient::class)
            ->sendMail(
                [$mail],
                $subject,
                $body,
                false,
                MSG_BIZ_TYPE::BIZ_COMMON_TYPE,
                '',
                MSG_PRIORITY::PRIORITY_MIDDLE,
                false,
                '',
                false,
                '',
                $attachment
            );
    }

    private static function uploadFileToMinio(
        $filePath,
        $uploadPath,
        $fileName,
        $contentType = "application/vnd.ms-excel"
    ) {
        $fs = Minio::getFileSystem("minio");
        if ($fs->has($uploadPath)) {
            $fs->delete($uploadPath);
        }
        $stream = fopen($filePath, 'r+');
        $fs->writeStream($uploadPath, $stream, [
            'ContentType' => $contentType,
            'ContentDisposition' => "attachment; filename=\"$fileName\""
        ]);
        fclose($stream);
    }

    public function createMemberExcel($data)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('sheet1');
        $title = ['拜访ID','服务类型','服务商id','服务商名称','姓名','手机号',
            '拜访类型','签入时间', '签出时间','拜访时长（分钟）',
            '拜访医院/药店/商业ID','拜访医院/药店/商业名称','医院等级','省','市','区','科室','医生','沟通产品',
            '签入经度','签入纬度','签入地址','签入超出原因','签入备注',
            '签出经度','签出纬度','签出地址','签出超出原因','签出备注',
            '拜访结果','后续跟进事项', '是否协访'];
        foreach ($title as $key => $value) {
            $sheet->setCellValueByColumnAndRow($key + 1, 1, $value);
        }

        $row = 2;
        foreach ($data as $item) {
            $sheet->setCellValue('A'.$row, $item['id']);
            $sheet->setCellValue('B'.$row, $item['h_t_n']);
            $sheet->setCellValue('C'.$row, $item['s_id']);
            $sheet->setCellValue('D'.$row, $item['s_n']);
            $sheet->setCellValue('E'.$row, $item['t_n']);
            $sheet->setCellValue('F'.$row, $item['mobile_num']);
            $sheet->setCellValue('G'.$row, $item['v_t_n']);
            $sheet->setCellValue('H'.$row, $item['check_in_time']);
            $sheet->setCellValue('I'.$row, $item['check_out_time']);
            $sheet->setCellValue('J'.$row, $item['last_time']);
            $sheet->setCellValue('K'.$row, $item['h_o']);
            $sheet->setCellValue('L'.$row, $item['h_n']);
            $sheet->setCellValue('M'.$row, $item['h_l']);
            $sheet->setCellValue('N'.$row, $item['h_p']);
            $sheet->setCellValue('O'.$row, $item['h_c']);
            $sheet->setCellValue('P'.$row, $item['h_a']);
            $sheet->setCellValue('Q'.$row, $item['d']);
            $sheet->setCellValue('R'.$row, $item['do']);
            $sheet->setCellValue('S'.$row, $item['se']);
            $sheet->setCellValue('T'.$row, $item['check_in_longitude']);
            $sheet->setCellValue('U'.$row, $item['check_in_latitude']);
            $sheet->setCellValue('V'.$row, $item['check_in_pos']);
            $sheet->setCellValue('W'.$row, $item['check_in_range_reason']);
            $sheet->setCellValue('X'.$row, $item['check_in_geo_comment']);
            $sheet->setCellValue('Y'.$row, $item['check_out_longitude']);
            $sheet->setCellValue('Z'.$row, $item['check_out_latitude']);
            $sheet->setCellValue('AA'.$row, $item['check_out_pos']);
            $sheet->setCellValue('AB'.$row, $item['check_out_range_reason']);
            $sheet->setCellValue('AC'.$row, $item['check_out_geo_comment']);
            $sheet->setCellValue('AD'.$row, $item['visit_result']);
            $sheet->setCellValue('AE'.$row, $item['next_visit_purpose']);
            $sheet->setCellValue('AF'.$row, $item['i_c_c']);
            $row++;
        }
        $fileName = '服务商报表' . date('YmdHis', time()) . rand(100000, 999999);
        $writer = new Xlsx($spreadsheet);
        $fileUri = DataStatus::SUPPLIER_REPORT_URI . $fileName . ".xlsx";
        $writer->save($fileUri);

        return [
            'fileUri' => $fileUri,
            'fileName' => $fileName
        ];
    }

    public function matchDrugInfo($drugArr, $visitId)
    {
        $data = [];
        $series = SupplierRepository::getSeriesByVisitId($visitId);
        if ($series->isNotEmpty()) {
            $seriesIds = array_column($series->toArray(), 'series_id');
            foreach ($seriesIds as $value) {
                if (array_key_exists($value, $drugArr)) {
                    $data[] = $drugArr[$value]['series_name'];
                }
            }
        }
        return $data;
    }

    public function getDrugInfo()
    {
        $drugArr = [];
        $drugInfo = HospitalRepository::getSeriesInfo();
        if ($drugInfo->isNotEmpty()) {
            foreach ($drugInfo->toArray() as $value) {
                $k = $value['id'];
                $drugArr[$k] = $value;
            }
        }
        return $drugArr;
    }

    public function matchDoctorInfo($doctorArr, $doctorIds)
    {
        $data = [];
        $doctorIds = array_unique(explode(';', $doctorIds));
        foreach ($doctorIds as $value) {
            if (array_key_exists($value, $doctorArr)) {
                $data[] = $doctorArr[$value]['true_name'];
            }
        }
        return $data;
    }

    public function getAllDoctorInfo()
    {
        $doctorArr = [];
        $doctorInfo = HospitalRepository::getAllDoctor();
        if ($doctorInfo->isNotEmpty()) {
            foreach ($doctorInfo->toArray() as $value) {
                $k = $value['id'];
                $doctorArr[$k] = $value;
            }
        }
        return $doctorArr;
    }

    public function matchDepartInfo($departArr, $departIds)
    {
        $data = [];
        $departIds = array_unique(explode(';', $departIds));
        foreach ($departIds as $value) {
            if (array_key_exists($value, $departArr)) {
                $data[] = $departArr[$value]['name'];
            }
        }
        return $data;
    }

    public function getAllDepartInfo()
    {
        $departArr = [];
        $departInfo = HospitalRepository::getAllDepart();
        if ($departInfo->isNotEmpty()) {
            foreach ($departInfo->toArray() as $value) {
                $k = $value['id'];
                $departArr[$k] = $value;
            }
        }
        return $departArr;
    }
}
