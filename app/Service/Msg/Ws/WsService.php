<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/sdc-crawler.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Service\Msg\Ws;

use App\Helper\Helper;
use GuzzleHttp\Client;

class WsService
{
    const SEND_USER_MSG_URI = '/api/send/user-msg';

    const SEND_USER_CMD_URI = '/api/send/user-cmd';

    /**
     * 通过ws通知客户端用户, 目前仅支持admin/admin-dealer-manage.
     *
     * @param $platform
     * @param $uid
     * @param $msg
     * @param $code
     * @param $type
     * @param $title
     */
    protected static function noticeUser($platform, $uid, $msg, $code = 500, $type = 'error', $title = '通知')
    {
        if ($uid <= 0) {
            return;
        }

        $cli = self::getClient();
        if (!$cli) {
            return;
        }
        $log = Helper::getLogger();

        try {
            $resp = $cli->post(self::SEND_USER_MSG_URI, [
                'json' => [
                    'uid' => $uid,
                    'platform' => $platform,
                    'data' => [
                        'biz' => 1,
                        'msg' => $msg,
                        'type' => $type,
                        'code' => $code,
                        'title' => $title,
                    ],
                ],
            ]);

            if ($resp->getStatusCode() != 200) {
                $log->error('send_ws_user_msg_error', [
                    'http_code' => $resp->getStatusCode(),
                    'reason' => $resp->getReasonPhrase(),
                ]);
                return;
            }

            $return = json_decode($resp->getBody()->getContents(), true);
            if (! (isset($return['code'], $return['msg']) && $return['code'] == 0)) {
                $log->error('send_user_msg_fail', [
                    'body' => $resp->getBody()->getContents(),
                ]);
                return;
            }
        } catch (\Throwable $e) {
            $log->error('notice_admin_error', [
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
        }
    }

    /**
     * 通过ws通知客户端任务完成文件列表,目前仅支持admin/admin-dealer-manage
     * @param int $uid 用户id
     * @param string $title 通知标题
     * @param array $list 通知文件列表[[$name, $url]]格式
     * @param int $duration 通知超时时间(毫秒)
     * @param mixed $platform
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected static function noticeDownload($platform, $uid, $title, $list, $duration = 30000)
    {
        $noticeList = [];
        foreach ($list as $item) {
            [$name, $url] = $item;
            $noticeList[] = ['name' => $name, 'url' => $url];
        }

        $cli = self::getClient();
        if (! $cli) {
            return;
        }
        $log = Helper::getLogger();
        try {
            $resp = $cli->post(self::SEND_USER_MSG_URI, [
                'json' => [
                    'uid' => $uid,
                    'platform' => $platform,
                    'data' => [
                        'biz' => 2,
                        'title' => $title,
                        'list' => $noticeList,
                    ],
                ],
            ]);
            if ($resp->getStatusCode() != 200) {
                $log->error('send_ws_user_download_error', [
                    'http_code' => $resp->getStatusCode(),
                    'reason' => $resp->getReasonPhrase(),
                ]);
                return;
            }

            $return = json_decode($resp->getBody()->getContents(), true);
            if (! (isset($return['code'], $return['msg']) && $return['code'] == 0)) {
                $log->error('send_user_download_fail', [
                    'body' => $resp->getBody()->getContents(),
                ]);
                return;
            }
        } catch (\Exception $e) {
            $log->error('notice_admin_download_error', [
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ]);
        }
    }

    protected static function getClient(): ?Client
    {
        $wsCliUrl = env('WS_CLIENT_URI');
        if (! $wsCliUrl) {
            return null;
        }
        return Helper::getHttpClient($wsCliUrl, 15);
    }
}
