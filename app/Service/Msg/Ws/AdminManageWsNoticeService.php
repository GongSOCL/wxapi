<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/sdc-crawler.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Service\Msg\Ws;

class AdminManageWsNoticeService extends WsService
{
    const PLATFORM_ADMIN_MANAGE = 2;

    public static function notice($uid, $msg, $code = 500, $type = 'error', $title = '通知')
    {
        try {
            self::noticeUser(
                self::PLATFORM_ADMIN_MANAGE,
                $uid,
                $msg,
                $code,
                $type,
                $title
            );
        } catch (\Exception $e) {

        }

    }

    public static function download($uid, $title, $list, $duration = 30000)
    {
        try {
            self::noticeDownload(
                self::PLATFORM_ADMIN_MANAGE,
                $uid,
                $title,
                $list,
                $duration
            );
        } catch (\Exception $e) {

        }

    }
}
