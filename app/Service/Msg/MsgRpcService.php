<?php

declare(strict_types=1);

namespace App\Service\Msg;

use App\Constants\DataStatus;
use App\Model\Qa\Users;
use Google\Protobuf\Internal\Message;
use Grpc\InsideMsg\CreateOneMsgReply;
use Grpc\InsideMsg\CreateOneMsgRequest;
use Grpc\InsideMsg\InsideMsgListReply;
use Grpc\InsideMsg\InsideMsgListRequest;
use Grpc\InsideMsg\LatestOneRequest;
use Grpc\InsideMsg\OneMsgDetailReply;
use Grpc\InsideMsg\OneMsgDetailRequest;
use Grpc\InsideMsg\ReadAllMsgReply;
use Grpc\InsideMsg\ReadAllMsgRequest;
use Youyao\Framework\Client\CoreClient;

class MsgRpcService extends CoreClient
{
    /**
     * @param InsideMsgListRequest $argument
     * @return Message
     */
    public function fetchInsideMsgList(InsideMsgListRequest $argument)
    {
        return $this->sendRequest($argument, [InsideMsgListReply::class, 'decode']);
    }

    /**
     * @param LatestOneRequest $argument
     * @return Message
     */
    public function fetchLatestOne(LatestOneRequest $argument)
    {
        return $this->sendRequest($argument, [OneMsgDetailReply::class, 'decode']);
    }

    /**
     * @param OneMsgDetailRequest $argument
     * @return Message
     */
    public function fetchOneMsgDetail(OneMsgDetailRequest $argument)
    {
        return $this->sendRequest($argument, [OneMsgDetailReply::class, 'decode']);
    }

    /**
     * @param ReadAllMsgRequest $argument
     * @return Message
     */
    public function readAllMsg(ReadAllMsgRequest $argument)
    {
        return $this->sendRequest($argument, [ReadAllMsgReply::class, 'decode']);
    }

    /**
     * @param CreateOneMsgRequest $argument
     * @return Message
     */
    public function createOneMsg(CreateOneMsgRequest $argument)
    {
        return $this->sendRequest($argument, [CreateOneMsgReply::class, 'decode']);
    }

    public function fetchAgentOneMsg($msgId, Users $users, $isApp): Message
    {
        $req = new OneMsgDetailRequest();
        $req->setPlatform($isApp ? DataStatus::MESSAGE_PLATFORM_APP : DataStatus::MESSAGE_PLATFORM_AGENT);
        $req->setUserId($users->uid);
        $req->setUserType(DataStatus::USER_TYPE_DOCTOR);
        $req->setMsgId($msgId);
        return $this->fetchOneMsgDetail($req);
    }
}
