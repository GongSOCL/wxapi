<?php
declare(strict_types=1);
namespace App\Service\Msg;

use App\Constants\DataStatus;
use App\Model\Qa\Msg;
use App\Model\Qa\Users;
use App\Repository\MsgRepository;
use App\Repository\RepsMsgRepository;
use Grpc\InsideMsg\CreateOneMsgReply;
use Grpc\InsideMsg\CreateOneMsgRequest;

class MsgService
{
    /**
     * @param $yyid
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getMsgList($yyid, $page, $pageSize)
    {
        //通过yyid获取消息列表
        $msgList = MsgRepository::getMsgList($yyid, DataStatus::WECHAT_AGENT, $page, $pageSize);
        $msgCount = MsgRepository::getMsgCount($yyid, DataStatus::WECHAT_AGENT);

        $data = [
            'page' => $page,
            'page_size' => $pageSize,
            'total' => $msgCount,
            'list' => $msgList,
            'unread' => RepsMsgRepository::getUnReadCount($yyid, DataStatus::WECHAT_AGENT)
        ];

        return $data;
    }

    /**
     * @param $yyid
     * @param $userType
     * @return array
     */
    public function getMsgListWithoutPage($yyid)
    {
        //通过yyid获取消息列表
        $msgList = MsgRepository::getMsgListWithoutPage($yyid, DataStatus::WECHAT_AGENT);
        $msgCount = MsgRepository::getMsgCount($yyid, DataStatus::WECHAT_AGENT);

        $data = [
            'total' => $msgCount,
            'list' => $msgList
        ];

        return $data;
    }


    /**
     * 获取消息详情
     * @param $yyid
     * @param $msgYyid
     * @return array
     */
    public function getMsgDetail($yyid, $msgYyid)
    {
        //获取详情
        if ($msgYyid=='') {
            //$msgYyid为空获取消息提醒
            $msgDetailObj = RepsMsgRepository::getRemindMsg($yyid, DataStatus::WECHAT_AGENT);
        } else {
            //$msgYyid不为空获取消息详情
            $msgDetailObj = RepsMsgRepository::getOneMsgDetail($yyid, $msgYyid);
        }

        $msgDetail = $msgDetailObj ? $msgDetailObj->toArray() : [];
        if (!empty($msgDetail)) {
            $msgDetail['unread'] = RepsMsgRepository::getUnReadCount($yyid, DataStatus::WECHAT_AGENT);
        }
        return $msgDetail;
    }

    /**
     * 给用户发一条消息
     *
     * @param $title
     * @param $msgType
     * @param Users|null $user
     * @param string $subTitle
     * @param string $content
     * @param string $link
     * @param int $sendType
     * @param bool $isFeedBack
     * @param string $start
     * @param string $end
     * @param bool $needAudit
     * @param int $memberId
     * @return array
     */
    public static function addUserMsg(
        $title,
        $msgType,
        ?Users $user,
        $subTitle = '',
        $content = '',
        $link = '',
        $sendType = Msg::SEND_TYPE_USER,
        $isFeedBack = false,
        $start = '',
        $end = '',
        $needAudit = false,
        $memberId = 0
    ): array {
        $msg = MsgRepository::addMsg(
            $title,
            $msgType,
            $subTitle,
            $content,
            $link,
            $sendType,
            $isFeedBack,
            $start,
            $end,
            $needAudit
        );

        $reps = null;
        if ($sendType == Msg::SEND_TYPE_USER) {
            $reps = RepsMsgRepository::addMsgReps($msg, $user, $memberId);
        }

        return [$msg, $reps];
    }

    /**
     * @param $userId
     * @param $userType
     * @param $title
     * @param $subTitle
     * @param $type
     * @param $body
     * @param string $link
     * @return int
     */
    public static function sendInsideMsg(
        $userId,
        $userType,
        $title,
        $subTitle,
        $type,
        $body,
        $link = ''
    ) {
        $msgRequest = new CreateOneMsgRequest();

        $msgRequest->setToUserId($userId);
        $msgRequest->setToUserType($userType);
        $msgRequest->setTitle($title);
        $msgRequest->setSubTitle($subTitle);
        $msgRequest->setType($type);
        $msgRequest->setBody($body);
        $msgRequest->setLink($link);

        /** @var CreateOneMsgReply $reply */
        $reply = (new MsgRpcService())->createOneMsg($msgRequest);

        return $reply->getId();
    }
}
