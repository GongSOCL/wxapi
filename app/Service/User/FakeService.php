<?php
declare(strict_types=1);
namespace App\Service\User;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\Jwt;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use Hyperf\Utils\ApplicationContext;

class FakeService
{
    public static function getEncPwd(): string
    {
        return env("FAKE_LOGIN_ENC_KEY", 'mr~(6[i)+"P{NvSjMVs.g1jrmRVPqu:L');
    }

    public static function getToken(string $encUid): string
    {
        $log = Helper::getLogger();
        $pwd = self::getEncPwd();

        //uid解码
        $uid = Helper::aesDec($encUid, $pwd);
        $log->info("get_token", [
            'enc_uid' => $uid,
            'uid' => $uid
        ]);
        if (false === $uid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法用户id");
        }
        $uid = intval($uid);

        //获取用户
        $user = UsersRepository::getUserByUid($uid);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户不存在");
        }

        //判断是否已经绑定手机号
        if (!($user->mobile_num && $user->mobile_verified)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户尚未绑定手机号或手机号未验证");
        }

        //判断是否已经绑定微信公众号
        $wu = WechatUserRepository::getOneWechatInfoByUserYYID($user->yyid, DataStatus::WECHAT_AGENT);
        if (!$wu) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户尚未绑定公众号");
        }

        //生成token并缓存token信息
        $ttl = env("FAKE_TOKEN_TTL", 300);
        return self::cacheWechatInfo($user->uid, $wu->id, $ttl);
    }

    public static function cacheWechatInfo($uid, $wechatId, $ttl = 300): string
    {
        $token = uniqid();
        $redis = Helper::getRedis("pod");
        $key = self::getTokenHashKey($token);
        $redis->hMSet($key, [
            'uid' => $uid,
            'wid' => $wechatId
        ]);
        $redis->expire($key, $ttl);

        return $token;
    }

    public static function getCacheInfoByCode($code): array
    {
        $redis = Helper::getRedis("pod");
        $key = self::getTokenHashKey($code);
        $redis->watch($key);
        try {
            $redis->multi();
            $redis->hMGet($key, ["uid", "wid"]);
            $redis->del($key);
            $res = $redis->exec();
            if (!(isset($res[0]['uid']) && isset($res[0]['wid']) && $res[0]['uid'] > 0 && $res[0]['wid'] > 0)) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法token");
            }
            return [(int)$res[0]['uid'], (int)$res[0]['wid']];
        } catch (\Exception $e) {
            //key已经被用过了
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法token");
        }
    }

    private static function getTokenHashKey($token): string
    {
        return sprintf("youyao:wei:fake:%s", $token);
    }

    public static function auth($code): array
    {
        [$uid, $wid] = self::getCacheInfoByCode($code);
        $user = UsersRepository::getUserByUid($uid);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户不存在");
        }

        $wu = WechatUserRepository::getById($wid);
        if (!$wu) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "微信记录不存在");
        }
        if ($wu->u_yyid != $user->yyid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户微信不一致");
        }
        $jwt = ApplicationContext::getContainer()
            ->get(Jwt::class);
        $expire = (int)env('FAKE_TOKEN_EXPIRE', 7200);

        return [
            'token' => $jwt->generate($user, $expire),
            'openid' => $wu->openid,
            'unionid' => $wu->unionid
        ];
    }
}
