<?php
declare(strict_types=1);
namespace App\Service\User;

use App\Helper\Helper;
use App\Model\Qa\Users;

class UserCacheService
{
    private static function buildUserPharmacyLinkKey(Users $users): string
    {
        $env = env('APP_ENV', 'local');
        return sprintf("wxapi:user:%s:pharmacy:store_link:%d", $env, $users->uid);
    }
    public static function getUserPharmacyUrlLink(Users $users): string
    {
        $key = self::buildUserPharmacyLinkKey($users);
        $res = Helper::getRedis()->get($key);
        return $res ? (string) $res : '';
    }

    public static function setUserPharmacyUrlLink(Users $users, $link, $ttl = 24 * 3600)
    {
        $key = self::buildUserPharmacyLinkKey($users);
        Helper::getRedis()->set($key, $link, $ttl);
    }

    public static function getUserPharmacySalesUrlLink(Users $users, $type): string
    {
        $key = self::buildUserPharmacySalesLinkKey($users, $type);
        $res = Helper::getRedis()->get($key);
        return $res ? (string) $res : '';
    }

    private static function buildUserPharmacySalesLinkKey(Users $users, $type): string
    {
        $env = env('APP_ENV', 'local');
        return sprintf("wxapi:user:%s:pharmacy:store_link:%d:%d", $env, $users->uid, $type);
    }

    public static function setUserPharmacySalesUrlLink(Users $users, $type, $link, $ttl = 24 * 3600)
    {
        $key = self::buildUserPharmacySalesLinkKey($users, $type);
        Helper::getRedis()->set($key, $link, $ttl);
    }
}
