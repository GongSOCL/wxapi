<?php
declare(strict_types=1);
namespace App\Service\User;

use App\Constants\Auth;
use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Constants\Wechat;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\Jwt;
use App\Helper\TaskManageHelper;
use App\Model\Qa\Agent;
use App\Model\Qa\AgentFamiliarCompany;
use App\Model\Qa\Company;
use App\Model\Qa\DepartmentImport;
use App\Model\Qa\NewDoctorUserinfo;
use App\Model\Qa\SystemDepart;
use App\Model\Qa\UserIdentity;
use App\Model\Qa\Users;
use App\Model\Qa\UserSession;
use App\Model\Qa\WechatUser;
use App\Model\Qa\YouyaoHospital;
use App\Repository\ActivityRepository;
use App\Repository\AdaptionRepository;
use App\Repository\AgentFamiliarCompanyRepository;
use App\Repository\AgentRepository;
use App\Repository\CompanyRepository;
use App\Repository\Contract\OaUserInviteRepository;
use App\Repository\DepartmentImportRepository;
use App\Repository\DepartmentUserRepository;
use App\Repository\DrugSeriesRepository;
use App\Repository\ImportUserRepository;
use App\Repository\NewGroup\NewDoctorUserinfoRepository;
use App\Repository\OrgBindingUserRepository;
use App\Repository\RefuseRemarkRepository;
use App\Repository\RegionRepository;
use App\Repository\RepsAcademicHelpRepository;
use App\Repository\RepsFamiliarHospitalRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\SupplierRepository;
use App\Repository\SystemDepartRepository;
use App\Repository\UploadQiniuRepository;
use App\Repository\UserCompanyChangeRepository;
use App\Repository\UserCompanyRepository;
use App\Repository\UserSessionRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Service\Common\AuditService;
use App\Service\Common\ServeService;
use App\Service\Common\SmsService;
use App\Service\Doctor\DoctorInfoRpcService;
use App\Service\Group\Doctor\DoctorGroupService;
use App\Service\Msg\Ws\AdminWsNoticeService;
use App\Service\Pharmacy\WechatSvcRpcService;
use App\Service\Points\PointRpcService;
use App\Service\Upload\UploadService;
use App\Service\Wechat\WechatBusiness;
use Exception;
use Grpc\Common\CommonPageResponse;
use Grpc\DoctorInfo\DoctorWechatInfoItem;
use Grpc\Msg\MSG_BIZ_TYPE;
use Grpc\Msg\MSG_PRIORITY;
use Grpc\Pharmacy\Common\ShareLinkToken;
use Grpc\Task\OrgUserChangeParams;
use Grpc\Wxapi\User\AgentDoctorGetReply;
use Grpc\Wxapi\User\AgentDoctorItem;
use Grpc\Wxapi\User\CityItem;
use Grpc\Wxapi\User\CompanyItem;
use Grpc\Wxapi\User\DepartItem;
use Grpc\Wxapi\User\HospitalItem;
use Grpc\Wxapi\User\ServeCompanyItem;
use Grpc\Wxapi\User\UserItem;
use Grpc\Wxapi\User\UserListReponse;
use Grpc\Wxapi\User\UserSearchItem;
use Grpc\Wxapi\User\UserSearchList;
use Grpc\Wxapi\User\UserVerifyStatus;
use Grpc\Wxapi\User\UserWechatItem;
use Grpc\Wxapi\User\VerifyInfoResponse;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use Hyperf\Utils\Coroutine;
use Youyao\Framework\Components\Msg\MessageRpcClient;

class UserService
{


    /**
     * @Inject
     * @var WechatBusiness
     */
    protected $wechatBusiness;

    public static function searchUser(string $keywords = "", $limit = 10): UserSearchList
    {
        $resp = UsersRepository::searchUserWithLimit($keywords, $limit);
        $reply = new UserSearchList();
        if ($resp->isEmpty()) {
            return $reply;
        }

        $data = [];
        /** @var Users $item */
        foreach ($resp as $item) {
            $data[] = (new UserSearchItem())
                ->setId($item->uid)
                ->setName($item->name);
        }
        $reply->setList($data);
        return $reply;
    }

    public static function getUserServeHospitalDoctors(
        $userId,
        $withWechat = true,
        $current = 1,
        $limit = 10
    ): AgentDoctorGetReply {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::USER_NOT_EXISTS);
        }

        $resp = NewDoctorUserinfoRepository::getAgentServeHospitalDoctors(
            $user,
            $withWechat,
            $current,
            $limit
        );
        $page = new CommonPageResponse();
        $page->setCurrent($resp->currentPage());
        $page->setPages($resp->lastPage());
        $page->setSize($limit);
        $page->setTotal($resp->total());

        $reply = new AgentDoctorGetReply();
        $reply->setPage($page);
        if ($resp->isEmpty()) {
            return $reply;
        }

        //根据openid取得医生微信信息
        $openIds = [];
        $hospitalYYIDS = [];
        /** @var NewDoctorUserinfo $item */
        foreach ($resp->items() as $item) {
            if ($item->hospital_yyid) {
                $hospitalYYIDS[] = $item->hospital_yyid;
            }
            if ($item->openid) {
                $openIds[] = $item->openid;
            }
        }
        $doctorMap = self::getDoctorsByOpenIds($openIds);

        //获取医院id
        $yyidResp = YouyaoHospitalRepository::getIdByYyids(array_unique($hospitalYYIDS));
        $yyidMap = $yyidResp->keyBy('yyid')->all();

        $list = [];
        foreach ($resp->items() as $item) {
            /** @var DoctorWechatInfoItem $doctorItem */
            $doctorItem = $doctorMap[$item->openid] ?? null;
            $o = new AgentDoctorItem();
            $name = $item->true_name ? $item->true_name : (
                $doctorItem && $doctorItem->getName() != "" ? $doctorItem->getName() : (
                    $item->nickname ? $item->nickname : ''
                )
            );
            $headImg = $doctorItem && $doctorItem->getHeadimg() != '' ? $doctorItem->getHeadimg() : (
                $item->headimgurl ? $item->headimgurl : ''
            );
            /** @var NewDoctorUserinfo $item */
            $o->setId($item->nid);
            $o->setName($name);
            $o->setUnionid($item->unionid);
            $o->setOpenid($item->openid);
            $o->setHeadimg($headImg);
            if ($item->hospital_yyid) {
                $o->setHospitalId($yyidMap[$item->hospital_yyid] ? $yyidMap[$item->hospital_yyid]->id : 0);
                $o->setHospitalName((string) $item->hospital_name);
            }
            if ($item->depart_id) {
                $o->setDepartId((int) $item->depart_id);
                $o->setDepartName((string) $item->depart_name);
            }
            $list[] = $o;
        }

        $reply->setDoctors($list);
        return $reply;
    }

    public static function getDoctorsByOpenIds(array $openIds): array
    {
        if (empty($openIds)) {
            return [];
        }
        $rpc = new DoctorInfoRpcService();
        $resp = $rpc->getWechatInfos(array_unique($openIds));

        $map = [];
        /** @var DoctorWechatInfoItem $item */
        foreach ($resp->getList() as $item) {
            $map[$item->getOpenid()]  = $item;
        }
        return $map;
    }

    public static function sendUserWechatTemplateMsg($userId, array $params, string $templateAlias, $url = "")
    {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::USER_NOT_EXISTS);
        }
        $wechat = WechatUserRepository::getOneWechatInfoByUserYYID($user->yyid, DataStatus::WECHAT_AGENT);
        if (!$wechat) {
            throw new BusinessException(ErrorCode::USER_WECHAT_INFO_NOT_EXISTS);
        }

        ApplicationContext::getContainer()
            ->get(MessageRpcClient::class)
            ->sendWechatTemplateMsg(
                [$wechat->openid],
                Wechat::WECHAT_CHANNEL_WXAPI,
                $templateAlias,
                MSG_BIZ_TYPE::BIZ_COMMON_TYPE,
                $url,
                $params,
                MSG_PRIORITY::PRIORITY_MIDDLE
            );
    }

    public static function getPharmacyAgentInviteInfo(): string
    {
        $user = Helper::getLoginUser();
        $link = UserCacheService::getUserPharmacyUrlLink($user);
        if ($link) {
            return $link;
        }

        //生成用户token
        $days = 25;
        $t = new ShareLinkToken();
        $t->setAgentId($user->uid);
        $token = Helper::genShareToken($t);
        $resp = WechatSvcRpcService::generateUrlLink('pages/agent/index', [
            'token' => $token
        ], $days);

        UserCacheService::setUserPharmacyUrlLink($user, $resp->getLink(), $days * 24 * 3600);

        return $resp->getLink();
    }

    public static function getPharmacyDoctorQrcode($doctorId)
    {
        $userId = Helper::getLoginUser()->uid;
        if(!ApplicationContext::getContainer()
            ->get(DoctorGroupService::class)
            ->checkAgentOwnerDoctorInGroup($userId, $doctorId)){
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '该医生不在群组内');
        }
        $resp = WechatSvcRpcService::generateDoctorQrLink($userId, $doctorId);
        return [
            'link'=>$resp->getLink()
        ];
    }

    public static function rebindUserCompany($userId, $companyId, $adminUserId = 0)
    {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::USER_NOT_EXISTS);
        }
        $oldCompany = $user->company;
        if (!$oldCompany) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '用户尚未认证关联公司，无法修改');
        }

        $newCompany = CompanyRepository::getById($companyId);
        if (!$newCompany) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '新公司不存在');
        }
        if ($oldCompany->id == $newCompany->id) {
            return;
        }

        //获取商务邀请用户信息
        $inviteInfo = OaUserInviteRepository::getUserInfo($user->uid, $oldCompany->id);

        Db::beginTransaction();
        try {
            //修改用户所属公司
            UsersRepository::updateCompany($user, $newCompany);
            //记录更改日志
            UserCompanyChangeRepository::addChange(
                $user,
                $oldCompany,
                $newCompany,
                $adminUserId
            );
            //更新商务邀请用户信息
            if ($inviteInfo) {
                OaUserInviteRepository::updateUserCompany($inviteInfo, $newCompany);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }

        //通知客户端邀请信息已经变更，可能影响合同
        if ($adminUserId && $inviteInfo) {
            go(function () use($adminUserId) {
                AdminWsNoticeService::notice(
                    $adminUserId,
                    "公司变更修改导致商务录入用户信息公司变更，可能影响合同!",
                    500,
                    'warning',
                    '用户所属公司变更影响通知'
                );
            });
        }
    }

    private static function getSalesLink(Users $user, $type = 1): string
    {
        $link = UserCacheService::getUserPharmacySalesUrlLink($user, $type);
        if ($link) {
            return $link;
        }

        $days = 25;
        $t = new ShareLinkToken();
        $t->setAgentId($user->uid);
        $token = Helper::genShareToken($t);
        $resp = WechatSvcRpcService::generateUrlLink('pages/store/record', [
            'token' => $token,
            'type' => (int) $type
        ], $days);

        UserCacheService::setUserPharmacySalesUrlLink($user, $type, $resp->getLink(), $days * 24 * 3600);

        return $resp->getLink();
    }

    public static function getPharmacySalesAndTopicRecordLink(): array
    {
        $user = Helper::getLoginUser();

        return [
            self::getSalesLink($user, 1),
            self::getSalesLink($user, 2)
        ];
    }

    public function getQrcode()
    {
        $userId = Helper::getLoginUser()->uid;

        //获取行为信息 来源：t_activity
        $activityData = ActivityRepository::getActivity(DataStatus::ACTIVITY_ID_QRCODE_AGENT_INVITE);
        if ($activityData) {
            $inviteData = $activityData->id.'#'.$activityData->event_key.'_'.$userId.'_';
        }

        $url = $this->wechatBusiness->getQrcode($inviteData, DataStatus::WECHAT_AGENT);
        return ['url' => $url];
    }



    /**
     * @param $user
     * @param $supplierId
     * @param $inviteAid
     * @return int[]
     */
    public static function checkSupplier($user, $supplierId, $inviteAid)
    {
        $isSupplier = 0;
        $isSupplierLeader = 0;
        if ($inviteAid==0) {
            // 判断是否是管理员或者是个人供应商
            $memberInfo = SupplierRepository::getOneMemberInfoByYyid($supplierId, $user->yyid);
            if ($memberInfo) {
                $supInfo = SupplierRepository::getSupplierInfoById($memberInfo->supplier_id);
                if ($supInfo&&$supInfo->type==1&&$memberInfo->is_leader==1) {
                    // 肯定是公司而且是管理员
                    $isSupplier = 1;
                    $isSupplierLeader = 1;
                } elseif ($supInfo&&$supInfo->type==2) {
                    // 肯定是个人供应商
                    $isSupplier = 2;
                    $isSupplierLeader = 1;
                }
            }
        } else {
            // 肯定是公司供应商而且是成员
            $isSupplier = 1;
            // 通过inviteAid获取用户uid
//            $agentInfo = SupplierRepository::getAgentInfo($inviteAid);
            // 同时添加供应商待审核成员信息
//            SupplierRepository::addJoinMember($supplierId, $user->uid, $agentInfo->uid, $user->yyid);
        }

        return [
            'supplier_id' => $supplierId,
            'isSupplier' => $isSupplier,
            'isSupplierLeader' => $isSupplierLeader
        ];
    }

    /**
     * 验证用户登陆信息
     *
     * @param $yyid
     * @param $token
     */
    public static function verifyUserLogin($yyid, $token)
    {
        $expire = 60*60*24*60;
        //从缓存中获取登陆信息
        $cacheSessionInfo = self::getUserSessionFromCache($yyid, $token, $expire);
        if ($cacheSessionInfo) {
            return $cacheSessionInfo;
        }

        //从数据库获取用户信息
        $dbSessionInfo = UserSessionRepository::getUserSession($yyid);
        if ((!$dbSessionInfo) || $dbSessionInfo->sid != $token) {
            throw new BusinessException(ErrorCode::ERROR_CODE_AUTHENTICATION_FAILED);
        }

        //过期了
        if (time() - $dbSessionInfo->created > $expire) {
            UserSessionRepository::deleteUserSession($yyid);
            throw new BusinessException(ErrorCode::ERROR_CODE_AUTHENTICATION_FAILED);
        }

        //缓存用户session信息
        self::addUserSessionToCache($dbSessionInfo, $expire);
        return $dbSessionInfo;
    }

    /**
     * 缓存用户session信息
     * @param UserSession $userSession
     * @param $expireSeconds
     */
    private static function addUserSessionToCache(UserSession $userSession, $expireSeconds)
    {
        $key = sprintf("yyao:session:%s", $userSession->yyid);
        $container = ApplicationContext::getContainer();
        $expireAt = $userSession->created + $expireSeconds;

        /** @var \Redis $redis */
        $redis = $container->get(\Hyperf\Redis\Redis::class);
        $redis->set($key, serialize($userSession->toArray()));
        $redis->expireAt($key, $expireAt);
    }

    /**
     * 从缓存中清除凭证
     * @param $yyid
     */
    public static function deleteUserCacheSession($yyid)
    {
        $key = sprintf("yyao:session:%s", $yyid);
        Helper::getRedisCache()->del($key);
    }

    /**
     * 从缓存中获取用户session
     * @param $yyid
     * @param $token
     * @param $expireSeconds
     * @return array|mixed
     */
    private static function getUserSessionFromCache($yyid, $token, $expireSeconds)
    {
        $key = sprintf("yyao:session:%s", $yyid);

        $redis = Helper::getRedisCache();
        $result = $redis->get($key);
        if (!$result) {
            return [];
        }

        $sessionInfo = unserialize($result);
        if ($sessionInfo['sid'] != $token) {
            //删除缓存
            $redis->del($key);
            return [];
        }

        $createAt = $sessionInfo['created'];
        $interval = time() - $createAt;
        if ($interval > $expireSeconds) {
            //删除缓存
            $redis->del($key);
            return [];
        }

        return $sessionInfo;
    }

    /**
     * app获取用户信息
     * @return array
     */
    public static function getUserInfo(): array
    {
        $user = Helper::getLoginUser();
        $authRole = Helper::getAuthRole();
        $wechatType = Helper::getWechatTypeByRole($authRole);
        $wechatInfo = WechatUserRepository::getOneWechatInfoByUserYYID($user->yyid, $wechatType);

        # 判断是否上传身份证 并且是否在有效期内
        $is_upload_idcard = $user->is_upload_idcard ?? 0;

        if ($is_upload_idcard) {
            # 增加平台选择 1优药 2优佳医 4优能汇
            $validPeriod = UserIdentity::where('uid', $user->yyid)
                ->where('platform', 1)
                ->orderBy('created_at', 'desc')
                ->first();

            if ($validPeriod) {
                $validTime = strtotime(str_replace('.', '-', $validPeriod->due_date));

                if ($validTime > time()) {
                    $is_upload_idcard = 1;
                } else {
                    $is_upload_idcard = 2;
                }
            } else {
                $is_upload_idcard = 0;
            }
        }

        //处理头像
        $avatar = "";
        if ($user->avatar_id > 0) {
            $upload = UploadQiniuRepository::getById($user->avatar_id);
            if ($upload) {
                $avatar = $upload->url;
            }
        }
        if ((!$avatar) && $wechatInfo) {
            $avatar = (string) $wechatInfo->headimgurl;
        }

        $verifyStatus = 1;
        switch ($user->is_agent) {
            case Users::AGENT_VERIFY_NO:
                $verifyStatus = 1;
                break;
            case Users::AGENT_VERIFY_OK:
                $verifyStatus = 3;
                break;
            case Users::AGENT_VERIFY_PROCESSING:
                $verifyStatus = 2;
                break;
            case Users::AGENT_VERIFY_REFUSED:
                $verifyStatus = 4;
                break;
        }
        $refuseMsg = "";
        if ($user->is_agent == Users::AGENT_VERIFY_REFUSED) {
            $refuse  = RefuseRemarkRepository::getUserRefuseMsg($user);
            $refuseMsg = $refuse ? $refuse->content : '';
        }
        $trueName = "";
        if ($user->isVerified()) {
            $agent = AgentRepository::getAgentByUser($user);
            !is_null($agent) && $trueName = (string) $agent->truename;
        }
        $wechatNickname = $wechatInfo && $wechatInfo->nickname ? $wechatInfo->nickname : '';
        return [
            'id' => $user->uid,
            'name' => $user->name ?: $wechatNickname,
            'wechat_nickname' => $wechatNickname,
            'avatar' => $avatar,
            'is_verified' => $user->is_agent == 2 ? 1 : 0,
            'verify_status' => $verifyStatus,
            'reject_reason' => $refuseMsg,
            'mobile' => (string) $user->mobile_num,
            'is_upload_idcard' => $is_upload_idcard,
            'is_bind_wechat' => $user->is_bind_wechat,
            'is_bind_ios' => $user->is_bind_ios,
            'true_name' => $trueName,
            'name_change_times' => $user->name_change_times,
            'headimg_change_times' => $user->headimg_change_times,
            'is_alert' => $user->is_alert
        ];
    }

    /**
     * 根据微信授权登陆
     * @param $openId
     * @return array
     * @throws Exception
     */
    public static function authByWechat($openId): array
    {
        $authRole = Helper::getAuthRole();
        $user = self::getUserByWechatAuth($openId, $authRole);

        return self::generateAuthInfo($user);
    }

    private static function generateAuthInfo(Users $user = null): array
    {
        $authStatus = Auth::AUTH_STATUS_BIND_MOBILE;
        $token = "";
        if ($user) {
            $token = ApplicationContext::getContainer()
                ->get(Jwt::class)
                ->generate($user);
            if (!$token) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "微信登陆失败");
            }
            $authStatus = Auth::AUTH_STATUS_SUCCESS;
        }
        return [
            'auth_status' => $authStatus,
            'token' => $token
        ];
    }

    private static function getUserByWechatAuth($openId, $authRole): ?Users
    {
        $user = null;
        $wechatType = Helper::getWechatTypeByRole($authRole);
        $wechatInfo = WechatUserRepository::getOneWechatInfo($openId, $wechatType);
        if (!$wechatInfo) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "未绑定微信，请重新授权");
        }

        //判断用户信息是否
        if ($wechatInfo->u_yyid) {
            $user = UsersRepository::getUserByYYID($wechatInfo->u_yyid);
            if ($user) {
                return $user;
            }
        }

        Db::beginTransaction();
        try {
            $bindWechat = null;
            //有unionid且非公众号登陆直接使用unionid去获取对应的关联用户
            if ($wechatInfo->unionid) {
                $relateType = self::getRelatedWechatType($wechatType);
                $bindWechat = WechatUserRepository::getOneWechatInfoByUnionId(
                    $wechatInfo->unionid,
                    $relateType,
                    true
                );
                $bindUser = $bindWechat ? $bindWechat->user : null;
                if ($bindUser) {
                    WechatUserRepository::bindWechatUser($wechatInfo, $bindUser);
                    Db::commit();
                    return $bindUser;
                }
            }

            //用户不存在，根据微信信息添加一个空手机号用户
            $user = UsersRepository::addUserAccountFromMobileAndWechat($wechatInfo);
            WechatUserRepository::bindWechatUser($wechatInfo, $user);
            if ($bindWechat) {
                WechatUserRepository::bindWechatUser($bindWechat, $user);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }


        return $user;
    }

    /**
     * @throws Exception
     */
    public static function bindWechatMobile($mobile, $code): array
    {
        if (!SmsService::verify($mobile, $code)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "验证码不正确");
        }

        //校验用户手机号
        $user = Helper::getLoginUser();
        if ($user->mobile_num && $user->mobile_verified) {
            //暂时不支持同一个手机号换绑
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "手机号已经绑定或者不能直接换绑");
        }

        //更新用户手机号
        UsersRepository::updateMobile($user, $mobile);

        //重新生成登陆信息
        return self::generateAuthInfo($user);
    }

    public static function appLogout()
    {
        Helper::getLoginUser();

        //获取token-设置redis黑名单
        $token = Helper::getAuthToken();
        if ($token) {
            ApplicationContext::getContainer()
                ->get(Jwt::class)
                ->logout($token);
        }
    }

    /**
     * 更新用户手机号
     * @param $mobile
     * @param $code
     */
    public static function updateUserMobileAccount($mobile, $code)
    {
        //校验短信
        if (!SmsService::verify($mobile, $code)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "验证码不正确");
        }

        $user = Helper::getLoginUser();
        if ($user->mobile_num == $mobile) {
            return;
        }

        $mobileExists = UsersRepository::getUserByMobile($mobile);
        if ($mobileExists) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该手机号已经注册");
        }

        UsersRepository::updateMobile($user, $mobile);
    }

    /**
     * 更新用户头像
     * @param $uploadId
     */
    public static function updateUserAvatar($uploadId)
    {
        $upload = UploadQiniuRepository::getById($uploadId);
        if (!$upload) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "上传头像不存在");
        }
        $user = Helper::getLoginUser();
        $oldId = $user->avatar_id;

        //更新用户记录
        $operateResult = UsersRepository::updateAvatar($user, $upload);

        //清除旧文件
        if ($operateResult && $oldId > 0 && $oldId != $uploadId) {
            Coroutine::create(function () use ($oldId) {
                //删除旧文件
                try {
                    UploadService::clearUpload($oldId, true);
                } catch (Exception $e) {
                    Helper::getLogger()->error("clear_qiniu_upload_error", [
                        'id' => $oldId,
                        'msg' => $e->getMessage()
                    ]);
                }
            });
        }
    }

    /**
     * 添加认证申请
     * @param $trueName
     * @param $provinceId
     * @param $companyId
     * @param $workingAge
     * @param array $departmentIds
     * @param array $hospitalIds
     * @param array $serveCompany
     * @param array $direction
     * @param $supplierId
     * @param $inviteAid
     * @throws Exception
     */
    public static function addVerify(
        $trueName,
        $provinceId,
        $companyId,
        $workingAge,
        array $departmentIds,
        array $hospitalIds = [],
        array $serveCompany = [],
        array $direction = [],
        $otherCompanyName = "",
        $supplierId = 0,
        $inviteAid = 0,
        $roleType = 0
    ) {
        $user = Helper::getLoginUser();
        if ($user->is_agent == Users::AGENT_VERIFY_OK) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户已认证完成，不要重复申请");
        }
        if ($user->is_agent == Users::AGENT_VERIFY_PROCESSING) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "认证已申请，不要重复申请");
        }
        //检查服务省份
        $province = RegionRepository::getById($provinceId);
        if (!$province) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务地区不存在");
        }

        //检查公司
        $company = CompanyRepository::getById($companyId);
        if (!$company) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务公司不存在");
        }
        if ($company->name == "其他") {
            if (!$otherCompanyName) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务公司名称不能为空");
            }
            $company = self::findOrAddCompanyByName($otherCompanyName);
        }

        //检查医院(可拒绝后重新申请，有残留数据)
        $validHospitals = new Collection();
        if (!empty($hospitalIds)) {
            $validHospitals = YouyaoHospitalRepository::getHospitalByIds($hospitalIds);
        }
        $validHospitalYYIDS = $validHospitals->pluck('yyid')->toArray();
        $existsHospitals = RepsFamiliarHospitalRepository::getUserFamiliarHospitals($user);
        $existsHospitalYYIDS = $existsHospitals->pluck('hospital_yyid')->toArray();
        [$delHospitalYYIDS, $addHospitalYYIDS] = Helper::diff($existsHospitalYYIDS, $validHospitalYYIDS);

        //检查科室
        $validDepartments = SystemDepartRepository::getByIds($departmentIds);
        if ($validDepartments->isEmpty()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "请至少选择一个熟悉科室");
        }
        $validDepartIds = $validDepartments->pluck('id')->toArray();


        //检查服务公司
        $serveCompanyData = [];
        if (!empty($serveCompany)) {
            $serveCompanyData = self::checkServeCompany($serveCompany);
        }

        // 检查是否是供应商管理员成员
        $supplierInfo = self::checkSupplier($user, $supplierId, $inviteAid);

        Db::beginTransaction();
        try {
            //添加/更新用户代表信息
            $agent = AgentRepository::findByUser($user);
            if ($agent) {
                AgentRepository::updateUserInfoFromVerify(
                    $user,
                    $trueName,
                    $province,
                    $workingAge,
                    $direction,
                    $validDepartIds,
                    $supplierInfo,
                    $inviteAid,
                    $roleType
                );
            } else {
                $agent = AgentRepository::addUserInfoFromVerify(
                    $user,
                    $trueName,
                    $province,
                    $workingAge,
                    $direction,
                    $validDepartIds,
                    $supplierInfo,
                    $inviteAid,
                    $roleType
                );
            }

            // 更新熟悉医院
            if (!empty($addHospitalYYIDS)) {
                $operateResult = RepsFamiliarHospitalRepository::batchAddUserFamiliarHospitals(
                    $user,
                    $addHospitalYYIDS
                );
                if (!$operateResult) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户服务医院更新失败");
                }
            }
            if (!empty($delHospitalYYIDS)) {
                $operateResult = RepsFamiliarHospitalRepository::batchDelFamiliarHospitals($user, $delHospitalYYIDS);
                if (!$operateResult) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户服务医院更新失败");
                }
            }

            //更新代表服务公司
            $operateResult = UsersRepository::updateCompany($user, $company);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户状态更新失败");
            }
            //添加服务公司
            AgentFamiliarCompanyRepository::clearUserFamiliarCompany($user);
            if (!empty($serveCompanyData)) {
                AgentFamiliarCompanyRepository::batchAddUserFamiliarCompany($user, $serveCompanyData);
            }

            // 更新用户是代表状态
            UsersRepository::updateUserAgent($user, Users::AGENT_VERIFY_PROCESSING, $agent->truename);
            Db::commit();
        } catch (Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }

    private static function findOrAddCompanyByName($name): Company
    {
        $company = CompanyRepository::getByName($name);
        if ($company) {
            return $company;
        }

        return CompanyRepository::addByName($name);
    }

    /**
     * 检查用户认证服务公司参数
     * @param $serveCompany
     * @return array
     */
    private static function checkServeCompany($serveCompany): array
    {
        $companyIds = [];
        $fieldIds = [];
        $adaptionIds = [];

        foreach ($serveCompany as $item) {
            $companyIds[] = $item['company_id'];
            foreach ($item['field_id'] as $f) {
                $fieldIds[] = $f;
            }
            foreach ($item['adaption_id'] as $a) {
                $adaptionIds[] = $a;
            }
        }

        $company = CompanyRepository::getByIds($companyIds);
        $companyMap = $company->keyBy('id')->all();

        $fields = AdaptionRepository::getFieldByIds($fieldIds);
        $fieldMap = $fields->keyBy('id')->all();

        $adaptions = AdaptionRepository::getAdaptionByIds($adaptionIds);
        $adaptionMap = $adaptions->keyBy('id')->all();

        $data = [];
        foreach ($serveCompany as $item) {
            $i = [];
            //检查公司参数
            if (!isset($companyMap[$item['company_id']])) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "公司不存在");
            }

            //检查治疗领域和适应症
            foreach ($item['field_id'] as $fid) {
                if (!isset($fieldMap[$fid])) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "{$fid}治疗领域不存在");
                }
            }
            foreach ($item['adaption_id'] as $aid) {
                if (!isset($adaptionMap[$aid])) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "适应症不存在");
                }
            }

            $i['company_name'] = $companyMap[$item['company_id']]->name;
            $i['field_id'] = $item['field_id'];
            $i['adaption_id'] = $item['adaption_id'];
            $i['product'] = $item['product_name'];
            $data[] = $i;
        }

        return $data;
    }

    /**
     * 获取认证信息
     * @return array
     */
    public static function getVerifyInfo(): array
    {
        $user = Helper::getLoginUser();
        if ($user->is_agent == DataStatus::DELETE) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户尚未完成认证");
        }

        list(
            $agent,
            $serveHospitals,
            $companyInfo,
            $city,
            $serveCompanies,
            $departItem,
            $directions
            ) = self::getUserVerifyDetail($user);

        $refuseReasion = RefuseRemarkRepository::getUserRefuseMsg($user);

        return [
            'true_name' => $agent->truename,
            'serve_hospitals' => $serveHospitals,
            'company' => $companyInfo,
            'city' => $city,
            'serve_company' => $serveCompanies,
            'department' => $departItem,
            'direction' => $directions,
            'last_refuse_reasion' => $refuseReasion ? $refuseReasion->content : ''
        ];
    }

    /**
     * 获取用户认证服务公司
     * @param Users $user
     * @return array
     */
    private static function getUserServeCompanies(Users $user): array
    {
        $familiarCompanies = AgentFamiliarCompanyRepository::getUserFamiliarCompanies($user);
        if ($familiarCompanies->isEmpty()) {
            return [];
        }

        $fieldYYIDS = $familiarCompanies->pluck('company_territory')->toArray();
        $fstr = implode(',', $fieldYYIDS);
        $fieldIDS = array_unique(explode(',', $fstr));
        $adaptionYYIDS = $familiarCompanies->pluck('company_adaptation')->toArray();
        $astr = implode(',', $adaptionYYIDS);
        $adaptionIDS = array_unique(explode(',', $astr));

        $fields = AdaptionRepository::getFieldByIds($fieldIDS);
        $fieldMap = $fields->keyBy('id')->all();

        $adaptions = AdaptionRepository::getAdaptionByIds($adaptionIDS);
        $adaptionMap = $adaptions->keyBy('id')->all();

        $fc = [];
        /** @var AgentFamiliarCompany $c */
        foreach ($familiarCompanies as $c) {
            $cf = explode(',', (string) $c->company_territory);
            $fs = [];
            foreach ($cf as $i) {
                if ($i && isset($fieldMap[$i])) {
                    $fs[] = $fieldMap[$i]->name;
                }
            }

            $ca = explode(',', (string) $c->company_adaptation);
            $fa = [];
            foreach ($ca as $i) {
                if ($i && isset($adaptionMap[$i])) {
                    $fa[] = $adaptionMap[$i]->name;
                }
            }

            $fc[] = [
                'id' => $c->id,
                'company_name' => $c->company_name,
                'field' => $fs,
                'adaption' => $fa,
                'product' => $c->drug_name
            ];
        }

        return $fc;
    }

    public static function recordWechatInfo(
        $wechatType,
        $openId,
        $nickname,
        $headImgUrl,
        $unionId = '',
        $sex = 0,
        $country = '',
        $province = '',
        $city = ''
    ) {
        $info = [
            'nickname' => $nickname,
            'sex' => $sex,
            'city' => $city,
            'province' => $province,
            'country' => $country,
            'headimgurl' => $headImgUrl,
            'openid' => $openId,
            'unionid' => $unionId
        ];
        $wechat = WechatUserRepository::getOneWechatInfo($openId, $wechatType);
        if ($wechat) {
            //已经存在更新微信信息
            WechatUserRepository::updateWechatInfoFromOauthRawInfo($wechat, $info);
        } else {
            $userYYID = "";
            if ($unionId) {
                //获取其它类型记录里面有同unionid的记录
                $relateType = UserService::getRelatedWechatType($wechatType);
                $sameUnionIdWechat = WechatUserRepository::getByUnionIdExcludeType($unionId, $relateType);
                if ($sameUnionIdWechat) {
                    $userYYID = (string) $sameUnionIdWechat->u_yyid;
                }
            }

            WechatUserRepository::addWechatInfoFromOauthRawInfo(
                $info,
                $wechatType,
                $userYYID,
                true
            );
        }
    }

    /**
     * 检查用户是否已经绑定手机号
     */
    public static function checkMobileBind()
    {
        $user = Helper::getLoginUser();
        if (!($user->mobile_num && $user->mobile_verified == 1)) {
            throw new BusinessException(ErrorCode::ERROR_MOBILE_NOT_BIND);
        }
    }

    /**
     * 获取app登陆用户的微信
     * @return WechatUser|null
     */
    public static function getAppLoginUserWechat(): ?WechatUser
    {
        $user = Helper::getLoginUser();
        $role = Helper::getAuthRole();
        $wechatType = Helper::getWechatTypeByRole($role);

        return WechatUserRepository::getOneWechatInfoByUserYYID($user->yyid, $wechatType);
    }

    /**
     * 根据手机号和微信帐号登陆验证
     * @param $mobile
     * @param $openId
     * @return array
     * @throws Exception
     */
    public static function authByMobileAndWechat($mobile, $openId): array
    {
        $wechatType = DataStatus::WECHAT_AGENT;

        Db::beginTransaction();
        try {
            //刷新时可能记录微信
            $wechat = self::parseAuthWechat($mobile, $openId, $wechatType);

            $user = self::parseAuthMobile($mobile, $wechat);

            //根据openid批量关联用户
            WechatUserRepository::bindUserByOpenId($wechat->openid, $user);

            //根据unionId批量关联用户
            if ($wechat->unionid) {
                WechatUserRepository::bindUserByUnionId($wechat->unionid, $user);
            }
            $wechat->refresh();
            Db::commit();

            //调用积分接口进行积分微信帐户绑定
            PointRpcService::bindWechat($user, $wechat);
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }

        return [$user, $wechat];
    }

    /**
     * 处理登陆手机号
     * @param $mobile
     * @param WechatUser $wu
     * @return Users
     */
    private static function parseAuthMobile($mobile, WechatUser $wu): Users
    {
        $mu = UsersRepository::getUserByMobile($mobile);
        if (!$mu) {
            if (!$wu->u_yyid) {
                //创建用户
                $u = UsersRepository::addUserAccountFromMobileAndWechat($wu, $mobile);
                WechatUserRepository::bindWechatUser($wu, $u);
                return $u;
            }

            $wwu = UsersRepository::getUserByYYID($wu->u_yyid);
            if (!$wwu->mobile_num) {
                UsersRepository::updateMobile($wwu, $mobile);
            }
            return $wwu;
        } else {
            $eu = WechatUserRepository::getOneWechatInfoByUserYYID($mu->yyid, $wu->wechat_type);
            if ($eu && $eu->id != $wu->id) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该手机号已经绑定另一个微信号");
            }

            if (!$wu->u_yyid) {
                WechatUserRepository::bindWechatUser($wu, $mu);
            }
            return $mu;
        }
    }

    /**
     * 处理微信记录
     * @param $mobile
     * @param $openId
     * @param $wechatType
     * @return WechatUser
     */
    private static function parseAuthWechat($mobile, $openId, $wechatType): WechatUser
    {
        $wt  = WechatUserRepository::getOneWechatInfo($openId, $wechatType);
        if (!$wt) {
            //通过微信服务器获取用户信息接口获取并更新信息
            $wt = self::refreshWechatInfo($openId, $wechatType);
        }

        if (!$wt) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "微信授权信息不存在");
        }

        if ($wt->u_yyid) {
            $wu = UsersRepository::getUserByYYID($wt->u_yyid);
            if (!$wu) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "微信绑定用户不存在");
            }
            if ($wu->mobile_num && $wu->mobile_num != $mobile) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该微信号已经绑定另一个手机号");
            }

            return $wt;
        }

        if ($wt->unionid) {
            $relateType = self::getRelatedWechatType($wechatType);
            $exists = WechatUserRepository::getByUnionIdExcludeType($wt->unionid, $relateType, true);
            if ($exists && $exists->u_yyid) {
                $eu = UsersRepository::getUserByYYID($exists->u_yyid);
                if (!$eu) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "微信绑定用户不存在");
                }
                if ($eu->mobile_num && $eu->mobile_num != $mobile) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该微信另一个端已经绑定其它手机号");
                }
                //TODO: 这里是否要先更一把user_yyid
            }
        }

        return $wt;
    }

    /**
     * 获取同一用户体系内另一类型的微信记录
     * @param $wechatType
     * @return int
     */
    public static function getRelatedWechatType($wechatType): int
    {
        switch ($wechatType) {
            case DataStatus::WECHAT_AGENT:
                return DataStatus::WECHAT_APP;
            case DataStatus::WECHAT_APP:
                return DataStatus::WECHAT_AGENT;
            default:
                throw new BusinessException(ErrorCode::SERVER_ERROR, "暂时不不支持该类型微信");
        }
    }

    private static function refreshWechatInfo($openId, $wechatType): ?WechatUser
    {
        //调用微信接口获取微信信息
        /**
         * @var WechatBusiness $wb
         */
        $wb = ApplicationContext::getContainer()
        ->get(WechatBusiness::class);
        $wechatInfo = $wb->getWechatInfo($openId, $wechatType);
        if ($wechatInfo) {
            return WechatUserRepository::findUpdateOrAddSubscribe($wechatInfo, $wechatType);
        }
        return null;
    }


    /**
     * 带分页用户列表
     * @param int $companyId
     * @param string $keywords
     * @param int $current
     * @param int $limit
     */
    public static function listUsersWithPage(
        $companyId = 0,
        $keywords = "",
        $vStatus = -1,
        $current = 1,
        $limit = 10,
        $submittedType = 0,
        $realType = 0
    ): UserListReponse {
        $company = null;
        if ($companyId) {
            $company = CompanyRepository::getById($companyId);
        }
        $resp = UsersRepository::getUsersWithPage(
            $keywords,
            $company,
            $vStatus,
            $current,
            $limit,
            $submittedType,
            $realType
        );

        $page = new CommonPageResponse();
        $page->setSize($limit);
        $page->setPages($resp->lastPage());
        $page->setCurrent($current);
        $page->setTotal($resp->total());

        $data = new UserListReponse();
        $data->setPage($page);
        if ($resp->isEmpty()) {
            return $data;
        }

        /** @var Collection $collect */
        $collect = $resp->getCollection();

        //批量获取公司信息
        $validCompanyYYIDS = $collect->reduce(function ($carry, Users $item) {
            $item->company_yyid && $carry[] = $item->company_yyid;
            return $carry;
        }, []);
        $companyMap  =[];
        if (!empty($validCompanyYYIDS)) {
            $company = CompanyRepository::getByYYIDS($validCompanyYYIDS);
            $companyMap = $company->keyBy('yyid')->all();
        }

        //批量获取微信信息
        $userYYIDS = $collect->pluck('yyid')->all();
        $wechat = WechatUserRepository::getUserAllWechat($userYYIDS);
        $wechatMap = $wechat->groupBy('u_yyid')->all();

        $users = [];
        /** @var Users $item */
        foreach ($resp->items() as $item) {
            $itemCompanyId = 0;
            $itemCompanyName= '';
            if ($item->company_yyid && isset($companyMap[$item->company_yyid])) {
                $itemCompanyId = $companyMap[$item->company_yyid]->id;
                $itemCompanyName = $companyMap[$item->company_yyid]->name;
            }

            $i = new UserItem();
            $i->setId($item->uid);
            $i->setName($item->name);
            $i->setTrueName($item->truename);
            $i->setCompany($itemCompanyName);
            $i->setCompanyId($itemCompanyId);
            $i->setMobile($item->mobile_num);
            $i->setStatus($item->v_status);
            $i->setSubmittedRoleType($item->submitted_role_type ?? 0);
            $i->setRealRoleType($item->real_role_type ?? 0);
            $i->setCreateAt(date('Y-m-d H:i:s', $item->created));
            $i->setVerifyStatus(self::buildVerifyStatus($item));
            //微信列表
            if (isset($wechatMap[$item->yyid])) {
                $wechatItems = [];
                /** @var WechatUser $wi */
                foreach ($wechatMap[$item->yyid] as $wi) {
                    $wwi = new UserWechatItem();
                    $wwi->setId($wi->id);
                    $wwi->setNickname($wi->nickname);
                    $wwi->setSubscribe($wi->subscribe);
                    $wwi->setWechatType($wi->wechat_type);
                    $wwi->setCreateTime((string)$wi->created_at);
                    $wechatItems[] = $wwi;
                }
                $i->setWechat($wechatItems);
            }
            $users[] = $i;
        }

        $data->setUsers($users);

        return $data;
    }


    private static function buildVerifyStatus(Users $user): int
    {
        $verifyStatus = UserVerifyStatus::VERIFY_UNKNOW;
        switch ($user->is_agent) {
            case Users::AGENT_VERIFY_NO:
                $verifyStatus = UserVerifyStatus::VERIFY_UNKNOW;
                break;
            case Users::AGENT_VERIFY_OK:
                $verifyStatus = UserVerifyStatus::VERIFY_PASSED;
                break;
            case Users::AGENT_VERIFY_PROCESSING:
                $verifyStatus = UserVerifyStatus::VERIFY_APPLYING;
                break;
            case Users::AGENT_VERIFY_REFUSED:
                $verifyStatus = UserVerifyStatus::VERIFY_REFUSED;
                break;
        }

        return $verifyStatus;
    }

    public static function verifyOk(int $uid, $realRole)
    {
        $user = UsersRepository::getUserByUid($uid);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户不存在");
        }

        $agent = AgentRepository::findByUser($user);
        if (!$agent) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "认证信息不存在");
        }

        if ($user->is_agent == Users::AGENT_VERIFY_OK) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户已经认证完成");
        }

        $agentName = $agent->truename;
        $company = $user->company;
        if (! $company) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户公司配置错误或不存在");
        }
        Db::transaction(function () use ($user, $agent, $company, $realRole) {
            UsersRepository::updateUserAgent($user, Users::AGENT_VERIFY_OK);
            // 判断代表认证是否是供应商
//            $updateData = self::agentSupplier($user, $realRole);
            AgentRepository::agree($agent, [], $realRole);
            self::bindUserCompany($user, $company);
        });

        $wechat = WechatUserRepository::getOneWechatInfoByUserYYID($user->yyid, DataStatus::WECHAT_AGENT);
        if ($wechat) {
            $url  = Helper::buildYouyaoUrl('/uniapp/pages/mine/mine');
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendVerifyResult(
                    $wechat->openid,
                    date('Y-m-d H:i:s'),
                    $url,
                    $user->name,
                    $agentName,
                    true,
                    ''
                );
        }
    }

    public static function agentSupplier($user, $realRole)
    {
        $supplierId = 0;
        $isSupplier = 0;
        $isSupplierLeader = 0;
        if ($user->mobile_num) {
            $supplierInfo = SupplierRepository::getSupplierByPhone($user->mobile_num);
            if ($supplierInfo) {
                if ($supplierInfo->role_type!=0&&
                    $supplierInfo->role_type!=$realRole) {
                    throw new BusinessException(ErrorCode::ROLE_TYPE_NOT_SAME);
                }
                $leadInfo = SupplierRepository::getSupplierLeader($supplierInfo->id);
                $supplierId = $leadInfo->supplier_id;
                $isSupplier = $supplierInfo->type;
                $isSupplierLeader = $leadInfo->is_leader;
            }
        }

        return [
            'supplier_id' => $supplierId,
            'is_supplier' => $isSupplier,
            'is_supplier_leader' => $isSupplierLeader
        ];
    }

    private static function bindUserCompany(Users $users, Company $company)
    {
        if (!$users->mobile_num) {
            return;
        }
        //判断不存在用户公司则添加用户公司
        $userCompany = UserCompanyRepository::findAddOrUpdate($users, $company);

        //获取最新导入记录
        $import = DepartmentImportRepository::getLastActiveImport($company);
        if (!$import) {
            //之前未同步过数据
            return;
        }
        $bind = OrgBindingUserRepository::getUserBind($users, $company);
        if ($bind) {
            //绑定记录已经存在了, 重新绑定了
            return;
        }

        //查询是否已经存在绑定关系，如果存在则记录日志并提醒返回
        $unbind = ImportUserRepository::getUnBindByMobile($import, $users->mobile_num);
        if (!$unbind) {
            return;
        }

        if (in_array(
            $import->import_type,
            [DepartmentImport::IMPORT_TYPE_DINGTALK, DepartmentImport::IMPORT_TYPE_EXCEL]
        )) {
            //添加绑定
            $bindings = OrgBindingUserRepository::addBinding($users, $company, $import, $unbind->user_id);
            //更新导入用户关联帐号
            ImportUserRepository::updateImportUserAccount($import, $bindings);
            //查询部门人员记录
            $deptUserList = DepartmentUserRepository::getByCompanyAndUserUniq($company, $bindings->ding_user_id);

            if ($deptUserList->isEmpty()) {
                return;
            }
            //更新绑定部门人员帐号信息
            DepartmentUserRepository::batchUpdateAccountByImport($company, $users, $bindings->ding_user_id);

            //推送人员变动记录
            $param = new OrgUserChangeParams();
            $param->setAddUser($deptUserList->pluck('id')->all());
            TaskManageHelper::create(0, $param, 'org-user-change')
                ->push();
        }
    }

    public static function verifyRefused(int $uid, $reason, $adminId = 0)
    {
        $user = UsersRepository::getUserByUid($uid);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户不存在");
        }

        $agent = AgentRepository::findByUser($user);
        if (!$agent) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "认证信息不存在");
        }

        $agentName = $agent->truename;
        Db::transaction(function () use ($user, $agent, $reason, $adminId) {
            UsersRepository::updateUserAgent($user, Users::AGENT_VERIFY_REFUSED);
            RefuseRemarkRepository::addVerifyRefuse($user, $reason, $adminId);
        });

        $wechat = WechatUserRepository::getOneWechatInfoByUserYYID($user->yyid, DataStatus::WECHAT_AGENT);
        if ($wechat) {
            $url = Helper::buildYouyaoUrl('/uniapp/pages/mine/myData');
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendVerifyResult(
                    $wechat->openid,
                    date('Y-m-d H:i:s'),
                    $url,
                    $user->name,
                    $agentName,
                    false,
                    $reason
                );
        }
    }



    public static function getAdminUserVerifyInfo(int $uid): VerifyInfoResponse
    {
        //用户信息
        $user = UsersRepository::getUserByUid($uid);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户不存在");
        }

        //认证信息
        /** @var Agent $agent */
        list(
            $agent,
            $serveHospitals,
            $companyInfo,
            $city,
            $serveCompanies,
            $departItem,
            $directions
            ) = self::getUserVerifyDetail($user);

        $data = new VerifyInfoResponse();
        $data->setTrueName($agent->truename);
        $data->setSupplierId($agent->supplier_id);
        $data->setSubmittedRoleType($agent->submitted_role_type);
        $data->setRealRoleType($agent->real_role_type);
        if (!empty($serveHospitals)) {
            $hos = array_reduce($serveHospitals, function ($carry, $item) {
                $o = new HospitalItem();
                $o->setId($item['id']);
                $o->setName($item['name']);
                $carry[] = $o;
                return $carry;
            }, []);
            $data->setServeHospital($hos);
        }

        if (!empty($companyInfo)) {
            $o = new CompanyItem();
            $o->setId($companyInfo['id']);
            $o->setName($companyInfo['name']);
            $data->setCompany($o);
        }

        if (!empty($city)) {
            $o = new CityItem();
            $o->setId($city['id']);
            $o->setName($city['name']);
            $data->setCity($o);
        }

        if (!empty($departItem)) {
            $departs = array_reduce($departItem, function ($carry, $v) {
                $o = new DepartItem();
                $o->setId($v['id']);
                $o->setName($v['name']);
                $carry[] = $o;

                return $carry;
            }, []);
            $data->setDepartment($departs);
        }

        if (!empty($directions)) {
            $data->setDirections($directions);
        }

        if (!empty($serveCompanies)) {
            $comps = array_reduce($serveCompanies, function ($carry, $item) {
                $o = new ServeCompanyItem();
                $o->setId($item['id']);
                $o->setCompanyName($item['company_name']);
                $o->setProduct($item['product']);
                $o->setField($item['field']);
                $o->setAdaption($item['adaption']);
                $carry[] = $o;
                return $carry;
            }, []);
            $data->setServeCompany($comps);
        }

        return $data;
    }

    /**
     * @param Users $user
     * @return array
     */
    private static function getUserVerifyDetail(Users $user): array
    {
        $agent = AgentRepository::findByUser($user);
        if (!$agent) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户认证信息不存在");
        }

        //处理医院
        $familiarHospitals = RepsFamiliarHospitalRepository::getUserFamiliarHospitals($user);
        $serveHospitals = [];
        $hospitalYYIDS = $familiarHospitals->pluck('hospital_yyid')->toArray();
        if (!empty($hospitalYYIDS)) {
            $hospitals = YouyaoHospitalRepository::getHospitalByYYIDs($hospitalYYIDS);

            /** @var YouyaoHospital $v */
            foreach ($hospitals as $v) {
                $serveHospitals[] = [
                    'id' => $v->id,
                    'name' => $v->hospital_name
                ];
            }
        }
        //获取公司信息
        $company = CompanyRepository::getByYYID($user->company_yyid);
        $companyInfo = [];
        if ($company) {
            $companyInfo = [
                'id' => $company->id,
                'name' => $company->name
            ];
        }

        // 可服务城市
        $city = [];
        if ($agent->service_region) {
            $region = RegionRepository::getByYYID($agent->service_region);
            if ($region) {
                $city = [
                    'id' => $region->id,
                    'name' => $region->region_name
                ];
            }
        }
        // serve_company
        $serveCompanies = self::getUserServeCompanies($user);

        //部门
        $departsIds = $agent->familiar_offices ? explode(',', $agent->familiar_offices) : [];
        $departItem = [];
        if (!empty($departsIds)) {
            $departs = SystemDepartRepository::getByIds($departsIds);
            /** @var SystemDepart $v */
            foreach ($departs as $v) {
                $departItem[] = [
                    'id' => $v->id,
                    'name' => $v->name
                ];
            }
        }

        $directions = [];
        $directIDS = explode(',', (string)$agent->serve_direction);
        if (!empty($directIDS)) {
            if (in_array(Agent::DIRECTION_HOSPITAL_LISTING, $directIDS)) {
                $directions[] = '医学列名服务';
            }

            if (in_array(Agent::DIRECTION_MARKETING_ACADEMIC, $directIDS)) {
                $directions[] = '学术营销服务';
            }
        }
        return array($agent, $serveHospitals, $companyInfo, $city, $serveCompanies, $departItem, $directions);
    }

    /**
     * @param $type
     * @param $content
     * @param string $email
     * @throws Exception
     */
    public static function addHelp($type, $content, $email = "")
    {
        $user = Helper::getLoginUser();
        Db::beginTransaction();
        try {
            //添加求助记录
            $reps = RepsAcademicHelpRepository::addHelp($user, $type, $content, $email);

            //如果邮箱不为空，更新用户邮箱
            if ($email) {
                UsersRepository::updateEmail($user->yyid, $email);
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }

        //给审核人员发送微信消息
        AuditService::helpAudit($reps->id);
    }

    public static function updateUserName(string $name)
    {
        $user = Helper::getLoginUser();

        UsersRepository::updateName($user, $name);
    }

    /**
     * 取消认证
     * @param int $uid
     */
    public static function verifyCancel(int $uid)
    {
        $user = UsersRepository::getUserByUid($uid);
        $agent = SupplierRepository::getMemberRole($user->yyid);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户不存在");
        }
        if (!$user->is_agent == Users::AGENT_VERIFY_OK) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该用户尚未通过认证，无法取消");
        }

        Db::transaction(function () use ($user, $agent) {
            UsersRepository::cancelVerify($user);
            if ($agent->supplier_id!=0) {
                if ($agent->is_supplier_leader==1) {
                    throw new BusinessException(
                        ErrorCode::BUSINESS_ERROR,
                        "服务商管理员无法取消认证，请联系管理员"
                    );
                }
                // 清除供应商
                $member = SupplierRepository::getOneMemberInfoByYyid($agent->supplier_id, $user->yyid);
                if ($member) {
                    //解除server_s个人绑定的医院
                    $bindInfo = SupplierRepository::getMemberDetailByMemberId($member->id);
                    if ($bindInfo->isNotEmpty()) {
                        foreach ($bindInfo->toArray() as $info) {
                            $user = UsersRepository::getUserByUid($info['user_id']);
                            $hospital = SupplierRepository::getHospitalById($info['hospital_id']);
                            $product = DrugSeriesRepository::getById($info['product_id']);
                            // 解除个人绑定关系
                            SupplierRepository::cancelServe($user, $hospital, $product);
                        }
                    }
                    SupplierRepository::cancelBindMember($member->id);
                    SupplierRepository::removeMember($member->id);
                }
            }
            //更新t_agent表字段结算标识字段real_role_type改为0
            UsersRepository::updateRoleType($user);
            RepsServeScopeSRepository::cancelUserServer($user);
        });

        //取消认证后可能删除服务，因此要全量同步es医院数据
        ServeService::syncAllHospitalEs();
    }

    public static function alertChange()
    {
        $user = Helper::getLoginUser();

        UsersRepository::alertChange($user);
    }

    /**
     * @return array
     */
    public static function getServiceProduct()
    {
        $user = Helper::getLoginUser();
        $res = UsersRepository::getServiceProduct($user->yyid);
        return $res->isNotEmpty() ? $res->toArray() : [];
    }

    /**
     * @return array
     */
    public static function getUserVerifyState()
    {
        $hasMobile = $verifyState = 0;
        $user = Helper::getLoginUser();
        $userInfo = UsersRepository::getUserByYYID($user->yyid);
        if ($userInfo) {
            if ($userInfo->mobile_num) {
                $hasMobile = 1;
            }
            $verifyState = $user->is_agent;
        }

        return [
            'has_mobile' => $hasMobile,
            'verify_state' => $verifyState
        ];
    }
}
