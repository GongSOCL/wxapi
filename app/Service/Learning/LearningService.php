<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 * $GrandTotaVal = date('Y-m-d').'&'.$plan_id.'&'.$mid.'&'.$learn_time.'&'.$page.'&'.$type.'&'.$seepTime;
 * $ContinuityVal = date('Y-m-d').'&'.$readDays.'&'.$plan_id.'&'.$mid.'&'.$seepTime;
 */
namespace App\Service\Learning;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ExaminationResult;
use App\Model\Qa\LearningMaterialsDistribution;
use App\Model\Qa\LearningPlan;
use App\Model\Qa\LearningPlanExam;
use App\Model\Qa\LearningPlanMsgNode;
use App\Model\Qa\NewKnowledge;
use App\Model\Qa\TDepartmentUser;
use App\Repository\Learning\LearningRepository;
use App\Service\Wechat\WechatBusiness;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

class LearningService
{

    /**
     * @Inject()
     * @var WechatBusiness
     */
    protected $wechatBusiness;

    /**
     * @Inject()
     * @var LearningRepository
     */
    protected $learningRepository;

    public function getKnowledgeList($keywords, $pid)
    {
        $where = [
            ['is_deleted' ,'=', 0],
            ['status' ,'=', 1],
        ];
        if ($keywords) {
            $where[] = ['title', 'like',"%{$keywords}%"];
        }

        return $this->learningRepository->getKnowledgeList($where, $keywords, $pid);
    }


    public function getKnowledgeInfo($id, $type, $learn_id)
    {
        # 查看缓存 当该文档在对应用户的阅读计划中 若不在 不用返回/记录         *** 通通不记录
        # 1、 缓存若有  取出阅读时间 / 阅读页码
        # 2、 缓存若无  存缓存

        # 判断是否在计划中
//        LearningMaterialsDistribution::where()->first();
        $info = $this->learningRepository->getKnowledgeInfo($id, $type);

        $read_time = $Mstatus = $page = $read = 0;
        $learnInfo = [];
        if ($learn_id && $type == 2) {
            $userId     = Helper::getLoginUser()->uid;

            $redis = Helper::getRedis();
            # 获取当前资料状态
            $MaterialStatus = LearningMaterialsDistribution::where(
                [
                    ['l_id',$learn_id],
                    ['m_id',$id],
                    ['user_id',$userId]
                ]
            )->select('status')->first();

            $Mstatus = $MaterialStatus['status'];
            # 取当前 资料阅读详情
            $GrandTotaKey = "learn:$userId:speed";
            $GrandTotaInfos = $redis->sMembers($GrandTotaKey);

            if ($GrandTotaInfos) {
                foreach ($GrandTotaInfos as $continuityKey) {
                    $Data = explode('&', $continuityKey);
                    if ($Data[1] == $learn_id && $Data[2] == $id) {
                        array_push($learnInfo, $Data);
                    }
                }
            }

            # 修改该计划该的状态 lr_st
            $this->learningRepository->updateLRSTByUidPlanId($id, $learn_id, $userId);
        }

        # 获取该资料的总时长 和 页码
        if ($learnInfo) {
            foreach ($learnInfo as $item) {
                $read_time+= $item[3];
            }
            $page = $this->searchmax($learnInfo, 4);
            $read = $this->searchmax($learnInfo, 3);
        }

        return ['info'=>$info,'read'=>$read,'read_time'=>$read_time,'page'=>$page,'m_status'=>$Mstatus];
    }

    /**
     * 取数组中最大值
     * @param $arr
     * @param $field
     * @return false|mixed
     */
    private function searchmax($arr, $field)
    {
        if (!is_array($arr) || !$field) {
            return false;
        }
        $temp = array();
        foreach ($arr as $key => $val) {
            $temp[] = $val[$field];
        }
        return max($temp);
    }

    /**
     * 取数组中最小值
     * @param $arr
     * @param $field
     * @return false|mixed
     */
    private function searchmin($arr, $field)
    {
        if (!is_array($arr)) {
            return false;
        }
        $temp = array();
        foreach ($arr as $key => $val) {
            $temp[] = $val[$field];
        }
        return min($temp);
    }

    /**
     * 学习时长相关数据
     * @return array
     */
    public function getLearningPlan()
    {
        $userId     = Helper::getLoginUser()->uid;
        $redis = Helper::getRedis();
        $GrandTotaKey = "learn:$userId:speed";      # 总数据   date&plan_ids&mid&learn_time&&page&type
        $ContinuityKey = "learn:$userId:continuity";    # 连续学习天数
        $GrandTotaInfos = $redis->sMembers($GrandTotaKey);
        $Continuit = $redis->get($ContinuityKey);

        $ToDayLearnMinute = 0;
        $LearnTotaMinute = $ContinuitDay = 0;    # 累计总时长 今日学习时长
        if ($GrandTotaInfos) {
            foreach ($GrandTotaInfos as $GrandTotaInfo) {
                $Data = explode('&', $GrandTotaInfo);
                $LearnTotaMinute+= $Data[3];
                if ($Data[0] == date('Y-m-d')) {
                    $ToDayLearnMinute += $Data[3];
                }
            }
        }

        if ($Continuit) {
            $Continuit = explode('&', $Continuit);
            $ContinuitDay = $Continuit[1];
        }
        # 查询多少完成的计划
        $PlanFinishList = $this->learningRepository->getPlanFinishList($userId);

        $finshIds = [];
        if ($PlanFinishList) {
            $learining = $all = [];
            if ($PlanFinishList['learining']['lear_id']) {
                $learining = explode(',', $PlanFinishList['learining']['lear_id']);
            }

            if ($PlanFinishList['all']['lear_id']) {
                $all = explode(',', $PlanFinishList['all']['lear_id']);
            }
            $finshIds = array_diff($all, $learining);
        }

        # 前端前世 已经完成数量可能出现和实际不同，原因 多次离职 入职，离职后 数据全部已过期，且已过去的计划可能有学习完成的资料，所以再入职时 筛选已完成时的计划可能为空，但是在页码显示已完成计划可能大于等于0
        return [
            'to_day_learn_minute'=>$ToDayLearnMinute,
            'learn_tota_minute'=>$LearnTotaMinute,
            'continuit_days'=>$ContinuitDay,
            'finsh_num'=>count($finshIds)
        ];
    }

    /**
     * 阅读记录写入
     * @param $type
     * @param $plan_id
     * @param $mid
     * @param $learn_time
     * @return bool
     */
    public function setLearningPlanReadRecord($type, $plan_id, $mid, $learn_time, $page, $m_status)
    {
        $userId     = Helper::getLoginUser()->uid;
        $redis = Helper::getRedis();
        if ($type == 2 && !$page) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "文档类型必传页码");
            return false;
        }

        # 详细时间 到秒，为继续学习提供数据
        $seepTime = date('Y-m-d H:i:s');
        # 判断该计划是否完成，未完成-记录。完成-不记录
        $info = $this->learningRepository->getPlanStatus($userId, $plan_id, $mid);

        $GrandTotaKey = "learn:$userId:speed";      # 总数据   date&plan_ids&mid&learn_time&type
        $GrandTotaVal = date('Y-m-d').'&'.$plan_id.'&'.$mid.'&'.$learn_time.'&'.$page.'&'.$type.'&'.$seepTime;
        $redis->sAdd($GrandTotaKey, $GrandTotaVal);


        # 处理 连续阅读天数  拉取之前数据 若之前的日期等于昨天日期 则阅读天数加一
        $ContinuityKey = "learn:$userId:continuity";    # 连续学习天数
        $Continuit = $redis->get($ContinuityKey);
        $readDays = 1;
        if ($Continuit) {
            $Continuit = explode('&', $Continuit);
            if ($Continuit[0] == date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))))) {
                $readDays+= $Continuit[1];
            }
        }
        $ContinuityVal = date('Y-m-d').'&'.$readDays.'&'.$plan_id.'&'.$mid.'&'.$seepTime;

        # 如果时间类型是2 【当手动点击了才开始计时】就把开始时间和计划的结束时间算出来更新到 learning_materials_distribution表中
        $plan = LearningPlan::where('id', $plan_id)
            ->select('dt_on', 'dt_off', 'one_day', 'last_day', 'node_two', 'node_one', 'date_type')
            ->first();

        if (!$plan) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "计划资料异常");
            return false;
        }

        if ($plan->date_type == 2) { # 更新用户推送信息表
            $day = $plan->dt_off;

            $last_day = $one_day = $node_one = $node_two = Db::raw("null");

            # 判断是否勾选推送信息节点
            if ($plan->one_day) {
                $one_day = date('Y-m-d');
            }

            if ($plan->last_day) {
                $last_day = date('Y-m-d', strtotime('+'.$day.' day', time()));
            }

            if ($plan->node_one) {
                $node_one = date('Y-m-d', strtotime('+'.$plan->node_one.' day', time()));
            }

            if ($plan->node_two) {
                $node_two = date('Y-m-d', strtotime('+'.$plan->node_two.' day', time()));
            }

            LearningMaterialsDistribution::where(
                [
                    ['l_id', $plan_id],
                    ['user_id', $userId]
                ]
            )->update(['sdt' => date('Y-m-d'), 'updated_at' => date('Y-m-d H:i:s')]);

            LearningPlanMsgNode::where(
                [
                    ['plan_id', $plan_id],
                    ['user_id', $userId]
                ]
            )->update(
                [
                    'node_one' => $node_one,
                    'node_two' => $node_two,
                    'last_day' => $last_day,
                    'one_day' => $one_day
                ]
            );
        }
        # 记录完成
        if ($m_status == 1) { # 资料
            if (!$info) {
                # 查找当前计划 除了当前接收到的资料id以外的资料状态
                $Plans = $this->learningRepository->getPlans($userId, $plan_id, $mid);
                if ($Plans) {
                    $Plans =  $Plans->toArray();

                    # 如果是已完成或者过期计划 跳过
                    if ($Plans[0]['lr_st'] != 6 || $Plans[0]['lr_st'] != 4) {
                        $status = 1;

                        $PlanStatus =  array_column($Plans, 'status');

                        if (in_array(1, $PlanStatus)) {
                            $status = 0;
                        }
                        if (in_array(2, $PlanStatus)) {
                            $status = 0;
                        }

                        # 若该用户 该计划的其他资料都已阅读完成 更新该计划完成
                        if ($status == 1) {
                            $this->learningRepository->updatePlanByUidPlanId($userId, $plan_id, $mid, $status);
                        } else {
                            # 更新计划下 指定资料状态
                            $this->learningRepository->updatePlanByUidPlanId($userId, $plan_id, $mid, $status);
                        }
                    }
                } else {
                    # 若未查到当前计划下 其他资料的状态 表示该计划下只有一个资料。   查询资料状态 不为4-6的
                    $PlansByMId = $this->learningRepository->getPlansByMid($userId, $plan_id, $mid);
                    if ($PlansByMId) {
                        $this->learningRepository->updatePlanByUidPlanId($userId, $plan_id, $mid, 1);
                    }
                }
            }
        }
        return $redis->set($ContinuityKey, $ContinuityVal);
    }


    /**
     * 计划详情
     * @param $lid
     * @return array
     */
    public function getPlanInfo($lid)
    {
        $userId     = Helper::getLoginUser()->uid;

        $where = [
            ['a.l_id' ,'=', $lid],
            ['a.user_id' ,'=', $userId],
            ['a.is_deleted' ,'=', 0]
        ];

        $wherePlanInfo = [
            ['l_id' ,'=', $lid],
            ['user_id' ,'=', $userId],
            ['is_deleted' ,'=', 0]
        ];
        $data = $this->learningRepository->getplanMaterialInfo($where);
        $logger = Helper::getLogger();
        $logger->error("Learn_plan_error2", [
            'body1' => $data,
        ]);
        $planInfo = $this->getPlanList('', $lid);
        $allTotal = count($data);
        $finshNum = 0;
        foreach ($data as $datum) {
            if ($datum['status'] == 3) {
                $finshNum++;
            }
        }
        $speed = 0;
        if ($finshNum> 0) {
            $speed = round($finshNum/$allTotal, 2) * 100 .'%';
        }

        $userId = Helper::getLoginUser()->uid;
        $redis = Helper::getRedis();
        $GrandTotaKey = "learn:$userId:speed";      # 总数据   date&plan_ids&mid&learn_time&&page&type
        $GrandTotaInfos = $redis->sMembers($GrandTotaKey);
        $total_tima = $ToDayLearnMinute = 0;
        if ($GrandTotaInfos) {
            foreach ($GrandTotaInfos as $continuityKey) {
                $Data = explode('&', $continuityKey);
                if ($Data[1] == $lid) {
                    $total_tima+= $Data[3];
                    if ($Data[0] == date('Y-m-d')) {
                        $ToDayLearnMinute+= $Data[3];
                    }
                }
            }
        }
        return [
            'data'=>$data,
            'plan'=>$planInfo[0],
            'speed'=>$speed,
            'total_tima'=>$total_tima,
            'today_learn_minute'=>$ToDayLearnMinute
        ];
    }



    # 用户完成每一个资料的时候 去数据库更新 lr_st = 6 表示该用户整个计划完成
    public function getPlanList($st, $lid)
    {
        $userId     = Helper::getLoginUser()->uid;
        $where = [
            ['a.user_id' ,'=', $userId],
            ['a.is_deleted' ,'=', 0]
        ];

        if ($lid) {
            $where[] = ['b.id' ,'=', $lid];
        }

        $data = $this->learningRepository->getPlanList($where);
        $logger = Helper::getLogger();
        $logger->error("Learn_plan_error2", [
            'body2' => $data,
        ]);

        $newDatas =  $this->learnListDetails($data, $userId);
        $logger->error("Learn_plan_error2", [
            'body3' => $newDatas,
        ]);
        # 最后一个接口 每次需要 去数据库判断该计划是否已全部完成  若完成 则更新  learning_materials_distribution 中 的 lr_st = 6

        # 计算百分比
        foreach ($newDatas as &$newData) {
            # 计算百分比
            if ($newData['check_status'] == 1 || $newData['check_status'] == 4) {
                $where = [
                    ['l_id' ,'=', $newData['id']],
                    ['user_id' ,'=', $userId],
                    ['is_deleted' ,'=', 0]
                ];
                $data = $this->learningRepository->getplanInfo($where);
                $allTotal = count($data);
                $finshNum = 0;
                foreach ($data as $datum) {
                    if ($datum['status'] == 3) {
                        $finshNum++;
                    }
                }
                $speed = 0;
                if ($finshNum> 0) {
                    $speed = round($finshNum/$allTotal, 2) * 100 .'%';
                }
                $newData['speed'] = $speed;
            }
            if ($newData['check_status'] == 3) {
                $newData['speed'] = '100%';
            }
        }

        $list  = [];
        if ($st == 1) {   # 进行中
            foreach ($newDatas as $itm) {
                if ($itm['check_status'] == 1) {
                    array_push($list, $itm);
                }
            }
            # 计算百分比
        } elseif ($st == 2) {  # 未开始
            foreach ($newDatas as $itm) {
                if ($itm['check_status'] == 2) {
                    array_push($list, $itm);
                }
            }
        } elseif ($st == 3) { # 完成
            foreach ($newDatas as $itm) {
                if ($itm['check_status'] == 3) {
                    array_push($list, $itm);
                }
            }
        } elseif ($st == 4) { # 过期
            foreach ($newDatas as $itm) {
                if ($itm['check_status'] == 4) {
                    array_push($list, $itm);
                }
            }
        } else {
            # 全部
            $list = $newDatas;
        }
        return $list;
    }

    private function learnListDetails($data, $userId)
    {
        $userId     = Helper::getLoginUser()->uid;
        $redis = Helper::getRedis();
        # lr_st = 1 进行中
        # lr_st = 6 完成
        $screenData = [];

        $userYYID   = Helper::getLoginUser()->yyid;

        foreach ($data as $k => $datum) {
            # Todo 查找当前用户 是否在计划关联的考试中
            # 获取计划的所有考试
            if ($datum['aexam_id']) {
                $ExamInfo = ComplianceExam::where('id', $datum['aexam_id'])->select('name')->first();

                if (!$ExamInfo) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试信息有误");
                }

                $data[$k]['exam_name'] = $ExamInfo->name;
            }

            if ($datum['lr_st'] == 6) {
                $data[$k]['check_status'] = 3;

                continue;
            }

            if ($datum['lr_st'] == 4) {   # 权限分配时 删除的用户资料状态，或者是 注销用户
                $data[$k]['check_status'] = 4;
                continue;
            }

            # `date_type`  '时间类型1固定时长字段开始 2固定时长手动 3固定时间',
            if ($datum['lr_st'] !=6 && $datum['lr_st'] != 4) {
                # 时间类型1固定时长字段开始 入职时间 大于设定开始时间  或小于开始时间  或 大于小于结束时间状态
                if ($datum['date_type'] == 1) {
                    # 查看入职时间  【如果不存在取查产品关联表  但目前 产品过来的用户 都没有入职时间】
                    $userInfo = LearningMaterialsDistribution::where(
                        [
                            ['user_id', $userId],
                            ['l_id',$datum['id']]
                        ]
                    )->first();
                    if (!$userInfo) {
                        throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户信息有误");
                        return false;
                    }
                    if (!$userInfo['employment_date']) {
                        throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户入职信息有误");
                        return false;
                    }

                    # 当前时间和入职时间的差  若
                    $sdt = date(
                        'Y-m-d',
                        strtotime(
                            '+'.$datum['dt_on'].'day',
                            strtotime($userInfo['employment_date'])
                        )
                    );   # 计划开始时间
                    $edt = date('Y-m-d', strtotime('+'.$datum['dt_off'].'day', strtotime($sdt)));   # 计划结束时间
                    $today = date('Y-m-d');

                    # 1进行中2未开始3完成4过期
                    if ($sdt > $today) {
                        $data[$k]['check_status'] = 2;
                    } elseif ($edt < $today) {
                        $data[$k]['check_status'] = 4;
                    } elseif ($sdt <= $today && $edt > $today) {
                        $data[$k]['check_status'] = 1;
                    }
                } elseif ($datum['date_type'] == 2) {
                    # 当用户点击阅读后 开始计算时间
                    $GrandTotaKey = "learn:$userId:speed";  # date&plan_ids&mid&learn_time&&page&type&datetime
                    $GrandTotaInfos = $redis->sMembers($GrandTotaKey);
                    $lData = [];
                    if ($GrandTotaInfos) {
                        foreach ($GrandTotaInfos as $continuityKey) {
                            $Data = explode('&', $continuityKey);
                            if ($Data[1] == $datum['id']) {
                                $Data[0] = strtotime($Data[0]);    # 把日期转时间戳
                                array_push($lData, $Data);
                            }
                        }
                    } else {
                        $data[$k]['check_status'] = 2;  # 若没有缓存 则未开始 跳过
                        continue;
                    }

                    if ($lData) {
                        $startDate = $this->searchmin($lData, 0);    # 获取用户第一次阅读时间
                        # 计算 第一次阅读时间 距离当前时间
                        $cdt = $this->getCountDays(strtotime(date('Y-m-d')), $startDate);
                        # 距离当前时间 天数 是否大于 规定时长天数
                        # 大于 则过期
                        if ($cdt > 0) {
                            if ($cdt >= $datum['dt_off']) {
                                $data[$k]['check_status'] = 4;  # 过期
                            } elseif ($cdt < $datum['dt_off']) {
                                $data[$k]['check_status'] = 1;  # 进行中
                            }
                        } else {
                            $data[$k]['check_status'] = 1;  # 进行中
                        }
                    } else {
                        $data[$k]['check_status'] = 2;  # 若没有缓存 则未开始 跳过
                        continue;
                    }
                } elseif ($datum['date_type'] == 3) {
                    # 有开始时间 结束时间 根据俩个值 判断 状态

                    # 当前时间大于等于 计划开始时间并 小于计划结束时间 则进行中
                    if (strtotime(date('Y-m-d')) >= strtotime($datum['dt_on'])
                        && strtotime(date('Y-m-d')) < strtotime($datum['dt_off'])) {
                        $data[$k]['check_status'] = 1;  # 进行中
                    } elseif (strtotime(date('Y-m-d')) < strtotime($datum['dt_on'])) { # 当前时间 小于计划开始时间 则 未开始
                        $data[$k]['check_status'] = 2;
                    } elseif (strtotime(date('Y-m-d')) >= strtotime($datum['dt_off'])) { # 当前时间 大于 结束时间 则过期
                        $data[$k]['check_status'] = 4;
                    }
                } elseif ($datum['date_type'] == 4) {  # 不限时
                    # 判断 缓存是否阅读 若有 则进行中。 若无 则未开始
                    $GrandTotaKey = "learn:$userId:speed";
                    $GrandTotaInfos = $redis->sMembers($GrandTotaKey);
                    $data[$k]['check_status'] = 2;  # 若没有缓存 则未开始
                    if ($GrandTotaInfos) {
                        foreach ($GrandTotaInfos as $continuityKey) {
                            $Data = explode('&', $continuityKey);
                            if ($Data[1] == $datum['id']) {
                                $data[$k]['check_status'] = 1;
                                continue;
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }


    /**
     * 时间差
     * @param $sTime
     * @param $eTime
     * @return float
     */
    private function getCountDays($sTime, $eTime)
    {
        $startDt = getdate($sTime);
        $endDt = getdate($eTime);
        $sUTime = mktime(12, 0, 0, $startDt['mon'], $startDt['mday'], $startDt['year']);
        $eUTime = mktime(12, 0, 0, $endDt['mon'], $endDt['mday'], $endDt['year']);
        return round(abs($sUTime - $eUTime) / 86400);
    }


    /**
     * 继续阅读
     * @return array|false|int[]
     */
    public function getKeepLearnInfo($plan_id)
    {
        $userId     = Helper::getLoginUser()->uid;
        $redis = Helper::getRedis();
        $ContinuityKey = "learn:$userId:continuity";    # 连续学习天数
        $Continuit = $redis->get($ContinuityKey);
        # $ContinuityVal = date('Y-m-d').'&'.$readDays.'&'.$plan_id.'&'.$mid.'&'.$seepTime;

        if ($Continuit) {
            $Continuit = explode('&', $Continuit);
            $field = ["id","pid","title","type","file_type","file_address","introduce","sort","created_at"];
            if (!$Continuit[2] || !$Continuit[3]) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户近期阅读数据异常");
                return false;
            }
            $NewKnowledges = NewKnowledge::where('id', $Continuit[3])->select($field)->first();

            # 查询该资料是否完成
            $data = $this->learningRepository->getMaterialStatus($userId, $Continuit[2], $Continuit[3]);
            if ($data) {
                # 获取 该资料的进度
//                $GrandTotaVal = date('Y-m-d').'&'.$plan_id.'&'.$mid.'&'.$learn_time.'&'.$page.'&'.$type.'&'.$seepTime;
                $GrandTotaKey = "learn:$userId:speed";
                $GrandTotaInfos = $redis->sMembers($GrandTotaKey);
                $lData = $speedData = [];
                if ($GrandTotaInfos) {
                    foreach ($GrandTotaInfos as $k => $continuityKey) {
                        $Data = explode('&', $continuityKey);
                        if ($Data[1] == $plan_id) {
                            $lData[$k]['learn_time'] = $Data[3];
                            $lData[$k]['page'] = $Data[4];
                            $lData[$k]['type'] = $Data[5];
                            $lData[$k]['speed_time'] = strtotime($Data[6]);
                        }
                    }

                    $startDate = $this->searchmax($lData, 'speed_time');    # 获取用户第一次阅读时间

                    $lData = array_column($lData, null, 'speed_time');
                    $speedData = $lData[$startDate];
                }
                return ['status' => 1, 'speed' => $speedData, 'info' => $NewKnowledges];
            }
        } else {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户近期阅读数据异常");
            return false;
        }
    }
}
