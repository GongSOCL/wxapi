<?php
declare(strict_types=1);
namespace App\Service\Meeting;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\MeetingApply;
use App\Model\Qa\MeetingAuditEvent;
use App\Model\Qa\MeetingParticipator;
use App\Model\Qa\MeetingTimeline;
use App\Model\Qa\Users;
use App\Repository\AgentRepository;
use App\Repository\DrugProductRepository;
use App\Repository\Meeting\MeetingApplyRepository;
use App\Repository\Meeting\MeetingAuditEventRepository;
use App\Repository\Meeting\MeetingParticipatorRepository;
use App\Repository\Meeting\MeetingTimelineRepository;
use App\Repository\SystemDepartRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Service\Doctor\DoctorInfoRpcService;
use Carbon\Carbon;
use Grpc\Area\CommonIdNameReply;
use Grpc\Common\CommonPageResponse;
use Grpc\DoctorInfo\UserInfoItem;
use Grpc\Livetype\CommonInfo;
use Grpc\Wxapi\Meeting\ApplyDetailReply;
use Grpc\Wxapi\Meeting\ApplyItem;
use Grpc\Wxapi\Meeting\ApplyListReply;
use Grpc\Wxapi\Meeting\ApplyUserItem;
use Grpc\Wxapi\Meeting\TimeItem;
use Hyperf\DbConnection\Db;

class MeetingService
{

    public static function listApply($zoneId = 0, $onlySelf = false, $status = 0, $current = 1, $limit = 10): array
    {
        $user = Helper::getLoginUser();

        $resp = MeetingApplyRepository::listApply($zoneId, $onlySelf ? $user->uid : 0, $status, $current, $limit);
        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $limit
        ];
        $data = [];
        if ($resp->isEmpty()) {
            return [$page, $data];
        }

        $meetingIds = $userIds = $zoneIds = $types = [];
        /** @var MeetingApply $item */
        foreach ($resp->items() as $item) {
            $meetingIds[] = $item->id;
            $userIds[] = $item->applicant_id;
            $zoneIds[] = $item->zone_id;
            if ($item->meeting_type > 0) {
                $types[] = $item->meeting_type;
            }
        }
        $userIds = array_unique($userIds);
        $zoneIds = array_unique($zoneIds);
        $types = array_unique($types);

        //获取会议开始时间最小值，结束时间最大值
        $timeResp = MeetingTimelineRepository::getMultiMeetingStartEnd($meetingIds);
        $timeMap = $timeResp->keyBy('meeting_id')->all();

        //获取用户数据
        $userMap = self::getApplicantNameMap($userIds);

        $typeMap = [];
        if (!empty($types)) {
            $typeMap = self::getLiveTypeMapByIds($types);
        }

        $zoneMap = [];
        if (!empty($zoneIds)) {
            $zoneMap = self::getZoneMap();
        }

        /** @var MeetingApply $item */
        foreach ($resp->items() as $item) {
            $start = $end = "";
            if (isset($timeMap[$item->id])) {
                $start = $timeMap[$item->id]->start;
                $end = $timeMap[$item->id]->end;
            }
            $typeName = isset($typeMap[$item->meeting_type]) ? $typeMap[$item->meeting_type]->getName() : '';
            $zoneName= $zoneMap[$item->zone_id] ?? '';
            $data[] = [
                'id' => $item->id,
                'name' => $item->name,
                'applicant_id' => $item->applicant_id,
                'applicant_name' => $userMap[$item->applicant_id] ?? '',
                'apply_time' => (string) $item->created_at,
                'start' => $start,                  //需要单独获取
                'end' => $end,
                'zone' => [
                    'id' => $item->zone_id,
                    'name' => $zoneName,
                ],
                'type' => [
                    'id' => $item->meeting_type,
                    'name' => $typeName
                ],
                'status' => $item->state
            ];
        }


        return [$page, $data];
    }

    public static function getLiveTypeMapByIds(array $typeIds): array
    {
        $rpc = new LiveTypeRpcService();
        $resp = $rpc->getByIds($typeIds);
        $data = [];
        /** @var CommonInfo $item */
        foreach ($resp->getItem() as $item) {
            $data[$item->getId()] = $item;
        }

        return $data;
    }

    private static function getZoneMap(): array
    {
        $rpc = new AreaRpcService();
        $areas = $rpc->listAllArea();
        $data = [];
        /** @var CommonIdNameReply $area */
        foreach ($areas->getList() as $area) {
            $data[$area->getId()] = $area->getName();
        }
        return $data;
    }

    public static function addApply(
        $mobile,
        $managerId,
        $compereId,
        $type,
        $subType,
        $categoryId,
        $productId,
        $name,
        $rangeType,
        array $time,
        $zoneId,
        $provinceId,
        $hospitalId,
        $departId,
        array $speaker,
        array $chairman = []
    ): MeetingApply {
        $user = Helper::getLoginUser();

        //检验人员
        $userMap = self::getDoctorUserMapByIds([
           $managerId,
           $compereId
        ]);
        if (!(isset($userMap[$managerId]) && isset($userMap[$compereId]))) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医生不存在");
        }

        //检验类型
        $max = self::getTypeMax($categoryId);

        //校验药品
        $product = DrugProductRepository::getById($productId);
        if (!$product) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "药品不存在");
        }

        //校验区域及省份
        $areaRpc = new AreaRpcService();
        if (!$areaRpc->checkArea($zoneId, $provinceId)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '大区或省份参数错误或不存在');
        }

        //校验医院
        self::checkMeetingHospitals($hospitalId, $speaker, $chairman);

        //校验科室
        self::checkDepartment($departId, $chairman, $speaker);

        //校验时间序列
        $minTime = self::checkTimesConflict($time, $categoryId, $max);

        Db::beginTransaction();
        try {
            //添加申请
            $apply = MeetingApplyRepository::addApply(
                $user,
                $mobile,
                $name,
                $managerId,
                $compereId,
                $type,
                $subType,
                $categoryId,
                $zoneId,
                $provinceId,
                $hospitalId,
                $departId,
                $productId,
                $rangeType,
                0,
                $minTime
            );

            //批量添加时间计划
            MeetingTimelineRepository::batchAdd($apply, $time);

            //添加主席讲者
            if ($chairman) {
                $ch = MeetingParticipatorRepository::addUser(
                    $apply,
                    $chairman['name'],
                    (int) $chairman['hospital_id'],
                    (int) $chairman['depart_id'],
                    (int) $chairman['level_id']
                );
                MeetingApplyRepository::setChairman($apply, $ch);
            }

            //添加讲者
            MeetingParticipatorRepository::batchAdd($apply, $speaker);
            MeetingAuditEventRepository::addApplyEvent($apply, MeetingAuditEvent::EVENT_TYPE_NEW, $user->uid);
            Db::commit();

            return $apply;
        } catch (\Throwable $e) {
            Db::rollBack();
            throw $e;
        }
    }

    private static function getDoctorUserMapByIds(array $ids): array
    {
        $rpc = new DoctorInfoRpcService();
        $resp = $rpc->getByIds($ids);
        $data = [];
        /** @var UserInfoItem $user */
        foreach ($resp->getUsers() as $user) {
            $data[$user->getId()] = $user;
        }
        return $data;
    }

    /**
     * @param $hospitalId
     * @param array $chairman
     * @param $speaker
     * @return void
     */
    private static function checkMeetingHospitals($hospitalId, array $speaker, array $chairman = []): void
    {
        $hospitalIds = [$hospitalId];
        if (!empty($chairman)) {
            $hospitalIds[] = $chairman['hospital_id'];
        }
        foreach ($speaker as $sp) {
            $hospitalIds[] = $sp['hospital_id'];
        }
        $hospitals = YouyaoHospitalRepository::getHospitalByIds($hospitalIds);

        //校验会议医院
        $hospitalMap = $hospitals->keyBy('id')->all();
        if (!isset($hospitalMap[$hospitalId])) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }

        if ((!empty($chairman)) && (!isset($hospitalMap[$chairman['hospital_id']]))) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS, "主席医院不存在");
        }

        foreach ($speaker as $sp) {
            if (!isset($hospitalMap[$sp['hospital_id']])) {
                throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS, '讲者医院不存在');
            }
        }
    }

    /**
     * @param $departId
     * @param array $chairman
     * @param array $speaker
     * @return void
     */
    private static function checkDepartment($departId, array $chairman, array $speaker): void
    {
        $departIds = [$departId];
        if (!empty($chairman)) {
            $departIds[] = $chairman['depart_id'];
        }
        foreach ($speaker as $sp) {
            $departIds[] = $sp['depart_id'];
        }

        $departs = SystemDepartRepository::getByIds($departIds);
        $departMap = $departs->keyBy('id')->all();
        if (!isset($departMap[$departId])) {
            throw new BusinessException(ErrorCode::DEPART_NOT_EXISTS);
        }
        if ((!empty($chairman)) && (!isset($departMap[$chairman['depart_id']]))) {
            throw new BusinessException(ErrorCode::DEPART_NOT_EXISTS, "主席讲者科室不存在");
        }
        foreach ($speaker as $sp) {
            if (!isset($departMap[$sp['depart_id']])) {
                throw new BusinessException(ErrorCode::DEPART_NOT_EXISTS, "讲者科室不存在");
            }
        }
    }

    private static function checkTimesConflict(array $times, $categoryId, $max = 1): string
    {
        $minTime = $times[0]['start'];
        $maxTime = $times[0]['end'];
        if ($max == 0) {
            return $minTime;
        }

        foreach ($times as $time) {
            // 检查和
            if ($time['start'] < $minTime) {
                $minTime = $time['start'];
            }
            if ($time['end'] > $maxTime) {
                $maxTime = $time['end'];
            }
        }

        //取得跨时间区间的所有记录
        $resp = MeetingTimelineRepository::getBetween($categoryId, $minTime, $maxTime);
        /** @var MeetingTimeline $item */
        foreach ($resp as $item) {
            $times[] = [
              'id' => $item->id,
              'start' => (string) $item->start,
              'end' => (string) $item->end
            ];
        }

        $column = array_column($times, 'start');
        array_multisort($column, SORT_ASC, SORT_REGULAR, $times);

        $cnt = 0;
        foreach ($times as $k => $v) {
            if (!isset($times[$k+1])) {
                return $minTime;
            }
            $next = $times[$k+1];
            $cid = isset($v['id']) ? intval($v['id']) : 0;
            $nid = isset($next['id']) ? intval($next['id']) : 0;
            if ($cid > 0 && $nid > 0 && $cid == $nid) {
                //同一条记录编辑时间，不用校验时间冲突
                continue;
            }
            if ($v['end'] >= $next['start']) {
                $cnt += 1;
                if ($cnt > $max) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该时间段内会议已经排满了，请更换重试");
                }
            }
        }

        return $minTime;
    }

    public static function editApply(
        $id,
        $mobile,
        $managerId,
        $compereId,
        $type,
        $subType,
        $categoryId,
        $productId,
        $name,
        $rangeType,
        array $time,
        $zoneId,
        $provinceId,
        $hospitalId,
        $departId,
        array $speaker,
        array $chairman = []
    ) {
        $user = Helper::getLoginUser();

        $apply = MeetingApplyRepository::getById($id);
        if (!$apply) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '会议申请不存在');
        }
        if ($apply->applicant_id != $user->uid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '会议申请不存在');
        }
        if (!self::isTimeValidEdit($apply)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '会议已经开始，无法编辑');
        }
        if (!$apply->isValidToEdit()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '当前申请状态不可编辑');
        }

        $timeAdd = $timeEdit = $delTimeIds = [];
        $minTime = "";
        //全量编辑判断排期时间变更
        if ($apply->isValidToEditAll()) {
            //允许编辑所有状态
            $max = self::getTypeMax($categoryId);
            $minTime = self::checkTimesConflict($time, $categoryId, $max);
            [$delTimeIds, $timeAdd, $timeEdit]= self::diffApplyTimes($apply, $time);
        }

        //校验人员
        $um = self::getDoctorUserMapByIds([$managerId, $compereId]);
        if (!(isset($um[$managerId]) && isset($um[$compereId]))) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '管理员或主持人不存在');
        }

        //校验医院
        self::checkMeetingHospitals($hospitalId, $speaker, $chairman);

        //校验科室
        self::checkDepartment($departId, $chairman, $speaker);

        //对比讲者
        [$delSpIds, $addSps, $updateSps]  = self::diffApplySpeaker($apply, $speaker);

        //查询当前已经关联
        $chairmanUser = null;
        if ($apply->chairman_id > 0) {
            $chairmanUser = MeetingParticipatorRepository::getById($apply->chairman_id);
        }
        //判断是否有状态流转
        $recordEvent = $apply->state == MeetingApply::STATE_FAIL;

        Db::beginTransaction();
        try {
            //编辑会议申请
            $updateAll = $apply->isValidToEditAll();
            $apply = MeetingApplyRepository::updateApplyBase(
                $apply,
                $updateAll ? $name : $apply->name,
                $updateAll ? $mobile : $apply->applicant_mobile,
                $managerId,
                $compereId,
                $updateAll ? $type : $apply->meeting_type,
                $updateAll ? $subType : $apply->meeting_sub_type,
                $updateAll ? $categoryId : $apply->meeting_category,
                $zoneId,
                $provinceId,
                $hospitalId,
                $departId,
                $updateAll ? $productId : $apply->series_id,
                $rangeType,
                $apply->chairman_id,
                $updateAll ? MeetingApply::STATE_NEW : $apply->state,
                $minTime
            );

            //编辑主席讲者
            if ($chairmanUser && (!empty($chairman))) {
                //更新
                MeetingParticipatorRepository::updateParticipator(
                    $chairmanUser,
                    $chairman['name'],
                    $chairman['hospital_id'],
                    $chairman['depart_id'],
                    $chairman['level_id']
                );
            } elseif ($chairmanUser && empty($chairman)) {
                //删除主席读者
                MeetingParticipatorRepository::deleteByIds([$chairmanUser->id]);

                //更新申请记录主席讲者为空
                MeetingApplyRepository::setChairman($apply);
            } elseif ((!$chairmanUser) && (!empty($chairman))) {
                //添加主席讲者
                $chairmanUser = MeetingParticipatorRepository::addUser(
                    $apply,
                    $chairman['name'],
                    $chairman['hospital_id'],
                    $chairman['depart_id'],
                    $chairman['level_id']
                );
                MeetingApplyRepository::setChairman($apply, $chairmanUser);
            }

            //编辑时间
            if (!empty($delTimeIds)) {
                MeetingTimelineRepository::deleteByIds($delTimeIds);
            }
            if (!empty($timeAdd)) {
                MeetingTimelineRepository::batchAdd($apply, $timeAdd);
            }
            if (!empty($timeEdit)) {
                /** @var MeetingTimeline $te */
                foreach ($timeEdit as $te) {
                    $te->save();
                }
            }
            if ($updateAll) {
                MeetingTimelineRepository::updateApplyCategory($apply);
            }

            //编辑讲者
            if (!empty($delSpIds)) {
                MeetingParticipatorRepository::deleteByIds($delSpIds);
            }
            if (!empty($addSps)) {
                MeetingParticipatorRepository::batchAdd($apply, $addSps, MeetingParticipator::TYPE_SPEAKER);
            }
            if (!empty($updateSps)) {
                foreach ($updateSps as $up) {
                    $up->save();
                }
            }

            if ($recordEvent) {
                //记录重新提交事件
                MeetingAuditEventRepository::addApplyEvent(
                    $apply,
                    MeetingAuditEvent::EVENT_TYPE_RE_NEW,
                    $user->uid,
                    MeetingAuditEvent::PLATFORM_WXAPI
                );
            }

            Db::commit();
        } catch (\Throwable $e) {
            Db::rollBack();
            throw $e;
        }
    }

    private static function isTimeValidEdit(MeetingApply $apply): bool
    {
        $minResp = MeetingTimelineRepository::getApplyMinStart($apply);
        if (!$minResp) {
            return true;
        }
        return strval($minResp->start) > date('Y-m-d H:i:s');
    }

    private static function diffApplySpeaker(MeetingApply $apply, array $speaker): array
    {
        $existSpeakers = MeetingParticipatorRepository::getApplyParticipator($apply, MeetingParticipator::TYPE_SPEAKER);
        $map = $existSpeakers->keyBy('id')->all();
        $existIds = $existSpeakers->pluck('id')->all();

        $update = [];
        $spIds = [];
        $add = [];
        foreach ($speaker as $sp) {
            if (isset($sp['id']) && $sp['id'] > 0 && isset($map[$sp['id']])) {
                $spIds[] = $sp['id'];
                /** @var MeetingParticipator $o */
                $o = $map[$sp['id']];
                $o->hospital_id = $sp['hospital_id'];
                $o->depart_id = $sp['depart_id'];
                $o->level_id = $sp['level_id'];
                $o->name = $sp['name'];
                $o->project = $sp['project'];
                $update[] = $o;
            } else {
                $add[] = $sp;
            }
        }

        [$delIds, $addIds] = Helper::diff($existIds, $spIds);

        return [$delIds, $add, $update];
    }

    public static function info(int $id): array
    {
        $user = Helper::getLoginUser();
        $apply = MeetingApplyRepository::getById($id);
        if (!$apply) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '会议申请不存在');
        }

        $applicantName = self::getApplicantName($apply);

        $hospitalIds = [$apply->hospital_id];
        $departIds = [$apply->depart_id];

        $chairman = null;

        if ($apply->chairman_id > 0) {
            $chairman = MeetingParticipatorRepository::getById($apply->chairman_id);
            if ($chairman) {
                $hospitalIds[] = $chairman->hospital_id;
                $departIds[] = $chairman->depart_id;
            }
        }

        //获取所有的讲者
        $spResp = MeetingParticipatorRepository::getApplyParticipator($apply, MeetingParticipator::TYPE_SPEAKER);
        /** @var MeetingParticipator $sps */
        foreach ($spResp as $sps) {
            $hospitalIds[] = $sps->hospital_id;
            $departIds[] = $sps->depart_id;
        }

        //获取所有医院
        $hospitals = YouyaoHospitalRepository::getHospitalByIds($hospitalIds);
        $hospitalMap = $hospitals->keyBy('id')->all();

        //获取所有部门
        $departs = SystemDepartRepository::getByIds($departIds);
        $departMap = $departs->keyBy('id')->all();

        //获取主席信息
        $chairmanInfo = [];
        if ($chairman) {
            $chairmanInfo = [
                'id' => $chairman->id,
                'name' => $chairman->name,
                'hospital' => [
                    'id' => $chairman->hospital_id,
                    'name' => isset($hospitalMap[$chairman->hospital_id]) ?
                        $hospitalMap[$chairman->hospital_id]->hospital_name : ''
                ] ,
                'depart' =>  [
                    'id' => $chairman->depart_id,
                    'name' =>  isset($departMap[$chairman->depart_id]) ? $departMap[$chairman->depart_id]->name : '',
                ],
                'level_id' => $chairman->level_id
            ];
        }

        $product = DrugProductRepository::getById($apply->series_id);

        //获取讲者信息
        $speakers = [];
        foreach ($spResp as $sps) {
            $speakers[] = [
                'id' => $sps->id,
                'name' => $sps->name,
                'hospital' => [
                    'id' => $sps->hospital_id,
                    'name' => isset($hospitalMap[$sps->hospital_id]) ?
                        $hospitalMap[$sps->hospital_id]->hospital_name : ''
                ],
                'depart' => [
                    'id' => $sps->depart_id,
                    'name' => isset($departMap[$sps->depart_id]) ? $departMap[$sps->depart_id]->name : ''
                ],
                'level' => $sps->level_id,
                'project' => $sps->project
            ];
        }

        //获取时间信息
        $times = [];
        $timeResp = MeetingTimelineRepository::getApplyTimes($apply);
        /** @var MeetingTimeline $time */
        foreach ($timeResp as $time) {
            $times[] = [
                'id' => $time->id,
                'start' => (string) $time->start,
                'end' => (string) $time->end
            ];
        }

        //获取管理员和主持人信息
        $doctorInfoMap = self::getDoctorUserMapByIds([
           $apply->manager_id,
            $apply->compere_id
        ]);

        //获取类型
        $typeIds = [$apply->meeting_type, $apply->meeting_category, $apply->meeting_sub_type];
        $typeMap = self::getLiveTypeMapByIds($typeIds);

        $typeName = $subTypeName ="";
        if (isset($typeMap[$apply->meeting_type])) {
            $typeName = $typeMap[$apply->meeting_type]->getName();
        }
        if ($apply->meeting_sub_type > 0 && isset($typeMap[$apply->meeting_sub_type])) {
            $subTypeName = $typeMap[$apply->meeting_sub_type]->getName();
        }
        $categoryName = "";
        if ($apply->meeting_category > 0 && isset($typeMap[$apply->meeting_category])) {
            $categoryName = $typeMap[$apply->meeting_category]->getName();
        }

        //获取大区和省份
        $areaRpc = new AreaRpcService();
        $areaInfo = $areaRpc->getAreaInfo($apply->zone_id);
        $provinceInfo = $areaRpc->getProvinceInfo($apply->province_id);

        $doctorGetter = function ($id) use (&$doctorInfoMap) {
            $res = [
                'id' => $id,
                'name' => ''
            ];
            if (isset($doctorInfoMap[$id])) {
                /** @var UserInfoItem $doctorItem */
                $doctorItem = $doctorInfoMap[$id];
                $res['name'] = $doctorItem->getTruename() != "" ? $doctorItem->getTruename() : $doctorItem->getName();
            }
            return $res;
        };

        $isTimeValid = self::isTimeValidEdit($apply);

        return [
            'id' => $apply->id,
            'name' => $apply->name,
            'applicant_id' => $apply->applicant_id,
            'applicant_name' => $applicantName,
            'manager' => $doctorGetter($apply->manager_id),
            'compere' => $doctorGetter($apply->compere_id),
            'type' => [
                'id' => $apply->meeting_type,
                'name' => $typeName,
            ],
            'sub_type' => [
                'id' => $apply->meeting_sub_type,
                'name' => $subTypeName,
            ],
            'category' => [
                'id' => $apply->meeting_category,
                'name' => $categoryName
            ],
            'range' => [
               'id' => $apply->range_type,
               'name' => $apply->range_type == 1 ? '区域' : '中央'
            ],
            'time' => $times,
            'zone' => [
                'id' => $apply->zone_id,
                'name' => $areaInfo->getName(),
            ],
            'province' => [
                'id' => $apply->province_id,
                'name' => $provinceInfo->getName(),
            ],
            'hospital' => [
                'id' => $apply->hospital_id,
                'name' => isset($hospitalMap[$apply->hospital_id]) ?
                    $hospitalMap[$apply->hospital_id]->hospital_name : ''
            ],
            'depart' => [
                'id' => $apply->depart_id,
                'name' =>isset($departMap[$apply->depart_id]) ? $departMap[$apply->depart_id]->name : '',
            ],
            'chairman' => $chairmanInfo,
            'speaker' => $speakers,
            'is_editable' => $apply->applicant_id == $user->uid && $apply->isValidToEdit() && $isTimeValid,
            'is_cancelable' => $apply->applicant_id == $user->uid
                && $apply->state != MeetingApply::STATE_CANCEL
                && $isTimeValid,
            'status' => $apply->state,
            'product' => [
                'id' => $apply->series_id,
                'name' => $product ? $product->name : ''
            ],
            'reason' => (string) $apply->reject_reason
        ];
    }

    /**
     * @param $type
     * @return int
     */
    private static function getTypeMax($type): int
    {
        $rpc = new LiveTypeRpcService();
        $typeResp = $rpc->getByIds([$type]);
        if ($typeResp->getItem()->count() == 0) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "会议分类不存在");
        }
        /** @var CommonInfo $typeInfo */
        $typeInfo = $typeResp->getItem()[0];
        $max = $typeInfo->getMaxTimes();
        return $max <= 0 ? 0 : $max;
    }

    public static function listProvince($id): array
    {
        $rpc = new AreaRpcService();
        $resp = $rpc->listAreaProvinces($id);
        $data = [];
        /** @var CommonIdNameReply $item */
        foreach ($resp->getList() as $item) {
            //中文输入
            $data[] = [
                'id' => $item->getId(),
                'name' => $item->getName()
            ];
        }

        return $data;
    }

    public static function listAreas(): array
    {
        $rpc = new AreaRpcService();
        $resp = $rpc->listAllArea();
        $data = [];
        /** @var CommonIdNameReply $item */
        foreach ($resp->getList() as $item) {
            $data[] = [
                'id' => $item->getId(),
                'name' => $item->getName()
            ];
        }

        return $data;
    }

    public static function listSubTypes($pid = 0, $type = 1): array
    {
        $rpc = new LiveTypeRpcService();
        $resp = $rpc->listTypes($pid, $type);
        $data = [];
        /** @var CommonInfo $item */
        foreach ($resp->getItem() as $item) {
            $data[] = [
                'id' => $item->getId(),
                'name' => $item->getName()
            ];
        }
        return $data;
    }

    public static function adminListApply(
        $applicantId = 0,
        $zoneId = 0,
        $status = 0,
        $applyStart = "",
        $applyEnd = "",
        $meetingStart = "",
        $meetingEnd = "",
        $current = 1,
        $limit = 10,
        $sortStartOrder = 0
    ): ApplyListReply {
        if ($meetingStart || $meetingEnd) {
            //需要关联时间查询
            $resp = MeetingApplyRepository::listWithTimeLimit(
                $applicantId,
                $zoneId,
                $status,
                $applyStart,
                $applyEnd,
                $meetingStart,
                $meetingEnd,
                $current,
                $limit,
                $sortStartOrder
            );
        } else {
            $resp = MeetingApplyRepository::listApplyWithCond(
                $applicantId,
                $zoneId,
                $status,
                $applyStart,
                $applyEnd,
                $current,
                $limit,
                $sortStartOrder
            );
        }

        $page = new CommonPageResponse();
        $page->setCurrent($resp->currentPage());
        $page->setPages($resp->lastPage());
        $page->setSize($resp->perPage());
        $page->setTotal($resp->total());
        $reply = new ApplyListReply();
        $reply->setPage($page);
        if ($resp->isEmpty()) {
            return $reply;
        }

        $ids = [];
        $userIds =  $departIds = $hospitalIds = $typeIds = $productIds = [];
        /** @var MeetingApply $item */
        foreach ($resp->items() as $item) {
            $hospitalIds[] = $item->hospital_id;
            $departIds[] = $item->depart_id;
            $ids[] = $item->id;
            $userIds[] =  $item->applicant_id;
            $typeIds[] = $item->meeting_type;
            $productIds[] = $item->series_id;
            if ($item->meeting_sub_type > 0) {
                $typeIds[] = $item->meeting_sub_type;
            }
        }

        //获取排期范围
        $timeResp = MeetingTimelineRepository::getMultiMeetingStartEnd($ids);
        $timeMap = [];
        if ($timeResp->isNotEmpty()) {
            foreach ($timeResp as $item) {
                $timeMap[$item->meeting_id] = [$item->start, $item->end];
            }
        }
        $userMap = [];
        if (!empty($userIds)) {
            $userMap = self::getApplicantNameMap($userIds);
        }

        //大区
        $zoneMap = self::getZoneMap();

        //类型
        $typeMap = [];
        if (! empty($typeIds)) {
            $typeMap = self::getLiveTypeMapByIds(array_unique($typeIds));
        }

        //部门
        $departMap = [];
        if (! empty($departIds)) {
            $departMap =SystemDepartRepository::getDepartMapByIds($departIds);
        }

        //药品
        $productMap = [];
        if (! empty($productIds)) {
            $productMap = DrugProductRepository::getProductMapByIds($productIds);
        }

        //总计数量
        $countMap = MeetingParticipatorRepository::getCountMapByApplyIds($ids);

        $data = [];
        /** @var MeetingApply $item */
        foreach ($resp->items() as $item) {
            $si = new ApplyItem();
            $si->setId($item->id);
            $si->setName($item->name);
            if (isset($userMap[$item->applicant_id])) {
                $si->setApplicantName($userMap[$item->applicant_id]);
            }
            $si->setApplicantMobile($item->applicant_mobile);
            if (isset($typeMap[$item->meeting_type])) {
                $si->setType($typeMap[$item->meeting_type]->getName());
            }
            if (isset($typeMap[$item->meeting_sub_type])) {
                $si->setSubType($typeMap[$item->meeting_sub_type]->getName());
            }
            if (isset($productMap[$item->series_id])) {
                $si->setProductName($productMap[$item->series_id]->name);
            }
            if (isset($zoneMap[$item->zone_id])) {
                $si->setZone($zoneMap[$item->zone_id]);
            }
            if (isset($departMap[$item->depart_id])) {
                $si->setDepart($departMap[$item->depart_id]->name);
            }
            if (isset($countMap[$item->id])) {
                $si->setSpeakerCount($countMap[$item->id]);
            }
            if (isset($timeMap[$item->id])) {
                [$start, $end] = $timeMap[$item->id];
                $si->setStart($start);
                $si->setEnd($end);
            }
            $si->setApplyTime((string) $item->created_at);
            $si->setStatus($item->state);
            $data[] = $si;
        }
        $reply->setList($data);

        return $reply;
    }

    public static function getApplyDetail(int $id): ApplyDetailReply
    {
        $apply = MeetingApplyRepository::getById($id);
        if (!$apply) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "会议申请不存在");
        }

        //获取所有的人员
        $partResp = MeetingParticipatorRepository::getApplyParticipator($apply);
        $departIds = [$apply->depart_id];
        $hospitalIds = [$apply->hospital_id];
        /** @var MeetingParticipator $partItem */
        foreach ($partResp as $partItem) {
            $departIds[] = $partItem->depart_id;
            $hospitalIds[] = $partItem->hospital_id;
        }

        $doctorMap = self::getDoctorUserMapByIds([
           $apply->manager_id,
           $apply->compere_id
        ]);

        $typeMap = self::getMeetingTypeMap($apply);

        $product = DrugProductRepository::getById($apply->series_id);

        $hospitalMap = YouyaoHospitalRepository::getHospitalMapByIds(array_unique($hospitalIds));

        $departMap = SystemDepartRepository::getDepartMapByIds(array_unique($departIds));

        $areaRpc = new AreaRpcService();
        $areaInfo = $areaRpc->getAreaInfo($apply->zone_id);
        $province = $areaRpc->getProvinceInfo($apply->province_id);

        $timeResp = MeetingTimelineRepository::getApplyTimes($apply);

        //构建响应
        $reply = new ApplyDetailReply();
        $reply->setId($apply->id);
        if ($apply->applicant_id) {
            $reply->setApplicantName(self::getApplicantName($apply));
        }
        $reply->setApplicantMobile($apply->applicant_mobile);
        if (isset($doctorMap[$apply->manager_id])) {
            /** @var UserInfoItem $manager */
            $manager = $doctorMap[$apply->manager_id];
            $managerName = $manager->getTruename() != '' ? $manager->getTruename() : $manager->getName();
            $reply->setManager($managerName);
        }
        if (isset($doctorMap[$apply->compere_id])) {
            /** @var UserInfoItem $compere */
            $compere = $doctorMap[$apply->compere_id];
            $compereName = $compere->getTruename() != "" ? $compere->getTruename() : $compere->getName();
            $reply->setCompere($compereName);
        }
        if (isset($typeMap[$apply->meeting_type])) {
            $reply->setType($typeMap[$apply->meeting_type]->getName());
        }
        if (isset($typeMap[$apply->meeting_sub_type])) {
            $reply->setType($typeMap[$apply->meeting_sub_type]->getName());
        }
        if ($product) {
            $reply->setProduct($product->name);
        }
        if (isset($hospitalMap[$apply->hospital_id])) {
            $reply->setHospital($hospitalMap[$apply->hospital_id]->hospital_name);
        }
        if (isset($departMap[$apply->depart_id])) {
            $reply->setDepart($departMap[$apply->depart_id]->name);
        }
        $reply->setZone($areaInfo->getName());
        $reply->setProvince($province->getName());
        //时间序列
        if ($timeResp->isNotEmpty()) {
            $timeData = $timeResp->reduce(function ($carry, MeetingTimeline $item) {
                $o = new TimeItem();
                $o->setId($item->id);
                $o->setStart($item->start);
                $o->setEnd($item->end);
                $carry[] = $o;

                return $carry;
            });
            $reply->setTime($timeData);
        }

        $speakers = [];
        /** @var MeetingParticipator $partItem */
        foreach ($partResp as $partItem) {
            $o = self::buildGrpcUserItemFromApplyPart($partItem, $hospitalMap, $departMap);
            if ($partItem->user_type == MeetingParticipator::TYPE_CHAIRMAN) {
                $reply->setChairman($o);
            } else {
                $speakers[] = $o;
            }
        }
        $reply->setSpeaker($speakers);

        return $reply;
    }

    private static function getLevelDesc($levelId): string
    {
        switch ($levelId) {
            case 1:
                return "全国KOL";
            case 2:
                return "地区KOL";
            case 3:
                return "区域KOL";
            default:
                return "";
        }
    }

    private static function buildGrpcUserItemFromApplyPart(
        MeetingParticipator $part,
        &$hospitalMap,
        &$departMap
    ): ApplyUserItem {
        $o =new ApplyUserItem();
        $o->setId($part->id);
        $o->setName($part->name);
        if (isset($hospitalMap[$part->hospital_id])) {
            $o->setHospital($hospitalMap[$part->hospital_id]->hospital_name);
        }
        if (isset($departMap[$part->depart_id])) {
            $o->setDepart($departMap[$part->depart_id]->name);
        }
        $o->setLevel(self::getLevelDesc($part->level_id));

        return $o;
    }

    private static function getMeetingTypeMap(MeetingApply $apply): array
    {
        $typeIds = [$apply->meeting_type];
        if ($apply->meeting_sub_type > 0) {
            $typeIds[] = $apply->meeting_sub_type;
        }
        if ($apply->meeting_category > 0) {
            $typeIds[] = $apply->meeting_category;
        }
        return self::getLiveTypeMapByIds($typeIds);
    }

    public static function audit($id, bool $isPass, string $reason)
    {
        $apply = MeetingApplyRepository::getById($id);
        if (!$apply) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "会议申请不存在");
        }
        if ($apply->state != MeetingApply::STATE_NEW) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "会议已审核完成,不能重复审核");
        }

        Db::transaction(function () use ($apply, $isPass, $reason) {
            //TODO: 需要处理用户
            if ($isPass) {
                MeetingApplyRepository::auditSuccess($apply);
                MeetingAuditEventRepository::addApplyEvent(
                    $apply,
                    MeetingAuditEvent::EVENT_TYPE_AUDIT_SUCCESS,
                    0,
                    MeetingAuditEvent::PLATFORM_ADMIN
                );
            } else {
                MeetingApplyRepository::auditFail($apply, $reason);
                MeetingAuditEventRepository::addApplyEvent(
                    $apply,
                    MeetingAuditEvent::EVENT_TYPE_AUDIT_FAILED,
                    0,
                    MeetingAuditEvent::PLATFORM_ADMIN
                );
            }
        });
    }

    /**
     * 获取申请人名
     * 真实姓名 -> 帐户姓名 -> 微信呢称
     * @param array $userIds
     * @return array
     */
    private static function getApplicantNameMap(array $userIds): array
    {
        if (empty($userIds)) {
            return [];
        }

        $agentYYIDS = $userYYIDS = [];
        $users = UsersRepository::getUserByUids($userIds);
        /** @var Users $user */
        foreach ($users as $user) {
            $userMap[$user->uid] = $user;
            if (!$user->name) {
                $userYYIDS[] = $user->yyid;
            }
            if ($user->isVerified()) {
                $agentYYIDS[] = $user->yyid;
            }
        }
        $wechatMap = $agentMap = [];
        if (!empty($userYYIDS)) {
            $wechat = WechatUserRepository::getMultiUsersWechat($userYYIDS);
            $wechatMap = $wechat->keyBy('u_yyid')->all();
        }
        if (!empty($agentYYIDS)) {
            $agents = AgentRepository::getMultiUserAgentInfo($agentYYIDS);
            $agentMap = $agents->keyBy('u_yyid')->all();
        }

        $map = [];
        /** @var Users $user */
        foreach ($users as $user) {
            $name = (string) $user->name;
            if ($user->isVerified() && isset($agentMap[$user->yyid])) {
                $name = (string) $agentMap[$user->yyid]->truename;
            }
            if ((!$name) && isset($wechatMap[$user->yyid])) {
                $name = $wechatMap[$user->yyid]->nickname;
            }
            $map[$user->uid] = $name;
        }

        return $map;
    }

    /**
     * @param MeetingApply $apply
     * @return string
     */
    public static function getApplicantName(MeetingApply $apply): string
    {
        $applicantName = "";
        if ($apply->applicant_id) {
            $applicant = UsersRepository::getUserByUid($apply->applicant_id);
            $applicantName = (string)$applicant->name;
            if ($applicant->isVerified()) {
                $agent = AgentRepository::getAgentByUser($applicant);
                if ($agent) {
                    $applicantName = (string)$agent->truename;
                }
            }
            if (!$applicantName) {
                $wechat = WechatUserRepository::getOneWechatInfoByUserYYID($applicant->yyid, DataStatus::WECHAT_AGENT);
                !is_null($wechat) && $applicantName = $wechat->nickname;
            }
        }
        return $applicantName;
    }

    public static function cancel($id)
    {
        $user = Helper::getLoginUser();
        $apply = MeetingApplyRepository::getById($id);
        if (!$apply) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "申请记录不存在");
        }
        if ($apply->applicant_id != $user->uid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前申请不允许编辑");
        }
        if ($apply->isCanceled()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "申请已经取消");
        }

        Db::transaction(function () use ($apply, $user) {
            $res = MeetingApplyRepository::cancelApply($apply);
            if ($res) {
                MeetingAuditEventRepository::addApplyEvent($apply, MeetingAuditEvent::EVENT_TYPE_CANCEL, $user->uid);
                return;
            }
            $apply->refresh();
            if (!$apply->isCanceled()) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '数据更新失败，请刷新重试');
            }
        });
    }

    private static function diffApplyTimes(MeetingApply $apply, array $times): array
    {
        $existItems = MeetingTimelineRepository::getApplyTimes($apply);
        $existIds = $existItems->pluck('id')->all();
        $maps = $existItems->keyBy('id')->all();

        $add = $update = $timeIds = [];
        $now = Carbon::now();
        foreach ($times as $time) {
            if (isset($time['id']) && $time['id'] > 0 && isset($maps[$time['id']])) {
                /** @var MeetingTimeline $o */
                $o = $maps[$time['id']];
                $o->start = $time['start'];
                $o->end = $time['end'];
                $o->updated_at = $now;
                $update[] = $o;
                $timeIds[] = $time['id'];
            } else {
                $add[] = $time;
            }
        }
        [$delIds, $addIds] =  Helper::diff($existIds, $timeIds);
        return [$delIds, $add, $update];
    }
}
