<?php
declare(strict_types=1);
namespace App\Service\Meeting;

use Google\Protobuf\GPBEmpty;
use Grpc\Area\AreaClient;
use Grpc\Area\CheckAreaProvinceRequest;
use Grpc\Area\CommonIdNameListReply;
use Grpc\Area\CommonIdNameReply;
use Grpc\Area\CommonIdRequest;
use Grpc\Area\GPBMetadata\Area;
use Hyperf\Utils\ApplicationContext;

class AreaRpcService
{
    private static function getClient(): AreaClient
    {
        return ApplicationContext::getContainer()
            ->get(AreaClient::class);
    }

    public function listAllArea(): CommonIdNameListReply
    {
        $req = new GPBEmpty();
        return self::getClient()->listAllArea($req);
    }

    public function getAreaInfo($id): CommonIdNameReply
    {
        $req = new CommonIdRequest();
        $req->setId($id);
        return self::getClient()->getAreaInfo($req);
    }

    public function listAreaProvinces($areaId): CommonIdNameListReply
    {
        $req = new CommonIdRequest();
        $req->setId($areaId);
        return self::getClient()->listAreaProvinces($req);
    }

    public function checkArea($areaId, $provinceId): bool
    {
        $req = new CheckAreaProvinceRequest();
        $req->setAreadId($areaId);
        $req->setProvinceId($provinceId);

        return self::getClient()->checkAreaProvince($req)->getIsOk();
    }

    public function getProvinceInfo($provinceId): CommonIdNameReply
    {
        $req = new CommonIdRequest();
        $req->setId($provinceId);
        return self::getClient()->getProvinceInfo($req);
    }
}
