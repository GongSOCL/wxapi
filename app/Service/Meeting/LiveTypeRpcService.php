<?php
declare(strict_types=1);
namespace App\Service\Meeting;

use Grpc\Livetype\IdsRequest;
use Grpc\Livetype\ListReply;
use Grpc\Livetype\ListRequest;
use Grpc\Livetype\LivetypeClient;
use Hyperf\Utils\ApplicationContext;

class LiveTypeRpcService
{
    private static function getClient(): LivetypeClient
    {
        return ApplicationContext::getContainer()
            ->get(LivetypeClient::class);
    }

    public function listTypes($pid = 0, $type = 1): ListReply
    {
        $req = new ListRequest();
        $req->setPid($pid);
        $req->setType($type);
        return self::getClient()->listTypes($req);
    }

    public function getByIds(array $typeIds): ListReply
    {
        $req = new IdsRequest();
        $req->setId($typeIds);

        return self::getClient()->typeInfo($req);
    }
}
