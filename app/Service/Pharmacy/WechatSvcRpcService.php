<?php

declare(strict_types=1);
namespace App\Service\Pharmacy;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use Grpc\Pharmacy\Wechat\DoctorQrLinkRequest;
use Grpc\Pharmacy\Wechat\UrlLinkReply;
use Grpc\Pharmacy\Wechat\UrlLinkRequest;
use Grpc\Pharmacy\Wechat\WechatSvcClient;
use Hyperf\Utils\ApplicationContext;

class WechatSvcRpcService
{
    private static function getClient()
    {
        return ApplicationContext::getContainer()
            ->get(WechatSvcClient::class);
    }

    public static function generateUrlLink(
        $path = "",
        array $params = [],
        $expireDays = 0,
        $expireEnd = ""
    ): UrlLinkReply {
        $req = new UrlLinkRequest();
        $req->setPath($path);
        $req->setParams(json_encode($params, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
        $req->setIsExpireByDaysInterval($expireDays > 0);
        $req->setExpireDays($expireDays);
        $req->setExpireEnd($expireEnd);
        return self::getClient()->generateUrlLink($req);
    }

    public static function generateDoctorQrLink($agentId, $doctorId)
    {
        if(empty($agentId) || empty($doctorId)) {
            throw new BusinessException(ErrorCode::SERVER_ERROR, '入参异常');
        }
        $req = new DoctorQrLinkRequest();
        $req->setAgentId($agentId);
        $req->setDoctorId($doctorId);
        return self::getClient()->generateDoctorQrLink($req);
    }
}
