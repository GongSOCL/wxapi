<?php
declare(strict_types=1);
namespace App\Service\Aerogen;

use App\Constants\DataStatus;
use App\Helper\Helper;
use App\Repository\NewGroup\NewAgentInviteRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\UsersRepository;
use App\Service\Wechat\WechatBusiness;
use App\Service\Wechat\WechatRpcService;
use Grpc\Wechat\WechatDoInfoByUnionIdReply;
use Grpc\Wechat\WechatDoInfoByUnionIdRequest;
use Hyperf\Di\Annotation\Inject;

class AerogenService
{
    /**
     * @Inject()
     * @var WechatRpcService
     */
    protected $wechatRpcService;

    /**
     * @Inject()
     * @var WechatBusiness
     */
    protected $wechatBusiness;

    /**
     * @param $agentYyid
     * @return array
     */
    public function getDoctorListByAgentYyid($agentYyid)
    {
        $userInfo = UsersRepository::getUserByYYID($agentYyid);
        $bindingDoctors = NewAgentInviteRepository::getBindingDoctorsList(
            $userInfo->uid,
            DataStatus::WECHAT_DOCTOR
        );
        $bindingDoctors = $bindingDoctors->isNotEmpty() ? $bindingDoctors->toArray() : [];

        foreach ($bindingDoctors as $k => $value) {
            $req = new WechatDoInfoByUnionIdRequest();
            $req->setUnionId($value['user_unionid']);

            /** @var WechatDoInfoByUnionIdReply $wechat */
            $wechat = $this->wechatRpcService->getWechatDoInfoByUnionId($req);

            if ($wechat) {
                $bindingDoctors[$k]['nickname'] = $wechat->getNickname();
                $bindingDoctors[$k]['headimgurl'] = $wechat->getHeadimgurl();
            } else {
                $bindingDoctors[$k]['nickname'] = '';
                $bindingDoctors[$k]['headimgurl'] = '';
            }
        }

        return $bindingDoctors;
    }

    /**
     * @param $agentYyid
     * @return array
     */
    public function getDrugstoreListByAgentYyid($agentYyid)
    {
        $data = RepsServeScopeSRepository::getDrugstoreListByAgentYyid($agentYyid);
        return $data->isNotEmpty() ? $data->toArray() : [];
    }

    /**
     * @param $agentYyid
     * @return array
     */
    public function getProductListByAgentYyid($agentYyid)
    {
        $data = RepsServeScopeSRepository::getProductListByAgentYyid($agentYyid);
        return $data->isNotEmpty() ? $data->toArray() : [];
    }

    /**
     * @param $agentYyid
     * @return string[]
     */
    public function getShareInfo($agentYyid)
    {
        $agent = UsersRepository::getUserByYYID($agentYyid);

        return [
            'title'=>'邀请',
            'describe'=>($agent->name ?? '').'邀请您点击生成专属二维码',
            'img'=>'http://img.youyao99.com/C67842AF30A6EF03A1D49D8237414237.jpg',
            'url'=>'',
        ];
    }

    /**
     * @param $agentYyid
     * @param $linkDoctorid
     * @return array
     */
    public function getDoctorQrcode($agentYyid, $linkDoctorid)
    {
        $agentId = UsersRepository::getUserByYYID($agentYyid)->uid;
        $qrcodeParams = DataStatus::REGULAR . '_' . $agentId . '_' . $linkDoctorid;

        $url = $this->wechatBusiness->getQrcode($qrcodeParams, DataStatus::WECHAT_AEROGEN);
        return ['url' => Helper::imgtobase64($url)];
    }

    /**
     * @return array
     */
    public function getDrugstoreQrcode()
    {
        return [ 'url' => env('AEROGEN_PUS_URL') ];
    }
}
