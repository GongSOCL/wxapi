<?php
declare(strict_types=1);

namespace App\Service\Monpic;

use Grpc\Monpic\MonpicCountReply;
use Grpc\Monpic\MonpicCountRequest;
use Grpc\Monpic\MonpicReply;
use Grpc\Monpic\MonpicRequest;
use Youyao\Framework\Client\CoreClient;

class MonpicRpcService extends CoreClient
{
    /**
     * @param MonpicRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function fetchMonpic(MonpicRequest $argument)
    {
        return $this->sendRequest($argument, [MonpicReply::class, 'decode']);
    }

    /**
     * @param MonpicCountRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function fetchMonpicCount(MonpicCountRequest $argument)
    {
        return $this->sendRequest($argument, [MonpicCountReply::class, 'decode']);
    }
}
