<?php
declare(strict_types=1);

namespace App\Service\Monpic;

use App\Constants\DataStatus;
use App\Model\Qa\MorningDreamPopTime;
use App\Repository\DrugSeriesRepository;
use App\Repository\MonpicRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\UsersRepository;
use Grpc\Monpic\MonpicCountReply;
use Grpc\Monpic\MonpicCountRequest;
use Grpc\Monpic\MonpicReply;
use Grpc\Monpic\MonpicRequest;
use Hyperf\Di\Annotation\Inject;

class MonpicService
{
    /**
     * @Inject()
     * @var MonpicRpcService
     */
    private $monpicRpcService;

    /**
     * @param $yyid
     * @return []
     */
    public function getMonpic($yyid, $type)
    {
        $data = [
            'pic_id' => 0,
            'url' => '',
            'date' => ''
        ];

        $dateArr = $this->checkTypeOne();

        //获取力蜚能的产品id
        $series = DrugSeriesRepository::getSeriesByDrugName(DataStatus::DRUG_SERIES_NAME);
        if ($series) {
            //判断用户是否代理了力蜚能
            $info = RepsServeScopeSRepository::getInfoByUserAndSeries($yyid, $series->yyid);
            if ($info&&!empty($dateArr)) {
                $mRequest = new MonpicRequest();
                $mRequest->setUserId(UsersRepository::getUserByYYID($yyid)->uid);
                $mRequest->setDate($dateArr[0]);
                $mRequest->setType($type);
                /** @var MonpicReply $reply */
                $reply = $this->monpicRpcService->fetchMonpic($mRequest);
                if ($reply->getUrl()) {
                    $data = [
                        'pic_id' => $reply->getPicId(),
                        'url' => $reply->getUrl(),
                        'date' => $dateArr[1]
                    ];
                }
            }
        }

        return $data;
    }

    public function checkTypeOne()
    {
        //是力蜚能的代表 判断当前时间是否超过早上6点 不超过获取上一天的海报（已6点为界）
        //当前时间戳
        $currentTime = time();
        //当天6点时间戳
        $limitTime = strtotime(date('Y-m-d', time()).' 06:00:00');
        if ($currentTime > $limitTime) {
            //当天
            $picData = date('Y-m-d', time()).' 00:00:00';
            $rdata = date('Y-m-d', time());
        } else {
            //前一天
            $picData = date('Y-m-d', strtotime('-1 day')).' 00:00:00';
            $rdata = date('Y-m-d', strtotime('-1 day'));
        }
        return [$picData, $rdata];
    }

    /**
     * @param $uid
     * @param $picId
     * @param $date
     * @return int
     */
    public function setMonpicCount($uid, $picId, $date)
    {
        $mRequest = new MonpicCountRequest();
        $mRequest->setUid($uid);
        $mRequest->setPicId($picId);
        $mRequest->setDate($date);
        /** @var MonpicCountReply $reply */
        $reply = $this->monpicRpcService->fetchMonpicCount($mRequest);

        return $reply->getCount();
    }
}
