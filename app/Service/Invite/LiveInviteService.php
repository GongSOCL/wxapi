<?php
declare(strict_types=1);
namespace App\Service\Invite;

use App\Constants\DataStatus;
use App\Model\Core\Live;
use App\Repository\HospitalRepository;
use App\Repository\LiveInvite\LiveInviteRepository;
use App\Repository\NewGroup\OrganizationRepository;
use App\Repository\UsersRepository;
use App\Service\Group\Doctor\OrganizationSearchService;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;
use App\Service\Wechat\WechatBusiness;

class LiveInviteService
{
    /**
     * @param $liveName
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function lives($liveName, $page, $pageSize)
    {
        $where[] = ['status', '=', DataStatus::REGULAR];
        $where[] = ['is_deleted', '=', DataStatus::DELETE];
        $where[] = [
            function ($q) {
                $q->whereIn('live_state', [Live::LIVE_STATE_1, Live::LIVE_STATE_2, Live::LIVE_STATE_3]);
            }];
        $where[] = [
            function ($q) {
                $q->whereIn('is_online', [Live::PURE_ONLINE, Live::HALF_ONLINE]);
            }];
        if ($liveName) {
            $where[] = ['title', 'like', '%' . $liveName . '%'];
        }
        $data = LiveInviteRepository::getLives($where, $page, $pageSize);
        $count = LiveInviteRepository::getLivesCount($where);
        return [
            'list' => $data->isNotEmpty() ? $data->toArray() : [],
            'total' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $userId
     * @param $doctorName
     * @param $liveId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function doctors($userId, $doctorName, $liveId, $page, $pageSize)
    {
        $doctorIds = [];
        // 获取用户的部门
        $orgs = OrganizationRepository::getOrgLevel($userId);
        if ($orgs->isNotEmpty()) {
            // 带组织架构
            $orgs = $orgs->toArray();
            foreach ($orgs as $org) {
                $redis = new OrganizationSearchService($org['dept_id']);
                $subDoctorIds = $redis->getSubscribeDoctor();
                if (!empty($subDoctorIds)) {
                    foreach ($subDoctorIds as $subDoctorId) {
                        if (!in_array($subDoctorId, $doctorIds)) {
                            array_push($doctorIds, $subDoctorId);
                        }
                    }
                }
                $unsubDoctorIds = $redis->getUnsubscribeDoctor();
                if (!empty($unsubDoctorIds)) {
                    foreach ($unsubDoctorIds as $unsubDoctorId) {
                        if (!in_array($unsubDoctorId, $doctorIds)) {
                            array_push($doctorIds, $unsubDoctorId);
                        }
                    }
                }
            }
            // 没有user_id 条件
            $where[] = ['ndu.wechat_type', '=', DataStatus::WECHAT_DOCTOR];
            $where[] = ['ndou.status', '=', DataStatus::REGULAR];
            if ($doctorName) {
                $where[] = [
                    function($q) use($doctorName) {
                        $q->where('ndu.true_name', 'like', '%' . $doctorName . '%')
                            ->orWhere('ndu.hospital_name', 'like', '%' . $doctorName . '%');
                    }];
            }
            $where[] = ['is_subscribed', '=', 1];

            $data = LiveInviteRepository::getOrgDoctors($where, $doctorIds, $page, $pageSize);
            if ($data->isNotEmpty()) {
                $data = $data->toArray();
                foreach ($data as $key => $item) {
                    $data[$key]['is_invited'] = 0;
                    //判断是否已经邀请过了
                    $i = LiveInviteRepository::getInviteInfo($userId, $liveId);
                    if ($i) {
                        $inviteHistory = LiveInviteRepository::getInviteHistory($userId, $i->id, $item['id']);
                        if ($inviteHistory) {
                            $data[$key]['is_invited'] = 1;
                        }
                    }
                }
            } else {
                $data = [];
            }
            $count = LiveInviteRepository::getOrgDoctorsCount($where, $doctorIds);
            return [
                'list' => $data,
                'total' => $count,
                'page' => $page,
                'page_size' => $pageSize
            ];
        } else {
            // 不带组织架构
            $where[] = ['ndou.user_id', '=', $userId];
            $where[] = ['ndu.wechat_type', '=', DataStatus::WECHAT_DOCTOR];
            $where[] = ['ndou.status', '=', DataStatus::REGULAR];
            if ($doctorName) {
                $where[] = ['ndu.true_name', 'like', '%' . $doctorName . '%'];
            }
            $where[] = ['is_subscribed', '=', 1];

            $data = LiveInviteRepository::getDoctors($where, $page, $pageSize);
            if ($data->isNotEmpty()) {
                $data = $data->toArray();
                foreach ($data as $key => $item) {
                    $data[$key]['is_invited'] = 0;
                    //判断是否已经邀请过了
                    $i = LiveInviteRepository::getInviteInfo($userId, $liveId);
                    if ($i) {
                        $inviteHistory = LiveInviteRepository::getInviteHistory($userId, $i->id, $item['id']);
                        if ($inviteHistory) {
                            $data[$key]['is_invited'] = 1;
                        }
                    }
                }
            } else {
                $data = [];
            }
            $count = LiveInviteRepository::getDoctorsCount($where);
            return [
                'list' => $data,
                'total' => $count,
                'page' => $page,
                'page_size' => $pageSize
            ];
        }
    }

    /**
     * @param $userId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function inviteList($userId, $page, $pageSize)
    {
        $list = [];
        $data = LiveInviteRepository::getInviteList($userId, $page, $pageSize);
        if ($data->isNotEmpty()) {
            $data = $data->toArray();
            foreach ($data as $key => $value) {
                $liveInfo = LiveInviteRepository::getLiveInfoById($value['live_id']);
                $list[$key]['invite_id'] = $value['id'];
                $list[$key]['live_id'] = $liveInfo ? $liveInfo->id : 0;
                $list[$key]['live_title'] = $liveInfo ? $liveInfo->title : [];
                $list[$key]['live_img'] = $liveInfo ? $liveInfo->thumbnail_img : [];
                $list[$key]['live_start_time'] = $liveInfo ? $liveInfo->start_time : [];
                $list[$key]['count'] = LiveInviteRepository::getInviteDetailCount($value['id']);
            }
        }
        $count = LiveInviteRepository::getInviteListCount($userId);
        return [
            'list' =>  $list,
            'total' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $inviteId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function detail($inviteId, $page, $pageSize)
    {
        $data = LiveInviteRepository::getInviteDetail($inviteId, $page, $pageSize);
        $count = LiveInviteRepository::getInviteDetailCount($inviteId);
        return [
            'list' =>  $data->isNotEmpty() ? $data->toArray() : [],
            'total' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $userId
     * @param $liveId
     * @param $doctorId
     * @param $marks
     * @return array
     * @throws \Exception
     */
    public function send($userId, $liveId, $doctorId, $marks)
    {
        Db::beginTransaction();
        try {
            //更新邀请表和详情表
            $inviteInfo = LiveInviteRepository::getInviteInfo($userId, $liveId);
            if ($inviteInfo) {
                $inviteId = $inviteInfo->id;
            } else {
                $inviteId = LiveInviteRepository::createInviteInfo($userId, $liveId);
            }
            foreach ($doctorId as $oneId) {
                $inviteDetailInfo = LiveInviteRepository::getInviteDetailInfo($inviteId, $oneId);
                if (!$inviteDetailInfo) {
                    $doctorInfo = LiveInviteRepository::getDoctorInfo($oneId);
                    LiveInviteRepository::createInviteDetailInfo($inviteId, $doctorInfo);
                }
                //添加邀请记录
                $historyId = LiveInviteRepository::createInviteHistory($userId, $inviteId, $oneId);
                // 判断是否大于2次 大于2次不在发送模板消息
                $sendTimes = LiveInviteRepository::getMaxInviteTimes($userId, $inviteId, $oneId);
                if ($sendTimes<=2) {
                    //发送模板文件
                    $this->sendInviteMsg($userId, $liveId, $oneId, $marks, $historyId);
                }
            }
            Db::commit();
            return [];
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }

    public function sendInviteMsg($userId, $liveId, $oneId, $marks, $historyId)
    {
        //代表信息
        $userInfo = LiveInviteRepository::getAgentInfo($userId);
        if ($userInfo&&$userInfo->truename) {
            $aName = $userInfo->truename;
        } else {
            $aName = $userInfo->name;
        }
        if ($marks) {
            $first = $aName."邀请您参加会议\r\n".$marks;
        } else {
            $first = $aName.'邀请您参加会议';
        }
        //直播信息
        $liveInfo = LiveInviteRepository::getLiveInfoById($liveId);
        //备注信息
        $m1 = '';
        $chairman = LiveInviteRepository::getLiveChairman($liveId);
        if ($chairman) {
            $cHospital = LiveInviteRepository::getHospitalInfo($chairman->hospital_id);
            if ($cHospital) {
                $m1 = '会议主席：'.$chairman->name.'  '.$cHospital->hospital_name;
            } else {
                $m1 = '会议主席：'.$chairman->name;
            }
        }
        $t = '会议讲者：';
        $m2 = '';
        $speakers = LiveInviteRepository::getLiveSpeakers($liveId);
        if ($speakers->isNotEmpty()) {
            foreach ($speakers->toArray() as $item) {
                $sHospital = LiveInviteRepository::getHospitalInfo($item['hospital_id']);
                if ($sHospital) {
                    if ($item['project']) {
                        $m2 .= $item['name'].'  '.$sHospital->hospital_name."\r\n".$item['project']."\r\n";
                    } else {
                        $m2 .= $item['name'].'  '.$sHospital->hospital_name."\r\n";
                    }
                } else {
                    if ($item['project']) {
                        $m2 .= $item['name'].'  '."\r\n".$item['project']."\r\n";
                    } else {
                        $m2 .= $item['name'].'  '."\r\n";
                    }
                }
            }
        }
        $m = $m1."\r\n".$t.$m2;
        //医生openid
        $dInfo = LiveInviteRepository::getDoctorInfo($oneId);
        //链接
        $env = env('APP_ENV', 'local');
        $pramas = '?live_id='.$liveId.'&invite_user_id='.$userId.'&invite_history_id='.$historyId;
        switch ($env) {
            case 'testing':
                $link = 'https://doctor.yyimgs.com/uniapp/pages/invite/posterEntry'.$pramas;
                break;
            case 'staging':
                $link = 'https://doctor.qa2.youyao99.com/uniapp/pages/invite/posterEntry'.$pramas;
                break;
            case 'prod':
                $link = 'https://doctor.youyao99.com/uniapp/pages/invite/posterEntry'.$pramas;
                break;
            default:
                $link = 'https://doctor.yyimgs.com/uniapp/pages/invite/posterEntry'.$pramas;
                break;
        }
        ApplicationContext::getContainer()
            ->get(WechatBusiness::class)
            ->sendLiveInviteMsg(
                $first,
                $liveInfo->title,
                $liveInfo->start_time,
                $m,
                [$dInfo->openid],
                $link
            );
    }
}
