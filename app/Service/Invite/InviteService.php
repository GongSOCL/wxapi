<?php
declare(strict_types=1);
namespace App\Service\Invite;

use App\Constants\DataStatus;
use App\Model\Qa\Users;
use App\Repository\NewGroup\NewAgentInviteRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Service\Wechat\WechatRpcService;
use Grpc\Wechat\WechatDoInfoByUnionIdReply;
use Grpc\Wechat\WechatDoInfoByUnionIdRequest;
use Hyperf\Di\Annotation\Inject;

class InviteService
{
    /**
     * @Inject()
     * @var WechatRpcService
     */
    protected $wechatRpcService;

    public function getInviteList($userId)
    {
        //获取邀请的代表
        $agent = [];
        $agentCount = 0;
        $inviteAgent = NewAgentInviteRepository::getMyUserAgents($userId, DataStatus::WECHAT_AGENT);
        if ($inviteAgent->isNotEmpty()) {
            $agentCount = $inviteAgent->count();
            foreach ($inviteAgent->toArray() as $key => $value) {
                $wechatAgent = WechatUserRepository::getByUnionid($value['user_unionid']);
                $agent[$key]['r_id'] = $value['id'];
                $agent[$key]['user_id'] = $wechatAgent->u_yyid ?? 0;
                $agent[$key]['nickname'] = $wechatAgent->nickname;
                $agent[$key]['headimgurl'] = $wechatAgent->headimgurl;
                $agent[$key]['created_at'] = $value['created_at'];
                $agent[$key]['user_type'] = DataStatus::WECHAT_AGENT;
                if ($wechatAgent->u_yyid) {
                    $agent[$key]['user_verified'] = UsersRepository::getUserByYYID($wechatAgent->u_yyid)->is_agent;
                } else {
                    $agent[$key]['user_verified'] = Users::AGENT_VERIFY_NO;
                }
            }
        }

        //获取邀请的医生
        $doctor = [];
        $doctorCount = 0;
        $inviteDoctor = NewAgentInviteRepository::getMyUserAgents($userId, DataStatus::WECHAT_DOCTOR);
        if ($inviteDoctor->isNotEmpty()) {
            $doctorCount = $inviteDoctor->count();
            foreach ($inviteDoctor->toArray() as $k => $v) {
                $req = new WechatDoInfoByUnionIdRequest();
                $req->setUnionId($v['user_unionid']);

                /** @var WechatDoInfoByUnionIdReply $wechatDoctor */
                $wechatDoctor = $this->wechatRpcService->getWechatDoInfoByUnionId($req);
                $doctor[$k]['r_id'] = $v['id'];
                $doctor[$k]['user_id'] = $wechatDoctor->getDoctorId();
                $doctor[$k]['nickname'] = $wechatDoctor->getNickname();
                $doctor[$k]['headimgurl'] = $wechatDoctor->getHeadimgurl();
                $doctor[$k]['user_verified'] = $wechatDoctor->getUserVerified();
                $doctor[$k]['created_at'] = $v['created_at'];
                $doctor[$k]['user_type'] = DataStatus::WECHAT_DOCTOR;
            }
        }

        //获取未注册的
        $unsign = [];
        foreach ($agent as $a) {
            if ($a['user_verified']!==Users::AGENT_VERIFY_OK) {
                array_push($unsign, $a);
            }
        }

        foreach ($doctor as $d) {
            if ($d['user_verified']!==Users::AGENT_VERIFY_NO) {
                array_push($unsign, $d);
            }
        }

        return [
            'all' => ['list' => array_merge($agent, $doctor), 'count' => $agentCount + $doctorCount],
            'agent' => ['list' => $agent, 'count' => $agentCount],
            'doctor' => ['list' => $doctor, 'count' => $doctorCount],
            'unSign' => ['list' => $unsign, 'count' =>count($unsign) ]
        ];
    }
}
