<?php
declare(strict_types=1);
namespace App\Service\Wechat;

use App\Constants\DataStatus;
use App\Constants\Wechat;
use App\Helper\Helper;
use App\Repository\WechatUserRepository;
use App\Service\Doctor\DoctorInfoRpcService;
use App\Service\Wechat\Callback\AgentCallbackService;
use App\Service\Wechat\Callback\DoctorCallbackService;
use EasyWeChat\Kernel\Messages\Message;
use EasyWeChat\OfficialAccount\Server\Guard;
use Grpc\DoctorInfo\TEMPLATE_MSG_TYPE;
use Grpc\Msg\MSG_BIZ_TYPE;
use Grpc\Msg\MSG_PRIORITY;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use Overtrue\Socialite\User;
use Youyao\Framework\Components\Msg\MessageRpcClient;

class WechatBusiness
{
    /**
     * @Inject
     * @var WechatManager
     */
    private $manager;

    public function getAccessToken($wechatType = DataStatus::WECHAT_DOCTOR)
    {
        return $this->manager->getInstance($wechatType)->getAccessToken();
    }

    /**
     * 微信调用
     *
     * @param int $wechatType
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \EasyWeChat\Kernel\Exceptions\BadRequestException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \ReflectionException
     */
    public function serve($wechatType = DataStatus::WECHAT_DOCTOR)
    {
        /**
         * @var Guard
         */
        $server = $this->manager->getInstance($wechatType)->getServer();

        $request = ApplicationContext::getContainer()->get(RequestInterface::class);
        if ($request->isMethod('GET')) {
            return $server->serve();
        }
        //使用日志组件记录日志
        $msg = $server->getMessage();
        Helper::getLogger()->info(
            sprintf("%s_receive_wechat_callback_msg", $wechatType == DataStatus::WECHAT_DOCTOR ?
                'doctor' : 'agent'),
            $msg
        );

        //记录微信日志
        Helper::getLogger()->info('wechat_log_before_log', $msg);
        if (isset($msg['FromUserName'])) { //记录日志到数据库
            WechatService::logMessage($msg, $wechatType);
        }

        //非关注消息都查找或者更新一次用户微信信息
        if (isset($msg['FromUserName']) && (!($msg['MsgType'] == 'Event' && $msg['Event'] == 'unsubscribe'))) {
            $wechatUserInfo = $this->getWechatInfo($msg['FromUserName'], $wechatType);
            Helper::getLogger()->info('wechat_callback_start_info', $wechatUserInfo);
            if ($wechatUserInfo) {
                self::setContextWechatInfo($wechatUserInfo);
                WechatUserRepository::findUpdateOrAddSubscribe($wechatUserInfo, $wechatType);
            }
        }

        //根据不同类型注册不同的数据
        //代表关键词回复
        $server->push([AgentCallbackService::class, 'textProxy'], Message::TEXT);
        //代表订阅事件
        $server->push([AgentCallbackService::class, 'subscribeProxy'], Message::EVENT);
        //代表取消订阅事件
        $server->push([AgentCallbackService::class, 'unsubscribeProxy'], Message::EVENT);
        //代表扫码事件
        $server->push([AgentCallbackService::class, 'scanProxy'], Message::EVENT);
        //医生订阅事件
        $server->push([DoctorCallbackService::class, 'subscribeProxy'], Message::EVENT);
        //医生取消订阅事件
        $server->push([DoctorCallbackService::class, 'unsubscribeProxy'], Message::EVENT);
        //医生扫码事件
        $server->push([DoctorCallbackService::class, 'scanProxy'], Message::EVENT);


        //返回数据
        return $server->serve();
    }

    /**
     * 把微信信息保存到上下文中,毕竟是网络请求，不要多次发起请求
     *
     * @param array $wxInfo
     * @param string $key
     */
    public static function setContextWechatInfo(array $wxInfo, $key = 'wechat_user_info')
    {
        Context::set($key, $wxInfo);
    }

    /**
     * 获取上下文微信信息
     * @param string $key
     * @return mixed|null
     */
    public static function getContextWechatInfo($key = 'wechat_user_info')
    {
        return Context::get($key);
    }



    /**
     * 公众号二维码
     * @param $inviteData
     * @param int $wechatType 1代表公众号2医生公众号
     * @return string
     */
    public function getQrcode($inviteData, $wechatType)
    {
        return $this->manager->getInstance($wechatType)->getQrcode($inviteData);
    }

    /**
     * 获取oauth网页授权跳转地址
     * @param string $callback
     * @param int $isSilent
     * @param string $state
     * @param int $wechatType
     * @return string
     */
    public function getOauthRedirectUrl(
        $callback,
        $isSilent = 1,
        $state = '',
        $wechatType = DataStatus::WECHAT_AGENT
    ): string {
        return $this->manager->getInstance($wechatType)
            ->getOauthRedirectUrl($callback, $isSilent, $state);
    }

    /**
     * 获取oauth网页授权用户信息
     *
     * @param int $wechatType
     * @return User
     */
    public function getOauthWechatInfo($wechatType = DataStatus::WECHAT_AGENT): User
    {
        return $this->manager->getInstance($wechatType)
            ->getOauthUserInfo();
    }

    public function getOauthWechatAccessToken($wechatType)
    {
        return $this->manager->getInstance($wechatType)->getOauthUserInfo();
    }

    /**
     * 获取js签名数据
     * @param $url
     * @param array $apis
     * @param false $debug
     * @param int $wechatType
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getWechatJsSignature(
        $url,
        array $apis,
        $debug = false,
        $wechatType = DataStatus::WECHAT_AGENT
    ): array {
        Helper::getLogger()->info('getWechatJsSignature', [
            'url' => $url,
            'apis' => $apis,
            'debug' => $debug,
            'wechat'
        ]);
        return $this->manager->getInstance($wechatType)
            ->getWechatJsSignature($url, $apis, $debug);
    }

    /**
     * 调用服务端接口获取用户信息
     *
     * @param $openId
     * @param int $wechatType
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function getWechatInfo($openId, $wechatType = DataStatus::WECHAT_AGENT)
    {
        return $this->manager->getInstance($wechatType)->getServerUserInfo($openId);
    }

    /**
     * 创建自定义菜单.
     * @param int $wechatType
     * @param array $buttons
     * @param array $matchRule
     * @return mixed
     */
    public function menuCreate(int $wechatType, array $buttons, array $matchRule = [])
    {
        return $this->manager->getInstance($wechatType)->menuCreate($buttons, $matchRule);
    }

    /**
     * 获取当前菜单.
     * @param int $wechatType
     * @return mixed
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function menuGet(int $wechatType)
    {
        return $this->manager->getInstance($wechatType)->menuGet();
    }

    public function menuDelete(int $wechatType, int $menuId = null)
    {
        return $this->manager->getInstance($wechatType)->menuDelete($menuId);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function getTemplateList(int $wechatType): array
    {
        return $this->manager->getInstance($wechatType)->getTemplateList();
    }

    public function sendServeAudit(array $auditors, $seriesName, $hospitalName, $trueName, $companyName = "")
    {
        //模板参数
        $templateData = [
            'first' => '您有一个待审核的服务申请，请及时审核！',
            'keyword1' => $trueName,
            'keyword2' => $companyName,
            'keyword3' => sprintf("%s(%s)", $seriesName, $hospitalName),
            'keyword4' => date('Y-m-d H:i:s')
        ];
        self::sendTemplateByMsgSvc(
            $auditors,
            $templateData,
            Wechat::SERVE_APPLY,
            Wechat::WECHAT_CHANNEL_WXAPI
        );
    }

    private static function sendTemplateByMsgSvc(
        array $openIds,
        array $params,
        $templateAlias,
        $wxChannel = "wxapi",
        $url = ""
    ): void {
        ApplicationContext::getContainer()
            ->get(MessageRpcClient::class)
            ->sendWechatTemplateMsg(
                $openIds,
                $wxChannel,
                $templateAlias,
                MSG_BIZ_TYPE::BIZ_COMMON_TYPE,
                $url,
                $params,
                MSG_PRIORITY::PRIORITY_MIDDLE,
                false,
                "",
                false,
                ""
            );
    }

    /**
     * 发送解绑申请
     * @param array $auditors
     * @param $seriesName
     * @param $hospitalName
     * @param $trueName
     * @param string $companyName
     */
    public function sendRemoveServeAudit(array $auditors, $seriesName, $hospitalName, $trueName, $companyName = "")
    {
        //模板参数
        $templateData = [
            'first' => '您有一个待解除的服务申请，请及时审核！',
            'keyword1' => $trueName,
            'keyword2' => $companyName,
            'keyword3' =>sprintf("%s(%s)", $seriesName, $hospitalName),
            'keyword4' => date('Y-m-d H:i:s'),
            'remark'   => '感谢你选择我们的服务!'
        ];
        self::sendTemplateByMsgSvc($auditors, $templateData, Wechat::TPL_SERVE_NOTICE, Wechat::WECHAT_CHANNEL_WXAPI);
    }

    /**
     * 发送纠错提醒
     * @param array $auditors
     * @param $userName
     * @param $content
     * @param string $companyName
     */
    public function sendHelp(array $auditors, $userName, $content, $companyName = '')
    {
        //模板参数
        $templateData = [
            'first' => '您有一个平台求助，请及时查看！',
            'keyword1' => $userName,
            'keyword2' => $companyName,
            'keyword3' => $content,
            'keyword4' => date('Y-m-d H:i:s'),
            'remark'   => '感谢你选择我们的服务!'
        ];
        self::sendTemplateByMsgSvc($auditors, $templateData, Wechat::TPL_SERVE_NOTICE, Wechat::WECHAT_CHANNEL_WXAPI);
    }

    /**
     * 认证通知
     */
    public function sendVerifyResult(
        $openId,
        $time,
        $url,
        $userName = "",
        $agentName = "",
        $isPass = true,
        $reason = ""
    ) {
        //模板参数
        $first = $isPass ? sprintf("尊敬的%s，恭喜您，您的身份认证申请已通过！", $userName) :
            sprintf("尊敬的%s, 您的身份认证申请已驳回！", $userName);
        $templateData = [
            'first' => $first,
            'keyword1' => $agentName,
            'keyword2' =>$time,
            'keyword3' =>  $isPass ? "通过" : "驳回",
            'remark' => !$isPass ? sprintf("驳回原因: %s", $reason) : "",
        ];
        self::sendTemplateByMsgSvc([$openId], $templateData, Wechat::VERIFY_NOTICE, Wechat::WECHAT_CHANNEL_WXAPI, $url);
    }

    /**
     * 供应商群消息
     */
    public function sendSupplierGroupMsg($first, $keyword1, $keyword2, $remark, $openids, $url = '')
    {
        $templateData = [
            'first' => $first,
            'keyword1' => $keyword1,
            'keyword2' => $keyword2,
            'remark' => $remark
        ];
        self::sendTemplateByMsgSvc($openids, $templateData, Wechat::SUPPLIER_GROUP, Wechat::WECHAT_CHANNEL_WXAPI, $url);
    }

    /**
     * 供应商绑定消息
     */
    public function sendSupplierApplyMsg($first, $keyword1, $keyword2, $remark, $openids, $url = '')
    {
        $templateData = [
            'first' => $first,
            'keyword1' => $keyword1,
            'keyword2' => $keyword2,
            'remark' => $remark
        ];
        self::sendTemplateByMsgSvc($openids, $templateData, Wechat::SUPPLIER_APPLY, Wechat::WECHAT_CHANNEL_WXAPI, $url);
    }

    /**
     * 供应商群消息
     */
    public function sendSupplierUnbindMsg($first, $keyword1, $keyword2, $remark, $openids, $url = '')
    {
        $templateData = [
            'first' => $first,
            'keyword1' => $keyword1,
            'keyword2' => $keyword2,
            'remark' => $remark
        ];
        self::sendTemplateByMsgSvc(
            $openids,
            $templateData,
            Wechat::SUPPLIER_UNBIND,
            Wechat::WECHAT_CHANNEL_WXAPI,
            $url
        );
    }

    /**
     * 直播邀请消息
     */
    public function sendLiveInviteMsg($first, $keyword1, $keyword2, $remark, $openids, $url = '')
    {
        $templateData = [
            'first' => $first,
            'keyword1' => $keyword1,
            'keyword2' => $keyword2,
            'remark' => $remark
        ];
        self::sendTemplateByMsgSvc($openids, $templateData, Wechat::LIVE_INVITE, Wechat::WECHAT_CHANNEL_YOUJIAYI, $url);
    }
}
