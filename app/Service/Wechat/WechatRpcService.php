<?php
declare(strict_types=1);

namespace App\Service\Wechat;

use Grpc\Wechat\WechatDoInfoByUnionIdReply;
use Grpc\Wechat\WechatDoInfoByUnionIdRequest;
use Youyao\Framework\Client\DoctorClient;

class WechatRpcService extends DoctorClient
{
    /**
     * @param WechatDoInfoByUnionIdRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function getWechatDoInfoByUnionId(WechatDoInfoByUnionIdRequest $argument)
    {
        return $this->sendRequest($argument, [WechatDoInfoByUnionIdReply::class, 'decode']);
    }
}
