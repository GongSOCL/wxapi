<?php
declare(strict_types=1);

namespace App\Service\Wechat;

use App\Constants\DataStatus;
use App\Model\Qa\WechatUser;
use App\Repository\WechatLogRepository;
use App\Repository\WechatUserRepository;
use EasyWeChat\Kernel\Contracts\MessageInterface;
use EasyWeChat\Kernel\Messages\Message;
use EasyWeChat\OfficialAccount\Server\Guard;
use phpDocumentor\Reflection\Types\False_;

class WechatService
{
    public static function logMessage($message, $wechatType)
    {
        return WechatLogRepository::log($message, $wechatType);
    }

    public static function processOauthRawMsg(array $raw = [], $type = DataStatus::WECHAT_AGENT)
    {
        $openId = isset($raw['openid']) && $raw['openid'] ? $raw['openid'] : '';
        if (!$openId) {
            return;
        }

        //更新wechat_user记录
        $wechat = WechatUserRepository::getOneWechatInfo($openId, $type);
        if ($wechat) {
            WechatUserRepository::updateWechatInfoFromOauthRawInfo($wechat, $raw);
        } else {
            WechatUserRepository::addWechatInfoFromOauthRawInfo($raw, $type);
        }
    }
}
