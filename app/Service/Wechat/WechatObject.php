<?php
declare(strict_types=1);
namespace App\Service\Wechat;

use App\Constants\ErrorCode;
use App\Exception\WechatException;
use App\Helper\AccessTokenProvider;
use App\Helper\EasyWechatCache;
use App\Helper\Helper;
use EasyWeChat\Factory;
use EasyWeChat\Kernel\Exceptions\InvalidConfigException;
use EasyWeChat\OfficialAccount\Application;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use Hyperf\Guzzle\CoroutineHandler;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Overtrue\Socialite\Exceptions\AuthorizeFailedException;
use Hyperf\Di\Annotation\Inject;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;
use function PHPStan\dumpType;

/**
 * 微信功能类
 * Class WechatObject
 * @package App\Service\Wechat
 */
class WechatObject
{
    //微信配置，参数参考:https://www.easywechat.com/docs/4.1/official-account/configuration
    private $config;

    //模板列表
    private $templates;

    /**
     * @var Application
     */
    private $officialAccount;

    //是否缓存
    private $cacheable;

    private $channelAlias = "";

    /**
     * @Inject
     * @var RequestInterface
     */
    private $hyperfRequest;

    public function __construct($config, $templates, $cacheable, $channelAlias = "")
    {
        $this->config = $config;
        $this->templates = $templates;
        $this->cacheable = $cacheable;
        $this->channelAlias = $channelAlias;
    }


    //获取OfficeAccount对象
    private function getOfficeAccountApp($rebindRequest = true): Application
    {
        $originAccount = $this->officialAccount;
        if (!$originAccount) {
            $this->initAccountApp();
            $originAccount = $this->officialAccount;
        }

        //需要克隆一个新对象并转换hyperf请求参数到当前对象
        $accountApp = clone $originAccount;
        if ($rebindRequest) {
            $accountApp->rebind('request', $this->buildRequestFromHyperf());
        }
        $accountApp->rebind('logger', Helper::getLogger());
        return $accountApp;
    }


    private function buildRequestFromHyperf(): Request
    {
        $get = $this->hyperfRequest->getQueryParams();
        $post = $this->hyperfRequest->getParsedBody();
        $cookie = $this->hyperfRequest->getCookieParams();
        $uploadFiles = $this->hyperfRequest->getUploadedFiles() ?? [];
        $server = $this->hyperfRequest->getServerParams();
        $xml = $this->hyperfRequest->getBody()->getContents();
        $files = [];
        /** @var \Hyperf\HttpMessage\Upload\UploadedFile $v */
        foreach ($uploadFiles as $k => $v) {
            $files[$k] = $v->toArray();
        }
        $request = new Request($get, $post, [], $cookie, $files, $server, $xml);
        $request->headers = new HeaderBag($this->hyperfRequest->getHeaders());

        return $request;
    }

    /**
     * 检查微信调用结果
     * @param $result
     * @param $api
     * @param array $data
     */
    private static function checkResult($result, $api, $data = [])
    {
        if (isset($result['errcode']) && $result['errcode'] && $result['errcode'] != 0) {
            Helper::getLogger()->error("wechatobject.checkResult", [
                'api' => $api,
                'result' => $result,
                'data' => $data
            ]);
            throw WechatException::newFromCode($result['errcode'], $result['errmsg'] ?? '', $api, $data);
        }
    }



    private function initAccountApp()
    {
        $app = Factory::officialAccount($this->config);

        $handler = new CoroutineHandler();

        // 设置 HttpClient，部分接口直接使用了 http_client。
        $config = $app['config']->get('http', []);
        $config['handler'] = $stack = HandlerStack::create($handler);
        $app->rebind('http_client', new Client($config));

        // 部分接口在请求数据时，会根据 guzzle_handler 重置 Handler
        $app['guzzle_handler'] = $handler;

        // 如果使用的是 OfficialAccount，则还需要设置以下参数
        $app->oauth->setGuzzleOptions([
            'http_errors' => false,
            'handler' => $stack,
        ]);

        $app['cache'] = new EasyWechatCache();
        $channelAlias = $this->channelAlias;
        if ($channelAlias) {
            $app['access_token'] = function ($app) use ($channelAlias) {
                return  AccessTokenProvider::newInstance($app, $channelAlias);
            };
        }


        $this->officialAccount = $app;
    }

    /**
     *  发送模板消息
     * @param $templateType
     * @param $toUserOpenId
     * @param array $data
     * @param string $url
     */
    public function sendTemplateMsg($templateType, $toUserOpenId, $data = [], $url = '')
    {
        if (!(isset($this->templates[$templateType]) && $this->templates[$templateType])) {
            Helper::getLogger()->error("微信模板未配置", [
                'type' => $templateType,
                'openid' => $toUserOpenId
            ]);
            return;
        }

        $sendData = [
            'touser' => $toUserOpenId,
            'template_id' => $this->templates[$templateType],
            'data' => $data
        ];
        if ($url) {
            $sendData['url'] = $url;
        }

        $result = $this->getOfficeAccountApp()->template_message->send($sendData);
        self::checkResult($result, 'sendTemplateMsg');
    }

    /**
     * 获取当前有效的access_token
     *
     * @return array
     */
    public function getAccessToken(): string
    {
        $result = $this->getOfficeAccountApp()->access_token->getToken();
        if (isset($result['errcode']) && $result['errcode']) {
            throw new WechatException($result['errcode'], $result['errmsg'], 'getAccessToken');
        }
        self::checkResult($result, 'getAccessToken');
        return $result['access_token'];
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \EasyWeChat\Kernel\Exceptions\BadRequestException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws InvalidConfigException
     */
    public function serve()
    {
        $result = $this->getOfficeAccountApp()->server->serve();
        self::checkResult($result, 'serve');
        return $result;
    }

    /**
     * @return \EasyWeChat\OfficialAccount\Server\Guard
     */
    public function getServer(): \EasyWeChat\OfficialAccount\Server\Guard
    {
        return $this->getOfficeAccountApp()->server;
    }

    /**
     * 获取临时二维码
     * @param $inviteId //邀请人的id
     * @return string
     */
    public function getQrcode($inviteData)
    {
        //通过微信用户信息中的qr_scene_str字段获取
        $url = '';
        $qrcode = $this->getOfficeAccountApp()->qrcode->temporary($inviteData, 6 * 24 * 3600);

        self::checkResult($qrcode, 'getQrcode');

        if (isset($qrcode['ticket'])) {
            $url = $this->getOfficeAccountApp()->qrcode->url($qrcode['ticket']);
        }
        return $url;
    }

    /**
     * 获取oauth授权地址
     *
     * @param $redirectUrl
     * @param int $isSilent
     * @param string $state
     * @return string
     */
    public function getOauthRedirectUrl($redirectUrl, $isSilent = 1, $state = ''): string
    {
        $scopes = $isSilent ? 'snsapi_base' : 'snsapi_userinfo';
        if ($state) {
            $result = $this->getOfficeAccountApp()->oauth
                ->scopes([$scopes])
                ->withState($state)
                ->redirect($redirectUrl);
        } else {
            $result = $this->getOfficeAccountApp()->oauth
                ->scopes([$scopes])
                ->redirect($redirectUrl);
        }
        self::checkResult($result, 'getOauthRedirectUrl');
        return $result;
    }

    /**
     * 获取微信网页授权用户信息
     *
     */
    public function getOauthUserInfo()
    {
        try {
            $app = $this->getOfficeAccountApp();
            $code = $app->request->get('code');
            $userInfo = $app->oauth->userFromCode($code);
            self::checkResult($userInfo, 'getOauthUserInfo');

            return $userInfo;
        } catch (\Exception $e) {
            if ($e instanceof AuthorizeFailedException) {   //还原原始的微信信息
                $body = $e->body;
                $code = $body['errcode'] ?? ErrorCode::WECHAT_BUSINESS_EXCEPTIN;
                $msg = $body['errmsg'] ?? ErrorCode::getMessage(ErrorCode::WECHAT_BUSINESS_EXCEPTIN);
                throw new WechatException($code, $msg);
            } else {
                throw $e;
            }
        }
    }

    /**
     * @param $url
     * @param array $apis
     * @param false $debug
     * @param bool $beats
     * @return array
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function getWechatJsSignature($url, array $apis, $debug = false, $beats = false): array
    {
        return $this->getOfficeAccountApp()->jssdk
            ->setUrl($url)
            ->getConfigArray($apis, $debug, false);
    }

    /**
     * 调用服务端接口获取已授权用户信息
     *
     * @param $openId
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws InvalidConfigException
     */
    public function getServerUserInfo($openId)
    {
            $result = $this->getOfficeAccountApp()->user->get($openId);

            self::checkResult($result, 'getServerUserInfo');

            return $result;
    }

    /**
     * 创建自定义菜单.
     * @param array $buttons
     * @param array $matchRule
     * @return mixed
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function menuCreate(array $buttons, array $matchRule = [])
    {
        return $this->getOfficeAccountApp(false)->menu->create($buttons, $matchRule);
    }

    /**
     * 获取当前菜单.
     * @return mixed
     * @throws InvalidConfigException
     */
    public function menuGet()
    {
        return $this->getOfficeAccountApp(false)->menu->current();
    }

    public function menuDelete(int $menuId = null)
    {
        return $this->getOfficeAccountApp(false)->menu->delete($menuId);
    }

    /**
     * 获取模板列表
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function getTemplateList(): array
    {
        return $this->getOfficeAccountApp(false)->template_message->getPrivateTemplates();
    }
}
