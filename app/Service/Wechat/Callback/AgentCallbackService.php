<?php

declare(strict_types=1);

namespace App\Service\Wechat\Callback;

use App\Constants\DataStatus;
use App\Model\Qa\NewAgentInvite;
use App\Repository\NewGroup\NewAgentInviteRepository;
use App\Repository\WechatUserRepository;
use App\Service\Group\AgentGroupService;
use App\Service\Wechat\WechatBusiness;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Psr\Log\LoggerInterface;

/**
 * 代表公众号回调
 *
 * Class DoctorCallbackService
 * @package App\Service\Wechat\Callback
 */
class AgentCallbackService implements CallbackInterface
{
    use CallbackProxyTrait;

    /**
     * 订阅事件
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function subscribe($message, LoggerInterface $logger)
    {
        //TODO:判断是否有订阅记录，没有的话添加一条订阅记录

        return new News([
            new NewsItem([
                'title' => '感谢您关注优药平台',
                'description' => '优药商业平台合伙人招募中；招募专业推广国际品牌药品和器械产品区域合伙人。',
                'image' => 'http://img.youyao99.com/3B8028FF45A695D13D3B8F2B2767C6CB.png',
                'url' => 'https://mp.weixin.qq.com/'.
                    's?__biz=MzI0MDg2Njc3OA==&mid=2247484592&'.
                    'idx=2&sn=f7d33429f014058e4efe5e620ceab3a2&'.
                    'chksm=e9150005de6289132a7462e883ed66edf87978f7f715931b6de3e4231c2cc733a96273d4122b&'.
                    'token=328115637&lang=zh_CN#rd'
            ])
        ]);
    }

    /**
     * 扫码事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function scan($message, LoggerInterface $logger)
    {
        $eventKey = $message['EventKey'];
        if ($message['Event'] == 'subscribe') {
            $eventKey = substr($message['EventKey'], 8);
        }
        //是否已经注册成为平台用户
        $wechatRegInfo = WechatUserRepository::getOneWechatInfo($message['FromUserName'], DataStatus::WECHAT_AGENT);

        //调用微信接口获取微信
        $wechatUserInfo = WechatBusiness::getContextWechatInfo();
        if (!$wechatUserInfo) {
            $wechatUserInfo = ApplicationContext::getContainer()->get(WechatBusiness::class)
                ->getWechatInfo($message['FromUserName'], DataStatus::WECHAT_AGENT);
        }

        //分解key判断活动入口
        list($activityId, $uid, $gid) = self::unpackScanKey($eventKey);
        if ($activityId && $uid && $activityId == DataStatus::ACTIVITY_ID_QRCODE_AGENT_GROUP && $gid) {  //医生群组事件
            AgentGroupService::processAgentGroupEvent(
                $uid,
                $wechatUserInfo,
                $gid,
                $wechatRegInfo,
                DataStatus::WECHAT_AGENT
            );
        } elseif ($activityId && $uid && $activityId == DataStatus::ACTIVITY_ID_QRCODE_AGENT_INVITE) {
            AgentGroupService::processAgentInviteEvent($uid, $wechatUserInfo, $wechatRegInfo, DataStatus::WECHAT_AGENT);
        }

        if ($message['Event'] == 'SCAN') {
            return <<<eof
欢迎加入优药平台！
优药商业平台招募专业推广国际品牌药品和器械产品区域合伙人。
eof;
        }
    }

    /**
     * 自定义菜单点击事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function click($message, LoggerInterface $logger)
    {
        $EventKey = $message['EventKey'];
        if ($EventKey == "MENU-CPZS") {//产品招商
            return new News([
                new NewsItem([
                    'title' => '宏健引流产品系列火热招商中！！！',
                    'description' => '优药',
                    'image' => 'https://mmbiz.qpic.cn/mmbiz_jpg/'.
                        'icLJAIegjKqzRIzlKj3mfVbHyAlQRkMTr1kydVAzgY'.
                        'giaZXHtwvauUicQ21K2lpziaP72gfEFtL7q2rPyKvY2Qicgcw/'.
                        '640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1',
                    'url' => 'https://mp.weixin.qq.com/s/BjhZcLa9BSqyo1rAWwLUEQ'
                ]),
                new NewsItem([
                    'title' => '太洛汀® 火热招商中！',
                    'description' => '优药',
                    'image' => 'https://mmbiz.qpic.cn/mmbiz_jpg/'.
                        'icLJAIegjKqwiasdsuD20ia4JlYib0pOCPbXjLsKzM'.
                        'bRndUyBXicBPJRgz5FBq3ib5d0phLReqJicKp7a9LUeQFcdYvTw/'.
                        '640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1',
                    'url' => 'https://mp.weixin.qq.com/s/GUvj-uqP3lsGFMSqKejQ3w'
                ])
            ]);
        }


        if ($EventKey == "MENU-WXFW") {//文献服务
            return <<<eof
优药文献服务致力于为医疗从业人员提供最好的学术支持服务，您可以回复序号获取服务：

【1】获取文献

【2】翻译文献

【3】其他学术帮助
eof;
        }
    }

    /**
     * 取消订阅事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function unsubscribe($message, LoggerInterface $logger)
    {
        $eventKey = $message['EventKey'];
        if ($message['Event'] == 'subscribe') {
            $eventKey = substr($message['EventKey'], 8);
        }
        $wechatUserInfo = WechatBusiness::getContextWechatInfo();
        if (!$wechatUserInfo) {
            $wechatUserInfo = ApplicationContext::getContainer()->get(WechatBusiness::class)
                ->getWechatInfo($message['FromUserName'], DataStatus::WECHAT_AGENT);
        }
        list($activityId, $uid, $gid) = self::unpackScanKey($eventKey);
        //group_member取消订阅
        NewAgentInviteRepository::changeSubscribe(
            $uid,
            $wechatUserInfo['unionid'],
            DataStatus::WECHAT_AGENT,
            NewAgentInvite::NOT_SUBSCRIBE
        );
        //wechat_user取消订阅
        WechatUserRepository::unsubscribe($wechatUserInfo['unionid'], DataStatus::WECHAT_AGENT);
    }

    /**
     * 发送普通文件消息事件
     *
     * @param $message
     * @param $
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function text($message, LoggerInterface $logger)
    {
        $keywords = trim($message['Content']);
        if (!$keywords) {   //没有任何内容
            return false;
        }

        if ($keywords == '直播') {
            return 'http://live.youyao99.com';
        }

        if ($keywords == "内部直播") {
            return "http://wx.youyao99.com/template/live/live_list.php";
        }

        if ($keywords == "英语文献") {//
            $describe = 'Youyao literature service is committed to providing '.
                ' the best academic support service for medical practitioners. '.
                'You can reply to the serial number to get the service:';
            return <<<eof
$describe

[1] Literature search

[2] Translation of Literature

[3] Other academic help
eof;
        }

        if ($keywords == "英语文献2") {//
            return 'Please reply to the literature materials you need to look up directly. '.
                'Our literature assistant will look up for you wholeheartedly.';
        }

        if ($keywords == "英语文献3") {//
            return "Ok, just a moment, please. We're looking for you.";
        }

        if ($keywords == "英语文献3") {//
            return "Ok, just a moment, please. We're looking for you.";
        }

        if ($keywords == "rd" || $keywords == "新药研发") {//
            return "http://rd.youyao99.com/template/";
        }

        if ($keywords == "月报") {//
            return "您好，点击http://wx.youyao99.com/company-report.html查询月报";
        }

        if ($keywords == "使用报告") {//
            return <<<eof
优锐医药http://wx.youyao99.com/youyao-report.html
优药全平台http://wx.youyao99.com/youyao-platform-report.html
RP0.2 http://rp.youyao99.com/home/favdata
eof;
        }

        if ($keywords == "优药久久") {//
            return <<<eof
线上伪登陆：http://wx.youyao99.com/weiv2/user_list.php
线上个人中心: http://wx.youyao99.com/v2wx/personalCenter						
线上产品列表：http://wx.youyao99.com/v2wx/

QA2伪登陆：http://wx.qa2.youyao99.com/qaweiv2/user_list.php
QA2个人中心：http://wx.qa2.youyao99.com/v2wx/personalCenter							
QA2产品列表：http://wx.qa2.youyao99.com/v2wx/

QA伪登陆：http://wx.qa.youyao99.com/qaweiv2/user_list.php
QA个人中心：http://wx.qa.youyao99.com/v2wx/personalCenter
QA产品列表：http://wx.qa.youyao99.com/v2wx/
eof;
        }
        if ($keywords == "优药久久旧") {//
            $hglsQA = 'http://wx.qa.youyao99.com/index.php?version=139&'.
                'a_v_flag=2#/drug/details?product_yyid=1CFA19C5921EFAA41E8A03B3E852B7AC&'.
                'id_type=1CFA19C5921EFAA41E8A03B3E852B7AC&'.
                'series_yyid=529435CCE7AB708DC55BAE3A1486DD03&t=139';
            $hglsQA2 = 'http://wx.qa2.youyao99.com/index.php?version=139&'.
                'a_v_flag=2#/drug/details?product_yyid=1CFA19C5921EFAA41E8A03B3E852B7AC&'.
                'id_type=1CFA19C5921EFAA41E8A03B3E852B7AC&'.
                'series_yyid=529435CCE7AB708DC55BAE3A1486DD03&t=139';
            return <<<eof
线上伪登陆：http://wx.youyao99.com/w_login/user_list.php
QA2伪登陆：http://wx.qa2.youyao99.com/w_login/user_list.php
QA2测试地址：http://wx.qa2.youyao99.com/index.php#/user/personal
QA伪登陆：http://wx.qa.youyao99.com/w_login/user_list.php
QA测试地址：http://wx.qa.youyao99.com/index.php#/user/personal
QA护固莱士：$hglsQA
QA2护固莱士：$hglsQA2
eof;
        }

        if ($keywords == "1") {//
            return "请直接回复您需要查找的文献资料，我们的文献助手会竭诚为您查询";
        }

        if ($keywords == "2") {//
            return "您可以将需要翻译的文献资料发送至xueshu@nuancebiotech.cn，我们会将翻译完成后的文件通过邮箱发送给您。";
        }

        if ($keywords == "3") {//
            return "请直接回复您需要的学术服务或支持，或者您也可以通过xueshu@nuancebiotech.cn联系我们。";
        }

        if ($keywords == "FS流向报告") {//
            return "FS流向报告: http://wx.youyao99.com/v2wx/fsReport";
        }

        return false;
    }

    public static function isText(): bool
    {
        $type = ApplicationContext::getContainer()
            ->get(RequestInterface::class)
            ->route('type', DataStatus::WECHAT_AGENT);
        return $type ==  DataStatus::WECHAT_AGENT;
    }

    public static function isSubscribe(): bool
    {
        $type = ApplicationContext::getContainer()
            ->get(RequestInterface::class)
            ->route('type', DataStatus::WECHAT_AGENT);
        return $type ==  DataStatus::WECHAT_AGENT;
    }

    public static function isUnsubscribe(): bool
    {
        $type = ApplicationContext::getContainer()
            ->get(RequestInterface::class)
            ->route('type', DataStatus::WECHAT_AGENT);
        return $type ==  DataStatus::WECHAT_AGENT;
    }

    public static function isScan(): bool
    {
        $type = ApplicationContext::getContainer()
            ->get(RequestInterface::class)
            ->route('type', DataStatus::WECHAT_AGENT);
        return $type ==  DataStatus::WECHAT_AGENT;
    }

    public static function isClick(): bool
    {
        $type = ApplicationContext::getContainer()
            ->get(RequestInterface::class)
            ->route('type', DataStatus::WECHAT_AGENT);
        return $type ==  DataStatus::WECHAT_AGENT;
    }
}
