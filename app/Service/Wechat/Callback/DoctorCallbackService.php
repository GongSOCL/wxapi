<?php

declare(strict_types=1);

namespace App\Service\Wechat\Callback;

use App\Constants\DataStatus;
use App\Repository\GroupMemberRepository;
use App\Repository\WechatUserRepository;
use App\Service\Doctor\DoctorInviterService;
use App\Service\Group\AgentGroupService;
use App\Service\Group\DoctorGroupService;
use App\Service\Wechat\WechatBusiness;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Psr\Log\LoggerInterface;

/**
 * 医生公众号回调
 *
 * Class DoctorCallbackService
 * @package App\Service\Wechat\Callback
 */
class DoctorCallbackService implements CallbackInterface
{
    use CallbackProxyTrait;

    /**
     * 订阅事件
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function subscribe($message, LoggerInterface $logger)
    {

        return new News([
            new NewsItem([
                'title' => '感谢您关注优佳医平台',
                'description' => '优佳医平台专注于医生的信息化服务，为您连线行业内最新资讯',
                'image' => 'http://static.youyao99.com/4b21e88f402e055d29545934668fb216.jpg',
                'url' => 'https://mp.weixin.qq.com/s/ao1jYqs9HrdL0B05CnDjKA'
            ])
        ]);
    }

    /**
     * 扫码事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function scan($message, LoggerInterface $logger)
    {
        $eventKey = $message['EventKey'];
        if ($message['Event'] == 'subscribe') {
            $eventKey = substr($message['EventKey'], 8);
        }
        //是否已经注册成为平台用户
        $wechatRegInfo = WechatUserRepository::getOneWechatInfo($message['FromUserName'], DataStatus::WECHAT_DOCTOR);

        //调用微信接口获取微信
        $wechatUserInfo = WechatBusiness::getContextWechatInfo();
        if (!$wechatUserInfo) {
            $wechatUserInfo = ApplicationContext::getContainer()->get(WechatBusiness::class)
                ->getWechatInfo($message['FromUserName'], DataStatus::WECHAT_DOCTOR);
        }


        //分解key判断活动入口
        list($activityId, $uid, $gid) = self::unpackScanKey($eventKey);
        if ($activityId && $uid &&
            $activityId == DataStatus::ACTIVITY_ID_QRCODE_DOCTOR_GROUP) {  //医生群组事件
            DoctorGroupService::processDoctorGroupEvent(
                $uid,
                $wechatUserInfo,
                $wechatRegInfo,
                DataStatus::WECHAT_DOCTOR
            );
        } elseif ($activityId && $uid
            && $activityId == DataStatus::ACTIVITY_ID_QRCODE_DOCTOR_INVITE) { //医生邀请事件
//            DoctorGroupService::processDoctorInviteEvent(
//$uid,
// $wechatUserInfo,
// $wechatRegInfo,
// DataStatus::WECHAT_DOCTOR
//);
            //使用t_reps_doctor_inviter表和t_group_member分开维护
            DoctorInviterService::processDoctorInviteEvent(
                $uid,
                $wechatUserInfo,
                $wechatRegInfo,
                DataStatus::WECHAT_DOCTOR
            );
        }

        if ($message['Event'] == 'SCAN') {
            return <<<eof
欢迎加入优佳医平台！
优佳医平台专注于医生的信息化服务，为您连线行业内最新资讯。
eof;
        }
    }

    /**
     * 自定义菜单点击事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function click($message, LoggerInterface $logger)
    {
    }

    /**
     * 取消订阅事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function unsubscribe($message, LoggerInterface $logger)
    {
        //group_member取消订阅
        GroupMemberRepository::batchSetUnsubscribe($message['FromUserName'], DataStatus::WECHAT_DOCTOR);
        //wechat_user取消订阅
        WechatUserRepository::unsubscribe($message['FromUserName'], DataStatus::WECHAT_AGENT);
    }

    /**
     * 发送普通文件消息事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function text($message, LoggerInterface $logger)
    {
    }

    public static function isText(): bool
    {
//        $type = ApplicationContext::getContainer()
//->get(RequestInterface::class)
//->route('type', DataStatus::WECHAT_AGENT);
//        return $type ==  DataStatus::WECHAT_DOCTOR;
        return false;
    }

    public static function isSubscribe(): bool
    {
        $type = ApplicationContext::getContainer()
            ->get(RequestInterface::class)
            ->route('type', DataStatus::WECHAT_AGENT);
        return $type ==  DataStatus::WECHAT_DOCTOR;
    }

    public static function isUnsubscribe(): bool
    {
        $type = ApplicationContext::getContainer()
            ->get(RequestInterface::class)
            ->route('type', DataStatus::WECHAT_AGENT);
        return $type ==  DataStatus::WECHAT_DOCTOR;
    }

    public static function isScan(): bool
    {
        $type = ApplicationContext::getContainer()
            ->get(RequestInterface::class)
            ->route('type', DataStatus::WECHAT_AGENT);
        return $type ==  DataStatus::WECHAT_DOCTOR;
    }

    public static function isClick(): bool
    {
        $type = ApplicationContext::getContainer()
            ->get(RequestInterface::class)
            ->route('type', DataStatus::WECHAT_AGENT);
        return $type ==  DataStatus::WECHAT_DOCTOR;
    }
}
