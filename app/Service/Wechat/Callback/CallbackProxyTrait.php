<?php
declare(strict_types=1);
namespace App\Service\Wechat\Callback;

use Hyperf\Utils\ApplicationContext;

trait CallbackProxyTrait
{
    public static function textProxy($message)
    {
        if (self::isText() && $message['MsgType'] ==  'text') {
            $logger = self::getLogger();
            $logger->info('wechat_text_callback_message', $message);
            return self::text($message, $logger);
        }
    }

    public static function unsubscribeProxy($message)
    {
        if (self::isUnsubscribe() && $message['MsgType'] == 'event' && $message['Event'] == 'unsubscribe') {
            $logger = self::getLogger();
            $logger->info('wechat_unsubscribe_callback_message', $message);
            return self::unsubscribe($message, $logger);
        }
    }

    public static function clickProxy($message)
    {
        if (self::isClick() && $message['MsgType'] == 'event' && $message['Event'] == 'CLICK') {
            $logger = self::getLogger();
            $logger->info('wechat_click_callback_message', $message);
            return self::click($message, $logger);
        }
    }

    public static function scanProxy($message)
    {
        //扫码事件，或者未关注时推送subscribe
        if (self::isScan() &&
            $message['MsgType'] == 'event' &&
            isset($message['EventKey']) && $message['EventKey'] &&
            ($message['Event'] == 'SCAN' || $message['Event'] == 'subscribe')
        ) {
            $logger = self::getLogger();
            $logger->info('wechat_scan_callback_message', $message);
            $logger->info("call_scan_class", [
                'class' => __CLASS__,
            ]);
            return self::scan($message, $logger);
        }
    }

    public static function subscribeProxy($message)
    {
        if (self::isSubscribe() && $message['MsgType'] == 'event' && $message['Event'] == 'subscribe') {
            $logger = self::getLogger();
            $logger->info('wechat_subscribe_callback_message', $message);
            return self::subscribe($message, $logger);
        }
    }

    /**
     * 获取上下文日志处理器
     *
     * @return \Psr\Log\LoggerInterface
     */
    public static function getLogger(): \Psr\Log\LoggerInterface
    {
        return ApplicationContext::getContainer()->get(\Hyperf\Logger\LoggerFactory::class)->get('default');
    }

    protected static function unpackScanKey($eventKey): array
    {
        $activityId = $uid = $gid ='';
        $_eventkey = explode('_', $eventKey);
        if (count($_eventkey) > 2) {
            $uid = $_eventkey[1];
            $gid = count($_eventkey) >= 3 ? $_eventkey[2] : '';
            $eventkey_1 = $_eventkey[0];
            $eventkey_2 = explode('#', $eventkey_1);
            $activityId = $eventkey_2[0];
        }
        return [$activityId, $uid, $gid];
    }
}
