<?php
declare(strict_types=1);
namespace App\Service\Wechat\Callback;

use Psr\Log\LoggerInterface;

interface CallbackInterface
{
    /**
     * 是否支持文本消息事件回调
     * @return bool
     */
    public static function isText():bool;

    /**
     * 是否支持订阅事件
     * @return bool
     */
    public static function isSubscribe():bool;

    /**
     * 是否支持取消订阅事件
     * @return bool
     */
    public static function isUnsubscribe():bool;

    /**
     * 是否支持扫码事件
     * @return bool
     */
    public static function isScan():bool;

    /**
     * 是否支持点击事件
     * @return bool
     */
    public static function isClick():bool;

    /**
     * 订阅事件
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function subscribe($message, LoggerInterface $logger);

    /**
     * 扫码事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function scan($message, LoggerInterface $logger);

    /**
     * 自定义菜单点击事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function click($message, LoggerInterface $logger);

    /**
     * 取消订阅事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function unsubscribe($message, LoggerInterface $logger);

    /**
     * 发送普通文件消息事件
     *
     * @param $message
     * @param LoggerInterface $logger
     * @return mixed
     */
    public static function text($message, LoggerInterface $logger);
}
