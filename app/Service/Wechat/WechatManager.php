<?php
declare(strict_types=1);
namespace App\Service\Wechat;

use App\Constants\DataStatus;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Container;

/**
 * 微信帐号管理类
 *
 * Class WechatManager
 * @package App\Service\Wechat
 */
class WechatManager
{

    private $wechatConfig;

    private $apps = [];

    public function __construct(Container $container)
    {
        /** @var ConfigInterface $config */
        $config = $container->get(ConfigInterface::class);
        if ($config->has('wechat')) {
            $this->wechatConfig = $config->get('wechat', []);
        }
    }

    public function getInstance($type = DataStatus::WECHAT_AGENT): WechatObject
    {
        $typeConfig = isset($this->wechatConfig[$type]) ? $this->wechatConfig[$type] : [];
        if (!$typeConfig) {
            throw new \UnexpectedValueException("微信类型尚未配置");
        }

        if (!isset($this->apps[$type])) {
            $this->apps[$type] = new WechatObject(
                $typeConfig['config'],
                $typeConfig['templates'],
                $typeConfig['cache'] ?? true,
                $typeConfig['alias'] ?? ''
            );
        }

        return $this->apps[$type];
    }
}
