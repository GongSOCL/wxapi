<?php
declare(strict_types=1);
namespace App\Service\Contract;

use App\Helper\Helper;

class ContractCacheService
{
    private static function buildRequestCacheKey($userId, $requestId)
    {
        return sprintf("youyao:contract:request:%d:%s", $userId, $requestId);
    }

    public static function cacheRequest($userId, $groupId, $requestId)
    {
        $key = self::buildRequestCacheKey($userId, $requestId);
        $cache = Helper::getRedis('pod');
        $cache->set($key, $groupId);
        $cache->expire($key, 600);
    }

    public static function getGroupByCache($userId, $requestId): int
    {
        $key = self::buildRequestCacheKey($userId, $requestId);
        $res = (int)Helper::getRedis('pod')->get($key);
        if (!$res) {
            throw new \UnexpectedValueException("非法请求，请重试");
        }
        return $res;
    }
}
