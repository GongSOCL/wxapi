<?php
declare(strict_types=1);
namespace App\Service\Contract;

use App\Constants\Auth;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Kernel\Minio\Minio;
use App\Model\Qa\OaSignGroup;
use App\Model\Qa\OaUserSign;
use App\Repository\Contract\OaSignGroupRepository;
use App\Repository\Contract\OaUserSignRepository;
use App\Service\Common\SmsService;
use App\Service\Msg\MsgRpcService;
use Grpc\Msg\MSG_BIZ_TYPE;
use Grpc\Msg\MSG_PRIORITY;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use Youyao\Framework\Components\Msg\MessageRpcClient;
use ZipStream\Option\Archive;
use ZipStream\ZipStream;

class ContractService
{
    /**
     * @param int $state
     * @param int $current
     * @param int $limit
     * @return array
     */
    public static function contractGroupList($state = 0, $current = 1, $limit = 10, $msgId = 0): array
    {
        $user = Helper::getLoginUser();
        if ($msgId > 0) {
            self::readMsg($msgId, $user, Helper::getAuthRole() == Auth::AUTH_ROLE_YOU_YAO_APP);
        }
        $signCount = OaSignGroupRepository::countToSigned($user);
        $resp = OaSignGroupRepository::getContractByStateWithGroup($user, $state, $current, $limit);
        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $limit
        ];
        $groups = [];
        if ($resp->isEmpty()) {
            return [$page, $groups, $signCount];
        }

        /** @var Collection $col */
        $col = $resp->getCollection();
        $groupIds  = $col->pluck('id')->all();
        $userSigned = OaUserSignRepository::getByGroupIds($groupIds);
        $signMap = $userSigned->groupBy('group_id')->all();
        $templateIds = $userSigned->pluck('template_id')->all();
        /** @var OaSignGroup $item */
        foreach ($resp as $item) {
            $groupItem = [
                'id' => $item->id,
                'name' => $item->title,
                'contracts' => [],
                'pdf_generated' => $item->isPdfGenerated() ? 1 : 0,
                'is_signed' => in_array(
                    $item->state,
                    [OaSignGroup::STATE_SIGNED, OaSignGroup::STATE_PDF_GENERATED]
                ) ? 1 : 0
            ];
            if (!isset($signMap[$item->id])) {
                continue;
            }

            /** @var OaUserSign $v */
            foreach ($signMap[$item->id] as $v) {
                $groupItem['contracts'][] = [
                    'id' => $v->id,
                    'name' => $v->title,
                    'no' => $v->contractno,
                    'start' => (string)$v->startdate,
                    'end' => (string)$v->enddate,
                    'pic' => ''
                ];
            }
            $groups[] = $groupItem;
        }

        return [$page, $groups, $signCount];
    }

    public static function startContractSign($id): array
    {
        $user = Helper::getLoginUser();
        $group  = OaSignGroupRepository::getGroupById($id);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }
        if (!$group->isValidToSign()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前合同组状态不允许签署");
        }

        $next = OaUserSignRepository::getGroupNextSign($user, $group, 0);
        if (!$next) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组未配置合同");
        }

        //添置kxrdh
        $requestId = uniqid();
        ContractCacheService::cacheRequest($user->uid, $group->id, $requestId);
        return [
            $requestId,
            $next->id
        ];
    }

    public static function readMsg($msgId, $user, $isApp)
    {
        try {
            ApplicationContext::getContainer()
                ->get(MsgRpcService::class)
                ->fetchAgentOneMsg($msgId, $user, $isApp);
        } catch (\Exception $e) {
            Helper::getLogger()->error("set_msg_read_error", [
                'msg_id' => $msgId,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public static function getContractTpl($id, int $contractId, string $requestId): array
    {
        $user = Helper::getLoginUser();
        $groupId = ContractCacheService::getGroupByCache($user->uid, $requestId);
        if ($groupId != $id) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法请求,合同组校验不一致");
        }

        $group  = OaSignGroupRepository::getGroupById($id);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }
        if (!$group->isValidToSign()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前合同组状态不允许签署");
        }

        $contract = OaUserSignRepository::getById($contractId);
        if (!$contract) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同不存在");
        }
        if ($contract->group_id != $id) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法请求，合同组不一致");
        }

        //下载模板文件
        $content = file_get_contents($contract->link);

        $next = OaUserSignRepository::getGroupNextSign($user, $group, $contractId);
        return [$content, $next ? $next->id : ""];
    }

    public static function getContractSignedPdf($id)
    {
        $contract = OaUserSignRepository::getById($id);
        if (!$contract) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同不存在");
        }
        if (!$contract->isPdfGenerated()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同pdf尚未生成完成");
        }

        return $contract->pdf_link;
    }

    public static function getContractTplInfo(int $id)
    {
        $contract = OaUserSignRepository::getById($id);
        if (!$contract) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同不存在");
        }
        if (!$contract->link) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同模模板不存在");
        }

        return file_get_contents($contract->link);
    }

    /**
     * @param int $groupId
     * @param $img
     * @param string $requestId
     * @param $code
     * @return void
     * @throws \Exception
     */
    public static function signContract(int $groupId, &$img, string $requestId, $code)
    {
        $user = Helper::getLoginUser();
        if (!($user->mobile_num && $user->mobile_verified)) {
            throw new BusinessException(ErrorCode::ERROR_MOBILE_NOT_BIND, "您尚未绑定手机号");
        }
        $verify = SmsService::verify($user->mobile_num, $code);
        if (!$verify) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "验证码错误，请重发");
        }
        $gid = ContractCacheService::getGroupByCache($user->uid, $requestId);
        if ($gid != $groupId) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法请求或请求已过期");
        }
        $group = OaSignGroupRepository::getGroupById($groupId);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }
        if (!$group->isValidToSign()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组状态不允许签署");
        }

        //上传签名文件
        ManageService::generatePdf($groupId, $img);
    }

    public static function sendContractWithMail(int $groupId, $mail = "")
    {
        $group = OaSignGroupRepository::getGroupById($groupId);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }
        if (!$group->isPdfGenerated()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同pdf未生成完成");
        }
        if ($mail) {
            $log = Helper::getLogger();
            try {
                self::mailContract($mail, $group);
                $log->debug("contract send mail success", [
                    'mail' => $mail,
                    'group' => $group->id
                ]);
            } catch (\Exception $e) {
                $log->error("contract send mail exception", [
                    'mail' => $mail,
                    'group' => $group->id,
                    'msg' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ]);
            }
        }
    }

    private static function mailContract($mail, OaSignGroup $group)
    {
        $link = self::zipContracts($group);
        ApplicationContext::getContainer()
            ->get(MessageRpcClient::class)
            ->sendMail(
                [$mail],
                sprintf("%s电子版合同", $group->title),
                '合同内容见附件!!',
                false,
                MSG_BIZ_TYPE::BIZ_COMMON_TYPE,
                '',
                MSG_PRIORITY::PRIORITY_MIDDLE,
                false,
                '',
                false,
                '',
                [
                    [sprintf("%s.zip", $group->title), $link]
                ]
            );
    }

    /**
     * 打包合同zip文件并上传到minio返回链接
     * @param OaSignGroup $group
     * @return string
     * @throws \ZipStream\Exception\OverflowException
     */
    private static function zipContracts(OaSignGroup $group): string
    {
        $contracts = $group->contracts()->get();
        $tempStream = fopen('php://memory', 'w+');

        // Init Options
        $zipStreamOptions = new Archive();
        $zipStreamOptions->setOutputStream($tempStream);

        // Create Zip Archive
        $zipStream = new ZipStream(sprintf("%s.zip", $group->title), $zipStreamOptions);
        /** @var OaUserSign $contract */
        foreach ($contracts as $contract) {
            $tmp = Helper::downloadFileToTmp($contract->pdf_link);
            $zipStream->addFileFromStream(
                sprintf("%s.pdf", $contract->title),
                $tmp
            );
            fclose($tmp);
        }
        $zipStream->finish();
        rewind($tempStream);
        $uploadPath = sprintf("/contract/mail/%s/%s.zip", date('Ymd'), $group->title);
        Minio::getFileSystem()->putStream($uploadPath, $tempStream);

        // Close Stream
        fclose($tempStream);

        //返回压缩文件路径
        return Minio::createPublicBucketFileLink(env('S3_BUCKET'), $uploadPath);
    }
}
