<?php
declare(strict_types=1);

namespace App\Service\Contract;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Kernel\Minio\Minio;
use App\Kernel\Uuid\Uuid;
use App\Model\Qa\OaSignGroup;
use App\Model\Qa\OaUserSign;
use App\Model\Qa\OaUserSignHospital;
use App\Model\Qa\OaUserSignHospitalTarget;
use App\Model\Qa\RepsServeScopeS;
use App\Repository\Contract\OaSignGroupRepository;
use App\Repository\Contract\OaSignImgRepository;
use App\Repository\Contract\OaSignTemplateRepository;
use App\Repository\Contract\OaUserInviteRepository;
use App\Repository\Contract\OaUserSignHospitalRepository;
use App\Repository\Contract\OaUserSignHospitalTargetRepository;
use App\Repository\Contract\OaUserSignRepository;
use App\Repository\Contract\OaYouyaoAttachmentContractRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\UsersRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Service\Contract\Items\ContractGroupService;
use App\Service\Contract\Items\UserAgreeOldService;
use App\Service\Contract\Items\UserAgreeService;
use App\Service\Contract\Items\YouyaoAttachmentOldService;
use App\Service\Contract\Items\YouyaoAttachmentService;
use App\Service\Contract\Items\YouyaoMasterOldTplService;
use App\Service\Contract\Items\YouyaoMasterTplService;
use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Youyao\Framework\ErrCode;

class ManageService
{
    /**
     * @param int $id 合同组id
     * @return void
     */
    public static function generateHtml($id)
    {
        ContractGroupService::generateHtmlTpl($id);
    }

    /**
     * @param int $id 合同组id
     * @param string $content 上传文件内容
     * @return void
     */
    public static function generatePdf($id, string &$content)
    {
        $user = Helper::getLoginUser();
        //检验合同组状态
        $group = OaSignGroupRepository::getGroupById($id);
        if (!$group->isValidToSign()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前合同状态不允许签署");
        }

        //上传文件到minio
        $path = sprintf(
            "/contract/sign/%s/%d_%s.txt",
            date('Ymd'),
            $user->uid,
            uniqid()
        );
        //更新合同记录状态
        Minio::getFileSystem()->put($path, $content);
        $uri = Minio::createPublicBucketFileLink(env('S3_BUCKET'), $path);

        //写入合同签名文件到minio并生成url
        Db::beginTransaction();
        try {
            $operateResult = OaSignGroupRepository::markSigned($group);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组状态更新失败");
            }
            //更新合同记录状态
            $operateResult = OaUserSignRepository::markGroupSigned($group);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同状态更新失败");
            }

            //添加签名图片
            $img = OaSignImgRepository::addImg($group, $uri);
            if (! $img) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "签名图片保存失败");
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }

        //获取合同组
        ContractGroupService::generatePdfContract($group, $uri);
    }


    public static function htmlGenerated($id, $isSuccess, $link = "")
    {
        $log = Helper::getLogger();
        //获取合同
        $contract = OaUserSignRepository::getById($id);
        if (!$contract) {
            $log->error("html生成回调合同不存在", [
                'id' => $id
            ]);
            return;
        }

        //判断状态
        if (!$contract->isValidToSign()) {
            $log->error("htm生成回调合同状态不正确", [
                'id' => $contract->id,
                'state' => $contract->state
            ]);
            return;
        }

        //调用合同组状态校验
        if ($isSuccess) {
            ContractGroupService::checkContractGroupHtmlOk($contract->group_id);
        }
    }

    public static function pdfGenerated($id, $isSuccess, $link = "")
    {
        $log = Helper::getLogger();
        //获取合同
        $contract = OaUserSignRepository::getById($id);
        if (!$contract) {
            $log->error("pdf生成回调合同不存在", [
                'id' => $id,
            ]);
            return;
        }

        //判断状态
        if (!in_array($contract->state, [OaUserSign::STATE_SIGNED, OaUserSign::STATE_PDF_GENERATED])) {
            $log->error("pdf生成回调合同状态不正确", [
                'id' => $contract->id,
                'state' => $contract->state
            ]);
            return;
        }

        //调用合同组状态校验
        if ($isSuccess) {
            ContractGroupService::checkContractGroupPdfOk($contract->group_id);
        } else {
            $log->error("合同生成pdf失败", [
                'id' => $id,
            ]);
        }
    }

    public static function addContractGroup($title, $userId, $start, $end, $signEnd = ""): OaSignGroup
    {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户不存在");
        }
        $company = $user->company;
        if (!$company) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组用户尚未认证");
        }

        $inviteInfo = OaUserInviteRepository::getUserInfo($userId, $company->id);
        if (!$inviteInfo) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "身份信息不存在");
        }
        if (!($inviteInfo->truename && $inviteInfo->id_card)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "身份证或真实姓名未录入");
        }

        return OaSignGroupRepository::addOne(
            $title,
            $userId,
            $start,
            $end,
            $signEnd
        );
    }

    /**
     * 添加优药主合同
     * @param $groupId
     * @param $title
     * @return OaUserSign
     */
    public static function addYouyaoMaster($groupId, $title, $isOld = false): OaUserSign
    {
        if ($isOld) {
            $identity = YouyaoMasterOldTplService::getIdentify();
        } else {
            $identity = YouyaoMasterTplService::getIdentify();
        }
        $group = OaSignGroupRepository::getGroupById($groupId);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }
        if ($group->state == OaSignGroup::STATE_NEW) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前合同组状态不允许添加其它合同");
        }

        //校验模板
        $template = OaSignTemplateRepository::getByAlias($identity);
        if (!$template) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同主模板尚未配置，请联系管理员");
        }

        $user = UsersRepository::getUserByUid($group->user_id);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组关联用户不存在");
        }
        $company = $user->company;
        if (!$company) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组用户尚未认证");
        }

        $no = strtoupper(uniqid());

        return OaUserSignRepository::addOne(
            $group,
            $template,
            $company,
            $title,
            $no,
            OaUserSign::TYPE_MASTER
        );
    }

    public static function addYouyaoAttachment(
        $groupId,
        $title,
        array $serves,
        $feeUnit,
        $percent = 100,
        $settlementDate = "",
        $isOld = false
    ): OaUserSign {
        if ($isOld) {
            $identity = YouyaoAttachmentOldService::getIdentify();
        } else {
            $identity = YouyaoAttachmentService::getIdentify();
        }
        $group = OaSignGroupRepository::getGroupById($groupId);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }
        if ($group->state == OaSignGroup::STATE_NEW) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前合同组状态不允许添加其它合同");
        }

        //校验模板
        $template = OaSignTemplateRepository::getByAlias($identity);
        if (!$template) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同主模板尚未配置，请联系管理员");
        }

        $user = UsersRepository::getUserByUid($group->user_id);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组关联用户不存在");
        }
        $company = $user->company;
        if (!$company) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组用户尚未认证");
        }

        $serveIds = [];
        foreach ($serves as $item) {
            $serveIds[] = $item['serve_id'];
        }

        $reps = RepsServeScopeSRepository::getByIds($serveIds);
        $serveMap = $reps->keyBy('id')->all();
        $hosYYIDS = $reps->pluck('hospital_yyid')->all();
        $hospitals = YouyaoHospitalRepository::getHospitalByYYIDs($hosYYIDS);
        $hospitalMap = $hospitals->keyBy('yyid')->all();

        //构建数据用来批量添加
        $repsServes = [];
        $hospitalTargets = [];
        $time = time();
        $now = Carbon::now();
        $factory = Uuid::getFactory();
        foreach ($serves as $item) {
            $serveId = $item['serve_id'];
            if (!isset($serveMap[$serveId])) {
                continue;
            }
            $hospitalYYID = $serveMap[$serveId]->hospital_yyid;
            if (!isset($hospitalMap[$hospitalYYID])) {
                continue;
            }
            $hospitalItem = $hospitalMap[$hospitalYYID];
            $YYID = bin2hex($factory->uuid1()->getBytes());
            $repsServes[] = [
                'yyid' => $YYID,
                'user_id' => $group->user_id,
                'company_id' => $company->id,
                'user_sign_id' => 0,
                'hospital_id' => $hospitalItem->id,
                'reps_serve_scope_s_id' => $serveId,
                'carryonnum' => $item['carryon_num'],
                'targetnum' => 0,
                'createtime' => $time,
                'updatetime' => $time,
            ];
            //处理指标医院
            foreach ($item['target'] as $target) {
                $hospitalTargets[] = [
                    'sign_hospital_yyid' => $YYID,
                    'month' => $target['month'],
                    'target' => $target['num'],
                    'status' => DataStatus::REGULAR,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }

        $no = strtoupper(uniqid());

        Db::beginTransaction();
        try {
            //添加合同
            $contract = OaUserSignRepository::addOne(
                $group,
                $template,
                $company,
                $title,
                $no,
                OaUserSign::TYPE_SLAVE
            );

            //添加合同服务记录
            /** @var RepsServeScopeS $item */
            foreach ($repsServes as $k => $v) {
                $repsServes[$k]['user_sign_id'] = $contract->id;
            }
            OaUserSignHospitalRepository::batchAdd($repsServes);

            //添加医院指标
            OaUserSignHospitalTargetRepository::batchAdd($hospitalTargets);

            //添加副表数据
            OaYouyaoAttachmentContractRepository::addOne($contract, $percent, $feeUnit, $settlementDate);
            Db::commit();
            return $contract;
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }

    public static function addUserAgree(int $groupId, $title, $isOld = false): OaUserSign
    {
        if ($isOld) {
            $identity = UserAgreeOldService::getIdentify();
        } else {
            $identity = UserAgreeService::getIdentify();
        }
        $group = OaSignGroupRepository::getGroupById($groupId);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }
        if ($group->state == OaSignGroup::STATE_NEW) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前合同组状态不允许添加其它合同");
        }

        //校验模板
        $template = OaSignTemplateRepository::getByAlias($identity);
        if (! $template) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同主模板尚未配置，请联系管理员");
        }

        $user = UsersRepository::getUserByUid($group->user_id);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组关联用户不存在");
        }
        $company = $user->company;
        if (!$company) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组用户尚未认证");
        }

        $no = strtoupper(uniqid());

        return OaUserSignRepository::addOne(
            $group,
            $template,
            $company,
            $title,
            $no,
            OaUserSign::TYPE_OTHER
        );
    }
}
