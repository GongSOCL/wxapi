<?php

namespace App\Service\Contract\Items;

class YouyaoMasterOldTplService extends YouyaoMasterTplService
{
    public static function getIdentify(): string
    {
        return "youyao-master-old";
    }

    public function getName(): string
    {
        return "优药主合同（旧）";
    }
}
