<?php

namespace App\Service\Contract\Items;

abstract class ItemAbstractService
{
    public function generateHtml()
    {
    }

    public function generatePdf()
    {
    }
}
