<?php
declare(strict_types=1);

namespace App\Service\Contract\Items;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\DrugSku;
use App\Model\Qa\OaUserSign;
use App\Model\Qa\OaUserSignHospital;
use App\Model\Qa\OaUserSignHospitalTarget;
use App\Model\Qa\YouyaoHospital;
use App\Repository\Contract\OaUserInviteRepository;
use App\Repository\Contract\OaUserSignHospitalRepository;
use App\Repository\Contract\OaUserSignHospitalTargetRepository;
use App\Repository\Contract\OaYouyaoAttachmentContractRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\YouyaoHospitalRepository;
use Grpc\Task\ContractGenerateMethod;

/**
 * 优药附件合同
 */
class YouyaoAttachmentService extends SignTplAbstractClass
{
    use SignTplTrait;

    /**
     * @inheritDoc
     */
    public static function getIdentify(): string
    {
        return "youyao-attachment";
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return "优药附件合同";
    }


    /**
     * @inheritDoc
     */
    public function validateTpl(): array
    {
        //获取附件合同相关信息
        $attachment = OaYouyaoAttachmentContractRepository::getContractAttachmentInfo($this->contract);
        if (!$attachment) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "附件合同配置信息不能为空");
        }

        //获取用户真实姓名和身份证号
        $userInfo = OaUserInviteRepository::getUserInfo($this->contract->user_id, $this->contract->company_id);
        if (!$userInfo) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户身份信息未录入");
        }

        //获取医院列表
        $reps = OaUserSignHospitalRepository::getSignRecords($this->contract);
        if ($reps->isEmpty()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同关联医院不能为空");
        }

        //获取医院关联批量
        $signTargets = OaUserSignHospitalTargetRepository::getByContract($this->contract);
        $targetMap = [];
        if ($signTargets->isNotEmpty()) {
            $targetMap = $signTargets->groupBy('sign_hospital_yyid')->all();
        }
        //获取医院
        $hospitalIds = $reps->pluck('hospital_id')->unique()->all();
        $hospitals = YouyaoHospitalRepository::getHospitalByIds($hospitalIds);
        if ($hospitals->isEmpty()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "youyao医院不能为空");
        }
        $hospitalMap = $hospitals->keyBy('id')->all();

        //获取产品列表
        $repsIds = $reps->pluck('reps_serve_scope_s_id')->unique()->all();
        $skus = RepsServeScopeSRepository::getDrugSkuByRepsIds($repsIds);
        if ($skus->isEmpty()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "产品列表不能为空");
        }
        $products = [];
        /** @var DrugSku $item */
        foreach ($skus as $item) {
            $products[] = [
                'name' => $item->name,
                'c_name' => $item->c_name,
                'specs' => $item->package ? sprintf("%s（%s）", $item->specs, $item->package) : $item->specs,
                'manufacturer' => $item->manufacturer
            ];
        }

        //获取达成率指标
        $hosLists = [];
        $months = [];
        /** @var OaUserSignHospital $item */
        foreach ($reps as $item) {
            if (!isset($hospitalMap[$item->hospital_id])) {
                continue;
            }
            /** @var YouyaoHospital $hospitalItem */
            $hospitalItem = $hospitalMap[$item->hospital_id];
            $subItem = [
                'id' => $hospitalItem->id,
                'name' => $hospitalItem->hospital_name,
                'carryonnum' => $item->carryonnum,
                'targetnum' => 0,
                'target' => [],
            ];
            //处理计划指标
            $itemYYID = $item->yyid;
            if (isset($targetMap[$itemYYID])) {
                /** @var OaUserSignHospitalTarget $targetItem */
                foreach ($targetMap[$itemYYID] as $targetItem) {
                    $month = $targetItem->month;
                    $m = (\DateTime::createFromFormat('Y-m', $month))->format('Y年n月');
                    $subItem['target'][$m] = (int) $targetItem->target;
                    $months[] = $m;
                }
            }
            $hosLists[] = $subItem;
        }

        return [
            'contract_no' => $this->contract->contractno,
            'username' => $userInfo->truename,
            'id_card' => $userInfo->id_card,
            'skus' => $products,
            'hospitals' => $hosLists,
            'start' => (string)$attachment->startdate,
            'end' => (string)$attachment->enddate,
            'percent' => $attachment->percent,
            'months' => array_unique($months),
            'settlement_date' => (string) $attachment->settlement_date,
            'fee_unit' => $attachment->fee_unit
        ];
    }

    /**
     * @inheritDoc
     */
    public function validatePdf(): array
    {
        return [
            'sign_img' => $this->signLink
        ];
    }

    /**
     * @inheritDoc
     */
    public function notifyHtmlGenerated($id, $link)
    {
        // TODO: Implement notifyHtmlGenerated() method.
    }

    /**
     * @inheritDoc
     */
    public function notifyPdfGenerated($id, $link)
    {
        // TODO: Implement notifyPdfGenerated() method.
    }

    /**
     * @inheritDoc
     */
    public function getHtmlGenerateMethod(): int
    {
        return ContractGenerateMethod::METHOD_TWIG_TPL;
    }

    /**
     * @inheritDoc
     */
    public function getPdfGenerateMethod(): int
    {
        return ContractGenerateMethod::METHOD_TWIG_HTML;
    }
}
