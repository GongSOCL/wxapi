<?php
declare(strict_types=1);
namespace App\Service\Contract\Items;

class UserAgreeOldService extends UserAgreeService
{
    public static function getIdentify(): string
    {
        return "user-agree-old";
    }

    public function getName(): string
    {
        return "知情同意书（旧）";
    }
}
