<?php
declare(strict_types=1);

namespace App\Service\Contract\Items;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\OaUserSign;
use App\Repository\CompanyRepository;
use App\Repository\Contract\OaUserInviteRepository;
use Grpc\Task\ContractGenerateMethod;

class UserAgreeService extends SignTplAbstractClass
{

    use SignTplTrait;

    /**
     * @inheritDoc
     */
    public static function getIdentify(): string
    {
        return "user-agree";
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return "知情同意书";
    }

    /**
     * @inheritDoc
     */
    public function validateTpl(): array
    {
        // 获取用户信息
        $info = OaUserInviteRepository::getUserInfo($this->contract->user_id, $this->contract->company_id);
        if (!$info) {
            throw new \UnexpectedValueException("用户身份信息未录入");
        }
        if (!($info->truename && $info->id_card)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户身份信息未录入");
        }
        //获取合同编号
        if (!$this->contract->contractno) {
            throw new BusinessException(
                ErrorCode::BUSINESS_ERROR,
                sprintf("合同%d编号不能为空", $this->contract->id)
            );
        }

        //获取公司信息
        $company = null;
        if ($this->contract->company_id) {
            $company = CompanyRepository::getById($this->contract->company_id);
        }
        if (!$company) {
            throw new BusinessException(
                ErrorCode::BUSINESS_ERROR,
                sprintf("合同%d甲方公司不能为空", $this->contract->id)
            );
        }
        return [
            'username' => $info->truename,
            'id_card' => $info->id_card
        ];
    }

    /**
     * @inheritDoc
     */
    public function validatePdf(): array
    {
        return [
            'sign_img' => $this->signLink
        ];
    }

    /**
     * @inheritDoc
     */
    public function notifyHtmlGenerated($id, $link)
    {
        // TODO: Implement notifyHtmlGenerated() method.
    }

    /**
     * @inheritDoc
     */
    public function notifyPdfGenerated($id, $link)
    {
        // TODO: Implement notifyPdfGenerated() method.
    }

    /**
     * @inheritDoc
     */
    public function getHtmlGenerateMethod(): int
    {
        return ContractGenerateMethod::METHOD_TWIG_TPL;
    }

    /**
     * @inheritDoc
     */
    public function getPdfGenerateMethod(): int
    {
        return ContractGenerateMethod::METHOD_TWIG_HTML;
    }
}
