<?php
declare(strict_types=1);

namespace App\Service\Contract\Items;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Constants\Wechat;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\TaskManageHelper;
use App\Model\Qa\OaSignGroup;
use App\Model\Qa\OaUserSign;
use App\Repository\Contract\OaSignGroupRepository;
use App\Repository\Contract\OaSignImgRepository;
use App\Repository\Contract\OaSignTemplateRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Service\Msg\MsgRpcService;
use Grpc\InsideMsg\CreateOneMsgRequest;
use Grpc\Msg\MSG_BIZ_TYPE;
use Grpc\Msg\MSG_PRIORITY;
use Grpc\Task\ContractGeneratorTask;
use Grpc\Task\ContractNoticeTask;
use Hyperf\Nsq\Nsq;
use Hyperf\Utils\ApplicationContext;
use Youyao\Framework\Components\Msg\MessageRpcClient;

class ContractGroupService
{
    /**
     * 生成组内所有合同html模板
     * @param int $id
     * @return void
     */
    public static function generateHtmlTpl($id)
    {
        //查询合同组并校验状态
        $group = OaSignGroupRepository::getGroupById($id);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }
        if (!$group->isValidToGenerate()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前合同组状态不允许生成模板");
        }

        //遍历合同组下的所有合同
        $contracts = $group->contracts;
        if ($contracts->isEmpty()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组内没有可用合同");
        }

        $params = [];
        /** @var OaUserSign $contract */
        foreach ($contracts as $contract) {
            if (!$contract->isValidToGenerateHtmlTpl()) {
                continue;
            }

            //获取模板并根据模板别名获取实例生成html
            $template = OaSignTemplateRepository::getById($contract->template_id);
            if (!$template) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, sprintf("合同%d模板不存在", $contract->id));
            }
            $param = self::getContractByIdentity($template->tpl_alias, $contract)
                ->generateHtml();
            if ($param) {
                $params[] = $param;
            }
        }

        //逐个推送任务
        $nsq = make(Nsq::class);
        foreach ($params as $param) {
            TaskManageHelper::create(0, $param, "contract-generate")->push(0.0, $nsq);
        }
    }

    /**
     * @param OaSignGroup $group
     * @param string $link 签名文件链接
     * @return void
     */
    public static function generatePdfContract(OaSignGroup $group, $link)
    {
        $group->refresh();
        //查询合同组并校验状态
        if (!$group->isSigned()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前合同组状态不允许签署");
        }
        $contracts = $group->contracts;
        if ($contracts->isEmpty()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组内没有可用合同");
        }

        //向合同签名表添加或更新签名文件
        $params = [];
        //遍历合同组内合同
        /** @var OaUserSign $contract */
        foreach ($contracts as $contract) {
            if ($contract->isValidToSign()) {
                continue;
            }

            $template = OaSignTemplateRepository::getById($contract->template_id);
            if (!$template) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, sprintf("合同%d模板不存在", $contract->id));
            }

            //投递合同生成签名任务
            $param = self::getContractByIdentity($template->tpl_alias, $contract)->generatePdf($link);
            if ($param) {
                echo $param->serializeToJsonString() . PHP_EOL;
                $params[] = $param;
            }
        }

        $nsq = make(Nsq::class);
        foreach ($params as $param) {
            TaskManageHelper::create(0, $param, 'contract-generate')->push(0.0, $nsq);
        }
    }

    /**
     * 检查合同组内合同是否html生成完成，完成后更新合同组状态
     * @param int $id
     * @return void
     */
    public static function checkContractGroupHtmlOk($id)
    {
        //检查合同组内合同未生成完成数量
        $group = OaSignGroupRepository::getGroupById($id);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }

        //全部完成后更新合同组状态到已生成html模板
        if ($group->state == OaSignGroup::STATE_TO_SIGN) {
            return;
        }

        //发起回调通知模板已经生成完成
        $log = Helper::getLogger();
        $rest = OaSignGroupRepository::getRestToGenerateHtml($group);
        $log->info("html_rest", [
            'rest' => $rest,
            'id' => $group->id
        ]);
        if ($rest == 0) {
            $operateResult = OaSignGroupRepository::htmlGenerateFinished($group);
            $log->info("group_update", [
                'rest' => $rest,
                'id' => $group->id,
                'res' => $operateResult
            ]);
            if ($operateResult) {
                //站内信提醒
                $link = Helper::buildYouyaoUrl('/uniapp/pages/contract/index', ['tabIndex' => 0]);
                self::noticeSignWithInsideMsg(
                    $group,
                    '待签署合同',
                    '您好，你有一份新的服务合同待签约，请尽快签署',
                    $link
                );
                //微信提醒
                self::noticeSignWithWechatMsg($group, $link);
            }
        }
    }

    private static function noticePdfGenerated(OaSignGroup $group, $link = "")
    {
        //查询用户
        $user = UsersRepository::getUserByUid($group->user_id);
        if (!$user) {
            return;
        }
        if (! $user->company) {
            return;
        }

        $wechat = WechatUserRepository::getOneWechatInfoByUserYYID($user->yyid, DataStatus::WECHAT_AGENT);
        if (!($wechat && $wechat->subscribe)) {
            return;
        }
        $log = Helper::getLogger();
        try {
            $res = ApplicationContext::getContainer()
                ->get(MessageRpcClient::class)
                ->sendWechatTemplateMsg(
                    [$wechat->openid],
                    Wechat::WECHAT_CHANNEL_WXAPI,
                    Wechat::CONTRACT_GENERATED,
                    MSG_BIZ_TYPE::BIZ_COMMON_TYPE,
                    $link,
                    [
                        'first' => '您好，您与我司的服务合同已经签署成功',
                        'keyword1' => (string)$user->company->name,
                        'keyword2' => $user->name ? $user->name : (
                        $wechat->nickname ? $wechat->nickname : ''
                        ),
                        'keyword3' => (new \DateTime((string)$group->updatetime))->format('Y年m月d日 H:i:s'),
                        'remark' => sprintf("合同-%s已签署完成功，点击此处，即可查看", $group->title)
                    ],
                    MSG_PRIORITY::PRIORITY_MIDDLE
                );
            if ($res->getCode() != 0) {
                throw new BusinessException(intval($res->getCode()), $res->getMsg());
            }
            $log->debug("send_contract_notice_ok", [
                'code' => $res->getCode(),
                'msg' => $res->getMsg(),
                'id' => $res->getMsgId()
            ]);
        } catch (\Exception $e) {
            $log->error("notice_contract_pdf_wechat_error", [
                'id' => $group->id,
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]);
        }
    }

    private static function noticeSignWithWechatMsg(OaSignGroup $group, $link = "")
    {
        //查询用户
        $user = UsersRepository::getUserByUid($group->user_id);
        if (!$user) {
            return;
        }
        if (! $user->company) {
            return;
        }

        $wechat = WechatUserRepository::getOneWechatInfoByUserYYID($user->yyid, DataStatus::WECHAT_AGENT);
        if (!($wechat && $wechat->subscribe)) {
            return;
        }
        try {
            $res = ApplicationContext::getContainer()
                ->get(MessageRpcClient::class)
                ->sendWechatTemplateMsg(
                    [$wechat->openid],
                    Wechat::WECHAT_CHANNEL_WXAPI,
                    Wechat::CONTRACT_SIGN_NOTICE,
                    MSG_BIZ_TYPE::BIZ_COMMON_TYPE,
                    $link,
                    [
                        'first' => '您好，您有一份新的服务合同待签约，请尽快签署',
                        'keyword1' => $user->company->name,
                        'keyword2' => $group->title,
                        'keyword3' => \DateTime::createFromFormat('Y-m-d', $group->end)->format('Y年m月d日'),
                        'remark' => '点击此处，即可进行签署'
                    ],
                    MSG_PRIORITY::PRIORITY_MIDDLE
                );
            if ($res->getCode() != 0) {
                throw new BusinessException(intval($res->getCode()), $res->getMsg());
            }
        } catch (\Exception $e) {
            $log = Helper::getLogger();
            $log->error("send_contract_notice_wechat_error", [
                'msg'=>$e->getMessage(),
                'trace'=>$e->getTrace()
            ]);
        }
    }

    private static function noticeSignWithInsideMsg(OaSignGroup $group, $title, $subTile, $link = "", $type = 16)
    {
        try {
            $req = new CreateOneMsgRequest();
            $req->setBody($subTile);
            $req->setToUserId($group->user_id);
            $req->setToUserType(DataStatus::USER_TYPE_AGENT);
            $req->setTitle($title);
            $req->setSubTitle($subTile);
            $req->setType($type);
            $req->setLink($link);
            (new MsgRpcService())->createOneMsg($req);
        } catch (\Exception $e) {
            $log = Helper::getLogger();
            $log->error("send_contract_notice_inside_erorr", [
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]);
        }
    }

    /**
     * 检查合同组内合同是否均已经签名完成， 签名完成后更新合同组状态及后续任务
     * @return void
     */
    public static function checkContractGroupPdfOk($id)
    {
        //检查合同组内合同未生成完成数量
        $group = OaSignGroupRepository::getGroupById($id);
        if (!$group) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组不存在");
        }

        //全部完成后更新合同组状态到已生成html模板
        if ($group->state == OaSignGroup::STATE_PDF_GENERATED) {
            return;
        }

        //发起回调通知模板已经生成完成
        $rest = OaSignGroupRepository::getRestToGeneratePdf($group);
        $log = Helper::getLogger();
        $log->debug("pdf_cb_rest", [
            'group_id' => $group->id,
            'rest' => $rest
        ]);
        if ($rest == 0) {
            //添加站内信
            $operateResult = OaSignGroupRepository::pdfGenerateFinished($group);
            $log->debug("pdf_cb_update_group", [
                'gid' => $group->id,
                'result' => $operateResult
            ]);
            if (!$operateResult) {
                return;
            }

            //发送站内信和微信模板消息通知
            $link = Helper::buildYouyaoUrl('/uniapp/pages/contract/index', ['tabIndex' => 1]);
            self::noticeSignWithInsideMsg(
                $group,
                '合同签署完成',
                '您好，您的服务合同已经签署完成',
                $link
            );

            //添加模板模板消息
            self::noticePdfGenerated($group, $link);
        }
    }

    /**
     * 根据合同模板别名获取处理程序
     * @param string $identity
     * @param OaUserSign $contract
     * @return SignTplAbstractClass
     */
    public static function getContractByIdentity(string $identity, OaUserSign $contract): SignTplAbstractClass
    {
        switch ($identity) {
            case YouyaoMasterTplService::getIdentify():
                return YouyaoMasterTplService::getInstance($contract);
            case YouyaoAttachmentService::getIdentify():
                return YouyaoAttachmentService::getInstance($contract);
            case UserAgreeService::getIdentify():
                return UserAgreeService::getInstance($contract);
            case YouyaoMasterOldTplService::getIdentify():
                return YouyaoMasterOldTplService::getInstance($contract);
            case YouyaoAttachmentOldService::getIdentify():
                return YouyaoAttachmentOldService::getInstance($contract);
            case UserAgreeOldService::getIdentify():
                return UserAgreeOldService::getInstance($contract);
            default:
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, sprintf("合同%s未关联处理模板", $identity));
        }
    }
}
