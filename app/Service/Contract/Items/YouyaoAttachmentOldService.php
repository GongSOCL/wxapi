<?php
declare(strict_types=1);
namespace App\Service\Contract\Items;

class YouyaoAttachmentOldService extends YouyaoAttachmentService
{
    public static function getIdentify(): string
    {
        return "youyao-attachment-old";
    }
    public function getName(): string
    {
        return "优药附件合同（旧）";
    }
}
