<?php

namespace App\Service\Contract\Items;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\OaUserSign;
use App\Repository\CompanyRepository;
use App\Repository\Contract\OaSignGroupRepository;
use App\Repository\Contract\OaUserInviteRepository;
use Grpc\Task\ContractGenerateMethod;

/**
 * 优药主合同模板
 */
class YouyaoMasterTplService extends SignTplAbstractClass
{
    use SignTplTrait;

    public static function getIdentify(): string
    {
        return "youyao-master";
    }


    public function notifyHtmlGenerated($id, $link)
    {
        $this->defaultNotifyHtmlGenerated($id, $link);
    }

    public function notifyPdfGenerated($id, $link)
    {
        $this->defaultNotifyPdfGenerated($id, $link);
    }


    public function getName(): string
    {
        return "优药主合同";
    }

    public function validateTpl(): array
    {
        // 获取用户信息
        $info = OaUserInviteRepository::getUserInfo($this->contract->user_id, $this->contract->company_id);
        if (!$info) {
            throw new \UnexpectedValueException("用户身份信息未录入");
        }
        //获取合同编号
        if (!$this->contract->contractno) {
            throw new BusinessException(
                ErrorCode::BUSINESS_ERROR,
                sprintf("合同%d编号不能为空", $this->contract->id)
            );
        }

        //获取公司信息
        $company = null;
        if ($this->contract->company_id) {
            $company = CompanyRepository::getById($this->contract->company_id);
        }
        if (!$company) {
            throw new BusinessException(
                ErrorCode::BUSINESS_ERROR,
                sprintf("合同%d甲方公司不能为空", $this->contract->id)
            );
        }


        // 获取生成时间
        return [
            'contract_no' => $this->contract->contractno,
            'username' => $info->truename,
            'id_card' => $info->id_card
        ];
    }

    public function validatePdf(): array
    {
        return [
            'sign_img' => $this->signLink
        ];
    }

    public function getHtmlGenerateMethod(): int
    {
        return ContractGenerateMethod::METHOD_TWIG_TPL;
    }

    public function getPdfGenerateMethod(): int
    {
        return ContractGenerateMethod::METHOD_TWIG_HTML;
    }
}
