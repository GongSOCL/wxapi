<?php
declare(strict_types=1);
namespace App\Service\Contract\Items;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\OaUserSign;
use App\Repository\Contract\OaSignImgRepository;
use Grpc\Task\ContractGenerateMethod;
use Grpc\Task\ContractGenerateType;
use Grpc\Task\ContractGeneratorTask;

trait SignTplTrait
{
    public function defaultNotifyHtmlGenerated($id, $link)
    {
        //查询合同记录

        //判断状态

        //更新记录模板链接及状态

        //调用合同组服务检查组状态
    }

    public function defaultNotifyPdfGenerated($id, $link)
    {
        //查询合同记录

        //判断状态

        //更新记录签名pdf链接及状态

        //调用合同组服务检查组状态
    }

    public static function downloadGetContent($url): string
    {
        //下载模板返回模板内容
        return "";
    }


    public static function validateAndGetSignLinkAndTpl(OaUserSign $contract): array
    {
        if (!$contract->link) {
            throw new \UnexpectedValueException(sprintf("合同%d模板链接不存在", $contract->id));
        }

        $signImg = OaSignImgRepository::getGroupLink($contract->group_id);
        if (!$signImg) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, sprintf("合同%d签名图片不存在", $contract->id));
        }

        return [$contract->link, $signImg->img_link];
    }
}
