<?php
declare(strict_types=1);

namespace App\Service\Contract\Items;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\OaUserSign;
use App\Repository\Contract\OaSignTemplateRepository;
use Grpc\Task\ContractGenerateMethod;
use Grpc\Task\ContractGenerateType;
use Grpc\Task\ContractGeneratorTask;

abstract class SignTplAbstractClass
{

    /**
     * @var OaUserSign
     */
    protected $contract;

    /**
     * @var string 模板链接
     */
    protected $tplLink = "";

    protected $signTime = "";

    protected $signLink = "";

    private function __construct()
    {
    }

    public static function getInstance(OaUserSign $contract): SignTplAbstractClass
    {
        $o = new static();
        $o->contract = $contract;
        return $o;
    }

    /**
     * 获取模板标识
     * @return string
     */
    abstract public static function getIdentify(): string;

    /**
     * 获取合同名称
     * @return string
     */
    abstract public function getName(): string;

    /**
     * 获取html模板文件链接
     * @return string
     */
    public function getTplLink(): string
    {
        //获取合同模板
        $template = OaSignTemplateRepository::getById($this->contract->template_id);
        if (!$template) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同模板不能为空");
        }
        if (!$template->link) {
            throw new BusinessException(
                ErrorCode::BUSINESS_ERROR,
                sprintf("合同%d原始模板文件链接为空", $this->contract->id)
            );
        }
        return (string)$template->link;
    }

    /**
     * 获取pdf模板文件链接
     * @return string
     */
    public function getPdfLink(): string
    {
        return (string)$this->contract->link;
    }

    public function setSignTime()
    {
        $this->signTime = date('Y-m-d H:i:s');
    }


    /**
     *
     * @return ContractGeneratorTask|null
     */
    public function generateHtml(): ?ContractGeneratorTask
    {
        if (!$this->contract->isValidToGenerateHtmlTpl()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前合同状态不允许生成html");
        }

        $data = $this->validateTpl();
        $this->tplLink = $this->getTplLink();
        if ($this->tplLink == "") {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同模板连接不能为空");
        }
        return self::createParams(
            $this->contract,
            static::getIdentify(),
            $data,
            $this->tplLink,
            ContractGenerateType::TYPE_HTML,
            $this->signTime,
            $this->getHtmlGenerateMethod()
        );
    }

    /**
     * 生成合同任务进行投递
     * @param string $link 签名图片链接
     * @return ContractGeneratorTask|null
     */
    public function generatePdf($link): ?ContractGeneratorTask
    {
        if ($this->contract->isPdfGenerated()) {
            return null;
        }
        if (!$this->contract->isSigned()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同状态不允许生成签署生成pdf");
        }
        $this->signLink = $link;

        //校验并设置参数
        $data = $this->validatePdf();
        $this->tplLink = $this->getPdfLink();
        if ($this->tplLink == "") {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "模板签名模板地址不能为空");
        }
        $this->setSignTime();
        ;

        return self::createParams(
            $this->contract,
            static::getIdentify(),
            $data,
            $this->tplLink,
            ContractGenerateType::TYPE_PDF,
            $this->signTime,
            $this->getPdfGenerateMethod()
        );
    }

    /**
     * 验证生成html模板参数
     * @return array
     */
    abstract public function validateTpl(): array;

    /**
     * 验证生成pdf模板参数
     * @return array
     */
    abstract public function validatePdf(): array;

    /**
     * @param int $id 模板id
     * @param string $link 生成html模板文件链接
     * @return mixed
     */
    abstract public function notifyHtmlGenerated($id, $link);

    /**
     * @param int $id 模板id
     * @param string $link 签名pdf链接
     * @return mixed
     */
    abstract public function notifyPdfGenerated($id, $link);

    /**
     * 获取html生成方式
     * @return int
     */
    abstract public function getHtmlGenerateMethod(): int;

    /**
     * 获取pdf模板生成方式
     * @return int
     */
    abstract public function getPdfGenerateMethod(): int;

    protected static function createParams(
        OaUserSign $contract,
        $identity,
        $param = [],
        $tplLink = "",
        $type = ContractGenerateType::TYPE_HTML,
        $signTime = "",
        $method = ContractGenerateMethod::METHOD_TWIG_HTML
    ): ContractGeneratorTask {
        $time = $signTime ? $signTime : date('Y-m-d H:i:s');

        $params = new ContractGeneratorTask();
        $params->setContractId($contract->id);
        $params->setGroupId($contract->group_id);
        $params->setCreateTime($time);
        $params->setTemplateAlias($identity);
        $params->setType($type);
        $params->setTplLink($tplLink);
        $params->setData(json_encode($param, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $params->setMethod($method);
        $params->setWatermark($contract->contractno);
        return $params;
    }
}
