<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Group\Doctor;

use App\Constants\Code\ErrorCodeGroup;
use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Exception\GroupException;
use App\Helper\Helper;
use App\Model\Qa\NewDoctorUserinfo;
use App\Model\Qa\NewGroup;
use App\Model\Qa\Users;
use App\Repository\ActivityRepository;
use App\Repository\AgentRepository;
use App\Repository\NewDoctorOpenidUserinfoRepository;
use App\Repository\NewGroup\NewAgentInviteRepository;
use App\Repository\NewGroup\NewDoctorUserinfoRepository;
use App\Repository\NewGroup\NewGroupMemberRepository;
use App\Repository\NewGroup\NewGroupRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\SystemDepartRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Service\User\UserService;
use App\Service\Wechat\WechatBusiness;
use Grpc\Common\CommonPageResponse;
use Grpc\DoctorInfo\DoctorWechatInfoItem;
use Grpc\Wxapi\DoctorGroup\AgentListRely;
use Grpc\Wxapi\DoctorGroup\DoctorAgentItem;
use Grpc\Wxapi\DoctorGroup\AgentDoctorsListReply;
use Grpc\Wxapi\DoctorGroup\GroupDoctorInfoReply;
use Grpc\Wxapi\DoctorGroup\QrCodeReply;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Collection;
use phpDocumentor\Reflection\Types\Null_;

class DoctorGroupService
{
    /**
     * @Inject()
     * @var WechatBusiness
     */
    protected $wechatBusiness;

    /**
     * @param $userId
     * @return array
     */
    public function getDoctorGroupInfo($userId)
    {
        $data = [];

        $data[] = array(
            'group_id'    => 'myDoctorGroup',
            'group_name'    => '我的医生',
            'group_num'     => NewAgentInviteRepository::getMyDoctorGroupCount($userId, DataStatus::WECHAT_DOCTOR)
        );
        $data[] = array(
            'group_id'    => 'subscribeGroup',
            'group_name'    => '已关注',
            'group_num'     => NewAgentInviteRepository::getSubscribeGroupCount($userId, DataStatus::WECHAT_DOCTOR)
        );
        $data[] = array(
            'group_id'    => 'unSubscribeGroup',
            'group_name'    => '未关注',
            'group_num'     => NewAgentInviteRepository::getUnsubscribeGroupCount($userId, DataStatus::WECHAT_DOCTOR)
        );

        return $data;
    }

    /**
     * @param $userId
     * @return array
     */
    public function getDoctorGroupList($userId)
    {
        //获取当前群列表
        $groups = NewGroupRepository::getMyGroupInfoByGroupName($userId, NewGroup::DOCTOR);
        if ($groups->isEmpty()) {
            return [];
        }
        $data = [];
        foreach ($groups as $group) {
            $data[] = [
                'group_id' => $group->id,
                'group_name' => $group->group_name,
                'group_num' => NewGroupMemberRepository::getGroupMembers($group->id)->count()
            ];
        }
        return $data;
    }

    /**
     * @param $userId
     * @param $groupName
     * @return array
     */
    public function createGroup($userId, $groupName)
    {
        //判断是否有同名群组
        $alreadyNamed = NewGroupRepository::sameGroupName($userId, $groupName, NewGroup::DOCTOR);
        if ($alreadyNamed) {
            throw new GroupException(ErrorCodeGroup::GROUP_ALREADY_EXISTS);
        }

        //创建群组
        $groupId = NewGroupRepository::createGroup($userId, $groupName, NewGroup::DOCTOR);
        if (!$groupId) {
            throw new GroupException(ErrorCodeGroup::FAIL_TO_CREATE_GROUP);
        }

        return [
            'id' => $groupId,
            'group_name' => $groupName
        ];
    }

    /**
     * @param $userId
     * @param $groupId
     * @return array
     * @throws \Exception
     */
    public function removeGroup($userId, $groupId)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        Db::beginTransaction();
        try {
            //删除t_group表
            NewGroupRepository::deleteGroup($userId, $groupId);
            //删除t_group_member表内医生
            NewGroupMemberRepository::deleteGroupMembers($groupId);
            Db::commit();
            return ['group_id' => $groupId];
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $userId
     * @param $groupId
     * @param $groupName
     * @return array
     */
    public function renameGroup($userId, $groupId, $groupName)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //判断是否有同名群组
        $alreadyNamed = NewGroupRepository::sameGroupName($userId, $groupName, NewGroup::DOCTOR, $groupId);
        if ($alreadyNamed) {
            throw new GroupException(ErrorCodeGroup::GROUP_ALREADY_EXISTS);
        }

        $update = NewGroupRepository::renameGroup($groupId, $groupName, NewGroup::DOCTOR);
        if (!$update) {
            throw new GroupException(ErrorCodeGroup::FAIL_TO_RENAME_GROUP);
        }
        return [
            'group_id' => $groupId,
            'group_name' => $groupName
        ];
    }

    /**
     * @param $groupId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function groupExists($groupId)
    {
        return NewGroupRepository::getGroupInfoById($groupId, NewGroup::DOCTOR);
    }

    public function getDoctorQrcodeUri(): string
    {
        $user = Helper::getLoginUser();
        return Helper::buildYoujiayiUrl("/pages/group/inviteQrcode", [
            'token' => Helper::encyptUserId($user->uid)
        ]);
    }

    public function getQrcodeFromToken($token): QrCodeReply
    {
        $uid = Helper::decryptUserId($token);

        //获取用户信息
        $user = UsersRepository::getUserByUid($uid);
        $name = "";
        if ($user->isVerified()) {
            $agent = AgentRepository::getAgentByUser($user);
            !is_null($agent) &&  $name= (string) $agent->truename;
        }
        if (!$name) {
            $wc = WechatUserRepository::getOneWechatInfoByUserYYID($user->yyid);
            if ($wc && $wc->nickname) {
                $name = $wc->nickname;
            }
        }
        if (!$name) {
            $name = (string) $user->name;
        }

        //生成分享二维码链接
        $activityData = ActivityRepository::getActivity(DataStatus::ACTIVITY_ID_QRCODE_DOCTOR_GROUP);
        if ($activityData) {
            $inviteData = $activityData->id.'#'.$activityData->event_key.'_'.$uid.'_';
        }

        $url = $this->wechatBusiness->getQrcode($inviteData, DataStatus::WECHAT_DOCTOR);
        $resp = new QrCodeReply();
        $resp->setUrl($url);
        $resp->setName($name);
        return $resp;
    }

    public function checkAgentOwnerDoctorInGroup($userId, $doctorId): bool
    {
        return NewDoctorOpenidUserinfoRepository::checkUserOwnDoctor($userId, $doctorId);
    }

    public function getServingAgentByDoctorRelatedHospital($doctorId): AgentListRely
    {
        $doctor=  NewDoctorUserinfoRepository::getDoctorById($doctorId);
        if (!$doctor) {
            throw new BusinessException(ErrorCode::DOCTOR_USER_NOT_EXISTS, "医生不存在或已注销");
        }

        $reply = new AgentListRely();
        $hospital = null;
        if ($doctor->hospital_yyid) {
            $hospital = YouyaoHospitalRepository::getHospitalByYYID($doctor->hospital_yyid);
        }
        if (!$hospital) {
            return $reply;
        }

        $reps = RepsServeScopeSRepository::getServeByHospital($hospital);
        if ($reps->isEmpty()) {
            return $reply;
        }
        $userYYIDS = $reps->pluck('user_yyid')->unique()->all();
        $users = UsersRepository::getUserByYYIDs($userYYIDS);
        if ($users->isEmpty()) {
            return $reply;
        }
        $list = self::getDoctorAgentList($users);
        $reply->setAgents($list);
        return $reply;
    }

    private static function getDoctorAgentList(Collection $users): array
    {
        $emptNameUser = [];
        /** @var Users $u */
        foreach ($users as $u) {
            if (!$u->name) {
                $emptNameUser[] = $u->yyid;
            }
        }
        $wmap = [];
        if (!$emptNameUser) {
            $wReps = WechatUserRepository::getMultiUsersWechat($emptNameUser);
            $wmap = $wReps->keyBy('u_yyid')->all();
        }

        $list = [];
        /** @var Users $user */
        foreach ($users as $user) {
            $o = new DoctorAgentItem();
            $o->setId($u->uid);
            $name = $user->name ? $user->name : (
            isset($wmap[$user->yyid]) && $wmap[$user->yyid]->nickname ? $wmap[$user->yyid]->nickname : ''
            );
            $o->setName($name);
            $list[] = $o;
        }
        return $list;
    }

    public function getAgentOwnGroupDoctor($doctorId): AgentListRely
    {
        $doctor=  NewDoctorUserinfoRepository::getDoctorById($doctorId);
        if (!$doctor) {
            throw new BusinessException(ErrorCode::DOCTOR_USER_NOT_EXISTS, "医生不存在或已注销");
        }

        $reply = new AgentListRely();
        $relations = NewDoctorOpenidUserinfoRepository::getRelatedToDoctor($doctor);
        if ($relations->isEmpty()) {
            return $reply;
        }
        $userIDS = $relations->pluck('user_id')->unique()->all();
        $users = UsersRepository::getUserByUids($userIDS);
        $list = self::getDoctorAgentList($users);
        $reply->setAgents($list);
        return $reply;
    }

    public function getAgentGroupDoctors($userId, $current, $limit, $showOnlyWithWechat = true): AgentDoctorsListReply
    {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '代表不存在');
        }
        $resp = NewDoctorOpenidUserinfoRepository::getAgentDoctorsWithPage(
            $user,
            $current,
            $limit,
            $showOnlyWithWechat
        );
        $page = new CommonPageResponse();
        $page->setSize($limit);
        $page->setTotal($resp->total());
        $page->setPages($resp->lastPage());
        $page->setCurrent($resp->currentPage());

        $reply = new AgentDoctorsListReply();
        $reply->setPage($page);
        if ($resp->isEmpty()) {
            return $reply;
        }

        $openIds = [];
        $hospitalYYIDS = [];
        /** @var NewDoctorUserinfo $item */
        foreach ($resp->items() as $item) {
            if ($item->hospital_yyid) {
                $hospitalYYIDS[] = $item->hospital_yyid;
            }
            if ($item->openid) {
                $openIds[] = $item->openid;
            }
        }
        $doctorMap = UserService::getDoctorsByOpenIds($openIds);

        //获取医院id
        $yyidResp = YouyaoHospitalRepository::getIdByYyids(array_unique($hospitalYYIDS));
        $yyidMap = $yyidResp->keyBy('yyid')->all();

        $list = [];
        /** @var NewDoctorUserinfo $item */
        foreach ($resp->items() as $item) {
            /** @var DoctorWechatInfoItem $doctorItem */
            $doctorItem = $doctorMap[$item->openid] ?? null;
            $name = $item->true_name ? $item->true_name : (
            $doctorItem && $doctorItem->getName() != "" ? $doctorItem->getName() : (
            $item->nickname ? $item->nickname : ''
            )
            );
            $headImg = $doctorItem && $doctorItem->getHeadimg() != '' ? $doctorItem->getHeadimg() : (
            $item->headimgurl ? $item->headimgurl : ''
            );
            $o = new GroupDoctorInfoReply();
            $o->setId($item->nid);
            $o->setName($name);
            $o->setUnionid($item->unionid);
            $o->setOpenid($item->openid);
            $o->setHeadimg($headImg);
            if ($item->hospital_yyid) {
                $o->setHospitalId($yyidMap[$item->hospital_yyid] ? $yyidMap[$item->hospital_yyid]->id : 0);
                $o->setHospitalName((string) $item->hospital_name);
            }
            if ($item->depart_id) {
                $o->setDepartId((int) $item->depart_id);
                $o->setDepartName((string) $item->depart_name);
            }
            $list[] = $o;
        }

        $reply->setList($list);

        return $reply;
    }

    /**
     * @param $nid
     * @return array
     */
    public function presetInvite($nid) {
        $user = Helper::getLoginUser();
        $activityData = ActivityRepository::getActivity(DataStatus::ACTIVITY_ID_QRCODE_DOCTOR_GROUP);
        if (!$activityData) {
            throw new BusinessException(ErrorCode::WRONG_ACTIVITY, "错误活动");
        }
        // 获取二维码记录
        $codeRecord = NewDoctorOpenidUserinfoRepository::getQrcodeRecode($activityData->id, $user->uid, $nid);
        if ($codeRecord) {
            $codeRecordId = $codeRecord->id;
        } else {
            $codeRecordId = NewDoctorOpenidUserinfoRepository::createQrcodeRecode($activityData->id, $user->uid, $nid, $activityData->event_key);
        }
        $inviteData = $activityData->id.'#'.$activityData->event_key.'_'.$user->uid.'_'.$nid.'_'.$codeRecordId;
        $url = $this->wechatBusiness->getQrcode($inviteData, DataStatus::WECHAT_DOCTOR);
        return ['url' => Helper::n_img_base_64($url)];
    }

    public function getGroupDoctorInfo($doctorId): GroupDoctorInfoReply
    {
        $doctor = NewDoctorOpenidUserinfoRepository::getInfoById($doctorId);
        if (!$doctor) {
            throw new BusinessException(ErrorCode::DOCTOR_USER_NOT_EXISTS);
        }

        $hospital = $depart = null;
        if ($doctor->hospital_yyid) {
            $hospital = YouyaoHospitalRepository::getHospitalByYYID($doctor->hospital_yyid);
        }
        if ($doctor->depart_id) {
            $depart = SystemDepartRepository::getById($doctor->depart_id);
        }

        $doctorMap = [];
        if ($doctor->openid) {
            $doctorMap = UserService::getDoctorsByOpenIds([$doctor->openid]);
        }

        $doctorItem = isset($doctorMap[$doctor->openid]) ? $doctorMap[$doctor->openid] : null;
        $headImg = $doctorItem && $doctorItem->getHeadimg() != '' ? $doctorItem->getHeadimg() : (
        $doctor->headimgurl ? $doctor->headimgurl : ''
        );

        $name = $doctor->true_name ? $doctor->true_name : (
        $doctorItem && $doctorItem->getName() != "" ? $doctorItem->getName() : (
            $doctor->nickname ? $doctor->nickname : ''
        )
        );

        $o = new GroupDoctorInfoReply();
        $o->setId($doctor->nid);
        $o->setName($name);
        $o->setUnionid($doctor->unionid);
        $o->setOpenid($doctor->openid);
        $o->setHeadimg($headImg);
        if ($hospital) {
            $o->setHospitalId($hospital->id);
            $o->setName($hospital->hospital_name);
        }
        if ($depart) {
            $o->setDepartId($depart->id);
            $o->setDepartName($depart->name);
        }

        return $o;
    }

    public function hospitals($userId, $yyid, $page, $pageSize)
    {
        // 获取服务的医院
        $myhospitals = NewGroupRepository::getMyServerHospitals($yyid, $page, $pageSize);
        $count = NewGroupRepository::getMyServerHospitalsCount($yyid)->count();
        // 获取医生数量
        $doctors = 0;
        $mydoctors = 0;
        if ($myhospitals->isNotEmpty()) {
            $doctors = NewGroupRepository::getMyDoctorsCount(array_column($myhospitals->toArray(), 'hospital_yyid'));
            $mydoctors = NewGroupRepository::getOcDoctorsCount($userId, array_column($myhospitals->toArray(), 'hospital_yyid'));
        }

        return [
            'hospital_count' => $myhospitals->count(),
            'doctor_count' => $doctors-$mydoctors,
            'hospital_list' => $myhospitals->isNotEmpty() ? $myhospitals->toArray() : [],
            'total' => $count,
            'page' => $page,
            'pageSize' => $pageSize,
        ];
    }

    public function doctors($uid, $yyid, $doctorName, $hospitalYyid, $page, $pageSize)
    {
        $where = [];
        $myhospitals = NewGroupRepository::getMyServerHospitalsWithoutPage($yyid);
        if ($myhospitals->isNotEmpty()) {
            $where[] = [
                function($q) use($myhospitals) {
                    $q->whereIn('hospital_yyid', array_column($myhospitals->toArray(), 'hospital_yyid'));
                }];
        }
        if ($hospitalYyid) {
            $where[] = ['hospital_yyid', '=', $hospitalYyid];
        }
        if ($doctorName) {
            $where[] = [
                function($q) use($doctorName) {
                    $q->where('true_name', 'like', '%' . $doctorName . '%')
                        ->orWhere('hospital_name', 'like', '%' . $doctorName . '%');
                }];
        }

        $list = NewDoctorUserinfoRepository::getMyDoctors($where, $page, $pageSize);
        if ($list->isNotEmpty()) {
            $list = $list->toArray();
            foreach ($list as $k=>$item) {
                // 判断该医生是否在我群组里
                $list[$k]['is_in'] = 0;
                $list[$k]['info_missing'] = 0;
                if (NewDoctorUserinfoRepository::getMyDoctorInGroup($uid, $item['nid'])) {
                    $list[$k]['is_in'] = 1;
                }
                if (!$item['true_name']||!$item['hospital_yyid']||!$item['depart_id']||$item['kol_level']===null) {
                    $list[$k]['info_missing'] = 1;
                }
            }
        }
        $count = NewDoctorUserinfoRepository::getMyDoctorsCount($where);

        return [
            'list' => $list,
            'total' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }
}
