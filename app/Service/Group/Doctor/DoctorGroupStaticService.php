<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Group\Doctor;

use App\Repository\NewGroup\NewGroupMemberRepository;
use App\Repository\NewGroup\NewGroupStaticRepository;
use Hyperf\DbConnection\Db;

class DoctorGroupStaticService
{
    /**
     * @param $userYyid
     * @param $userId
     * @return array
     */
    public function departments($userYyid, $userId)
    {
        $departs = [];
        // 获取代理药品
        $reps = NewGroupStaticRepository::getServerReps($userYyid);
        if ($reps->isNotEmpty()) {
            $seriesYyids = array_unique(array_column($reps->toArray(), 'series_yyid'));
            $products = NewGroupStaticRepository::getProducts($seriesYyids);
            if ($products->isNotEmpty()) {
                $departs = NewGroupStaticRepository::getStaticDeparts(array_column($products->toArray(), 'id'));
                if ($departs->isNotEmpty()) {
                    $departs = $departs->toArray();
                    foreach ($departs as $key=>$depart) {
                        $departs[$key]['count'] = NewGroupStaticRepository::getStaticMembersCount($userId, $depart['depart_id']);
                    }
                }
            }
        }

        return $departs;
    }

    /**
     * @param $userId
     * @param $departId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function members($userId, $departId, $page, $pageSize)
    {
        $memberList = NewGroupStaticRepository::getStaticMembersByUserId($userId, $departId, $page, $pageSize);
        $count = NewGroupStaticRepository::getStaticMembersByUserIdCount($userId, $departId);

        if ($memberList->isNotEmpty()) {
            $memberList = $memberList->toArray();
            foreach ($memberList as $k=>$value) {
                $memberList[$k]['group_count'] = NewGroupMemberRepository::getDoctorGroups($value['nid'], $userId)->count();
            }
        }

        return [
            'list' => $memberList,
            'total' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $userId
     * @return array|string[]
     * @throws \Exception
     */
    public function autoGroup($userId)
    {
        // 判断是否自动分过组
        $auto = NewGroupStaticRepository::isAutoGroup($userId);
        if (!$auto) {
            // 自动分组
            $doctors = NewGroupStaticRepository::getMyDoctors($userId);
            if ($doctors->isNotEmpty()) {
                Db::beginTransaction();
                try {
                    // 清除分组
                    NewGroupStaticRepository::deleteAutoGroup($userId);
                    foreach ($doctors->toArray() as $doctor) {
                        $depart = $doctor['depart_id'];
                        $staticDepart = NewGroupStaticRepository::getStaticDepartById($depart);
                        if ($staticDepart) {
                            NewGroupStaticRepository::createStaticDoctor(
                                $staticDepart->group_department_id,
                                $depart,
                                $userId,
                                $doctor['nid']
                            );
                        }
                    }
                    NewGroupStaticRepository::createAutoFlag($userId);
                    Db::commit();
                    return [];
                } catch (\Exception $exception) {
                    Db::rollBack();
                    throw $exception;
                }
            } else {
                return ['没有医生'];
            }
        } else {
            return ['已分组'];
        }
    }

    /**
     * @param $userId
     * @param $nId
     * @return array|\Hyperf\Utils\Collection
     */
    public function inGroup($userId, $nId)
    {
        $res = [];
        $groups = NewGroupStaticRepository::getInGroupList($userId, $nId);
        if ($groups->isNotEmpty()) {
            $res = NewGroupStaticRepository::getGroupName(array_column($groups->toArray(), 'group_id'));
        }
        return $res->isNotEmpty() ? $res->toArray() : [];
    }
}
