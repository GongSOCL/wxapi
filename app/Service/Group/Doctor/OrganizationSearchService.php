<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Service\Group\Doctor;

use App\Constants\DataStatus;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;

class OrganizationSearchService
{
    private $redis;

    private $departAgentKey;
    private $departSubscribeDoctorKey;
    private $departUnsubscribeDoctorKey;

    public function __construct($departId)
    {
        $container = ApplicationContext::getContainer();
        $this->redis = $container->get(RedisFactory::class)->get('share');
        $this->departAgentKey = 'org:depart_agent_list_'.$departId;
        $this->departSubscribeDoctorKey = 'org:depart_doctor_s_list_'.$departId;
        $this->departUnsubscribeDoctorKey = 'org:depart_doctor_n_list_'.$departId;
    }

    public function getAgentCount()
    {
        $redis = $this->redis;
        return $redis->zCard($this->departAgentKey);
    }

    public function getSubscribeDoctorCount()
    {
        $redis = $this->redis;
        return $redis->zCard($this->departSubscribeDoctorKey);
    }

    public function getUnsubscribeDoctorCount()
    {
        $redis = $this->redis;
        return $redis->zCard($this->departUnsubscribeDoctorKey);
    }

    public function getSubscribeDoctor()
    {
        $redis = $this->redis;
        return $redis->zRange($this->departSubscribeDoctorKey, 0, -1);
    }

    public function getUnsubscribeDoctor()
    {
        $redis = $this->redis;
        return $redis->zRange($this->departUnsubscribeDoctorKey, 0, -1);
    }
}
