<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Service\Group\Doctor;

use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;

class DoctorSelectRedisLogic
{
    private $redis;

    private $doctorSelectKey;

    public function __construct($userId)
    {
        $container = ApplicationContext::getContainer();
        $this->redis = $container->get(RedisFactory::class)->get('share');
        $this->doctorSelectKey = 'doctor-select:list_'.$userId;
    }

    public function storeKey($doctorId)
    {
        $redis = $this->redis;
        $redis->sAdd($this->doctorSelectKey, $doctorId);
    }

    public function getOneKey($doctorId)
    {
        $redis = $this->redis;
        return $redis->sIsMember($this->doctorSelectKey, $doctorId);
    }

    public function delOneKey($doctorId)
    {
        $redis = $this->redis;
        $redis->sRem($this->doctorSelectKey, $doctorId);
    }

    public function delAllKey()
    {
        $redis = $this->redis;
        $redis->del($this->doctorSelectKey);
    }

    public function getDoctors()
    {
        $redis = $this->redis;
        return $redis->sMembers($this->doctorSelectKey);
    }

    public function getCount()
    {
        $redis = $this->redis;
        return $redis->sCard($this->doctorSelectKey);
    }
}
