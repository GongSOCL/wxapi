<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Service\Group\Doctor;

use App\Constants\DataStatus;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;

class SearchService
{
    private $redis;

    private $userSearchShowKey;      //该用户的搜索历史合集

    public function __construct($userId)
    {
        $container = ApplicationContext::getContainer();
        $this->redis = $container->get(RedisFactory::class)->get('share');
        $this->userSearchShowKey = 'org:doctor_search_show_'.$userId;
    }

    /**
     * @param $keyWord
     */
    public function storeSearchInfo($keyWord)
    {
        $redis = $this->redis;
        //将该值加入集合
        $redis->zAdd($this->userSearchShowKey, [], time(), $keyWord);
        $redis->expire($this->userSearchShowKey, DataStatus::EXPIRE_TIME_WITHOUT_DOWN);
    }

    /**
     * @return array
     */
    public function getSearchHistory()
    {
        $redis = $this->redis;
        //最多取9个
        return $data = $redis->zRevRange($this->userSearchShowKey, 0, 8, false);
    }
}
