<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Group\Doctor;

use App\Constants\Code\ErrorCodeGroup;
use App\Constants\DataStatus;
use App\Exception\GroupException;
use App\Repository\NewGroup\OrganizationRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;

class OrganizationService
{
    private $myDepartId = [];

    /**
     * @param $userId
     * @return array[]
     */
    public function getNavigate($userId)
    {
        // 获取用户的部门 （有可能多部门）
        $orgInfo = OrganizationRepository::getOrgLevel($userId);
        if ($orgInfo->isEmpty()) {
            throw new GroupException(ErrorCodeGroup::USER_DONT_HAVE_ORGANIZATION);
        }
        $orgInfo = $orgInfo->toArray();
        if (count($orgInfo)==1) {
            // 单部门 直接获取当前所在部门的导航数组
            $navigate = $this->getNavigateList($orgInfo[0]['path']);
        } else {
            // 多部门 先获取用户等级最高的部门
            $path = $this->getTopPath($orgInfo);
            $navigate = $this->getNavigateList($path);
        }

        return $navigate;
    }

    /**
     * @param $departId
     * @param $userId
     * @return array
     */
    public function getTopDepartment($departId, $userId)
    {
        // 验证用户最高级部门是否一致
        $navigate = $this->getNavigate($userId);
        $mark = count($navigate);
        if ($departId!=array_pop($navigate)['department_id']) {
            throw new GroupException(ErrorCodeGroup::WRONG_USER_DEPARTMENT);
        }
        $orgInfo = OrganizationRepository::getOrgLevel($userId);
        if ($orgInfo->isEmpty()) {
            throw new GroupException(ErrorCodeGroup::USER_DONT_HAVE_ORGANIZATION);
        }
        $orgInfo = $orgInfo->toArray();
        $topDepart = [];
        foreach ($orgInfo as $value) {
            $dep = explode('-', $value['path']);
            if (count($dep)==1||count($dep)==$mark) {
                if (!in_array($value['dept_id'], $topDepart)) {
                    array_push($topDepart, $value['dept_id']);
                }
            } else {
                if (count($dep)>$mark) {
                    if (!in_array($dep[$mark], $topDepart)) {
                        array_push($topDepart, $dep[$mark]);
                    }
                }
            }
        }
        return $this->getDepsByIds($topDepart);
    }

    /**
     * @param $departId
     * @param $userId
     * @return array
     */
    public function getNextDepartment($departId, $userId)
    {
        // 获取该部门下所有子部门
        $subDepartments = OrganizationRepository::getSubDepartByParentsId($departId);
        if ($subDepartments->isEmpty()) {
            return [];
        }
        $subDepartIds = array_column($subDepartments->toArray(), 'dept_id');

        $dids = [];
        $myDepartIds = [];
        // 获取用户的部门 （有可能多部门）
        $orgInfo = OrganizationRepository::getOrgLevel($userId);
        // 我的所有部门和子部门
        if ($orgInfo->isNotEmpty()) {
            $myDepartIds = $this->getMySubDeparts($orgInfo->toArray());
        }

        foreach ($subDepartIds as $subDepartId) {
            if (in_array($subDepartId, $myDepartIds)&&
                !in_array($subDepartId, $dids)) {
                array_push($dids, $subDepartId);
            }
        }

        return $this->getDepsByIds($dids);
    }

    /**
     * @param $departId
     * @param $userId
     * @return array
     */
    public function getDepartmentAgent($departId, $userId)
    {
        $myDepartIds = [];
        // 获取用户的部门 （有可能多部门）
        $orgInfo = OrganizationRepository::getOrgLevel($userId);
        // 我的所有部门和子部门
        if ($orgInfo->isNotEmpty()) {
            $myDepartIds = $this->getMySubDeparts($orgInfo->toArray());
        }
        // 不在我的部门和子部门里的不展示
        if (!in_array($departId, $myDepartIds)) {
            return [];
        }
        // 获取该部门下的所有代表
        $agents = OrganizationRepository::getDepartmentAgent($departId);
        if ($agents->isEmpty()) {
            return [];
        }
        $agents = $agents->toArray();
        foreach ($agents as $key => $value) {
            // 获取头像
            $wechat = WechatUserRepository::getOneWechatInfoByUserYYID($value['yyid'], DataStatus::WECHAT_AGENT);
            $agents[$key]['headimgurl'] = $wechat ? $wechat->headimgurl : '';
            // 获取该代表下医生数量
            $agents[$key]['subscribe_count'] = OrganizationRepository::getSubscribeDoctorInAgent($value['uid']);
            $agents[$key]['unsubscribe_count'] = OrganizationRepository::getUnsubscribeDoctorInAgent($value['uid']);
        }
        return $agents;
    }

    /**
     * @param $uid
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getDoctorByAgent($uid, $page, $pageSize)
    {
        $list = OrganizationRepository::getDoctorInAgent($uid, $page, $pageSize);
        $count = OrganizationRepository::getDoctorCountInAgent($uid);

        return [
            'list' => $list->isNotEmpty() ? $list->toArray() : [],
            'total' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $userId
     * @param $keyword
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function searchDoctor($userId, $keyword, $page, $pageSize)
    {
        $doctorIds = [];
        $redis = new SearchService($userId);
        $redis->storeSearchInfo($keyword);
        // 获取用户的部门
        $orgs = OrganizationRepository::getOrgLevel($userId);
        if ($orgs->isNotEmpty()) {
            $orgs = $orgs->toArray();
            foreach ($orgs as $org) {
                $redis = new OrganizationSearchService($org['dept_id']);
                $subDoctorIds = $redis->getSubscribeDoctor();
                if (!empty($subDoctorIds)) {
                    foreach ($subDoctorIds as $subDoctorId) {
                        if (!in_array($subDoctorId, $doctorIds)) {
                            array_push($doctorIds, $subDoctorId);
                        }
                    }
                }
                $unsubDoctorIds = $redis->getUnsubscribeDoctor();
                if (!empty($unsubDoctorIds)) {
                    foreach ($unsubDoctorIds as $unsubDoctorId) {
                        if (!in_array($unsubDoctorId, $doctorIds)) {
                            array_push($doctorIds, $unsubDoctorId);
                        }
                    }
                }
            }
        }

        $list = OrganizationRepository::searchDoctorInAgent($keyword, $doctorIds, $page, $pageSize);
        $count = OrganizationRepository::searchDoctorCountInAgent($keyword, $doctorIds);

        return [
            'list' => $list->isNotEmpty() ? $list->toArray() : [],
            'total' => $count->count(),
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $userId
     * @return array
     */
    public function searchRecord($userId)
    {
        $redis = new SearchService($userId);
        return $redis->getSearchHistory();
    }

    /**
     * @param $depIds
     * @return array
     */
    public function getDepsByIds($depIds)
    {
        $data = OrganizationRepository::getDepsByIds($depIds);
        if ($data->isEmpty()) {
            return [];
        }
        $data = $data->toArray();
        foreach ($data as $key => $value) {
            $redis = new OrganizationSearchService($value['department_id']);
            $data[$key]['agent_count'] = $redis->getAgentCount();
            $data[$key]['subscribe_count'] = $redis->getSubscribeDoctorCount();
            $data[$key]['unsubscribe_count'] = $redis->getUnsubscribeDoctorCount();
        }

        return $data;
    }

    /**
     * @param $orgInfo
     * @return string
     */
    public function getTopPath($orgInfo)
    {
        $path = [];
        foreach ($orgInfo as $value) {
            array_push($path, explode('-', $value['path']));
        }
        $p = $this->myIntersect($path);
        return implode('-', $p);
    }

    /**
     * @param $path
     * @param int $i
     * @param array $cp
     * @return array|mixed
     */
    public function myIntersect($path, &$i = 0, &$cp = [])
    {
        // 递归获取部门路径二维数组中最高部门
        if ($i==0) {
            $cp = $path[$i];
        } else {
            $cp = array_intersect($cp, $path[$i]);
        }
        $i++;
        if ($i<count($path)) {
            $this->myIntersect($path, $i, $cp);
        }
        return $cp;
    }

    /**
     * @param $path
     * @return array[]
     */
    public function getNavigateList($path)
    {
        $navigate = [
            [
                'department_id' => 0,
                'department_name' => '优锐医药',
                'level' => 0,
            ]
        ];
        $deps = explode('-', $path);
        if (count($deps)>1) {
            foreach ($deps as $dk => $dv) {
                if ($dk!=0) {
                    $orgPart = OrganizationRepository::getDepById($dv);
                    array_push($navigate, [
                        'department_id' => $orgPart->id,
                        'department_name' => $orgPart->name,
                        'level' => $orgPart->level
                    ]);
                }
            }
        }
        return $navigate;
    }

    public function getMySubDeparts($departs)
    {
        // 获取该部门下所有子部门
        foreach ($departs as $item) {
            if (!in_array($item['dept_id'], $this->myDepartId)) {
                array_push($this->myDepartId, $item['dept_id']);
            }
            $subDepartments = OrganizationRepository::getSubDepartByParentsId($item['dept_id']);
            if ($subDepartments->isNotEmpty()) {
                $this->getMySubDeparts($subDepartments->toArray());
            }
        }

        return $this->myDepartId;
    }


    public function levelReset()
    {
        $data = OrganizationRepository::getAllDepartment();
        $data = $data->toArray();
        foreach ($data as $value) {
            $level = count(explode('-', $value['path']));
            OrganizationRepository::resetLevel($value['id'], $level);
        }
    }
}
