<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Group\Doctor;

use App\Constants\Code\ErrorCodeGroup;
use App\Constants\DataStatus;
use App\Exception\GroupException;
use App\Helper\Helper;
use App\Helper\TaskManageHelper;
use App\Model\Qa\NewDoctorUserinfo;
use App\Model\Qa\NewGroup;
use App\Model\Qa\NewGroupMember;
use App\Repository\AdaptionRepository;
use App\Repository\DoctorOpenidUserinfoRepository;
use App\Repository\HospitalRepository;
use App\Repository\NewDoctorOpenidUserinfoRepository;
use App\Repository\NewGroup\NewAgentInviteRepository;
use App\Repository\NewGroup\NewDoctorUserinfoRepository;
use App\Repository\NewGroup\NewGroupMemberRepository;
use App\Repository\NewGroup\NewGroupRepository;
use App\Repository\NewGroup\NewGroupStaticRepository;
use App\Repository\SystemDepartRepository;
use App\Service\Wechat\WechatRpcService;
use Grpc\Task\GroupDoctorDepartChangeEvent;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

class DoctorGroupMemberService
{
    /**
     * @Inject()
     * @var WechatRpcService
     */
    protected $wechatRpcService;

    /**
     * @param $userId
     * @param $nIds
     * @param $groupId
     * @return mixed
     */
    public function joinAdd($userId, $nIds, $groupId)
    {
        $missInfo = [];
        // 判断医生数据是否完整
        foreach ($nIds as $oneId) {
            /** @var NewDoctorUserinfo $oneDoctor */
            $oneDoctor = NewDoctorUserinfoRepository::getShareDoctorInfo($oneId);
            if (!$oneDoctor->true_name||!$oneDoctor->hospital_yyid||!$oneDoctor->depart_id||$oneDoctor->kol_level===null) {
                array_push($missInfo, [
                    'nid' => $oneDoctor->nid,
                    'true_name' => $oneDoctor->true_name,
                ]);
            }
        }

        if(count($missInfo)!=0) {
            return [
                'has_missing_info' => 1,
                'missing_list' => $missInfo
            ];
        }

        foreach ($nIds as $nid) {
            // 建立代表和医生关系
            $repsExist = DoctorOpenidUserinfoRepository::getDoctorReps($userId, $nid);
            if (!$repsExist) {
                DoctorOpenidUserinfoRepository::addDoctorReps($userId, $nid);
            }
            // 根据部门自动进固定组
            $departId = NewDoctorUserinfoRepository::getShareDoctorInfo($nid)->depart_id;
            $staticDepart = NewGroupStaticRepository::getStaticDepartById($departId);
            if ($staticDepart) {
                $autoReps = NewGroupStaticRepository::getAutoReps(
                    $staticDepart->group_department_id,
                    $departId,
                    $userId,
                    $nid
                );
                if (!$autoReps) {
                    NewGroupStaticRepository::createStaticDoctor(
                        $staticDepart->group_department_id,
                        $departId,
                        $userId,
                        $nid
                    );
                }
            }
        }

        if ($groupId) {
            foreach ($nIds as $id) {
                $memberExist = NewGroupMemberRepository::getRepeatMember($groupId, $id, $userId);
                if (!$memberExist) {
                    NewGroupMemberRepository::addMember(
                        $id,
                        $groupId,
                        $userId,
                        NewGroupMember::DOCTOR);
                }
            }
        }
        return [
            'has_missing_info' => 0,
            'missing_list' => []
        ];
    }

    /**
     * @param $keyword
     * @param $userId
     * @param $groupId
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function members($keyword, $userId, $groupId, $page, $pageSize)
    {
        if ($keyword) {
            $where[] = [
                function($q) use($keyword) {
                    $q->where('true_name', 'like', '%' . $keyword . '%')
                        ->orWhere('hospital_name', 'like', '%' . $keyword . '%');
                }];
            if ($groupId==''||$groupId=='myDoctorGroup') {
                $memberList = NewAgentInviteRepository::searchDoctorGroup($userId, $where, DataStatus::WECHAT_DOCTOR, $page, $pageSize);
                $memberCount = NewAgentInviteRepository::searchDoctorGroupCount($userId, $where, DataStatus::WECHAT_DOCTOR);
            } elseif ($groupId=='unSubscribeGroup') {
                $memberList = NewAgentInviteRepository::searchUnsubscribeGroup($userId, $where, DataStatus::WECHAT_DOCTOR, $page, $pageSize);
                $memberCount = NewAgentInviteRepository::searchUnsubscribeGroupCount($userId, $where, DataStatus::WECHAT_DOCTOR);
            } elseif ($groupId=='subscribeGroup') {
                $memberList = NewAgentInviteRepository::searchSubscribeGroup($userId, $where, DataStatus::WECHAT_DOCTOR, $page, $pageSize);
                $memberCount = NewAgentInviteRepository::searchSubscribeGroupCount($userId, $where, DataStatus::WECHAT_DOCTOR);
            } else {
                $memberList = NewGroupMemberRepository::searchGroupDoctorsListByGroupId(
                    $userId,
                    $where,
                    $groupId,
                    NewGroupMember::DOCTOR,
                    DataStatus::WECHAT_DOCTOR,
                    $page,
                    $pageSize);
                $memberCount = NewGroupMemberRepository::searchGroupDoctorsListByGroupIdCount(
                    $userId,
                    $where,
                    $groupId,
                    NewGroupMember::DOCTOR,
                    DataStatus::WECHAT_DOCTOR,
                );
            }
        } else {
            if ($groupId==''||$groupId=='myDoctorGroup') {
                $memberList = NewAgentInviteRepository::getMyDoctorGroup(
                    $userId,
                    DataStatus::WECHAT_DOCTOR,
                    $page,
                    $pageSize
                );
                $memberCount = NewAgentInviteRepository::getMyDoctorGroupCount($userId, DataStatus::WECHAT_DOCTOR);
            } elseif ($groupId=='unSubscribeGroup') {
                $memberList = NewAgentInviteRepository::getUnsubscribeGroup(
                    $userId,
                    DataStatus::WECHAT_DOCTOR,
                    $page,
                    $pageSize
                );
                $memberCount = NewAgentInviteRepository::getUnsubscribeGroupCount($userId, DataStatus::WECHAT_DOCTOR);
            } elseif ($groupId=='subscribeGroup') {
                $memberList = NewAgentInviteRepository::getSubscribeGroup(
                    $userId,
                    DataStatus::WECHAT_DOCTOR,
                    $page,
                    $pageSize
                );
                $memberCount = NewAgentInviteRepository::getSubscribeGroupCount($userId, DataStatus::WECHAT_DOCTOR);
            } else {
                //获取该群组成员列表
                $memberList = NewGroupMemberRepository::getGroupDoctorsListByGroupId(
                    $userId,
                    $groupId,
                    NewGroupMember::DOCTOR,
                    DataStatus::WECHAT_DOCTOR,
                    $page,
                    $pageSize
                );
                $memberCount = NewGroupMemberRepository::getGroupDoctorsListByGroupIdCount(
                    $userId,
                    $groupId,
                    NewGroupMember::DOCTOR,
                    DataStatus::WECHAT_DOCTOR,
                );
            }
        }

        if ($memberList->isNotEmpty()) {
            foreach ($memberList->toArray() as $k => $value) {
                $memberList[$k]['is_subscribe'] = 0;
                $memberList[$k]['group_count'] = NewGroupMemberRepository::getDoctorGroups($value['nid'], $userId)
                    ->count();
                if ($value['openid']) {
                    $dWechatInfo = NewGroupMemberRepository::getWechatUserByOpenid($value['openid']);
                    if ($dWechatInfo) {
                        $memberList[$k]['is_subscribe'] = $dWechatInfo->subscribe;
                    }
                }
            }
        }

        return [
            'list' => $memberList,
            'total' => $memberCount,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    /**
     * @param $userId
     * @param $groupId
     * @param $rIds
     * @return array
     */
    public function addMember($userId, $groupId, $rIds)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        foreach ($rIds as $rId) {
            // 获取医生库id
            $repsDoctor = NewDoctorUserinfoRepository::getDoctorReps($rId);
            // 判断是否已经在群里
            $repeatMember = NewGroupMemberRepository::getRepeatMember($groupId, $repsDoctor->new_doctor_id, $userId);
//            if ($repeatMember) {
//                throw new GroupException(ErrorCodeGroup::MEMBER_ALREADY_IN);
//            }
            // 判断是否在其他群里
//            $otherMember = NewGroupMemberRepository::getOtherMember($repsDoctor->new_doctor_id, $userId);
//            if ($otherMember->isNotEmpty()) {
//                // 解除关系
//                foreach ($otherMember->toArray() as $other) {
//                    NewGroupMemberRepository::deleteMemberById($other['id']);
//                }
//            }
            if (!$repeatMember) {
                NewGroupMemberRepository::addMember(
                    $repsDoctor->new_doctor_id,
                    $groupId,
                    $userId,
                    NewGroupMember::DOCTOR
            );
            }
        }
        return ['group_id' => $groupId, 'insertIds' => $rIds];
    }

    /**
     * @param $groupId
     * @param $memberIds
     * @return array
     */
    public function deleteMember($groupId, $memberIds)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        NewGroupMemberRepository::removeMember($memberIds);
        return ['group_id' => $groupId, 'member_ids' => $memberIds];
    }

    /**
     * @param $userId
     * @param $groupId
     * @param $memberIds
     * @return array
     */
    public function changeMember($userId, $groupId, $memberIds)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        foreach ($memberIds as $memberId) {
            $membersInfo = NewGroupMemberRepository::getInfoById($memberId);
            if ($membersInfo) {
                NewGroupMemberRepository::addMember(
                    $membersInfo->new_doctor_id,
                    $groupId,
                    $userId,
                    NewGroupMember::DOCTOR
                );
            }
        }
        NewGroupMemberRepository::removeMember($memberIds);
        return ['group_id' => $groupId, 'changeIds' => $memberIds];
    }

    /**
     * @param $userId
     * @param $nId
     * @return array
     */
    public function getMemberInfo($userId, $nId)
    {
        //获取医生信息
        $member = NewGroupMemberRepository::getInGroup($userId, $nId);
        $infoObj = NewDoctorUserinfoRepository::getShareDoctorInfo($nId);

        $info = $infoObj ? $infoObj->toArray() : [];
        if($info['mobile_num']) {
            $pattern = '/1[0-9]{10}/';
            preg_match($pattern, $info['mobile_num'], $matches);
            $info['mobile_num'] = $matches[0];
        } else {
            $info['mobile_num'] = '';
        }
        if ($info['field_id']) {
            $info['field_name'] = AdaptionRepository::getById((int)$info['field_id'])->name;
        } else {
            $info['field_name'] = '';
        }
        if ($info['hospital_yyid']) {
            $hos = HospitalRepository::getHospitalByYyId($info['hospital_yyid']);
            $info['hospital_id'] = $hos ? $hos->id : 0;
        } else {
            $info['hospital_id'] = 0;
        }
        //获取所在群组信息
        if ($member&&$member->group_id) {
            $group = NewGroupRepository::getGroupInfoById($member->group_id, NewGroup::DOCTOR);
            $info['group_yyid'] = $group->yyid;
            $info['group_name'] = $group->group_name;
        } else {
            $info['group_yyid'] = '';
            $info['group_name'] = '我的医生';
        }
        //获取fieldname
        return $info;
    }

    /**
     * @param $groupId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function groupExists($groupId)
    {
        return NewGroupRepository::getMyGroupInfo($groupId, NewGroup::DOCTOR);
    }

    /**
     * @param $trueName
     * @param $hospitalId
     */
    public function checkSame($trueName, $hospitalId)
    {
        $hospitalInfo = HospitalRepository::getHospitalById($hospitalId);
        $same = NewDoctorUserinfoRepository::checkSame($trueName, $hospitalInfo->yyid);
        if ($same) {
            throw new GroupException(ErrorCodeGroup::SAME_HOSPITAL_DOCTOR);
        }
    }

    /**
     * @param $infoId
     * @param $trueName
     * @param $gender
     * @param $mobile
     * @param $hospitalId
     * @param $departId
     * @param $position
     * @param $jobTitle
     * @param $fieldId
     * @param $kolLevel
     * @param $clinicRota
     * @return array
     */
    public function updateMemberInfo(
        $infoId,
        $trueName,
        $gender,
        $mobile,
        $hospitalId,
        $departId,
        $position,
        $jobTitle,
        $fieldId,
        $kolLevel,
        $clinicRota
    ) {
        $user = Helper::getLoginUser();
        if ($infoId==0) {
            // 新增
            $departInfo = SystemDepartRepository::getById($departId);
            $hospitalInfo = HospitalRepository::getHospitalById($hospitalId);
            $nid = NewDoctorUserinfoRepository::createDoctor(
                $trueName,
                $gender,
                $mobile,
                $hospitalInfo ? $hospitalInfo->yyid : '',
                $hospitalInfo ? $hospitalInfo->hospital_name : '',
                $departId,
                $departInfo ? $departInfo->name : '',
                $jobTitle,
                $position,
                $fieldId,
                $kolLevel,
                $clinicRota
            );
            NewDoctorUserinfoRepository::createDoctorReps($user->uid, $nid);
            // 判断科室是否对应固定部门
            $staticDepart = NewGroupStaticRepository::getStaticDepartById($departId);
            if ($staticDepart) {
                NewGroupStaticRepository::createStaticDoctor(
                    $staticDepart->group_department_id,
                    $departId,
                    $user->uid,
                    $nid
                );
            }
        } else {
            // 修改
            $departInfo = SystemDepartRepository::getById($departId);
            $hospitalInfo = HospitalRepository::getHospitalById($hospitalId);
            $oldDoctor = NewDoctorUserinfoRepository::getDoctorById($infoId);
            NewDoctorUserinfoRepository::updateDoctorInfo(
                $infoId,
                $trueName,
                $gender,
                $mobile,
                $hospitalInfo ? $hospitalInfo->yyid : '',
                $hospitalInfo ? $hospitalInfo->hospital_name : '',
                $departId,
                $departInfo ? $departInfo->name : '',
                $jobTitle,
                $position,
                $fieldId,
                $kolLevel,
                $clinicRota
            );

            //如果科室发生了变更，广播该变更事件
            if ($oldDoctor && $oldDoctor->depart_id != $departId) {
                //先清除
                NewGroupStaticRepository::deleteStaticDoctor(
                    $oldDoctor->depart_id,
                    $user->uid,
                    $infoId
                );
                //改变默认分组
                $staticDepart = NewGroupStaticRepository::getStaticDepartById($departId);
                if ($staticDepart) {
                    NewGroupStaticRepository::createStaticDoctor(
                        $staticDepart->group_department_id,
                        $departId,
                        $user->uid,
                        $infoId
                    );
                }
                //推送科室变更事件
                $event = new GroupDoctorDepartChangeEvent();
                $event->setDoctorId($infoId);
                TaskManageHelper::create(0, $event, 'group-doctor-depart-change')
                    ->push();
            }
        }
    }

    /**
     * @param $infoId
     * @param $relatedId
     * @param $agree
     * @return array
     * @throws \Exception
     */
    public function mergeInfo($infoId, $relatedId, $agree)
    {
        $reps = NewDoctorUserinfoRepository::getDoctorReps($infoId);
        $rInfo = NewDoctorUserinfoRepository::getShareDoctorInfo($relatedId);
        if ($agree==1) {
            // 合并
            Db::beginTransaction();
            try {
                NewDoctorUserinfoRepository::mergeInfo($reps->new_doctor_id, $rInfo);
                NewDoctorUserinfoRepository::updateReps($relatedId, $reps->new_doctor_id);
                NewDoctorUserinfoRepository::deleteDoctor($relatedId);
                Db::commit();
                return ['nid' => $reps->new_doctor_id];
            } catch (\Exception $exception) {
                Db::rollBack();
                throw $exception;
            }
        } else {
            // 不合并 只修改手机号
            NewDoctorUserinfoRepository::updateDoctorPhone($reps->new_doctor_id, $rInfo->mobile_num);

            return ['nid' => $reps->new_doctor_id];
        }
    }

    /**
     * @param $userId
     * @param $rId
     * @return array
     */
    public function deleteForAll($userId, $rId)
    {
        Db::beginTransaction();
        try {
            $resp = NewDoctorUserinfoRepository::getDoctorReps($rId);
            $memberGroup = NewDoctorUserinfoRepository::getMyDoctorsInGroup($userId, $resp->new_doctor_id);
            if ($memberGroup->isNotEmpty()) {
                // 移除自定义群组
                NewGroupMemberRepository::deleteMembersByIds(array_column($memberGroup->toArray(), 'id'));
            }
            // 解除关系
            NewAgentInviteRepository::deleteDoctor($rId);
            // 移除固定群组
            NewGroupStaticRepository::removeStaticDoctor($userId, $resp->new_doctor_id);
            // 清空二维码记录
            NewDoctorOpenidUserinfoRepository::updateQrcodeRecode($userId, $resp->new_doctor_id);
            Db::commit();
            return ['r_id' => $rId];
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }
}
