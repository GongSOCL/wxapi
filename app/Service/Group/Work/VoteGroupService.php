<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Group\Work;

use App\Constants\Code\ErrorCodeGroup;
use App\Exception\GroupException;
use App\Model\Qa\NewGroup;
use App\Model\Qa\NewGroupVote;
use App\Model\Qa\NewGroupVoteDetail;
use App\Repository\NewGroup\NewGroupMemberRepository;
use App\Repository\NewGroup\NewGroupRepository;
use App\Repository\VoteGroupRepository;

class VoteGroupService
{
    /**
     * @param $userId
     * @param $groupId
     * @param $tax
     * @return array
     */
    public function createVote($userId, $groupId, $tax)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        if ($tax > 100) {
            throw new GroupException(ErrorCodeGroup::WRONG_TAX);
        }
        //判断是否存在进行中的投票
        $voteInfo = VoteGroupRepository::getProcessingVoteByGroupYyid($groupId);
        if ($voteInfo) {
            throw new GroupException(ErrorCodeGroup::VOTE_ALREADY_EXIST);
        }
        //获取上一次的贡献比例
        $groupInfo = NewGroupRepository::getGroupInfoById($groupId, NewGroup::WORK);
        $fromTax = 0;
        if ($groupInfo && $groupInfo->tax_point) {
            $fromTax = $groupInfo->tax_point;
        }
        $voteid = VoteGroupRepository::addVote($groupId, $userId, $fromTax, $tax / 100);
        return ['vode_id' => $voteid];
    }

    /**
     * @param $groupId
     * @param $voteId
     * @return mixed
     */
    public function getVoteDetail($groupId, $voteId)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        $agreeNum = VoteGroupRepository::getVoteCountByStatus($voteId, NewGroupVoteDetail::STATUS_AGREE);

        $disagreeNum = VoteGroupRepository::getVoteCountByStatus($voteId, NewGroupVoteDetail::STATUS_DISAGREE);

        $memberNum = NewGroupMemberRepository::getGroupMembers($groupId)->count();

        $data['agree_rate'] = $memberNum == 0 ? 0 : intval($agreeNum * 100 / $memberNum);
        $data['disagree_rate'] = $memberNum == 0 ? 0 : intval($disagreeNum * 100 / $memberNum);
        $data['abstain_rate'] = 100 - $data['agree_rate'] - $data['disagree_rate'];

        return $data;
    }

    /**
     * @param $userId
     * @param $groupId
     * @param $voteId
     * @param $agree
     * @return array
     */
    public function memberVote($userId, $groupId, $voteId, $agree)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //群主无法投票
        if ($groupExists->leader_id == $userId) {
            throw new GroupException(ErrorCodeGroup::LEADER_CAN_NOT_VOTE);
        }
        //投票信息是否存在
        $vote = VoteGroupRepository::getNowVote($groupId, $voteId);
        if (!$vote) {
            throw new GroupException(ErrorCodeGroup::VOTE_NOT_EXIST);
        }
        //是否投过票了
        if (VoteGroupRepository::hasVoted($voteId, $userId)) {
            throw new GroupException(ErrorCodeGroup::ALREADY_VOTED);
        }
        //投票
        $voteDetailId = VoteGroupRepository::memberVote($groupId, $voteId, $userId, $agree);
        if (!$voteDetailId) {
            throw new GroupException(ErrorCodeGroup::FAIL_TO_VOTE);
        }
        // 只要有一个不同意则投票失败
        $memberNum = NewGroupMemberRepository::getGroupMembers($groupId)->count();
        $votedNum = VoteGroupRepository::getVoteCount($voteId);
        if ($agree == NewGroupVoteDetail::STATUS_DISAGREE) {
            VoteGroupRepository::updateVoteStatus($voteId, NewGroupVote::STATUS_FAILED);
        } else {
            // 全员同意，更新投票状态为通过,且修改群组贡献度
            if ($votedNum >= $memberNum) {
                VoteGroupRepository::updateVoteStatus($voteId, NewGroupVote::STATUS_PASSED);
                NewGroupRepository::changeTax($groupId, $vote->to_tax);
            }
        }
        //返回投票情况
        $voteDetailInfo = VoteGroupRepository::getVoteDetailById($voteDetailId);
        $votedCount = VoteGroupRepository::getVoteCount($voteId);
        $voteStatus = VoteGroupRepository::getVoteById($voteId);

        $data = [
            'vote_detail_yyid' => $voteDetailInfo->yyid,
            'member_count' => $memberNum,
            'vote_count' => $votedCount,
            'vote_status' => $voteStatus->status
        ];

        return $data;
    }

    /**
     * @param $userId
     * @param $voteId
     * @return array
     */
    public function cancelVote($userId, $voteId)
    {
        //投票信息是否存在
        $vote = VoteGroupRepository::getVoteById($voteId);
        if (!$vote) {
            throw new GroupException(ErrorCodeGroup::VOTE_NOT_EXIST);
        }
        //判断群是否存在
        $groupExists = $this->groupExists($vote->group_id);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //群主，无法中止投票
        if ($groupExists->leader_id !== $userId) {
            throw new GroupException(ErrorCodeGroup::ONLY_LEADER_CAN_CANCEL);
        }
        VoteGroupRepository::updateVoteStatus($voteId, NewGroupVote::STATUS_CLOSED);
        return ['vote_id' => $voteId];
    }

    /**
     * @param $groupId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function groupExists($groupId)
    {
        return NewGroupRepository::getGroupInfoById($groupId, NewGroup::WORK);
    }
}
