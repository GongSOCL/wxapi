<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Group\Work;

use App\Constants\Code\ErrorCodeGroup;
use App\Constants\DataStatus;
use App\Exception\GroupException;
use App\Model\Qa\NewGroup;
use App\Model\Qa\NewGroupMember;
use App\Repository\ActivityRepository;
use App\Repository\DrugSeriesRepository;
use App\Repository\NewGroup\NewGroupMemberRepository;
use App\Repository\NewGroup\NewGroupRepository;
use App\Repository\UsersRepository;
use App\Repository\VoteGroupRepository;
use App\Repository\WechatUserRepository;
use App\Service\Wechat\WechatBusiness;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

class WrokGroupService
{
    /**
     * @Inject()
     * @var WechatBusiness
     */
    protected $wechatBusiness;

    /**
     * @param $userId
     * @param $keyWord
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getGroupList(
        $userId,
        $userYyid,
        $keyWord,
        $page,
        $pageSize
    ) {
        //我创建的组
        $myGroupInfo = NewGroupRepository::getMyGroupInfo($userId, NewGroup::WORK);
        $myGroupIdsArr = $myGroupInfo->isNotEmpty() ? array_column($myGroupInfo->toArray(), 'id') : [];
        //有我的组
        $wechatInfo = WechatUserRepository::getByYyid($userYyid);
        $joinGroupIdsArr = [];
        if ($wechatInfo) {
            $joinGroupInfo = NewGroupMemberRepository::getJoinGroupInfo(
                $wechatInfo->unionid,
                NewGroup::WORK
            );
            $joinGroupIdsArr = $joinGroupInfo->isNotEmpty() ? array_column($joinGroupInfo->toArray(), 'group_id') : [];
        }

        //所有组的id搜索
        $groupIds = array_unique(array_filter(array_merge($myGroupIdsArr, $joinGroupIdsArr)));
        $groupInfo = NewGroupRepository::getGroupInfo(
            $groupIds,
            $keyWord,
            $page,
            $pageSize
        );
        $groupInfoCount = NewGroupRepository::getGroupCount($groupIds, $keyWord);
        return [
            'list' => $groupInfo->isNotEmpty() ? $groupInfo->toArray() : [],
            'total' => $groupInfoCount,
            'page' => $page,
            'page_size' => $pageSize,
        ];
    }

    /**
     * @param $userId
     * @param $groupId
     * @return array
     */
    public function getGroupDetail($userId, $groupId)
    {
        // 群组基本信息
        $group = NewGroupRepository::getGroupInfoById($groupId, NewGroup::WORK);
        if (!$group) {
            return [];
        }
        // 群组负责产品
        $seriesInfo = [];
        if ($group && $group->series_id) {
            $seriesInfo = DrugSeriesRepository::getById($group->series_id);
        }
        // 投票信息
        $voteInfo = VoteGroupRepository::getByGroupId($groupId);
        $hasVoted = [];
        if ($voteInfo) {
            $hasVoted = VoteGroupRepository::hasVoted($voteInfo->id, $userId);
        }
        // 群主信息
        $leaderInfo = UsersRepository::getUserByUid($group->leader_id);

        $data = [];
        $data['is_leader'] = $userId === $group->leader_id ? 1 : 0;
        $data['group_id'] = $group->id;
        $data['group_name'] = $group->group_name;
        $data['series_id'] = $seriesInfo ? $seriesInfo->id : '';
        $data['series_name'] = $seriesInfo ? $seriesInfo->series_name : '';
        $data['leader_name'] = $leaderInfo->name ?? $leaderInfo->wechat;
        $data['notice'] = $group->notice;
        $data['current_tax_rate'] = $group->tax_point ? (string)(($group->tax_point * 100).'%') : '';
        $data['vote_status'] = $voteInfo ? $voteInfo->status : -1;
        $data['has_voted'] = $hasVoted ? 1 : 0;
        $data['vote_id'] = $voteInfo ? $voteInfo->id : '';
        $data['vote_tax_rate'] = ($voteInfo && $voteInfo->to_tax) ? (string)(($voteInfo->to_tax * 100).'%') : '';
        $data['join_num'] = NewGroupMemberRepository::getWorkGroupJoinMemberNum($group->id, NewGroup::WORK);

        return $data;
    }

    /**
     * @param $userId
     * @param $groupName
     * @return array
     */
    public function createGroup($userId, $groupName)
    {
        //判断是否有同名群组
        $alreadyNamed = NewGroupRepository::sameGroupName($userId, $groupName, NewGroup::WORK);
        if ($alreadyNamed) {
            throw new GroupException(ErrorCodeGroup::GROUP_ALREADY_EXISTS);
        }
        //创建群组
        $groupId = NewGroupRepository::createGroup($userId, $groupName, NewGroup::WORK);
        return ['group_id' => $groupId];
    }

    /**
     * @param $userId
     * @param $groupId
     * @param $groupName
     * @return array
     */
    public function renameGroup($userId, $groupId, $groupName)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //判断是否有同名群组
        $alreadyNamed = NewGroupRepository::sameGroupName($userId, $groupName, NewGroup::WORK, $groupId);
        if ($alreadyNamed) {
            throw new GroupException(ErrorCodeGroup::GROUP_ALREADY_EXISTS);
        }
        //改名
        $update = NewGroupRepository::renameGroup($groupId, $groupName, NewGroup::WORK);
        if (!$update) {
            throw new GroupException(ErrorCodeGroup::FAIL_TO_RENAME_GROUP);
        }
        return ['group_id' => $groupId];
    }

    /**
     * @param $userId
     * @param $groupId
     * @param $content
     * @return array
     */
    public function changeNotice($userId, $groupId, $content)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //只有群主才能修改群公告
        if ($groupExists->leader_id !== $userId) {
            throw new GroupException(ErrorCodeGroup::ONLY_LEADER_CAN_CHANGE_NOTICE);
        }
        //修改群公告
        $update = NewGroupRepository::changeNotice($groupId, $content);
        if (!$update) {
            throw new GroupException(ErrorCodeGroup::FAIL_TO_CHANGE_NOTICE);
        }
        return ['group_id' => $groupId];
    }

    /**
     * @param $userId
     * @param $groupId
     * @return array
     */
    public function getSeries($userId, $groupId)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //只有群主才能查看产品
        if ($groupExists->leader_id !== $userId) {
            throw new GroupException(ErrorCodeGroup::ONLY_LEADER_CAN_CHECK_SERIES);
        }
        $seriess = DrugSeriesRepository::getAvailableSeries();
        $groupInfo = NewGroupRepository::getGroupInfoById($groupId, NewGroup::WORK);

        $data = [];
        foreach ($seriess as $series) {
            $data[] = [
                'series_id' => $series->id,
                'name' => $series->series_name,
                'selected' => $groupInfo->series_id === $series->id ? 1 : 0
            ];
        }
        return $data;
    }

    /**
     * @param $userId
     * @param $groupId
     * @param $seriesId
     * @return array
     */
    public function changeSeries($userId, $groupId, $seriesId)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //只有群主才能查看产品
        if ($groupExists->leader_id !== $userId) {
            throw new GroupException(ErrorCodeGroup::ONLY_LEADER_CAN_CHECK_SERIES);
        }
        //产品是否存在
        $series = DrugSeriesRepository::getById($seriesId);
        if (!$series) {
            throw new GroupException(ErrorCodeGroup::SERIES_NOT_EXISTS);
        }
        //判断群组是否已经绑定产品
        $groupRepsInfo = NewGroupRepository::getGroupInfoById($groupId, NewGroup::WORK);
        if ($groupRepsInfo->series_id) {
            throw new GroupException(ErrorCodeGroup::CAN_NOT_CHANGE_SERIES);
        }
        //判断组内成员是否参与其他组并选择同一个产品
        $mumbersGroup = NewGroupMemberRepository::getGroupMembers($groupId);
        $allMumbers = [];
        if ($mumbersGroup->isNotEmpty()) {
            $allMumbers = array_column($mumbersGroup->toArray(), 'user_unionid');
        }
        $userYyidsHasSameGroup = NewGroupMemberRepository::filterUserIdsBySeriesChange(
            $seriesId,
            $allMumbers,
            $groupId
        );
        if ($userYyidsHasSameGroup->isNotEmpty()) {
            throw new GroupException(ErrorCodeGroup::MEMBER_ALREADY_IN_SERIES);
        }
        NewGroupRepository::changeSeries($groupId, $seriesId);
        return ['series_id' => $seriesId];
    }

    /**
     * @param $userId
     * @param $userYyid
     * @param $groupId
     * @param $memberId
     * @return array
     * @throws \Exception
     */
    public function choseNewLeader($userId, $userYyid, $groupId, $memberId)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //只有群主才能修改群主
        if ($groupExists->leader_id !== $userId) {
            throw new GroupException(ErrorCodeGroup::ONLY_LEADER_CAN_CHANGE_LEADER);
        }
        //检查是否有进行中的投票
        $voteInfo = VoteGroupRepository::getProcessingVoteByGroupYyid($groupId);
        if ($voteInfo) {
            throw new GroupException(ErrorCodeGroup::CAN_NOT_CHANGE_LEADER);
        }
        //判断微信信息
        $wxinfo = WechatUserRepository::getByYyid($userYyid);
        if (!$wxinfo) {
            throw new GroupException(ErrorCodeGroup::WECHAT_MEMBER_NOT_EXIST);
        }
        Db::beginTransaction();
        try {
            $memberInfo = NewGroupMemberRepository::getInfoById($memberId);
            $wechatInfo = WechatUserRepository::getByUnionid($memberInfo->user_unionid);
            $userInfo = UsersRepository::getUserByYYID($wechatInfo->u_yyid);
            //t_group表leader_yyid变更
            NewGroupRepository::changeLeader($groupId, $userInfo->uid);
            //将当前群主加入成员
            $isMember = NewGroupMemberRepository::getByUserIdAndGroupId($wxinfo->unionid, $groupId);
            if (!$isMember) {
                NewGroupMemberRepository::createOneMember(NewGroupMember::WORK, $groupId, $wxinfo);
            }
            //删除现群主
            NewGroupMemberRepository::deleteMemberById($memberId);
            Db::commit();
            return [
                'group_id' => $groupId,
                'leader_id' => $userInfo->uid,
                'old_leader_id' => $userId
            ];
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $userId
     * @param $groupId
     * @return array
     * @throws \Exception
     */
    public function removeGroup($userId, $groupId)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //只有群主才能解散群
        if ($groupExists->leader_id !== $userId) {
            throw new GroupException(ErrorCodeGroup::ONLY_LEADER_CAN_DELETE_GROUP);
        }
        Db::beginTransaction();
        try {
            NewGroupRepository::deleteGroup($userId, $groupId);
            NewGroupMemberRepository::deleteGroupMembers($groupId);
            Db::commit();
            return ['group_id' => $groupId];
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $groupId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function groupExists($groupId)
    {
        return NewGroupRepository::getGroupInfoById($groupId, NewGroup::WORK);
    }

    /**
     * @param $userId
     * @param $groupId
     * @return array
     */
    public function getQrcode($userId, $groupId)
    {
        //获取行为信息 来源：t_activity
        $activityData = ActivityRepository::getActivity(DataStatus::ACTIVITY_ID_QRCODE_AGENT_GROUP);
        if ($activityData) {
            $inviteData = $activityData->id.'#'.$activityData->event_key.'_'.$userId.'_'.$groupId;
        }

        $url = $this->wechatBusiness->getQrcode($inviteData, DataStatus::WECHAT_AGENT);
        return ['url' => $url];
    }
}
