<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Group\Work;

use App\Constants\Code\ErrorCodeGroup;
use App\Exception\GroupException;
use App\Model\Qa\NewAgentInvite;
use App\Model\Qa\NewGroup;
use App\Model\Qa\NewGroupMember;
use App\Repository\NewGroup\NewAgentInviteRepository;
use App\Repository\NewGroup\NewGroupMemberRepository;
use App\Repository\NewGroup\NewGroupRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;

class WrokGroupMemberService
{
    /**
     * @param $groupId
     * @return array
     */
    public function getMemberList($groupId)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }

        //获取组长
        $leaderUser = UsersRepository::getUserByUid($groupExists->leader_id);
        $leaderWechat = WechatUserRepository::getByYyid($leaderUser->yyid);
        $leaderHospitalNum = RepsServeScopeSRepository::getServeHospitalNum($leaderUser->yyid);

        $data = [];
        $data[0]['name'] = $leaderWechat->nickname;
        $data[0]['headimgurl'] = $leaderWechat->headimgurl;
        $data[0]['realname'] = $leaderUser ? $leaderUser->name : '';
        $data[0]['user_unionid'] = $leaderWechat->unionid;
        $data[0]['member_id'] = 0;
        $data[0]['user_id'] = $leaderUser ? $leaderUser->uid : '';
        $data[0]['hospital_num'] = $leaderHospitalNum ? $leaderHospitalNum->hospital_num : 0;
        $data[0]['is_leader'] = 1;

        //获取群组员
        $members = NewGroupMemberRepository::getGroupMembers($groupId);
        if ($members->isNotEmpty()) {
            foreach ($members->toArray() as $k => $member) {
                //成员微信信息
                $wusers = WechatUserRepository::getByUnionid($member['user_unionid']);
                $userInfo = UsersRepository::getUserByYYID($wusers->u_yyid);
                // 成员服务医院数
                $hospitalNum = RepsServeScopeSRepository::getServeHospitalNum($wusers->u_yyid);
                $data[$k+1]['name'] = $wusers->nickname;
                $data[$k+1]['headimgurl'] = $wusers->headimgurl;
                $data[$k+1]['user_unionid'] = $wusers->unionid;
                $data[$k+1]['realname'] = $userInfo ? $userInfo->name : '';
                $data[$k+1]['member_id'] = $member['id'];
                $data[$k+1]['user_id'] = $userInfo ? $userInfo->uid : '';
                $data[$k+1]['hospital_num'] = $hospitalNum ? $hospitalNum->hospital_num : 0;
                $data[$k+1]['is_leader'] = 0;
            }
        }
        return $data;
    }

    /**
     * @param $userId
     * @param $groupId
     * @return array
     */
    public function getJoinList($userId, $groupId)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //不是群主无法查看待加入人员列表
        if ($groupExists->leader_id !== $userId) {
            throw new GroupException(ErrorCodeGroup::ONLY_LEADER_CAN_CHECK_JOINLIST);
        }
        $members = NewGroupMemberRepository::getGroupJoinList($groupId, NewGroupMember::WORK);
        if (empty($members)) {
            return [];
        }
        $data = [];
        foreach ($members->toArray() as $k => $member) {
            //成员微信信息
            $wusers = WechatUserRepository::getByUnionid($member['user_unionid']);
            $userInfo = UsersRepository::getUserByYYID($wusers->u_yyid);
            $data[$k]['name'] = $wusers->nickname;
            $data[$k]['headimgurl'] = $wusers->headimgurl;
            $data[$k]['realname'] = $userInfo ? $userInfo->name : '';
            $data[$k]['member_id'] = $member['id'];
            $data[$k]['user_id'] = $userInfo ? $userInfo->uid : '';
            $data[$k]['user_unionid'] = $member['user_unionid'];
        }
        return $data;
    }

    /**
     * @param $userId
     * @param $userYyid
     * @param $groupId
     * @return array
     */
    public function getMyagentList($userId, $userYyid, $groupId)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //关联的待邀请人员
        $agents = NewAgentInviteRepository::getMyUserAgents($userId, NewAgentInvite::AGENT);
        if ($agents->isEmpty()) {
            return [];
        }
        $userUnionids = array_unique(array_column($agents->toArray(), 'user_unionid'));
        //排除自己（可能有）
        $myid = WechatUserRepository::getByYyid($userYyid);
        $userUnionids = array_diff($userUnionids, [$myid->unionid]);
        //排除群主（可能有）
        $leadUserInfo = UsersRepository::getUserByUid($groupExists->leader_id);
        $leadUnionid = WechatUserRepository::getByYyid($leadUserInfo->yyid);
        $userUnionids = array_diff($userUnionids, [$leadUnionid->unionid]);
        //排除已加入和待加入
        $alreadyIn = NewGroupMemberRepository::getGroupMembersByStatus($groupId);
        $notInviteUnionids = array_diff(
            $userUnionids,
            array_unique(array_column($alreadyIn->toArray(), 'user_unionid'))
        );
        if (empty($notInviteUnionids)) {
            return [];
        }
        //排除那些已经进了同产品其它组的成员
        $diffUserUnionids = $this->diffUsersWithSameGroupSeries($groupExists, $notInviteUnionids);
        if (empty($diffUserUnionids)) {
            return [];
        }

        $data = [];
        foreach (array_values($diffUserUnionids) as $k => $diffUserUnionid) {
            //成员微信信息
            $wusers = WechatUserRepository::getByUnionid($diffUserUnionid);
            $userInfo = UsersRepository::getUserByYYID($wusers->u_yyid);
            $data[$k]['name'] = $wusers->nickname;
            $data[$k]['headimgurl'] = $wusers->headimgurl;
            $data[$k]['realname'] = $userInfo ? $userInfo->name : '';
            $data[$k]['user_unionid'] = $diffUserUnionid;
            $data[$k]['user_id'] = $userInfo ? $userInfo->uid : '';
        }
        return $data;
    }

    /**
     * @param $groupInfo
     * @param $notInviteUnionids
     * @param bool $checkSeries
     * @return array
     */
    public function diffUsersWithSameGroupSeries($groupInfo, $notInviteUnionids, $checkSeries = false)
    {
        if (!$groupInfo->series_id) {
            //在分组末分配产品的情况下不用比较同系产品人员
            if ($checkSeries) {
                throw new GroupException(ErrorCodeGroup::CAN_NOT_ADD_MEMBER);
            }
            //尚末分配产品线，也就意味着可以返回所有的人
            return $notInviteUnionids;
        } else {
            //现在已经加入相同产品组的代表id
            $userYyidsHasSameGroup = NewGroupMemberRepository::filterUserIdsBySeries(
                $groupInfo->series_id,
                $notInviteUnionids,
                NewGroupMember::WORK
            );
            if ($userYyidsHasSameGroup->isNotEmpty()) {
                return array_diff($notInviteUnionids, array_column($userYyidsHasSameGroup->toArray(), 'user_unionid'));
            } else {
                return $notInviteUnionids;
            }
        }
    }

    /**
     * @param $userId
     * @param $userYyid
     * @param $groupId
     * @param $addUserUnionids
     * @return mixed
     */
    public function addMember($userId, $userYyid, $groupId, $addUserUnionids)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }

        $members = NewGroupMemberRepository::getGroupMembers($groupId);
        $userUnionids = [];
        if ($members->isNotEmpty()) {
            $userUnionids = array_filter(array_column($members->toArray(), 'user_unionid'));
        }
        $myUnionid = WechatUserRepository::getByYyid($userYyid);
        // 当前用户不在群组中且不是管理员，无法添加成员
        if ($userId !== $groupExists->leader_id && !in_array($myUnionid->unionid, $userUnionids)) {
            throw new GroupException(ErrorCodeGroup::CAN_NOT_ADD_MEMBER_NOT_IN);
        }
        foreach ($addUserUnionids as $addUserUnionid) {
            NewGroupMemberRepository::addMember($addUserUnionid, $groupId, $userId, NewGroupMember::WORK);
        }

        return $addUserUnionids;
    }

    /**
     * @param $userId
     * @param $groupId
     * @param $memberIds
     * @param $approve
     * @return mixed
     */
    public function approval($userId, $groupId, $memberIds, $approve)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //非管理员无法进行此操作
        if ($userId !== $groupExists->leader_id) {
            throw new GroupException(ErrorCodeGroup::ONLY_LEADER_CAN_APPROVE);
        }
        $unionids = NewGroupMemberRepository::getGroupMembersByIds($memberIds);
        //判断是否已经进了同产品其它组
        if ($groupExists->series_id) {
            $alreadyInOtherGroup = NewGroupMemberRepository::filterUserIdsBySeries(
                $groupExists->series_id,
                array_unique(array_column($unionids->toArray(), 'user_unionid')),
                NewGroupMember::WORK
            );
            if ($alreadyInOtherGroup->isNotEmpty()) {
                throw new GroupException(ErrorCodeGroup::ALREADY_IN_DRUG_SERIES);
            }
        }
        NewGroupMemberRepository::approveMember($groupId, $approve, $memberIds);
        return $memberIds;
    }

    /**
     * @param $userId
     * @param $groupId
     * @param $memberIds
     * @return mixed
     */
    public function removeMember($userId, $groupId, $memberIds)
    {
        //判断群是否存在
        $groupExists = $this->groupExists($groupId);
        if (!$groupExists) {
            throw new GroupException(ErrorCodeGroup::GROUP_NOT_EXISTS);
        }
        //非管理员无法进行此操作
        if ($userId !== $groupExists->leader_id) {
            throw new GroupException(ErrorCodeGroup::ONLY_LEADER_CAN_DELETE);
        }
        NewGroupMemberRepository::removeMember($memberIds);
        return $memberIds;
    }

    /**
     * @param $groupId
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function groupExists($groupId)
    {
        return NewGroupRepository::getGroupInfoById($groupId, NewGroup::WORK);
    }
}
