<?php
declare(strict_types=1);
namespace App\Service\Group;

use App\Constants\DataStatus;
use App\Model\Qa\NewGroupMember;
use App\Model\Qa\WechatUser;
use App\Repository\NewGroup\NewAgentInviteRepository;
use App\Repository\NewGroup\NewGroupMemberRepository;

class AgentGroupService
{
    public static function processAgentGroupEvent(
        $inviterId,
        $wxinfo,
        $groupId,
        WechatUser $wechatUser = null,
        $wechatType = DataStatus::WECHAT_AGENT
    ) {
        //查找好友关系
        $friendRelation = NewAgentInviteRepository::findRelation($inviterId, $wxinfo, $wechatType);
        if ($friendRelation) {
            //如果存在好友关系直接把status=1
            NewAgentInviteRepository::updateRelationStatus($inviterId, $wxinfo, $wechatType);
        } else {
            //如果不存在 添加好友关系
            NewAgentInviteRepository::addRelation($inviterId, $wxinfo, $wechatType);
        }

        //查找组邀请关系(分组邀请关系有地方确认，不直接确认)
        $groupRelation = NewGroupMemberRepository::findGroupRelation(
            $groupId,
            NewGroupMember::WORK,
            $wxinfo,
            $inviterId
        );
        if (!$groupRelation) {
            //没有直接添加一条
            NewGroupMemberRepository::addMember(
                $wxinfo['unionid'],
                $groupId,
                $inviterId,
                NewGroupMember::WORK,
                NewGroupMember::JOIN
            );
        }
    }

    public static function processAgentInviteEvent(
        $inviterId,
        $wxinfo,
        WechatUser $wechatUser = null,
        $wechatType = DataStatus::WECHAT_AGENT
    ) {
        //查找好友关系
        $friendRelation = NewAgentInviteRepository::findRelation($inviterId, $wxinfo, $wechatType);
        if ($friendRelation) {
            //如果存在好友关系直接把status=1
            NewAgentInviteRepository::updateRelationStatus($inviterId, $wxinfo, $wechatType);
        } else {
            //如果不存在 添加好友关系
            NewAgentInviteRepository::addRelation($inviterId, $wxinfo, $wechatType);
        }
    }
}
