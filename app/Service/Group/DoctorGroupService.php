<?php
declare(strict_types=1);
namespace App\Service\Group;

use App\Constants\DataStatus;
use App\Model\Qa\WechatUser;
use App\Repository\DoctorOpenidUserinfoRepository;
use App\Repository\GroupMemberRepository;
use App\Repository\UsersRepository;

class DoctorGroupService
{
    /**
     * 医生群组邀请事件
     *
     * @param $uid
     * @param $wxInfo
     * @param WechatUser|null $wechatUser
     * @param int $wechatType
     */
    public static function processDoctorGroupEvent(
        $uid,
        array $wxInfo,
        WechatUser $wechatUser = null,
        $wechatType = DataStatus::WECHAT_AGENT
    ) {
        $user = UsersRepository::getUserByUid($uid);
        $agentYyid = $user ? $user->yyid : 'user_yyid';

        $inviteeUserYyid = $wechatUser ? $wechatUser->u_yyid : '';

        //更新代表医生信息
        DoctorOpenidUserinfoRepository::findUpdateOrAddDoctorWechat($uid, $wxInfo, $wechatType);

        //查找/更新好友邀请关系
        GroupMemberRepository::findOrUpdateRelation(
            $agentYyid,
            $wxInfo,
            $inviteeUserYyid,
            false,
            DataStatus::USER_TYPE_DOCTOR,
            $wechatType
        );

        //更新订阅信息
        GroupMemberRepository::batchSetSubscribed($wxInfo['openid'], $wechatType);
    }

    /**
     * 个人中心医生邀请医生事件
     *
     * @param $uid
     * @param array $wxInfo
     * @param WechatUser|null $wechatUser
     * @param int $wechatType
     */
    public static function processDoctorInviteEvent(
        $uid,
        array $wxInfo,
        WechatUser $wechatUser = null,
        $wechatType = DataStatus::WECHAT_DOCTOR
    ) {
        $user = UsersRepository::getUserByUid($uid);
        $agentYyid = $user ? $user->yyid : 'user_yyid';

        $inviteeUserYyid = $wechatUser ? $wechatUser->u_yyid : '';

        //查找/更新好友邀请关系
        GroupMemberRepository::findOrUpdateRelation(
            $agentYyid,
            $wxInfo,
            $inviteeUserYyid,
            true,
            DataStatus::USER_TYPE_DOCTOR,
            $wechatType
        );

        //更新订阅信息
        GroupMemberRepository::batchSetSubscribed($wxInfo['openid'], $wechatType);
    }
}
