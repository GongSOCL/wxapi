<?php
declare(strict_types=1);

namespace App\Service\ALiYun;

use AlibabaCloud\Darabonba\Stream\StreamUtil;
use AlibabaCloud\SDK\Ocrapi\V20210707\Models\RecognizeIdcardRequest;
use AlibabaCloud\SDK\Ocrapi\V20210707\Ocrapi;
use AlibabaCloud\Tea\Exception\TeaError;
use AlibabaCloud\Tea\Tea;
use AlibabaCloud\Tea\Utils\Utils;
use AlibabaCloud\Tea\Utils\Utils\RuntimeOptions;
use App\Helper\Helper;
use App\Helper\Minio;
use App\Service\Report\ReportService;
use App\Service\Upload\UploadRpcService;
use Darabonba\OpenApi\Models\Config;
use \Exception;

class OCRAPIService
{
    /**
     * 使用AK&SK初始化账号Client
     * @param string $accessKeyId
     * @param string $accessKeySecret
     * @return Ocrapi Client
     */
    public static function createClient()
    {
        $resp = UploadRpcService::getOssSts();

        $config = new Config([
            // 您的 AccessKey ID
            "accessKeyId" => $resp->getKey(),
            // 您的 AccessKey Secret
            "accessKeySecret" =>$resp->getSecret(),
            'securityToken' => $resp->getToken(),
            'type' => 'sts'
        ]);

        // 访问的域名
        $config->endpoint = "ocr-api.cn-hangzhou.aliyuncs.com";

        return new Ocrapi($config);
    }

    /**
     * @param string[] $args
     * @return void
     */
    public static function main($FilePath, $fileName)
    {
        $client = self::createClient();

        $redis = Helper::getRedis();

        $uploadPath = sprintf("/idcard/%s/%s_%s.jpg", date('Ymd'), $fileName, uniqid());

        ReportService::uploadFileToMinio($FilePath, $uploadPath, $fileName, 'image/jpeg');

        $url = \App\Kernel\Minio\Minio::createPublicBucketFileLink(env('S3_BUCKET'), $uploadPath, '', true);

        $recognizeIdcardRequest = new RecognizeIdcardRequest([
            "url" => $url
        ]);

        $runtime = new RuntimeOptions([]);

        try {
            // 复制代码运行请自行打印 API 的返回值
            $resp = $client->recognizeIdcardWithOptions($recognizeIdcardRequest, $runtime);

            $res = json_decode(Utils::toJSONString(Tea::merge($resp)), true);

            if (isset($res['statusCode']) && $res['statusCode'] == 200) {
                $info = json_decode($res['body']['Data'], true);

                if (isset($info['data']['face'])) {
                    return $info['data']['face']['data'];
                } else {
                    return $info['data']['back']['data'];
                }
            }
        } catch (Exception $error) {
            if (!($error instanceof TeaError)) {
                $error = new TeaError([], $error->getMessage(), $error->getCode(), $error);
            }
            // 获取报错数据
            $res = Utils::assertAsString($error->message);

            $key = "OCR-ERROR";

            $redis->set($key, $res);
        }
    }

    public static function uploadFile($localPath, $path, $isForce = false): string
    {
        $fs = Minio::getFileSystem();
        $hasPath = $fs->has($path);
        if ($hasPath && ! $isForce) {
            return '';
        }
//        if ($hasPath) {
//            $fs->delete($path);
//        }

        $fs->put($path, file_get_contents($localPath));
        return Minio::createPublicBucketFileLink(env('S3_BUCKET'), $path);
    }
}
