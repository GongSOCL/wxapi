<?php
declare(strict_types=1);
namespace App\Service\Compliance;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Constants\ExamCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\TaskManageHelper;
use App\Kernel\Pdf\Pdf;
use App\Kernel\Qiniu\Upload;
use App\Kernel\Uuid\Uuid;
use App\Model\Qa\ComplianceAgreement;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ComplianceExamAnswersSnapshot;
use App\Model\Qa\ComplianceQuestion;
use App\Model\Qa\ComplianceQuestionAnswer;
use App\Model\Qa\ComplianceQuestionItem;
use App\Model\Qa\ComplianceTestPaper;
use App\Model\Qa\ComplianceTestPaperItem;
use App\Model\Qa\ComplianceUserAgreement;
use App\Model\Qa\ComplianceUserExam;
use App\Model\Qa\LearningPlan;
use App\Model\Qa\QuestionnaireTestPaper;
use App\Model\Qa\TComplianceTestExamFraction;
use App\Model\Qa\Users;
use App\Model\Qa\TComplianceQuestionItemsFraction;
use App\Repository\ComplianceAgreementRepository;
use App\Repository\ComplianceExamAnswersSnapshotRepository;
use App\Repository\ComplianceExamRepository;
use App\Repository\ComplianceExamResponseRepository;
use App\Repository\ComplianceExamUserRepository;
use App\Repository\ComplianceQuestionAnswerRepository;
use App\Repository\ComplianceQuestionItemsRepository;
use App\Repository\ComplianceQuestionRepository;
use App\Repository\ComplianceTestPaperRepository;
use App\Repository\ComplianceUserAgreementRepository;
use App\Repository\ComplianceUserExamRepository;
use App\Repository\ExaminationResultRepository;
use App\Repository\RepsServeScopePRepository;
use App\Repository\UpExamDateRepository;
use App\Service\Compliance\Agree\AgreeHtml;
use App\Service\User\UserService;
use Carbon\Carbon;
use Grpc\Task\ExamToDb;
use Hyperf\DbConnection\Db;
use Hyperf\Paginator\LengthAwarePaginator;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use Hyperf\Utils\Coroutine;
use InvalidArgumentException;
use phpDocumentor\Reflection\Types\Self_;
use Ramsey\Uuid\Rfc4122\UuidV4;
use function _HumbugBox2acd634d137b\React\Promise\Stream\first;
use function PHPStan\dumpType;

class ExamService
{
    /**
     * 考试列表
     * @param string $keywords
     * @param int $status
     * @param int $current
     * @param int $limit
     * @return array
     * @throws BusinessException
     * @throws InvalidArgumentException
     */
    public static function list(string $keywords = "", int $status = 0, int $current = 1, int $limit = 10): array
    {
        $user = Helper::getLoginUser();

        // 0全部 1进行中 2已通过 3未通过 4已过期
        $resp = null;
        switch ($status) {
            case 1:
                $resp = self::getProcessingExams($keywords, $current, $limit);
                break;
            case 2:
                $resp = self::getPartedExams($keywords, $current, $limit, false);
                break;
            case 3:
                $resp = self::getPartedExams($keywords, $current, $limit, true);
                break;
            case 4:
                $resp = self::getExpireExams($keywords, $current, $limit);
                break;
            default:
                $resp = self::getAllExams($keywords, $current, $limit);
        }


        $page = [
            'total' => 0,
            'current' => 1,
            'pages' => 1,
            'limit' => $limit
        ];
        $datas = [];
        if (!$resp || ($resp->total() == 0)) {
            return [$page, $datas];
        }
        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $limit
        ];

        $statusMap = [];
        if ($status  == 0) {
            $pageYYIDS = $resp->getCollection()->pluck('yyid')->all();
            $statusMap = self::filterUserExamsInfos($user, $pageYYIDS);
        }



        # 【待重新封装计算考试状态 - 整合列表 && 详情 20220728】 - 比较混乱




        //构建返回数据
        foreach ($resp->items() as $item) {
                [$st, $passTime] = self::transferStatus($item, $status, $statusMap);

            if ($status == 1) {
                if ($st == 1) {
                    $datas[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'start' => $item->exam_start,
                        'end' => (string) $item->exam_end,
                        'is_time_unlimited' => $item->exam_end ? 0 : 1,
                        'status' => $st,
                        'pass_time' => $passTime,
                        'exam_num' => $item->exam_num,
                        'exam_time' => $item->exam_time,
                        'plan_days' => $item->plan_days,
                        'exam_type' => $item->exam_type
                    ];
                } else {
                    continue;
                }
            } elseif ($status == 4) {
                if ($st == 4) {
                    $datas[] = [
                        'id' => $item->id,
                        'name' => $item->name,
                        'start' => $item->exam_start,
                        'end' => (string) $item->exam_end,
                        'is_time_unlimited' => $item->exam_end ? 0 : 1,
                        'status' => $st,
                        'pass_time' => $passTime,
                        'exam_num' => $item->exam_num,
                        'exam_time' => $item->exam_time,
                        'plan_days' => $item->plan_days,
                        'exam_type' => $item->exam_type
                    ];
                } else {
                    continue;
                }
            } else {
                $datas[] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'start' => $item->exam_start,
                    'end' => (string) $item->exam_end,
                    'is_time_unlimited' => $item->exam_end ? 0 : 1,
                    'status' => $st,
                    'pass_time' => $passTime,
                    'exam_num' => $item->exam_num,
                    'exam_time' => $item->exam_time,
                    'plan_days' => $item->plan_days,
                    'exam_type' => $item->exam_type
                ];
            }
        }

        $data = $datas;

        if (!$status && $data) {
            # 查询是否存在补考 若该考试下 status=2【不通过】 并且 有补考
            $examIds = array_column($data, 'id');

            $examIds = array_unique($examIds);

            if (count($examIds) > 0) {
                $upExamData = UpExamDateRepository::getUpExamDate($examIds);

                $upExamDatas = [];

                if (!$upExamData->isEmpty()) {
                    $upExamDatas = $upExamData->toArray();

                    $upExamDatas = array_column($upExamDatas, null, 'exam_id');
                }

                # 勾建新的考试列表
                $now = date('Y-m-d H:i:s');

                foreach ($data as $k => $val) {
                    $exam_num = $data[$k]['exam_num'];

                    if ($val['exam_type'] > 1) {
                        continue;
                    }

                    if ($val['status'] == 3) {  # 【 赵 判断考试是否通过状态 】原始状态 已通过
                        continue;
                    }

                    $userInfo = ExaminationResultRepository::getUserStatus([$val['id']], $user->yyid);
                    if (!$userInfo->isNotEmpty()) {
                        continue;
                    }
                    $userInfo = $userInfo->toArray();

                    $userInfo = array_column($userInfo, null, 'exam_id');

                    $status_exam = 0;

                    $statusExam = $val['status'];

                    $examConf = 0;

                    $up = 0;

                    if (isset($upExamDatas[$val['id']])) {  # Todo  有补考
                        if ($now >= $val['end'] && $val['end']) {    # 考试结束
                            $val['exam_num'] = $exam_num;

                            $status_exam = self::examStatus($val, $upExamDatas[$val['id']], $userInfo[$val['id']], 1);

                            $examConf = 1;

                            $data[$k]['start'] = $upExamDatas[$val['id']]['up_exam_start'];

                            $data[$k]['end'] = $upExamDatas[$val['id']]['up_exam_end'];

                            if ($now > $upExamDatas[$val['id']]['up_exam_end']) {
                                $statusExam = 4;    # 过期
                            } elseif ($now <= $upExamDatas[$val['id']]['up_exam_start']) {
                                $statusExam = 0;    # 未开始
                            } else {
                                $statusExam = 2;
                            }

                            $up = 1;
                        } else {
                            # 考试未结束
                            # 当考试次数大于等于重考次数 再走下去
                            if ($userInfo[$val['id']]['status'] >=1 && $userInfo[$val['id']]['status'] <=4
                                && $userInfo[$val['id']]['exam_num'] >= 1
                                && $userInfo[$val['id']]['exam_num'] >= $exam_num) {
                                $val['exam_num'] = $exam_num;

                                $status_exam = self::examStatus(
                                    $val,
                                    $upExamDatas[$val['id']],
                                    $userInfo[$val['id']],
                                    2
                                );

                                $examConf = 1;
                            } elseif ($userInfo[$val['id']]['exam_num'] < $exam_num) {
                                if ($userInfo[$val['id']]['exam_num'] == 0 && $val['start'] < $now) {
                                    $statusExam = 1;
                                } else {
                                    $statusExam = 2;
                                }
                            }
                        }

                        if ($userInfo[$val['id']]['status'] == 3 || $examConf == 1) {
                            $data[$k]['start'] = $upExamDatas[$val['id']]['up_exam_start'];

                            $data[$k]['end'] = $upExamDatas[$val['id']]['up_exam_end'];

                            $data[$k]['exam_time'] = $upExamDatas[$val['id']]['up_exam_time'];

                            $data[$k]['exam_num'] = $upExamDatas[$val['id']]['up_exam_num'] ?
                                ($upExamDatas[$val['id']]['up_exam_num']) : '';
                        }

                        if ($status_exam == 1001) {
                            $data[$k]['status'] = 0;
                        } elseif ($status_exam == 1002) {
                            $data[$k]['status'] = 2;

                            if ($userInfo[$val['id']]['exam_num'] == 0  && $userInfo[$val['id']]['up_exam'] == 0) {
                                $data[$k]['status'] = 1;
                            }

                            if ($up == 1) {   # 在补考
                                $data[$k]['exam_num'] = $upExamDatas[$val['id']]['up_exam_num'];

                                $data[$k]['exam_time'] = $upExamDatas[$val['id']]['up_exam_time'];
                            } else {
                                $data[$k]['exam_num'] = $val['exam_num'];
                            }
                        } else {
                            $data[$k]['status'] = $status_exam ? $status_exam : $statusExam;    # 禁止再考
                        }
                    } else {    # Todo  没有补考
                        $data[$k]['exam_num'] = $exam_num;

                        if ($val['start'] > $now) {
                            $data[$k]['status'] = $status_exam;
                            continue;
                        }

                        # 无补考
                        if ($val['end'] && $val['end'] < $now) {
                            $data[$k]['status'] = 4;    # 已过期
                        } elseif (!$val['end']) {
                            $data[$k]['status'] = 1;

                            # 无过期时间 取判断考试次数
                            if ($userInfo[$val['id']]['status'] >1 || $userInfo[$val['id']]['exam_num'] > 0) {
                                $status_exam = self::examStatus($val, [], $userInfo[$val['id']], 3);

                                $data[$k]['status'] = $status_exam;
                            }
                        }
                    }
                }
            }
        }

        # 0=未开始 1=进行中 2【未通过有补考】【5未通过结束】= 未通过 3已通过 4 已过期
        # 处理计划类考试
        $HandleData = self::handleLearnPlanExamData($data);

        return [$page, $HandleData];
    }


    /**
     * 处理关联计划的考试
     * @param array $datas
     * @return mixed
     */
    private static function handleLearnPlanExamData(array $datas)
    {
        $user = Helper::getLoginUser();

        # Todo 1普通考试这里不计算   **  2计划-自动固定入职 3计划-自动 4计划-手动   **
        foreach ($datas as &$data) {
            # Todo 关联计划的
            $status = $plan_status = 1;

            $lastDay = $PLanInfo = $PLanStatus = '';

            if ($data['exam_type'] > 1 && $data['exam_type'] < 5) {
                $ExamInfo = ComplianceExamRepository::getExamStatus($data['id'], $user->yyid);

                if (!$ExamInfo) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "计划考试不存在");
                }

                if ($ExamInfo['status'] == 4) {
                    $status = 5;    # 未通过结束
                } elseif ($ExamInfo['status'] == 2) {
                    $status = 3;    # 已通过
                } elseif ($ExamInfo['status'] == 3) {
                    # 判断是否无限时 无限时 = 2 可继续考试
                    $status = 5;    # 因为没有补考 所以不用再取判断是否可以继续考试
                }

                $PLanInfo = ComplianceExamRepository::getLearnPlans($data['exam_type'], $data['id'], $user->uid);

                if (!$PLanInfo) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "计划不存在");
                }

                # 获取计划详细信息 ， 用户 入职时间
//                    $PLanAndUserInfo = ComplianceExamRepository::getPLanAndUserInfo($PLanInfo['plan_id'], $user->uid);

                $PLanStatus = ComplianceExamRepository::getUserLearnPLanStatus($PLanInfo['plan_id'], $user->uid);

                if (!$PLanStatus) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户计划资料有误");
                }

                #  3已通过 4 已过期 5未通过结束 【没有补考】
                if ($status >= 2) {
                    $data['status'] = $status;
                    $data['plan_status'] = 6;
                    $data['plan_name'] = isset($PLanInfo['name']) ? $PLanInfo['name'] : '';
                    $data['date_type'] = isset($PLanInfo['date_type']) ? $PLanInfo['date_type'] : '';
                    $data['dt_off'] = isset($PLanInfo['dt_off']) ? $PLanInfo['dt_off'] : '';
                    $data['dt_on'] = isset($PLanInfo['dt_on']) ? $PLanInfo['dt_on'] : '';
                    continue;
                }

                if ($data['exam_type'] == 2) {    # 计划时间内 完成计划和考试
                    /**
                     *  1、所有状态起始为 进行中...
                     *  2、获取考试结果表的状态 examination_results, status = 2 是否通过
                     *  3、获取该用户计划发送短信表 learning_plan_msg_node 中的开始和结束日期 并计算是否过期
                     *  4、若已过期，但考试结果表中依然未通过 则考试状态 "未通过！"
                     */
                    if ($ExamInfo['status'] == 1 || $ExamInfo['status'] == 2) { # 进行中
                        if ($PLanInfo['date_type'] == 1) {    # 当 计划是自动【入职】
                            # 判断是否未开始，入职时间加上入职后n天开始=实际计划开始时间
                            $sdt = strtotime(
                                '+'.$PLanInfo['dt_on'].'day',
                                strtotime($PLanInfo['employment_date'])
                            );

                            # 1进行中2未开始3完成4过期
                            if ($sdt > time()) {
                                $status = 'NotStarted';
                            } else {
                                # 判断是否过期 计划最后一天 小于当前时间 = 过期
                                if (!$PLanInfo['last_day']) {    # 若不存在 =  未勾选推送时间
                                    # 入职时间 + 入职后 N 天 + N天结束 = 结束时间
                                    $day = $PLanInfo['dt_on'] + $PLanInfo['dt_off'];

                                    $lastDay = strtotime('+' . $day . 'day', strtotime($PLanInfo['employment_date']));
                                } else {
                                    $lastDay = strtotime($PLanInfo['last_day']);
                                }
                            }
                        } elseif ($PLanInfo['date_type'] == 2) {  # 手动点击开始学习
                            if ($PLanInfo['sdt']) {   # 若手动开始的 有开始学习的时间  考试进行中 否则 是未开始 【 默认就是进行中状态 】
                                if ($PLanInfo['last_day']) {
                                    $lastDay = strtotime($PLanInfo['last_day']);
                                } else {
                                    $day = $PLanInfo['dt_off'];

                                    $lastDay = strtotime('+' . $day . 'day', strtotime($PLanInfo['sdt']));
                                }
                            } else {
                                $status = 0;
                            }
                        } elseif ($PLanInfo['date_type'] == 3) { # 固定日期 N - N
                            $lastDay = strtotime($PLanInfo['dt_off']);

                            if (strtotime($PLanInfo['dt_on']) < time()) {  # 计划开始时间小于当前时间  考试未开始
                                $status = 0;
                            }
                        }

                        # 当状态为未开始时候
                        if ($status != 'NotStarted') {
                            if ($lastDay && $lastDay < time()) {
                                $status = 4;
                            } else {
                                if ($PLanInfo['date_type'] != 4) {
                                    $status = 0;
                                }
                            }

                            # 判断计划是否完成
                            if ($PLanStatus['plan_status'] == 6) {
                                $plan_status = 6;   # 计划完成

                                $status = 1;
                            }
                        }
                    }
                } elseif ($data['exam_type'] == 3) {  # 计划完成后 plan_days 天内 完成考试
                    /**
                     *  1、所有状态起始为 未开始！
                     *  2、获取考试结果表的状态 examination_results, status = 2 是否通过
                     *  3、从计划完成时间算起 plan_days 天内 完成考试
                     *  4、若已过期，但考试结果表中依然未通过 则考试状态 "未通过！"
                     */

                    if (!$data['plan_days']) {
                        throw new BusinessException(ErrorCode::BUSINESS_ERROR, "计划考试结束时间不存在");
                    }

                    $plan_days = $data['plan_days'];

                    if ($ExamInfo['status'] == 1 || $ExamInfo['status'] == 2) { # 进行中
                        # 判读计划是否完成 若完成 根据完成时间 加上考试配置结束时间 是否大于当前时间 判断过期或进行中
                        if ($PLanStatus['plan_status'] == 6) {
                            $plan_status = 6;   # 计划完成

                            if (strtotime(
                                '+' . $plan_days . ' day',
                                strtotime($PLanStatus['finish_day'])
                            ) > time()) {  # 当前时间大于学习计划时间 考试开始
                                $status = 1;    # 已完成计划 考试进行中...
                            } else {
                                $status = 4;    # 过期
                            }
                        } else {
                            $status = 0;
                        }
                    }
                } elseif ($data['exam_type'] == 4) {  # 完成计划后&& 结束 plan_days 内开启 并 完成考试
                    /**
                     *  1、所有状态起始为 未开始！
                     *  2、获取考试结果表的状态 examination_results, status = 2 是否通过
                     *  3、从计划完成时间算起 plan_days 天内开启并完成考试
                     *  4、若已过期，但考试结果表中依然未通过 则考试状态 "未通过！"
                     */

                    if (!$data['plan_days']) {
                        throw new BusinessException(ErrorCode::BUSINESS_ERROR, "计划考试结束时间不存在");
                    }

                    $plan_days = $data['plan_days'];

                    if ($ExamInfo['status'] == 1 || $ExamInfo['status'] == 2) { # 进行中
                        # 计算 自动 手动 固定 的计划结束时间
                        if ($PLanInfo['date_type'] == 1) {
                            if ($PLanInfo['last_day']) {
                                $lastDay = $PLanInfo['last_day'];
                            } else {
                                $day = $PLanInfo['dt_on'] + $PLanInfo['dt_off'];

                                $lastDay = strtotime('+' . $day . 'day', strtotime($PLanInfo['employment_date']));
                            }
                        } elseif ($PLanInfo['date_type'] == 2) {
                            if ($PLanInfo['sdt']) {   # 若手动开始的 有开始学习的时间  考试进行中 否则 是未开始 【 默认就是进行中状态 】
                                if ($PLanInfo['last_day']) {
                                    $lastDay = $PLanInfo['last_day'];
                                } else {
                                    $day = $PLanInfo['dt_off'];

                                    $lastDay = strtotime('+' . $day . 'day', strtotime($PLanInfo['sdt']));
                                }
                            }
                        } elseif ($PLanInfo['date_type'] == 3) {
                            $lastDay = strtotime($PLanInfo['dt_off']);
                        }   # 无限时计划 不会指定给 exam_type = 4 的考试

                        # 判读计划是否完成 若完成 根据完成时间 加上考试配置结束时间 是否大于当前时间 判断过期或进行中
                        if ($lastDay < time()) {  # 必须是计划学习结束后
                            if ($PLanStatus['plan_status'] == 6) {
                                $plan_status = 6;   # 计划完成
                                if (strtotime(
                                    '+' . $plan_days . ' day',
                                    strtotime($PLanStatus['finish_day'])
                                ) > time()) {  # 当前时间大于学习计划时间 考试开始
                                    $status = 1;    # 已完成计划 考试进行中...
                                } else {
                                    $status = 4;    # 过期
                                }
                            } else {
                                $status = 0;
                            }
                        } else {
                            if ($PLanStatus['plan_status'] == 6) {
                                $plan_status = 6;
                            }

                            $status = 0;
                        }
                    }
                }


                if ($ExamInfo['exam_num'] >= 1 && $status == 1) {
                    $status = 2;   # 重考
                }
                $data['status'] = $status;
                $data['plan_status'] = $plan_status;
                $data['plan_name'] = isset($PLanInfo['name']) ? $PLanInfo['name'] : '';
                $data['date_type'] = isset($PLanInfo['date_type']) ? $PLanInfo['date_type'] : '';
                $data['dt_off'] = isset($PLanInfo['dt_off']) ? $PLanInfo['dt_off'] : '';
                $data['dt_on'] = isset($PLanInfo['dt_on']) ? $PLanInfo['dt_on'] : '';
            }
        }

        return $datas;
    }

    private static function transferStatus(ComplianceExam $exam, $status = 0, $statusMap = []): array
    {

            //0未开始 1进行中 2未通过 3已通过 4已过期
        $now = date('Y-m-d H:i:s');
        if ($exam->exam_start && $now < $exam->exam_start) {
            return [0, ''];
        }
        if ($status != 0) {
            return [$status, ''];
        }

        if (!isset($statusMap[$exam->yyid])) {
            return $exam->exam_end && $exam->exam_end <= $now ? [4, ''] : [1, ''];
        }
        $st = (int)$statusMap[$exam->yyid]['status'];
        return $st == 1 ? [3, (string)$statusMap[$exam->yyid]['pass_time']] : [2, ''];
    }

    private static function filterUserExamsInfos(Users $users, array $examYYIDS): array
    {
        $maps = ComplianceUserExamRepository::getUserExamLastRecord($users, $examYYIDS);
        return $maps->keyBy('exam_yyid')->all();
    }

    /**
     * 获取用户所有考试
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return LengthAwarePaginator|null
     */
    private static function getAllExams(string $keywords = "", int $current = 1, int $limit = 10): ?LengthAwarePaginator
    {
        $user = Helper::getLoginUser();

        $resp = RepsServeScopePRepository::getUserServeProducts($user);
        $productYYIDS = $resp->pluck('yyid')->unique()->all();

        return ComplianceExamRepository::getUserExamsWithPage($user, $productYYIDS, $keywords, $current, $limit);
    }

    /**
     * 获取用户可用考试列表
     * @param Users $user
     * @return array
     * @throws InvalidArgumentException
     */
    private static function getUserValidExamYYIDS(Users $user): array
    {
         //已通过(未通过
        //获取用户服务产品
        $resp = RepsServeScopePRepository::getUserServeProducts($user);
        $productYYIDS = $resp->pluck('yyid')->unique()->all();

        //用户本身考试不会特别多， 将其拆分成单独的查询
        $resp = ComplianceExamUserRepository::getUserExams($user, $productYYIDS);
        if ($resp->isEmpty()) {
            return [];
        }
        return $resp->pluck('exam_yyid')->all();
    }


    /**
     * 获取用户进行中的考试
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return null|LengthAwarePaginator
     * @throws BusinessException
     * @throws InvalidArgumentException
     */
    private static function getProcessingExams(
        string $keywords = "",
        int $current = 1,
        $limit = 10
    ): ?LengthAwarePaginator {
        //进行中: 开始/结束时间不为空且结束时间大于当前时间或结束时间为null, 且考试参与用户有当前考试|用户服务产品在考试参与产品人员里面 且无用户参数通过记录
        $user = Helper::getLoginUser();

        //获取用户所有的考试
        $validExamYYIDS = self::getUserValidExamYYIDS($user);
        if (empty($validExamYYIDS)) {
            return null;
        }
        //过滤出已经未参与的考试(这里可能存在数据过大的问题)
        $resp = ComplianceUserExamRepository::filterUserPartedExams($user);
        $participateExamYYIDS = $resp->pluck('exam_yyid')->all();
        $valid = array_diff($validExamYYIDS, $participateExamYYIDS);
        if (empty($valid)) {
            return null;
        }

        //使用范围获取考试列表, 指定未过期
        return ComplianceExamRepository::filterExams($keywords, $current, $limit, $valid, false, false);
    }

    /**
     * 获取已经过期的考试(未参与过期,已参与但未通过的)
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return null|LengthAwarePaginator
     * @throws BusinessException
     * @throws InvalidArgumentException
     */
    private static function getExpireExams(
        string $keywords = "",
        int $current = 1,
        int $limit = 10
    ): ?LengthAwarePaginator {
        //已过期: 开始/结束时间均不为null且结束时间小于等于当前时间(和是否参与无关), 且考试参与用户有当前考试|用户服务产品在考试参与产品人员里面
        $user = Helper::getLoginUser();

        //获取用户所有的考试
        $validExamYYIDS = self::getUserValidExamYYIDS($user);
        if (empty($validExamYYIDS)) {
            return null;
        }

        //过滤出已经未参与且过过期的考试
        $resp = ComplianceUserExamRepository::filterUserPartedExams($user, true);
        $participatedExamYYIDS = $resp->pluck('exam_yyid')->all();
        $valid = array_diff($validExamYYIDS, $participatedExamYYIDS);
        if (empty($valid)) {
            return null;
        }

        //使用范围获取考试列表, 指定未过期
        return ComplianceExamRepository::filterExams($keywords, $current, $limit, $valid, false, true);
    }

    /**
     * 获取用户已经通过的考试列表
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @param bool $isPassed
     * @return null|LengthAwarePaginator
     */
    private static function getPartedExams(
        $keywords = "",
        $current = 1,
        $limit = 10,
        $isPassed = true
    ): ?LengthAwarePaginator {
        //已通过: 开始/结束时间不为空且结束时间大于当前时间或结束时间为null， 且参与记录里面有用户通过考试记录
        $user = Helper::getLoginUser();

        $resp = ComplianceUserExamRepository::filterExamsByLastStatus($user, $isPassed);
        if ($resp->isEmpty()) {
            return null;
        }
        $valid = $resp->pluck('exam_yyid')->all();

        return ComplianceExamRepository::filterExams($keywords, $current, $limit, $valid);
    }

    private static function checkUserOwnExam(Users $users, ComplianceExam $exam): bool
    {
        $serveProduct = RepsServeScopePRepository::getUserServeProducts($users);
        $productYYIDS = $serveProduct->pluck('yyid')->all();

        return ComplianceExamUserRepository::checkUserExam($users, $exam, $productYYIDS);
    }

    /**
     * 获取考试信息
     * @param int $id
     * @return array
     */
    public static function getExamInfo(int $id): array
    {
        $exam = ComplianceExamRepository::getById($id);
        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在或未开放");
        }

        //获取最后一次参与记录
        $user = Helper::getLoginUser();
        $last = ComplianceUserExamRepository::getExamLast($user, $exam);
        $status = 0;
        $last_created_time = '';
        if ($last) {
            $last_arr= $last->toArray();
            $last_created_time = $last_arr['created_time'];
            $status = $last->status == DataStatus::REGULAR ? 3 : 2;
        } else {
            //没有参与记录需要判断用户是否有参与权限
            $owned = self::checkUserOwnExam($user, $exam);
            if (!$owned) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在或未分配");
            }
            $now = date('Y-m-d H:i:s');
            $status = $exam->exam_start > $now ? 0 : (
                $exam->exam_end && $exam->exam_end < $now ? 4 : 1);
        }

        //获取用户协议签署记录
        $userAgree = null;
        if ($exam->agreement_yyid) {
            $userAgree = ComplianceUserAgreementRepository::getUserExamAgreement($exam, $user);
        }

        if ($status != 3 && $exam->exam_type == 1) {
        # 当考试过期时查询补考 4 过期 2 未通过
//        if($status == 4 || $status == 2) {
            $now = date('Y-m-d H:i:s');
            # 查询该考试是否有补考
            $userInfo = ExaminationResultRepository::getUserStatus([$id], $user->yyid);
            $upExamData = UpExamDateRepository::getUpExamDate([$id]);
            if (!$userInfo->isEmpty()) {
                $userInfo = $userInfo->toArray();
                # 补考 未考 未通过 过期ƒ
                # 当考试次数大于等于重考次数 再走下去
                if ($userInfo[0]['status'] >=1 && $userInfo[0]['status'] <=4
                    && $userInfo[0]['exam_num'] >= 1 && $userInfo[0]['exam_num'] >= ($exam->exam_num)) {
                    if ($exam->exam_end && $exam->exam_end < $now) {  # 结束
                        if (!$upExamData->isEmpty()) {
                            $upExamData = $upExamData->toArray();
                            # 有补考时间 查询用户是否需要补考
                               $status_exam = self::examStatus($exam->toArray(), $upExamData[0], $userInfo[0], 1);
                               $last = true;

                            if ($status_exam && $status_exam != 5) {
                                if ($now < $upExamData[0]['up_exam_start']) {
                                    $status = 0;     # 未开始
                                    $last = false;
                                } else {
                                    $status = 1;
                                    $last = false;
                                    if ($now > $upExamData[0]['up_exam_end']) {
                                        $status = 4;    # 过期
                                    }
                                    if ($userInfo[0]['status'] >= 2) {
                                        $status = 2;
                                        $last = true;
                                    }
                                }
                            } elseif ($status_exam == 5) {
                                $status = $status_exam;
                                $last = true;
                            }
                               $exam->exam_start = $upExamData[0]['up_exam_start'];
                               $exam->exam_end = $upExamData[0]['up_exam_end'];
                               $exam->exam_time = $upExamData[0]['up_exam_time'];
                               $exam->exam_num = $upExamData[0]['up_exam_num'];
                        } else {
                            $status = 4;
 //                           if ($status == 2) { # 当未通过时 且没补考
 //                               $last = true;
 //                               if ($exam->exam_end && $exam->exam_end > $now) {
 //                                   $status = 2;
 //                               } elseif ($exam->exam_end && $exam->exam_end < $now) {
 //                                   $status = 4;
 //                                   $last = false;
 //                               } elseif (!$exam->exam_end) {
 //                                   $status = 2;
 //                               }
 //                           }
                        }
                    } elseif ($exam->exam_end && $exam->exam_end > $now) {  # 未过期
                       # 考试未过期 考试时间不动 若有考试次数 判断考试次数
                        $upExamDatas = [];
                        if (!$upExamData->isEmpty()) {
                            $upExamData = $upExamData->toArray();
                            $upExamDatas = $upExamData[0];
                        }
                        $status_exam = self::examStatus($exam->toArray(), $upExamDatas, $userInfo[0], 2);
                        if ($status_exam == 1001) {
                            $status = 0;
                            $last = $status_exam?true:false;
                            $exam->exam_start = $upExamData[0]['up_exam_start'];
                            $exam->exam_end = $upExamData[0]['up_exam_end'];
                            $exam->exam_time = $upExamData[0]['up_exam_time'];
                            $exam->exam_num = $upExamData[0]['up_exam_num'];
                        } else {
                            $status = $status_exam?$status_exam:2;
                            $last = ($status>=2)?true:false;
                        }
                    } else {
                        # 无过期时间
                        $status_exam = 0;
                        if ($exam->exam_num && $exam->exam_num > 0) {
                            $status_exam = self::examStatus($exam->toArray(), $upExamData[0], $userInfo[0], 3);
                        }
                        $status = $status_exam?$status_exam:2;
                        $last = ($status>=2)?true:false;
                    }
                } else { # 未考完/状态
                    # 是否过期 已过期
                    if ($exam->exam_end && $exam->exam_end < $now) {
                        # 是否有补考
                        if (!$upExamData->isEmpty()) {
                            $upExamData = $upExamData->toArray();
                            # 有补考时间 查询用户是否需要补考
                            $status_exam = self::examStatus($exam->toArray(), $upExamData[0], $userInfo[0], 1);
                            # 过期 有考过
                            $last = false;
                            if ($userInfo[0]['exam_num'] > 0 || $userInfo[0]['up_exam'] > 0) {
                                $last = true;
                            }

                            if ($status_exam == 1001) {
                                $status = 0;
                            } elseif ($status_exam == 1002) {
                                $status = 2;
                                if ($userInfo[0]['exam_num'] == 0 && $userInfo[0]['up_exam'] == 0) {
                                    $status = 1;
                                }
                            } else {
                                $status = $status_exam?$status_exam:4;
                            }
                            if ($status_exam > 0) {
                                $exam->exam_start = $upExamData[0]['up_exam_start'];
                                $exam->exam_end = $upExamData[0]['up_exam_end'];
                                $exam->exam_time = $upExamData[0]['up_exam_time'];
                                $exam->exam_num = $upExamData[0]['up_exam_num'];
                            }
                        }
                    } else {
                        if ($userInfo[0]['status'] >1 || $userInfo[0]['exam_num'] > 0) {
                            $status_exam = self::examStatus($exam->toArray(), [], $userInfo[0], 3);
                            $status = $status_exam?$status_exam:2;
                            $last = ($status>=2)?true:false;
                        }
                    }
                }
            }
        }


        $dt_off = $date_type = $dt_on = $plan_status = $plan_name = $plan_id = '';
        # 若有关联计划
        if ($exam->exam_type > 1) {
            $LearningPlanInfo = ComplianceExamRepository::getLearnPlanInfo($exam->id, $user->uid);
            if ($LearningPlanInfo) {
                $dt_off = $LearningPlanInfo['dt_off'];
                $dt_on = $LearningPlanInfo['dt_on'];
                $date_type = $LearningPlanInfo['date_type'];
                $plan_name = $LearningPlanInfo['name'];
                $plan_id = $LearningPlanInfo['id'];
                if (strpos($LearningPlanInfo['plan_status'], '6') !== false) {
                    $plan_status = 6;
                }
            }
        }

        $info = [
            'name' => $exam->name,
            'desc' => $exam->introduce,
            'status' => $status,
            'last_time' => $last ? $last_created_time : '',
            'with_agreement' => $status == 3 && $exam->agreement_yyid ? 1 : 0,
            'is_sign' => $status == 3 && $userAgree ? 1 : 0,
            'sign_time' => $status == 3 && $userAgree ? (string)$userAgree->created_time : '',
            'is_time_unlimited' => $exam->exam_end ? 0 : 1,
            'start' => (string)$exam->exam_start,
            'end' => (string)$exam->exam_end,
            'exam_time' => (string)$exam->exam_time,
            'exam_num' => (string)$exam->exam_num,
            'is_review' => (int)$exam->is_review,
            'dt_off' => $dt_off,
            'dt_on' => $dt_on,
            'date_type' => $date_type,
            'plan_status' => $plan_status,
            'exam_type' => $exam->exam_type,
            'plan_name' => $plan_name,
            'plan_days' => $exam->plan_days,
            'plan_id' => $plan_id,
            'id' => $exam->id,
        ];
        if ($info['exam_type'] > 1) {
            $HandleInfo = self::handleLearnPlanExamData([$info]);

            return $HandleInfo[0];
        } else {
            return $info;
        }
    }

    private static function examStatus($exam, $upExamData, $userInfo, $t)
    {
        $now = date('Y-m-d H:i:s');
        $up_exam_num = 0;
        if ($upExamData) {
            $up_exam_num = $upExamData['up_exam_num'];
        }
        # 考试次数
        $status = false;
        if ($t == 1) {    # 考试结束
            if ($upExamData) {
                if ($upExamData['up_exam_start'] < $now && $upExamData['up_exam_end'] > $now) {   # 补考中
                    if ($up_exam_num > 0 && $userInfo['up_exam'] == $up_exam_num) {
                        $status = 5;
                    } elseif ($userInfo['up_exam'] < $up_exam_num) {
                        $status = 1002; # 继续考试
                    } elseif (!$up_exam_num) {
                        # 考试过期 切补考无限次数
                        $status = 1002; # 继续考试
                    }
                } elseif ($upExamData['up_exam_start'] > $now) {
                    $status = 1001;    # 补考未开始
                } elseif ($upExamData['up_exam_end'] < $now) {
                    $status = 4;        # 补考结束
                }
            } else {
                $status = 4;
            }
        } elseif ($t == 2) {  # 考试未结束
            if ($upExamData) {
                if ($upExamData['up_exam_start'] < $now && $upExamData['up_exam_end'] > $now) { # 补考中
                    # 若考试+ 补考期间 考试+补考次数用完
                    if ($userInfo['exam_num'] == $exam['exam_num'] &&
                        $userInfo['up_exam'] == $up_exam_num) {
                        $status = 5;
                    }  if ($exam['exam_num'] > 0 && $userInfo['exam_num'] == ($exam['exam_num'])) {
                        $status = 5;
                    } elseif ($up_exam_num > 0 && $userInfo['up_exam'] == $up_exam_num) {
                        $status = 5;
                    }
                } elseif ($upExamData['up_exam_start'] > $now) {
                    $status = 1001;    # 补考未开始
                } elseif ($upExamData['up_exam_end'] < $now) {
                    $status = 4;    # 补考结束
                }
            } else {
                if ($exam['exam_num'] > 0 && $userInfo['exam_num'] == ($exam['exam_num'])) {
                    $status = 5;
                }
            }
        } elseif ($t == 3) {  # 考试未结束 无补考
            $status = 5;

            if ($exam['exam_num'] > 0) {
                if ($userInfo['exam_num'] == ($exam['exam_num'])) {
                    $status = 5;
                }

                if ($userInfo['exam_num'] < ($exam['exam_num'])) {
                    $status = 2;
                }
            }
        } elseif ($t == 10) {  # 无限制考试时间【不存在补考】
            if ($exam['exam_num'] > 0 && $userInfo['exam_num'] == ($exam['exam_num'])) {
                $status = 5;
            }
        }



        return $status;
    }
    /**
     * 开始考试
     * @param int $id
     * @return array
     */
    public static function startExam(int $id): array
    {
        $user = Helper::getLoginUser();

        $exam = ComplianceExamRepository::getById($id);

        # 验证用户是否存在excel导入表中
        # 【后期统一该如grpc传入，统一excel导入人员和指定人员走rpc 防止俩表信息不一致出现考试人员拿不到考试分数等信息】
        $ExamInfo = ComplianceExamRepository::getExamStatus($id, $user->yyid);

        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }
        if (!$ExamInfo) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试用户信息有误");
        }
        if (!$exam->test_paper_yyid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试尚未配置完成，无法参与");
        }
        $now = date('Y-m-d H:i:s');
        if ($exam->exam_start > $now) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试尚未开始，无法参与");
        }

        //检查用户是否分配了该考试
        if (!self::checkUserOwnExam($user, $exam)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在或用户未分配该考试");
        }

        //获取最新考试记录
        $last = ComplianceUserExamRepository::getExamLast($user, $exam);

        if ($last && $last->isPassed()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试已通过，无法重复答题");
        }

        //获取考试第一题目
        //生成一个串
//        $requestId=  uniqid();
        $requestId=  $exam->yyid.'-'.$user->uid;  # 生成固定 字符串
        $questionItemIds = self::getExamQuestionIds($exam, 1);

        //添加超过2小时未答完的记录(垃圾数据)
        go(function () use ($exam, $user, $requestId) {
            self::clearOldRequest($exam, $user, $requestId);
        });
        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $exam);
        # 若有考试时间 缓存考试时间 每次初始化 提交剩余考试时间
        $upExamData = UpExamDateRepository::getUpExamDate([$exam->id]);
        if ($exam->exam_time || !$upExamData->isEmpty()) {
            $examTime = $exam->exam_time;
            $useTimen = '';
            # 获取缓存时间
            $getcCacheExamTime = ExamCacheService::getCacheExamTime($cacheRequestId);
            if ($getcCacheExamTime) {
                $time = explode('-', $getcCacheExamTime);
                $useTime = $time[0]+($time[1]*60);
                if ($useTime <= time()) {
                    $useTimen = '-1';
                } else {
                    $useTimen = ($useTime - time());
                }
                $useTimen = round($useTimen, 1);
                if ($useTimen<=0) {
                    # 自动提交
                    self::finishExam($exam->id, $requestId);
                    $useTimen = '-2';
                }
            } else {
                # 配置缓存
                if ($exam->exam_end) {
                    if ($exam->exam_end > date('Y-m-d H:i:s')) {
                        if ($examTime) {
                            ExamCacheService::cacheExamTime($examTime, $cacheRequestId);
                            $useTimen = $examTime * 60;
                        } else {
                            $useTimen = '-1';
                        }
                    } else {
                        # 过期
                        if (!$upExamData->isEmpty()) {
                            $upExamDatas = $upExamData->toArray();
                            $upExamDatas = array_column($upExamDatas, null, 'exam_id');
                            if ($upExamDatas[$exam->id]['up_exam_time']) {
                                ExamCacheService::cacheExamTime(
                                    $upExamDatas[$exam->id]['up_exam_time'],
                                    $cacheRequestId
                                );
                                $useTimen = $upExamDatas[$exam->id]['up_exam_time'] * 60;
                            } else {
                                $useTimen = '-1';
                            }
                        } else {  # 无补考 不限时
                            $useTimen = '-1';
                        }
                    }
                } else {
                    # 无限时间
                    if ($examTime) {    # 考试答题时间
                        ExamCacheService::cacheExamTime($examTime, $cacheRequestId);
                        $useTimen = $examTime * 60;
                    } else {
                        $useTimen = '-1';
                    }
                }
            }
        } else {
            $useTimen = '-1';
        }

        # 判断考试/补考结束时间是否存在 并且大于当前时间
        # 若考试/补考结束时间存在 并且有考试时长限制 比较 考试/补考时间距离当前时间 是否大于考试限制时长【单位：秒】 取最小值
        # 若考试无限制时长 取 考试结束时间至当前时间时长
        if ($exam->exam_end && $exam->exam_end > date('Y-m-d H:i:s')) {
            $examEndTime = strtotime($exam->exam_end) - time();
            if ($useTimen > 0) { # 考试限制时间
                if ($useTimen > $examEndTime) {
                    $useTimen = $examEndTime;
                }
            } else {
                $useTimen = $examEndTime;
            }
        } else {
            # 补考
            if (!$upExamData->isEmpty()) {
                $upExamDatas = $upExamData->toArray();
                if ($upExamDatas[$exam->id]['up_exam_end']
                    && $upExamDatas[$exam->id]['up_exam_end'] > date('Y-m-d H:i:s')) {
                    $examEndTime = strtotime($upExamDatas[$exam->id]['up_exam_end']) - time();
                    # 考试限制时间
                    if ($upExamDatas[$exam->id]['up_exam_time']) {
                        if ($useTimen > $examEndTime) {
                            $useTimen = $examEndTime;
                        }
                    } else {
                        $useTimen = $examEndTime;
                    }
                }
            }
        }

        //缓存试题
        sort($questionItemIds);
        $info = ExamCacheService::cacheExamQuestionList($cacheRequestId, $questionItemIds, 7200);

        # 断点续考
        $redis = Helper::getRedis();
        $answerKey = ExamCacheService::buildExamAnswerKey($cacheRequestId);
        $answerKeys = $redis->hKeys($answerKey);    # 已答题目的所有key
        $itemId = $info[0];
        $preItemId = '';
        $is_sub = '-1';
        sort($answerKeys);
        if ($answerKeys) {
            $QuestionKey = ExamCacheService::buildQuestionCacheKey($cacheRequestId);
            $QuestionData = $redis->zRange($QuestionKey, 0, -1);
            $answerItemId = last($answerKeys);
            $nextQuestion = ExamCacheService::getNextQuestionId($cacheRequestId, (int) $answerItemId);
            $itemId = $nextQuestion[0];
            $preItemId = $answerItemId;
            $item_id = last($QuestionData);
            if ($item_id == $itemId) {
                $is_sub = 1;
            }
        }

        return [$itemId, count($questionItemIds), $requestId, $useTimen, $preItemId, $is_sub];
    }

    private static function clearOldRequest(ComplianceExam $exam, Users $users, $newRequestID)
    {
        ExamCacheService::clearOldRequests($exam->id, $users->uid, $newRequestID);
    }

    private static function buildCacheRequestId($requestId, Users $users, ComplianceExam $exam): string
    {
        return sprintf("%d:%d:%s", $exam->id, $users->uid, $requestId);
    }

    /**
     * 根据题库yyid获取所有的试题yyid
     * @param array $qs
     * @return array
     */
    private static function getQuestionItemsByQuestions(array $qs, $examItem): array
    {
        //根据yyid过滤有效题库(处理下架不一致)
        $resp = ComplianceQuestionRepository::getQuestionsByYYIDS($qs);
        if ($resp->isEmpty()) {
            return [];
        }
        $qs = $resp->pluck('yyid')->all();

        //根据题库获取所有题目
        $qi  = ComplianceQuestionItemsRepository::getItemsByQuestionYYIDS($qs, $examItem);
        $ids = [];
        /** @var ComplianceQuestionItem $item */
        foreach ($qi as $item) {
            array_push($ids, $item['id']);
        }
        return $ids;
    }

    /**
     * 获取试卷所有试题id列表
     * @param ComplianceTestPaper $paper
     * @return array
     */
    public static function getQuestionItemsByTestPaper(ComplianceTestPaper $paper, $testPaperConf): array
    {
        /** @var Collection $items */
        $items = $paper->testPaperItems;
        $ids = [];
        if (!$paper->isTestPaperQuestion()) {
            //题目出题
            $questionYYID = $items->reduce(function ($carry, ComplianceTestPaperItem $item) {
                if ($item->item_yyid) {
                    $carry[] = $item->item_yyid;
                }
                return $carry;
            }, []);
            //根据题目id获取有效题目
            $resp = ComplianceQuestionItemsRepository::getItemsByYYIDS($questionYYID);
            /** @var ComplianceQuestionItem $item */
            foreach ($resp as $item) {
                array_push($ids, $item->id);
            }

            # 随机出题 随机指定数量题目
            if (isset($testPaperConf['item_num'])) {
                shuffle($ids);
                $ids = array_slice($ids, 0, $testPaperConf['item_num']);
            }
        } else {
            # 随机出题  题库出题 gdfgd
            if (isset($testPaperConf['examItem'])) {
                $examItem = array_column($testPaperConf['examItem'], null, 'yyid');
                $qs = array_column($testPaperConf['examItem'], 'yyid');
                $ids = self::getQuestionItemsByQuestions($qs, $examItem);
            } else {
                //题库出题
                $qs = $items->reduce(function ($carry, ComplianceTestPaperItem $item) {
                    if ($item->question_yyid) {
                        $carry[] = $item->question_yyid;
                    }
                    return $carry;
                }, []);
                if (!empty($qs)) {
                    $ids = self::getQuestionItemsByQuestions($qs, []);
                }
            }
        }
        return $ids;
    }

    /**
     * 获取考试试题
     * @param int $examId
     * @param int $questionId
     * @param string $requestId
     * @return array
     */
    public static function getQuestion(int $examId, int $questionId, string $requestId): array
    {
        $user = Helper::getLoginUser();
        $exam = ComplianceExamRepository::getById($examId);
        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        //检查缓存中试题
        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $exam);
        ExamCacheService::checkRequestAndQuestion($cacheRequestId, $questionId);

        //获取试题及选项
        $questionItem = ComplianceQuestionItemsRepository::getItemsById($questionId);
        if (!$questionItem) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "试题不存在或已下架");
        }

        /** @var Collection $answers */
        $answers = $questionItem->answers;
        $options = $answers->reduce(function ($carry, $item) {
            $carry[] = [
                'id' => $item->id,
                'name' => $item->name
            ];
            return $carry;
        }, []);


        $redis = Helper::getRedis();
        $answerKey = ExamCacheService::buildExamAnswerKey($cacheRequestId);
        $answerKeys = $redis->hKeys($answerKey);    # 已答题目的所有key
        $pre_item_id = '';
        $s_num = 1;
        $nc = '';
        $is_sub = '-1';
        $itemInfo = "";
        sort($answerKeys);
        if ($answerKeys) {
            if (in_array($questionId, $answerKeys)) {
                $Index = array_search($questionId, $answerKeys);
                $preIndex = ($Index==0)?$Index:($Index-1);
                if ($Index > 0) {
                    $s_num += $Index;
                } else {
                    $s_num = 1;
                }

                # 获取已答题的题目选项
                $rank = $redis->hGet($answerKey, (string)$questionId);

                if ($rank) {
                    $rank = json_decode($rank, true);
                    $itemInfo = json_encode($rank['options']);
                }
            } else {
                $Index = array_search(last($answerKeys), $answerKeys);
                $nc = count($answerKeys);   # 当只做了一题时 index=0 不会给出缓存里的题
                $preIndex = ($Index==0)?$Index:($Index);
                $s_num += $nc;
            }
            if ($Index != 0 || $nc == 1) {
                $pre_item_id = $answerKeys[$preIndex];
            }

            $QuestionKey = ExamCacheService::buildQuestionCacheKey($cacheRequestId);
            $QuestionData = $redis->zRange($QuestionKey, 0, -1);
            $item_id = last($QuestionData);
            if ($item_id == $questionId) {
                $is_sub = 1;
            }
        }

        return [
            'item_info' => $itemInfo,
            'is_sub' => $is_sub,
            's_num' => $s_num,
            'pre_item_id' => $pre_item_id,
            'question_id' => $questionItem->id,
            'name' => $questionItem->name,
            'type' => [
                'id' => $questionItem->type,
                'desc' => $questionItem->getTypeDesc()
            ],
            'options' => $options
        ];
    }

    /**
     * 回答考试
     * @param $examId
     * @param $questionId
     * @param string $requestId
     * @param array $optionIds
     * @param string $content
     * @return array
     */
    public static function answerExam(
        $examId,
        $questionId,
        string $requestId,
        array $optionIds = [],
        $content = ""
    ): array {
        $user = Helper::getLoginUser();

        $exam = ComplianceExamRepository::getById($examId);

        $ExamInfo = ComplianceExamRepository::getExamStatus($examId, $user->yyid);

        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        # 再次验证
        if (!$ExamInfo) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试用户信息有误");
        }

        //检查缓存中试题
        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $exam);
        ExamCacheService::checkRequestAndQuestion($cacheRequestId, (int)$questionId);

        //获取试题及选项
        $questionItem = ComplianceQuestionItemsRepository::getItemsById($questionId);
        if (!$questionItem) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "试题不存在或已下架");
        }

        //检验答案选项
        /** @var Collection $answer */
        $checker = QuestionItemChecker::getChecker($questionItem);
        if ($questionItem->isWithOptions()) {
            $checker->withOptions($optionIds);
        } else {
            $checker->withContent($content);
        }
        $checkResult = $checker->check();

        //缓存当前题目校验结果
        $wechat = UserService::getAppLoginUserWechat();
        ExamCacheService::cacheExamAnswer(
            $cacheRequestId,
            $questionItem,
            $checkResult,
            $optionIds,
            $content,
            7200,
            $wechat ? $wechat->openid :''
        );
        //从缓存获取下一题目id
        $nextQuestion = ExamCacheService::getNextQuestionId($cacheRequestId, $questionId);

        return [
            'is_question_pass' => $checkResult ? 1 : 0,
            'is_last' => empty($nextQuestion) ? 1 : 0,
            'next_question_id' => !empty($nextQuestion) ? $nextQuestion[0] : '',
            'is_explain' => $exam->wrong_remind == 0 ? 0 : 1,
            'explain' => $exam->wrong_remind == 0? '' : $questionItem->explain_content
        ];
    }

    private static function processExamsToDb($cacheRequestId, $examId, $userId)
    {
        $p = new ExamToDb();
        $p->setExamId($examId);
        $p->setUserId($userId);
        $p->setRequestId($cacheRequestId);
        $p->setPlatform(ExamCode::YOUYAO);
        $p->setLastTime(date('Y-m-d H:i:s'));

        $tm = TaskManageHelper::create(0, $p, 'exam-to-db');
        $tm->push();
    }

    /**
     * @throws \Exception
     */
    public static function finishExam(int $examId, string $requestId): array
    {
        $user = Helper::getLoginUser();
        $exam = ComplianceExamRepository::getById($examId);

        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        //检查缓存中试题
        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $exam);
        $isFinished = ExamCacheService::checkFinished($cacheRequestId);

        if (!$isFinished) {
            # 强制提交  有结束时间才有补考时间 一定有考试时间才会强制提交
            $isFinished = self::forceSubmExam($cacheRequestId);
        } else {
            if (!$isFinished) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "答题尚未完成");
            }
        }

        //检查是否通过
        $examPassed = ExamCacheService::checkExamPassed(
            $cacheRequestId,
            $exam->pass_condition,
            $exam->pass_type,
            $exam->id
        );

        # Todo 无论通过与否  根据考试id 写入新表 examination_results
        if ($examPassed['status']) {
            $pass = true;
            $status = 2;
        } else {
            $pass = false;
            $status = 4;    # 判断考试是否存在补考 若存在 =3 若不存在 =4【考试不通过】
        }

        ExaminationResultRepository::updateStatusByexamId($examPassed, $user->yyid, $exam->id, $status);

        //写入用户数据并投递处理缓存数据
//        .'-nsq'

        $last = self::writeExamResult($exam, $user, $pass, $cacheRequestId.'-nsq');

        $redis = Helper::getRedis();
        $redis->hDel("youyao:exam:timeh", $cacheRequestId);
        $redis->del("youyao:exam:answers:".$cacheRequestId);
        $redis->del("youyao:exam:questions:".$cacheRequestId);
        $redis->del("youyao:exam:rights:".$cacheRequestId);

        return [
            'with_agreement' => $pass && $exam->agreement_yyid ? 1 : 0,
            'is_exam_pass' => $pass ? 1 : 0,
//            'last_time' => (string)$last->created_time
        ];
    }

    private static function forceSubmExam($cacheRequestId)
    {
        $redis = Helper::getRedis();
        $key = ExamCacheService::buildQuestionCacheKey($cacheRequestId);
        $answerKey = ExamCacheService::buildExamAnswerKey($cacheRequestId);
        # 当考试有结束时间时
        $answerKeys = $redis->hKeys($answerKey);    # 已答题目的所有key
        $questionKey = $redis->zRange($key, 0, -1);    # 未答题目的所有
        $keyArrayDiff = array_diff($questionKey, $answerKeys);

        # 手动缓存redis 所有题目答案为空 //缓存当前题目校验结果
        foreach ($keyArrayDiff as $item) {
            $questionItem = ComplianceQuestionItemsRepository::getItemsById($item);
            if (!$questionItem) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "试题不存在或已下架");
            }

            $wechat = UserService::getAppLoginUserWechat();
            ExamCacheService::cacheExamAnswer(
                $cacheRequestId,
                $questionItem,
                false,
                [],
                '',
                7200,
                $wechat ? $wechat->openid : ''
            );
        }
        return true;
    }

    private static function writeExamResult(
        ComplianceExam $exam,
        Users $users,
        bool $examPassed,
        string $cacheRequestId
    ): ComplianceUserExam {
        //检查最后一次提交记录状态
        $last = ComplianceUserExamRepository::getExamLast($users, $exam);

        //如果已经通过直接返回false
        if ($last && $last->isPassed()) {
            return $last;
        }

        //重置旧的数据,添加新数据
        $wechat = UserService::getAppLoginUserWechat();

        Db::beginTransaction();
        try {
            ComplianceUserExamRepository::clearUserExams($exam, $users);
            $last = ComplianceUserExamRepository::addUserExam(
                $exam,
                $users,
                $examPassed,
                $wechat ? $wechat->openid :''
            );

            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }

        //触发用户积分发放操作
        if ($examPassed) {
            try {
                ExamRpcService::finishExamGainPoint($exam->id, $users->uid);
            } catch(\Exception $e) {
                Helper::getLogger()
                    ->error("finish_exam_point_send_error", [
                        'exam_id' => $exam->id,
                        'user_id' => $users->uid,
                        'msg' => $e->getMessage()
                    ]);
            }
        }

        //最后记录未通过记录通过状态
        self::processExamsToDb($cacheRequestId, $exam->id, $users->uid);
        return $last;
    }

    /**
     * 回顾考试
     * @param int $examId
     * @param string $requestId
     * @return array
     */
    public static function review(int $examId, string $requestId = ""): array
    {
        $user = Helper::getLoginUser();
        $exam = ComplianceExamRepository::getById($examId);
        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        //获取最后一次记录，如果未完成答题，没有回顾
        $last = ComplianceUserExamRepository::getExamLast($user, $exam);

        if (!$last) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "尚未完成考试，无法回顾");
        }

        $reviews = [];
        //首先通过缓存获取考试回顾列表
        if ($requestId) {
            $reviews = self::getAnswersFromCache($exam, $user, $requestId);
        }
        if (!empty($reviews)) {
            return $reviews;
        }

        //其次通过snapshot表获取考试回顾
        $reviews = self::getAnswersFromDb($exam, $user);
        if (!empty($reviews)) {
            return $reviews;
        }

        //最后通过错误选项和考试组合成回顾选项
        return self::getAnswersFromErrResponse($exam, $user);
    }

    /**
     * 从缓存中获取用户的回顾记录
     * @param ComplianceExam $exam
     * @param Users $user
     * @param $requestId
     * @return array
     */
    private static function getAnswersFromCache(ComplianceExam $exam, Users $user, $requestId): array
    {
        $cacheRequestId = self::buildCacheRequestId($requestId, $user, $exam);

        //从缓存获取所有的题目id
        $questionIds = ExamCacheService::getQuestionIds($cacheRequestId);

        if (!$questionIds) {
            return [];
        }

        //获取试题集合
        $userAnswers = ExamCacheService::getAnswers($cacheRequestId);
        if (!$userAnswers) {
            return [];
        }
        $userAnswers = array_map(function ($val) {
            return json_decode($val, true);
        }, $userAnswers);

        //根据试题id获取题目和选项
        $questions = ComplianceQuestionItemsRepository::getItemsByIds($questionIds);
        if ($questions->isEmpty()) {
            return [];
        }
        $questionYYIDS = $questions->pluck('yyid')->all();

        $answers = ComplianceQuestionAnswerRepository::getByQuestionYYIDS($questionYYIDS);
        $answerMap = $answers->groupBy('question_item_yyid')->all();

        //构建响应数据
        $data = [];
        /** @var ComplianceQuestionItem $question */
        foreach ($questions as $question) {
            $answerItem = $userAnswers[$question->id] ?? [];
            $optionIds = $answerItem['options'] ?? [];

            $options = [];
            isset($answerMap[$question->yyid]) &&
            $options = self::buildReviewOptions($answerMap[$question->yyid], $optionIds);

            $data[]  = [
                'id' => $question->id,
                'name' => $question->name,
                'type' => [
                    'id' => $question->type,
                    'desc' => $question->getTypeDesc()
                ],
                'options' => $options,
                'is_right' => $answerItem['is_right'] ?? 0,
                'explain' => $question->explain_content,
                'content' => $answerItem['content'] ?? ''
            ];
        }

        return $data;
    }

    /**
     * 根据错误记录还原出考试回顾记录(这个是为了兼容旧记录)
     * @param ComplianceExam $exam
     * @param Users $user
     * @return array
     */
    private static function getAnswersFromErrResponse(ComplianceExam $exam, Users $user): array
    {
        //获取错误记录
        $response = ComplianceExamResponseRepository::getExamResponse($exam, $user);
        $responseMap = $response->keyBy('question_item_yyid')->all();
        $wrongYYIDS = $response->pluck('question_item_yyid')->all();

        //获取考试id
        $questionIds = self::getExamQuestionIds($exam, 2);
        if (empty($questionIds)) {
            return [];
        }
        //获取所有考试
        $questions = ComplianceQuestionItemsRepository::getItemsByIds($questionIds);
        if ($questions->isEmpty()) {
            return [];
        }

        //获取所有答案
        $questionYYIDS = $questions->pluck('yyid')->all();


        $answers = ComplianceQuestionAnswerRepository::getByQuestionYYIDS($questionYYIDS);
        $answerQuestionMap = $answers->groupBy('question_item_yyid')->all();

        //构建返回值
        $data = [];
        /** @var ComplianceQuestionItem $question */
        foreach ($questions as $question) {
            $answerYYID = [];

            if (isset($responseMap[$question->yyid])) {
                $answerYYID = $responseMap[$question->yyid]->getAnswerYYIDS();
            }

            $chooseByRight = !isset($responseMap[$question->yyid]);

            $options = [];
            if (isset($answerQuestionMap[$question->yyid])) {
                /** @var ComplianceQuestionAnswer $answer */
                foreach ($answerQuestionMap[$question->yyid] as $answer) {
                    $isChosen = in_array($answer->yyid, $answerYYID) ? 1 : (
                        $chooseByRight && $answer->is_right ? 1 : 0
                    );

                    $options[] = [
                        'id' => $answer->id,
                        'name' => $answer->name,
                        'is_right' => $answer->is_right ? 1 : 0,
                        'is_choosed' => $isChosen
                    ];
                }
            }

            $data[] = [
                'id' => $question->id,
                'name' => $question->name,
                'type' => [
                    'id' => $question->type,
                    'desc' => $question->getTypeDesc()
                ],
                'options' => $options,
                'is_right' => in_array($question->yyid, $wrongYYIDS) ? 0 : 1,
                'explain' => $question->explain_content,
                'content' => $answerItem['content'] ?? ''
            ];
        }

        return $data;
    }

    /**
     * 通过用户答题提交历史快照获取回顾记录(只展示最新记录)
     * @param ComplianceExam $exam
     * @param Users $user
     * @return array
     */
    private static function getAnswersFromDb(ComplianceExam $exam, Users $user): array
    {
        //判断快照表是否存在记录
        $snapshots = ComplianceExamAnswersSnapshotRepository::getUserExamSnapshot($user, $exam);
        if ($snapshots->isEmpty()) {
            return [];
        }
        $snapMap = $snapshots->keyBy('question_id')->all();

        //存在则从snap
        $questionIds = $snapshots->pluck('question_id')->all();
        $questions = ComplianceQuestionItemsRepository::getItemsByIds($questionIds);
        if ($questions->isEmpty()) {
            return [];
        }

        $answerYYIDS = $questions->pluck('yyid')->all();
        $answers = ComplianceQuestionAnswerRepository::getByQuestionYYIDS($answerYYIDS);
        $answerMap  = $answers->groupBy('question_item_yyid')->all();

        $data = [];
        /** @var ComplianceQuestionItem $question */
        foreach ($questions as $question) {
            if (!isset($snapMap[$question->id])) {
                continue;
            }
            /** @var ComplianceExamAnswersSnapshot $snap */
            $snap = $snapMap[$question->id];

            $options = [];
            isset($answerMap[$question->yyid]) &&
                $options = self::buildReviewOptions($answerMap[$question->yyid], $snap->getOptionIds());

            $data[] = [
                'id' => $question->id,
                'name' => $question->name,
                'type' => [
                    'id' => $question->type,
                    'desc' => $question->getTypeDesc()
                ],
                'options' => $options,
                'is_right' => $snap->is_pass,
                'explain' => $question->explain_content,
                'content' => (string)$snap->content
            ];
        }

        return $data;
    }

    private static function buildReviewOptions(Collection $answers, array $answerOptionIds = []): array
    {
        $data = [];
        foreach ($answers as $answer) {
            $data[] = [
                'id' => $answer->id,
                'name' => $answer->name,
                'is_right' => $answer->is_right ? 1 : 0,
                'is_choosed' => in_array($answer->id, $answerOptionIds) ? 1 : 0
            ];
        }

        return $data;
    }

    /**
     * 获取所有的考试试题
     * @param ComplianceExam $exam
     * @return array
     */
    private static function getExamQuestionIds(ComplianceExam $exam, $t): array
    {
        $user = Helper::getLoginUser();
        $testPaper = ComplianceTestPaperRepository::getByYYID($exam->test_paper_yyid);

        if (!$testPaper) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试尚未配置完成，无法参与");
        }

        if ($testPaper->question_type == 1) { # 随机出题  查 t_compliance_question_items_fraction 配置的题目
            $examItem = $examItemNum = [];
            if ($testPaper->test_paper_type == 0) {    # 题库出题
                $examItem = TComplianceTestExamFraction::where(
                    [
                        ['test_id',$testPaper->id],
                        ['status',1],
                        ['type',2],
                        ['exam_id',$exam->id]
                    ]
                )
                    ->select("question_bank_id", "item_num")
                    ->get()->toArray();
                # 获取 题库yyid ComplianceQuestion

                foreach ($examItem as $k => $item) {
                    $quYYid = ComplianceQuestion::where('id', $item['question_bank_id'])
                        ->select('yyid')
                        ->first()
                        ->toArray();
                    $examItem[$k]['yyid'] = $quYYid['yyid'];
                }
            } else {
                # 题目出题
                $examItemNum = TComplianceTestExamFraction::where(
                    [
                        ['test_id',$testPaper->id],
                        ['status',1],['type',3],
                        ['exam_id',$exam->id]
                    ]
                )
                    ->select("item_num")
                    ->get()->toArray();
            }

            # 获取随机题目数量的id
            $questionItemIds = self::getQuestionItemsByTestPaper(
                $testPaper,
                [
                    'item_num'=>isset($examItemNum[0]['item_num'])?$examItemNum[0]['item_num']:[],
                    'examItem' => $examItem
                ]
            );
        } else {  # 顺序出题 【老规矩】
            $questionItemIds = self::getQuestionItemsByTestPaper($testPaper, []);
            if (empty($questionItemIds)) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该考试未配置试题");
            }
        }

        # 写入数据库 回顾时数据尚未落地到数据库【随机题】
        if ($t == 1) {    # 初始化时
            ExaminationResultRepository::addExamRandItem($exam, $user, $testPaper->id, $questionItemIds);
        } else {
            $examRandItem = ExaminationResultRepository::getExamRandItem($exam, $user, $testPaper->id);
            if (!$examRandItem->isEmpty()) {
                $examRandItem = $examRandItem->toArray();
                $examRandItem = array_column($examRandItem, 'item_id');
                return $examRandItem;
            }
        }
        return $questionItemIds;
    }

    /**
     * @param int $examId
     * @return ComplianceAgreement
     */
    public static function getExamAgreement(int $examId): ComplianceAgreement
    {
        $user = Helper::getLoginUser();
        $exam = ComplianceExamRepository::getById($examId);
        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }
        if (!$exam->agreement_yyid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该考试未关联协议");
        }

        if (!self::checkUserOwnExam($user, $exam)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在或未分配该考试");
        }

        $agree = (new ComplianceAgreementRepository)->getByYYID($exam->agreement_yyid);
        if (!$agree) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试协议不存在");
        }

        return $agree;
    }

    /**
     * 同意并签署协议生成pdf
     * @param int $examId
     * @param string $img
     * @return ComplianceUserAgreement
     */
    public static function agree(int $examId, string &$img): ComplianceUserAgreement
    {
        $user = Helper::getLoginUser();

        //判断考试
        $exam = ComplianceExamRepository::getById($examId);
        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }
        if (!$exam->agreement_yyid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该考试未关联协议");
        }

        //判断用户考试记录
        $last = ComplianceUserExamRepository::getExamLast($user, $exam);
        if (!$last) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "尚未完成考试，无法签署协议");
        }
        if (!$last->isPassed()) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试未通过，无法签署协议");
        }

        //获取考试协议
        $agree = (new ComplianceAgreementRepository)->getByYYID($exam->agreement_yyid);
        if (!$agree) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试协议不存在");
        }

        //获取用户最后一次记录
        $lastSign = ComplianceUserAgreementRepository::getUserExamAgreement($exam, $user);
        if ($lastSign) {
            return $lastSign;
        }

        $now = Carbon::now();

        $key = ApplicationContext::getContainer()
            ->get(Uuid::class)
            ->generate();
        $pdf = self::createAgreementPdf($agree, $now, $img, $key);

        return ComplianceUserAgreementRepository::addExamAgree($exam, $user, $agree, $now, $pdf, $key);
    }

    public static function createAgreementPdf(ComplianceAgreement $agreement, Carbon $time, string &$signImg, $key)
    {
        $container = ApplicationContext::getContainer();
        $html = $container->get(AgreeHtml::class)
            ->generate($agreement->agreement_content, $signImg, $time);

        $pdf = $container->get(Pdf::class)
            ->createFromHtml($html);

        return $container->get(Upload::class)
            ->putToUser(sprintf("%s.pdf", $key), $pdf->getBody()->getContents());
    }

    /**
     * 获取用户签署协议
     * @param int $examId
     * @return ComplianceUserAgreement
     */
    public static function getExamAgree(int $examId): ComplianceUserAgreement
    {
        $user = Helper::getLoginUser();

        //判断考试
        $exam = ComplianceExamRepository::getById($examId);
        if (!$exam) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试不存在");
        }

        //获取用户考试协议
        $agree = ComplianceUserAgreementRepository::getUserExamAgreement($exam, $user);
        if (!$agree) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "协议不存在或未签署");
        }
        if (!$agree->agree_pdf) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "协议不存在或未生成协议");
        }

        return $agree;
    }
}
