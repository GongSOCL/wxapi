<?php
declare(strict_types=1);
namespace App\Service\Compliance;

use App\Constants\Code\ErrorCodeDoctor;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\PaperQuestionItem;
use App\Repository\ComplianceQuestionItemsRepository;
use App\Repository\ComplianceQuestionRepository;
use App\Repository\PaperQuestionItemRepository;
use Hyperf\Utils\Collection;
use Youyao\Framework\ErrCode;

/**
 * 题库服务
 * Class QuestionService
 * @package App\Service\Compliance
 */
class QuestionService
{
    public static function filterQuestionItemsWithQuestionYYIDS($questionYYID)
    {
        //获取有效题库，并筛选有效试题
        $questions = ComplianceQuestionRepository::getQuestionsByYYIDS($questionYYID);
        if (!$questions || ($questions->isEmpty())) {   //关联题库已经失效了
            return null;
        }
        //实际有效question_yyid
        $validQuestionYYIDS = $questions->pluck('yyid')->toArray();
        if (empty($validQuestionYYIDS)) {
            return null;
        }
        return ComplianceQuestionItemsRepository::getItemsByQuestionYYIDS($validQuestionYYIDS);
    }

    /**
     * @param array $itemYYIDS
     * @return \Hyperf\Utils\Collection
     */
    public static function filterQuestionItemsWithItemYYIDS(array $itemYYIDS): \Hyperf\Utils\Collection
    {
        return ComplianceQuestionItemsRepository::getItemsByYYIDS($itemYYIDS);
    }

    public static function getLogicQuestionAndAnswersByQuestionId($questionId): array
    {
        /** @var PaperQuestionItem $question */
        $question =  PaperQuestionItemRepository::getQuestionItemById($questionId);
        if (!$question) {
            throw new BusinessException(ErrorCode::QUESTION_NOT_EXISTS);
        }
        $answers = new Collection();
        if ($question->isWithOptions()) {
            $answers = $question->answer;
        }

        return [$question, $answers];
    }
}
