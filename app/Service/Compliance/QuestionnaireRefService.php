<?php
declare(strict_types=1);

namespace App\Service\Compliance;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\NewDoctorUserinfo;
use App\Model\Qa\QuestionnairePaper;
use App\Model\Qa\QuestionnairePaperInvitee;
use App\Model\Qa\QuestionnaireWechatInvitee;
use App\Model\Qa\Users;
use App\Repository\NewAgentInviteRepository;
use App\Repository\NewDoctorOpenidUserinfoRepository;
use App\Repository\QuestionnairePaperInviteeRepository;
use App\Repository\QuestionnairePaperRepository;
use App\Repository\QuestionnaireWechatInviteeRepository;
use App\Repository\WechatUserRepository;
use App\Service\Doctor\DoctorInfoService;
use App\Service\Doctor\DoctorWechatBusinessService;
use DateTime;
use Hyperf\Utils\Collection;

/**
 * 问卷服务
 * Class QuestionnaireService
 * @package App\Service\Compliance
 */
class QuestionnaireRefService
{
    const MSG_TYPE_NOTICE = 1;      //提醒回答问卷
    const MSG_TYPE_INVITE = 2;      //邀请回答问卷
    const MSG_TYPE_REINVITE = 3;    //重新邀请回答问卷

    //获取医生微信信息map(以openid为下标)
    private static function getDoctorsWechatOpenIDMap(array $openIds): array
    {
        $infos = DoctorInfoService::getWechatInfoByOpenIds($openIds);
        $map = [];
        foreach ($infos as $item) {
            $openId = $item['openid'];
            $map[$openId] = $item;
        }

        return $map;
    }

    /**
     * app查找代表相关的问卷
     *
     * @param $isProcessing
     * @return array
     */
    public static function appGetAgentsPapers($isProcessing): array
    {
        [$papers, $inviteeList, $countList] = self::getAgentPapers($isProcessing);

        //组合返回数据
        return self::buildAppPaperList($papers, $inviteeList, $countList);
    }

    /**
     * 公众号查找代表相关的问卷
     *
     * @param $isProcessing
     * @return array
     */
    public static function wechatGetAgentsPapers($isProcessing): array
    {
        //获取问卷，问卷邀请代表,问卷答题统计列表
        [$papers, $inviteeList, $countList] = self::getAgentPapers($isProcessing);

        //组合返回数据
        return self::buildWechatPaperList($papers, $inviteeList, $countList);
    }


    /**
     * 获取代表的所有问卷.
     * @param $isProcessing
     * @return array
     */
    private static function getAgentPapers($isProcessing): array
    {
        $agent = Helper::getLoginUser();

        //查找开放邀请的问卷
        $papers = self::getUserQuestionnairePapers($agent, $isProcessing);
        $col = new Collection();
        if ($papers->isEmpty()) {
            //没有数据，直接返回空集
            return [$col, $col, $col];
        }

        //获取邀请记录
        $paperIds = $papers->pluck('id')->toArray();
        $inviteeList = QuestionnairePaperInviteeRepository::getUserPapersByPaperIds($agent, $paperIds);

        //根据paperIds获取当前问卷实际已经占用的名额
        $countList = QuestionnaireWechatInviteeRepository::getPaperValidUsedQuota($paperIds, $agent);

        return [$papers, $inviteeList, $countList];
    }

    /**
     * 获取用户问卷|目前仅查询开放邀请的
     *
     * @param Users $users
     * @param bool $isProcessing
     * @return Collection
     */
    private static function getUserQuestionnairePapers(Users $users, bool $isProcessing = true): Collection
    {
        if ($isProcessing) {
            //$p2 = Orm_Questionnaire_Paper::getUserProcessingWithoutInviteAnswerPaperIds();
            $paperIds = QuestionnairePaperRepository::getUserProcessingInviteAnswerPapersIds($users->uid);
        } else {
//            $p2 = Orm_Questionnaire_Paper::getUserFinishedWithoutInviteAnswerPaperIds();
            $paperIds = QuestionnairePaperRepository::getUserFinishedInviteAnswerPapersIds($users->uid);
        }
        if (empty($paperIds)) {
            return new Collection();
        }

        //根据id找出所有的记录
        return QuestionnairePaperRepository::getPapersByIds($paperIds);
    }




    /**
     * 获取问卷代表邀请列表
     *
     * @param int $paperId
     * @return array
     */
    public static function getAppAgentInvitees(int $paperId): array
    {
        /** @var Users $agent */
        /** @var QuestionnairePaper $paper */
        [$paper, $agent] = self::checkPaperAndAgent($paperId);

        return self::getPaperInvitees($paper, $agent);
    }

    /**
     * 公众号获取问卷邀请记录列表
     * @param string $paperYYID
     * @return array
     */
    public static function getWechatAgentInvitees(string $paperYYID): array
    {
        /** @var Users $agent */
        /** @var QuestionnairePaper $paper */
        [$paper, $agent] = self::checkPaperAndAgentWithYYID($paperYYID);

        //构建返回数据
        return self::getPaperInvitees($paper, $agent);
    }

    /**
     * 校验问卷和代表
     *
     * @param $paperId
     * @return array
     */
    private static function checkPaperAndAgent($paperId, $checkExpire = false): array
    {
        $paper = QuestionnairePaperRepository::getPaperById($paperId);
        if ((!$paper) || (!$paper->isPublished())) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_EXISTS);
        }
        if ($checkExpire && $paper->isExpired()) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_EXPIRED);
        }
        //目前仅支持开放邀请的问卷代表查看
        if (!$paper->isInviteToAnswer()) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_INVITE_ANSWER_NO_NOT_SUPPORT);
        }

        $agent = Helper::getLoginUser();

        return [$paper, $agent];
    }

    /**
     * 校验问卷和代表
     *
     * @param $paperId
     * @return array
     */
    private static function checkPaperAndAgentWithYYID($paperYYID): array
    {
        $paper = QuestionnairePaperRepository::getPaperByYyid($paperYYID);
        if ((!$paper) || (!$paper->isPublished())) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_EXISTS);
        }
        //目前仅支持开放邀请的问卷代表查看
        if (!$paper->isInviteToAnswer()) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_INVITE_ANSWER_NO_NOT_SUPPORT);
        }

        $agent = Helper::getLoginUser();

        return [$paper, $agent];
    }

    /**
     * 获取问卷可邀请好友列表
     *
     * @param $paperId
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return array
     */
    public static function getAppPaperAvailableWechatDoctor(
        $paperId,
        string $keywords = '',
        $current = 1,
        $limit = 10
    ): array {
        /** @var Users $agent */
        [$paper, $agent] = self::checkPaperAndAgent($paperId);

        return self::getAvailableDoctorsForPaper($agent, $keywords, $current, $limit, $paper);
    }

    /**
     * 获取问卷可邀请好友列表
     *
     * @param $paperYYID
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return array
     */
    public static function getWechatPaperAvailableWechatDoctor(
        $paperYYID,
        string $keywords = '',
        $current = 1,
        $limit = 10
    ): array {
        /** @var Users $agent */
        [$paper, $agent] = self::checkPaperAndAgentWithYYID($paperYYID);

        return self::getAvailableDoctorsForPaper($agent, $keywords, $current, $limit, $paper);
    }

    /**
     * 公众号邀请医生回答问卷
     *
     */
    public static function wechatInviteDoctor($paperYYID, string $doctorOpenid)
    {
        /** @var Users $agent */
        /** @var QuestionnairePaper $paper */
        [$paper, $agent] = self::checkPaperAndAgentWithYYID($paperYYID);

        //获取代表关联医生
        $doctorRecord = NewDoctorOpenidUserinfoRepository::getDoctorInfo($agent, $doctorOpenid);
        if (!$doctorRecord) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医生信息不存在");
        }

        self::invite($doctorRecord, $agent, $paper);
    }

    /**
     * 邀请医生回答问卷
     *
     * @param integer $paperId
     * @param integer $openInfoDoctorId
     */
    public static function appInviteDoctor(int $paperId, int $openInfoDoctorId)
    {
        /** @var Users $agent */
        /** @var QuestionnairePaper $paper */
        [$paper, $agent] = self::checkPaperAndAgent($paperId, true);

        //获取医生记录
        $doctorReps = NewDoctorOpenidUserinfoRepository::getRepsInfo($openInfoDoctorId);
        if ($doctorReps->user_id != $agent->uid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医生关联代表信息不一致");
        }
        $doctorRecord = NewDoctorOpenidUserinfoRepository::getInfoById($doctorReps->new_doctor_id);
        if (!$doctorRecord) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医生信息不存在");
        }

        self::invite($doctorRecord, $agent, $paper);
    }

    private static function getAgentName(Users $users): string
    {
        if ($users->name) {
            return $users->name;
        }

        $agentWechatInfo = WechatUserRepository::getOneWechatInfoByUserYYID($users->yyid, DataStatus::WECHAT_AGENT);

        return $agentWechatInfo ? $agentWechatInfo->nickname : '-';
    }

    /**
     * 检查是否已达到限额
     * @param QuestionnairePaper $paper
     * @param QuestionnairePaperInvitee $invitor
     */
    private static function checkQuotaExceed(QuestionnairePaper $paper, QuestionnairePaperInvitee $invitor)
    {
        if ($invitor['invite_quota'] == $invitor['invited']) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_INVITE_QUOTE_EXCEED);
        }

        //获取问卷已经邀请的数量
        $validQuota = QuestionnaireWechatInviteeRepository::getPaperValidQuota($paper, $invitor->invitee_id);
        if ($validQuota == $invitor->invite_quota) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_INVITE_QUOTE_EXCEED);
        }
    }

    /**
     * 重新邀请医生
     *
     * @param $paperId
     * @param $inviteId
     */
    public static function appReInviteDoctor($paperId, $inviteId)
    {
        list($paper, $agent) = self::checkPaperAndAgent($paperId, true);

        //检查邀请记录
        $inviteRecord = QuestionnaireWechatInviteeRepository::getInviteeById($inviteId);
        if (!$inviteRecord) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "邀请记录不存在");
        }
        //检查邀请人
        if ($inviteRecord->invitor_id != $agent->uid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "邀请记录不存在");
        }
        //检查状态
        if (!$inviteRecord->isStatusValidToReInvited()) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_PAPER_WECHAT_NOT_VALID_NOTICE);
        }

        self::reInvite($paper, $inviteRecord, $agent);
    }

    /**
     * 重新邀请医生
     *
     */
    public static function wechatReInviteDoctor(string $paperYYID, string $doctorOpenId)
    {
        /** @var Users $agent */
        list($paper, $agent) = self::checkPaperAndAgentWithYYID($paperYYID);

        //检查邀请记录
        $inviteRecord = QuestionnaireWechatInviteeRepository::getInviteRecord($paper->id, $doctorOpenId, $agent->uid);
        if (!$inviteRecord) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "邀请记录不存在");
        }

        //检查状态
        if (!$inviteRecord->isStatusValidToReInvited()) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_PAPER_WECHAT_NOT_VALID_NOTICE);
        }

        self::reInvite($paper, $inviteRecord, $agent);
    }


    /**
     * 通知答题
     *
     * @param $paperId
     * @param $inviteId
     */
    public static function appNoticeToAnswer($paperId, $inviteId)
    {
        /** @var Users $agent */
        /** @var QuestionnairePaper $paper */
        list($paper, $agent) = self::checkPaperAndAgent($paperId, true);

        //检查邀请记录
        $inviteRecord = QuestionnaireWechatInviteeRepository::getInviteeById($inviteId);
        if (!$inviteRecord) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "邀请记录不存在");
        }
        //检查邀请人
        if ($inviteRecord->invitor_id != $agent->uid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "邀请记录不存在");
        }

        self::noticeAnswer($inviteRecord, $paper, $agent);
    }

    /**
     * 通知答题
     *
     */
    public static function wechatNoticeToAnswer($paperYYID, $doctorOpenId)
    {
        /** @var Users $agent */
        /** @var QuestionnairePaper $paper */
        list($paper, $agent) = self::checkPaperAndAgentWithYYID($paperYYID);

        //检查邀请记录
        $inviteRecord = QuestionnaireWechatInviteeRepository::getInviteRecord(
            $paper->id,
            $doctorOpenId,
            $agent->uid
        );
        if (!$inviteRecord) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "邀请记录不存在");
        }

        self::noticeAnswer($inviteRecord, $paper, $agent);
    }

    /**
     * 发送相关消息
     *
     * @param QuestionnairePaper $paper
     * @param $agentName
     * @param QuestionnaireWechatInvitee $inviteeRecord
     * @param int $actionType
     */
    public static function sendDoctorMsg(
        QuestionnairePaper $paper,
        $agentName,
        QuestionnaireWechatInvitee $inviteeRecord,
        int $actionType = self::MSG_TYPE_INVITE
    ) {
        $url = self::generateQuestionnaireUrl(
            $paper,
            $inviteeRecord->invitor_id,
            DataStatus::USER_TYPE_AGENT,
            0,
            DataStatus::USER_TYPE_DOCTOR,
            $inviteeRecord->id
        );

        switch ($actionType) {
            case self::MSG_TYPE_INVITE:
                // 推送模板消息并
                DoctorWechatBusinessService::sendQuestionnaireWechatInviteMsg(
                    $inviteeRecord->wechat_open_id,
                    $url,
                    $agentName,
                    $paper->name,
                    $inviteeRecord->last_invite_time,
                    $inviteeRecord->last_expire_time
                );
                break;
            case self::MSG_TYPE_REINVITE:
                //不能使用原始数据的时间，需要确保发送消息成功后再行变更
                $now = new DateTime();
                $start = $now->format("Y-m-d H:i:s");
                $expireTime = $now->modify("+{$paper['invite_expire_hour']} hours")
                    ->format("Y-m-d H:i:s");
                // 推送模板消息并
                DoctorWechatBusinessService::sendQuestionnaireWechatReInvite(
                    $inviteeRecord->wechat_open_id,
                    $url,
                    $agentName,
                    $paper->name,
                    $start,
                    $expireTime
                );

                break;
            case self::MSG_TYPE_NOTICE:
                DoctorWechatBusinessService::sendQuestionnaireWechatNoticeToAnswer(
                    $inviteeRecord->wechat_open_id,
                    $url,
                    $agentName,
                    $paper->name,
                    $inviteeRecord->last_invite_time,
                    $inviteeRecord->last_expire_time
                );
                break;
        }

        //TODO: 后面需要调用另一个服务给医生发站内信息
    }

    /**
     * 获取问卷地址(先写死吧，这个本来要前端传的)
     * @param $inviteeRecordId
     * @return string
     */
    private static function getQuestionnaireUrl($inviteeRecordId): string
    {
        $env = env('APP_ENV', 'dev');
        switch ($env) {
            case DataStatus::APP_ENV_DEV:
                return "https://doctor.yyimgs.com/uniapp/pages/survey/survey?paper_yyid={$inviteeRecordId}";
            case DataStatus::APP_ENV_STAGING:
                return "https://doctor.qa2.youyao99.com/uniapp/pages/survey/survey?paper_yyid={$inviteeRecordId}";
            case DataStatus::APP_ENV_PROD:
                return "https://doctor.youyao99.com/uniapp/pages/survey/survey?paper_yyid={$inviteeRecordId}";
        }
    }

    /**
     * 获取问卷地址(先写死吧，这个本来要前端传的)
     * @param $inviteeRecordId
     * @return string
     */
    private static function getLogicQuestionnaireUrl($inviteeRecordId): string
    {
        $env = env('APP_ENV', 'dev');
        switch ($env) {
            case DataStatus::APP_ENV_DEV:
                return "https://doctor.yyimgs.com/uniapp/pages/survey/survey?paper_yyid={$inviteeRecordId}";
            case DataStatus::APP_ENV_STAGING:
                return "https://doctor.qa2.youyao99.com/uniapp/pages/survey/survey?paper_yyid={$inviteeRecordId}";
            case DataStatus::APP_ENV_PROD:
                return "https://doctor.youyao99.com/uniapp/pages/survey/survey?paper_yyid={$inviteeRecordId}";
        }
    }

    /**
     * @param Collection $papers
     * @param array $inviteeMap
     * @param array $countMap
     * @return array
     */
    public static function buildAppPaperList(Collection $papers, Collection $inviteeList, Collection $countList): array
    {
        $inviteeMap = $inviteeList->keyBy('paper_id')->all();
        $countMap = $countList ? $countList->keyBy('paper_id')->all() : [];

        $result = [];
        foreach ($papers as $paper) {
            $paperId = $paper['id'];
            $userTotalQuota = $paper['total_quota'] > 0 && isset($inviteeMap[$paper['id']]) &&
            $inviteeMap[$paperId]['invite_quota'] > 0 ? $inviteeMap[$paperId]['invite_quota'] : 0;
            $restInvite = 0;
            $invited = isset($countMap[$paperId]) ? $countMap[$paperId]['cnt'] : 0;
            if ($userTotalQuota > 0) {
                $restInvite = max($userTotalQuota - $invited, 0);
            }
            $result[] = [
                'id' => $paper['id'],
                'name' => $paper['name'],
                'invited' => $invited,
                'rest_invite' => $restInvite,
                'invite_quota' => $userTotalQuota
            ];
        }
        return $result;
    }


    /**
     * @param Collection $papers
     * @param Collection $inviteeList
     * @param Collection $countList
     * @return array
     */
    public static function buildWechatPaperList(
        Collection $papers,
        Collection $inviteeList,
        Collection $countList
    ): array {
        $inviteeMap = $inviteeList->keyBy('paper_id')->all();
        $countMap = $countList ? $countList->keyBy('paper_id')->all() : [];

        $result = [];
        foreach ($papers as $paper) {
            $paperId = $paper['id'];
            $userTotalQuota = $paper['total_quota'] > 0 && isset($inviteeMap[$paper['id']]) &&
            $inviteeMap[$paperId]['invite_quota'] > 0 ? $inviteeMap[$paperId]['invite_quota'] : 0;
            $restInvite = 0;
            $invited = isset($countMap[$paperId]) ? $countMap[$paperId]['cnt'] : 0;
            if ($userTotalQuota > 0) {
                $restInvite = max($userTotalQuota - $invited, 0);
            }
            $result[] = [
                'yyid' => $paper['yyid'],
                'name' => $paper['name'],
                'invited' => $invited,
                'rest_invite' => $restInvite,
                'invite_quota' => $userTotalQuota
            ];
        }
        return $result;
    }

    /**
     * @param QuestionnairePaper $paper
     * @param Users $agent
     * @return array
     */
    public static function getPaperInvitees(QuestionnairePaper $paper, Users $agent): array
    {
//需要检验并获取
        $invitorRecord = QuestionnairePaperInviteeRepository::checkUserInvited($paper, $agent->uid);
        if (!$invitorRecord) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_INVITED);
        }

        //获取邀请回答关系
        $wechatInvitees = QuestionnaireWechatInviteeRepository::getUserPaperInviteeList($paper, $agent->uid);
        $inviteeMaps = $wechatInvitees->keyBy("invitee_type")->all();

        $doctorWechatMap = $agentWechatMap = [];
        //处理医生列表
        if (isset($inviteeMaps[DataStatus::USER_TYPE_DOCTOR])) {
            $doctorOpenIds = $inviteeMaps[DataStatus::USER_TYPE_DOCTOR]->pluck("wechat_open_id")->all();
            if ($doctorOpenIds) {
                $doctorWechatMap = self::getDoctorsWechatOpenIDMap($doctorOpenIds);
            }
        }

        //处理代表微信信息列表
        if (isset($inviteeMaps[DataStatus::USER_TYPE_AGENT])) {
            $agentOpenIds = $inviteeMaps[DataStatus::USER_TYPE_AGENT]->pluck('wechat_open_id')->all();
            if (!empty($agentOpenIds)) {
                $agentWechatMap = WechatUserRepository::getListByOpenids(
                    $agentOpenIds,
                    DataStatus::WECHAT_AGENT,
                    'openid'
                );
            }
        }

        //处理返回数据
        $result = [
            'paper_name' => $paper->name,
            "invite_more" => 0,
            "statistical" => [
                "invite_quota" => $invitorRecord['invite_quota'],       //总额度
                "answered" => $invitorRecord['invited'],        //已回答
                "invalid" => 0,         //已作废
                "rest_invite" => 0      //可邀请
            ],
            "list" => []
        ];

        $counter = $expireCounter = 0;       //有效和过期占位计数器
        $now = date("Y-m-d H:i:s");
        /** @var QuestionnaireWechatInvitee $item */
        foreach ($wechatInvitees as $item) {
            $itemOpenId = $item->wechat_open_id;
            //判断统计数据
            if ($item->isValidToCount($now)) {
                $counter++;
            }

            $inviteeStatus = $item->isFinished() ? 2 : (
            $item->isExpired($now) || $paper->isExpired() ? 3 : 1
            );
            if ($inviteeStatus == 3) {
                $expireCounter++;
            }

            //添加数据集合
            if ($item['invitee_type'] == DataStatus::WECHAT_DOCTOR) {
                $itemWechat = isset($doctorWechatMap[$itemOpenId]) && $doctorWechatMap[$itemOpenId] ?
                    $doctorWechatMap[$itemOpenId] : [];
            } else {
                $itemWechat = isset($agentWechatMap[$itemOpenId]) && $agentWechatMap[$itemOpenId] ?
                    $agentWechatMap[$itemOpenId] : [];
            }
            $result['list'][] = [
                'id' => $item->id,
                "wechat_name" => $itemWechat ? $itemWechat['nickname'] : '',
                "status" => $inviteeStatus,
                "invitee_openid" => $itemOpenId
            ];
        }
        $result['statistical']['invalid'] = $expireCounter;
        $result['statistical']['rest_invite'] = max($invitorRecord['invite_quota'] - $counter, 0);
        $result['invite_more'] = intval($result['statistical']['rest_invite'] > 0);

        return $result;
    }

    /**
     * @param Users $agent
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @param $paper
     * @return array
     */
    public static function getAvailableDoctorsForPaper(
        Users $agent,
        string $keywords,
        int $current,
        int $limit,
        $paper
    ): array {
        //获取代表所关联的所有医生
        $resp = NewDoctorOpenidUserinfoRepository::searchAgentDoctorWithPage($agent, $keywords, $current, $limit);
        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $resp->perPage()
        ];
        $data = [];
        if ($resp->isEmpty()) {
            return [$page, $data];
        }

        $openIds = [];
        /** @var NewDoctorUserinfo $item */
        foreach ($resp as $item) {
            if ($item->openid) {
                $openIds[] = $item->openid;
            }
        }

        $validOpenIds = [];
        $doctorsMap = [];
        //筛选openid调用服务获取医生数据
        if (!empty($openIds)) {
            $doctorWechatItems = DoctorInfoService::getWechatInfoByOpenIds($openIds);
            foreach ($doctorWechatItems as $item) {
                $doctorsMap[$item['openid']] = $item;
                $validOpenIds[] = $item['openid'];
            }
        }
        //获取问卷已经关联的医生
        $paperInviteeWechat = QuestionnaireWechatInviteeRepository::getUserPaperInviteeList($paper, $agent->uid);
        $paperInviteeOpenIds = $paperInviteeWechat->pluck('wechat_open_id')->all();

        //遍历医生列表增加状态
        /** @var NewDoctorUserinfo $item */
        foreach ($resp as $item) {
            $itemOpenId = $item->openid;
            //0未关注 1已关注未邀请 2已邀请
            $itemStatus = $itemOpenId && isset($doctorsMap[$itemOpenId]) ? 1 : 0;
            if ($itemOpenId && in_array($itemOpenId, $paperInviteeOpenIds)) {
                $itemStatus = 2;
            }
            //用户真实姓名 -> 用户呢称  -> 用户微信呢称 -> -
            $itemName = $item->true_name ?: (
                $item->nickname ?: (
                    isset($doctorsMap[$itemOpenId])
                    && $doctorsMap[$itemOpenId]['nickname']
                        ?  $doctorsMap[$itemOpenId]['nickname'] : '-' )
            );
            $data[] = [
                'doctor_id' => $item->id,
                'is_followed' => $itemStatus != 0 ? 1 : 0,
                'name' => $itemName,
                'openid' => $itemOpenId,
                'status' => $itemStatus
            ];
        }

        return [$page, $data];
    }

    /**
     * @param QuestionnairePaper $paper
     * @param int $invitorId
     * @param int $invitorUserType
     * @param int $inviteeId
     * @param int $inviteeUserType
     * @param int $inviteId
     * @return string
     */
    private static function generateQuestionnaireUrl(
        QuestionnairePaper $paper,
        int $invitorId,
        int $invitorUserType = DataStatus::USER_TYPE_AGENT,
        int $inviteeId = 0,
        int $inviteeUserType = DataStatus::USER_TYPE_DOCTOR,
        int $inviteId = 0
    ): string {
        $shareKey = Helper::createPaperShareKey(
            $paper->id,
            $invitorId,
            $invitorUserType,
            $inviteeId,
            $inviteeUserType,
            $inviteId
        );
        if ($paper->isLogicPaper()) {
            return self::getLogicQuestionnaireUrl($shareKey);
        } else {
            return self::getQuestionnaireUrl($shareKey);
        }
    }

    /**
     * @param NewDoctorUserinfo $doctorRecord
     * @param Users $agent
     * @param QuestionnairePaper $paper
     * @throws \Exception
     */
    private static function invite(NewDoctorUserinfo $doctorRecord, Users $agent, QuestionnairePaper $paper): void
    {
        //根据微信openid获取用户在优佳医的微信信息
        $doctorWechatInfo = DoctorInfoService::getWechatInfoByOpenId($doctorRecord->openid);
        if (!$doctorWechatInfo) {
            throw new BusinessException(ErrorCode::USER_WECHAT_INFO_NOT_EXISTS);
        }

        //获取微信好友关系
        $wechatRelation = NewAgentInviteRepository::getUserInviteRelations(
            $agent,
            $doctorRecord->unionid,
            $doctorRecord->openid
        );
        if (!$wechatRelation) {
            throw new BusinessException(ErrorCode::USER_NOT_WECHAT_FRIENDS, "该医生尚不是您的微信好友，无法邀请回答问卷");
        }

        //获取问卷代表信息及限额检查
        $invitorRecord = QuestionnairePaperInviteeRepository::checkUserInvited($paper, $agent->uid);
        if (!$invitorRecord) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_INVITED);
        }
        self::checkQuotaExceed($paper, $invitorRecord);

        //检查是否已经被邀请过了
        $doctorInviteRecord = QuestionnaireWechatInviteeRepository::getInviteRecord(
            $paper->id,
            $doctorRecord->openid,
            $agent->uid
        );
        if ($doctorInviteRecord) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_USER_WECHAT_INVITE_EXISTS);
        }

        //2pc: 添加邀请记录并标记状态为新建
        $unionId = $doctorWechatInfo['unionid'] ?? '';
        $newInviteeRecord = QuestionnaireWechatInviteeRepository::addInvitee(
            $paper,
            $agent,
            $doctorRecord->openid,
            DataStatus::USER_TYPE_DOCTOR,
            $unionId
        );

        //这部分东西实际上如果有一个消息系统再配合回调处理，可以异步出去
        try {
            $name = self::getAgentName($agent);
            self::sendDoctorMsg($paper, $name, $newInviteeRecord);

            //标记已邀请
            QuestionnaireWechatInviteeRepository::markInvited($newInviteeRecord, $paper);
        } catch (\Exception $e) {
            //取消邀请记录
            QuestionnaireWechatInviteeRepository::cancelInvite($newInviteeRecord);
            throw $e;
        }
    }

    /**
     * @param $paper
     * @param $inviteRecord
     * @param $agent
     */
    private static function reInvite($paper, $inviteRecord, $agent): void
    {
        //检查过期时间
        if ($paper->invite_expire_hour > 0 && (!$inviteRecord->isExpired())) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_PAPER_WECHAT_INVITE_NOT_EXPIRE);
        }

        //获取问卷代表信息及限额检查
        $invitorRecord = QuestionnairePaperInviteeRepository::checkUserInvited($paper, $agent->uid);
        if (!$invitorRecord) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_INVITED);
        }
        //重新检查配额
        self::checkQuotaExceed($paper, $invitorRecord);

        //检查过期时间
        $agentName = self::getAgentName($agent);
        self::sendDoctorMsg(
            $paper,
            $agentName,
            $inviteRecord,
            self::MSG_TYPE_REINVITE
        );

        //标记已重新邀请
        QuestionnaireWechatInviteeRepository::markReInvited($inviteRecord, $paper);
    }

    /**
     * @param $inviteRecord
     * @param QuestionnairePaper $paper
     * @param Users $agent
     */
    private static function noticeAnswer(
        QuestionnaireWechatInvitee $inviteRecord,
        QuestionnairePaper $paper,
        Users $agent
    ): void {
        //检查状态
        if (!$inviteRecord->isStatusValidToNotice()) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_PAPER_WECHAT_NOT_VALID_NOTICE);
        }
        //检查过期时间
        if ($paper->invite_expire_hour > 0 && $inviteRecord->isExpired()) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_INVITE_EXPIRED);
        }

        //发送提醒消息
        $agentName = self::getAgentName($agent);
        self::sendDoctorMsg($paper, $agentName, $inviteRecord, self::MSG_TYPE_NOTICE);
    }
}
