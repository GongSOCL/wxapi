<?php
declare(strict_types=1);

namespace App\Service\Compliance;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\PaperQuestionItem;
use App\Model\Qa\QuestionnairePaper;
use App\Model\Qa\QuestionnaireWechatInvitee;
use App\Repository\ComplianceQuestionItemsRepository;
use App\Repository\PaperQuestionItemRepository;
use App\Repository\QuestionnairePaperInviteeRepository;
use App\Repository\QuestionnairePaperRepository;
use App\Repository\QuestionnaireWechatInviteeRepository;
use App\Repository\UploadQiniuRepository;
use App\Repository\WechatUserRepository;
use App\Service\Doctor\DoctorInfoService;
use App\Service\Points\PointsService;
use App\Task\QuestionAnswerToDbTask;
use Exception;
use Grpc\Questionnaire\QuestionnaireInviteShare;
use GuzzleHttp\Exception\GuzzleException;
use Hyperf\Database\Model\Collection;
use Hyperf\DbConnection\Db;
use UnexpectedValueException;

/**
 * 问卷服务
 * Class QuestionnaireService
 * @package App\Service\Compliance
 */
class QuestionnaireService
{
    const MSG_TYPE_NOTICE = 1;      //提醒回答问卷
    const MSG_TYPE_INVITE = 2;      //邀请回答问卷
    const MSG_TYPE_REINVITE = 3;    //重新邀请回答问卷

    const DEFAULT_START_BG = 'http://static.youyao99.com/5efde4f42f4de8f82484af12f78e8953.png';
    const DEFAULT_TAIL_BG = 'http://static.youyao99.com/13bb31169ab07b6895317509e4ba094c.png';
    const DEFAULT_ANSWER_BG = 'http://static.youyao99.com/13bb31169ab07b6895317509e4ba094c.png';

    /**
     * 开始考试
     *
     * @param QuestionnaireInviteShare $share
     * @param $openid
     * @return array
     */
    public static function startQuestionnaire(QuestionnaireInviteShare $share, $openid): array
    {
        /** @var QuestionnairePaper $paper */
        list($paper, $invitee) = self::checkPaperAndWechatDoctorInvitee($share, $openid);

        //调用core服务获取下一题目
        [$total, $nextQuestionId] = QuestionnaireRpcService::getFirstQuestion($paper->id);

        if ($paper->isInviteToAnswer() && $invitee) {
            QuestionnaireWechatInviteeRepository::setStart($invitee);
        }

        [$startBgUrl, $tailBgUrl, $answerBgUrl] = self::getPaperBg($paper);

        $nextQuestion = null;
        return [
            $paper,
            $total,
            Helper::generateHashId($nextQuestionId),
            $startBgUrl,
            $tailBgUrl,
            $answerBgUrl
        ];
    }

    private static function getPaperBg(QuestionnairePaper $paper): array
    {
        $ids = [];
        $startBgId = $paper->start_bg_id;
        $tailBgId = $paper->tail_bg_id;
        $answerBgId = $paper->answer_bg_id;
        if ($startBgId > 0) {
            $ids[] = $startBgId;
        }
        if ($tailBgId > 0) {
            $ids[] = $tailBgId;
        }
        if ($answerBgId > 0) {
            $ids[] = $answerBgId;
        }
        $bgMap = [];
        if (!empty($ids)) {
            $bgMap = UploadQiniuRepository::getByIds($ids)->keyBy('id')->toArray();
        }

        $startBgUrl = $startBgId > 0 && isset($bgMap[$startBgId]) ? $bgMap[$startBgId]['url'] : self::DEFAULT_START_BG;
        $tailBgUrl = $tailBgId > 0 && isset($bgMap[$tailBgId]) ? $bgMap[$tailBgId]['url'] : self::DEFAULT_TAIL_BG;
        $answerBgUrl = $answerBgId > 0
        && isset($bgMap[$answerBgId]) ? $bgMap[$answerBgId]['url'] : self::DEFAULT_ANSWER_BG;

        return [$startBgUrl, $tailBgUrl, $answerBgUrl];
    }


    /**
     * 校验问卷及受邀者资格并获取问卷和受邀者
     *
     * @param QuestionnaireInviteShare $share
     * @param string $openId
     * @return array
     */
    private static function checkPaperAndWechatDoctorInvitee(QuestionnaireInviteShare $share, string $openId): array
    {
        $paper = QuestionnairePaperRepository::getPaperById($share->getPaperId());
        if ((!$paper) || !($paper->isPublished())) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_EXISTS);
        }
        if ($paper->isExpired()) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_EXPIRED);
        }

        //开放邀请的需要获取邀请记录
        $wechatInvitee = null;
        if ($paper->isInviteToAnswer()) {
            //获取邀请记录
            /** @var QuestionnaireWechatInvitee $wechatInvitee */
            $wechatInvitee = QuestionnaireWechatInviteeRepository::getInviteeById($share->getInviteId());
            if (!$wechatInvitee) {
                throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_INVITED);
            }
            if ($wechatInvitee->wechat_open_id != $openId) {
                throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_INVITED);
            }

            //判断是否已完成答题(特殊状态码需要给前端处理页面)
            if ($wechatInvitee->isFinished()) {
                throw new BusinessException(
                    ErrorCode::QUESTIONNAIRE_FINISHED
                );
            }

            //判断邀请记录状态只有新建
            if (!$wechatInvitee->isStatusValidToAnswer()) {
                throw new BusinessException(ErrorCode::QUESTIONNAIRE_STATUS_ERROR);
            }

            //状态过期时间
            if ($wechatInvitee->isExpired()) {
                throw new BusinessException(ErrorCode::QUESTIONNAIRE_INVITE_EXPIRED);
            }
        }

        return [$paper, $wechatInvitee];
    }


    /**
     * 获取问卷试题详情
     *
     * @param string $openid 微信openid
     * @param QuestionnaireInviteShare $share
     * @param string $questionYYID 试题yyid(id hash)
     * @param integer $pos 第n题
     * @return array
     */
    public static function getQuestionItemInfo(
        string $openid,
        QuestionnaireInviteShare $share,
        string $questionYYID,
        $pos
    ): array {
        //校验并获取问卷和邀请者
        /** @var QuestionnairePaper $paper */
        list($paper, $invitee) = self::checkPaperAndWechatDoctorInvitee($share, $openid);

        $questionId = Helper::decodeHashId($questionYYID);
        if (!$questionId) {
            throw new BusinessException(ErrorCode::QUESTION_NOT_EXISTS);
        }

        //获取问题及答案
        if ($paper->isLogicPaper()) {
            return QuestionnaireService::getLogicQuestionAnswerInfo($paper, $questionId, $pos);
        } else {
            return self::getNormalQuestionAnswerInfo($paper, $questionId, $pos);
        }
    }

    private static function getLogicQuestionAnswerInfo($paper, $questionId, $pos): array
    {
        /** @var PaperQuestionItem $question */
        [$question, $answers] = QuestionService::getLogicQuestionAndAnswersByQuestionId($questionId);
        //获取统计总数及下一题的yyid
        [$total, $nextQuestionItemId] = QuestionnaireRpcService::getNextQuestion(
            $paper->id,
            $questionId,
            [],
            '',
            $pos + 1
        );
        $result = [
            'total' => $total,
            'pos' => $pos,
            'yyid' => Helper::generateHashId($question->id),
            'name' => $question->name,
            'type' => $question->type,
            'items' => [],
            'next_question_yyid' => $nextQuestionItemId ? Helper::generateHashId($nextQuestionItemId) : "",
            'is_skip' => $question->is_skip
        ];
        foreach ($answers as $answer) {
            $result['items'][] = [
                'yyid' => Helper::generateHashId($answer['id']),
                'name' => $answer['name']
            ];
        }

        return $result;
    }

    private static function getNormalQuestionAnswerInfo(QuestionnairePaper $paper, $questionId, $pos): array
    {
        list($questionItem, $answers) = self::getQuestionAndAnswer($questionId);

        //获取统计总数及下一题的yyid
        [$total, $nextQuestionItemId] = QuestionnaireRpcService::getNextQuestion(
            $paper->id,
            $questionId,
            [],
            '',
            $pos + 1
        );

        $result = [
            'total' => $total,
            'pos' => $pos,
            'yyid' => Helper::generateHashId($questionItem->id),
            'name' => $questionItem['name'],
            'type' => $questionItem['type'],
            'items' => [],
            'next_question_yyid' => $nextQuestionItemId ? Helper::generateHashId($nextQuestionItemId) : "",
            'is_skip' => 0
        ];
        foreach ($answers as $answer) {
            $result['items'][] = [
                'yyid' => Helper::generateHashId($answer['id']),
                'name' => $answer['name']
            ];
        }

        return $result;
    }

    /**
     * 获取试题内容和答题选项
     *
     * @param $questionId
     * @return array
     */
    private static function getQuestionAndAnswer($questionId): array
    {
        //获取试题信息
        $questionItem = ComplianceQuestionItemsRepository::getItemsById($questionId);
        if (!$questionItem) {
            throw new BusinessException(
                ErrorCode::QUESTIONNAIRE_QUESTION_ITEM_NOT_EXISTS
            );
        }

        //获取试题选项,对于非收集信息的选项题是必须要有答案的
        $answers = null;
        if (!$questionItem->isContentCheck()) {
            $answers = $questionItem->answers;
            if ((!$questionItem->isContentCheck())
                && (!$answers || ($answers instanceof Collection && $answers->isEmpty()))) {
                throw new BusinessException(ErrorCode::QUESTIONNAIRE_ITEM_WITHOUT_ANSWER);
            }
        }

        return [$questionItem, $answers];
    }

    /**
     * 回答问卷某一题目
     *
     * @param $requestId
     * @param $openId
     * @param QuestionnaireInviteShare $share
     * @param $questionYYID
     * @param int $pos
     * @param array $answerYYIDS
     * @param string $content
     * @return array
     * @throws GuzzleException
     */
    public static function answerQuestion(
        $requestId,
        $openId,
        QuestionnaireInviteShare $share,
        $questionYYID,
        $pos = 1,
        $answerYYIDS = [],
        $content = ''
    ): array {
        //检查并获取问卷和邀请关系记录
        /** @var QuestionnairePaper $paper */
        list($paper, $invitee) = self::checkPaperAndWechatDoctorInvitee($share, $openId);

        if ($share->getInviteeType() == DataStatus::USER_TYPE_DOCTOR) {
            $doctorWechatInfo = DoctorInfoService::getWechatInfoByOpenId($openId);
            $wechatId = $doctorWechatInfo['id'] ?? 0;
        } else {
            $wechat = WechatUserRepository::getOneWechatInfo($openId, $share->getInviteeType());
            if (!$wechat) {
                throw new BusinessException(ErrorCode::USER_WECHAT_INFO_NOT_EXISTS);
            }
            $wechatId = $wechat->id;
        }

        $answerIds = [];
        foreach ($answerYYIDS as $answerYYID) {
            $answerId = Helper::decodeHashId($answerYYID);
            if ($answerId) {
                $answerIds[] = $answerId;
            }
        }

        $questionId = Helper::decodeHashId($questionYYID);
        [$isRight, $rightAnswerIds] = QuestionnaireRpcService::checkQuestionAnswers(
            $paper->id,
            $questionId,
            $answerIds,
            $content,
            $pos
        );

        list($total, $nextQuestionId) = QuestionnaireRpcService::getNextQuestion(
            $paper->id,
            $questionId,
            $answerIds,
            $content,
            $pos + 1
        );

        //记录数据
        if ($paper->isLogicPaper()) {
            $question = PaperQuestionItemRepository::getQuestionItemById($questionId);
            self::cacheQuestionAnswer(
                $question,
                $openId,
                $requestId,
                $answerIds,
                $content,
                $invitee,
                $wechatId
            );
        }

        //更新统计
        if (!$nextQuestionId && ((!$paper->isLogicPaper() && $isRight) || $paper->isLogicPaper())) {
            self::markInviteFinished($paper, $invitee);
            if ($paper->isLogicPaper()) {
                $cacheKey = self::buildCacheKey($requestId, $openId);
                //TODO: goroutine execute
                //投递task开始落地消息
                Helper::executeTask([QuestionAnswerToDbTask::class, 'handle'], [$cacheKey]);
            }
        }

        if ($paper->isLogicPaper()) {
            $needCheck = 0;
            $explain = "";
        } else {
            $question = ComplianceQuestionItemsRepository::getItemsById($questionId);
            if (!$question) {
                throw new BusinessException(ErrorCode::QUESTION_NOT_EXISTS);
            }
            $needCheck = $question->isWithOptions();
            $explain = $question->explain_content;
        }
        $outputRightOptions = [];
        foreach ($rightAnswerIds as $opt) {
            $outputRightOptions[] = Helper::generateHashId($opt);
        }

        //组合数据
        return [
            'need_check' => $needCheck ? 1 : 0,      //问卷实际上并不进行校验
            'is_right' => $isRight ? 1 : 0,
            'right_options' => $outputRightOptions,
            'explain' => $explain,
            "next_question_yyid" => $nextQuestionId ? Helper::generateHashId($nextQuestionId) : ""
        ];
    }


    //获取医生微信信息map(以openid为下标)
    public static function cacheQuestionAnswer(
        PaperQuestionItem $question,
        $openId,
        $requestId,
        $answerIds = [],
        $content = "",
        ?QuestionnaireWechatInvitee $invitee = null,
        $wechatId = 0
    ) {
        $listKey = self::buildCacheKey($requestId, $openId);
        $expire = 2 * 60 * 60 * 24;
        $data = [
            'paper_id' => $question->paper_id,
            'wechat_id' => $wechatId,
            'wechat_open_id' => $openId,
            'invite_id' => $invitee ? $invitee->id : 0,
            'question_id' => $question->id,
            'answers' => $answerIds,
            'content' => $content
        ];

        $client = Helper::getRedisCache();
        $client->lPush($listKey, json_encode($data, JSON_UNESCAPED_UNICODE));
        $client->expire($listKey, $expire);
    }

    private static function buildCacheKey($requestId, $openId): string
    {
        return sprintf("youyao:wxapi:questionnaire:%s:%s", $requestId, $openId);
    }

    /**
     * 标记回答完成
     *
     * @param QuestionnairePaper $paper
     * @param QuestionnaireWechatInvitee|null $wechatInviteeRecord
     * @throws GuzzleException
     */
    public static function markInviteFinished(
        QuestionnairePaper $paper,
        ?QuestionnaireWechatInvitee $wechatInviteeRecord
    ) {
        $invitor = null;
        if ($paper->isInviteToAnswer()) {
            $invitor = QuestionnairePaperInviteeRepository::checkUserInvited(
                $paper,
                $wechatInviteeRecord['invitor_id'],
                DataStatus::USER_TYPE_AGENT
            );
        }

        $saveResult = false;
        Db::beginTransaction();
        try {
            if ($wechatInviteeRecord) {
                //抢占式保存成功了，则进行后续的数据更新，否则不操作
                $saveResult = QuestionnaireWechatInviteeRepository::setFinished($wechatInviteeRecord);
                Helper::getLogger()->info("markInviteFinished", [
                    'id' => $wechatInviteeRecord->id,
                    'result' => $saveResult
                ]);
            } else {    //开放邀请
                $saveResult = true;
            }

            if ($saveResult) {
                QuestionnairePaperRepository::finishOne($paper);
                if ($paper->isInviteToAnswer() && $invitor) {
                    QuestionnairePaperInviteeRepository::finishOne($invitor);
                }
            }
            Db::commit();
        } catch (Exception $e) {
            Helper::getLogger()->info("markInviteFinished", [
                'id' => $wechatInviteeRecord->id,
                'exception' => $e
            ]);
            Db::rollBack();
        }

        if (!$saveResult) {
            return;
        }

        //保存成功并且已经注册了系统的，给当前用户发放积分
        if ($wechatInviteeRecord->invitee_type == DataStatus::USER_TYPE_DOCTOR) {
            $doctorWechatInfo = DoctorInfoService::getWechatInfoByOpenId($wechatInviteeRecord->wechat_open_id);
            $uid = $doctorWechatInfo['uid'] ?? 0;
            $wechatId = $doctorWechatInfo['id'] ?? 0;
        } else {
            $wechatUser = WechatUserRepository::getOneWechatInfo(
                $wechatInviteeRecord->wechat_open_id,
                $wechatInviteeRecord->invitee_type
            );
            $uid = $wechatUser->user ? $wechatUser->user->uid : 0;
            $wechatId = $wechatUser->id;
        }
        PointsService::addQuestionnaireActivityEvent($uid, $paper->id, $wechatId);
    }

    /**
     * 添加问卷邀请记录
     * @param QuestionnairePaper $paper
     * @param $inviteeId
     * @param $quote
     * @param int $inviteeUserType
     */
    public static function addPaperInvitee(
        QuestionnairePaper $paper,
        $inviteeId,
        $quote,
        $inviteeUserType = DataStatus::USER_TYPE_AGENT
    ) {
        $inviteRecord = QuestionnairePaperInviteeRepository::checkUserInvited($paper, $inviteeId, $inviteeUserType);
        if ($inviteRecord) {
            throw new UnexpectedValueException("问卷邀请记录已经存在，无法重新邀请");
        }

        QuestionnairePaperInviteeRepository::addPaperInviteRecord($paper, $inviteeId, $quote, $inviteeUserType);
    }
}
