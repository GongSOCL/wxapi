<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Compliance\Agree;

use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Kernel\Pdf\Pdf;
use App\Kernel\Qiniu\Upload;
use App\Kernel\Uuid\Uuid;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ComplianceUserAgreement;
use App\Model\Qa\ComplianceUserExam;
use App\Repository\ComplianceAgreementRepository;
use App\Repository\ComplianceExamRepository;
use App\Repository\ComplianceUserAgreementRepository;
use App\Repository\ComplianceUserExamRepository;
use App\Service\Compliance\ExamService;
use Carbon\Carbon;
use Grpc\Exam\ExamRegister;
use Hyperf\Di\Annotation\Inject;

class AgreeService
{
    /**
     * @Inject
     * @var ComplianceExamRepository
     */
    private $exam;

    /**
     * @Inject
     * @var ComplianceUserExamRepository
     */
    private $userExam;

    /**
     * @Inject
     * @var ComplianceUserAgreementRepository
     */
    private $userAgree;

    /**
     * @Inject
     * @var Uuid
     */
    private $uuid;

    /**
     * @Inject
     * @var Upload
     */
    private $upload;

    /**
     * @Inject
     * @var AgreeHtml
     */
    private $html;

    /**
     * @Inject
     * @var ComplianceAgreementRepository
     */
    private $agreement;

    /**
     * @Inject
     * @var Pdf
     */
    private $pdf;

    public function process(string $examYyid, string $userYyid, string $signImg)
    {
        $user = Helper::getLoginUser();

        $userExam = $this->userExam->getPassUserExam($examYyid, $user->uid);
        if (! $userExam instanceof ComplianceUserExam) {
            throw new BusinessException(-1, '未通过该项测试');
        }

        $exam = $this->exam->getByYyid($examYyid);
        if (! $exam instanceof ComplianceExam) {
            throw new BusinessException(-1, '测试不存在');
        }

        $userAgree = ComplianceUserAgreementRepository::getUserExamAgreement($exam, $user);
        if (!$userAgree) {
            $key = $this->uuid->generate();
            $agreement = $this->agreement->getByYyid($exam->agreement_yyid);
            $now = Carbon::now();
            $pdf = ExamService::createAgreementPdf($agreement, $now, $signImg, $key);

            $userAgree = ComplianceUserAgreementRepository::addExamAgree($exam, $user, $agreement, $now, $pdf, $key);
        }

        return $userAgree->agree_pdf;
    }
}
