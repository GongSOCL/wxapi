<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Compliance\Agree;

use Carbon\Carbon;
use DiDom\Document;
use DiDom\Element;

class AgreeHtml
{
    public function generate(string $agreement, string &$signImg, Carbon $date)
    {
        $dom = Document::create(
            $this->getTemplate($agreement)
        );

        $this->replaceSignElement($dom, $signImg);

        $this->replaceDateElement($dom, $date->format('Y年m月d日'));

        return $dom->html();
    }

    private function replaceSignElement(Document $dom, string &$signImg)
    {
        if ($finds = $dom->xpath('//p[text()[normalize-space()="员工签字："]]/span')) {
            $span = new Element('span', null, ['style' => 'text-decoration: underline;"']);
            $span->appendChild(
                new Element('img', null, ['src' => $signImg, 'style' => 'width: 100px;'])
            );

            $finds[0]->replace($span);
        }
    }

    private function replaceDateElement(Document $dom, string $date)
    {
        if ($finds = $dom->xpath('//p[text()[normalize-space()="签字日期："]]/span')) {
            $finds[0]->replace(
                new Element('span', $date, ['style' => 'text-decoration: underline;"'])
            );
        }
    }

    private function getTemplate(string $agreement)
    {
        return <<<HTML
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
{$agreement}
</body>
</html>
HTML;
    }
}
