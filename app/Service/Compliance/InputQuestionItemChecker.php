<?php
declare(strict_types=1);
namespace App\Service\Compliance;

/**
 * 输入框校验器
 * Class Bll_Compliance_Questionnaire_InputQuestionItemChecker
 */
class InputQuestionItemChecker extends QuestionItemChecker
{
    public bool $needCheck = false;

    public function check(): bool
    {
        $this->rightOptions = [];
        //目前由于输入内容不校验，可以直接返回成功
        return true;
    }
}
