<?php
declare(strict_types=1);


namespace App\Service\Compliance;

use Grpc\Questionnaire\FirstQuestionnaireRequest;
use Grpc\Questionnaire\QuestionAnswerRequest;
use Grpc\Questionnaire\QuestionnaireClient;
use Hyperf\Utils\ApplicationContext;

class QuestionnaireRpcService
{
    public static function getFirstQuestion($paperId): array
    {
        $request = new FirstQuestionnaireRequest();
        $request->setPaperId($paperId);
        $resp = ApplicationContext::getContainer()->get(QuestionnaireClient::class)
            ->getFirstQuestionYYID($request);

        return [$resp->getTotal(), $resp->getNextId()];
    }

    public static function checkQuestionAnswers($paperId, $questionId, $optionIds = [], $contents = "", $pos = 0): array
    {
        $request = new QuestionAnswerRequest();
        $request->setPaperId($paperId);
        $request->setQuestionId($questionId);
        $request->setAnswerOptions($optionIds);
        $request->setContent($contents);
        $request->setPos($pos);

        $resp = ApplicationContext::getContainer()->get(QuestionnaireClient::class)
            ->checkQuestionAnswers($request);

        return [$resp->getIsRight(), $resp->getRightAnswerIds()];
    }

    public static function getNextQuestion($paperId, $questionId, $optionIds = [], $contents = "", $pos = 0): array
    {
        $request = new QuestionAnswerRequest();
        $request->setPaperId($paperId);
        $request->setQuestionId($questionId);
        $request->setAnswerOptions($optionIds);
        $request->setContent($contents);
        $request->setPos($pos);

        $resp = ApplicationContext::getContainer()->get(QuestionnaireClient::class)
            ->getNextQuestion($request);

        return [$resp->getTotal(), $resp->getNextId()];
    }
}
