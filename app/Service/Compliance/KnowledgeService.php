<?php
declare(strict_types=1);


namespace App\Service\Compliance;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\ComplianceKnowledge;
use App\Model\Qa\ComplianceKnowledgeDir;
use App\Model\Qa\Users;
use App\Repository\ComplianceDirSeriesRepository;
use App\Repository\ComplianceKnowledgeDirRepository;
use App\Repository\ComplianceKnowledgeRepository;
use App\Repository\RepsServeScopeSRepository;
use Hyperf\Utils\Collection;
use Hyperf\Utils\Context;

class KnowledgeService
{
    public static function topDirs(): array
    {
        $subDirs = self::getValidSubDirs();
        if ($subDirs->isEmpty()) {
            return [];
        }
        $parentDirYYIDS = $subDirs->pluck('p_yyid')->toArray();

        $parentDirs = ComplianceKnowledgeDirRepository::getParentDirs($parentDirYYIDS);
        $result = [];
        /** @var ComplianceKnowledgeDir $parentDir */
        foreach ($parentDirs as $parentDir) {
            $result[] = [
                'id' => $parentDir->id,
                'name' => $parentDir->dir_name
            ];
        }
        return $result;
    }

    private static function getValidSubDirs(): Collection
    {
        $user = Helper::getLoginUser();

        //获取用户服务产品
        $series = RepsServeScopeSRepository::getUserServeSeries($user);
        if ($series->isEmpty()) {
            return new Collection();
        }

        $seriesYYIDS = $series->pluck('id')->toArray();
        $seriesYYIDS = array_unique($seriesYYIDS);

        //根据服务产品获取相应目录id
        $knowledgeSeries = ComplianceDirSeriesRepository::getDirIdsBySeriesIds($seriesYYIDS);
        if ($knowledgeSeries->isEmpty()) {
            return new Collection();
        }
        $dirIds = $knowledgeSeries->pluck('dir_id')->toArray();
        $dirIds = array_unique($dirIds);

        //获取子目录
        return ComplianceKnowledgeDirRepository::getSubDirById($dirIds);
    }

    public static function search($keywords, $withId = false): array
    {
        $subDirs = self::getValidSubDirs();
        if ($subDirs->isEmpty()) {
            return [];
        }
        $subDirYYIDS = $subDirs->pluck('yyid')->toArray();

        $knowledgeItems = ComplianceKnowledgeRepository::searchKnowledge($keywords, $subDirYYIDS);
        if ($knowledgeItems->isEmpty()) {
            return [];
        }
        $result = [];
        /** @var ComplianceKnowledge $knowledge */
        foreach ($knowledgeItems as $knowledge) {
            $result[] = [
                'id' => $withId ? $knowledge->id : $knowledge->yyid,
                'name' => $knowledge->title
            ];
        }
        return $result;
    }

    public static function treeView($topDirId = 0, $withId = false): array
    {
        $filterDirYYID = "";
        if ($topDirId) {
            /** @var ComplianceKnowledgeDir $filterDir */
            $filterDir = ComplianceKnowledgeDirRepository::getDirById($topDirId);
            if (!($filterDir && (!$filterDir->p_yyid))) {
                return [];
            }
            $filterDirYYID = $filterDir->yyid;
        }
        $subDirs = self::getValidSubDirs();
        if ($filterDirYYID && $subDirs->isNotEmpty()) {
            $subDirs = $subDirs->filter(function ($val, $key) use ($filterDirYYID) {
                return $val->p_yyid == $filterDirYYID;
            });
        }
        if ($subDirs->isEmpty()) {
            return [];
        }
        $subDirYYIDS = $subDirs->pluck('yyid')->toArray();
        $parentDirYYIDS = $subDirs->pluck('p_yyid')->toArray();

        $parentDirs = ComplianceKnowledgeDirRepository::getParentDirs($parentDirYYIDS);
        if ($parentDirs->isEmpty()) {
            return [];
        }

        $knowledge = ComplianceKnowledgeRepository::getDirsKnowledge($subDirYYIDS);

        return self::buildDirKnowledgeTree($subDirs, $parentDirs, $knowledge, $withId);
    }

    private static function buildDirKnowledgeTree(
        Collection $subDirs,
        Collection $parentDir,
        Collection $knowledge,
        $withId = false
    ): array {
        $result = [];
        $parentDirMap = $parentDir->keyBy('yyid')->all();
        $knowledgeDirMap = $knowledge->groupBy('dir_yyid')->all();
        $subDirMap = $subDirs->groupBy('p_yyid')->all();

        /** @var ComplianceKnowledgeDir $subDir */
        foreach ($subDirMap as $parentYYID => $subDirItems) {
            if (!isset($parentDirMap[$parentYYID])) {
                continue;
            }

            /** @var ComplianceKnowledgeDir $parentDir */
            $parentDir = $parentDirMap[$parentYYID];
            $itemData = [
                'id' => $parentDir->id,
                'name' => $parentDir->dir_name,
                'type' => 1,
                'children' => []
            ];
            /** @var ComplianceKnowledgeDir $subDirItem */
            foreach ($subDirItems as $subDirItem) {
                $child = [
                    'id' => $subDirItem->id,
                    'name' => $subDirItem->dir_name,
                    'type' => 1,
                    'children' => []
                ];
                $dirKnowledge = $knowledgeDirMap[$subDirItem->yyid] ?? (new Collection());
                /** @var ComplianceKnowledge $item */
                foreach ($dirKnowledge as $item) {
                    //老数据需要单独处理id
                    $child['children'][] = [
                        'id' => $withId ? $item->id : $item->yyid,
                        'name' => $item->title,
                        'type' => 2,
                        'children' => []
                    ];
                }

                $itemData['children'][] = $child;
            }
            $result[] = $itemData;
        }

        return $result;
    }

    public static function infoKnowledge(int $id): array
    {
        $knowledge = ComplianceKnowledgeRepository::getById($id);
        if (!$knowledge) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "知识库文档不存在");
        }

        return [
            'title' => $knowledge->title,
            'intro' => $knowledge->introduce,
            'type' => $knowledge->file_type,
            'link' => $knowledge->file_address,
            'create_time' => $knowledge->created_time
        ];
    }
}
