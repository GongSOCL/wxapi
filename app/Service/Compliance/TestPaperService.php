<?php
declare(strict_types=1);
namespace App\Service\Compliance;

use App\Model\Qa\ComplianceTestPaper;
use App\Repository\ComplianceTestPaperRepository;
use Hyperf\Database\Model\Collection;

/**
 * 试卷服务
 * Class TestPaperService
 * @package App\Service\Compliance
 */
class TestPaperService
{
    /**
     * 根据试卷id获取所有的试题
     * @param array $testPaperIds
     * @return array
     */
    public static function getQuestionItems(array $testPaperIds): array
    {
        //需要对试题进行排序
        $testPapers = ComplianceTestPaperRepository::getTestPapersByIds($testPaperIds);
        if ($testPapers->isEmpty()) {    //末配置题目
            return [];
        }

        $items = [];
        /** @var ComplianceTestPaper $testPaper */
        foreach ($testPapers as $testPaper) {
            /** @var Collection $paperItems */
            $paperItems = $testPaper->testPaperItems;
            if (!$paperItems) {    //试卷末关联试题或者题库
                continue;
            }

            //获取问卷项目
            $questionItems = null;
            if ($testPaper->isTestPaperQuestion()) {    //问卷出题
                $settingQuestionYyids = $paperItems->pluck('question_yyid')->toArray();
                if (empty($settingQuestionYyids)) {
                    continue;
                }
                $questionItems = QuestionService::filterQuestionItemsWithQuestionYYIDS($settingQuestionYyids);
            } else {    //试题出题
                $itemYYIDS = $paperItems->pluck('item_yyid')->all();
                $questionItems = QuestionService::filterQuestionItemsWithItemYYIDS($itemYYIDS);
            }

            //合并数据
            if ($questionItems && $questionItems->isNotEmpty()) {
                $items = array_merge($items, $questionItems->toArray());
            }
        }

        return $items;
    }
}
