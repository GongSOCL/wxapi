<?php


namespace App\Service\Compliance;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\ComplianceExam;
use App\Model\Qa\ComplianceQuestionItem;
use App\Model\Qa\ItemsFraction;
use App\Model\Qa\TComplianceQuestionItemsFraction;
use App\Repository\ExaminationResultRepository;
use App\Repository\ItemsFractionRepository;

class ExamCacheService
{
    public static function buildQuestionCacheKey($requestId): string
    {
        return "youyao:exam:questions:{$requestId}";
    }

    public static function buildExamAnswerKey($requestId): string
    {
        return "youyao:exam:answers:{$requestId}";
    }

    private static function buildExamRightKey($requestId): string
    {
        return "youyao:exam:rights:{$requestId}";
    }

    public static function cacheExamTime($examTime, $requestId, $ttl = 86400)
    {
        $key = "youyao:exam:timeh";
        $redis = Helper::getRedis();
        $redis->hSet($key, $requestId, time().'-'.$examTime);
        $redis->expire($key, $ttl);
    }
    public static function getCacheExamTime($cacheRequestId)
    {
        $key = "youyao:exam:timeh";
        $redis = Helper::getRedis();
        $rank = $redis->hGet($key, $cacheRequestId);
        return $rank;
    }
    /**
     * 批量缓存考试试题列表
     * @param $requestId
     * @param array $questionItemIds
     * @param int $ttl
     */
    public static function cacheExamQuestionList($requestId, array $questionItemIds, $ttl = 3600)
    {
        $redis = Helper::getRedis();
        $key = self::buildQuestionCacheKey($requestId);
        $keyNsq = self::buildQuestionCacheKey($requestId.'-nsq');
        $cacheData = $redis->zCard($key);
        if ($cacheData < 1) {
            $pipeline = $redis->pipeline();
            foreach ($questionItemIds as $k => $id) {
                $pipeline->zAdd($key, $k + 1, $id);
            }
            $pipeline->expire($key, $ttl);
            $pipeline->exec();
        }

        $cacheData = $redis->zCard($keyNsq);
        if ($cacheData < 1) {
            $pipeline = $redis->pipeline();
            foreach ($questionItemIds as $k => $id) {
                $pipeline->zAdd($keyNsq, $k + 1, $id);
            }
            $pipeline->expire($keyNsq, $ttl);
            $pipeline->exec();
        }

        return $redis->zRange($key, 0, -1);
    }

    public static function checkRequestAndQuestion(string $requestId, int $questionId)
    {
        $redis = Helper::getRedis();
        $key = self::buildQuestionCacheKey($requestId);

        if (!$redis->exists($key)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试记录不存在");
        }

        # 有考试时间才自动提交
        $rank = $redis->zRank($key, $questionId);
        if ($rank === false) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试题目不存在");
        }
    }

    public static function getNextQuestionId(string $cacheRequestId, int $questionId): array
    {
        $redis = Helper::getRedis();
        $key = self::buildQuestionCacheKey($cacheRequestId);
        if (!$redis->exists($key)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试记录不存在");
        }

        $score = $redis->zScore($key, $questionId);
        if ($score == false) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试题目不存在");
        }

        //
        $score = (int)$score;
        $end = strval($score + 1);
        return $redis->zRangeByScore($key, "($score", $end, ['limit' => 1]);
    }

    public static function checkExamPassed(
        string $requestId,
        int $pass_condition,
        int $type,
        int $exid
    ): array {
        $rightKey = self::buildExamRightKey($requestId);
        $redis = Helper::getRedis();
        $nums = (int)$redis->sCard($rightKey);

        $rightData = $redis->sMembers($rightKey);
        # Todo 【pass_type=1】 如果缓存中答对的题目数量大于考试配置数量 则通过
        if ($type == 1) {
            if ($nums >= $pass_condition) {
                return ['status'=>true,'fraction'=>0,'type'=>$type, 'nums'=>$nums];
            } else {
                return ['status'=>false,'fraction'=>0,'type'=>$type, 'nums'=>$nums];
            }
        } else {
                # Todo  【pass_type=2】 如果缓存中答对的题目总分大于考试配置总分则通过
                # 根据考试id 题目id 查询该提分数
                # 试卷 考试 动态分数表
                $fractionData = ExaminationResultRepository::getTestExamFraction($exid);
            if ($fractionData) {
                $fractionData = json_decode(json_encode($fractionData), true);
            }
                # 获取所有答对题
                #   --- 1顺序出题 题库题目出题   随机出题 题库出题
                #   --- 2 随机出题 题库出题
                #   --- 3 随机出题 题目出题
                $fraction = 0;
            if ($fractionData[0]['type'] == 1) {
                foreach ($fractionData as $item) {
                    if (in_array($item['item_id'], $rightData)) {
                        $fraction += $item['fraction'];
                    }
                }
            } elseif ($fractionData[0]['type'] == 2) {    # 题库+随机
                # 查看 答对的每一道题属于某题库
                $fractionData = ExaminationResultRepository::getTestPaperQuestionItemFraction($exid, $rightData);
                $fractionData = $fractionData->toArray();
                $fractionData = array_column($fractionData, 'fraction');
                $fraction = array_sum($fractionData);
            } else {  # 题目+随机
                # 获取每题分数
                $fractionData = ExaminationResultRepository::getTestExamFraction($exid);
                $fractionData = $fractionData->toArray();
                $fraction = count($rightData) * $fractionData['fraction'];
            }

                # 若考试设定为0分可通过 则直接通过
            if ($pass_condition == 0) {
                return ['status' => true, 'fraction' => $fraction, 'type' => $type, 'nums'=>$nums];
            } else {  # 否则 判断是否通过分数线
                # 获取该考试下所有缓存
                if ($rightData) {
                    if ($fraction >= $pass_condition) {
                        return ['status' => true, 'fraction' => $fraction, 'type' => $type, 'nums'=>$nums];
                    } else {
                        return ['status' => false, 'fraction' => $fraction, 'type' => $type, 'nums'=>$nums];
                    }
                } else {
                    return ['status' => false, 'fraction' => 0, 'type' => $type, 'nums'=>$nums];
                }
            }
        }
    }

    /**
     * 缓存考试题目
     * @param string $cacheRequestId
     * @param ComplianceQuestionItem $question
     * @param bool $isRight
     * @param array $optionIds
     * @param string $content
     * @param int $ttl
     */
    public static function cacheExamAnswer(
        string $cacheRequestId,
        ComplianceQuestionItem $question,
        bool $isRight,
        array $optionIds = [],
        string $content = "",
        int $ttl = 3600,
        $openId = ""
    ) {
        $redis = Helper::getRedis();

        //缓存答题选项和内容
        $answerKey = self::buildExamAnswerKey($cacheRequestId);
        $answerKeyNsq = self::buildExamAnswerKey($cacheRequestId.'-nsq');
        $content = [
            'options' => $optionIds,
            'content' => (string)$content,
            'is_right' => $isRight ? 1 : 0,
            'type' => $question->type,
            'openid' => (string) $openId
        ];
        $redis->hSet(
            $answerKey,
            (string)$question->id,
            json_encode($content, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
        );
        $redis->expire($answerKey, $ttl);
        $redis->hSet(
            $answerKeyNsq,
            (string)$question->id,
            json_encode($content, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
        );
        $redis->expire($answerKeyNsq, $ttl);

        //处理正确数量统计列表
        $rightKey = self::buildExamRightKey($cacheRequestId);
        $rightKeyNsq = self::buildExamRightKey($cacheRequestId.'-nsq');
        $redis->sRem($rightKey);
        if ($isRight) {
            $redis->sAdd($rightKey, $question->id);
        } else {
            if ($redis->sIsMember($rightKey, $question->id)) {
                $redis->sRem($rightKey, $question->id);
            }
        }
        $redis->expire($rightKey, $ttl);

        $redis->sRem($rightKeyNsq);
        if ($isRight) {
            $redis->sAdd($rightKeyNsq, $question->id);
        } else {
            if ($redis->sIsMember($rightKeyNsq, $question->id)) {
                $redis->sRem($rightKeyNsq, $question->id);
            }
        }
        $redis->expire($rightKeyNsq, $ttl);
    }

    public static function checkFinished(string $cacheRequestId): bool
    {
        $redis = Helper::getRedis();
        $key = self::buildQuestionCacheKey($cacheRequestId);
        if (!$redis->exists($key)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试记录不存在");
        }

        //获取最后一条记录
        $item = $redis->zRevRange($key, 0, 0);
        if (empty($item)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "考试记录不存在");
        }
        $answerKey = self::buildExamAnswerKey($cacheRequestId);
        $lastId = current($item);

        return $redis->hExists($answerKey, $lastId);
    }

    public static function clearOldRequests($examId, $userId, $newRequestID, $ttl = 14400, $clearInterval = 7200)
    {
        $keyOff = sprintf("%s:%s", $examId, $userId);
        $key = self::buildUserExamParticipateKey($keyOff);
        $redis = Helper::getRedis();
        $now = time();

        //添加新记录(重置过期时间)
        $redis->zAdd($key, $now, (string)$newRequestID);
        $redis->expire($key, $ttl);

        //移除超过一定时限的key
        $list = $redis->zRangeByScore($key, '-inf', strval($now - $clearInterval));
        if (!empty($list)) {
            $remKey = array_reduce($list, function ($carry, $item) use ($keyOff) {
                $keyItem = sprintf("%s:%s", $keyOff, $item);
                $carry[] = self::buildQuestionCacheKey($keyItem);
                return $carry;
            }, []);

            array_unshift($list, $key);

            //删除数据并移除入口key
            $pipeline = $redis->pipeline();
            $pipeline->del($remKey);
            call_user_func_array([$pipeline, 'zRem'], $list);
            $pipeline->exec();
        }
    }


    private static function buildUserExamParticipateKey($key): string
    {
        return sprintf("yoyao:user_exam:start:%s", $key);
    }

    public static function getQuestionIds(string $cacheRequestId): array
    {
        $key = self::buildQuestionCacheKey($cacheRequestId);
        $redis = Helper::getRedis();
        return $redis->zRangeByScore($key, '-inf', 'inf');
    }

    public static function getAnswers($requestId): array
    {
        $key = self::buildExamAnswerKey($requestId);
        $redis = Helper::getRedis();
        return $redis->hGetAll($key);
    }


    /**
     * 缓存答案及题目 用户信息
     * @param $requestId
     * @param array $questionItemIds
     * @param int $ttl
     */
    public static function cacheExamAnswerAndUserInfo($requestId, array $questionItemIds, $ttl = 3600)
    {
        $redis = Helper::getRedis();
        $pipeline = $redis->pipeline();
        $key = self::buildQuestionCacheKey($requestId);
        foreach ($questionItemIds as $k => $id) {
            $pipeline->zAdd($key, $k + 1, $id);
        }
        $pipeline->expire($key, $ttl);
        $pipeline->exec();
    }
}
