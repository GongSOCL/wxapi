<?php
declare(strict_types=1);

namespace App\Service\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;

class SmsCacheService
{
    public static function isLock($mobile, $ttl = 60): bool
    {
        return (bool) Helper::getRedis('pod')
            ->set(self::buildMobileLockKey($mobile), 1, ['nx', 'ex' => $ttl]);
    }

    public static function delMobileCode($mobile)
    {
        Helper::getRedis('pod')
            ->del(self::buildMobileKey($mobile));
    }

    public static function cacheCode($mobile, $code, $ttl = 300)
    {
        $key = self::buildMobileKey($mobile);
        $redis = Helper::getRedis('pod');
        $redis->set($key, $code);
        $redis->expire($key, $ttl);
    }

    private static function buildMobileKey($mobile): string
    {
        return sprintf("youyao:sms:verify:%s", $mobile);
    }

    private static function buildMobileLockKey($mobile): string
    {
        return sprintf("youyao:sms:lock:%s", $mobile);
    }

    private static function buildPreSendSmsList(): string
    {
        return "youyao:sms:presend:recent";
    }
    private static function buildPreSendSmsInfo($sid): string
    {
        return sprintf("youyao:sms:presend:info:%s", $sid);
    }

    public static function getCodeFromCache($mobile): string
    {
        $key = self::buildMobileKey($mobile);
        $redis = Helper::getRedis('pod');
        return (string) $redis->get($key);
    }

    public static function preSendSms($mobile, string $sid, $code, $ttl = 120)
    {
        //将数据放进hash里面
        $infoKey = self::buildPreSendSmsInfo($sid);

        //将sid放进zset里面, 并设置过期
        $end = time() + $ttl;
        $recentPresentKey = self::buildPreSendSmsList();

        $redis = Helper::getRedis('pod');
        $redis->pipeline()
            ->hMSet($infoKey, [
                'mobile' => $mobile,
                'code' => $code
            ])->expire($infoKey, $ttl)
            ->zAdd($recentPresentKey, $end, (string)$sid)
            ->exec();
    }

    public static function getPreSendInfo($sid, $ttl = 600): array
    {
        $recentPresentKey = self::buildPreSendSmsList();
        $redis = Helper::getRedis('pod');
        $redis->zRemRangeByScore($recentPresentKey, '-inf', strval(time() - $ttl));

        if (false == $redis->zScore($recentPresentKey, (string)$sid)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "验证码不存在");
        }

        $infoKey = self::buildPreSendSmsInfo($sid);
        $info = $redis->hMGet($infoKey, ['mobile', 'code']);
        if (!$info) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "验证码不存在");
        }

        return [$info['mobile'], $info['code']];
    }

    /**
     * 添加到最近发送队列
     * @param $mobile
     * @param $code
     * @param int $ttl      验证码过期时间
     */
    public static function addRecent($mobile, $code, $ttl = 60)
    {
        $key = self::buildRecentSendZSet();
        $redis = Helper::getRedis('pod');
        $val = sprintf("%s:%s", $mobile, $code);
        $now = time();
        $redis->pipeline()
            ->zAdd($key, time() + $ttl, $val)               //添加新元素
            ->expireAt($key, $now + 3600)                   //续租时间， 1小时内如果没有消息则自动过期
            ->zRemRangeByScore($key, '-inf', sprintf("(%d", $now))      //移除旧数据,避免过快增长
            ->exec();
    }

    private static function buildRecentSendZSet(): string
    {
        return "youyao:sms:recent";
    }

    public static function clearPreSend(string $sid)
    {
        $recentPresentKey = self::buildPreSendSmsList();
        $infoKey = self::buildPreSendSmsInfo($sid);
        $redis = Helper::getRedis('pod');
        $redis->pipeline()
            ->unlink($infoKey)
            ->zRem($recentPresentKey, $sid)
            ->exec();
    }

    public static function failRemoveCode($mobile, $code)
    {
        $key = self::buildMobileKey($mobile);
        $redis = Helper::getRedis('pod');
        $redis->watch($key);
        try {
            $cacheCode = $redis->get($key);
            if ($cacheCode != $code) {
                $redis->unwatch();
                return;
            }

            $redis->multi();
            $redis->del($key);
            $redis->exec();
        } catch (\Exception $e) {
            $redis->unwatch();
        }
    }
}
