<?php


namespace App\Service\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\Region;
use App\Repository\RegionRepository;

class CityService
{
    /**
     * 获取下一级区域
     * @param $parentId
     * @param int $parentLevel
     * @return array
     */
    public static function getNextRegion($parentId, int $parentLevel = 1): array
    {
        $region = RegionRepository::getById($parentId);
        if (!$region) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "区域不存在");
        }

        if ($region->level != $parentLevel) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "区域不存在,或类型不正确");
        }

        $reps = RegionRepository::getNextOne($parentId);
        $data = [];
        /** @var Region $item */
        foreach ($reps as $item) {
            $data[] = [
                'id' => $item->id,
                'name' => $item->region_name
            ];
        }

        return $data;
    }
}
