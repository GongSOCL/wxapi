<?php
declare(strict_types=1);

namespace App\Service\Common;

use App\Helper\Helper;
use Youyao\Framework\Components\Msg\MessageCallbackAbstract;

class MsgCallback extends MessageCallbackAbstract
{
    /**
     * 短信回调
     * @param $msgId
     * @param bool $isSuccess
     * @return string
     */
    public function smsCallback($msgId, bool $isSuccess): string
    {
        $log = Helper::getLogger();
        $mobile = $code = "";
        try {
            //根据消息id获取存储数据
            [$mobile, $code] = SmsCacheService::getPreSendInfo((string)$msgId);

            if ($isSuccess) {
                //成功并且非生产环境则推送到最近发送队列里面
                false == Helper::isOnline() && SmsCacheService::addRecent($mobile, $code);
            } else {
                //发送失败删除缓存验证码
                SmsCacheService::failRemoveCode($mobile, $code);
            }

            //清除预发送记录
            SmsCacheService::clearPreSend((string)$msgId);
            $log->debug("sms_callback_success", [
                'mobile' => $mobile,
                'sid' => $msgId
            ]);
        } catch (\Exception $e) {
            $log->error('sms_callback_error', [
                'msgId' => $msgId,
                'is_success' => $isSuccess,
                'mobile' => $mobile ? $mobile : '',
                'code' => $code ? $code : '',
                'err' => $e->getMessage()
            ]);
        }
        return "";
    }
}
