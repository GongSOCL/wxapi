<?php
declare(strict_types=1);


namespace App\Service\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;

/**
 * 高德地图
 * Class AmapService
 * @package App\Service\Common
 */
class AmapService
{
    private $signKey = "";

    private $key;

    public function __construct()
    {
        if (empty($this->key)) {
            $this->key = env('AMAP_MAP_KEY');
            $this->signKey = env('AMAP_MAP_SIGN_KEY', '');
        }
    }

    private function sign(array $data)
    {
        ksort($data, SORT_STRING);
        $tmpStr = '';
        foreach ($data as $key => $value) {
            if (strlen($tmpStr) == 0) {
                $tmpStr .= $key . "=" . $value;
            } else {
                $tmpStr .= "&" . $key . "=" . $value;
            }
        }
        $tmpStr .= $this->signKey;
        $signStr = md5($tmpStr);
        return $signStr;
    }
    public function transferCoordinate($lng, $lat, $type = 'gps')
    {
        $url = 'https://restapi.amap.com/v3/assistant/coordinate/convert';
        $params = [
            'key' => $this->key,
            'locations' => sprintf('%s,%s', $lng, $lat),
            'coordsys' => $type,
            'output' => 'JSON'
        ];
        if ($this->signKey) {
            $sign = $this->sign($params);
            $params['sig'] = $sign;
        }
        Helper::getLogger()->info("amap_transfer_coordinate", [
            'params' => $params
        ]);
        $resp = Helper::getHttpClient($url)->request('GET', '', [
            'query' => $params
        ]);
        $data = json_decode($resp->getBody()->getContents(), true);
        if (!(isset($data['status']) && $data['status'] == 1)) {
            throw new BusinessException(ErrorCode::BAIDU_MAP_REVERSE_COORD_ERROR, $data['info']);
        }
        [$retLng, $retLat] = explode(',', $data['locations']);
        return [$retLng, $retLat];
    }

    public function reverseCoordinate($lng, $lat): array
    {
        $url = "https://restapi.amap.com/v3/geocode/regeo";
        $params = [
            'key' => $this->key,
            'location' => sprintf('%s,%s', $lng, $lat),
            'radius' => 0,
            'extensions' => 'base',
            'batch' => "false",
            'output' => 'JSON'
        ];
        if ($this->signKey) {
            $sign = $this->sign($params);
            $params['sig'] = $sign;
        }
        $res = Helper::getHttpClient($url)->request('GET', '', [
            'query' => $params
        ]);
        $data = json_decode($res->getBody()->getContents(), true);
        if (!(isset($data['status']) && $data['status'] == 1)) {
            throw new BusinessException(ErrorCode::BAIDU_MAP_REVERSE_COORD_ERROR, $data['info']);
        }
        return $data['regeocode'];
    }
}
