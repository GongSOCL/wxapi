<?php


namespace App\Service\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\RepsComment;
use App\Repository\DrugInterpretRepository;
use App\Repository\RepsCommentRepository;

class CommentService
{

    /**
     * 添加评论
     * @param $resourceId
     * @param $comment
     * @param int $parentCommentId
     * @return RepsComment
     */
    public static function addInterceptComment($resourceId, $comment, int $parentCommentId = 0): RepsComment
    {
        $user = Helper::getLoginUser();
        $interpret = DrugInterpretRepository::getById($resourceId);
        if (!$interpret) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "文献不存在");
        }
        $parentYYID = "";
        if ($parentCommentId > 0) {
            $parentComment = RepsCommentRepository::getCommentById($parentCommentId);
            if ($parentComment && $parentComment->comment_pyyid) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "目前仅允许两级评论");
            }
            $parentComment && $parentYYID = $parentComment->yyid;
        }

        return RepsCommentRepository::addComment($interpret->yyid, $user, $comment, $parentYYID);
    }

    /**
     * 删除评论
     * @param $id
     */
    public static function deleteComment($id)
    {
        $user = Helper::getLoginUser();
        $comment = RepsCommentRepository::getCommentById($id);
        if (!$comment) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "评论不存在");
        }

        if ($comment->user_yyid != $user->yyid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "仅允许删除自己的评论");
        }

        if (!$comment->comment_pyyid) {
            RepsCommentRepository::deleteParentComment($comment);
        } else {
            RepsCommentRepository::deleteSubComment($comment);
        }
    }
}
