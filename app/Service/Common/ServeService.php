<?php
declare(strict_types=1);

namespace App\Service\Common;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\TaskManageHelper;
use App\Model\Qa\DrugProduct;
use App\Model\Qa\DrugSeries;
use App\Model\Qa\RepsServeScopeP;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\SupplierMemberDetail;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;
use App\Repository\AgentRepository;
use App\Repository\DrugProductRepository;
use App\Repository\DrugSeriesRepository;
use App\Repository\HospitalMonthTransPRepository;
use App\Repository\HospitalStarPRepository;
use App\Repository\NewGroup\NewDoctorUserinfoRepository;
use App\Repository\RepsServeScopePRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\SalesTargetMonthRepository;
use App\Repository\SupplierRepository;
use App\Repository\TRepsAgentStoreRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Service\Transaction\TransactionService;
use App\Service\User\UserService;
use App\Service\Wechat\WechatBusiness;
use DateInterval;
use DatePeriod;
use DateTime;
use Grpc\Common\CommonPageResponse;
use Grpc\Task\YouyaoHospitalEsSyncAllTask;
use Grpc\Task\YouyaoHospitalEsSyncSingleTask;
use Grpc\Wxapi\User\AgentItem;
use Grpc\Wxapi\User\AgentServeHospitalsReply;
use Grpc\Wxapi\User\AgentSpecifyHospitalsReply;
use Grpc\Wxapi\User\HospitalItem;
use Grpc\Wxapi\User\ServeListResponse;
use Grpc\Wxapi\User\ServiceAgentsReply;
use Grpc\Wxapi\User\UserServeItem;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use Hyperf\Utils\Coroutine;
use function Clue\StreamFilter\fun;

class ServeService
{

    /**
     * 申请药品
     * @param int $hospitalId
     * @param int $seriesId
     */
    public static function apply(int $hospitalId, int $seriesId)
    {
        $user = Helper::getLoginUser();

        UserService::checkMobileBind();

        //检查医院
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }

        //检查药品
        $series = DrugSeriesRepository::getById($seriesId);
        if (!$series) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "药品不存在");
        }

        // 判断用户是否属于供应商体系  成员不可在优药前端自行申请医院
        $agent = AgentRepository::findByUser($user);
        if ($agent->supplier_id!=0) {
            // 属于供应商体系
            $supplierMember = SupplierRepository::getMemberInfoByUserid($agent->supplier_id, $user->uid);
            if ($supplierMember) {
                // 检查是否已经有申请
                $reps = SupplierRepository::getMemberDetailByUserId(
                    $agent->supplier_id,
                    $supplierMember->id,
                    $user->uid,
                    $hospitalId,
                    $seriesId,
                    SupplierMemberDetail::TYPE_1
                );

                if ($reps&&$reps->status==DataStatus::REGULAR) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "请等待解除申请审批完成后再次申请");
                }
                if ($reps&&$reps->status==2) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该药品或医院已被申请，请重新选择");
                }
                //添加申请记录 申请记录属于供应商 不与个人挂钩
                $id = SupplierRepository::addSupplierMemberDetail(
                    $agent->supplier_id,
                    $supplierMember->id,
                    $user->uid,
                    $hospitalId,
                    $seriesId
                );

                return [
                    'id' => $id,
                    'type' => 'supplier_apply'
                ];
            } else {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "请联系服务商管理员分配服务");
            }
        } else {
            // 普通代表
            //检查是否已经有申请
            $resp = RepsServeScopeSRepository::getHospitalSeriesServe($hospital, $series);
            if ($resp) {
                //自己已经申请过了
                if ($resp->user_yyid == $user->yyid) {
                    if ($resp->status == 5) {
                        throw new BusinessException(ErrorCode::BUSINESS_ERROR, "请等待解除申请审批完成后再次申请");
                    }
                    return $resp;
                }
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "该药品已被申请，请重新选择");
            }

            //添加申请记录
            $serve = RepsServeScopeSRepository::addServe($user, $hospital, $series);
            //发送审批申请
            AuditService::serveApplyAudit($serve->id);

            return [
                'id' => $serve->id,
                'type' => 'agent_apply'
            ];
        }
    }

    /**
     * 解绑申请
     * @param int $serveId
     */
    public static function remove(int $serveId)
    {
        $user = Helper::getLoginUser();

        UserService::checkMobileBind();

        $serveProduct = RepsServeScopePRepository::getById($serveId);
        if (!$serveProduct) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务产品不存在");
        }

        //检查数据
        $serve = RepsServeScopeSRepository::getByServeProduct($serveProduct);
        if (!$serve) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务不存在");
        }
        if ($serve->user_yyid != $user->yyid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "只允许解绑自己的申请");
        }
        if ($serve->status != DataStatus::REGULAR) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "当前服务尚未申请通过");
        }

        Db::beginTransaction();
        try {
            //判断用户是否属于供应商体系
            $user = Helper::getLoginUser();
            $agent = AgentRepository::findByUser($user);
            $supplierMember = SupplierRepository::getOneMemberInfoByYyid($agent->supplier_id, $user->yyid);
            if ($supplierMember) {
                if ($supplierMember->is_leader==0) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "请联系服务商管理员解除服务");
                }
                // 属于供应商体系
                // 检查是否已经有申请记录 有的直接解除
                $reps = SupplierRepository::getMemberDetailByUserId(
                    $supplierMember->supplier_id,
                    $supplierMember->id,
                    $user->uid,
                    $serve->hospital_yyid,
                    $serve->series_yyid,
                    SupplierMemberDetail::TYPE_2
                );
                if ($reps) {
                    SupplierRepository::unbindMember($reps->id);
                }
            }

            //变更申请到解绑申请状态
            $saveResult = RepsServeScopeSRepository::removeServe($serve);

            Db::commit();
            //发送解绑申请消息给审核人员
            AuditService::serveRemoveAudit($serve->id);
        } catch (\Exception $exception) {
            Db::rollBack();
            throw $exception;
        }
    }

    /**
     * 根据医院获取服务产品列表及详情
     * @param int $hospitalId
     * @return array
     * @throws \Exception
     */
    public static function getServeProductDetailList(int $hospitalId): array
    {
        $user = Helper::getLoginUser();
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }
        $hospitalInfo = [
            'hospital_name' => $hospital->hospital_name,
            'hospital_level' => $hospital->level,
            'hospital_addr' => $hospital->addr
        ];
        $products = [];

        //获取医院服务的药品
        $resp = RepsServeScopePRepository::getByUserAndHospital($user, $hospital);
        if ($resp->isEmpty()) {
            return [$hospitalInfo, $products];
        }

        $productYYIDS = $resp->pluck('product_yyid')->all();
        $seriesYYID = $resp->pluck('series_yyid')->all();

        //获取系列药品信息
        $ss = DrugSeriesRepository::getByYYIDS($seriesYYID);
        $ssMap = $ss->keyBy('yyid')->all();

        //药品信息
        $ps = DrugProductRepository::getByYYIDS($productYYIDS);
        $productMap = $ps->keyBy('yyid')->all();

        //服务信息
        $hsp = HospitalStarPRepository::getStarByHospitalAndProducts($hospital, $productYYIDS);
        $hspMap = $hsp->keyBy('product_yyid')->all();
        /** @var RepsServeScopeP $item */
        foreach ($resp as $item) {
            $itemProductYYID = $item->product_yyid;

            //获取系列产品信息
            if (!isset($ssMap[$item->series_yyid])) {
                continue;
            }
            /** @var DrugSeries $itemSeries */
            $itemSeries = $ssMap[$item->series_yyid];

            //获取产品信息
            if (!isset($productMap[$itemProductYYID])) {
                continue;
            }
            /** @var DrugProduct $productItem */
            $productItem = $productMap[$itemProductYYID];
            $start = isset($hspMap[$itemProductYYID]) ? $hspMap[$itemProductYYID]->star : 0;

            $products[] = [
                'id'  => $item->id,
                'product_id' => $productItem->id,
                'name' => $productItem->name,
                'status' => self::getRemoveStatusDesc($item->status),
                'star' => $start
            ];
        }

        return [$hospitalInfo, $products];
    }

    /**
     * 获取服务产品的统计信息
     * @param int $serveProductId
     * @return array
     * @throws \Exception
     */
    public static function getServeProductStatistics(int $serveProductId): array
    {
        $user = Helper::getLoginUser();
        $serveProduct = RepsServeScopePRepository::getById($serveProductId);
        if (!$serveProduct) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务记录不存在");
        }
        if ($serveProduct->user_yyid != $user->yyid) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务记录不存在");
        }

        //服务系列
        $serveSeries = RepsServeScopeSRepository::getByServeProduct($serveProduct);
        if (!$serveSeries) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务系列药品记录不存在");
        }
        if (!in_array($serveSeries->status, [0, 1, 5, 6])) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "状态不正确");
        }

        //医院
        $hospital = $serveSeries->hospital;
        if (!$hospital) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医院不存在");
        }

        //产品
        $product = DrugProductRepository::getByYYID($serveProduct->product_yyid);
        if (!$product) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "产品不存在");
        }

        $series = $serveSeries->series;

        //获取总览数据
        [$lastMonth, $overviewData] = TransactionService::statisticsOverviewData($hospital, $product);

        //统计当月数据
        $lastMonthData = TransactionService::statisticOneMonthData($lastMonth, $hospital, $product, $series);

        // 统计过去三个月内的数据
        $lastThreeMonthData = TransactionService::statisticThreeMonthData($hospital, $product, $series);

        //获取过去6个月连续时间内的数据
        $chartData = TransactionService::statisticChartData($hospital, $product, $lastMonth);

        return [$overviewData, $chartData, $lastMonthData, $lastThreeMonthData];
    }

    public static function getServeList(
        int $uid = 0,
        $hospitalId = 0,
        $seriesId = 0,
        $start = "",
        $end = "",
        $current = 1,
        $limit = 10
    ): ServeListResponse {
        $users = null;
        if ($uid) {
            $users = UsersRepository::getUserByUid($uid);
        }
        $hospital = null;
        if ($hospitalId) {
            $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        }
        $series = null;
        if ($seriesId) {
            $series = DrugSeriesRepository::getById($seriesId);
        }
        $resp = RepsServeScopeSRepository::getServeList($users, $hospital, $series, $start, $end, $current, $limit);

        $page = new CommonPageResponse();
        $page->setSize($limit);
        $page->setCurrent($current);
        $page->setTotal($resp->total());
        $page->setPages($resp->lastPage());

        $data = new ServeListResponse();
        $data->setPage($page);
        if ($resp->isEmpty()) {
            return $data;
        }

        /** @var Collection $collect */
        $collect = $resp->getCollection();

        //获取药品
        $seriesMap = [];
        if ($series) {
            $seriesMap[$series->yyid] = $series;
        } else {
            $seriesYYIDS = $collect->pluck('series_yyid')->all();
            if (!empty($seriesYYIDS)) {
                $seriesResp = DrugSeriesRepository::getByYYIDS($seriesYYIDS);
                $seriesMap = $seriesResp->keyBy('yyid')->all();
            }
        }

        //获取医院
        $hospitalMap = [];
        if ($hospital) {
            $hospitalMap[$hospital->yyid] = $hospital;
        } else {
            $hospitalYYIDS = $collect->pluck('hospital_yyid')->all();
            if (!empty($hospitalYYIDS)) {
                $hospitalResp = YouyaoHospitalRepository::getHospitalByYYIDs($hospitalYYIDS);
                $hospitalMap = $hospitalResp->keyBy('yyid')->all();
            }
        }

        //用户
        $userMap = [];
        if ($users) {
            $userMap[$users->yyid] = $users;
        } else {
            $userYYIDS = $collect->pluck('user_yyid')->all();
            if (!empty($userYYIDS)) {
                $userResp = UsersRepository::getUserByYYIDs($userYYIDS);
                $userMap = $userResp->keyBy('yyid')->all();
            }
        }

        $userData = [];
        /** @var RepsServeScopeS $item */
        foreach ($resp->items() as $item) {
            $series = $seriesMap[$item->series_yyid] ?? '';
            $user = $userMap[$item->user_yyid] ?? '';
            $hospital = $hospitalMap[$item->hospital_yyid] ?? '';
            $i = new UserServeItem();
            $i->setId($item->id);
            $i->setStatus($item->status);
            $i->setSeriesName($series ? $series->series_name : '');
            $i->setHospitalName($hospital ? $hospital->hospital_name : '');
            $i->setUserName($user ? $user->name : '');
            $i->setApplyTime((string)$item->created_time);
            $userData[] = $i;
        }
        $data->setList($userData);

        return $data;
    }

    public static function applyOk(int $serveId)
    {
        $serve = RepsServeScopeSRepository::getById($serveId);
        if (!$serve) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务申请不存在");
        }
        if ($serve->status != 0) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务状态不正确，无法审批通过");
        }

        Db::beginTransaction();
        try {
            // 将该医院下所有医生加入该代表
            $newDoctors = NewDoctorUserinfoRepository::getNewDoctorsByHospital($serve->hospital_yyid);
            if ($newDoctors->isNotEmpty()) {
                $user = UsersRepository::getUserByYYID($serve->user_yyid);
                foreach ($newDoctors->toArray() as $item) {
                    NewDoctorUserinfoRepository::createDoctorReps($user->uid, $item['nid']);
                }
            }

            $operateResult = RepsServeScopeSRepository::auditServe($serve, true);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "审核失败");
            }
            YouyaoHospitalRepository::updateStatusNormal($serve->hospital_yyid);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
        self::refreshHospitalEs($serve->hospital->id);
    }

    public static function applyRefused(int $serveId)
    {
        $serve = RepsServeScopeSRepository::getById($serveId);
        if (!$serve) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务申请不存在");
        }

        if ($serve->status != 0) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务状态不正确，无法审批通过");
        }

        RepsServeScopeSRepository::auditServe($serve, false);
    }

    public static function getUserServeProducts(Users $user): Collection
    {
        $resp = RepsServeScopePRepository::getUserServeProducts($user);
        if ($resp->isEmpty()) {
            return $resp;
        }
        $id = $resp->pluck('id')->all();
        return DrugProductRepository::getByIds($id);
    }

    /**
     * 获取代理药品状态
     * @param $seriesStatus
     * @return array
     */
    private static function getRemoveStatusDesc($seriesStatus): array
    {
        $statusItem = [];
        if ($seriesStatus == 6) {
            $statusItem = [
                'status' => $seriesStatus,
                'desc' => '解除服务'
            ];
        } elseif ($seriesStatus == 5) {
            $statusItem = [
                'status' => $seriesStatus,
                'desc' => '解除服务申请中'
            ];
        } elseif ($seriesStatus == 1) {
            $statusItem = [
                'status' => $seriesStatus,
                'desc' => '解除服务'
            ];
        } elseif ($seriesStatus < 1) {
            $statusItem = [
                'status' => $seriesStatus,
                'desc' => '服务审批中'
            ];
        } else {
            $statusItem = [
                'status' => $seriesStatus,
                'desc' => '未知'
            ];
        }

        return $statusItem;
    }

    public static function getUserServeHospitals(Users $users, DrugProduct $product = null): array
    {
        $resp = RepsServeScopePRepository::getUserServeList($users, $product);
        if ($resp->isEmpty()) {
            return [];
        }

        return $resp->pluck('hospital_yyid')->unique()->all();
    }

    /**
     * 解绑审核
     * @param int $serveId
     * @param bool $isOk
     */
    public static function auditUnbind(int $serveId, bool $isOk)
    {
        $serve = RepsServeScopeSRepository::getById($serveId);
        if (!$serve) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "服务申请不存在");
        }

//        if ($serve->status != 5) {
//            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户尚未申请解绑该服务");
//        }

        Db::beginTransaction();
        try {
            if ($isOk) {
                // 该代表将该医院下所有医生清除
                $newDoctors = NewDoctorUserinfoRepository::getNewDoctorsByHospital($serve->hospital_yyid);
                if ($newDoctors->isNotEmpty()) {
                    $user = UsersRepository::getUserByYYID($serve->user_yyid);
                    NewDoctorUserinfoRepository::deleteDoctorsReps(
                        $user->uid,
                        array_column($newDoctors->toArray(), 'nid')
                    );
                }
            }

            RepsServeScopeSRepository::unbindAudit($serve, $isOk);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }

        //刷新服务医院缓存
        self::refreshHospitalEs($serve->hospital->id);
    }

    /**
     * 同步单家医院信息到es里面
     * @param $hospitalId
     */
    public static function refreshHospitalEs($hospitalId)
    {
        $task = new YouyaoHospitalEsSyncSingleTask();
        $task->setId($hospitalId);
        TaskManageHelper::create(0, $task, 'sync-single-hospital')->push();
    }

    /**
     * 同步所有医院信息进es
     */
    public static function syncAllHospitalEs()
    {
        $task = new YouyaoHospitalEsSyncAllTask();
        $task->setDatetime(date('Y-m-d H:i:s'));
        TaskManageHelper::create(0, $task, 'sync-all-hospital-es')->push();
    }

    public static function getUserServeHospitalByUid($userId): AgentServeHospitalsReply
    {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::USER_NOT_EXISTS);
        }

        $resp = RepsServeScopeSRepository::getUserServeHospitals($user);
        $reply = new AgentServeHospitalsReply();
        if ($resp->isEmpty()) {
            return $reply;
        }

        $hospitals = $resp->map(function ($item) {
            $o = new HospitalItem();
            /** @var YouyaoHospital $item */
            $o->setId($item->id);
            $o->setName($item->hospital_name);
            return $o;
        })->all();
        $reply->setHospitals($hospitals);
        return $reply;
    }

    /**
     * 获取代表关于药店项目指定的药店列表信息
     * @param $userId
     * @return AgentSpecifyHospitalsReply
     */
    public static function getUserSpecialHospitalByUid($userId, $keywords = '')
    {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::USER_NOT_EXISTS);
        }
        $hospitals = TRepsAgentStoreRepository::getUserServeHospitals($userId, $keywords);
        $reply = new AgentSpecifyHospitalsReply();
        if ($hospitals->isEmpty()) {
            return $reply;
        }
        $list = $hospitals->map(function ($row) {
            $o = new HospitalItem();
            /** @var YouyaoHospital $row */
            $o->setId($row->id);
            $o->setName($row->hospital_name);
            return $o;
        })->toArray();
        $reply->setHospitals($list);
        return $reply;
    }

    public static function checkUserServerHospital($userId, $hospitalId): bool
    {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::USER_NOT_EXISTS);
        }
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }
        return RepsServeScopeSRepository::isUserServeHospital($user, $hospital);
    }

    public static function checkUserSpecifyServeHospital($userId, $hospitalId): bool
    {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::USER_NOT_EXISTS);
        }
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }
        return TRepsAgentStoreRepository::isUserServeHospital($userId, $hospitalId);
    }

    public static function getStoreAgents($storeId): ServiceAgentsReply
    {
        $agents = TRepsAgentStoreRepository::getStoreAgents($storeId);
        $reply = new ServiceAgentsReply();
        if ($agents->isEmpty()) {
            return $reply;
        }
        $list = [];
        $agents->each(function ($row) use (&$list) {
            $item = new AgentItem();
            $item->setUserId($row->uid);
            $item->setName($row->name);
            $list[] = $item;
        });
        $reply->setAgents($list);
        return $reply;
    }
}
