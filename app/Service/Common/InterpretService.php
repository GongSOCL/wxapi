<?php


namespace App\Service\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\RepsComment;
use App\Model\Qa\Users;
use App\Repository\DrugInterpretRepository;
use App\Repository\RepsCommentRepository;
use App\Repository\ResourceNumRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Service\User\UserService;

class InterpretService
{
    public static function info($id): array
    {
        UserService::checkMobileBind();

        $interpret = DrugInterpretRepository::getById($id);
        if (!$interpret) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "文献不存在");
        }

        $readNum = (int) ResourceNumRepository::getReadNum($interpret->yyid);

        return [
            'type' => $interpret->type,
            'title' => $interpret->title,
            'sub_title' => $interpret->sub_title,
            'content' => $interpret->content,
            'read_num' => $readNum
        ];
    }

    /**
     * 获取文献评论
     * @param $id
     * @return array
     */
    public static function getInterpretComments($id): array
    {
        $loginUser = Helper::getLoginUser();
        $interpret = DrugInterpretRepository::getById($id);
        if (!$interpret) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "文献不存在");
        }

        $comments = RepsCommentRepository::getResourceComments($interpret->yyid);
        $total = 0;
        $data = [];
        if ($comments->isNotEmpty()) {
            //获取用户映射
            $userYYIDS = [];
            /** @var RepsComment $v */
            foreach ($comments as $v) {
                $v->user_yyid && $userYYIDS[] = $v->user_yyid;
            }
            $userMap = [];
            if (!empty($userYYIDS)) {
                $users = UsersRepository::getUserWithWechat($userYYIDS);
                $userMap = $users->keyBy("yyid")->all();
            }

            //取出父级评论id
            $topComments = $comments->filter(function ($val) {
                return (string) $val->comment_pyyid == "";
            });

            //取出子级评论
            $secondComments = $comments->filter(function ($val) {
                return (string) $val->comment_pyyid != "";
            });
            $secondCommentsMap =  $secondComments->groupBy("comment_pyyid")->all();

            //遍历父级
            /** @var RepsComment $tc */
            foreach ($topComments as $tc) {
                $item = self::buildCommentFromItem($tc, $loginUser, $userMap);
                ++$total;
                //遍历子级
                if (isset($secondCommentsMap[$tc->yyid])) {
                    foreach ($secondCommentsMap[$tc->yyid] as $stc) {
                        ++$total;
                        $item['children'][] = self::buildCommentFromItem($stc, $loginUser, $userMap);
                    }
                }
                $data[] = $item;
            }
        }

        return [$total, $data];
    }

    private static function buildCommentFromItem(RepsComment $tc, Users $loginUser, array $userMap = []): array
    {
        $defaultAvatar = "";
        $name = "";
        $avatar = $defaultAvatar;
        if (isset($userMap[$tc->user_yyid])) {
            $name = $userMap[$tc->user_yyid]->name ?: ($userMap[$tc->user_yyid]->nickname ?: '');
            $avatar = $userMap[$tc->user_yyid]->headimgurl ?: $defaultAvatar;
        }

        $isDelete = $tc->user_yyid == $loginUser->yyid ? 1 : 0;
        return [
            'id' => $tc->id,
            'comment' => $tc->content,
            'comment_time' => Helper::formatInputDate(strval($tc->created_time), 'Y-m-d H:i'),
            'name' => $name,
            'avatar' => $avatar,
            'isDelete' => $isDelete,
            'children' => []
        ];
    }

    /**
     * 查看文献
     * @param $id
     */
    public static function viewInterpret($id)
    {
        UserService::checkMobileBind();

        $interpret = DrugInterpretRepository::getById($id);
        if (!$interpret) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "文献不存在");
        }

        $rn  = ResourceNumRepository::findByResourceYYID($interpret->yyid);
        if (!$rn) {
            ResourceNumRepository::addResourceRead($interpret->yyid);
        } else {
            ResourceNumRepository::incrReadNum($interpret->yyid);
        }
    }
}
