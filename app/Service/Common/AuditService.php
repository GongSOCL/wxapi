<?php
declare(strict_types=1);


namespace App\Service\Common;

use App\Constants\DataStatus;
use App\Helper\Helper;
use App\Model\Qa\RepsServeScopeS;
use App\Repository\AgentRepository;
use App\Repository\RepsAcademicHelpRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\UsersRepository;
use App\Repository\WechatUserRepository;
use App\Service\Wechat\WechatBusiness;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Coroutine;

class AuditService
{
      /**
     * 获取审核人员的微信信息
     * @return array
     */
    private static function getAuditorWechatOpenId():array
    {
        //获取接收人微信信息
        $auditorStr = (string) env('AUDITOR');
        $auditors = explode(',', $auditorStr);
        if (empty($auditors)) {
            return [];
        }
        $wechatUsers = WechatUserRepository::getWechatInfoByUserMobile($auditors);
        if ($wechatUsers->isEmpty()) {
            return [];
        }

        return $wechatUsers->pluck('openid')->all();
    }

    /**
     * 根据服务id获取其相关信息(服务记录,系列药品信息,服务医院,服务代表信息,服务代表所属公司信息)
     * @param $serveId
     * @return array
     */
    private static function getServeInfo($serveId): array
    {
        $serve = RepsServeScopeSRepository::getById($serveId);
        if (!$serve) {
            return [];
        }

        $serveUser = $serve->users;
        if (!$serveUser) {
            return [];
        }
        $serveUserCompany = $serveUser->company;

        $serveHospital = $serve->hospital;
        if (!$serveHospital) {
            return [];
        }

        $serveSeries = $serve->series;
        if (!$serveSeries) {
            return  [];
        }

        $serveAgent = AgentRepository::getAgentByUser($serveUser);
        if (!$serveAgent) {
            return [];
        }

        return [$serve, $serveSeries, $serveAgent, $serveHospital, $serveUserCompany];
    }

    /**
     * 发送代理药品申请给审批者
     */
    public static function serveApplyAudit($serveId)
    {
        Coroutine::create(function () use ($serveId) {
            $log = Helper::getLogger();

            $reply = self::getServeInfo($serveId);
            if (!$reply) {
                return;
            }
            [$serve, $serveSeries, $serveAgent, $serveHospital, $serveUserCompany] = $reply;

            //获取审核人员人微信信息
            $openIds = self::getAuditorWechatOpenId();
            if (empty($openIds)) {
                $log->warning("drug_serve_remove_apply_auditor_empty", [
                    'serve_id' => $serve->id
                ]);
                return;
            }

            //给审核人员发送审核消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendServeAudit(
                    $openIds,
                    $serveSeries->series_name,
                    $serveHospital->hospital_name,
                    $serveAgent->truename,
                    $serveUserCompany ? $serveUserCompany->name : ''
                );
        });
    }

    /**
     * 发送审核解绑申请消息
     * @param int $serveId
     */
    public static function serveRemoveAudit(int $serveId)
    {
        Coroutine::create(function () use ($serveId) {
            $log = Helper::getLogger();

            $reply = self::getServeInfo($serveId);
            if (!$reply) {
                return;
            }
            /** @var RepsServeScopeS $serve */
            [$serve, $serveSeries, $serveAgent, $serveHospital, $serveUserCompany] = $reply;

            $openIds = self::getAuditorWechatOpenId();
            if (empty($openIds)) {
                $log->warning("drug_serve_remove_apply_auditor_empty", [
                    'serve_id' => $serve->id
                ]);
                return;
            }

            //发送解除申请模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendRemoveServeAudit(
                    $openIds,
                    $serveSeries->series_name,
                    $serveHospital->hospital_name,
                    $serveAgent->truename,
                    $serveUserCompany ? $serveUserCompany->name : ''
                );
        });
    }

    public static function helpAudit($helpId)
    {
        go(function () use ($helpId) {
            $help = RepsAcademicHelpRepository::getById($helpId);
            if (!$help) {
                return;
            }

            $user = UsersRepository::getUserByYYID($help->user_yyid);
            if (!$user) {
                return;
            }

            $userCompany = $user->company;
            $companyName = $userCompany ? $userCompany->name : '';
            $openIds = self::getAuditorWechatOpenId();
            if (empty($openIds)) {
                return;
            }

            //发送解除申请模板消息
            ApplicationContext::getContainer()
                ->get(WechatBusiness::class)
                ->sendHelp(
                    $openIds,
                    $user->name,
                    $help->content,
                    $companyName
                );
        });
    }
}
