<?php
declare(strict_types=1);


namespace App\Service\Common;

use App\Constants\DataStatus;
use GuzzleHttp\Exception\GuzzleException;
use Hyperf\Utils\ApplicationContext;

class GeoService
{

    /**
     * 地理位置逆编码.
     *
     * @param float $lng 经度
     * @param float $lat 纬度
     * @param string $coordinateType    坐标系统类型
     * @return false|string
     * @throws GuzzleException
     */
    public static function reverseGeo($lng, $lat, $coordinateType = DataStatus::COORDINATE_GPS)
    {
        $amap = new AmapService();
        if ($coordinateType != DataStatus::COORDINATE_GCJ02) {
            [$retLng, $retLat] = $amap->transferCoordinate($lng, $lat, $coordinateType);
        } else {
            $retLng = $lng;
            $retLat = $lat;
        }

        $locations = $amap->reverseCoordinate($retLng, $retLat);

        return $locations['formatted_address'];
    }

    public static function transferCoordinate($lng, $lat, $coordinateType = 'gps'): array
    {
        $amap = new AmapService();
        return $amap->transferCoordinate($lng, $lat, $coordinateType);
    }
}
