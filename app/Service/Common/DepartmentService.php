<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\SystemDepart;
use App\Repository\AgentServeDepartRepository;
use App\Repository\DepartmentRepository;
use App\Repository\SystemDepartRepository;
use Hyperf\Di\Annotation\Inject;

class DepartmentService
{

    /**
     * @Inject()
     * @var DepartmentRepository
     */
    private $departmentRepository;

    public static function listSystemDeparts($keywords = ""): array
    {
        $systemDeparts = SystemDepartRepository::searchSystemDeparts($keywords);
        if ($systemDeparts->isEmpty()) {
            return [];
        }

        $result = [];
        foreach ($systemDeparts as $systemDepart) {
            $result[] = [
                'id' => $systemDepart->id,
                'name' => $systemDepart->name
            ];
        }

        return $result;
    }

    /**
     * 带分页列出部门列表
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return array
     */
    public static function listSystemDepartsWithPage($keywords = "", $current = 1, $limit = 10): array
    {
        $resp = SystemDepartRepository::searchSystemDepartsWithPage($keywords, $current, $limit);

        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $resp->perPage()
        ];
        $data = [];
        if ($resp->isNotEmpty()) {
            /** @var SystemDepart $v */
            foreach ($resp->items() as $v) {
                $data[] = [
                    'id' => $v->id,
                    'name' => $v->name
                ];
            }
        }

        return [$page, $data];
    }

    /**
     * 获取用户服务科室.
     *
     * @param int $hospitalId
     * @param string $keywords
     * @return array
     */
    public static function getServeHospitalDeparts($hospitalId = 0, $keywords = ""): array
    {
        $user = Helper::getLoginUser();

        $departs = AgentServeDepartRepository::getUserServeDeparts($user, $hospitalId, $keywords);
        if ($departs->isEmpty()) {
            return [];
        }

        $result = [];
        foreach ($departs as $systemDepart) {
            $result[] = [
                'id' => $systemDepart->id,
                'name' => $systemDepart->name
            ];
        }

        return $result;
    }

    public static function addServe($hospitalId, array $departIds)
    {
        $user = Helper::getLoginUser();

        // 获取医院已经关联的id
        $resp = AgentServeDepartRepository::filterServeDeparts($user, $hospitalId, $departIds);
        $existsDepartIds = $resp->pluck('depart_id')
            ->unique()
            ->toArray();

        // 对比出需要添加的
        [$delIds, $addIds] = Helper::diff($existsDepartIds, $departIds);

        // 批量添加关联关系
        if (!empty($addIds)) {
            $operateResult = AgentServeDepartRepository::batchAddUserHospitalDeparts($user, $hospitalId, $addIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::VISIT_ADD_DEPART_FAIL);
            }
        }
    }


    /**
     * @param $hyyid
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getDepartmentInfo($hyyid, $page, $pageSize)
    {

        $departmentInfo = $this->departmentRepository->getDepartmentInfo($hyyid, $page, $pageSize);

        $departmentCount = $this->departmentRepository->getDepartmentCount($hyyid);

        $data = [
            'list' => $departmentInfo,
            'page' => $page,
            'page_size' => $pageSize,
            'total' => $departmentCount
        ];

        return $data;
    }
}
