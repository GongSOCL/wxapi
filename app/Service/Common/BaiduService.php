<?php
declare(strict_types=1);


namespace App\Service\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use GuzzleHttp\Exception\GuzzleException;

class BaiduService
{

    private function calcBaiduSn($sk, $url, $querystring_arrays, $method = 'GET'): string
    {
        if ($method === 'POST') {
            ksort($querystring_arrays);
        }
        $querystring = http_build_query($querystring_arrays);
        return md5(urlencode($url.'?'.$querystring.$sk));
    }

    /**
     * 获取地理定位应用配置
     * @return array
     */
    private function getGeoApp(): array
    {
        return [
            env('BAIDU_MAP_AK'),
            env('BAIDU_MAP_SK')
        ];
    }

    /**
     * 逆地理位置编码
     * @param $lng
     * @param $lat
     * @param $coordinateType
     * @return array
     * @throws GuzzleException
     */
    public function reverseCoordinate($lng, $lat, $coordinateType): array
    {
        $uri = "/reverse_geocoding/v3/";
        $baseUri = 'http://api.map.baidu.com';
        [$ak, $sk] = $this->getGeoApp();

        $data = [
            'ak' => $ak,
            'output' => 'json',
            'coordtype' => $coordinateType,
            'location' => sprintf('%s,%s', $lat, $lng)
        ];
        $sn = $this->calcBaiduSn($sk, $uri, $data, 'GET');
        $data['sn'] = $sn;
        $response = Helper::getHttpClient($baseUri)->request('GET', $uri, [
            'query' => $data
        ]);

        $resp = $response->getBody()->getContents();
        $respData = json_decode($resp, true);
        if (!(isset($respData['status']) && $respData['status'] == 0)) {
            $errMsg = $respData['message'] ?? '百度地图接口异常';
            throw new BusinessException(ErrorCode::BAIDU_MAP_REVERSE_COORD_ERROR, $errMsg);
        }
        return $respData['result'];
    }
}
