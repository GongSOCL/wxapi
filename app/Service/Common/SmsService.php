<?php
declare(strict_types=1);

namespace App\Service\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Repository\Login\LoginForVersionRepository;
use Exception;
use Grpc\Msg\MSG_PROJECT;
use Hyperf\Utils\ApplicationContext;
use Youyao\Framework\Components\Msg\TplSmsRpcClient;

class SmsService
{
    /**
     * @param $mobile
     * @return int
     * @throws Exception
     */
    public static function sendVerify($mobile, $check)
    {
        $limitTimesOneMinute = (int) env('SMS_SEND_INTERVAL_PER_TIME', 55);
        //设置一个发送频率锁(n秒一次)
        if (!SmsCacheService::isLock($mobile, $limitTimesOneMinute)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "发送频率过快，秒稍后重试");
        }
        if ($check) {
            $chekInfo = LoginForVersionRepository::getPwdByPhone($mobile);
            $code = $chekInfo&&$chekInfo->psd ? $chekInfo->psd : Helper::generateNInteger(6);
        } else {
            $code = Helper::generateNInteger(6);
        }

        try {
            $callbackUrl = (string)env('MSG_CALLBACK_URL', '');
            $sid = ApplicationContext::getContainer()
                ->get(TplSmsRpcClient::class)
                ->setProject(MSG_PROJECT::PROJECT_WXAPI)
                ->sendCaptureCode($mobile, $code, $callbackUrl);

            //缓存数据到已发送并且设定过期时间为10分钏
            Helper::getLogger()
                ->debug("send_sms_for_mobile", [
                    'mobile' => $mobile,
                    'sid' => $sid
                ]);

            //记录预先发布短信
            SmsCacheService::cacheCode($mobile, $code, 305);
            SmsCacheService::preSendSms($mobile, strval($sid), $code, 120);
        } catch (Exception $e) {
            SmsCacheService::delMobileCode($mobile);
            throw $e;
        }
    }


    /**
     * 验证验证码
     * @param $mobile
     * @param $code
     * @return bool
     */
    public static function verify($mobile, $code): bool
    {
        $cacheCode = SmsCacheService::getCodeFromCache($mobile);
        SmsCacheService::delMobileCode($mobile);
        return $cacheCode != "" && $code == $cacheCode;
    }
}
