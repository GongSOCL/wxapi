<?php
declare(strict_types=1);


namespace App\Service\Common;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\DrugInterpret;
use App\Model\Qa\DrugManual;
use App\Model\Qa\DrugProduct;
use App\Model\Qa\DrugSeries;
use App\Model\Qa\ProductInfo;
use App\Repository\DrugInterpretRepository;
use App\Repository\DrugManualRepository;
use App\Repository\DrugProductRepository;
use App\Repository\DrugSeriesRepository;
use App\Repository\ProductInfoRepository;
use App\Repository\RepsServeScopeRepository;
use App\Repository\ResourceNumRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Service\User\UserService;
use Hyperf\Utils\Collection;
use Hyperf\Utils\Context;
use Youyao\Framework\ErrCode;

class ProductService
{
    /**
     * 获取用户服务医院.
     *
     * @return array
     */
    public static function getUserServeDrugSeries($hospitalId = 0): array
    {
        $user = Helper::getLoginUser();
        $hospital = null;
        if ($hospitalId > 0) {
            $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        }
        $drugCollection = RepsServeScopeRepository::getUserServeDrugSeries($user, $hospital);
        $result = [];
        $idMap = [];
        foreach ($drugCollection as $item) {
            if (isset($idMap[$item->id])) {
                continue;
            }
            $idMap[$item->id] = 1;

            $result[] = [
                'id' => $item->id,
                'name' => $item->series_name
            ];
        }

        return $result;
    }

    /**
     * 获取首页展示药品列表
     * @return array
     */
    public static function getAppProduct(): array
    {
        $showProducts = DrugProductRepository::getShowProducts();
        if ($showProducts->isEmpty()) {
            return [];
        }

        $seriesYYIDS = $showProducts->pluck('series_yyid')
            ->unique()
            ->all();
        $series = DrugSeriesRepository::getValidSeriesByYyids($seriesYYIDS);
        $seriesMaps = $series->keyBy("yyid")->all();

        $data = [];
        $imageBaseUrl = env('QINIU_BUCKET_IMAGE_DOMAIN', '');
        /** @var DrugProduct $item */
        foreach ($showProducts as $item) {
            if (!isset($seriesMaps[$item->series_yyid])) {
                continue;
            }
            /** @var DrugSeries $seriesItem */
            $seriesItem = $seriesMaps[$item->series_yyid];
            $pic = $item->list_pic;
            if ($pic && $imageBaseUrl) {
                $pic = sprintf("%s/%s", rtrim($imageBaseUrl, '/'), $pic);
            }
            $data[] = [
                'id' => $item->id,
                'name' => $item->name,
                'intro' => $item->c_name,
                'series_id' => $seriesItem->id,
                'pic' => $pic
            ];
        }

        return $data;
    }

    public static function getSeriesInfo(int $id): array
    {
        $series = DrugSeriesRepository::getById($id);
        if (!$series) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "系列药品不存在");
        }

        $pic = $series->detail_pic;
        $listPic = $series->list_pic;
        $baseImgUrl = env('QINIU_BUCKET_IMAGE_DOMAIN', '');
        if ($baseImgUrl) {
            $pic && $pic = sprintf("%s/%s", rtrim($baseImgUrl, '/'), $pic);
            $listPic && $listPic = sprintf("%s/%s", rtrim($baseImgUrl, '/'), $listPic);
        }

        $productInfo = ProductInfoRepository::getBySeriesId($id);
        $infoMap = $productInfo->keyBy("info_type")->all();
        $seriesDirect = isset($infoMap[ProductInfo::INFO_TYPE_DIRECTION]) ?
            $infoMap[ProductInfo::INFO_TYPE_DIRECTION]->content : '暂无';
        $market = isset($infoMap[ProductInfo::INFO_TYPE_MARKET_ANALYZE]) ?
            $infoMap[ProductInfo::INFO_TYPE_MARKET_ANALYZE]->content : '暂无';
        $investment = isset($infoMap[ProductInfo::INFO_TYPE_INVESTMENT_SCOPE]) ?
            $infoMap[ProductInfo::INFO_TYPE_INVESTMENT_SCOPE]->content : '暂无';
        $zonePrice = isset($infoMap[ProductInfo::INFO_TYPE_ZONE_PRICE]) ?
            $infoMap[ProductInfo::INFO_TYPE_ZONE_PRICE]->content : '暂无';
        $platformPolicy = isset($infoMap[ProductInfo::INFO_TYPE_PLATFORM_POLICY]) ?
             $infoMap[ProductInfo::INFO_TYPE_PLATFORM_POLICY]->content : '暂无';
        $rules = isset($infoMap[ProductInfo::INFO_TYPE_SETTLEMENT_RULES]) ?
            $infoMap[ProductInfo::INFO_TYPE_SETTLEMENT_RULES]->content : '暂无';

        return [
            'approveal' => (string) $series->approval_id,
            'type' => $series->type,
            'manufacturer' => $series->manufacturer,
            'name' => $series->series_name,
            'sub_series_name' => (string)$series->sub_series_name,
            'series_name_en' => $series->series_name_en,
            'pic' => $pic,
            'list_pic' => $listPic,
            'desc' => $series->desc,
            'series_direct' => $seriesDirect,
            'market_analyze' => $market,
            'investment_scope' => $investment,
            'zone_price' => $zonePrice,
            'platform_policy' => $platformPolicy,
            'settlement_rules' => $rules
        ];
    }

    public static function getProductManual($id): array
    {
        $product = DrugProductRepository::getById($id);
        if (!$product) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "药品不存在");
        }

        $manuals = DrugManualRepository::getProductManuals($product);
        $data = [];
        /** @var DrugManual $item */
        foreach ($manuals as $item) {
            $data[] = [
                'name' => $item->name,
                'content' => $item->content
            ];
        }

        return [$product->name, $data];
    }

    public static function getProductInterprets(int $id, int $current = 1, int $limit = 10): array
    {
        //产品文献部分暂时不验证登录
//        UserService::checkMobileBind();

        $product = DrugProductRepository::getById($id);
        if (!$product) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "药品不存在");
        }

        $resp = DrugInterpretRepository::getProductDrugInterprets($product, $current, $limit);
        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $resp->perPage()
        ];

        $data = [];
        if ($resp->isNotEmpty()) {
            /** @var Collection $col */
            $col = $resp->getCollection();
            ;
            $YYIDS = $col->pluck("yyid")->toArray();

            $readNums = ResourceNumRepository::getReadNums($YYIDS);
            $readNumMap = $readNums->keyBy('resource_yyid')->all();
            /** @var DrugInterpret $item */
            foreach ($col as $item) {
                $readNum = 0;
                if (isset($readNumMap[$item->yyid])) {
                    $readNum = (int) $readNumMap[$item->yyid]->read_num;
                }
                $data[] = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'create_time' => Helper::formatInputDate($item->created_time, "Y-m-d H:i"),
                    'read_num' => $readNum
                ];
            }
        }

        return [$page, $data];
    }
}
