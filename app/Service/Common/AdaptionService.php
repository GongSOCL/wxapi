<?php

namespace App\Service\Common;

use App\Model\Qa\Adaption;
use App\Repository\AdaptionRepository;

class AdaptionService
{
    /**
     * 治疗领域列表
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return array
     */
    public static function listFields($keywords = "", $current = 1, $limit = 10): array
    {
        $resp = AdaptionRepository::getFields($keywords, $current, $limit);

        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $resp->perPage()
        ];
        $data = [];
        if ($resp->isNotEmpty()) {
            /** @var Adaption $v */
            foreach ($resp->items() as $v) {
                $data[] = [
                    'id' => $v->id,
                    'name' => $v->name
                ];
            }
        }

        return [$page, $data];
    }

    /**
     * 适应症列表
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return array
     */
    public static function listAdaption($keywords = "", $current = 1, $limit = 10): array
    {
        $resp = AdaptionRepository::getAdaptions($keywords, $current, $limit);

        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $resp->perPage()
        ];
        $data = [];
        if ($resp->isNotEmpty()) {
            /** @var Adaption $v */
            foreach ($resp->items() as $v) {
                $data[] = [
                    'id' => $v->id,
                    'name' => $v->name
                ];
            }
        }

        return [$page, $data];
    }
}
