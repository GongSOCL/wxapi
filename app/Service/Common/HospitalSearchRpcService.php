<?php


namespace App\Service\Common;

use Grpc\Common\CommonPageRequest;
use Grpc\Hospital\HOSPITAL_TRANSACTION_STATUS;
use Grpc\Hospital\HospitalSearchClient;
use Grpc\Hospital\HospitalSearchResponse;
use Grpc\Hospital\SearchRequest;
use Hyperf\Utils\ApplicationContext;

class HospitalSearchRpcService
{
    private static function getClient(): HospitalSearchClient
    {
        return ApplicationContext::getContainer()->get(HospitalSearchClient::class);
    }

    public function searchHospital(
        $current = 1,
        $limit = 100,
        $provinceYYID = "",
        $cityYYID = "",
        $levels = [],
        $keywords = "",
        $seriesYYID = "",
        $showOnlyApply = false,
        $transactionStatus = HOSPITAL_TRANSACTION_STATUS::TRANSACTION_ALL,
        $companyYYID = ""
    ): HospitalSearchResponse {

        $request = new SearchRequest();
        $request->setProvinceYyid($provinceYYID);
        $request->setCityYyid($cityYYID);
        $request->setLevel($levels);
        $request->setKeywords($keywords);
        $request->setSeriesYyid($seriesYYID);
        $request->setFilterOnlyApply($showOnlyApply);
        $request->setTransactionStatus($transactionStatus);
        $page = new CommonPageRequest();
        $page->setPage($current);
        $page->setSize($limit);
        $request->setPage($page);
        $request->setCompanyYyid($companyYYID);

        return self::getClient()->searchHospital($request);
    }
}
