<?php


namespace App\Service\Common;

use App\Model\Qa\Company;
use App\Repository\CompanyRepository;
use Grpc\Wxapi\Company\CompanyItem;
use Grpc\Wxapi\Company\SearchCompanyReply;

class CompanyService
{
    /**
     * 公司列表
     * @param string $keywords
     * @param int $current
     * @param int $limit
     * @return array
     */
    public static function listCompany($keywords = "", $current = 1, $limit = 10): array
    {
        $resp = CompanyRepository::listCompany($keywords, $current, $limit);

        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $resp->perPage()
        ];
        $data = [];
        if ($resp->isNotEmpty()) {
            /** @var Company $v */
            foreach ($resp->items() as $v) {
                $data[] = [
                    'id' => $v->id,
                    'name' => $v->name
                ];
            }
        }

        return [$page, $data];
    }

    public static function searchCompanyWithLimit($keywords = '', $limit = 5): SearchCompanyReply
    {
        $resp = CompanyRepository::searchCompanyWithLimit($keywords, $limit);
        $reply = new SearchCompanyReply();
        if ($resp->isEmpty()) {
            return $reply;
        }

        $list = [];
        /** @var Company $item */
        foreach ($resp as $item) {
            $o = new CompanyItem();
            $o->setId($item->id);
            $o->setName($item->name);
            $list[] = $o;
        }
        $reply->setList($list);
        return $reply;
    }
}
