<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Common;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\TaskManageHelper;
use App\Model\Qa\DrugSeries;
use App\Model\Qa\ImportBatchList;
use App\Model\Qa\RepsServeScopeS;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;
use App\Repository\DrugSeriesRepository;
use App\Repository\HospitalRepository;
use App\Repository\HospitalTransactionStatusRepository;
use App\Repository\ImportBatchListRepository;
use App\Repository\ImportBatchRepository;
use App\Repository\RegionRepository;
use App\Repository\RepsServeScopeRepository;
use App\Repository\RepsServeScopeSRepository;
use App\Repository\SupplierRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Request\Helper\SyncSingleHospitalRequest;
use Grpc\Hospital\HospitalSearchItem;
use Grpc\Task\YouyaoHospitalEsSyncAllTask;
use Grpc\Task\YouyaoHospitalEsSyncSingleTask;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use Hyperf\Utils\Context;

class HospitalService
{

    /**
     * @Inject()
     * @var HospitalRepository
     */
    private $hospitalRepository;

    /**
     * 查找医院
     * @param int $provinceId
     * @param int $cityId
     * @param array $level
     * @param int $current
     * @param int $limit
     * @param int $seriesId
     * @param int $transaction
     * @param string $keywords
     * @param false $filterCanApply
     * @return array
     */
    public static function searchHospitals(
        $provinceId = 0,
        $cityId = 0,
        array $level = [],
        $current = 1,
        $limit = 10,
        $seriesId = 0,
        $transaction = 0,
        $keywords = "",
        $filterCanApply = false
    ): array {
        $user = Helper::getLoginUser();
        $provinceYYID = "";
        $cityYYID = "";
        //省份
        if ($provinceId) {
            $province = RegionRepository::getById($provinceId);
            if ($province) {
                $provinceYYID = $province->yyid;
            }
        }

        //城市
        if ($cityId) {
            $city = RegionRepository::getById($cityId);
            if ($city) {
                $cityYYID = $city->yyid;
            }
        }

        $series = null;
        $seriesYYID = "";
        if ($seriesId) {
            $series = DrugSeriesRepository::getById($seriesId);
            $seriesYYID = $series->yyid;
        }
        if (!$series) {
            $transaction = 0;
            $filterCanApply = false;
        }

        $searchCompany = (string)$user->company_yyid == "" ? "" : (
            $user->company_yyid == DataStatus::COMPANY_YOURUI ?
                DataStatus::COMPANY_YOURUI : DataStatus::COMPANY_YOUYAO
        );
        $resp = ApplicationContext::getContainer()
            ->get(HospitalSearchRpcService::class)
            ->searchHospital(
                $current,
                $limit,
                $provinceYYID,
                $cityYYID,
                $level,
                $keywords,
                $seriesYYID,
                $filterCanApply,
                $transaction,
                $searchCompany
            );
        $respPage = $resp->getPage();
        $respData = $resp->getData();
        $page = [
            'total' => $respPage->getTotal(),
            'current' => $respPage->getCurrent(),
            'pages' => $respPage->getPages(),
            'limit' => $respPage->getSize()
        ];

        $data = [];
        //对数据进行回表处理
        if ($respData->count() > 0) {
            $hospitalIDS = [];
            /** @var HospitalSearchItem $item */
            foreach ($respData as $item) {
                $hospitalIDS[] = $item->getId();
            }

            $hospitals = YouyaoHospitalRepository::getHospitalByIds($hospitalIDS);
            $hospitalMap = $hospitals->keyBy('id')->all();
            $log = Helper::getLogger();
            $log->info("hospital_search_es", [
                'resp_count' => $respData->count(),
                'count' => $hospitals->count(),
                'all' => $hospitalIDS
            ]);
            $hospitalYYIDS = $hospitals->pluck('yyid')->all();

            $applyList = [];
            $transactionList = [];
            if ($series && (!empty($hospitalYYIDS))) {
                $applyList = self::filterApplyHospitals($series, $hospitalYYIDS);
                $transactionList = self::filterTransactionHospitals($series, $hospitalYYIDS);
            }

            // 供应商被占用的医院yyid
            $supplierList = [];
            if ($series && (!empty($hospitalYYIDS))) {
                $supplierList = self::filtersupplierHospitals($seriesId, $hospitalIDS);
            }

            foreach ($hospitalIDS as $hospitalId) {
                if (!isset($hospitalMap[$hospitalId])) {
                    continue;
                }
                /** @var YouyaoHospital $v */
                $v = $hospitalMap[$hospitalId];
                $vCanApply = false;
                $vTransaction = false;
                if ($series) {
                    $vCanApply = !in_array($v->yyid, $applyList) && !in_array($v->yyid, $supplierList);
                    $vTransaction = in_array($v->yyid, $transactionList);
                }
                $item = [
                    'id' => $v->id,
                    'name' =>  (string) $v->hospital_name,
                    'level' => (string) $v->level,
                    'can_apply' => $vCanApply ? 1 : 0,
                    'transaction' => $vTransaction ? 1 : 0
                ];

                $data[] = $item;
            }
        }

        return [$page, $data];
    }

    private static function filtersupplierHospitals($seriesId, $hospitalIDS)
    {
        $resp = SupplierRepository::getOccupiedHospital($seriesId, $hospitalIDS);
        if ($resp->isNotEmpty()) {
            $hospitals = SupplierRepository::getHospitals(
                array_unique(
                    array_column($resp->toArray(), 'hospital_id')
                )
            );
            return $hospitals->isNotEmpty() ? array_column($hospitals->toArray(), 'yyid') : [];
        } else {
            return [];
        }
    }

    private static function filterApplyHospitals(DrugSeries $series, array $hospitalYYIDS): array
    {
        $resp = RepsServeScopeSRepository::getServingBySeriesAndHospitals($series, $hospitalYYIDS);
        return $resp->pluck('hospital_yyid')->all();
    }

    private static function filterTransactionHospitals(DrugSeries $series, array $hospitalYYIDS): array
    {
        //获取最后时间
        $batch = ImportBatchRepository::getNewest();
        if (!$batch) {
            return [];
        }
        $resp = ImportBatchListRepository::getMaxDate($batch);
        if (!$resp) {
            return [];
        }

        //计算时间区间
        $a = new \DateTime($resp['transaction_date']);
        $end = $a->format('Y-m-t 23:59:59');
        $a->modify('-5 months');
        $start = $a->format('Y-m-01 00:00:00');

        //根据药品,时间范围，医院id过滤有效的医院
        $resp = HospitalTransactionStatusRepository::filterValidHospitalsBySeriesAndHospitals(
            $series,
            $start,
            $end,
            $hospitalYYIDS
        );

        return $resp->pluck('hospital_yyid')->all();
    }

    /**
     * 获取药品医院可申请详情
     * @param int $hospitalId
     * @param int $seriesId
     * @return array
     */
    public static function getHospitalDrugDetail(int $hospitalId, int $seriesId = 0): array
    {
        $user = Helper::getLoginUser();

        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }

        $isApplied = true;
        if ($seriesId > 0) {
            $series = DrugSeriesRepository::getById($seriesId);
            if (!$series) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "系列药品不存在");
            }

            // 检查医院是否已经申请过了
            $isApplied = RepsServeScopeSRepository::isSeriesApplied($user, $hospital, $series);
        }


        return [
            'name' => $hospital->hospital_name,
            'level' => $hospital->level,
            'can_apply' => $isApplied ? 0 : 1,
            'addr' => $hospital->addr,
            'hospital_dean' => $hospital->hos_dean,
            'bed_num' => (int) $hospital->bed_num,
            'import_department' => $hospital->p_department
        ];
    }

    public static function syncSingleHospitalEs(int $hospitalId)
    {
        $params = new YouyaoHospitalEsSyncSingleTask();
        $params->setId($hospitalId);
        TaskManageHelper::create(0, $params, 'sync-single-hospital')
            ->push();
    }

    public static function syncAllHospitalEs()
    {
        $params = new YouyaoHospitalEsSyncAllTask();
        TaskManageHelper::create(0, $params, 'sync-all-hospital-es')
            ->push();
    }

    /**
     * @param $keyword
     * @param $pyyid
     * @param $cyyid
     * @param $level
     * @param $page
     * @param $pageSize
     * @return array
     */
    public function getHospitalInfo($keyword, $pyyid, $cyyid, $level, $page, $pageSize): array
    {
        $hospitalInfo = $this->hospitalRepository->getHospitalInfo($keyword, $pyyid, $cyyid, $level, $page, $pageSize);

        //总数量
        $hospitalCount = $this->hospitalRepository->getHospitalCount($keyword, $pyyid, $cyyid, $level);

        $data = [
            'list' => $hospitalInfo,
            'page' => $page,
            'page_size' => $pageSize,
            'total' => $hospitalCount
        ];

        return $data;
    }

    /**
     * 获取用户服务医院.
     *
     * @param $provinceId
     * @param $cityId
     * @param $level
     * @param $keywords
     * @param $page
     * @param $pageSize
     * @return array
     */
    public static function getUserServeHospitals(
        $provinceId,
        $cityId,
        $level,
        $keywords = "",
        $page = 1,
        $pageSize = 10
    ): array {
        $user = Helper::getLoginUser();

        //省份
        $provinceYYID = '';
        if ($provinceId) {
            $province = RegionRepository::getById($provinceId);
            if ($province) {
                $provinceYYID = $province->yyid;
            }
        }

        //城市
        $cityYYID = '';
        if ($cityId) {
            $city = RegionRepository::getById($cityId);
            if ($city) {
                $cityYYID = $city->yyid;
            }
        }

        $hospitalCollections = RepsServeScopeRepository::getUserServeHospital(
            $provinceYYID,
            $cityYYID,
            $level,
            $user,
            $page,
            $pageSize,
            $keywords
        );
        $count = RepsServeScopeRepository::getUserServeHospitalCount(
            $provinceYYID,
            $cityYYID,
            $level,
            $user,
            $keywords
        )->count();
        $result = [];
        /** @var YouyaoHospital $item */
        foreach ($hospitalCollections as $item) {
            $result[] = [
                'id' => $item->id,
                'name' => $item->hospital_name,
                'org_type' => $item->org_type
            ];
        }

        return [
            'list' => $result,
            'total' => $count,
            'page' => $page,
            'pageSize' => $pageSize
        ];
    }
}
