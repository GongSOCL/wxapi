<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Service\Exchange;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\Users;
use App\Model\Qa\WechatUser;
use App\Repository\WechatUserRepository;
use App\Service\Points\PointRpcService;
use App\Service\Points\PointsService;
use GuzzleHttp\Exception\GuzzleException;
use Hyperf\Di\Annotation\Inject;

class ExchangeService
{
    /**
     * @Inject
     * @var PointsService
     */
    private $pointService;

    /**
     * @Inject
     * @var WechatUserRepository
     */
    private $wechatUserRepo;

    public function exchangeGoods(Users $user, string $goodsYyid, int $goodsNum, int $wechatType)
    {
        $wechatUser = $this->wechatUserRepo->getOneWechatInfoByUserYYID($user->yyid, $wechatType);
        if (! $wechatUser instanceof WechatUser) {
            throw new BusinessException(ErrorCode::SERVER_ERROR, '该账号暂无微信绑定关系，请先绑定微信');
        }
        $isAgent = $wechatType == DataStatus::WECHAT_AGENT;
        return $this->pointService->exchangeGoods($user, $wechatUser, $goodsYyid, $goodsNum, $isAgent);
    }

    /**
     * @throws GuzzleException
     */
    public function fetchGoods(Users $user, $page, $pageSize)
    {
        return PointRpcService::fetchExchangeGoodsList($user->uid, $page, $pageSize);
    }
}
