<?php
declare(strict_types=1);
namespace App\Service\Report;

use App\Constants\Code\ErrorCodeDoctor;
use App\Constants\DataStatus;
use App\Exception\DoctorException;
use App\Helper\Helper;
use App\Kernel\Minio\Minio;
use App\Repository\CheckoutDetailRepository;
use App\Repository\DealerAgentPhoneRepository;
use App\Repository\DealerDrugCRepository;
use App\Repository\DealerHospitalCRepository;
use App\Repository\DealerHospitalSalesRepository;
use App\Repository\DealerSalesNumRepository;
use App\Repository\DealerSalesValueRepository;
use App\Repository\DealerVisitRepository;
use App\Repository\UsersRepository;
use Grpc\Msg\MSG_BIZ_TYPE;
use Grpc\Msg\MSG_PRIORITY;
use Hyperf\Utils\ApplicationContext;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Youyao\Framework\Components\Msg\MessageRpcClient;

class ReportService
{
    /**
     * @param $yyid
     * @return array[]
     */
    public function getVisitReport($yyid)
    {
        $data = [];
        $month = [];
        //根据yyid获取companyid和staff_code
        $userInfo = $this->getStaffCodeByYyid($yyid);
        //获取已上传的最近月份
        $lastMonth = DealerVisitRepository::getLastMonth($userInfo['company_yyid'], $userInfo['staff_code']);
        if ($lastMonth->isNotEmpty()) {
            $month = $lastMonth->toArray()[0]['month'];
            $reportInfo = DealerVisitRepository::getReportInfo(
                $userInfo['company_yyid'],
                $month,
                $userInfo['staff_code']
            );
            $reportInfo = $reportInfo->toArray();
            foreach ($reportInfo as $k => $v) {
                $proInfo = DealerDrugCRepository::getProInfo($userInfo['company_yyid'], $v['product_id']);
                if ($proInfo) {
                    $reportInfo[$k]['product_name_cn'] = $proInfo->product_name_cn;
                } else {
                    throw new DoctorException(ErrorCodeDoctor::ILLEGAL_DRUG_IN_REPORT);
                }
            }

            foreach ($reportInfo as $value) {
                if ($value['ins_class']!=''&&
                    $value['ins_class']!=null&&
                    $value['ins_class']!='其他'&&
                    $value['ins_class']!='未评级'&&
                    $value['ins_class']!='Undefined'&&
                    $value['ins_class']!='undefined') {
                    $data[$value['product_name_cn']][$value['ins_class']] = $value;
                } else {
                    $data[$value['product_name_cn']]['其他'] = $value;
                }
            }
        }

        return [
            'month' => $month,
            'report' => $data
        ];
    }

    /**
     * @param $yyid
     * @return array
     */
    public function getSalesReport($yyid)
    {
        //根据yyid获取companyid和staff_code
        $userInfo = $this->getStaffCodeByYyid($yyid);
        //获取销售数量报表信息
        $saleNumData = $this->getSalesNumReport($userInfo);
        //获取销售金额报表信息
        $saleValueData = $this->getSalesValueReport($userInfo);

        return [
            'saleNum' => $saleNumData,
            'saleValue' => $saleValueData
        ];
    }

    /**
     * @param $userInfo
     * @return array
     */
    public function getSalesValueReport($userInfo)
    {
        $saleValueReport = $this->getSalesValue($userInfo);

        $month = '';
        $reportData = [];
        if (!empty($saleValueReport)) {
            $month = $saleValueReport[0]['month'];
            foreach ($saleValueReport as $value) {
                $reportData[$value['product_name_cn']] = $value;
            }
        }

        return [
            'month' => $month,
            'report' => $reportData
        ];
    }

    /**
     * @param $userInfo
     * @return array
     */
    public function getSalesNumReport($userInfo)
    {
        $saleNumReport = $this->getSalesNum($userInfo);

        $month = '';
        $reportData = [];
        if (!empty($saleNumReport)) {
            $month = $saleNumReport[0]['month'];
            foreach ($saleNumReport as $value) {
                $reportData[$value['product_name_cn']][$value['sku_name_cn']] = $value;
            }
        }

        return [
            'month' => $month,
            'report' => $reportData
        ];
    }

    /**
     * @param $yyid
     * @param $email
     * @return string[]
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function sendMail($yyid, $email)
    {
        //存邮箱
        UsersRepository::updateEmail($yyid, $email);
        //根据yyid获取companyid和staff_code
        $userInfo = $this->getStaffCodeByYyid($yyid);
        //销售数据
        $originalData = $this->getHospitalSales($userInfo);
        //生成文件
        $file = $this->createSalesExcel($originalData, $yyid);

        //发送报告
        if ($file['fileUri']) {
            //上传并发送邮件
            go(function () use ($file, $email) {
                try {
                    self::sendSalesMail($file, $email);
                } catch (\Throwable $e) {
                    $log = Helper::getLogger();
                    $log->error("send_sales_report_error", [
                        'msg' => $e->getMessage(),
                        'trace' => $e->getTrace()
                    ]);
                }
            });
            return ['邮件发送成功'];
        } else {
            throw new DoctorException(ErrorCodeDoctor::NO_REPORT);
        }
    }

    public static function uploadFileToMinio(
        $filePath,
        $uploadPath,
        $fileName,
        $contentType = "application/vnd.ms-excel"
    ) {
        $fs = Minio::getFileSystem("minio");
        if ($fs->has($uploadPath)) {
            $fs->delete($uploadPath);
        }
        $stream = fopen($filePath, 'r+');
        $fs->writeStream($uploadPath, $stream, [
            'ContentType' => $contentType,
            'ContentDisposition' => "attachment; filename=\"$fileName\""
        ]);
        fclose($stream);
    }

    private static function sendMailMsg($mail, $subject, $body, array $attachment)
    {
        //上传文件
        ApplicationContext::getContainer()
            ->get(MessageRpcClient::class)
            ->sendMail(
                [$mail],
                $subject,
                $body,
                false,
                MSG_BIZ_TYPE::BIZ_COMMON_TYPE,
                '',
                MSG_PRIORITY::PRIORITY_MIDDLE,
                false,
                '',
                false,
                '',
                $attachment
            );
    }

    /**
     * @param $originalData
     * @param $yyid
     * @return string[]
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function createSalesExcel($originalData, $yyid)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('sheet1');
        $title = ['类型','月份','员工号','姓名','手机号','医院编号',
            '医院名称','产品编号','产品名称','SKU编号','SKU名称',
            '指标','销售','达成率','去年历史销售','增长率','有效拜访数量'];
        foreach ($title as $key => $value) {
            $sheet->setCellValueByColumnAndRow($key + 1, 1, $value);
        }

        //循环赋值
        $t = 2;
        foreach ($originalData as $value) {
            $sheet->setCellValue('A'.$t, $value['type']);
            $sheet->setCellValue('B'.$t, $value['month']);
            $sheet->setCellValue('C'.$t, $value['staff_code']);
            $sheet->setCellValue('D'.$t, $value['truename']);
            $sheet->setCellValue('E'.$t, $value['phone_number']);
            $sheet->setCellValue('F'.$t, $value['hospital_id']);
            $sheet->setCellValue('G'.$t, $value['hospital_name']);
            $sheet->setCellValue('H'.$t, $value['product_id']);
            $sheet->setCellValue('I'.$t, $value['product_name_cn']);
            $sheet->setCellValue('J'.$t, $value['sku_id']);
            $sheet->setCellValue('K'.$t, $value['sku_name_cn']);
            $sheet->setCellValue('L'.$t, $value['month_target']);
            $sheet->setCellValue('M'.$t, $value['month_sales']);
            $sheet->setCellValue('N'.$t, $value['month_ach']);
            $sheet->setCellValue('O'.$t, $value['ly_sales']);
            $sheet->setCellValue('P'.$t, $value['month_gr']);
            $sheet->setCellValue('Q'.$t, $value['eff_call_num']);
            $t++;
        }
        $fileName = '医院销售报表' . $yyid . date('Ymd', time()) . rand(100000, 999999);
        $writer = new Xlsx($spreadsheet);
        $fileUri = DataStatus::SALES_REPORT_URI . $fileName . ".xlsx";
        $writer->save($fileUri);
        return [
            'fileUri' => $fileUri,
            'fileName' => $fileName
        ];
    }

    /**
     * @param $yyid
     * @param $userInfo
     * @return array
     */
    public function getHospitalSales($userInfo)
    {
        $originalData = DealerHospitalSalesRepository::getReportInfo(
            $userInfo['company_yyid'],
            $userInfo['staff_code']
        );
        if ($originalData->isNotEmpty()) {
            $originalData = $originalData->toArray();
        } else {
            throw new DoctorException(ErrorCodeDoctor::NO_REPORT);
        }
        foreach ($originalData as $k => $v) {
            //姓名和手机
            $originalData[$k]['truename'] = $userInfo['staff_name'];
            $originalData[$k]['phone_number'] = $userInfo['phone_number'];
            //获取医院
            $hospitalInfo = DealerHospitalCRepository::getHospitalInfo(
                $userInfo['company_yyid'],
                $v['hospital_id']
            );
            if ($hospitalInfo) {
                $originalData[$k]['hospital_name'] = $hospitalInfo->hospital_name_cn;
            } else {
                throw new DoctorException(ErrorCodeDoctor::ILLEGAL_HOSPITAL_IN_REPORT);
            }
            $skuInfo = DealerDrugCRepository::getSkuInfo(
                $userInfo['company_yyid'],
                $v['product_id'],
                $v['sku_id']
            );
            if ($skuInfo) {
                $originalData[$k]['product_name_cn'] = $skuInfo->product_name_cn;
                $originalData[$k]['sku_name_cn'] = $skuInfo->sku_name_cn;
            } else {
                throw new DoctorException(ErrorCodeDoctor::ILLEGAL_DRUG_IN_REPORT);
            }
        }
        return $originalData;
    }

    /**
     * @param $userInfo
     * @return array
     */
    public function getSalesNum($userInfo)
    {
        $originalData = [];
        //获取已上传的最近月份
        $lastMonth = DealerSalesNumRepository::getLastMonth(
            $userInfo['company_yyid'],
            $userInfo['staff_code']
        );
        if ($lastMonth->isNotEmpty()) {
            $month = $lastMonth->toArray()[0]['month'];
            $originalData = DealerSalesNumRepository::getReportInfo(
                $userInfo['company_yyid'],
                $month,
                $userInfo['staff_code']
            );
            $originalData = $originalData->toArray();
            foreach ($originalData as $k => $v) {
                $skuInfo = DealerDrugCRepository::getSkuInfo(
                    $userInfo['company_yyid'],
                    $v['product_id'],
                    $v['sku_id']
                );
                if ($skuInfo) {
                    $originalData[$k]['product_name_cn'] = $skuInfo->product_name_cn;
                    $originalData[$k]['sku_name_cn'] = $skuInfo->sku_name_cn;
                } else {
                    throw new DoctorException(ErrorCodeDoctor::ILLEGAL_DRUG_IN_REPORT);
                }
            }
        }
        return $originalData;
    }

    /**
     * @param $userInfo
     * @return array
     */
    public function getSalesValue($userInfo)
    {
        $originalData = [];
        //获取已上传的最近月份
        $lastMonth = DealerSalesValueRepository::getLastMonth(
            $userInfo['company_yyid'],
            $userInfo['staff_code']
        );
        if ($lastMonth->isNotEmpty()) {
            $month = $lastMonth->toArray()[0]['month'];
            $originalData = DealerSalesValueRepository::getReportInfo(
                $userInfo['company_yyid'],
                $month,
                $userInfo['staff_code']
            );
            $originalData = $originalData->toArray();
            foreach ($originalData as $k => $v) {
                $proInfo = DealerDrugCRepository::getProInfo(
                    $userInfo['company_yyid'],
                    $v['product_id']
                );
                if ($proInfo) {
                    $originalData[$k]['product_name_cn'] = $proInfo->product_name_cn;
                } else {
                    throw new DoctorException(ErrorCodeDoctor::ILLEGAL_DRUG_IN_REPORT);
                }
            }
        }
        return $originalData;
    }

    /**
     * @param $yyid
     * @param $email
     * @return string[]
     */
    public function sendCheckoutMail($yyid, $email)
    {
        //存邮箱
        UsersRepository::updateEmail($yyid, $email);
        //根据yyid获取companyid和staff_code
        $userInfo = $this->getStaffCodeByYyid($yyid);
        //拜访明细
        $originalData = $this->getCheckoutDetail($userInfo);
        //生成文件
        $file = $this->createCheckoutDetailExcel($originalData, $yyid);

        //发送报告
        if ($file['fileUri']) {
            go(function () use ($file, $email) {
                self::sendCheckout($file, $email);
            });
            return ['邮件发送成功'];
        } else {
            throw new DoctorException(ErrorCodeDoctor::NO_REPORT);
        }
    }

    /**
     * @param $originalData
     * @param $yyid
     * @return string[]
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function createCheckoutDetailExcel($originalData, $yyid)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('sheet1');
        $title = ['活动ID','手机号码','员工编号','活动类型编号','活动类型名称','活动日期',
            '签入时间','签出时间','活动时常(单位:小时)','机构编号','机构名称','科室',
            '医生编号','医生姓名','是否有效GPS'];
        foreach ($title as $key => $value) {
            $sheet->setCellValueByColumnAndRow($key + 1, 1, $value);
        }

        //循环赋值
        $t = 2;
        foreach ($originalData as $value) {
            $sheet->setCellValue('A'.$t, $value['activity_id']);
            $sheet->setCellValue('B'.$t, $value['telephone']);
            $sheet->setCellValue('C'.$t, $value['staff_code']);
            $sheet->setCellValue('D'.$t, $value['activity_type_code']);
            $sheet->setCellValue('E'.$t, $value['activity_type_name']);
            $sheet->setCellValue('F'.$t, $value['activity_dt']);
            $sheet->setCellValue('G'.$t, $value['check_in_datetime']);
            $sheet->setCellValue('H'.$t, $value['check_out_datetime']);
            $sheet->setCellValue('I'.$t, $value['activity_duration']);
            $sheet->setCellValue('J'.$t, $value['inscode']);
            $sheet->setCellValue('K'.$t, $value['insname_cn']);
            $sheet->setCellValue('L'.$t, $value['department']);
            $sheet->setCellValue('M'.$t, $value['customer_code']);
            $sheet->setCellValue('N'.$t, $value['customer_name']);
            $sheet->setCellValue('O'.$t, $value['flag']);
            $t++;
        }
        $fileName = '医院拜访个人明细报表' . $yyid . date('Ymd', time()) . rand(100000, 999999);
        $writer = new Xlsx($spreadsheet);
        $fileUri = DataStatus::CHECKOUT_DETAIL_URI . $fileName . ".xlsx";
        $writer->save($fileUri);
        return [
            'fileUri' => $fileUri,
            'fileName' => $fileName
        ];
    }

    /**
     * @param $userInfo
     * @return array
     */
    public function getCheckoutDetail($userInfo)
    {
        $originalData = CheckoutDetailRepository::getCheckoutDetailInfo(
            $userInfo['company_yyid'],
            $userInfo['staff_code']
        );
        if ($originalData->isEmpty()) {
            throw new DoctorException(ErrorCodeDoctor::NO_REPORT);
        }
        return $originalData->toArray();
    }

    /**
     * @param $userId
     * @return array
     */
    public function getDeparts($userId)
    {
        $month = CheckoutDetailRepository::getLatestMonth($userId);
        $eMonth = $month ? $month->month : '';
        $departs = CheckoutDetailRepository::getDeparts($userId, $eMonth);
        return $departs ? $departs->toArray() : [];
    }

    /**
     * @param $userId
     * @param $department
     * @return array
     */
    public function doctorVisit($userId, $department)
    {
        $originalData = CheckoutDetailRepository::getDoctorVisitDetailInfo($userId, $department);
        return $originalData ? $originalData->toArray() : [];
    }

    /**
     * @param $yyid
     * @return array
     */
    public function getStaffCodeByYyid($yyid)
    {
        //根据yyid获取companyid和手机号
        $userInfo = UsersRepository::getUserByYYID($yyid);
        if (!$userInfo||!$userInfo->company_yyid||!$userInfo->mobile_num) {
            throw new DoctorException(ErrorCodeDoctor::REPORT_HISPOTAL_PHONE_MISSING);
        }
        //根据手机获取工号
        $staffInfo = DealerAgentPhoneRepository::getStaffInfoByPhone(
            $userInfo->company_yyid,
            $userInfo->mobile_num
        );
        if (!$staffInfo) {
            throw new DoctorException(ErrorCodeDoctor::NO_STAFF_CODE);
        }
        return [
            'company_yyid' => $userInfo->company_yyid,
            'staff_code' => $staffInfo->staff_code,
            'staff_name' => $staffInfo->staff_name,
            'phone_number' => $staffInfo->phone_number
        ];
    }

    /**
     * @param array $file
     * @param $email
     */
    private static function sendSalesMail(array $file, $email): void
    {
        //上传文件到minio存储
        $uploadPath = sprintf("/sales/%s/%s_%s.xlsx", date('Ymd'), $file['fileName'], uniqid());
        self::uploadFileToMinio($file['fileUri'], $uploadPath, $file['fileName']."xlsx");
        $url = Minio::createPublicBucketFileLink(env('S3_BUCKET'), $uploadPath);

        //发送邮件消息
        self::sendMailMsg(
            $email,
            '月销售报表',
            '月销售报表，详情见附件',
            [
                [$file['fileName'] . ".xlsx", $url]
            ]
        );
    }

    private static function sendCheckout(array $file, $email): void
    {
        //上传文件到minio存储
        $uploadPath = sprintf("/checkout/%s/%s_%s.xlsx", date('Ymd'), $file['fileName'], uniqid());
        self::uploadFileToMinio($file['fileUri'], $uploadPath, $file['fileName'] . ".xlsx");
        $url = Minio::createPublicBucketFileLink(env('S3_BUCKET'), $uploadPath);

        //发送邮件消息
        self::sendMailMsg(
            $email,
            '医院拜访个人明细',
            '医院拜访个人明细，详情见附件',
            [
                [$file['fileName'] . ".xlsx", $url]
            ]
        );
    }
}
