<?php
declare(strict_types=1);


namespace App\Service\Upload;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Kernel\Qiniu\Upload;
use App\Model\Qa\UploadQiniu;
use App\Repository\UploadQiniuRepository;
use Hyperf\Utils\ApplicationContext;

class UploadService
{

    public static function getConfig($type, $scope, $business, $channel = 1): array
    {
        switch ($channel) {
            case 1:
                $resp = UploadRpcService::upload($type, $scope, $business);
                return [
                    'path' => $resp->getPath(),
                    'token' => $resp->getToken(),
                    'expire_time' => $resp->getExpireTime(),
                    'bucket' => $resp->getBucket(),
                    'base_uri' => $resp->getBaseUri(),
                    'upload_uri' => $resp->getUploadUri(),
                    'channel' => 1
                ];
            case 3:
                $resp = UploadRpcService::getOssToken($type, $scope, $business);
                return [
                    'path' => $resp->getPath(),
                    'token' => $resp->getToken(),
                    'expire_time' => $resp->getExpireTime(),
                    'bucket' => $resp->getBucket(),
                    'base_uri' => $resp->getBaseUri(),
                    'upload_uri' => $resp->getUploadUri(),
                    'key' => $resp->getKey(),
                    'signature' => $resp->getSignature(),
                    'policy' => $resp->getPolicy(),
                    'channel' => 3
                ];
            default:
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "不支持该存储方式");
        }
    }

    public static function addUpload($bucket, $key, $name, $url, $channel = 1): array
    {
        $resp = UploadRpcService::addUploadRecord($key, $bucket, $name, $url, $channel);
        $res = json_decode($resp->serializeToJsonString(), true);
        $res['type'] = (int) $resp->getType();
        return $res;
    }

    public static function getItemByIds($ids): array
    {
        $items = [];
        $uploadItems = UploadQiniuRepository::getByIds($ids);
        /** @var UploadQiniu $item */
        foreach ($uploadItems as $item) {
            $items[] = [
                'id' => $item->id,
                'name' => $item->file_name,
                'url' => $item->url,
                'type' => $item->storage_type
            ];
        }
        return $items;
    }

    /**
     * 删除文件
     * @param $id
     * @param bool $delFile
     */
    public static function clearUpload($id, bool $delFile = false)
    {
        $uploadItem = UploadQiniuRepository::getById($id);
        if (!($uploadItem && $uploadItem->isValid())) {
            return;
        }

        UploadRpcService::deleteFiles([$id], !$delFile);
    }
}
