<?php
declare(strict_types=1);


namespace App\Service\Upload;

use App\Helper\Helper;
use Google\Protobuf\GPBEmpty;
use Grpc\Upload\AddUploadRequest;
use Grpc\Upload\AddUploadResponse;
use Grpc\Upload\DeleteUploadRequest;
use Grpc\Upload\OssStsResponse;
use Grpc\Upload\OssTokenResponse;
use Grpc\Upload\StorageChannel;
use Grpc\Upload\TokenRequest;
use Grpc\Upload\TokenResponse;
use Grpc\Upload\UploadClient;
use Hyperf\Utils\ApplicationContext;

class UploadRpcService
{
    public static function upload($type, $scope, $business): TokenResponse
    {
        $request = new TokenRequest();
        $request->setScope($scope);
        $request->setType($type);
        $request->setBusiness($business);

        return self::getClient()->GetToken($request);
    }

    private static function getChannel($channel = 1): int
    {
        switch ($channel) {
            case 1:
                return StorageChannel::CHANNEL_QINIU;
            case 2:
                return StorageChannel::CHANNEL_MINIO;
            case 3:
                return StorageChannel::CHANNEL_OSS;
            default:
                return StorageChannel::CHANNEL_UNKNOWN;
        }
    }

    public static function addUploadRecord($key, $bucket, $name, $url, $channel = 1): AddUploadResponse
    {
        $loginUser = Helper::tryGetLoginUser();
        $request = new AddUploadRequest();
        $request->setKey($key);
        $request->setBucket($bucket);
        $request->setName($name);
        $request->setUrl($url);
        $request->setChannel(self::getChannel($channel));
        if ($loginUser) {
            $request->setUid($loginUser->uid);
        }
        return self::getClient()->AddUploadRecord($request);
    }

    private static function getClient(): UploadClient
    {
        return ApplicationContext::getContainer()->get(UploadClient::class);
    }

    public static function getOssToken($type, $scope, $business): OssTokenResponse
    {
        $req = new TokenRequest();
        $req->setType($type);
        $req->setScope($scope);
        $req->setBusiness($business);
        return self::getClient()->GetOssToken($req);
    }

    public static function deleteFiles(array $ids, $retainFile = false)
    {
        $req = new DeleteUploadRequest();
        $req->setUploadId($ids);
        $req->setRetainFile($retainFile);
        self::getClient()->DeleteByIds($req);
    }

    public static function getOssSts(): OssStsResponse
    {
        return self::getClient()->GetOssSts(new GPBEmpty());
    }
}
