<?php


namespace App\Service\Transaction;

use App\Constants\Auth;
use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\DrugProduct;
use App\Model\Qa\DrugSeries;
use App\Model\Qa\DrugSku;
use App\Model\Qa\RepsTransactionP;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;
use App\Repository\CompanyMonthPointRepository;
use App\Repository\DrugProductRepository;
use App\Repository\DrugSkuRepository;
use App\Repository\HospitalMonthTransPRepository;
use App\Repository\RepsServeScopePRepository;
use App\Repository\RepsTransactionPRepository;
use App\Repository\SalesTargetMonthRepository;
use App\Repository\SdcDataRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Service\Common\ServeService;
use App\Service\Msg\MsgRpcService;
use DateInterval;
use DatePeriod;
use DateTime;
use Grpc\InsideMsg\OneMsgDetailRequest;
use Hyperf\Utils\Collection;

class TransactionService
{
    /**
     * @return array
     */
    public static function getServeProductTransactionSummary(): array
    {
        $user = Helper::getLoginUser();

        //获取用户服务药品及所属药品
        $serveProducts = RepsServeScopePRepository::getUserServeList($user);
        if ($serveProducts->isEmpty()) {
            return [];
        }
        $hospitalYYIDS = $serveProducts->pluck('hospital_yyid')->all();
        $productYYIDS = $serveProducts->pluck('product_yyid')->all();

        $products = DrugProductRepository::getByYYIDS($productYYIDS);

        //并行查询数据
        $data = [];
        foreach ($products as $product) {
            $ret = self::calcUserProductTransactions($product, $hospitalYYIDS);
            !empty($ret) && $data[] = $ret;
        }

        return $data;
    }

    /**
     * 计算用户服务产品流向数据
     * @param DrugProduct $drugProduct
     * @param array $hospitalYYIDS
     * @return array
     */
    private static function calcUserProductTransactions(DrugProduct $drugProduct, array $hospitalYYIDS): array
    {
        $max = HospitalMonthTransPRepository::getByProductAndHospitals($drugProduct, $hospitalYYIDS);
        if (!($max && $max['max_month'])) {
            //需要返回产品信息
            return [
                'id' => $drugProduct->id,
                'name' => $drugProduct->name,
                'last_serve_month' => '',
                'current_month' => [],
                'last_three_month' => []
            ];
        }

        //获取当月数据
        $lastMonth = $max['max_month'];
        $lastMonthData = self::calcOneMonthData($lastMonth, $hospitalYYIDS, $drugProduct);

        //获取历史三个月内数据
        $lastThreeMonthData = self::calcLastThreeMonthData($hospitalYYIDS, $drugProduct);

        //组合返回记录
        return [
            'id' => $drugProduct->id,
            'name' => $drugProduct->name,
            'last_serve_month' => $lastMonth,
            'current_month' => $lastMonthData,
            'last_three_month' => $lastThreeMonthData
        ];
    }

    public static function calcOneMonthData($lastMonth, array $hospitalYYIDS, DrugProduct $product): array
    {
        $mom = $yoy = "-";
        list($current, $lastMonth, $lastYearMonth) = self::calcOneMonthTime($lastMonth);
        $productMonthRangeData = HospitalMonthTransPRepository::getMonthRangeDataByHospitalsAndProduct(
            $hospitalYYIDS,
            $product,
            [
                $current,
                $lastMonth,
                $lastYearMonth
            ]
        );
        $pmrMap = $productMonthRangeData->keyBy('month')->all();

        $currentTotal = isset($pmrMap[$current]) ? (int)$pmrMap[$current]->num : 0;
        $lastMonthTotal = isset($pmrMap[$lastMonth]) ? (int)$pmrMap[$lastMonth]->num : 0;
        $lastYearTotal = isset($pmrMap[$lastYearMonth]) ? (int)$pmrMap[$lastYearMonth]->num : 0;
        //环比
        if ($lastMonthTotal != 0) {
            $mom = self::getIncrRate($currentTotal, $lastMonthTotal);
        }

        //同比
        if ($lastYearTotal != 0) {
            $yoy = self::getIncrRate($currentTotal, $lastYearTotal);
        }

        //target
        $target = SalesTargetMonthRepository::getByHospitalsAndProduct($hospitalYYIDS, $product->series_yyid, $current);

        //获取当月相对数据
        return [
            //潜力
            'target' => $target ? $target->target : 0,
            //环比
            'mom' => $mom,
            //同比
            'yoy' => $yoy,
            //实际
            'total' => $currentTotal
        ];
    }

    /**
     * 统计最近6个月内产品的销售数据图表
     * @param YouyaoHospital $hospital
     * @param DrugProduct $product
     * @param string $lastMonth
     * @return array[]
     * @throws \Exception
     */
    public static function statisticChartData(
        YouyaoHospital $hospital,
        DrugProduct $product,
        $lastMonth = ""
    ): array {
        if (!$lastMonth) {
            $start = (new DateTime());
        } else {
            //要包含尾部数据，需要再加一个月
            $start = (new DateTime($lastMonth))->modify("+1 month");
        }


        $end = clone $start;
        $end->modify("-6 months");
        $interval = new DateInterval("P1M");
        $period = new DatePeriod($end, $interval, $start);

        $dateItems = [];
        /** @var DateTime $i */
        foreach ($period as $i) {
            $dateItems[] = $i->format('Y-m');
        }


        //获取范围内的数据(少量in可以直接用上索引)
        $resp = HospitalMonthTransPRepository::getMonthRangeDataByHospitalAndProduct($hospital, $product, $dateItems);
        $map = $resp->keyBy('month')->all();

        $data = [];
        $productName = $product->name;
        /** @var DateTime $i */
        foreach ($period as $i) {
            $m = $i->format('Y-m');
            $data[] = [
                'date' => $i->format('y/m'),
                'num' => isset($map[$m]) ? (int)$map[$m]->num : 0
            ];
        }

        return [
            'rows' => $data,
        ];
    }

    /**
     * 统计最近6个月内产品的销售数据图表
     * @param array $hospitalYYIDS
     * @param DrugProduct $product
     * @param string $lastMonth
     * @return array[]
     * @throws \Exception
     */
    public static function getProductChartDataLastSixMonth(
        array $hospitalYYIDS,
        DrugProduct $product,
        $lastMonth = ""
    ): array {
        if (!$lastMonth) {
            $start = (new DateTime());
        } else {
            //要包含尾部数据，需要再加一个月
            $start = (new DateTime($lastMonth))->modify("+1 month");
        }


        $end = clone $start;
        $end->modify("-6 months");
        $interval = new DateInterval("P1M");
        $period = new DatePeriod($end, $interval, $start);

        $dateItems = [];
        /** @var DateTime $i */
        foreach ($period as $i) {
            $dateItems[] = $i->format('Y-m');
        }


        //获取范围内的数据(少量in可以直接用上索引)
        $resp = HospitalMonthTransPRepository::getMonthRangeDataByHospitalsAndProduct(
            $hospitalYYIDS,
            $product,
            $dateItems
        );
        $map = $resp->keyBy('month')->all();

        $data = [];
        $productName = $product->name;
        /** @var DateTime $i */
        foreach ($period as $i) {
            $m = $i->format('Y-m');
            $data[] = [
                'date' => $i->format('y/m'),
                'num' => isset($map[$m]) ? (int)$map[$m]->num : 0
            ];
        }

        return [
            'rows' => $data,
        ];
    }

    /**
     * 统计历史数据总览
     * @param YouyaoHospital $hospital
     * @param DrugProduct $product
     * @return array
     * @throws \Exception
     */
    public static function statisticsOverviewData(YouyaoHospital $hospital, DrugProduct $product): array
    {
        $topMax = HospitalMonthTransPRepository::getMonthTopByHospitalAndProduct($hospital, $product);

        //获取历史统计数据
        $data = [
            'max' => 0,
            'total' => 0,
            'avg' => 0,
            'month' => '-'
        ];
        $lastMonth = '';
        if ($topMax && $topMax['max_month']) {
            $lastMonth = $topMax['max_month'];

            //获取历史一年内的总计和月均
            $a = new DateTime($lastMonth);
            $lastYear = $a->modify('-1 years')->format('Y-m');
            $sa = HospitalMonthTransPRepository::getAfterHospitalProductSumAndAvg($hospital, $product, $lastYear);

            $data = [
                'max' => (string)$topMax['max_num'],
                'total' => $sa ? $sa['sum_num'] : 0,
                'avg' => $sa && $sa['avg_num'] ? round($sa['avg_num']) : 0,
                'month' => $lastMonth
            ];
        }
        return [$lastMonth, $data];
    }

    /**
     * 获取产品月均最高值
     * @param YouyaoHospital $hospital
     * @param array $productYYID
     */
    public static function getProductsMonthTopData(YouyaoHospital $hospital, array $productYYID)
    {
        HospitalMonthTransPRepository::getMonthTopByHospitalAndProducts($hospital, $productYYID);
    }

    /**
     * 统计服务医院当月数据
     * @param $lastMonth
     * @param YouyaoHospital $hospital
     * @param DrugProduct $product
     * @param DrugSeries $series
     * @return array
     * @throws \Exception
     */
    public static function statisticOneMonthData(
        $lastMonth,
        YouyaoHospital $hospital,
        DrugProduct $product,
        DrugSeries $series
    ): array {
        //获取当月相对数据
        $lastMonthData = [
            //潜力
            'target' => 0,
            //环比
            'mom' => '-',
            //同比
            'yoy' => '-',
            //实际
            'total' => 0
        ];
        if ($lastMonth) {
            $mom = $yoy = "-";
            list($current, $lastMonth, $lastYearMonth) = self::calcOneMonthTime($lastMonth);

            $productMonthRangeData = HospitalMonthTransPRepository::getMonthRangeDataByHospitalAndProduct(
                $hospital,
                $product,
                [
                    $current,
                    $lastMonth,
                    $lastYearMonth
                ]
            );
            $pmrMap = $productMonthRangeData->keyBy('month')->all();

            $currentTotal = isset($pmrMap[$current]) ? (int)$pmrMap[$current]->num : 0;
            $lastMonthTotal = isset($pmrMap[$lastMonth]) ? (int)$pmrMap[$lastMonth]->num : 0;
            $lastYearTotal = isset($pmrMap[$lastYearMonth]) ? (int)$pmrMap[$lastYearMonth]->num : 0;
            //环比
            if ($lastMonthTotal != 0) {
                $mom = self::getIncrRate($currentTotal, $lastMonthTotal);
            }

            //同比
            if ($lastYearTotal != 0) {
                $yoy = self::getIncrRate($currentTotal, $lastYearTotal);
            }

            //target
            $target = SalesTargetMonthRepository::getByHospitalAndProduct($hospital, $series, $current);
            $lastMonthData = [
                'target' => $target ? $target->target : 0,
                'mom' => $mom,
                'yoy' => $yoy,
                'total' => $currentTotal
            ];
        }

        return $lastMonthData;
    }

    /**
     * 计算增长比率
     * @param $current
     * @param $before
     * @return false|float
     */
    public static function getIncrRate($current, $before)
    {
        if ($before == 0) {
            return 0;
        }
        $current = $current + 0.0;
        $before = $before + 0.0;
        return round(($current - $before) / $before * 100, 1);
    }

    /**
     * 获取过去三个月内的相对数据
     * @param YouyaoHospital $hospital
     * @param DrugProduct $product
     * @param DrugSeries $series
     * @return array
     */
    public static function statisticThreeMonthData(
        YouyaoHospital $hospital,
        DrugProduct $product,
        DrugSeries $series
    ): array {
        $data = [
            'target' => 0,
            'total' => 0,
            'mom' => '-',
            'yoy' => '-',
            'title' => '-'
        ];
        //获取最近的三个日期
        $reps = HospitalMonthTransPRepository::getLastServeRecord($hospital, $product, 3);

        if ($reps->isNotEmpty()) {
            //最近三月销量总和
            $total = $reps->sum("num");
            $dates = $reps->pluck('month')->all();

            $ago = function ($date, $step, $format = "Y-m") {
                return (new DateTime($date))->modify($step)->format($format);
            };

            //统计三个日期对应的三个月前的总销量
            $tm = "-3 months";
            $threeMonthAgoDates = array_map(function ($val) use ($ago, $tm) {
                return $ago($val, $tm);
            }, $dates);

            $threeMonthReps = HospitalMonthTransPRepository::getTotalByHospitalAndProducts(
                $hospital,
                $product,
                $threeMonthAgoDates
            );
            $monthTotal = $threeMonthReps ? $threeMonthReps['total'] : 0;
            $mom = "-";
            if ($monthTotal != 0) {
                $mom = TransactionService::getIncrRate($total, $monthTotal);
            }

            //统计三个日期对应一年前的总销量
            $ym = "-1 years";
            $oneYearAgoDates = array_map(function ($val) use ($ago, $ym) {
                return $ago($val, $ym);
            }, $dates);
            $lastYearReps = HospitalMonthTransPRepository::getTotalByHospitalAndProducts(
                $hospital,
                $product,
                $oneYearAgoDates
            );

            $lastYearTotal = $lastYearReps ? $lastYearReps['total'] : 0;
            $yoy = "-";
            if ($lastYearTotal != 0) {
                $yoy = TransactionService::getIncrRate($total, $lastYearTotal);
            }

            //获取最小日期和最大日期
            $min = $max = $dates[0];
            foreach ($dates as $item) {
                if ($min > $item) {
                    $min = $item;
                    continue;
                }
                if ($max < $item) {
                    $max = $item;
                    continue;
                }
            }

            //获取
            $targetResp = SalesTargetMonthRepository::getTotalByHospitalAndSeries($hospital, $series, $dates);
            $data = [
                'total' => $total,
                'target' => $targetResp ? (int)$targetResp['total'] : 0,
                'mom' => $mom,
                'yoy' => $yoy,
                'title' => sprintf(
                    "%s月-%s月数据",
                    Helper::formatInputDate($min, 'm'),
                    Helper::formatInputDate($max, 'm')
                )
            ];
        }

        return $data;
    }

    /**
     * @param $lastMonth
     * @return array
     * @throws \Exception
     */
    private static function calcOneMonthTime($lastMonth): array
    {
        //算当月
        $a = new DateTime($lastMonth);
        $current = $a->format('Y-m');
        //算环比
        $a->modify('-1 month');
        $lastMonth = $a->format('Y-m');
        //算同比
        $a->modify('-11 months');
        $lastYearMonth = $a->format('Y-m');
        return array($current, $lastMonth, $lastYearMonth);
    }

    private static function calcLastThreeMonthData(array $hospitalYYIDS, DrugProduct $drugProduct): array
    {
        $data = [
            'target' => 0,
            'total' => 0,
            'mom' => '-',
            'yoy' => '-',
            'title' => '-'
        ];
        //获取最近的三个日期
        $reps = HospitalMonthTransPRepository::getLastServeHospitalsProductRecord($hospitalYYIDS, $drugProduct, 3);

        if ($reps->isNotEmpty()) {
            //最近三月销量总和
            $total = $reps->sum("num");
            $dates = $reps->pluck('month')->all();

            $ago = function ($date, $step, $format = "Y-m") {
                return (new DateTime($date))->modify($step)->format($format);
            };

            //统计三个日期对应的三个月前的总销量
            $tm = "-3 months";
            $threeMonthAgoDates = array_map(function ($val) use ($ago, $tm) {
                return $ago($val, $tm);
            }, $dates);

            $threeMonthReps = HospitalMonthTransPRepository::getTotalByHospitalsAndProducts(
                $hospitalYYIDS,
                $drugProduct,
                $threeMonthAgoDates
            );
            $monthTotal = $threeMonthReps ? $threeMonthReps['total'] : 0;
            $mom = "-";
            if ($monthTotal != 0) {
                $mom = TransactionService::getIncrRate($total, $monthTotal);
            }

            //统计三个日期对应一年前的总销量
            $ym = "-1 years";
            $oneYearAgoDates = array_map(function ($val) use ($ago, $ym) {
                return $ago($val, $ym);
            }, $dates);
            $lastYearReps = HospitalMonthTransPRepository::getTotalByHospitalsAndProducts(
                $hospitalYYIDS,
                $drugProduct,
                $oneYearAgoDates
            );

            $lastYearTotal = $lastYearReps ? $lastYearReps['total'] : 0;
            $yoy = "-";
            if ($lastYearTotal != 0) {
                $yoy = TransactionService::getIncrRate($total, $lastYearTotal);
            }

            //获取最小日期和最大日期
            $min = $max = $dates[0];
            foreach ($dates as $item) {
                if ($min > $item) {
                    $min = $item;
                    continue;
                }
                if ($max < $item) {
                    $max = $item;
                    continue;
                }
            }

            //获取
            $targetResp = SalesTargetMonthRepository::getTotalByHospitalsAndSeries(
                $hospitalYYIDS,
                $drugProduct->series_yyid,
                $dates
            );

            $data = [
                'total' => $total,
                'target' => $targetResp ? (int)$targetResp['total'] : 0,
                'mom' => $mom,
                'yoy' => $yoy,
                'title' => sprintf(
                    "%s月-%s月数据",
                    Helper::formatInputDate($min, 'Y-m'),
                    Helper::formatInputDate($max, 'Y-m')
                )
            ];
        }

        return $data;
    }

    /**
     * 获取产品流水详情
     * @param int $productId
     * @return array
     */
    public static function getProductTransactionDetail(int $productId): array
    {
        $user = Helper::getLoginUser();

        $product = DrugProductRepository::getById($productId);
        if (!$product) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "产品不存在");
        }

        //检查用户是否服务该产品
        $isServed = RepsServeScopePRepository::checkUserServeProduct($user, $product);
        if (!$isServed) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户尚未服务该产品，无法查询流向");
        }

        //获取用户服务医院
        $hospitalYYIDS = ServeService::getUserServeHospitals($user);

        //获取统计数据
        $data = self::calcUserProductTransactions($product, $hospitalYYIDS);

        //获取6个月内的统计图表数据
        $chart = self::getProductChartDataLastSixMonth($hospitalYYIDS, $product, $data['last_serve_month']);

        //组合返回数据
        $data['chart'] = $chart;

        return $data;
    }

    public static function getRankHospitals(int $productId, $current = 1, $limit = 10): array
    {
        $user = Helper::getLoginUser();

        $product = DrugProductRepository::getById($productId);
        if (!$product) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "产品不存在");
        }

        //检查用户是否服务该产品
        $isServed = RepsServeScopePRepository::checkUserServeProduct($user, $product);
        if (!$isServed) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "用户尚未服务该产品，无法查询流向");
        }

        //获取用户服务医院
        $hospitalYYIDS = ServeService::getUserServeHospitals($user, $product);

        //获取最大月份
        $data = [];
        $max = HospitalMonthTransPRepository::getByProductAndHospitals($product, $hospitalYYIDS);
        $max = $max->toArray();
        $maxMonth = $max && $max['max_month'] ? (string)$max['max_month'] : '';
        if (!$maxMonth) {
            return [[
                'total' => 0,
                'current' => $current,
                'pages' => 0,
                'limit' => $limit
            ], $data];
        }
        $lastMonth = $max['max_month'];

        //根据产品和医院列表获取医院分页数据
        $resp = HospitalMonthTransPRepository::getHospitalsByRanks(
            $product,
            $hospitalYYIDS,
            $lastMonth,
            $current,
            $limit
        );

        $page = [
            'total' => $resp->total(),
            'current' => $resp->currentPage(),
            'pages' => $resp->lastPage(),
            'limit' => $limit
        ];
        if ($resp->isEmpty()) {
            return [$page, $data];
        }

        //获取医院
        /** @var Collection $col */
        $col = $resp->getCollection();
        $validHospitalYYIDS = $col->pluck('hospital_yyid')->all();
        $hospital = YouyaoHospitalRepository::getHospitalByYYIDs($validHospitalYYIDS);
        $hospitalMap = $hospital->keyBy('yyid')->all();

        //获取上一个月数量用来计算相对量
        $prevMonth = DateTime::createFromFormat('Y-m', $lastMonth)->modify('-1 month')->format('Y-m');
        $prevData = HospitalMonthTransPRepository::getDataByMonthProductAndHospitals(
            $validHospitalYYIDS,
            $product,
            $prevMonth
        );
        $prevMap = $prevData->keyBy('hospital_yyid')->all();

        //组合数据
        foreach ($resp as $item) {
            $hospitalYYID = $item['hospital_yyid'];
            $sum = (int)$item['sum'];
            if (!isset($hospitalMap[$hospitalYYID])) {
                continue;
            }
            /** @var YouyaoHospital $hospitalItem */
            $hospitalItem = $hospitalMap[$hospitalYYID];
            $prevNum = isset($prevMap[$hospitalYYID]) ? (int)$prevMap[$hospitalYYID]->num : 0;
            $incr = self::getIncrRate($sum, $prevNum);
            $data[] = [
                'id' => $hospitalItem->id,
                'name' => $hospitalItem->hospital_name,
                'sum' => $sum,
                'rate' => $incr
            ];
        }

        return [$page, $data];
    }

    /**
     * 获取流水医院
     * @param int $productId
     * @param string $keywords
     * @return array
     */
    public static function getTransHospitals(int $productId, string $keywords = ""): array
    {
        $user = Helper::getLoginUser();

        $product = DrugProductRepository::getById($productId);
        if (!$product) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "产品不存在");
        }

        //获取所有服务医院
        $serveList = RepsServeScopePRepository::getUserServeList($user);
        if ($serveList->isEmpty()) {
            return [];
        }
        $hospitalYYIDS = $serveList->pluck('hospital_yyid')->all();

        //查询数据
        $resp = RepsTransactionPRepository::getTransHospitals($product, $hospitalYYIDS, $keywords);

        //组合数据
        $data = [];
        $filter = [];
        /** @var YouyaoHospital $item */
        foreach ($resp as $item) {
            if (isset($filter[$item->id])) {
                continue;
            }
            $filter[$item->id] = true;

            $data[] = [
                'id' => $item->id,
                'name' => $item->hospital_name
            ];
        }

        return $data;
    }

    public static function monthList(int $productId): array
    {
        $user = Helper::getLoginUser();

        $product = DrugProductRepository::getById($productId);
        if (!$product) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "产品不存在");
        }


        return self::getUserFlowSixMonths();
    }

    private static function getUserFlowSixMonths(): array
    {
        //获取6个月未结算记录
        $unSettleMonths = self::getUnsettleMonth();

        //获取6个结算记录
        $settleMonths = self::getSettleMonth();

        //排序组合出月份及是否结算
        $list = array_unique(array_merge($unSettleMonths, $settleMonths));
        if (empty($list)) {
            return [];
        }

        rsort($list, SORT_STRING);
        $dates = array_slice($list, 0, 6);
        $ret = [];
        foreach ($dates as $item) {
            $ret[] = [
                'month' => $item,
                'is_trend' => in_array($item, $settleMonths) ? 1 : 0
            ];
        }

        return $ret;
    }

    private static function getSettleMonth(): array
    {
        $resp = RepsTransactionPRepository::getLastTransMonths(6);
        return $resp->pluck('transaction_month')->all();
    }

    private static function getUnsettleMonth(): array
    {
        //获取最后有记录的一个月
        $resp = CompanyMonthPointRepository::getLastMonth();
        $next = "";
        if ($resp) {
            $next = DateTime::createFromFormat("Y-m", $resp->month)->modify("+1 month")->format("Y-m-01");
        }

        $dates = SdcDataRepository::getLastDate($next, 6);
        if (empty($dates)) {
            return [];
        }
        $ret = [];
        foreach ($dates as $item) {
            $ret[] = $item->order_month;
        }
        return $ret;
    }


    public static function getDetails($productId, $hospitalId = 0, $month = ""): array
    {
        $user = Helper::getLoginUser();

        //获取产品
        $product = DrugProductRepository::getById($productId);
        if (!$product) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "产品不存在");
        }

        //获取医院
        $hospital = null;
        if ($hospitalId) {
            $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        }

        //检查药品和医院
        if (false == RepsServeScopePRepository::checkUserServeProduct($user, $product)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "您尚未服务该产品");
        }
        if ($hospital && false == RepsServeScopePRepository::checkUserServeHospitals($user, $hospital)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "您尚未申请服务该医院");
        }

        if ($month) {
            //固定月份
            $monthData =  self::getMonthData($user, $product, $month, $hospital);
        } else {
            $monthData = self::getMonthsData($user, $product, $hospital);
        }

        $productData = [
            'id' => $product->id,
            'name' => $product->name
        ];
        $hospitalData = $hospital ? [
            'id' => $hospital->id,
            'name' => $hospital->hospital_name
        ] : [];

        krsort($monthData);
        //非固定月份获取最近连续6个月数据
        return [array_values($monthData), $productData, $hospitalData];
    }

    private static function getMonthsData(Users $user, DrugProduct $product, YouyaoHospital $hospital = null): array
    {
        //获取最近6个月并按是否结算进行分割
        $months = self::getUserFlowSixMonths();
        if (empty($months)) {
            return [];
        }
        $trendMonth = $unTrendMonth = [];
        foreach ($months as $item) {
            $item['is_trend'] == 1 ? ($trendMonth[] = $item['month']) : ($unTrendMonth[] = $item['month']);
        }

        //获取统计医院范围
        $hospitalYYIDS = [];
        if ($hospital) {
            $hospitalYYIDS[] = $hospital->yyid;
        } else {
            //获取用户服务医院列表
            $resp = RepsServeScopePRepository::getUserServeList($user);
            $hospitalYYIDS = $resp->pluck('hospital_yyid')->unique()->all();
        }

        $data = [];
        if ($trendMonth) {
            $trendData = self::getTrendMonthsData($product, $trendMonth, $hospitalYYIDS);
            (!empty($trendMonth)) && $data = array_merge($data, $trendData);
        }

        if ($unTrendMonth) {
            $unTrendData = self::getUnTrendMonthsData($product, $unTrendMonth, $hospitalYYIDS, $user);
            (!empty($unTrendData)) && $data = array_merge($data, $unTrendData);
        }

        return $data;
    }

    /**
     * 获取未结算月份数据
     */
    private static function getUnTrendMonthsData(
        DrugProduct $product,
        array $unTrendMonth,
        array $hospitalYYIDS,
        Users $users
    ): array {
        $skus = DrugSkuRepository::getFromUserAndProduct($users, $product, $hospitalYYIDS);
        $skuYYIDS = $skus->pluck('yyid')->all();
        $resp = SdcDataRepository::getTransactionMultiMonthData($unTrendMonth, $skuYYIDS, $users, $hospitalYYIDS);
        if ($resp->isEmpty()) {
            return [];
        }

        $hospitalYYIDS = $resp->pluck('hospital_yyid')->unique()->all();
        $hospitals = YouyaoHospitalRepository::getHospitalByYYIDs($hospitalYYIDS);
        $hosMap = $hospitals->keyBy('yyid')->all();

        $data = [];
        foreach ($resp as $item) {
            $month = $item->order_month;
            $hosItemName = isset($hosMap[$item->hospital_yyid]) ? $hosMap[$item->hospital_yyid]->hospital_name : '';
            (!isset($data[$month])) && $data[$month] = [
                'sum' => 0,
                'details' => [],
                'month_date' => $month
            ];
            $data[$month]['sum'] += $item->order_num;
            $data[$month]['details'][] = [
                'month_date' => $item->order_date,
                'hospital_name' => $hosItemName,
                'total' => $item->order_num
            ];
        }

        return $data;
    }

    private static function getTrendMonthsData(DrugProduct $product, array $trendMonth, array $hospitalYYIDS): array
    {
        $resp = RepsTransactionPRepository::getTransactionsMontshList($product, $trendMonth, $hospitalYYIDS);
        if ($resp->isEmpty()) {
            return [];
        }
        $respHospitalYYIDS =  $resp->pluck('hospital_yyid')->all();
        $hospitals = YouyaoHospitalRepository::getHospitalByYYIDs($respHospitalYYIDS);
        $hosMap = $hospitals->keyBy('yyid')->all();


        $data = [];
        foreach ($resp as $item) {
            $hosItemName = isset($hosMap[$item['hospital_yyid']]) ? $hosMap[$item['hospital_yyid']]->hospital_name : '';
            $month = $item['transaction_month'];
            (!isset($data[$month])) && $data[$month] = [
                'sum' => 0,
                'details' => [],
                'month_date' => $month
            ];

            $data[$month]['sum'] += $item['num'];
            $data[$month]['details'][] = [
                'month_date' => $item['transaction_date'],
                'hospital_name' => $hosItemName,
                'total' => $item['num']
            ];
        }

        return $data;
    }



    private static function getMonthData(
        Users $user,
        DrugProduct $product,
        $month,
        YouyaoHospital $hospital = null
    ): array {
        $hospitalYYIDS = [];
        if ($hospital) {
            $hospitalYYIDS[] = $hospital->yyid;
        } else {
            //获取用户服务医院列表
            $resp = RepsServeScopePRepository::getUserServeList($user);
            $hospitalYYIDS = $resp->pluck('hospital_yyid')->unique()->all();
        }

        //判断当月是否结算
        $isTrend = RepsTransactionPRepository::checkMonthTransed($month);

        $data[$month] = [
            'sum' => 0,
            'month_date' => $month,
            'details' => []
        ];
        if (!$isTrend) {
            //未结算
            $sku = DrugSkuRepository::getFromUserAndProduct($user, $product);
            $skuYYID = $sku->pluck('yyid')->all();
            $resp = SdcDataRepository::getTransactionMonthData($month, $skuYYID, $user, $hospitalYYIDS);
            if ($resp->isEmpty()) {
                return $data;
            }
            $hospitalYYIDS = $resp->pluck('hospital_yyid')->unique()->all();
            $hospitals = YouyaoHospitalRepository::getHospitalByYYIDs($hospitalYYIDS);
            $hospitalMap = $hospitals->keyBy('yyid')->all();

            foreach ($resp as $item) {
                $hosItemName = isset($hospitalMap[$item->hospital_yyid]) ?
                    $hospitalMap[$item->hospital_yyid]->hospital_name :  "";
                $data[$month]['sum'] += ($item->order_num ? $item->order_num : 0);
                $data[$month]['details'][] = [
                        'month_date' => $item->order_date,
                        'hospital_name' => $hosItemName,
                        'total' => $item->order_num
                ];
            }
        } else {
            //已结算
            $resp = RepsTransactionPRepository::getTransactionsMonthList($product, $month, $hospitalYYIDS);
            if ($resp->isEmpty()) {
                return $data;
            }
            $hospitalYYIDS = $resp->pluck('hospital_yyid')->unique()->all();
            $hospitals = YouyaoHospitalRepository::getHospitalByYYIDs($hospitalYYIDS);
            $hospitalMap = $hospitals->keyBy('yyid')->all();

            /**
             * @var RepsTransactionP $item
             */
            foreach ($resp as $item) {
                $hosItemName = isset($hospitalMap[$item->hospital_yyid]) ?
                    $hospitalMap[$item->hospital_yyid]->hospital_name :  "";
                $data[$month]['sum'] += $item->num;
                $data[$month]['details'][] = [
                        'month_date' => $item['transaction_date'],
                        'hospital_name' => $hosItemName,
                        'total' => $item['num']
                ];
            }
        }

        return $data;
    }

    public static function getDailyMsgTransaction(string $msgDate, $orderDate, $msgId = 0): array
    {
        $user = Helper::getLoginUser();

        //获取sku
        $sku = DrugSkuRepository::getUserServeDrugs($user);
        if ($sku->isEmpty()) {
            return [];
        }

        //获取产品
        $productYYIDS = $sku->pluck('product_yyid')->unique()->all();
        $products = DrugProductRepository::getByYYIDS($productYYIDS);
        $productMap = $products->keyBy('yyid')->all();

        //获取流向(以产品-医院聚合出总数量)
        $skuYYID = $sku->pluck('yyid')->unique()->all();
        $trans = SdcDataRepository::getDailyTrans($user, $skuYYID, $orderDate, $msgDate);
        $transMap = $trans->groupBy('drug_yyid')->all();

        $data = [];
        /** @var DrugSku $item */
        foreach ($sku as $item) {
            if (!isset($productMap[$item->product_yyid])) {
                continue;
            }
            if (!isset($transMap[$item->yyid])) {
                continue;
            }

            $subItem = [
                'name' => $item->name,
                'product_id' => $productMap[$item->product_yyid]->id,
                'date' => $orderDate,
                'detail' => []
            ];
            foreach ($transMap[$item->yyid] as $transItem) {
                $subItem['detail'][] = [
                    'hospital' => $transItem->hospital_name,
                    'order_num' => $transItem->order_num
                ];
            }
            $data[] = $subItem;
        }
        if ($msgId && $msgId > 0) {
            //更新消息已读，调用并丢弃
            $isApp = Helper::getAuthRole() == Auth::AUTH_ROLE_YOU_YAO_APP;
            go(function () use ($msgId, $user, $isApp) {
                $req = new OneMsgDetailRequest();
                $req->setMsgId($msgId);
                $req->setPlatform($isApp ? DataStatus::MESSAGE_PLATFORM_APP : DataStatus::MESSAGE_PLATFORM_AGENT);
                $req->setUserId($user->uid);
                $req->setUserType(DataStatus::USER_TYPE_AGENT);
                (new MsgRpcService())->fetchOneMsgDetail($req);
            });
        }

        return $data;
    }
}
