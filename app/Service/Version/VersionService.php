<?php
declare(strict_types=1);

namespace App\Service\Version;

use App\Repository\VersionRepository;

class VersionService
{
    /**
     * @param $osType
     * @return \Hyperf\Database\Model\Builder|\Hyperf\Database\Model\Model|object|null
     */
    public function getVersion($osType)
    {
        return VersionRepository::getVersion($osType);
    }
}
