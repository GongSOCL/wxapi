<?php
declare(strict_types=1);
namespace App\Service\Favorites;

use Grpc\Favorites\AddFavoritesReply;
use Grpc\Favorites\AddFavoritesRequest;
use Grpc\Favorites\AddResourceReply;
use Grpc\Favorites\AddResourceRequest;
use Grpc\Favorites\DeleteFavoritesReply;
use Grpc\Favorites\DeleteFavoritesRequest;
use Grpc\Favorites\DeleteResourceReply;
use Grpc\Favorites\DeleteResourceRequest;
use Grpc\Favorites\FavoritesDetailReply;
use Grpc\Favorites\FavoritesDetailRequest;
use Grpc\Favorites\FavoritesListReply;
use Grpc\Favorites\FavoritesListRequest;
use Grpc\Favorites\NameFavoritesReply;
use Grpc\Favorites\NameFavoritesRequest;
use Grpc\Favorites\ResourceInFavoritesReply;
use Grpc\Favorites\ResourceInFavoritesRequest;
use Youyao\Framework\Client\CoreClient;

class FavoritesRpcService extends CoreClient
{
    /**
     * @param FavoritesListRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function getFavoritesList(FavoritesListRequest $argument)
    {
        return $this->sendRequest($argument, [FavoritesListReply::class, 'decode']);
    }

    /**
     * @param FavoritesDetailRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function getFavoritesDetail(FavoritesDetailRequest $argument)
    {
        return $this->sendRequest($argument, [FavoritesDetailReply::class, 'decode']);
    }

    /**
     * @param AddFavoritesRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function addFavorites(AddFavoritesRequest $argument)
    {
        return $this->sendRequest($argument, [AddFavoritesReply::class, 'decode']);
    }

    /**
     * @param DeleteFavoritesRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function deleteFavorites(DeleteFavoritesRequest $argument)
    {
        return $this->sendRequest($argument, [DeleteFavoritesReply::class, 'decode']);
    }

    /**
     * @param NameFavoritesRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function renameFavorites(NameFavoritesRequest $argument)
    {
        return $this->sendRequest($argument, [NameFavoritesReply::class, 'decode']);
    }

    /**
     * @param AddResourceRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function addResource(AddResourceRequest $argument)
    {
        return $this->sendRequest($argument, [AddResourceReply::class, 'decode']);
    }

    /**
     * @param DeleteResourceRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function deleteResource(DeleteResourceRequest $argument)
    {
        return $this->sendRequest($argument, [DeleteResourceReply::class, 'decode']);
    }

    /**
     * @param ResourceInFavoritesRequest $argument
     * @return \Google\Protobuf\Internal\Message
     */
    public function getResourceInFavorites(ResourceInFavoritesRequest $argument)
    {
        return $this->sendRequest($argument, [ResourceInFavoritesReply::class, 'decode']);
    }
}
