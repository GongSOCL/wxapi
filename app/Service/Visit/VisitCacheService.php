<?php
declare(strict_types=1);


namespace App\Service\Visit;

use App\Constants\VisitSupplement;
use App\Helper\Helper;
use App\Helper\Lock;
use Hyperf\Utils\ApplicationContext;

class VisitCacheService
{
    public static function clearHospital($hospitalId, $companyYYID = "")
    {
        $geoKey = self::buildHospitalKey($hospitalId, $companyYYID);
        $emptyKey = self::buildHospitalDistEmpty($hospitalId, $companyYYID);
        $maxKey = self::buildHospitalGeoMaxKey($hospitalId, $companyYYID);
        $redis = Helper::getRedis('pod');

        $redis->del($geoKey, $emptyKey, $maxKey);
    }

    private static function buildHospitalMaintenanceKey($hospitalId, $companyYYID = ""): string
    {
        return "youyao:geo:empty:hospital:{$companyYYID}:{$hospitalId}";
    }

    public static function enterMaintenanceMode($hospitalId, $companyYYID = "")
    {
        $key = self::buildHospitalMaintenanceKey($hospitalId, $companyYYID);
        return ApplicationContext::getContainer()->get(Lock::class)
            ->acquire($key, 10, 30);
    }

    public static function leaveMaintenanceModel($hospitalId, $identifier, $companyYYID = ""): bool
    {
        $key = self::buildHospitalMaintenanceKey($hospitalId, $companyYYID);
        return ApplicationContext::getContainer()->get(Lock::class)
            ->release($key, $identifier);
    }

    public static function refreshCache($hospitalId, array $geoList = [], $maxDist = 0, $companyYYID = "")
    {
        $geoKey = self::buildHospitalKey($hospitalId, $companyYYID);
        $emptyKey = self::buildHospitalDistEmpty($hospitalId, $companyYYID);
        $maxKey = self::buildHospitalGeoMaxKey($hospitalId, $companyYYID);
        $redis = Helper::getRedis('pod');

        //清除所有key
        $redis->del($geoKey, $emptyKey, $maxKey);

        //设置最大距离
        $redis->set($maxKey, $maxDist, [
            'ex' => 3600
        ]);

        if (empty($geoList)) {
            $redis->set($emptyKey, 1, [
                'nx',
                'ex' => 3600
            ]);
            return;
        }

        $pipeline= $redis->pipeline();
        foreach ($geoList as $geoItem) {
            $id = $geoItem['id'];
            $lng = $geoItem['lng'];
            $lat = $geoItem['lat'];
            $pointKey = self::buildPos($id);
            $pipeline->geoadd($geoKey, $lng, $lat, $pointKey);
        }

        //批量添加地理坐标并设置过期时间一小时
        $res = $pipeline->expire($geoKey, 3600)
            ->exec();

        Helper::getLogger()->info('_add_geo_res', [
            'res' => $res,
            'hospitalId' => $hospitalId,
            'company_yyid' => $companyYYID
        ]);
    }

    private static function buildHospitalDistEmpty($hospitalId, $dealerYYID = ""): string
    {
        return "youyao:geo:empty:hospital:{$dealerYYID}:{$hospitalId}";
    }

    private static function buildHospitalKey($hospitalId, $dealerYYID = ""): string
    {
        return "youyao:geo:hospital:{$dealerYYID}:{$hospitalId}";
    }

    private static function buildHospitalGeoMaxKey($hospitalId, $dealerYYID = ""): string
    {
        return "youyao:geo:max:hospital:{$dealerYYID}:{$hospitalId}";
    }

    private static function buildPos($posId): string
    {
        return "point:{$posId}";
    }

    public static function isCached($hospitalId, $companyYYID = "")
    {
        $key = self::buildHospitalKey($hospitalId, $companyYYID);
        $redis = Helper::getRedis('pod');
        return $redis->exists($key);
    }

    public static function isEmpty($hospitalId, $companyYYID = "")
    {
        $key = self::buildHospitalDistEmpty($hospitalId, $companyYYID);
        $redis = Helper::getRedis('pod');
        return $redis->exists($key);
    }


    /**
     * 获取与当前输入点满足最大距离范围内的所有点
     * @param $hospitalId
     * @param $lng
     * @param $lat
     * @param string $companyYYID
     * @return array
     */
    public static function getHospitalMaxRadiusPoints($hospitalId, $lng, $lat, $companyYYID = ""): array
    {
        $geoKey = self::buildHospitalKey($hospitalId, $companyYYID);
        $maxKey = self::buildHospitalGeoMaxKey($hospitalId, $companyYYID);
        $redis = Helper::getRedis('pod');
        $max =  $redis->get($maxKey);
        $max  = $max ?? 0;

        $lng = $lng + 0.0;
        $lat = $lat + 0.0;
        $max = $max + 0.0;
        $res = $redis->georadius($geoKey, $lng, $lat, $max, 'm', [
            'WITHDIST'
        ]);
        Helper::getLogger()->info("georadius", [
            'res' => $res,
            'lng' => $lng,
            'lat' => $lat,
            'max' => $max,
            'key' => $geoKey
        ]);

        $result = [];
        if (!$res) {
            $res = [];
        }
        $logger = Helper::getLogger();
        foreach ($res as $item) {
            [$point, $dist] = $item;
            $logger->info('point_dist', [
                'point' => $point,
                'dist' => $dist
            ]);
            if (substr($point, 0, 6) == 'point:') {
                $id = intval(substr($point, 6));
                $id > 0 && $result[] = [
                    'id' => $id,
                    'dist' => $dist
                ];
            }
        }
        return $result;
    }

    public static function calcGpsDistinct(array $hospitalGps, array $checkInGps = [], array $checkOutGps = []): array
    {
        $redis = Helper::getRedis('pod');

        //批量添加gps点
        $pipeLine = $redis->pipeline();
        $tmpKey = sprintf("visit:gps:tmp:distinct:%s", uniqid());
        $key = [];
        foreach ($hospitalGps as $i => $item) {
            $p = sprintf("t%s", $i);
            $pipeLine->geoadd($tmpKey, $item['lng'], $item['lat'], $p);
            $key[$p] = $i;
        }
        if (!empty($checkInGps)) {
            $pipeLine->geoadd($tmpKey, $checkInGps['lng'], $checkInGps['lat'], "checkIn");
        }
        if (!empty($checkOutGps)) {
            $pipeLine->geoadd($tmpKey, $checkOutGps['lng'], $checkOutGps['lat'], 'checkOut');
        }
        $pipeLine->expire($tmpKey, 600)->exec();

        //计算各个点同签入点之间的距离
        if ($checkInGps) {
            foreach ($key as $p => $index) {
                $distinct = $redis->geodist($tmpKey, $p, "checkIn", 'm');
                $hospitalGps[$index]['check_in_distinct'] = $distinct;
            }
        }

        //计算各个点同签出点之间的距离
        if ($checkOutGps) {
            foreach ($key as $p => $index) {
                $distinct = $redis->geodist($tmpKey, $p, "checkOut", 'm');
                $hospitalGps[$index]['check_out_distinct'] = $distinct;
            }
        }
        $redis->unlink($tmpKey);

        return $hospitalGps;
    }

    public static function clearCompanySupplement($companyId)
    {
        $key = "youyao:visit:supplement:company";
        $field = sprintf("cid:%d", $companyId);
        $redis = Helper::getRedis();
        $redis->hDel($key, $field);
    }

    public static function clearUserSupplement($userId)
    {
        $keyId = $userId % 512;
        $key = sprintf("youyao:visit:supplement:user:%d", $keyId);
        $field = sprintf("uid:%d", $userId);
        $redis = Helper::getRedis();
        $redis->hDel($key, $field);
    }

    private static function buildCompanySupplementCacheKey($companyId): string
    {
        return sprintf("youyao:visit:supplement:company:%d", $companyId);
    }

    public static function checkCompanySupplement($companyId): int
    {
        $key = "youyao:visit:supplement:company";
        $field = sprintf("cid:%d", $companyId);
        $redis = Helper::getRedis();
        $value = $redis->hGet($key, $field);
        if (false === $value) {
            return VisitSupplement::STATUS_COMPANY_NONE;
        }

        return $value == VisitSupplement::STATUS_COMPANY_YES ?
            VisitSupplement::STATUS_COMPANY_YES : VisitSupplement::STATUS_COMPANY_NO;
    }

    public static function checkUserSupplement($userId): int
    {
        $keyId = $userId % 512;
        $key = sprintf("youyao:visit:supplement:user:%d", $keyId);
        $field = sprintf("uid:%d", $userId);
        $redis = Helper::getRedis();
        $value = $redis->hGet($key, $field);
        if (false === $value) {
            return VisitSupplement::STATUS_USER_NONE;
        }

        return $value == VisitSupplement::STATUS_USER_YES ?
            VisitSupplement::STATUS_USER_YES : VisitSupplement::STATUS_USER_NO;
    }

    /**
     * 添加用户是否支持补签
     * @param $userId
     * @param bool $isSuplement
     * @return void
     * @throws \RedisException
     */
    public static function addUserSupplement($userId, $isSuplement = false)
    {
        $keyId = $userId % 512;
        $key = sprintf("youyao:visit:supplement:user:%d", $keyId);
        $field = sprintf("uid:%d", $userId);
        $redis = Helper::getRedis();
        $redis->hSet($key, $field, $isSuplement ? VisitSupplement::STATUS_USER_YES : VisitSupplement::STATUS_USER_NO);
    }

    /**
     * 添加缓存公司是否支持补签
     * @param $companyId
     * @param $isSupplement
     * @return void
     * @throws \RedisException
     */
    public static function addCompanySupplement($companyId, $isSupplement = false)
    {
        $key = "youyao:visit:supplement:company";
        $field = sprintf("cid:%d", $companyId);
        $redis = Helper::getRedis();
        $redis->hSet(
            $key,
            $field,
            $isSupplement ? VisitSupplement::STATUS_COMPANY_YES : VisitSupplement::STATUS_COMPANY_NO
        );
        $redis->expire($key, 60 * 60 * 24);
    }
}
