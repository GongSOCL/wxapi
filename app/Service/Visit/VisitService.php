<?php
declare(strict_types=1);


namespace App\Service\Visit;

use App\Constants\Code\ErrorCodeGroup;
use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Constants\VisitSupplement;
use App\Exception\BusinessException;
use App\Exception\GroupException;
use App\Helper\Helper;
use App\Helper\TaskManageHelper;
use App\Model\Qa\AgentVisit;
use App\Model\Qa\AgentVisitConfig;
use App\Model\Qa\AgentVisitGeo;
use App\Model\Qa\AgentVisitPic;
use App\Model\Qa\Company;
use App\Model\Qa\DealerHospitalGps;
use App\Model\Qa\DrugSeries;
use App\Model\Qa\NewDoctorOpenidUserinfo;
use App\Model\Qa\NewDoctorUserinfo;
use App\Model\Qa\SystemDepart;
use App\Model\Qa\Users;
use App\Model\Qa\YouyaoHospital;
use App\Repository\AgentVisitConfigRepository;
use App\Repository\AgentVisitDepartRepository;
use App\Repository\AgentVisitDoctorRepository;
use App\Repository\AgentVisitDrugSeriesRepository;
use App\Repository\AgentVisitGeoRepository;
use App\Repository\AgentVisitLogRepository;
use App\Repository\AgentVisitPicRepository;
use App\Repository\AgentVisitRepository;
use App\Repository\CompanyRepository;
use App\Repository\DealerHospitalCRepository;
use App\Repository\DealerHospitalGpsRepository;
use App\Repository\DoctorOpenidUserinfoRepository;
use App\Repository\DrugSeriesRepository;
use App\Repository\NewDoctorOpenidUserinfoRepository;
use App\Repository\SystemDepartRepository;
use App\Repository\UploadQiniuRepository;
use App\Repository\UsersRepository;
use App\Repository\VisitSupplementCompanyRepository;
use App\Repository\VisitSupplementUserRepository;
use App\Repository\YouyaoHospitalLngLatRepository;
use App\Repository\YouyaoHospitalRepository;
use App\Service\Common\AmapService;
use App\Service\Common\GeoService;
use App\Service\Group\Doctor\DoctorSelectRedisLogic;
use App\Service\Points\PointRpcService;
use App\Service\Upload\UploadService;
use Grpc\Common\CommonPageResponse;
use Grpc\Point\UserType;
use Grpc\Task\VisitNoticeType;
use Grpc\Task\VisitWechatNotice;
use Grpc\Wxapi\Visit\CompanyListReply;
use Grpc\Wxapi\Visit\CompanySupplementItem;
use Grpc\Wxapi\Visit\GetVisitDistinctResponse;
use Grpc\Wxapi\Visit\HospitalDistinct;
use Grpc\Wxapi\Visit\SearchUserItem;
use Grpc\Wxapi\Visit\SearchUserToAddSupplementReply;
use Grpc\Wxapi\Visit\UserListReply;
use Grpc\Wxapi\Visit\UserSupplementItem;
use Grpc\Wxapi\Visit\VisitGeo;
use Hyperf\DbConnection\Db;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Collection;
use Youyao\Framework\ErrCode;

class VisitService
{
    /**
     * 列表.
     *
     * @param string $checkInStartAt
     * @param string $checkInEndAt
     * @param string $checkOutStartAt
     * @param string $checkOutEndAt
     * @param int $checkType
     * @param int $visitType
     * @param int $hospitalId
     * @param array $officeIds
     * @param int $current
     * @param int $limit
     * @return array
     */
    public static function list(
        $checkInStartAt = "",
        $checkInEndAt = "",
        $checkOutStartAt = "",
        $checkOutEndAt = "",
        $checkType = 0,
        $visitType = 0,
        $hospitalId = 0,
        array $officeIds = [],
        $current = 1,
        $limit = 10,
        $createStartAt = "",
        $createEndAt = ""
    ): array {
        $user = Helper::getLoginUser();

        //获取数据带分页
        $data = AgentVisitRepository::getUserVisitData(
            $user,
            $checkInStartAt,
            $checkInEndAt,
            $checkOutStartAt,
            $checkOutEndAt,
            $createStartAt,
            $createEndAt,
            $checkType,
            $visitType,
            $hospitalId,
            $officeIds,
            $current,
            $limit
        );

        //组合返回数据
        $result = [];
        $pageData = [
            'total' => $data->total(),
            'current' => $data->currentPage(),
            'pages' => $data->lastPage(),
            'limit' => $data->perPage()
        ];
        if ($data->isNotEmpty()) {
            $collect = $data->getCollection();
            // 获取医院
            $hospitalIds = $collect->pluck('hospital_id')
                ->unique()
                ->toArray();
            $hospitalMap = [];
            if ($hospitalIds) {
                $hospitals = YouyaoHospitalRepository::getHospitalByIds($hospitalIds);
                $hospitalMap = $hospitals->keyBy('id')->toArray();
            }

            // 获取科室
            $visitIds = $collect->pluck('id')
                    ->toArray();
            $departs = AgentVisitDepartRepository::getVisitDepartsByVisitIds($visitIds);
            $departMap = $departs->groupBy('visit_id')->all();

            // 获取医生
            $doctorMaps = [];
            if (!empty($visitIds)) {
                $visitDoctors = AgentVisitDoctorRepository::getVisitsAllDoctors($visitIds);
                $doctorMaps = $visitDoctors->groupBy('visit_id')->all();
            }

            /** @var AgentVisit $item */
            foreach ($data->items() as $item) {
                //处理医院
                $hospitalId = $item->hospital_id;
                $hospitalName = $hospitalId > 0
                && isset($hospitalMap[$hospitalId]) ? $hospitalMap[$hospitalId]['hospital_name'] : '';

                //处理时间
                $date = "";
                $isCheckIn = AgentVisit::CHECK_STATUS_CHECK_IN == $item->check_status;
                if ($isCheckIn) {
                    $date = Helper::formatInputDate($item->check_in_time, 'Y-m-d');
                } else {
                    $date = Helper::formatInputDate($item->check_out_time, 'Y-m-d');
                }

                //获取科室
                $visitId = $item->id;
                $departs = [];
                if (isset($departMap[$visitId])) {
                    /** @var SystemDepart $v */
                    foreach ($departMap[$visitId] as $v) {
                        $departs[] =  $v->name;
                    }
                }
                $doctorNames = [];
                if (isset($doctorMaps[$visitId])) {
                    /** @var NewDoctorUserinfo $v */
                    foreach ($doctorMaps[$visitId] as $v) {
                        $doctorNames[] = $v->true_name ? $v->true_name :  ($v->nickname ? $v->nickname : '-');
                    }
                }

                // 组合数据
                $result[] = [
                    'id' => $item->id,
                    'date' => $date,
                    'check_type' => $isCheckIn ? 1 : 2,
                    'hospital' => $hospitalName,
                    'type' => self::getVisitTypeDesc($item->visit_type),
                    'departs' => $departs,
                    'doctors' => $doctorNames,
                    'state' => is_null($item->visit_state) ? '' : (int)$item->visit_state,
                    'state_comment' => (string)$item->visit_state_comment
                ];
            }
        }

        return [$pageData, $result];
    }

    public static function getVisitTypeDesc($type): string
    {
        switch ($type) {
            case AgentVisit::VISIT_TYPE_TO_FACE:
                return "面对面拜访";
                break;
            case AgentVisit::VISIT_TYPE_ONLINE:
                return "线上拜访";
                break;
            case AgentVisit::VISIT_TYPE_INTERNAL_MEETING:
                return "内部会议";
                break;
            case AgentVisit::VISIT_TYPE_EXTERNAL_MEETING:
                return "外部会议";
            case AgentVisit::VISIT_TYPE_ADMINISTRATIVE_WORK:
                return "行政类工作";
            default:
                return "其它拜访方式";
                break;
        }
    }

    /**
     * 详情.
     *
     * @param $id
     * @return array
     */
    public static function info($id): array
    {
        $user = Helper::getLoginUser();
        //获取拜访记录
        $visit = AgentVisitRepository::getVisitById($id);
        if (!$visit) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_NOT_EXISTS);
        }
        // 获取药品
        $series = AgentVisitDrugSeriesRepository::getVisitDrugSeries($visit);
        $seriesIds = $series->isEmpty() ? [] : $series->pluck('series_id')->unique()->toArray();
        $drugSeries = DrugSeriesRepository::getByIds($seriesIds);
        $seriesList = [];
        /** @var DrugSeries $item */
        foreach ($drugSeries as $item) {
            $seriesList[] = [
                'id' => $item->id,
                'name' => $item->series_name
            ];
        }

        // 获取照片(编辑由于涉及到照片的更新，需要区分状态获取)
        $pics = [];
        $picType = $visit->isCheckIn() ? AgentVisitPic::PIC_TYPE_CHECK_IN : AgentVisitPic::PIC_TYPE_CHECK_OUT;
        $uploads = AgentVisitPicRepository::getVisitUpload($visit, $picType);
        $uploadIds = $uploads->pluck('upload_id')->unique()->toArray();
        if (!empty($uploadIds)) {
            $pics = UploadService::getItemByIds($uploadIds);
        }

        // 获取部门
        $departs = new Collection();
        $visitDepartIds = $visit->visitDeparts->pluck('depart_id')->toArray();
        if (!empty($visitDepartIds)) {
            $departs = SystemDepartRepository::getByIds($visitDepartIds);
        }
        $departList = [];
        $departIds = [];
        if ($departs->isNotEmpty()) {
            /** @var SystemDepart $item */
            foreach ($departs as $item) {
                $departIds[] = $item->id;
                $departList[] = [
                    'id' => $item->id,
                    'name' =>$item->name
                ];
            }
        }

        // 获取地理位置
        $geo = $visit->isCheckIn() ? $visit->checkInGeo : $visit->checkOutGeo ;
        $geoOpt = $visit->isCheckIn() ? $visit->check_in_geo_opt : $visit->check_out_geo_opt;
        $geoComment = $visit->isCheckIn() ? $visit->check_in_geo_comment : $visit->check_out_geo_comment;

        // 获取医院
        $hospital = $visit->hospital;
        $doctors = [];
        if ($hospital && (!empty($departIds))) {
            //获取可用doctor列表
            $resp = AgentVisitDoctorRepository::getDoctors($visit);
            foreach ($resp as $doctorInfo) {
                $doctors[] = [
                    'id' => $doctorInfo->id,
                    'name' => $doctorInfo->true_name ?? ($doctorInfo->nickname ?? '-')
                ];
            }
        }


        //返回数据
        return  [
            'id' => $visit->id,
            'hospital' => $hospital ?  [
                'id' => $hospital->id,
                'name' => $hospital->hospital_name,
                'org_type' => self::getUserCompanyDealerHospital($user, $hospital)
            ] : [],
            'doctors' => $doctors,
            'visit_type' => $visit->visit_type,
            'visit_status' => $visit->check_status,
            'departs' => $departList,
            'series_ids' => $seriesList,
            'result' => $visit->visit_result,
            'next' => $visit->next_visit_purpose,
            'pics' => $pics,
            'lat' => $geo ? $geo->latitude : 0,
            'lng' => $geo ? $geo->longitude : 0,
            'is_fake_geo' => $geo ? $geo->is_fake_geo : 0,     // TODO: fake geo
            'coordinate_type' => $geo ? $geo->origin_coordinate_type : 'gps',
            'position' => $geo ? $geo->sign_pos : "",
            'geo_opt' => $geoOpt,
            'geo_comment' => $geoComment,
            'is_collaborative' => $visit->is_collaborative,
            'is_supplement' => (int) $visit->is_supplement,
        ];
    }

    /**
     * 获取用户医院对应公司获取类型.
     *
     * @param Users $user
     * @param YouyaoHospital $hospital
     * @return int
     */
    private static function getUserCompanyDealerHospital(Users $user, YouyaoHospital $hospital): int
    {
        $dealerHospital = DealerHospitalCRepository::getUserCompanyHospital($user, $hospital);
        if ($dealerHospital) {
            return $dealerHospital->getHospitalType();
        }
        return $hospital->org_type ? $hospital->org_type : 1;
    }

    /**
     * 获取拜访配置.
     *
     * @param $type
     * @return array
     */
    public static function getVisitConfig($type): array
    {
        $resp = AgentVisitConfigRepository::getConfigsByType($type);
        if ($resp->isEmpty()) {
            return [];
        }

        $configs = [];
        /** @var AgentVisitConfig $item */
        foreach ($resp as $item) {
            $configs[] = [
                'id' => $item->id,
                'name' => $item->config_name,
                'is_extra_input' => $item->is_extra_input
            ];
        }
        return $configs;
    }

    /**
     * @param $visitId
     * @return array
     */
    public static function getNewCheckOutInfo($visitId): array
    {
        $user = Helper::getLoginUser();
        //拜访信息
        $visitInfo = AgentVisitRepository::getVisitById($visitId);
        if (!$visitInfo) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_NOT_EXISTS);
        }

        //医院信息
        $hospital = $visitInfo->hospital;
        $hospitalInfo = $hospital ? [
            'id' => $hospital->id,
            'name' => $hospital->hospital_name,
            'org_type' => self::getUserCompanyDealerHospital($user, $hospital)
        ] : [];

        return [
            'id' => $visitId,
            'hospital' => $hospitalInfo,
            'visit_type' => $visitInfo->visit_type,
            'is_supplement' => (int) $visitInfo->is_supplement
        ];
    }

    /**
     * 签出.
     *
     * @param $id
     * @param $hospitalId
     * @param $visitType
     * @param array $departIds
     * @param array $seriesIds
     * @param string $result
     * @param string $next
     * @param int $lat
     * @param int $lng
     * @param string $position
     * @param array $picIds
     * @param int $geoOpt
     * @param string $geoComment
     * @param array $doctorIds
     * @throws \Exception
     */
    public static function checkOut(
        $id,
        $hospitalId,
        $visitType,
        array $departIds = [],
        array $seriesIds = [],
        $result = "",
        $next = "",
        $lat = 0,
        $lng = 0,
        $position = "",
        $picIds = [],
        $geoOpt = 0,
        $geoComment = "",
        array $doctorIds = [],
        $coordinateType = DataStatus::COORDINATE_GPS,
        $isCollaborative = 0,
        $checkOutTime = '',
        $isFakeGeo = false
    ) {
        // 修改：$departIds不再从参数获取，而是根据医生所在的科室获取，同时需兼容从参数获取departIds的版本
        $validDoctorIds = [];
        $validDepartIds = [];
        if (!empty($doctorIds)) {
            $doctorInfos = NewDoctorOpenidUserinfoRepository::getInfoByIds($doctorIds);
            $validDoctorIds = $doctorInfos->pluck('id')->unique()->toArray();
            $validDepartIds = $doctorInfos->pluck('depart_id')->unique()->toArray();
        }

        // 兼容：如果有参数按参数
        if (empty($departIds)) {
            $departIds = $validDepartIds;
        }

        /** @var AgentVisit $visit */
        list($visit, $retLng, $retLat, $checkResult) = self::preCheckOut(
            $id,
            $hospitalId,
            $visitType,
            $lng,
            $lat,
            $geoOpt,
            $departIds,
            false,
            $coordinateType,
            $isFakeGeo
        );

        if ($checkOutTime && $visit->isSupplement() === false) {
            $checkOutTime = "";
        }
        if ($visit->is_supplement) {
            if (!$checkOutTime) {
                $checkOutTime = date('Y-m-d H:i:s');
            }
            if ($visit->check_in_time >= $checkOutTime) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '签出时间不能早于签入时间');
            }
        }

        if ($visit->created_time < date('Y-m-d 00:00:00')) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "签入已过期，仅允许签出当天的签到");
        }

        $logger  = Helper::getLogger();

        Db::beginTransaction();
        try {
            // 添加地理位置
            $geo = AgentVisitGeoRepository::addGeoPos($lng, $lat, $retLng, $retLat, $position, $coordinateType, $isFakeGeo);

            //编辑签出记录
            $operateResult = AgentVisitRepository::checkOut(
                $visit,
                $geo,
                $result,
                $next,
                $geoOpt,
                $geoComment,
                $checkResult,
                $isCollaborative,
                $checkOutTime
            );
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_FAIL);
            }

            //添加部门记录
            if (!empty($departIds)) {
                $operateResult = AgentVisitDepartRepository::addVisitDeparts($visit->id, $departIds);
                if (!$operateResult) {
                    throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_DEPART_FAIL);
                }
            }

            //添加药品记录
            $operateResult = AgentVisitDrugSeriesRepository::addVisitDrugs($visit->id, $seriesIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_DRUG_FAIL);
            }

            //添加照片记录
            $operateResult = AgentVisitPicRepository::batchAddVisitUploads(
                $visit,
                $picIds,
                AgentVisitPic::PIC_TYPE_CHECK_OUT
            );
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_PIC_FAIL);
            }

            //添加医生
            if (!empty($validDoctorIds)) {
                $operateResult = AgentVisitDoctorRepository::batchAddVisitDoctors($visit, $validDoctorIds);
                if (!$operateResult) {
                    throw new BusinessException(ErrorCode::BUSINESS_ERROR, "添加代表拜访医生失败");
                }
            }

            Db::commit();

            //发放积分
            $visit->refresh();
            self::sendVisitPoints($visit);
            //添加日志
            self::addVisitLog(
                $visit->user_id,
                $visit->id,
                $retLng,
                $retLat,
                "签出"
            );
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }

    private static function sendVisitPoints(AgentVisit $visit)
    {
        //TODO: 这边后期可以加入判断逻辑和规则, 根据特例来发放
        $user  = Helper::getLoginUser();
        //使用通用规则发放积分
        try {
            $groupResp = PointRpcService::getDefaultGroup();
            PointRpcService::activity(
                'agent_visit',
                5,
                $user->uid,
                0,
                0,
                UserType::USER_TYPE_AGENT,
                $groupResp->getInfo()->getId()
            );
        } catch (\Exception $e) {
            Helper::getLogger()
                ->info("visit_point_error", [
                    'visit_id' => $visit->id,
                    'user' => $user->uid,
                    'msg' => $e->getMessage()
                ]);
        }
    }

    /**
     * 检查地理位置.
     *
     * @param $hospitalId
     * @param $lat
     * @param $lng
     * @return bool
     */
    public static function checkGeo($hospitalId, $lat, $lng, $coordinateType = DataStatus::COORDINATE_GPS): bool
    {
        //非高德系需要转换成高德系
        if ($coordinateType != DataStatus::COORDINATE_GCJ02) {
            [$retLng, $retLat] = GeoService::transferCoordinate($lng, $lat, $coordinateType);
        } else {
            $retLng = $lng;
            $retLat = $lat;
        }

        return self::checkGeoPoints($hospitalId, $retLat, $retLng);
    }

    /**
     * 检查地理位置数据.
     *
     * @param $hospitalId
     * @param $lat
     * @param $lng
     * @return bool
     */
    private static function checkGeoPoints($hospitalId, $lat, $lng): bool
    {
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "医院不存在");
        }
        $user = Helper::getLoginUser();
        if (VisitCacheService::isEmpty($hospitalId, $hospital->company_yyid)) { //没有数据，设置一个key防止捅到数据库里面
            return true;
        }

        if (!VisitCacheService::isCached($hospitalId, $hospital->company_yyid)) {
            self::loadHospitalGeoRedis($hospitalId);
            //载入数据依然为空说明数据库也没有数据
            if (VisitCacheService::isEmpty($hospitalId, $hospital->company_yyid)) {
                return true;
            }
        }

        $logger =  Helper::getLogger();
        // 从缓存中获取到当前输入点最大范围内的所有点
        $filterPoints = VisitCacheService::getHospitalMaxRadiusPoints($hospitalId, $lng, $lat, $hospital->company_yyid);
        $logger->info('filter_points', $filterPoints);
        if (empty($filterPoints)) {
            return false;
        }

        // 获取数据库存储的对应点位允许最大距离，有满足的就返回true, 否则返回false
        $pointIds = array_column($filterPoints, 'id');
        $gps = DealerHospitalGpsRepository::getGpsByIds($pointIds);
        $gpsMap = $gps->keyBy('id')->all();
        $logger->info('gps_map', $gpsMap);
        foreach ($filterPoints as $item) {
            $gpsId = $item['id'];
            $dist = $item['dist'];
            //比较距离，符合则返回true,其它返回false
            $logger->info('_gps_foreach', [
                'exists' => $gpsMap[$gpsId] ?? 0,
                'range' => isset($gpsMap[$gpsId]) ?  $gpsMap[$gpsId]->range : 0
            ]);
            if (isset($gpsMap[$gpsId]) && $gpsMap[$gpsId]->range > $dist) {
                return true;
            }
        }
        return false;
    }

    /**
     * 从数据库获取医院gps数据.
     *
     * @param YouyaoHospital $hospital
     * @return Collection
     */
    private static function getUserHospitalGpsData(YouyaoHospital $hospital): Collection
    {
        if (!$hospital->company_yyid) {
            return new Collection();
        }

        //根据文件获取最新上传gps坐标
        return DealerHospitalGpsRepository::getUserHospitalGps($hospital);
    }

    /**
     * 加载医院坐标进缓存.
     *
     * @param $hospitalId
     */
    private static function loadHospitalGeoRedis($hospitalId)
    {
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }
        $gpsData = self::getUserHospitalGpsData($hospital);

        // 查询数据库获取hospital对应地理生成 id/lng/lat形式list map
        $geoList = [];
        //过滤出最远距离
        $maxDist = 0;
        /** @var DealerHospitalGps $item */
        foreach ($gpsData as $item) {
            $geoList[] = [
                'id' => $item->id,
                'lng' => $item->longitude,
                'lat' => $item->latitude
            ];
            if ($item->range > $maxDist) {
                $maxDist = $item->range;
            }
        }

        // 不管有没有数据都进行缓存更新
        VisitCacheService::refreshCache($hospitalId, $geoList, $maxDist, $hospital->company_yyid);
    }

    /**
     * 签入.
     *
     * @param $visitType
     * @param $hospitalId
     * @param $pics
     * @param $lat
     * @param $lng
     * @param $position
     * @param int $geoOpt
     * @param string $geoComment
     * @return AgentVisit
     * @throws \Exception
     */
    public static function checkIn(
        $visitType,
        $hospitalId,
        $pics,
        $lat,
        $lng,
        $position,
        $geoOpt = 0,
        $geoComment = "",
        $coordinateType = DataStatus::COORDINATE_GPS,
        $specialId = 0,
        $specialComment = "",
        $checkInTime = "",
        $isFakeGeo = false
    ): AgentVisit {
        $user = Helper::getLoginUser();

        if ($checkInTime) {
            //校验当前用户是否允许补签
            if (!self::checkUserValidCheckInSupplement($user)) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '非法签入，不允许补签');
            }
        }

        // 转换坐标为高德系
        if ($coordinateType != DataStatus::COORDINATE_GCJ02) {
            [$retLng, $retLat] = GeoService::transferCoordinate($lng, $lat, $coordinateType);
        } else {
            [$retLng, $retLat] = [$lng, $lat];
        }

        $special = null;
        if ($specialId > 0) {
            $special = AgentVisitConfigRepository::getById($specialId);
            if (!$special) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "异常选项不存在");
            }
        }
        // 检查坐标是否超出范围
        $checkResult = true;
        if ($isFakeGeo) {
            if (!self::checkUserValidCheckInSupplement($user)) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '非法签入，不允许补签');
            }
            //获取医院是否存在gps点位
            if ($hospitalId &&  (!YouyaoHospitalLngLatRepository::checkHospitalPointExists($hospitalId))) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '当前医院未配置定位，无法使用自动生成gps');
            }
        } else {
            if (is_null($special) && $hospitalId && $visitType == AgentVisit::VISIT_TYPE_TO_FACE) {
                $checkResult = self::checkGeoPoints($hospitalId, $retLat, $retLng);
                if ((!$checkResult) && (!$geoOpt)) {
                    throw new BusinessException(ErrorCode::AGENT_VISIT_OVER_GEO_EMPTY_COMMENT);
                }
            }
        }

        // 过滤有效图片
        $uploads = UploadQiniuRepository::getByIds($pics);
        if ($uploads->isEmpty()) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_AT_LEAST_ONE_PIC);
        }
        $picIds = $uploads->pluck('id')->toArray();

        Db::beginTransaction();
        try {
            // 添加地理位置
            $geo  = AgentVisitGeoRepository::addGeoPos($lng, $lat, $retLng, $retLat, $position, $coordinateType, $isFakeGeo);

            // 添加拜访记录
            $visit = AgentVisitRepository::addCheckIn(
                $user,
                $visitType,
                $hospitalId,
                $geo,
                $geoOpt,
                $geoComment,
                $checkResult,
                $specialId,
                $specialComment,
                $checkInTime
            );

            // 添加关联图片
            $operateResult = AgentVisitPicRepository::batchAddVisitUploads($visit, $picIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_ADD_PICS_FAIL);
            }
            Db::commit();
            self::addCheckInNotice($visit);
            //添加日志
            self::addVisitLog(
                $user->uid,
                $visit->id,
                $retLng,
                $retLat,
                "签入"
            );
            return $visit;
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }

    private static function addVisitLog($userId, $visitId, $longitude, $latitude, $desc)
    {
        try {
            $request= ApplicationContext::getContainer()
                ->get(RequestInterface::class);
            $role = (string)Helper::getAuthRole();
            $device = $role == 0 ? '公众号' : ($role == 1 ? 'app': $role);
            $ip = Helper::getIp($request);
            $ua = Helper::getUserAgent($request);
            go(function () use ($visitId, $userId, $desc, $ua, $ip, $longitude, $latitude, $device) {
                try {
                    AgentVisitLogRepository::addLog(
                        $visitId,
                        $userId,
                        $ua,
                        $ip,
                        $desc,
                        $longitude,
                        $latitude,
                        $device
                    );
                } catch (\Exception $e) {
                    Helper::getLogger()->error("save_visit_log_error", [
                        'msg' => $e->getMessage(),
                        'visit_id'=>$visitId
                    ]);
                }
            });
        } catch (\Exception $e) {
            Helper::getLogger()->error("save_visit_log_error", [
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]);
        }
    }

    public static function addCheckInNotice(AgentVisit $visit)
    {
        //留个配置测试起来方便
        $visitNoticeInterval = env('VISIT_NOTICE_INTERVAL', '30,60');
        $interval = explode(',', $visitNoticeInterval);
        foreach ($interval as $int) {
            self::sendMsg($visit->id, (int)$int);
        }
    }

    private static function sendMsg($id, $interval)
    {
        $topic = "visit-notice-delay";

        //站内信
        $pm = new VisitWechatNotice();
        $pm->setInterval($interval);
        $pm->setVisitId($id);
        $pm->setNoticeType(VisitNoticeType::NOTICE_TYPE_INSIDE);
        TaskManageHelper::create(0, $pm, $topic)->push($interval * 60);

        //微信模板消息
        $wm = new VisitWechatNotice();
        $wm->setInterval($interval);
        $wm->setVisitId($id);
        $wm->setNoticeType(VisitNoticeType::NOTICE_TYPE_WECHAT);
        TaskManageHelper::create(0, $wm, $topic)->push($interval * 60);
    }

    /**
     * 签入编辑.
     *
     * @param $id
     * @param $visitType
     * @param $hospitalId
     * @param $picIds
     * @param int $lng
     * @param int $lat
     * @param string $position
     * @param int $geoOpt
     * @param string $geoComment
     * @throws \Exception
     */
    public static function editCheckIn(
        $id,
        $visitType,
        $hospitalId,
        $picIds,
        $lng = 0,
        $lat = 0,
        $position = "",
        $geoOpt = 0,
        $geoComment = "",
        $coordinateType = DataStatus::COORDINATE_GPS,
        $isFakeGeo = false
    ) {
        $visit = AgentVisitRepository::getVisitById($id);
        if (!$visit) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_NOT_EXISTS);
        }
        if (!$visit->isCheckIn()) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_STATUS_DENY_MODIFY);
        }

        // 转换到高德系坐标并校验范围
        $checkResult = true;
        if ($coordinateType != DataStatus::COORDINATE_GCJ02) {
            [$retLng, $retLat] = GeoService::transferCoordinate($lng, $lat, $coordinateType);
        } else {
            [$retLng, $retLat] = [$lng, $lat];
        }

        // 检查坐标是否超出范围
        if ($isFakeGeo) {
            $user = Helper::getLoginUser();
            if (!self::checkUserValidCheckInSupplement($user)) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '非法签入，不允许补签');
            }
            if ($hospitalId && (!YouyaoHospitalLngLatRepository::checkHospitalPointExists($hospitalId))) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '当前医院未配置定位，无法使用自动生成gps');
            }
        } else {
            if ((!$visit->visit_special_id) && $hospitalId && $visitType == AgentVisit::VISIT_TYPE_TO_FACE) {
                $checkResult = self::checkGeoPoints($hospitalId, $retLat, $retLng);
                if ((!$checkResult) && (!$geoOpt)) {
                    throw new BusinessException(ErrorCode::AGENT_VISIT_OVER_GEO_EMPTY_COMMENT);
                }
            }
        }


        //获取当前拜访已经关联的图片进行对比
        $existsPics = AgentVisitPicRepository::getVisitUpload($visit, AgentVisitPic::PIC_TYPE_CHECK_IN);
        $existsPicIds = $existsPics->pluck('upload_id')->unique()->toArray();
        [$deletePicIds, $addPicIds] = Helper::diff($existsPicIds, $picIds);

        Db::beginTransaction();
        try {
            // 添加/编辑地理位置
            $geo = self::addOrUpdateGeo($visit, $lng, $lat, $retLng, $retLat, $position, true, $coordinateType, $isFakeGeo);

            // 编辑签入
            $operateResult = AgentVisitRepository::editCheckIn(
                $visit,
                $visitType,
                $hospitalId,
                $geo,
                $checkResult,
                $geoOpt,
                $geoComment
            );
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_FAIL);
            }

            // 添加/编辑图片
            self::editPics($visit, $addPicIds, $deletePicIds);

            Db::commit();
            //添加日志
            self::addVisitLog(
                $visit->user_id,
                $visit->id,
                $retLng,
                $retLat,
                '编辑签入'
            );
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }

    /**
     * 编辑签出.
     *
     * @param $id
     * @param $hospitalId
     * @param $visitType
     * @param array $departIds
     * @param array $seriesIds
     * @param string $result
     * @param string $next
     * @param int $lat
     * @param int $lng
     * @param string $position
     * @param array $picIds
     * @param int $geoOpt
     * @param string $geoComment
     * @throws \Exception
     */
    public static function editCheckOut(
        $id,
        $hospitalId,
        $visitType,
        array $departIds = [],
        array $seriesIds = [],
        $result = "",
        $next = "",
        $lat = 0,
        $lng = 0,
        $position = "",
        $picIds = [],
        $geoOpt = 0,
        $geoComment = "",
        array $doctorIds = [],
        $coordinateType = DataStatus::COORDINATE_GPS,
        $isCollaborative = 0,
        $isFakeGeo = false
    ) {
        // 修改：$departIds不再从参数获取，而是根据医生所在的科室获取，同时需兼容从参数获取departIds的版本
        $validDepartIds = [];
        if (!empty($doctorIds)) {
            $doctorInfos = NewDoctorOpenidUserinfoRepository::getInfoByIds($doctorIds);
            $validDepartIds = $doctorInfos->pluck('depart_id')->unique()->toArray();
        }

        // 兼容：如果有参数按参数
        if (empty($departIds)) {
            $departIds = $validDepartIds;
        }

        //检查数据
        /** @var AgentVisit $visit */
        list($visit, $retLng, $retLat, $checkResult) = self::preCheckOut(
            $id,
            $hospitalId,
            $visitType,
            $lng,
            $lat,
            $geoOpt,
            $departIds,
            true,
            $coordinateType,
            $isFakeGeo
        );

        //对比部门
        $visitDepartIds = $visit->visitDeparts->pluck('depart_id')->unique()->toArray();
        [$deleteDepartIds, $addDepartIds] = Helper::diff($visitDepartIds, $departIds);

        //对比图片
        $pics = AgentVisitPicRepository::getVisitUpload($visit, AgentVisitPic::PIC_TYPE_CHECK_OUT);
        $existPicIds = $pics->pluck('upload_id')->unique()->toArray();
        [$deletePicIds, $addPicIds] = Helper::diff($existPicIds, $picIds);

        // 对比产品
        $visitDrugs = AgentVisitDrugSeriesRepository::getVisitDrugSeries($visit);
        $drugIds = $visitDrugs->pluck('series_id')->toArray();
        [$deleteDrugIds, $addDrugIds] = Helper::diff($drugIds, $seriesIds);

        // 对比医生
        [$deleteDoctorIds, $addDoctorIds] = self::diffVisitDoctors($visit, $doctorIds);

        Db::beginTransaction();
        try {
            // 添加/编辑地理位置
            $geo = self::addOrUpdateGeo($visit, $lng, $lat, $retLng, $retLat, $position, false, $coordinateType, $isFakeGeo);

            // 编辑签出
            $operateResult = AgentVisitRepository::editCheckOut(
                $visit,
                $geo,
                $result,
                $next,
                $geoOpt,
                $geoComment,
                $checkResult,
                $isCollaborative
            );
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_FAIL);
            }

            // 编辑图片
            self::editPics($visit, $addPicIds, $deletePicIds, AgentVisitPic::PIC_TYPE_CHECK_OUT);

            // 编辑部门
            self::editDeparts($visit, $addDepartIds, $deleteDepartIds);

            // 编辑产品
            self::editVisitDrugs($visit, $addDrugIds, $deleteDrugIds);

            // 编辑医生
            self::editDoctor($visit, $addDoctorIds, $deleteDoctorIds);

            Db::commit();
            self::addVisitLog(
                $visit->user_id,
                $visit->id,
                $retLng,
                $retLat,
                '编辑签出'
            );
        } catch (\Exception $e) {
            Db::rollBack();
            throw $e;
        }
    }

    private static function editDoctor($visit, array $addDoctorIds = [], array $delDoctorIds = [])
    {
        if (!empty($addDoctorIds)) {
            $operateResult = AgentVisitDoctorRepository::batchAddVisitDoctors($visit, $addDoctorIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "更新拜访医生关系失败");
            }
        }

        if (!empty($delDoctorIds)) {
            $operateResult = AgentVisitDoctorRepository::batchRemoteVisitDoctors($visit, $delDoctorIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "更新拜访医生关系失败");
            }
        }
    }

    /**
     * @param AgentVisit $visit
     * @param array $doctorIds
     * @return array
     */
    private static function diffVisitDoctors(AgentVisit $visit, array $doctorIds = []): array
    {
        $validDoctorIds = [];
        if (!empty($doctorIds)) {
            $doctorInfos = NewDoctorOpenidUserinfoRepository::getInfoByIds($doctorIds);
            $validDoctorIds = $doctorInfos->pluck('id')->unique()->toArray();
        }

        $exists = AgentVisitDoctorRepository::getVisitDoctors($visit);
        $existsIds = $exists->pluck('doctor_id')->unique()->toArray();

        return Helper::diff($existsIds, $validDoctorIds);
    }

    private static function editDeparts(AgentVisit $visit, array $addDepartIds = [], array $delDepartIds = [])
    {
        if (!empty($addDepartIds)) {
            $operateResult = AgentVisitDepartRepository::addVisitDeparts($visit->id, $addDepartIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_DEPART_FAIL);
            }
        }

        if (!empty($delDepartIds)) {
            $operateResult = AgentVisitDepartRepository::batchDeleteDeparts($visit, $delDepartIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_DEPART_FAIL);
            }
        }
    }

    /**
     * （编辑）签出前检查数据
     * @param $id
     * @param $hospitalId
     * @param $visitType
     * @param float $lng
     * @param float $lat
     * @param int $geoOpt
     * @param array $departIds
     * @param bool $isEdit
     * @return array
     */
    private static function preCheckOut(
        $id,
        $hospitalId,
        $visitType,
        $lng = 0,
        $lat = 0,
        $geoOpt = 0,
        array $departIds = [],
        $isEdit = false,
        $coordinateType = DataStatus::COORDINATE_GPS,
        $isFakeGeo = false
    ): array {
        $visit = AgentVisitRepository::getVisitById($id);
        if (!$visit) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_NOT_EXISTS);
        }
        // 编辑 - 已签出
        // 签出 - 已签入
        if (!(($isEdit && $visit->isCheckOut()) || ((!$isEdit) && $visit->isCheckIn()))) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_STATUS_DENY_MODIFY);
        }
        $user = Helper::getLoginUser();
        if ($user->uid != $visit->user_id) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_ONLY_EDIT_SELF);
        }
        if ($visit->hospital_id != $hospitalId) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_CHECK_OUT_DENY_HOSPITAL_MODIFY);
        }
        if ($visit->visit_type != $visitType) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_CHECK_OUT_DENY_TYPE_MODIFY);
        }

        // 转换到高德系坐坐标系并校验位置是否在配置范围内
        if ($coordinateType != DataStatus::COORDINATE_GCJ02) {
            [$retLng, $retLat] = GeoService::transferCoordinate($lng, $lat, $coordinateType);
        } else {
            [$retLng, $retLat] = [$lng, $lat];
        }
        $checkResult = true;
        if ($isFakeGeo) {
            if (!self::checkUserValidCheckInSupplement($user)) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '非法签入，不允许补签');
            }
            if ($visit->hospital_id && (!YouyaoHospitalLngLatRepository::checkHospitalPointExists($hospitalId))) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '当前医院未配置定位，无法使用自动生成gps');
            }
        } else {
            if ($visit->hospital_id && $visitType == AgentVisit::VISIT_TYPE_TO_FACE) {
                $checkResult = self::checkGeoPoints($visit->hospital_id, $retLat, $retLng);
            }
        }

        if ((!$checkResult) && (!$geoOpt)) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_OVER_GEO_EMPTY_COMMENT);
        }

        // 校验医院类型机构至少选择一个部门
        if (empty($departIds)) {
            $hospital = $visit->hospital;
            if ($hospital) {
                $type = self::getUserCompanyDealerHospital($user, $hospital);
                if ($type == YouyaoHospital::ORG_TYPE_HOSPITAL) {
                    throw new BusinessException(ErrorCode::AGENT_VISIT_HOSPITAL_AT_LEAST_ONE_DEPART);
                }
            }
        }

        return array($visit, $retLng, $retLat, $checkResult);
    }

    /**
     * 添加或者编辑地理位置.
     *
     * @param AgentVisit $visit
     * @param float $lng
     * @param float $lat
     * @param $retLng
     * @param $retLat
     * @param string $position
     * @param bool $isCheckIn
     * @return AgentVisitGeo
     */
    private static function addOrUpdateGeo(
        AgentVisit $visit,
        $lng,
        $lat,
        $retLng,
        $retLat,
        string $position,
        $isCheckIn = true,
        $coordinateType = DataStatus::COORDINATE_GPS,
        $isFakeGeo = false
    ): AgentVisitGeo {
        if ($isCheckIn) {
            $geo = $visit->checkInGeo;
        } else {
            $geo = $visit->checkOutGeo;
        }
        if ($geo) {
            $operateResult = AgentVisitGeoRepository::editGeoPos(
                $geo,
                $lng,
                $lat,
                $retLng,
                $retLat,
                $position,
                $coordinateType,
                $isFakeGeo
            );
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_GEO_EDIT_FAIL);
            }
        } else {
            $geo = AgentVisitGeoRepository::addGeoPos($lng, $lat, $retLng, $retLat, $position, $coordinateType, $isFakeGeo);
        }
        return $geo;
    }

    /**
     * 编辑拜访图片.
     *
     * @param AgentVisit $visit
     * @param array $addPicIds
     * @param array $deletePicIds
     * @param int $picType
     */
    private static function editPics(
        AgentVisit $visit,
        array $addPicIds = [],
        array $deletePicIds = [],
        $picType = AgentVisitPic::PIC_TYPE_CHECK_IN
    ): void {
        if (!empty($addPicIds)) {
            $operateResult = AgentVisitPicRepository::batchAddVisitUploads($visit, $addPicIds, $picType);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_PIC_FAIL);
            }
        }
        if (!empty($deletePicIds)) {
            $operateResult = AgentVisitPicRepository::batchDeleteVisitUpload($visit, $deletePicIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_PIC_FAIL);
            }
        }
    }

    /**
     * 编辑拜访产品.
     *
     * @param AgentVisit $visit
     * @param array $addDrugIds
     * @param array $deleteDrugIds
     */
    private static function editVisitDrugs(AgentVisit $visit, array $addDrugIds = [], array $deleteDrugIds = [])
    {
        if (!empty($addDrugIds)) {
            $operateResult = AgentVisitDrugSeriesRepository::batchAddVisitSeries($visit, $addDrugIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_DRUG_FAIL);
            }
        }

        if (!empty($deleteDrugIds)) {
            $operateResult = AgentVisitDrugSeriesRepository::batchDeleteVisitDrugs($visit, $deleteDrugIds);
            if (!$operateResult) {
                throw new BusinessException(ErrorCode::AGENT_VISIT_EDIT_DRUG_FAIL);
            }
        }
    }

    /**
     * @param int $hospitalId
     * @param $departId
     * @param string $keywords
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public static function getGroupDoctors(int $hospitalId, $departId, $keywords = "", $page = 1, $pageSize = 10): array
    {
        $user = Helper::getLoginUser();
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }
        $doctorInfos = NewDoctorOpenidUserinfoRepository::getAgentDoctors($user, $hospital->yyid, $departId, $keywords, $page, $pageSize);
        $count = NewDoctorOpenidUserinfoRepository::getAgentDoctorsCount($user, $hospital->yyid, $departId, $keywords);

        $resp = [];
        $hospitalName = $hospital->hospital_name;
        $defaultHeadImgUrl = "http://img.youyao99.com/7f13a9f1c658124512777af5bccc08c4.png";
        /** @var NewDoctorUserinfo $doctorInfo */
        foreach ($doctorInfos as $doctorInfo) {
            $resp[] = [
                'id' => $doctorInfo->id,
                'name' => $doctorInfo->true_name ?: ($doctorInfo->nickname ?: "-"),
                'head_img_url' => $doctorInfo->headimgurl ?: $defaultHeadImgUrl,
                'hospital_name' => $hospitalName,
                'depart_name' => $doctorInfo->depart_name
            ];
        }
        return [
            'list' => $resp,
            'total' => $count,
            'page' => $page,
            'page_size' => $pageSize
        ];
    }

    public static function addGroupDoctors(
        $check,
        $name,
        $hospitalId,
        $departId,
        $mobile = "",
        $sex = 1,
        $position = "",
        $jobTitle = "",
        $fieldId = 0,
        $clinicRota = ""
    ): NewDoctorOpenidUserinfo {
        $user = Helper::getLoginUser();
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }
        $depart = SystemDepartRepository::getById($departId);
        if (!$depart) {
            throw new BusinessException(ErrCode::ParamsErr, "部门不存在");
        }
        // 判断手机号是否被占用
        $infoByMobile = DoctorOpenidUserinfoRepository::getDoctorByMobile($mobile);
        if ($infoByMobile->isNotEmpty()) {
            throw new GroupException(ErrorCodeGroup::DOCTOR_PHONE_ALREADY_USED);
        }

        // 判断同一医院同一科室同一真实姓名是否在群组中存在
        if ($check) {
            $infoByMobile = DoctorOpenidUserinfoRepository::getDoctorByHospital($hospital->yyid, $departId, $name);
            if ($infoByMobile->isNotEmpty()) {
                throw new GroupException(ErrorCodeGroup::REPEAT_DOCTOR);
            }
        }

        //doctor表增加一条记录
        $doctor = new NewDoctorUserinfo();
        $doctor->openid = "";
        $doctor->unionid = "";
        $doctor->wechat_type = DataStatus::WECHAT_DOCTOR;
        $doctor->nickname = "";
        $doctor->headimgurl = "";
        $doctor->true_name = $name;
        $doctor->gender = $sex;
        $doctor->mobile_num = $mobile;
        $doctor->hospital_yyid = $hospital->yyid;
        $doctor->hospital_name = $hospital->hospital_name;
        $doctor->depart_id = $depart->id;
        $doctor->depart_name = $depart->name;
        $doctor->job_title = $jobTitle;
        $doctor->position = $position;
        $doctor->field_id = $fieldId;
        $doctor->clinic_rota = $clinicRota;
        $doctor->created_at = $doctor->updated_at = date('Y-m-d H:i:s');
        $doctor->save();
        $doctor->refresh();

        $reps = new NewDoctorOpenidUserinfo();
        $reps->user_id = $user->uid;
        $reps->new_doctor_id = $doctor->nid;
        $reps->status = DataStatus::REGULAR;
        $reps->created_at = $doctor->updated_at = date('Y-m-d H:i:s');
        $reps->save();
        $reps->refresh();

        return $reps;
    }

    public static function getGeoInfo($id): GetVisitDistinctResponse
    {
        $visit = AgentVisitRepository::getVisitById($id);
        if (!$visit) {
            throw new BusinessException(ErrorCode::AGENT_VISIT_NOT_EXISTS);
        }

        $resp = new GetVisitDistinctResponse();
        $checkInGps = $checkOutGps = [];
        if ($visit->check_in_geo > 0) {
            $checkInGeo = $visit->checkInGeo;
            if ($checkInGeo) {
                $checkInGps = [
                    'lng' => $checkInGeo->amap_longitude,
                    'lat' => $checkInGeo->amap_latitude
                ];
                $checkIn = new VisitGeo();
                $checkIn->setLng((string)$checkInGeo->amap_longitude);
                $checkIn->setLat((string)$checkInGeo->amap_latitude);
                $resp->setCheckIn($checkIn);
            }
        }
        if ($visit->check_out_geo > 0) {
            $checkOutGeo = $visit->checkOutGeo;
            if ($checkOutGeo) {
                $checkOutGps = [
                    'lng' => $checkOutGeo->amap_longitude,
                    'lat' => $checkOutGeo->amap_latitude
                ];
                $checkIn = new VisitGeo();
                $checkIn->setLng((string)$checkOutGeo->amap_longitude);
                $checkIn->setLat((string)$checkOutGeo->amap_latitude);
                $resp->setCheckOut($checkIn);
            }
        }

        //获取医院gps
        $hospital = $visit->hospital;
        $user = UsersRepository::getUserByUid($visit->user_id);

        $gpsData = self::getUserHospitalGpsData($hospital);
        if (empty($gpsData)) {
            return $resp;
        }
        $hospitalGps = [];
        /** @var DealerHospitalGps $item */
        foreach ($gpsData as $item) {
            $hospitalGps[] = [
                'lng' => $item->longitude,
                'lat' => $item->latitude,
                'max' => $item->range
            ];
        }
        //计算医院gps同签入签出距离
        $gpsWithDistinct = VisitCacheService::calcGpsDistinct($hospitalGps, $checkInGps, $checkOutGps);
        $hosGps = [];
        foreach ($gpsWithDistinct as $item) {
            $i = new HospitalDistinct();
            $i->setLng((string)$item['lng']);
            $i->setLat((string)$item['lat']);
            $i->setRange((string)$item['max']);
            $i->setCheckInDistinct(
                strval(isset($item['check_in_distinct']) &&  $item['check_in_distinct']? $item['check_in_distinct']: 0)
            );
            $i->setCheckOutDistinct(
                strval(
                    isset($item['check_out_distinct']) &&
                    $item['check_out_distinct'] ?
                        $item['check_out_distinct']: 0
                )
            );
            $hosGps[] =  $i;
        }
        $resp->setHospitalDistinct($hosGps);

        return $resp;
    }

    public static function checkUserValidCheckInSupplement(Users $user): bool
    {
        //检查公司是否支持补签
        $company = $user->company;
        $isOk = self::checkCompanySupplementOk($company);
        if ($isOk) {
            return true;
        }

        //检查个人是否支持补签
        return self::checkUserSupplementOk($user);
    }

    private static function checkCompanySupplementOk(Company $company): bool
    {
        $status = VisitCacheService::checkCompanySupplement($company->id);
        if ($status != VisitSupplement::STATUS_COMPANY_NONE) {
            return $status == VisitSupplement::STATUS_COMPANY_YES;
        }

        $isOk = VisitSupplementCompanyRepository::checkCompanyIsOk($company->id);
        VisitCacheService::addCompanySupplement($company->id, $isOk);
        return $isOk;
    }

    private static function checkUserSupplementOk(Users $users): bool
    {
        $status = VisitCacheService::checkUserSupplement($users->uid);
        if ($status != VisitSupplement::STATUS_USER_NONE) {
            return $status == VisitSupplement::STATUS_USER_YES;
        }

        $isOk = VisitSupplementUserRepository::checkUserIsOk($users->uid);
        VisitCacheService::addUserSupplement($users->uid, $isOk);
        return $isOk;
    }

    public static function getSupplementCompanyList($keywords = "", $current = 1, $limit = 10): CompanyListReply
    {
        $reply = VisitSupplementCompanyRepository::getCompanyWithPage($keywords, $current, $limit);
        $page = new CommonPageResponse();
        $page->setTotal($reply->total());
        $page->setSize($limit);
        $page->setCurrent($reply->currentPage());
        $page->setPages($reply->lastPage());

        $response = new CompanyListReply();
        $response->setPage($page);
        if ($reply->isEmpty()) {
            return $response;
        }

        $data = [];
        foreach ($reply->items() as $item) {
            $o = new CompanySupplementItem();
            $o->setId($item->id);
            $o->setName($item->name);
            $o->setCompanyId($item->company_id);
            $data[] = $o;
        }

        $response->setCompany($data);
        return $response;
    }

    public static function getSupplementUserList($keywords = "", $current = 1, $limit = 10): UserListReply
    {
        $resp = VisitSupplementUserRepository::listSupplementUserWithPage($keywords, $current, $limit);
        $page = new CommonPageResponse();
        $page->setTotal($resp->total());
        $page->setSize($limit);
        $page->setCurrent($resp->currentPage());
        $page->setPages($resp->lastPage());

        $reply = new UserListReply();
        $reply->setPage($page);
        if ($resp->isEmpty()) {
            return $reply;
        }

        $data = [];
        foreach ($resp->items() as $item) {
            $o = new UserSupplementItem();
            $o->setId($item->id);
            $o->setName($item->name);
            $o->setUserId($item->uid);
            $o->setMobile($item->mobile_num);
            $o->setTrueName($item->true_name);
            $data[] = $o;
        }
        $reply->setUsers($data);
        return $reply;
    }

    public static function addSupplementCompany($companyId)
    {
        $company = CompanyRepository::getById($companyId);
        if (!$company) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '公司不存在');
        }
        if (!VisitSupplementCompanyRepository::checkCompanyIsOk($company->id)) {
            VisitSupplementCompanyRepository::addCompanySupplement($company->id);
            VisitCacheService::clearCompanySupplement($companyId);  //清除记录让后面的请求自己去建立缓存记录
        }
    }

    public static function delSupplementCompany($companySupplementId)
    {
        $sup = VisitSupplementCompanyRepository::getById($companySupplementId);
        if (!$sup) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '公司不存在');
        }

        VisitSupplementCompanyRepository::delCompanySupplement($sup->id);
        VisitCacheService::clearCompanySupplement($sup->company_id);
    }

    public static function delSupplementUser($userSupplementId)
    {
        $sup = VisitSupplementUserRepository::getById($userSupplementId);
        if (!$sup) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '人员不存在');
        }
        VisitSupplementUserRepository::delById($sup->id);
        VisitCacheService::clearUserSupplement($sup->user_id);
    }

    public static function getDoctorDeparts($userId, $hospitalId)
    {
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::HOSPITAL_NOT_EXISTS);
        }

        $departs = NewDoctorOpenidUserinfoRepository::getDoctorDeparts($userId, $hospital->yyid);
        return $departs->isNotEmpty() ? $departs->toArray() : [];
    }

    public static function selectList($userId)
    {
        $doctors = [];
        $redis = new DoctorSelectRedisLogic($userId);
        $doctorIds = $redis->getDoctors();

        $defaultHeadImgUrl = "http://img.youyao99.com/7f13a9f1c658124512777af5bccc08c4.png";

        foreach ($doctorIds as $key=>$doctorId) {
            /** @var NewDoctorUserinfo $doctorData */
            $doctorData = NewDoctorOpenidUserinfoRepository::getDoctorsById($doctorId);
            if ($doctorData) {
                $doctors[$key]['id'] = $doctorData->id;
                $doctors[$key]['name'] = $doctorData->true_name ?: ($doctorData->nickname ?: "-");
                $doctors[$key]['head_img_url'] = $doctorData->headimgurl ?: $defaultHeadImgUrl;
                $doctors[$key]['hospital_name'] = $doctorData->hospital_name;
                $doctors[$key]['depart_name'] = $doctorData->depart_name;
            }
        }
        return $doctors;
    }

    public static function addSelectList($doctorIds, $userId)
    {
        $redis = new DoctorSelectRedisLogic($userId);
        foreach ($doctorIds as $doctorId) {
            if (!$redis->getOneKey($doctorId)) {
                $redis->storeKey($doctorId);
            }
        }
    }

    public static function deleteSelectList($doctorIds, $userId)
    {
        $redis = new DoctorSelectRedisLogic($userId);
        foreach ($doctorIds as $doctorId) {
            $redis->delOneKey($doctorId);
        }
    }

    public static function clearSelectList($userId)
    {
        $redis = new DoctorSelectRedisLogic($userId);
        $redis->delAllKey();
    }

    public static function doctorCount($userId)
    {
        $redis = new DoctorSelectRedisLogic($userId);
        return $redis->getCount();
    }

    public static function addSupplementUser($userId)
    {
        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '代表不存在');
        }

        if (VisitSupplementUserRepository::checkUserIsOk($userId)) {
            return;
        }

        VisitSupplementUserRepository::addUser($user->uid);

        VisitCacheService::clearUserSupplement($user->uid);
    }

    public static function searchUserForAddSupplement($keywords = "", $limit = 5): SearchUserToAddSupplementReply
    {
        $resp = VisitSupplementUserRepository::filterUserBySearch($keywords, $limit);
        $reply = new SearchUserToAddSupplementReply();
        if ($resp->isEmpty()) {
            return $reply;
        }

        $list = [];
        /** @var Users $item */
        foreach ($resp as $item) {
            $o = new SearchUserItem();
            $o->setId($item->uid);
            $o->setName($item->true_name ? $item->true_name : ($item->name ? $item->name : ''));
            $list[] = $o;
        }
        $reply->setList($list);
        return $reply;
    }

    public static function generateHospitalRandomGeoPos($hospitalId = 0, $visitId = 0): array
    {
        $user = Helper::getLoginUser();
        if (!self::checkUserValidCheckInSupplement($user)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '非法签入，不允许补签');
        }
        if ($visitId> 0) {
            $visit = AgentVisitRepository::getVisitById($visitId);
            if ($visit && $visit->hospital_id > 0) {
                $hospitalId = $visit->hospital_id;
            }
        }
        if (!$hospitalId) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '医院不存在或拜访不存在');
        }
        $hospital = YouyaoHospitalRepository::getHospitalById($hospitalId);
        if (!$hospital) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '医院不存在');
        }

        $lngLat = YouyaoHospitalLngLatRepository::getHospitalGeo($hospital);
        if (!$lngLat) {
            return [];
        }

        //生成随机经纬度
        [$lng, $lat] = Helper::getDistanceGeoPoint((float) $lngLat->lng, (float) $lngLat->lat, mt_rand(0, 360), mt_rand(0, (int) floor($lngLat->distance)));

        //调用高德地图获取经纬度对应地址
        $desc = GeoService::reverseGeo($lng, $lat, DataStatus::COORDINATE_GCJ02);

        //返回地址信息
        return [
            'pos' => $desc,
            'lng' => $lng,
            'lat' => $lat,
            'coordinate_type' => DataStatus::COORDINATE_GCJ02
        ];
    }
}
