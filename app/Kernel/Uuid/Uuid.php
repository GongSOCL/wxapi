<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Kernel\Uuid;

use Ramsey\Uuid\Codec\OrderedTimeCodec;
use Ramsey\Uuid\Uuid as SDK;
use Ramsey\Uuid\UuidFactory;

class Uuid
{
    public function generate(): string
    {
        return str_replace('-', '', SDK::uuid4()->toString());
    }

    public static function getFactory(): UuidFactory
    {
        $factory = new UuidFactory();
        $codec = new OrderedTimeCodec($factory->getUuidBuilder());
        $factory->setCodec($codec);
        return $factory;
    }
}
