<?php
declare(strict_types=1);

namespace App\Kernel\Minio;

use App\Exception\BusinessException;
use Aws\Handler\GuzzleV6\GuzzleHandler;
use Aws\S3\S3Client;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Filesystem\FilesystemFactory;
use Hyperf\Guzzle\CoroutineHandler;
use Hyperf\Utils\ApplicationContext;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use Aws\Sts\StsClient;

class Minio
{
    /**
     * 获取filesystem对象处理基本上传之类的工作
     * @param string $name
     */
    public static function getFileSystem($name = "minio")
    {
        return ApplicationContext::getContainer()
            ->get(FilesystemFactory::class)
            ->get($name);
    }

    /**
     * 获取对应的s3client, 可以支持更多操作
     * @param string $name
     * @return S3Client
     */
    public static function getS3Client($name = "minio"): S3Client
    {
        $adaptor = ApplicationContext::getContainer()
            ->get(FilesystemFactory::class)
            ->get($name)
            ->getAdapter();
        if (!($adaptor && $adaptor instanceof  AwsS3Adapter)) {
            throw new \UnexpectedValueException("配置不存在");
        }

        return $adaptor->getClient();
    }

    /**
     * 创建私有bucket文件对象链接(含过期时间)
     * @param $bucket
     * @param $key
     * @param string $expireTime
     * @param string $name
     * @return string
     */
    public static function createPrivateBucketFile($bucket, $key, $expireTime = "+ 1 days", $name = "minio"): string
    {
        $client = self::getS3Client($name);
        $cmd = $client->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key' => $key
        ]);
        return (string)$client->createPresignedRequest($cmd, $expireTime)->getUri();
    }

    /**
     * 创建公共bucket文件对象uri
     * @param $bucket
     * @param $key
     * @param string $baseUri
     * @return string
     */
    public static function createPublicBucketFileLink($bucket, $key, $baseUri = "", $useSsl = false): string
    {
        $baseUri = $baseUri ?: (string)env("MINIO_BASE_URI");
        $path = sprintf("%s/%s", trim($bucket, '/'), trim($key, '/'));
        if ($baseUri) {
            $path = sprintf("%s/%s", rtrim($baseUri, '/'), $path);
        }
        if ($useSsl && substr($path, 0, 6) != 'https:') {
            $path = 'https' . substr($path, 4);
        }
        return $path;
    }

    public static function getSts($name = "minio")
    {
        $file = ApplicationContext::getContainer()
            ->get(ConfigInterface::class)
            ->get("file");
        if (!isset($file['storage'][$name])) {
            throw new BusinessException(AppErr::ServerErr, "存储配置不存在");
        }
        $cfg = $file['storage'][$name];
        if (isset($cfg['driver'])) {
            unset($cfg['driver']);
        }
        $handler = new GuzzleHandler(new Client([
            'handler' => HandlerStack::create(new CoroutineHandler()),
        ]));
        $cfg['http_handler'] = $handler;
        $stsClient = new StsClient($cfg);
        $roleToAssumeArn = sprintf("arn:aws:iam::%s:role/%s", env('S3_KEY'), 'writeonly');
        $res = $stsClient->assumeRole([
            'RoleArn' => $roleToAssumeArn,
            'RoleSessionName' => 'session1'
        ]);
        return $res->toArray();
    }
}
