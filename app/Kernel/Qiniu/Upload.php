<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Kernel\Qiniu;

use App\Exception\BusinessException;
use App\Helper\Helper;
use Hyperf\Config\Annotation\Value;
use Hyperf\Di\Annotation\Inject;
use Qiniu\Config;
use Qiniu\Http\Error;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;

/**
 * Class Upload.
 */
class Upload
{
    /**
     * @Inject
     * @var UploadManager
     */
    private $upload;

    /**
     * @Inject
     * @var Auth
     */
    private $auth;

    /**
     * @Value("qiniu.bucket.user.name")
     */
    private $userBucketName;

    /**
     * @Value("qiniu.bucket.user.domain")
     */
    private $userBucketDomain;

    public function putToUser(
        $key,
        $data,
        $params = null,
        $mime = 'application/octet-stream',
        $fname = 'default_filename'
    ) {
        [$ret, $err] = $this->upload->put(
            $this->auth->uploadToken($this->userBucketName),
            $key,
            $data,
            $params,
            $mime,
            $fname
        );

        /** @var \Qiniu\Http\Error $err */
        if ($err !== null) {
            throw new BusinessException(-1, '文件上传失败：' . $err->message());
        }

        return $this->getUserBucketUrl($ret['key']);
    }

    public function getUserBucketUrl($filename)
    {
        return $this->userBucketDomain . '/' . $filename;
    }

    private function getBucketManager(): BucketManager
    {
        $config = new Config();
        return new BucketManager(
            $this->auth->getAuth(),
            $config
        );
    }

    /**
     * 删除空间中的文件
     * @param $bucket
     * @param $key
     * @return bool
     */
    public function deleteFile($bucket, $key): bool
    {
        $log = Helper::getLogger();
        /** @var Error $err */
        [$ret, $err] = $this->getBucketManager()->delete($bucket, $key);
        if ($err != null) {
            $log->error("delete_qiniu_file_error", [
                'key' => $key,
                'bucket' => $bucket,
                'code' => $err->code(),
                'message' => $err->message()
            ]);
            return false;
        }

        return true;
    }
}
