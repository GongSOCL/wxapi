<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Kernel\Qiniu;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Annotation\Inject;
use Qiniu\Auth as SDK;

/**
 * Class Auth
 * @package App\Kernel\Qiniu
 * @mixin SDK
 */
class Auth
{
    /**
     * @var SDK
     */
    private $auth;

    /**
     * @Inject
     * @var ConfigInterface
     */
    private $config;

    public function __construct()
    {
        $this->auth = new SDK($this->config->get('qiniu.ak'), $this->config->get('qiniu.sk'));
    }

    public function __call($name, $arguments)
    {
        return $this->auth->{$name}(...$arguments);
    }

    public function getAuth(): SDK
    {
        return $this->auth;
    }
}
