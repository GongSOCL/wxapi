<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Kernel\Pdf;

use App\Helper\Helper;
use GuzzleHttp\HandlerStack;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Guzzle\CoroutineHandler;
use Hyperf\Guzzle\HandlerStackFactory;
use Hyperf\Utils\ApplicationContext;
use TheCodingMachine\Gotenberg\Client;
use TheCodingMachine\Gotenberg\DocumentFactory;
use TheCodingMachine\Gotenberg\HTMLRequest;

class Pdf
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @Inject
     * @var ConfigInterface
     */
    private $config;

    public function __construct()
    {
        $handleStack = new HandlerStack(new CoroutineHandler());
        $this->client = new Client(
            $this->config->get('gotenberg.endpoint'),
            \Http\Adapter\Guzzle6\Client::createWithConfig([
                'timeout' => 15,
                'debug' => false,
                'handler' => $handleStack
                ])
        );
    }

    /**
     * @throws \TheCodingMachine\Gotenberg\ClientException
     * @throws \TheCodingMachine\Gotenberg\FilesystemException
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function createFromHtml(string &$html)
    {
        return $this->client->post(
            new HTMLRequest(
                DocumentFactory::makeFromString('index.html', $html)
            )
        );
    }
}
