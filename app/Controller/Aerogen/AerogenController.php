<?php

/**
 *
 * @OA\Post(
 *      path="/aerogen/doctor/list",
 *      tags={"优云邀请"},
 *      summary="医生列表",
 *      description="医生列表",
 *      operationId="/aerogen/doctor/list",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="r_id", type="integer", description="好友关系id"),
 *                          @OA\Property(property="user_unionid", type="string", description="医生unionid"),
 *                          @OA\Property(property="nickname", type="string", description="昵称"),
 *                          @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                          @OA\Property(property="headimgurl", type="string", description="头像"),
 *                          @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                          @OA\Property(property="link_doctor_id", type="integer", description="关联的医生id")
 *                      )
 *                  )
 *          )
 *      )
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/aerogen/drugstore/list",
 *      tags={"优云邀请"},
 *      summary="药店列表",
 *      description="药店列表",
 *      operationId="/aerogen/drugstore/list",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="drugstore_id", type="integer", description="药店id"),
 *                          @OA\Property(property="drugstore_name", type="string", description="药店名称")
 *                      )
 *                  )
 *          )
 *      )
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/aerogen/product/list",
 *      tags={"优云邀请"},
 *      summary="产品列表",
 *      description="产品列表",
 *      operationId="/aerogen/product/list",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="drug_id", type="integer", description="产品id"),
 *                          @OA\Property(property="drug_name", type="string", description="产品名称")
 *                      )
 *                  )
 *          )
 *      )
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/aerogen/share/info",
 *      tags={"优云邀请"},
 *      summary="分享信息",
 *      description="分享信息",
 *      operationId="/aerogen/share/info",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="title", type="integer", description="标题"),
 *                          @OA\Property(property="describe", type="string", description="描述"),
 *                          @OA\Property(property="img", type="string", description="图片"),
 *                          @OA\Property(property="url", type="string", description="地址")
 *                      )
 *                  )
 *          )
 *      )
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/aerogen/qrcode/doctor",
 *      tags={"优云邀请"},
 *      summary="医生二维码",
 *      description="医生二维码",
 *      operationId="/aerogen/qrcode/doctor",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="link_doctor_id", type="integer", description="关联医生id"),
 *                  required={"user_token","user_yyid","link_doctor_id"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="url", type="string", description="二维码地址")
 *                      )
 *                  )
 *          )
 *      )
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/aerogen/qrcode/drugstore",
 *      tags={"优云邀请"},
 *      summary="药店二维码",
 *      description="药店二维码",
 *      operationId="/aerogen/qrcode/drugstore",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="url", type="string", description="页面地址")
 *                      )
 *                  )
 *          )
 *      )
 * )
 */

declare(strict_types=1);

namespace App\Controller\Aerogen;

use App\Controller\Controller;
use App\Request\Aerogen\DoctorListRequest;
use App\Request\Aerogen\DoctorQrcodeRequest;
use App\Service\Aerogen\AerogenService;
use Hyperf\Di\Annotation\Inject;

class AerogenController extends Controller
{
    /**
     * @Inject()
     * @var AerogenService
     */
    private $aerogenService;

    /**
     * @param DoctorListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doctorList(DoctorListRequest $request)
    {
        $agentYyid = $request->input('user_yyid');
        $data = $this->aerogenService->getDoctorListByAgentYyid($agentYyid);

        return $this->response->success($data);
    }

    /**
     * @param DoctorListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function drugstoreList(DoctorListRequest $request)
    {
        $agentYyid = $request->input('user_yyid');
        $data = $this->aerogenService->getDrugstoreListByAgentYyid($agentYyid);

        return $this->response->success($data);
    }

    /**
     * @param DoctorListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function productList(DoctorListRequest $request)
    {
        $agentYyid = $request->input('user_yyid');
        $data = $this->aerogenService->getProductListByAgentYyid($agentYyid);

        return $this->response->success($data);
    }

    /**
     * @param DoctorListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function shareInfo(DoctorListRequest $request)
    {
        $agentYyid = $request->input('user_yyid');
        $data = $this->aerogenService->getShareInfo($agentYyid);

        return $this->response->success($data);
    }

    /**
     * @param DoctorQrcodeRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doctorQrcode(DoctorQrcodeRequest $request)
    {
        $agentYyid = $request->input('user_yyid');
        $linkDoctorid = $request->input('link_doctor_id');
        $data = $this->aerogenService->getDoctorQrcode($agentYyid, $linkDoctorid);

        return $this->response->success($data);
    }

    /**
     * @param DoctorListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function drugstoreQrcode(DoctorListRequest $request)
    {
        $data = $this->aerogenService->getDrugstoreQrcode();

        return $this->response->success($data);
    }
}
