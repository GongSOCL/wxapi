<?php
declare(strict_types=1);


namespace App\Controller\Visit;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Controller\Controller;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\AgentVisit;
use App\Request\Visit\AgentVisiDoctorAddRequest;
use App\Request\Visit\AgentVisiDoctorListRequest;
use App\Request\Visit\AgentVisitListRequest;
use App\Request\Visit\CheckGeoRequest;
use App\Request\Visit\CheckInRequest;
use App\Request\Visit\CheckOutRequest;
use App\Request\Visit\GetVisitConfigRequest;
use App\Service\Visit\VisitService;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Validation\Rule;
use Hyperf\Validation\ValidationException;
use Hyperf\Validation\ValidatorFactory;
use Psr\Http\Message\ResponseInterface;

class VisitController extends Controller
{

    /**
     *
     * @OA\Post (
     *      path="/agent/visits",
     *      tags={"代表拜访"},
     *      summary="拜访列表",
     *      description="代表拜访",
     *      operationId="AgentVisitList",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="check_in_start_at", type="string", description="搜索开始时间"),
     *                  @OA\Property(property="check_in_end_at", type="string", description="搜索结束时间"),
     *                  @OA\Property(property="check_out_start_at", type="string", description="搜索开始时间"),
     *                  @OA\Property(property="check_out_end_at", type="string", description="搜索结束时间"),
     *                  @OA\Property(property="hospital_id", type="integer", description="搜索医院id"),
     *                  @OA\Property(
     *                      property="office_id",
     *                      type="array",
     *                      description="搜索科室id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="check_type", type="integer", description="拜访类型: 0所有 1签入 2签出"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 0所有 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                  @OA\Property(property="current", type="integer", description="当前页: 默认1"),
     *                  @OA\Property(property="limit", type="integer", description="每页显示的数量:默认10"),
     *                  required={"user_yyid", "user_token"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="拜访记录列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="拜访id"),
     *                                  @OA\Property(property="date", type="string", description="拜访时间"),
     *                                  @OA\Property(
     *                                      property="check_type",
     *                                      type="integer",
     *                                      description="记录类型: 1签入 2签出"),
     *                                  @OA\Property(property="type", type="string", description="拜访类型"),
     *                                  @OA\Property(property="hopital", type="string", description="医院名称"),
     *                                  @OA\Property(property="state", type="integer", description="是否有效的拜访0有效1无效"),
     *                                  @OA\Property(
     *                                      property="state_comment",
     *                                      type="string",
     *                                      description="关于是否有效的拜访的说明"),
     *                                  @OA\Property(
     *                                      property="departs",
     *                                      type="array",
     *                                      description="科室名称列表",
     *                                      @OA\Items(type="string")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="doctors",
     *                                      type="array",
     *                                      description="医生列表",
     *                                      @OA\Items(type="string")
     *                                  )
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AgentVisitListRequest $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function list(AgentVisitListRequest $request): ResponseInterface
    {
        //获取并格式化时间字段
        $checkInStartAt =  $request->post('check_in_start_at', '');
        $checkInEndAt = $request->post('check_in_end_at', '');
        $checkOutStartAt = $request->post('check_out_start_at', '');
        $checkOutEndAt = $request->post('check_out_end_at', '');
        $formatStr = "Y-m-d 00:00:00";
        $endFormatStr = "Y-m-d 23:59:59";
        $departIds = Helper::getArrayRequestParams($request, 'office_id');
        if ($checkInStartAt) {
            $checkInStartAt = Helper::formatInputDate($checkInStartAt, $formatStr);
        }
        if ($checkInEndAt) {
            $checkInEndAt = Helper::formatInputDate($checkInEndAt, $endFormatStr);
        }
        if ($checkOutStartAt) {
            $checkOutStartAt = Helper::formatInputDate($checkOutStartAt, $formatStr);
        }
        if ($checkOutEndAt) {
            $checkOutEndAt = Helper::formatInputDate($checkOutEndAt, $endFormatStr);
        }
        if ($checkInStartAt && $checkInEndAt) {
            [$checkInStartAt, $checkInEndAt] = Helper::exchangeDate($checkInStartAt, $checkInEndAt);
        }
        if ($checkOutEndAt && $checkOutStartAt) {
            [$checkOutStartAt, $checkOutEndAt] = Helper::exchangeDate($checkOutStartAt, $checkOutEndAt);
        }
        $checkType = $request->post('check_type', 0);
        $visitType = $request->post('visit_type', 0);
        [$page, $data] = VisitService::list(
            $checkInStartAt,
            $checkInEndAt,
            $checkOutStartAt,
            $checkOutEndAt,
            $checkType,
            $visitType,
            intval($request->post('hospital_id', 0)),
            $departIds,
            intval($request->post('current', 1)),
            intval($request->post('limit', 10))
        );
        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/agent/visits/new-check-out-info/{id}",
     *      tags={"代表拜访"},
     *      summary="签出新增时获取拜访记录详情",
     *      description="代表拜访",
     *      operationId="AgentVisitNewCheckOut",
     *      @OA\Parameter(
     *         description="访问记录id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="详情",
     *                              @OA\Property(property="id", type="integer", description="上传id"),
     *                              @OA\Property(
     *                                  property="hospital",
     *                                  type="object",
     *                                  description="医院信息",
     *                                  @OA\Property(property="id", type="integer", description="医院id"),
     *                                  @OA\Property(property="name", type="string", description="医院名称"),
     *                                  @OA\Property(
     *                                      property="org_type",
     *                                      type="integer",
     *                                      description="1 表格医院  2表示药店   3表示经销商")
     *                              ),
     *                              @OA\Property(
     *                                  property="visit_type",
     *                                  type="integer",
     *                                  description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function newCheckOutInfo($id): ResponseInterface
    {
        $info = VisitService::getNewCheckOutInfo($id);
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/agent/visits/info/{id}",
     *      tags={"代表拜访"},
     *      summary="签入或签出修改时获取拜访记录详情",
     *      description="代表拜访",
     *      operationId="AgentVisitInfo",
     *      @OA\Parameter(
     *         description="访问记录id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="详情",
     *                              @OA\Property(property="id", type="integer", description="上传id"),
     *                              @OA\Property(
     *                                  property="hospital",
     *                                  type="object",
     *                                  description="医院信息",
     *                                  @OA\Property(property="id", type="integer", description="医院id"),
     *                                  @OA\Property(property="name", type="string", description="医院名称"),
     *                                  @OA\Property(
     *                                      property="org_type",
     *                                      type="integer",
     *                                      description="1 表格医院  2表示药店   3表示经销商")
     *                              ),
     *                              @OA\Property(
     *                                  property="visit_type",
     *                                   type="integer",
     *                                   description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                              @OA\Property(property="visit_status", type="integer", description="1已签入 2已签出"),
     *                              @OA\Property(
     *                                  property="departs",
     *                                  type="array",
     *                                  description="科室列表",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="id", type="integer", description="科室id"),
     *                                      @OA\Property(property="name", type="string", description="科室名称")
     *                                  )
     *                              ),
     *                              @OA\Property(
     *                                  property="doctors",
     *                                  type="array",
     *                                  description="医生列表",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="id", type="integer", description="医生信息表id"),
     *                                      @OA\Property(property="name", type="string", description="医生名称")
     *                                  )
     *                              ),
     *                              @OA\Property(
     *                                  property="series_ids",
     *                                  type="array",
     *                                  description="产品id列表",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="id", type="integer", description="药品id"),
     *                                      @OA\Property(property="name", type="string", description="药品名称")
     *                                  )
     *                              ),
     *                              @OA\Property(property="result", type="string", description="拜访结果"),
     *                              @OA\Property(property="next", type="string", description="后续跟进事宜"),
     *                              @OA\Property(
     *                                  property="pics",
     *                                  type="array",
     *                                  description="图片列表:已签入时为签入图片列表 已签出时为签出图片列表",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="id", type="integer", description="图上上传id"),
     *                                      @OA\Property(property="name", type="string", description="上传图片名称"),
     *                                      @OA\Property(property="url", type="string", description="上传图片url"),
     *                                  )
     *                              ),
     *                              @OA\Property(property="lat", type="number", description="纬度"),
     *                              @OA\Property(property="lng", type="number", description="经度"),
     *                              @OA\Property(property="position", type="string", description="位置描述"),
     *                              @OA\Property(
     *                                  property="geo_opt",
     *                                  type="integer",
     *                                  description="超出范围选项id,默认传0表示未超出范围"),
     *                              @OA\Property(property="geo_comment", type="string", description="超出选项范围输入内容"),
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function info($id): ResponseInterface
    {
        $info = VisitService::info($id);
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Put(
     *      path="/agent/visits/check-in",
     *      tags={"代表拜访"},
     *      summary="代表拜访签入",
     *      description="代表拜访签入",
     *      operationId="AgentVisitCheckIn",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                  @OA\Property(
     *                      property="pics",
     *                      type="array",
     *                      description="上传图片id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="lat", type="float", description="纬度"),
     *                  @OA\Property(property="lng", type="float", description="经度"),
     *                  @OA\Property(property="position", type="string", description="位置描述"),
     *                  @OA\Property(property="geo_opt", type="integer", description="地理位置超出选项id,当调用检查接口返回未通过时需要"),
     *                  @OA\Property(property="geo_comment", type="string", description="地理位置超出填空内容，当调用检查接口返回未通过时需要"),
     *                  @OA\Property(property="special_id", type="integer", description="异常签入选项id, 默认不传为0"),
     *                  @OA\Property(
     *                      property="special_comment",
     *                      type="string",
     *                      description="异常签入选项备注,当speical_id不为0时必传"
     *                  ),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "hospital_id",
     *                      "visit_type",
     *                      "lat",
     *                      "lng"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="新加拜访记录id")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param CheckInRequest $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function checkIn(CheckInRequest $request): ResponseInterface
    {
        list($pics, $visitType, $hospitalId) = $this->checkInParamValidate($request);
        $coordinateType = DataStatus::COORDINATE_GPS;
        $visit = VisitService::checkIn(
            $visitType,
            $hospitalId,
            $pics,
            $request->post('lat'),
            $request->post('lng'),
            $request->post('position'),
            (int) $request->post('geo_opt', 0),
            $request->post('geo_comment', ""),
            $coordinateType,
            (int)$request->post('special_id', 0),
            (string)$request->post('special_comment', '')
        );

        return $this->response->success([
            'id' => $visit->id
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/agent/visits/check-geo",
     *      tags={"代表拜访"},
     *      summary="代表拜访地理位置检查",
     *      description="代表拜访地理位置检查",
     *      operationId="AgentVisitCheckGeo",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="lat", type="float", description="纬度"),
     *                  @OA\Property(property="lng", type="float", description="经度"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "hospital_id",
     *                      "lat",
     *                      "lng"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="result", type="integer", description="检查结果: 1在范围内 0不在范围内")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param CheckGeoRequest $request
     * @return ResponseInterface
     */
    public function checkGeo(CheckGeoRequest $request): ResponseInterface
    {
        $checkResult = VisitService::checkGeo(
            $request->post('hospital_id'),
            $request->post('lat'),
            $request->post('lng')
        );
        return $this->response->success([
            'result' => $checkResult ? 1 : 0
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/agent/visits/config",
     *      tags={"代表拜访"},
     *      summary="获取拜访配置",
     *      description="获取拜访配置",
     *      operationId="GetAgentVisitConfig",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="type", type="integer", description="配置类型：  1 拜访目的, 2超出范围选项 3. 异常拜访原因"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "type"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="config",
     *                              type="array",
     *                              description="配置列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="配置选项id"),
     *                                  @OA\Property(property="name", type="string", description="配置选项名称"),
     *                                  @OA\Property(
     *                                      property="is_extra_input",
     *                                      type="integer",
     *                                      description="是否需要额外输入内容"
     *                                  )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetVisitConfigRequest $request
     * @return ResponseInterface
     */
    public function config(GetVisitConfigRequest $request): ResponseInterface
    {
        $config = VisitService::getVisitConfig((int)$request->post('type'));
        return $this->response->success([
            'config' => $config
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/agent/visits/check-out/{id:\d+}",
     *      tags={"代表拜访"},
     *      summary="代表拜访记录签出",
     *      description="代表拜访记录签出",
     *      operationId="AgentVisitCheckOut",
     *      @OA\Parameter(
     *         description="访问记录id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                  @OA\Property(
     *                      property="depart_ids",
     *                      type="array",
     *                      description="科室id列表 多个id使用逗号连接",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="doctor_ids",
     *                      type="array",
     *                      description="医生id列表",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="series_ids",
     *                      type="array",
     *                      description="产品id列表 多个id使用逗号连接",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="result", type="string", description="拜访结果"),
     *                  @OA\Property(property="next", type="string", description="后续跟进事宜"),
     *                  @OA\Property(
     *                      property="pics",
     *                      type="array",
     *                      description="上传图片id列表 多个id使用逗号连接",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="lat", type="float", description="纬度"),
     *                  @OA\Property(property="lng", type="float", description="经度"),
     *                  @OA\Property(property="position", type="string", description="位置描述"),
     *                  @OA\Property(property="geo_opt", type="integer", description="超出范围选项id,默认传0表示未超出范围"),
     *                  @OA\Property(property="geo_comment", type="string", description="超出选项范围输入内容"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "hospital_id",
     *                      "visit_type",
     *                      "pics",
     *                      "lat",
     *                      "lng",
     *                      "result",
     *                      "next"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param CheckOutRequest $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function checkOut($id, CheckOutRequest $request): ResponseInterface
    {
        // 获取并校验参数
        list($departIds, $seriesIds, $picIds, $result, $next, $doctorIds) = $this->checkOutParamValidate($request);

        // 签出
        VisitService::checkOut(
            $id,
            $request->post('hospital_id'),
            $request->post('visit_type'),
            $departIds,
            $seriesIds,
            $result,
            $next,
            $request->post('lat', 0),
            $request->post('lng', 0),
            $request->post('position', ''),
            $picIds,
            (int) $request->post('geo_opt', 0),
            (string) $request->post('geo_comment', ''),
            $doctorIds
        );

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/agent/visits/edit-check-in/{id}",
     *      tags={"代表拜访"},
     *      summary="代表拜访签入编辑",
     *      description="代表拜访签入编辑",
     *      operationId="AgentVisitCheckInEdit",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                  @OA\Property(
     *                      property="pics",
     *                      type="array",
     *                      description="上传图片id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="lat", type="float", description="纬度"),
     *                  @OA\Property(property="lng", type="float", description="经度"),
     *                  @OA\Property(property="position", type="string", description="位置描述"),
     *                  @OA\Property(property="geo_opt", type="integer", description="地理位置超出选项id,当调用检查接口返回未通过时需要"),
     *                  @OA\Property(property="geo_comment", type="string", description="地理位置超出填空内容，当调用检查接口返回未通过时需要"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "hospital_id",
     *                      "visit_type",
     *                      "lat",
     *                      "lng"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param CheckInRequest $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function editCheckIn($id, CheckInRequest $request): ResponseInterface
    {
        list($pics, $visitType, $hospitalId) = $this->checkInParamValidate($request);

        VisitService::editCheckIn(
            $id,
            $visitType,
            $hospitalId,
            $pics,
            $request->post('lng', 0),
            $request->post('lat', 0),
            $request->post('position', ""),
            (int) $request->post('geo_opt', 0),
            $request->post('geo_comment', '')
        );

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/agent/visits/edit-check-out/{id}",
     *      tags={"代表拜访"},
     *      summary="代表拜访签出编辑",
     *      description="代表拜访签出编辑",
     *      operationId="AgentVisitCheckOutEdit",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                   @OA\Property(
     *                      property="depart_ids",
     *                      type="array",
     *                      description="科室id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="doctor_ids",
     *                      type="array",
     *                      description="医生id列表",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="series_ids",
     *                      type="array",
     *                      description="产品id列表",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="result", type="string", description="拜访结果"),
     *                  @OA\Property(property="next", type="string", description="后续跟进事宜"),
     *                  @OA\Property(
     *                      property="pics",
     *                      type="array",
     *                      description="上传图片id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="lat", type="float", description="纬度"),
     *                  @OA\Property(property="lng", type="float", description="经度"),
     *                  @OA\Property(property="position", type="string", description="位置描述"),
     *                  @OA\Property(property="geo_opt", type="integer", description="地理位置超出选项id,当调用检查接口返回未通过时需要"),
     *                  @OA\Property(property="geo_comment", type="string", description="地理位置超出填空内容，当调用检查接口返回未通过时需要"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "hospital_id",
     *                      "visit_type",
     *                      "lat",
     *                      "lng"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param CheckOutRequest $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function editCheckOut($id, CheckOutRequest $request): ResponseInterface
    {
        list($departIds, $seriesIds, $picIds, $result, $next, $doctorIds) = $this->checkOutParamValidate($request);

        // 签出
        VisitService::editCheckOut(
            $id,
            $request->post('hospital_id'),
            $request->post('visit_type'),
            $departIds,
            $seriesIds,
            $result,
            $next,
            $request->post('lat', 0),
            $request->post('lng', 0),
            $request->post('position', ''),
            $picIds,
            (int) $request->post('geo_opt', 0),
            $request->post('geo_comment', ''),
            $doctorIds
        );
        return $this->response->success([]);
    }

    /**
     * @param CheckOutRequest $request
     * @return array
     */
    private function checkOutParamValidate(CheckOutRequest $request): array
    {
        // 获取并校验参数
        $departIds = Helper::getArrayRequestParams($request, 'depart_ids');
        $seriesIds = Helper::getArrayRequestParams($request, 'series_ids');
        $picIds = Helper::getArrayRequestParams($request, 'pics');
        $doctorIds = Helper::getArrayRequestParams($request, 'doctor_ids');

        $visitType = $request->post('visit_type');
        $result = $request->post('result', '');
        $next = $request->post('next', '');
        $isTypeRequired = function () use ($visitType) {
            return in_array($visitType, [
                AgentVisit::VISIT_TYPE_TO_FACE,
                AgentVisit::VISIT_TYPE_ONLINE,
                AgentVisit::VISIT_TYPE_EXTERNAL_MEETING
            ]);
        };
        $isNextRequired = function () use ($visitType) {
            return in_array($visitType, [
                AgentVisit::VISIT_TYPE_TO_FACE,
                AgentVisit::VISIT_TYPE_ONLINE
            ]);
        };
        $validator = ApplicationContext::getContainer()->get(ValidatorFactory::class)
            ->make([
                'visit_type' => $visitType,
                'series_ids' => $seriesIds,
                'pics'       => $picIds,
                'result'     => $result,
                'next'       => $next
            ], [
                'series_ids'   => [
                    'array',
                    Rule::requiredIf($isTypeRequired)
                ],
                'series_ids.*' => 'integer|nullable|gt:0',
                'pics'         => 'required|array',
                'pics.*'       => 'required|integer|gt:0',
                'result'       => [
                    'string',
                    Rule::requiredIf($isTypeRequired)
                ],
                'next'         => [
                    'string',
                    Rule::requiredIf($isNextRequired)
                ]
            ], [
                'series.ids.required_if'               => '请至少选择一个产品',
                'series_ids.*.integer|series_ids.*.gt' => '产品格式不正确',
                'pics.required'                        => '请至少上传一张图片',
                'pics.array'                           => '上传图片格式不正确',
                'pics.*.required'                      => '请至少上传一张图片',
                'pics.*.integer|pics.*.gt'             => '上传图片格式不正确',
                'result.required_if'                   => '拜访结果不能为空',
                'next.required_if'                     => '后续计划不能为空'
            ]);
        if ($validator->fails()) {
            throw new ValidationException($validator, $this->response);
        }
        $geoComment = (string) $request->post('geo_comment', '');
        if (mb_strlen($geoComment) > 64) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "超出范围原因超出长度限制，最长允许64个字");
        }
        return array($departIds, $seriesIds, $picIds, $result, $next, $doctorIds);
    }

    /**
     * @param CheckInRequest $request
     * @return array
     */
    private function checkInParamValidate(CheckInRequest $request): array
    {
        $pics = Helper::getArrayRequestParams($request, 'pics');
        $validator = ApplicationContext::getContainer()->get(ValidatorFactory::class)
            ->make([
                'pics' => $pics,
            ], [
                'pics'   => 'required|array',
                'pics.*' => 'integer|gt:0'
            ], [
                'pics.required'            => '需要至少拍一张照片',
                'pics.array'               => '照片格式不正确',
                'pics.*.integer'           => '图片上传未完成，非法请求',
                'pics.*.gt'           => '图片上传未完成，非法请求',
            ]);
        if ($validator->fails()) {
            throw new ValidationException($validator, $this->response);
        }

        $visitType = $request->post('visit_type');
        $hospitalId = $request->post('hospital_id', 0);
        if ($hospitalId == 0 && in_array($visitType, [
                AgentVisit::VISIT_TYPE_TO_FACE,
                AgentVisit::VISIT_TYPE_ONLINE
            ])) {
            throw new BusinessException(ErrorCode::CHECKIN_HOSPITAL_EMPTY_INVALID);
        }

        $geoComment = (string) $request->post('geo_comment', '');
        if (mb_strlen($geoComment) > 64) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "超出范围原因超出长度限制，最长允许64个字");
        }
        return array($pics, $visitType, $hospitalId);
    }


    /**
     *
     * @OA\Post(
     *      path="/agent/visits/doctors",
     *      tags={"代表拜访"},
     *      summary="获取代表群组医生",
     *      description="获取代表群组医生",
     *      operationId="AgentVisitDoctorList",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="keywords", type="string", description="搜索关键字"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "hospital_id",
     *                      "depart_id"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      @OA\Property(
     *                          property="list",
     *                          type="array",
     *                          description="医生列表",
     *                          @OA\Items(
     *                              type="object",
     *                              @OA\Property(property="id", type="integer", description="医生id"),
     *                              @OA\Property(property="name", type="string", description="医生名"),
     *                              @OA\Property(property="head_img_url", type="string", description="头像地址"),
     *                              @OA\Property(property="hospital_name", type="string", description="医院名称")
     *                          )
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AgentVisiDoctorListRequest $request
     * @return ResponseInterface
     */
    public function getDoctors(AgentVisiDoctorListRequest $request): ResponseInterface
    {
        $hospitalId = (int) $request->post('hospital_id');
        $keywords = (string) $request->post('keywords', '');
        $resp = VisitService::getGroupDoctors($hospitalId, $keywords);

        return $this->response->success($resp);
    }

    /**
     *
     * @OA\Post(
     *      path="/agent/visits/doctors/add",
     *      tags={"代表拜访"},
     *      summary="添加代表群组医生",
     *      description="添加代表群组医生",
     *      operationId="AgentVisitDoctorAdd",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="check", type="boolean", description="是否判断，有相同姓名医生存在"),
     *                  @OA\Property(property="name", type="string", description="姓名"),
     *                  @OA\Property(property="sex", type="integer", description="性别: 1男 2女"),
     *                  @OA\Property(property="mobile", type="string", description="手机号"),
     *                  @OA\Property(property="position", type="string", description="级别"),
     *                  @OA\Property(property="job_title", type="string", description="职称"),
     *                  @OA\Property(property="field_id", type="integer", description="擅长领域id"),
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="depart_id", type="integer", description="部门id"),
     *                  @OA\Property(property="clinic_rota", type="integer", description="门诊时间"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "hospital_id",
     *                      "depart_id",
     *                      "mobile",
     *                      "name"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      @OA\Property(
     *                          property="id",
     *                          type="integer",
     *                          description="新加医生记录id"
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AgentVisiDoctorAddRequest $request
     * @return ResponseInterface
     */
    public function addDoctor(AgentVisiDoctorAddRequest $request): ResponseInterface
    {
        $doctor = VisitService::addGroupDoctors(
            (boolean) $request->post('check', true),
            (string) $request->post('name'),
            (int) $request->post('hospital_id'),
            (int) $request->post('depart_id'),
            (string) $request->post('mobile'),
            (int) $request->post('sex', 1),
            (string) $request->post('position', ''),
            (string) $request->post('job_title', ''),
            (int) $request->post('field_id', 0),
            (string) $request->post('clinic_rota')
        );

        return $this->response->success([
            'id' => $doctor->id
        ]);
    }
}
