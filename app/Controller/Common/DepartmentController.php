<?php

declare(strict_types=1);

namespace App\Controller\Common;

use App\Controller\Controller;
use App\Request\Common\AddServeDepartsRequest;
use App\Request\Common\DepartSearchRequest;
use App\Request\Common\ServeHospitalDepartSearchRequest;
use App\Request\Doctor\DepartmentRequest;
use App\Service\Common\DepartmentService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Str;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\Validation\ValidationException;
use Psr\Http\Message\ResponseInterface;

class DepartmentController extends Controller
{
    /**
     * @Inject()
     * @var DepartmentService
     */
    private $department;

    /**
     * @param DepartmentRequest $request
     * @return ResponseInterface
     */
    public function index(DepartmentRequest $request)
    {
        $params = $request->validated();
        $hyyid = $request->post('hospital_yyid');

        $page = isset($params['page'])&&$params['page'] ? (int)$params['page'] : 1;
        $pageSize = isset($params['page_size'])&&$params['page_size'] ? (int)$params['page_size'] : 10;

        $departmentInfo = $this->department->getDepartmentInfo($hyyid, $page, $pageSize);

        return $this->response->success($departmentInfo);
    }

    /**
     *
     * @OA\Post(
     *      path="/departs",
     *      tags={"科室"},
     *      summary="系统科室列表",
     *      description="系统科室列表",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="keywords", type="string", description="科室名称搜索条件"),
     *                  required={"user_yyid", "user_token"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="科室列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="科室id"),
     *                                  @OA\Property(property="name", type="string", description="科室名称")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param DepartSearchRequest $request
     * @return ResponseInterface
     */
    public function list(DepartSearchRequest $request): ResponseInterface
    {
        $keywords = $request->post('keywords', "");
        $list = DepartmentService::listSystemDeparts($keywords);

        return $this->response->success([
            'list' => $list
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/departs/serve",
     *      tags={"科室"},
     *      summary="服务科室列表",
     *      description="服务科室列表",
     *      operationId="DepartServeList",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                  @OA\Property(property="keywords", type="string", description="科室名称搜索条件"),
     *                  required={"user_yyid", "user_token"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="服务科室列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="科室id"),
     *                                  @OA\Property(property="name", type="string", description="服务科室名称")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ServeHospitalDepartSearchRequest $request
     * @return ResponseInterface
     */
    public function serveList(ServeHospitalDepartSearchRequest $request): ResponseInterface
    {
        $hospitalId = $request->post('hospital_id', 0);
        $keywords = $request->post('keywords', "");
        $list = DepartmentService::getServeHospitalDeparts($hospitalId, $keywords);

        return $this->response->success([
           'list' => $list
        ]);
    }

    /**
     *
     * @OA\Put(
     *      path="/departs/serve",
     *      tags={"科室"},
     *      summary="用户添加服务科室",
     *      description="用户添加服务科室",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                  @OA\Property(
     *                      property="depart_ids",
     *                      type="array",
     *                      description="科室id",
     *                      @OA\Items(type="integer")
     *                 ),
     *                 required={"user_yyid", "user_token", "hospital_id", "depart_ids"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AddServeDepartsRequest $request
     * @return ResponseInterface
     */
    public function serve(AddServeDepartsRequest $request): ResponseInterface
    {
        $hospitalId = $request->post('hospital_id');
        $departIds = $this->getArrayRequestParams($request, 'depart_ids');
        $validDepart = [
            'depart_ids' => $departIds
        ];
        $validator = ApplicationContext::getContainer()->get(ValidatorFactoryInterface::class)
            ->make(
                $validDepart,
                [
                    'depart_ids' => 'required|array',
                    'depart_ids.*' => 'required|integer|gt:0'
                ],
                [
                    'depart_ids.required' => '科室不能为空',
                    'depart_ids.*.required' => '科室不能为空',
                    'depart_ids.*.integer|depart_ids.*.gt' => '科室格式不正确'
                ]
            );
        if ($validator->fails()) {
            throw new ValidationException($validator, $this->response);
        }

        DepartmentService::addServe($hospitalId, $departIds);
        return $this->response->success([]);
    }

    private function getArrayRequestParams($request, $paramsName): array
    {
        $data = [];
        $item = $request->post($paramsName);
        if (is_array($item)) {
            return $item;
        }
        if (is_string($item)) {
            $data = json_decode($item, true);
        }
        return $data;
    }
}
