<?php

declare(strict_types=1);

namespace App\Controller\Common;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\Common\GetServeHospitalRequest;
use App\Request\Doctor\HospitalRequest;
use App\Service\Common\HospitalService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;
use Hyperf\Utils\Str;
use Psr\Http\Message\ResponseInterface;

class HospitalController extends Controller
{

    /**
     * @Inject()
     * @var HospitalService
     */
    private $hospital;

    /**
     * 关键字搜索医院
     * @param HospitalRequest $request
     * @return ResponseInterface
     */
    public function index(HospitalRequest $request)
    {
        $params = $request->validated();
        $keyword = isset($params['keyword']) ? $params['keyword'] : '';
        $pyyid = isset($params['pro_yyid']) ? $params['pro_yyid'] : '';
        $cyyid = isset($params['city_yyid']) ? $params['city_yyid'] : '';

        $page = isset($params['page'])&&$params['page'] ? (int)$params['page'] : 1;
        $pageSize = isset($params['page_size'])&&$params['page_size'] ? (int)$params['page_size'] : 10;
        $level = isset($params['level'])&&$params['level'] ? (int)$params['level'] : 0;

        $hospitalInfo = $this->hospital->getHospitalInfo($keyword, $pyyid, $cyyid, $level, $page, $pageSize);

        return $this->response->success($hospitalInfo);
    }

    /**
     *
     * @OA\Post(
     *      path="/hospital/serve",
     *      tags={"医院"},
     *      summary="服务医院列表",
     *      description="医院",
     *      operationId="HospitalServe",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="keywords", type="string", description="名称搜索关键字"),
     *                  required={"user_yyid", "user_token"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="服务医院列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="医院id"),
     *                                  @OA\Property(property="name", type="string", description="服务医院名称"),
     *                                  @OA\Property(property="org_type", type="integer", description="组织类型: 1 表格医院
     * 2表示药店   3表示经销商")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetServeHospitalRequest $request
     * @return ResponseInterface
     */
    public function serve(GetServeHospitalRequest $request): ResponseInterface
    {
        $list = HospitalService::getUserServeHospitals(
            $request->post('keywords', '')
        );
        return $this->response->success([
            'list' => $list
        ]);
    }
}
