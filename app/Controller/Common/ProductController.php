<?php
declare(strict_types=1);


namespace App\Controller\Common;

use App\Controller\Controller;
use App\Service\Common\ProductService;
use Hyperf\Utils\Str;
use Psr\Http\Message\ResponseInterface;

/**
 * 产品服务.
 * Class ProductController
 * @package App\Controller\Common
 */
class ProductController extends Controller
{
    /**
     *
     * @OA\Post(
     *      path="/series/serve",
     *      tags={"药品系列"},
     *      summary="代理药品系列列表",
     *      description="药品系列",
     *      operationId="ProductSeriesServe",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  required={"user_yyid", "user_token"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="代理药品系列",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药品系列id"),
     *                                  @OA\Property(property="name", type="string", description="药品系列名称")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     */
    public function serve(): ResponseInterface
    {
        $list = ProductService::getUserServeDrugSeries();
        return $this->response->success([
            'list' => $list
        ]);
    }
}
