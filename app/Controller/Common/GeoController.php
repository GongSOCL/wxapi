<?php
declare(strict_types=1);


namespace App\Controller\Common;

use App\Controller\Controller;
use App\Request\Common\ReverseGeoRequests;
use App\Service\Common\GeoService;
use GuzzleHttp\Exception\GuzzleException;
use Hyperf\Utils\Str;
use Psr\Http\Message\ResponseInterface;

/**
 * 地理服务.
 * Class GeoController
 * @package App\Controller\Common
 */
class GeoController extends Controller
{
    /**
     *
     * @OA\Post (
     *      path="/geo",
     *      tags={"地理位置"},
     *      summary="根据经纬度获取地理位置",
     *      description="地理位置",
     *      operationId="GeoDesc",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="lat", type="float", description="纬度"),
     *                   @OA\Property(property="lng", type="float", description="经度"),
     *                  required={"user_yyid", "user_token", "lat", "lng"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="desc", type="string", description="详细地址"),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ReverseGeoRequests $requests
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function desc(ReverseGeoRequests $requests): ResponseInterface
    {
        $location = GeoService::reverseGeo(
            $requests->post('lng'),
            $requests->post('lat')
        );
        return $this->response->success([
           'desc' => $location
        ]);
    }
}
