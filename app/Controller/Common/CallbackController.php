<?php
declare(strict_types=1);

namespace App\Controller\Common;

use App\Service\Common\MsgCallback;
use Hyperf\Utils\ApplicationContext;
use Youyao\Framework\Components\Msg\MessageRpcClient;

class CallbackController
{

    public static function msg()
    {
        $container = ApplicationContext::getContainer();
        $call = $container->get(MsgCallback::class);
        return $container->get(MessageRpcClient::class)
            ->callback($call);
    }
}
