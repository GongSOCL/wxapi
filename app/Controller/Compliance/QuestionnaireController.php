<?php

declare(strict_types=1);

namespace App\Controller\Compliance;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Controller\Controller;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\QuestionnairePaper;
use App\Repository\WechatUserRepository;
use App\Request\Compliance\Questionnaire\AnswerRequest;
use App\Request\Compliance\Questionnaire\DoctorListRequest;
use App\Request\Compliance\Questionnaire\GetRequest;
use App\Request\Compliance\Questionnaire\InviteeListRequest;
use App\Request\Compliance\Questionnaire\InviteRequest;
use App\Request\Compliance\Questionnaire\ListRequest;
use App\Request\Compliance\Questionnaire\NoticeAnswerRequest;
use App\Request\Compliance\Questionnaire\ReInviteRequest;
use App\Request\Compliance\Questionnaire\StartRequest;
use App\Service\Compliance\QuestionnaireRefService;
use App\Service\Compliance\QuestionnaireService;
use App\Service\Wechat\Callback\AgentCallbackService;
use App\Service\Wechat\Callback\DoctorCallbackService;
use App\Service\Wechat\WechatBusiness;
use Hyperf\Guzzle\ClientFactory;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Str;
use Psr\Http\Message\ResponseInterface;

class QuestionnaireController extends Controller
{
    /**
     *
     * @OA\Post(
     *      path="/compliance/questionnaire/start",
     *      tags={"问卷"},
     *      summary="开始问卷",
     *      description="问卷",
     *      operationId="questionnaireStart",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="openid", type="string", description="用户微信open-id"),
     *                  @OA\Property(property="wechat_type", type="integer", description="微信类型: 1 代表 2医生 默认2"),
     *                  @OA\Property(property="paper_yyid", type="string", description="试卷yyid"),
     *                  required={"openid", "paper_yyid"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="total", type="integer", description="试卷题目总数"),
     *                          @OA\Property(property="next_question_yyid", type="string", description="下一题yyid"),
     *                          @OA\Property(property="request_id", type="string", description="请求id,后续get和answer需要带上"),
     *                          @OA\Property(property="intro", type="string", description="问卷简介"),
     *                          @OA\Property(property="start_bg", type="string", description="开始页背景图url"),
     *                          @OA\Property(property="backend_bg", type="string", description="结束页背景图url"),
     *                          @OA\Property(property="answer_bg", type="string", description="答题页面背景图url")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     *
     * @param StartRequest $request
     * @return ResponseInterface
     */
    public function start(StartRequest $request)
    {
        $shareKey = $request->post('paper_yyid');
        if (!$shareKey) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_INVITED);
        }
        $share = Helper::decodeShareKey($shareKey);

        /** @var QuestionnairePaper $paper */
        [
            $paper,
            $total,
            $nextQuestionItemYYID,
            $startBg,
            $tailBg,
            $answerBg
        ] = QuestionnaireService::startQuestionnaire(
            $share,
            $request->post('openid')
        );
        return $this->response->success([
            'total' => $total,
            'next_question_id' => $nextQuestionItemYYID,
            'request_id' => Str::random(32),
            'intro' => $paper->intro,
            'start_bg' => $startBg,
            'end_bg' => $tailBg,
            'answer_bg' => $answerBg
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/questionnaire/reinvite",
     *      tags={"问卷"},
     *      summary="重新邀请",
     *      description="问卷",
     *      operationId="questionnaireReinvite",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  allOf={
     *                      @OA\Schema(ref="#/components/schemas/loginAuthenticate"),
     *                      @OA\Schema(
     *                          type="object",
     *                          @OA\Property(property="paper_yyid", type="string", description="问卷yyid"),
     *                          @OA\Property(property="invitee_openid", type="string", description="受邀人openid"),
     *                          required={"paper_yyid", "invitee_openid"}
     *                      )
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     *
     * @param ReInviteRequest $request
     * @return ResponseInterface
     */
    public function reInvite(ReInviteRequest $request): ResponseInterface
    {
        QuestionnaireRefService::wechatReInviteDoctor(
            (string)$request->post('paper_yyid'),
            (string)$request->post('invitee_openid')
        );
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/questionnaire/notice-answer",
     *      tags={"问卷"},
     *      summary="提醒作答",
     *      description="问卷",
     *      operationId="questionnaireNoticeAnswer",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  allOf={
     *                      @OA\Schema(ref="#/components/schemas/loginAuthenticate"),
     *                      @OA\Schema(
     *                          type="object",
     *                          @OA\Property(property="paper_yyid", type="string", description="问卷yyid"),
     *                          @OA\Property(property="invitee_openid", type="string", description="受邀人openid"),
     *                          required={"paper_yyid", "invitee_openid"}
     *                      )
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(property="data",type="object")
     *                  )
     *          )
     *      ),
     *     @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     *
     * @param NoticeAnswerRequest $request
     * @return ResponseInterface
     */
    public function noticeAnswer(NoticeAnswerRequest $request): ResponseInterface
    {
        QuestionnaireRefService::wechatNoticeToAnswer(
            (string)$request->post('paper_yyid'),
            (string)$request->post('invitee_openid')
        );

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/questionnaire/list",
     *      tags={"问卷"},
     *      summary="问卷列表",
     *      description="问卷",
     *      operationId="questionnaireList",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  allOf={
     *                      @OA\Schema(ref="#/components/schemas/loginAuthenticate"),
     *                      @OA\Schema(
     *                          type="object",
     *                          @OA\Property(property="status", type="string", description="试卷状态: 1进行中 2已结束"),
     *                          required={}
     *                      )
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="yyid", type="string", description="问卷yyid"),
     *                          @OA\Property(property="name", type="string", description="问卷名称"),
     *                          @OA\Property(property="invited", type="integer", description="已经邀请人数"),
     *                          @OA\Property(property="rest_invite", type="integer", description="可用邀请名额"),
     *                          @OA\Property(property="invite_quota", type="integer", description="邀请总量"),
     *                      )
     *                  )
     *          )
     *      ),
     *     @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     * @param ListRequest $request
     * @return ResponseInterface
     */
    public function list(ListRequest $request): ResponseInterface
    {
        $result = QuestionnaireRefService::wechatGetAgentsPapers(
            $request->input('status', 1)  == 1
        );

        return $this->response->success($result);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/questionnaire/invitees-list",
     *      tags={"问卷"},
     *      summary="问卷受邀请人列表",
     *      description="问卷",
     *      operationId="questionnaireInviteesList",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  allOf={
     *                      @OA\Schema(ref="#/components/schemas/loginAuthenticate"),
     *                      @OA\Schema(
     *                          type="object",
     *                          @OA\Property(property="paper_yyid", type="string", description="试卷yyid"),
     *                          required={"paper_yyid"}
     *                      )
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="invite_more", type="integer", description="是否可以邀请更多: 0不可 1可以"),
     *                          @OA\Property(property="paper_name", type="string", description="问卷名称"),
     *                          @OA\Property(
     *                              property="statistical",
     *                              type="object",
     *                              description="统计数据",
     *                              @OA\Property(property="invite_quota", type="integer", description="总计"),
     *                              @OA\Property(property="answered", type="integer", description="已回答"),
     *                              @OA\Property(property="invalid", type="integer", description="已作废"),
     *                              @OA\Property(property="rest_invite", type="integer", description="可邀请")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="邀请人员列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="wechat_name", type="string", description="受邀请人yyid"),
     *                                  @OA\Property(
     *                                      property="status",
     *                                      type="integer",
     *                                      description="受邀请人回答状态: 1 未作答 2 已作答 3 已作废"
     *                                  ),
     *                                  @OA\Property(property="invitee_openid", type="string", description="受邀请人openid")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *     @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     * @param InviteeListRequest $request
     * @return ResponseInterface
     */
    public function inviteeList(InviteeListRequest $request): ResponseInterface
    {
        $result = QuestionnaireRefService::getWechatAgentInvitees(
            (string)$request->post('paper_yyid')
        );

        return $this->response->success($result);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/questionnaire/invite",
     *      tags={"问卷"},
     *      summary="邀请医生",
     *      description="问卷",
     *      operationId="questionnaireInvite",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  allOf={
     *                      @OA\Schema(ref="#/components/schemas/loginAuthenticate"),
     *                      @OA\Schema(
     *                          type="object",
     *                          @OA\Property(property="paper_yyid", type="string", description="问卷yyid"),
     *                          @OA\Property(property="invitee_openid", type="string", description="医生微信open-id"),
     *                          required={"paper_yyid", "invitee_openid"}
     *                      )
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     * @param InviteRequest $request
     * @return ResponseInterface
     */
    public function invite(InviteRequest $request): ResponseInterface
    {
        QuestionnaireRefService::wechatInviteDoctor(
            (string)$request->post('paper_yyid'),
            (string)$request->post('openid')
        );

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/questionnaire/get",
     *      tags={"问卷"},
     *      summary="获取问卷试题",
     *      description="问卷",
     *      operationId="questionnaireGet",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="openid", type="string", description="用户微信open-id"),
     *                  @OA\Property(property="paper_yyid", type="string", description="试卷yyid"),
     *                  @OA\Property(property="pos", type="integer", description="获取题目是第几题"),
     *                  @OA\Property(property="question_yyid", type="string", description="试题yyid"),
     *                  @OA\Property(property="request_id", type="string", description="请求id,来自start请求返回"),
     *                  @OA\Property(property="wechat_type", type="integer", description="微信类型: 1 代表 2医生 默认2"),
     *                  required={"openid", "paper_yyid", "question_yyid", "request_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="total", type="integer", description="该套问卷题目总数"),
     *                          @OA\Property(property="pos", type="integer", description="当前题目是第几题"),
     *                          @OA\Property(property="yyid", type="string", description="试题yyid"),
     *                          @OA\Property(property="name", type="string", description="试题名称"),
     *                          @OA\Property(property="type", type="integer", description="题目类型 1表示单选题 2表示多选题  3表示填写题"),
     *                          @OA\Property(
     *                              property="items",
     *                              type="array",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="yyid", type="string", description="选项yyid"),
     *                                  @OA\Property(property="name", type="string", description="选项名称"),
     *                              )
     *                          ),
     *                          @OA\Property(property="next_question_yyid", type="string", description="下一题目yyid"),
     *                           @OA\Property(property="is_skip", type="integer", description="是否允许跳过不答: 0不允许 1允许")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     * @param GetRequest $request
     * @return ResponseInterface
     */
    public function get(GetRequest $request): ResponseInterface
    {
        $pos = $request->post('pos', 1);
        $shareKey = $request->post('paper_yyid');
        if (!$shareKey) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_INVITED);
        }
        $share = Helper::decodeShareKey($shareKey);

        $info = QuestionnaireService::getQuestionItemInfo(
            $request->post('openid'),
            $share,
            $request->post('question_yyid'),
            $pos
        );
        return $this->response->success($info);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/questionnaire/doctor-list",
     *      tags={"问卷"},
     *      summary="问卷可邀请医生列表",
     *      description="问卷",
     *      operationId="questionnaireDoctorList",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  allOf={
     *                      @OA\Schema(ref="#/components/schemas/loginAuthenticate"),
     *                      @OA\Schema(
     *                          type="object",
     *                          @OA\Property(property="paper_yyid", type="string", description="问卷yyid"),
     *                          @OA\Property(property="keywords", type="string", description="医生微信名搜索条件"),
     *                          required={"paper_yyid"}
     *                      )
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="array",
     *                          @OA\Items(
     *                              type="object",
     *                              @OA\Property(
     *                                  property="is_followed",
     *                                  type="integer",
     *                                  description="是否已关注: 0未关注 1已关注"
     *                              ),
     *                              @OA\Property(property="name", type="string", description="微信名"),
     *                              @OA\Property(property="openid", type="string", description="微信open-id"),
     *                              @OA\Property(
     *                                  property="status",
     *                                  type="integer",
     *                                  description="状态: 0未关注 1已关注未邀请 2已邀请, 只有状态是2的记录可以发送邀请"
     *                              ),
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *     @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     * @param DoctorListRequest $request
     * @return ResponseInterface
     */
    public function doctorList(DoctorListRequest $request): ResponseInterface
    {
        //公众号端目前没有做分页， 先放一个大点的数
        [$page, $data] = QuestionnaireRefService::getWechatPaperAvailableWechatDoctor(
            (string)$request->post('paper_yyid'),
            (string)$request->post('keywords', ''),
            1,
            10000
        );

        return $this->response->success($data);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/questionnaire/answer",
     *      tags={"问卷"},
     *      summary="回答问题",
     *      description="问答问卷",
     *      operationId="questionnaireAnswer",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="openid", type="string", description="用户微信open-id"),
     *                  @OA\Property(property="paper_yyid", type="string", description="试卷yyid"),
     *                  @OA\Property(property="question_yyid", type="string", description="当前回答的试题yyid"),
     *                  @OA\Property(property="pos", type="integer", description="当前回答的试题位置"),
     *                  @OA\Property(
     *                      property="option_yyids",
     *                      type="array",
     *                      description="选择的选项yyid列表",
     *                      @OA\Items(type="string")
     *                  ),
     *                  @OA\Property(property="content", type="string", description="如果题目类型是填空题是填写"),
     *                  @OA\Property(property="request_id", type="string", description="请求id"),
     *                  @OA\Property(property="wechat_type", type="integer", description="微信类型: 1 代表 2医生 默认2"),
     *                  required={"openid", "paper_yyid", "question_yyid", "request_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="need_check", type="integer", description="是否需要校验对错: 1是 0否"),
     *                          @OA\Property(property="is_right", type="integer", description="回答是否正确: 1是 0否"),
     *                          @OA\Property(
     *                               property="right_options",
     *                              type="array",
     *                              description="正确选择的yyid",
     *                              @OA\Items(type="string")
     *                          ),
     *                          @OA\Property(property="explain", type="string", description="答案解析"),
     *                          @OA\Property(property="next_question_yyid", type="string", description="下一题yyid")
     *                      ),
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AnswerRequest $request
     * @return ResponseInterface
     */
    public function answer(AnswerRequest $request): ResponseInterface
    {
        $answerYYIDS = $request->post('option_yyids', []);
        if (is_string($answerYYIDS)) {
            $answerYYIDS = json_decode($answerYYIDS, true);
        }

        $shareKey = $request->post('paper_yyid');
        if (!$shareKey) {
            throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_INVITED);
        }
        $share = Helper::decodeShareKey($shareKey);
        $content = $params['content'] ?? '';

        $result = QuestionnaireService::answerQuestion(
            $request->post('request_id'),
            $request->post('openid'),
            $share,
            $request->post('question_yyid'),
            $request->post('pos', 1),
            $answerYYIDS,
            $content
        );
        return $this->response->success($result);
    }
}
