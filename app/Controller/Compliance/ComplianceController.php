<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Compliance;

use App\Controller\Controller;
use App\Model\Qa\Users;
use App\Request\Compliance\Compliance\AgreeRequest;
use App\Service\Compliance\Agree\AgreeService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;

class ComplianceController extends Controller
{
    /**
     * @Inject
     * @var AgreeService
     */
    private $agree;

    public function agree(AgreeRequest $request)
    {
        $examYyid = $request->post('exam_yyid', '');
        $signImg = $request->post('sign_img', '');

        /** @var Users $user */
        $user = Context::get('user');

        $pdf = $this->agree->process($examYyid, $user->yyid, $signImg);

        return $this->response->success(['pdf' => $pdf]);
    }
}
