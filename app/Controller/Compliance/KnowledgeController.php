<?php
declare(strict_types=1);


namespace App\Controller\Compliance;

use App\Controller\Controller;
use App\Request\Compliance\Knowledge\SearchKnowledgeRequest;
use App\Request\Compliance\Knowledge\TreeViewKnowledgeRequest;
use App\Request\Compliance\Questionnaire\AnswerRequest;
use App\Service\Compliance\KnowledgeService;
use Hyperf\Utils\Str;
use Psr\Http\Message\ResponseInterface;

class KnowledgeController extends Controller
{
    /**
     *
     * @OA\Post(
     *      path="/compliance/knowledges/top-dirs",
     *      tags={"知识库"},
     *      summary="顶级目录列表",
     *      description="顶级目录列表",
     *      operationId="KnowledgeTopDirList",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  required={}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                           @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="顶级目录列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="目录id"),
     *                                  @OA\Property(property="name", type="string", description="目录名")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function listTopDir(): ResponseInterface
    {
        $topDirs = KnowledgeService::topDirs();
        return $this->response->success([
            'list' => $topDirs
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/knowledges/search",
     *      tags={"知识库"},
     *      summary="搜索",
     *      description="搜索",
     *      operationId="KnowledgeSearch",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="keywords", type="string", description="搜索关键字"),
     *                  required={"keywords"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                               property="list",
     *                              type="array",
     *                              description="符合规则知识库文件id",
     *                              @OA\Items(
     *                                  type="object",
     *                                   @OA\Property(property="id", type="string", description="文件yyid"),
     *                                   @OA\Property(property="name", type="string", description="上传文件名")
     *                              )
     *                          )
     *                      ),
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param SearchKnowledgeRequest $request
     * @return ResponseInterface
     */
    public function search(SearchKnowledgeRequest $request): ResponseInterface
    {
        $keywords = $request->post('keywords');
        $knowledge = KnowledgeService::search($keywords);
        return $this->response->success([
            'list' => $knowledge
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/compliance/knowledges",
     *      tags={"知识库"},
     *      summary="知识库树状列表",
     *      description="知识库树状列表",
     *      operationId="KnowledgeTreeList",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="dir_id", type="integer", description="顶级目录id"),
     *                  required={}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                               property="list",
     *                              type="array",
     *                              description="知识库树状列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="节点id"),
     *                                  @OA\Property(property="name", type="string", description="节点名称"),
     *                                  @OA\Property(
     *                                      property="type",
     *                                      type="integer",
     *                                      description="节点类型： 1目录 2知识库资源文件"
     *                                  ),
     *                                  @OA\Property(
     *                                      property="children",
     *                                      type="array",
     *                                      description="子节点",
     *                                      @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(property="id", type="integer", description="节点id"),
     *                                          @OA\Property(property="name", type="string", description="节点名称"),
     *                                          @OA\Property(
     *                                              property="type",
     *                                              type="integer",
     *                                              description="节点类型： 1目录 2知识库资源文件"
     *                                          ),
     *                                          @OA\Property(
     *                                              property="children",
     *                                              type="array",
     *                                              description="子节点",
     *                                              @OA\Items(
     *                                                  type="object",
     *                                                  @OA\Property(property="id", type="integer", description="节点id"),
     *                                                  @OA\Property(
     *                                                      property="name",
     *                                                      type="string",
     *                                                      description="节点名称"
     *                                                  ),
     *                                                  @OA\Property(
     *                                                      property="type",
     *                                                      type="integer",
     *                                                      description="节点类型： 1目录 2知识库资源文件"
     *                                                  )
     *                                          )
     *                                          )
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                      ),
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param TreeViewKnowledgeRequest $request
     * @return ResponseInterface
     */
    public function list(TreeViewKnowledgeRequest $request): ResponseInterface
    {
        $dirId = $request->post('dir_id', 0);
        $view = KnowledgeService::treeView($dirId);return $this->response->success([
            'list' => $view
        ]);
    }
}
