<?php

/**
 *
 * @OA\Post(
 *      path="/doctor/info/show",
 *      tags={"医生"},
 *      summary="医生个人信息",
 *      description="医生个人信息",
 *      operationId="/info/show",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(
 *                              property="access",
 *                              type="integer",
 *                              description="Timestamp for previous time user accessed the site."),
 *                          @OA\Property(property="company_name", type="integer", description="公司名称"),
 *                          @OA\Property(property="company_yyid", type="string", description="公司yyid"),
 *                          @OA\Property(
 *                              property="is_agent",
 *                              type="integer",
 *                              description="0表示无  1表示已经填写信息未认证
 * 2 表示已经填写已认证 3表示申请认证中 4表示拒绝认证的"),
 *                          @OA\Property(
 *                              property="is_doctor",
 *                              type="integer",
 *                              description="0表示无  1表示已经填写信息未认证
 * 2 表示已经填写已认证 3表示取消认证的"),
 *                          @OA\Property(
 *                              property="last_client_ip",
 *                              type="string",
 *                              description="last sign-in ip address"),
 *                          @OA\Property(
 *                              property="login",
 *                              type="integer",
 *                              description="Timestamp for user’s last login."),
 *                          @OA\Property(property="mail", type="integer", description="User’s e-mail address."),
 *                          @OA\Property(
 *                              property="mail_verified",
 *                              type="string",
 *                              description="status is active(1) or blocked(0)"),
 *                          @OA\Property(property="mobile_num", type="integer", description="user accont mobile num"),
 *                          @OA\Property(
 *                              property="mobile_verified",
 *                              type="integer",
 *                              description="status is active(1) or blocked(0)"),
 *                          @OA\Property(property="name", type="integer", description="nick name."),
 *                          @OA\Property(property="pass", type="string", description="User’s password md5 (hashed)."),
 *                          @OA\Property(property="headimgurl", type="string", description="头像"),
 *                          @OA\Property(property="region_name", type="integer", description="地区"),
 *                          @OA\Property(property="region_yyid", type="integer", description="地区yyid"),
 *                          @OA\Property(property="tel_num", type="integer", description=""),
 *                          @OA\Property(property="tengqq", type="string", description=""),
 *                          @OA\Property(property="uid", type="integer", description="Primary Key: Unique user ID."),
 *                          @OA\Property(property="update_date", type="string", description=""),
 *                          @OA\Property(property="user_type", type="integer", description=""),
 *                          @OA\Property(property="v_date", type="integer", description="verified date"),
 *                          @OA\Property(
 *                              property="v_status",
 *                              type="integer",
 *                              description="Whether the user is active(1) or blocked(0) or reject(2)."),
 *                          @OA\Property(property="wechat", type="string", description="微信号"),
 *                          @OA\Property(property="weibo", type="integer", description="微博"),
 *                          @OA\Property(property="yyid", type="string", description="UNIQUE Key: uuid"),
 *                      ),
 *                      example={
 *                          "errcode":0,
 *                          "errmsg":"Success",
 *                          "data":{
 *                              "access": 1502950757,
 *                              "company_name": "优锐医药",
 *                              "company_yyid": "338EEF6193A011E7BAD8A1F3E2912E36",
 *                              "is_agent": 0,
 *                              "is_doctor": 1,
 *                              "login": 1502950757,
 *                              "mail": "",
 *                              "mail_verified": 0,
 *                              "mobile_num": "15154101765",
 *                              "mobile_verified": 1,
 *                              "name": "屈保云",
 *                              "headimgurl": "http://thirdwx.qlogo.cn/mmopen/7ylF4BEq4Ak9x11e6txS0qOSNSYnZ5bhw6xVg10iaEQufoMibfC3V9VOcX809icnUzHNPcJUWyJH5oqyo942nuiazWIAFUzST00S/132",
 *                              "pass": "",
 *                              "picture": "",
 *                              "region_yyid": "11734FE6943F11E7BAD8A1F3E2912E36",
 *                              "tel_num": "",
 *                              "tengqq": "",
 *                              "uid": 1,
 *                              "user_type": 2,
 *                              "v_date": 1502950757,
 *                              "v_status": 1,
 *                              "wechat": "屈保云",
 *                              "weibo": "",
 *                              "yyid": "FF7856E1831311E7A54A5254009FF7CE",
 *                              "region_name": "历下区,济南市,山东省",
 *                              "is_upload_idcard": 0,
 *                          }
 *                      }
 *                  )
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Doctor;

use App\Controller\Controller;
use App\Request\Doctor\InfoRequest;
use App\Service\Doctor\InfoService;
use Hyperf\Di\Annotation\Inject;

class InfoController extends Controller
{
    /**
     * @Inject()
     * @var InfoService
     */
    private $info;

    /**
     * @param InfoRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function show(InfoRequest $request)
    {
        $yyid = $request->post('user_yyid');

        //个人信息
        $personInfo = $this->info->getPersonInfo($yyid);

        return $this->response->success($personInfo);
    }
}
