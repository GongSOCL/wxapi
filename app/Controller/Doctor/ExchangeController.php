<?php

/**
 * @OA\Post(
 *      path="/doctor/exchange/index",
 *      tags={"医生"},
 *      summary="积分兑换的物品列表",
 *      description="积分兑换的物品列表",
 *      operationId="/exchange/index",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="page", type="integer", description="当前页码"),
 *                  @OA\Property(property="page_size", type="integer", description="每页显示条数"),
 *                  required={"user_yyid","user_token"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                 type="object",
 *                 @OA\Property(property="errcode", type="string", description="错误码"),
 *                 @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                 @OA\Property(
 *                     property="data",
 *                     type="object",
 *                     @OA\Property(
 *                         property="money_point",
 *                         type="integer",
 *                         description="我的消费积分"),
 *                         @OA\Property(property="list", type="object",
 *                             @OA\Property(property="goods_yyid", type="string", description="商品编号"),
 *                             @OA\Property(property="goods_type", type="integer", description="商品类型1现金 2实物 3虚拟"),
 *                             @OA\Property(property="goods_name", type="string", description="商品名称"),
 *                             @OA\Property(property="goods_pic", type="string", description="商品icon图片"),
 *                             @OA\Property(property="desc", type="string", description="商品描述",
 *                                   example="每100的服务积分可兑换10个消费积分"),
 *                             @OA\Property(property="money_point", type="integer", description="需要的消费积分"),
 *                             @OA\Property(property="service_point", type="integer", description="需要的服务积分"),
 *                         ),
 *                     ),
 *                 ),
 *            example={
 *                  "errcode": 0,
 *                  "errmsg": "Success",
 *                  "data": {
 *                      "list": {
 *                          {
 *                              "desc": "每100的服务积分可兑换10个消费积分",
 *                              "goods_name": "兑换现金",
 *                              "goods_pic": "http://coupons.quanduogo.com/ico.png",
 *                              "goods_yyid": "D97C834C8EB011E7A54A5254009FF71E",
 *                              "money_point": 60,
 *                              "service_point": 6
 *                          },
 *                          {
 *                              "desc": "每100的服务积分可兑换10个消费积分",
 *                              "goods_name": "兑换现金",
 *                              "goods_pic": "http://coupons.quanduogo.com/ico.png",
 *                              "goods_yyid": "D97C834C8EB011E7A54A5254009FF7CE",
 *                              "money_point": 100,
 *                              "service_point": 100
 *                          }
 *                      },
 *                      "page": 1,
 *                      "page_size": 10,
 *                      "total": 2,
 *                      "money_point": 6100
 *                  }
 *            },
 *         ),
 *      ),
 *     @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/doctor/exchange/create",
 *      tags={"医生"},
 *      summary="积分兑换",
 *      description="积分兑换",
 *      operationId="/exchange/create",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="goods_yyid", type="string", description="商品id"),
 *                  @OA\Property(property="goods_num", type="integer", description="数量"),
 *                  required={"user_yyid","user_token","goods_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *              mediaType="application/json",
 *              @OA\Schema(
 *                 type="object",
 *                 @OA\Property(property="errcode", type="string", description="错误码"),
 *                 @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                 @OA\Property(property="data",type="object",
 *                      @OA\Property(property="type", type="string", description="商品类型"),
 *                      @OA\Property(property="status", type="integer", description="状态"),
 *                      @OA\Property(property="desc", type="string", description="简述"),
 *                      @OA\Property(property="created_at", type="string", description="创建时间"),
 *                      @OA\Property(property="updated_at", type="string", description="更新时间"),
 *                      @OA\Property(property="money_point", type="integer", description="花费的消费积分"),
 *                      @OA\Property(property="service_point", type="integer", description="花费的服务积分"),
 *                 ),
 *             ),
 *            example={
 *                "errcode": 0,
 *                "errmsg": "Success",
 *                "data": {
 *                      "type": 2,
 *                      "status": 1,
 *                      "desc": "兑换现金",
 *                      "created_at": "",
 *                      "updated_at": "",
 *                      "money_point": 100,
 *                      "service_point": 100
 *                }
 *            },
 *         ),
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Doctor;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Repository\PointsExchangeRepository;
use App\Request\Doctor\ExchangeRequest;
use App\Request\Doctor\PointsExchangeRequest;
use App\Service\Exchange\ExchangeService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Arr;

class ExchangeController extends Controller
{
    /**
     * @Inject
     * @var ExchangeService
     */
    private $pointsExchange;

    /**
     * @Inject
     * @var PointsExchangeRepository
     */
    private $exchange;

    /**
     * @param ExchangeRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(ExchangeRequest $request)
    {
        $params = $request->validated();
        $page = (int) Arr::get($params, 'page', 1);
        $pageSize = (int) Arr::get($params, 'page_size', 10);
        $user = user();

        $exchangeList = $this->exchange->getGoodsList($user->uid, $page, $pageSize, false);

        return $this->response->success($exchangeList);
    }

    /**
     * @param PointsExchangeRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(PointsExchangeRequest $request)
    {
        $params = $request->validated();
        $goodsId = $params['goods_yyid'];
        $goodsNum = (int) Arr::get($params, 'goods_num', 1);
        $user = user();

        $resp = $this->pointsExchange->exchangeGoods($user, $goodsId, $goodsNum, DataStatus::WECHAT_DOCTOR);

        return $this->response->success($resp);
    }
}
