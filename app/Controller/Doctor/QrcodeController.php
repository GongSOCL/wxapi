<?php
/**
 *
 * @OA\Post(
 *      path="/doctor/qrcode/create",
 *      tags={"医生"},
 *      summary="二维码",
 *      description="二维码",
 *      operationId="/qrcode/create",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="invite_type", type="string", description="需要邀请的用户类型，1-代表 2-医生，现阶段默认只有邀请医生"),
 *                  required={"user_token","user_yyid","invite_type"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="url", type="string", description="二维码链接"),
 *                      ),
 *                      example={
 *                          "errcode": 0,
 *                          "errmsg": "Success",
 *                          "data": {"url": "https://pay.weixin.qq.com/wiki/doc/api/img/chapter6_1_1.png"}}
 *                  )
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Doctor;

use App\Controller\Controller;
use App\Request\Doctor\QrRequest;
use App\Service\Doctor\QrcodeService;
use Hyperf\Di\Annotation\Inject;

class QrcodeController extends Controller
{

    /**
     * @Inject()
     * @var QrcodeService
     */
    private $qrCode;


    /**
     * @param QrRequest $request
     * @return array|mixed
     */
    public function create(QrRequest $request)
    {
        //邀请人的id
        $inviteYyid = $request->post('user_yyid');
        //被邀请人的类型
        $inviteeType = $request->post('invite_type');

        $qrcode = $this->qrCode->getQrcode($inviteYyid, $inviteeType);

        return $this->response->success($qrcode);
    }
}
