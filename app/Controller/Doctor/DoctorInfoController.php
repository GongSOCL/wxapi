<?php
declare(strict_types=1);

namespace App\Controller\Doctor;

use App\Controller\Controller;
use App\Request\Doctor\EditDoctorInfoRequest;
use App\Request\Doctor\SearchValidMergeDoctorRequest;
use App\Request\Visit\AgentVisiDoctorListRequest;
use App\Service\Doctor\InfoService;
use Psr\Http\Message\ResponseInterface;

class DoctorInfoController extends Controller
{
    /**
     *
     * @OA\Post (
     *      path="/doctor/{id}/delete",
     *      tags={"群组医生"},
     *      summary="删除群组医生",
     *      description="删除群组医生",
     *      operationId="DeleteGroupDoctor",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function delete($id): ResponseInterface
    {
        $id = (int) $id;
        InfoService::deleteDoctor($id);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post (
     *      path="/doctor/{id}/info",
     *      tags={"群组医生"},
     *      summary="群组医生详情",
     *      description="群组医生详情",
     *      operationId="InfoGroupDoctor",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="详细信息",
     *                              @OA\Property(property="name", type="string", description="姓名"),
     *                              @OA\Property(property="sex", type="integer", description="性别: 1男 2女"),
     *                              @OA\Property(property="mobile", type="string", description="联系电话"),
     *                              @OA\Property(
     *                                  property="hospital",
     *                                  type="object",
     *                                  description="医院信息",
     *                                  @OA\Property(property="id", type="integer", description="关联医院id"),
     *                                  @OA\Property(property="name", type="string", description="关联医院名称")
     *                              ),
     *                              @OA\Property(
     *                                  property="depart",
     *                                  type="object",
     *                                  description="科室信息",
     *                                  @OA\Property(property="id", type="integer", description="关联科室id"),
     *                                  @OA\Property(property="name", type="string", description="关联科室名称")
     *                              ),
     *                              @OA\Property(property="job_title", type="string", description="级别"),
     *                              @OA\Property(property="position", type="string", description="职称"),
     *                              @OA\Property(
     *                                  property="field",
     *                                  type="object",
     *                                  description="擅长领域",
     *                                  @OA\Property(property="id", type="integer", description="擅长领域id"),
     *                                  @OA\Property(property="name", type="string", description="擅长领域名称")
     *                              ),
     *                              @OA\Property(property="field_id", type="integer", description="擅长领域"),
     *                              @OA\Property(property="clinic_type", type="integer", description="门诊类型:0未知 1专家2普通"),
     *                              @OA\Property(property="clinic_rota", type="string", description="门诊值班情况 原样返回")
     *                          ),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function info($id): ResponseInterface
    {
        $id = (int) $id;
        $info = InfoService::getInfo($id);
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/doctor/{id}/edit",
     *      tags={"群组医生"},
     *      summary="更新群组医生详情",
     *      description="更新群组医生详情",
     *      operationId="EditGroupDoctor",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="name", type="string", description="姓名"),
     *                  @OA\Property(property="sex", type="integer", description="性别: 1男 2女"),
     *                  @OA\Property(property="mobile", type="string", description="联系电话"),
     *                  @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                  @OA\Property(property="depart_id", type="integer", description="科室id"),
     *                  @OA\Property(property="job_title", type="string", description="级别"),
     *                  @OA\Property(property="position", type="string", description="职称"),
     *                  @OA\Property(property="field_id", type="integer", description="擅长领域"),
     *                  @OA\Property(property="clinic_type", type="integer", description="门诊类型:0未知 1专家2普通"),
     *                  @OA\Property(property="clinic_rota", type="string", description="门诊值班情况 原样返回"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "name",
     *                      "mobile",
     *                      "hospital_id",
     *                      "depart_id"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param EditDoctorInfoRequest $request
     * @return ResponseInterface
     */
    public function edit($id, EditDoctorInfoRequest $request): ResponseInterface
    {
        InfoService::editDoctorInfo(
            $id,
            (string) $request->post('name'),
            (string) $request->post('mobile'),
            (int) $request->post('hospital_id'),
            (int) $request->post('depart_id'),
            (int) $request->post('sex', 0),
            (string) $request->post('job_title', ''),
            (string) $request->post('position', ''),
            (int) $request->post('field_id', 0),
            (int) $request->post('clinic_type', 0),
            (string) $request->post('clinic_rota', '')
        );
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post (
     *      path="/doctor/{id}/valid-merge",
     *      tags={"群组医生"},
     *      summary="根据手机号查找可合并医生",
     *      description="根据手机号查找可合并医生",
     *      operationId="ValidMergeGroupDoctor",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="mobile", type="string", description="搜索手机号"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                      "mobile"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="详细信息",
     *                              @OA\Property(property="id", type="integer", description="医生id"),
     *                              @OA\Property(property="name", type="string", description="姓名"),
     *                              @OA\Property(property="sex", type="integer", description="性别: 1男 2女"),
     *                              @OA\Property(property="mobile", type="string", description="联系电话"),
     *                              @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                              @OA\Property(
     *                                  property="hospital",
     *                                  type="object",
     *                                  description="医院信息",
     *                                  @OA\Property(property="id", type="integer", description="关联医院id"),
     *                                  @OA\Property(property="name", type="string", description="关联医院名称")
     *                              ),
     *                              @OA\Property(
     *                                  property="depart",
     *                                  type="object",
     *                                  description="科室信息",
     *                                  @OA\Property(property="id", type="integer", description="关联科室id"),
     *                                  @OA\Property(property="name", type="string", description="关联科室名称")
     *                              ),
     *                              @OA\Property(property="job_title", type="string", description="级别"),
     *                              @OA\Property(property="position", type="string", description="职称"),
     *                              @OA\Property(property="field_id", type="integer", description="擅长领域"),
     *                              @OA\Property(
     *                                  property="field",
     *                                  type="object",
     *                                  description="擅长领域",
     *                                  @OA\Property(property="id", type="integer", description="擅长领域id"),
     *                                  @OA\Property(property="name", type="string", description="擅长领域名称")
     *                              ),
     *                              @OA\Property(property="clinic_type", type="integer", description="门诊类型:0未知 1专家2普通"),
     *                              @OA\Property(property="clinic_rota", type="string", description="门诊值班情况 原样返回")
     *                          ),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function validMerge($id, SearchValidMergeDoctorRequest $request): ResponseInterface
    {
        $id = (int) $id;
        $info = InfoService::getValidDoctorToMerge($id, (string) $request->post('mobile'));
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/doctor/{id}/merge/{from_doctor_id}",
     *      tags={"群组医生"},
     *      summary="合并医生",
     *      description="合并医生信息",
     *      operationId="MergeGroupDoctor",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  required={
     *                      "user_yyid",
     *                      "user_token",
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param $mergeId
     * @return ResponseInterface
     */
    public function merge($id, $mergeId): ResponseInterface
    {
        $id = (int) $id;
        $mergeId = (int) $mergeId;
        InfoService::mergeDoctor($id, $mergeId);
        return $this->response->success([]);
    }
}
