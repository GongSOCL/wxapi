<?php

/**
 *
 * @OA\Post(
 *      path="/user/idcard/upload",
 *      tags={"用户"},
 *      summary="上传身份证",
 *      description="上传身份证",
 *      operationId="/user/idcard/upload",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="photo", type="file", description="身份证照片"),
 *                  required={"user_token","user_yyid","photo"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="fileName", type="string", description="文件名称"),
 *                      )
 *                  )
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/user/idcard",
 *      tags={"用户"},
 *      summary="身份证信息提交",
 *      description="身份证信息提交",
 *      operationId="/user/idcard",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="idcard_front", type="string", description="身份证正面"),
 *                  @OA\Property(property="idcard_back", type="string", description="身份证反面"),
 *                  required={"user_token","user_yyid","idcard_front","idcard_back"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="yyid", type="string", description="用户yyid")
 *                      )
 *                  )
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Doctor;

use App\Controller\Controller;
use App\Request\Doctor\IdcardRequest;
use App\Request\Doctor\IdcardUploadRequest;
use App\Service\Doctor\UserIdentifyService;
use Hyperf\Di\Annotation\Inject;

class IdcardController extends Controller
{

    /**
     * @Inject()
     * @var UserIdentifyService
     */
    private $userIdentify;

    /**
     * @param IdcardUploadRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function upload(IdcardUploadRequest $request)
    {
        $yyid = $request->input('user_yyid');
        $file = $request->file('photo');

        $fileName = $this->userIdentify->uploadIdcard($yyid, $file);

        return $this->response->success(['fileName' => $fileName]);
    }


    /**
     * @param IdcardRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function idcard(IdcardRequest $request)
    {
        $front = $request->input('idcard_front');
        $back = $request->input('idcard_back');
        $yyid = $request->input('user_yyid');

        $result = $this->userIdentify->idcardSubmit($yyid, $front, $back);

        return $this->response->success($result);
    }
}
