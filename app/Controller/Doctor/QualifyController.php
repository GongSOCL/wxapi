<?php

/**
 *
 * @OA\Post(
 *      path="/doctor/authentication/index",
 *      tags={"医生"},
 *      summary="医生已填写的认证信息",
 *      description="医生已填写的认证信息",
 *      operationId="/authentication/index",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="true_name", type="string", description="姓名"),
 *                          @OA\Property(property="gender", type="integer", description="性别 1-男 2-女 0未知"),
 *                          @OA\Property(property="job_title", type="string", description="职称"),
 *                          @OA\Property(property="position", type="string", description="职位"),
 *                          @OA\Property(property="field_id", type="integer", description="擅长领域id"),
 *                          @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                          @OA\Property(property="depart_id", type="integer", description="部门id"),
 *                          @OA\Property(property="status", type="string", description="1已认证0未认证"),
 *                      ),
 *                  ),
 *              example={
 *                  "errcode":0,
 *                  "errmsg":"Success",
 *                  "data":{
 *                      "id": 30,
 *                      "yyid": "573907915AD511EBA5E75254008AFEE6",
 *                      "u_yyid": "FF7856E1831311E7A54A5254009FF7CE",
 *                      "true_name": "屈保云",
 *                      "gender": 1,
 *                      "job_title": "主治医师",
 *                      "position": "科室主任",
 *                      "field_id": "1",
 *                      "hospital_name": "河南大学第一附属医院",
 *                      "depart_id": "5662",
 *                      "status": 0
 *                  }
 *              },
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/doctor/authentication/show",
 *      tags={"医生"},
 *      summary="医生预留信息",
 *      description="医生预留信息",
 *      operationId="/authentication/show",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="mobile_num", type="string", description="手机号"),
 *                  required={"user_token","user_yyid","mobile_num"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="true_name", type="string", description="姓名"),
 *                          @OA\Property(property="gender", type="integer", description="性别 1-男 2-女 0未知"),
 *                          @OA\Property(property="hospital_yyid", type="string", description="医院id"),
 *                          @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                          @OA\Property(property="depart_id", type="integer", description="部门id"),
 *                          @OA\Property(property="depart_name", type="string", description="部门名称"),
 *                          @OA\Property(property="job_title", type="string", description="职称"),
 *                          @OA\Property(property="position", type="string", description="职位"),
 *                          @OA\Property(property="field_id", type="integer", description="擅长领域id"),
 *                      ),
 *                  ),
 *              example={
 *                  "errcode":0,
 *                  "errmsg":"Success",
 *                  "data":{
 *                      "true_name": "屈保云",
 *                      "gender": 1,
 *                      "hospital_yyid": "8994DF2B534411E79E6C68F728954D54",
 *                      "hospital_name": "河南大学附属医院",
 *                      "depart_id": 5662,
 *                      "depart_name": "血液科",
 *                      "job_title": "主治医师",
 *                      "position": "科室主任",
 *                      "field_id": 0
 *                  }
 *              },
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */


/**
 *
 * @OA\Post(
 *      path="/doctor/authentication/create",
 *      tags={"医生"},
 *      summary="医生认证",
 *      description="医生认证 错误码：700001 该医院不存在，700002 该医院不存在该科室，700003 提交认证资料失败，7000010 信息已提交，正在认证中",
 *      operationId="/authentication/create",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                  @OA\Property(property="gender", type="integer", description="性别1-男 2-女 0-未知"),
 *                  @OA\Property(property="hospital_yyid", type="string", description="所在医院"),
 *                  @OA\Property(property="depart_id", type="string", description="所在科室"),
 *                  @OA\Property(property="job_title", type="string", description="职称"),
 *                  @OA\Property(property="position", type="string", description="级别"),
 *                  @OA\Property(property="field_id", type="integer", description="擅长领域"),
 *                  required={"user_token","user_yyid","true_name","gender","hospital_yyid","depart_id","job_title","position","field_id"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                      ),
 *                  ),
 *              example={"errcode":0,"errmsg":"Success","data":{}},
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Doctor;

use App\Controller\Controller;
use App\Request\Doctor\InfoRequest;
use App\Request\Doctor\PreInfoRequest;
use App\Request\Doctor\QualifyRequest;
use App\Service\Doctor\AuthenticationService;
use Hyperf\Di\Annotation\Inject;

class QualifyController extends Controller
{
    /**
     * @Inject()
     * @var AuthenticationService
     */
    private $authentication;


    /**
     * 获取认证信息
     * @param InfoRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(InfoRequest $request)
    {
        $yyid = $request->post('user_yyid');

        $verifyInfo = $this->authentication->getVerifyInfo($yyid);

        return $this->response->success($verifyInfo);
    }

    /**
     * 获取预留信息
     * @param PreInfoRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function show(PreInfoRequest $request)
    {

        $yyid = $request->post('user_yyid');
        $num = $request->post('mobile_num');

        $preInfo = $this->authentication->getPreInfo($num);

        return $this->response->success($preInfo);
    }

    /**
     * 认证
     * @param QualifyRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(QualifyRequest $request)
    {
        $params = $request->validated();

        $qualifyResult = $this->authentication->qualify($params);

        return $this->response->success($qualifyResult);
    }
}
