<?php

/**
 *
 * @OA\Post(
 *      path="/doctor/invitation/index",
 *      tags={"医生"},
 *      summary="邀请列表",
 *      description="邀请列表",
 *      operationId="/invitation/index",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="page", type="integer", description="页码"),
 *                  @OA\Property(property="page_size", type="integer", description="每页显示条数"),
 *                  @OA\Property(property="user_type", type="integer", description="用户类型 0-全部 1-代表 2-医生"),
 *                  @OA\Property(property="is_auth", type="integer", description="是否认证 0-全部 1-未认证 2-已认证"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="总数"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="每页显示数量"),
 *                          @OA\Property(property="list", type="array", description="邀请列表",
 *                              type="object",
 *                              @OA\Property(property="user_yyid", type="string", description="被邀请人的id"),
 *                              @OA\Property(property="headimgurl", type="string", description="被邀请人头像"),
 *                              @OA\Property(property="nickname", type="string", description="被邀请人的姓名"),
 *                              @OA\Property(property="name", type="string", description="被邀请人的姓名"),
 *                              @OA\Property(property="user_type", type="integer", description="1-医生 2-代表"),
 *                              @OA\Property(property="is_auth", type="integer", description="状态1-未认证 2-已认证"),
 *                              @OA\Property(property="created_time", type="string", description="邀请时间"),
 *                          ),
 *                      ),
 *                  ),
 *              example={
 *                  "errcode": 0,
 *                  "errmsg": "Success",
 *                  "data": {
 *                      "page": 1,
 *                      "page_size": 10,
 *                      "total": 6,
 *                      "list": {
 *                          {
 *                              "user_yyid": "31297280881A11EAA957525400115EAF",
 *                              "headimgurl": "http://thirdwx.qlogo.cn/mmopen/uchmtWQh7iaq7N6oG3jubLVHLjricU3RsDuZLwdsSW1W4dyxXPDb5YpwnvxYc3sviaproXiasrgWCOEfdDdrW0wUe4OQ3rIOIfMV/132",
 *                              "nickname": "郑涛",
 *                              "name": "郑涛",
 *                              "user_type": 1,
 *                              "is_auth": 1,
 *                              "status": 2,
 *                              "created_at": "2020-11-03 17:33:01"
 *                           },
 *                          {
 *                              "user_yyid": "2FB3ED08885311EAA957525400115EAF",
 *                              "headimgurl": "http://thirdwx.qlogo.cn/mmopen/uchmtWQh7iaq7N6oG3jubLRlehiblY9wZB9p79HpkayTp7GL6oyaibpQM8iagzXLrpfeWtYjgh11qhFTia882t0I1dtwAtA2Xico9c/132",
 *                              "nickname": "齐梓良",
 *                              "name": "齐梓良",
 *                              "user_type": 1,
 *                              "is_auth": 1,
 *                              "status": 2,
 *                              "created_at": "2020-11-03 17:32:42"
 *                          },
 *                          {
 *                              "user_yyid": "FA549621883311EAA957525400115EAF",
 *                              "headimgurl": "http://thirdwx.qlogo.cn/mmopen/k8LkYfaDDibTzBrMyicZtrDAibxFd1m93gFcUUIW5Zt15rmibRk0K3sKichTZOrNHOyrUf89jjmHU2adFqdicWtbrnxQ/132",
 *                              "nickname": "阿宾",
 *                              "name": "阿宾",
 *                              "user_type": 1,
 *                              "is_auth": 1,
 *                              "status": 2,
 *                              "created_at": "2020-11-03 15:29:13"
 *                          }
 *                      }
 *                  }
 *              },
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */



/**
 *
 * @OA\Post(
 *      path="/doctor/invitation/create",
 *      tags={"医生"},
 *      summary="邀请医生（暂时用不到）",
 *      description="邀请医生（暂时用不到）",
 *      operationId="/invitation/create",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="邀请人yyid"),
 *                  @OA\Property(property="member_yyid", type="string", description="待加入列表的member_yyid"),
 *                  required={"user_token","user_yyid","member_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                      ),
 *                  ),
 *              example={
 *                  "errcode": 0,
 *                  "errmsg": "Success",
 *                  "data": {
 *                  }
 *              },
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Doctor;

use App\Controller\Controller;
use App\Request\Doctor\InvitationRequest;
use App\Request\Doctor\InviteDoctorRequest;
use App\Service\Doctor\DoctorInviterService;
use Hyperf\Di\Annotation\Inject;

class InvitationController extends Controller
{

    /**
     * @Inject()
     * @var DoctorInviterService
     */
    private $doctorInviter;

    /**
     * @param InvitationRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(InvitationRequest $request)
    {
        $yyid = $request->post('user_yyid');
        $params = $request->validated();

        $page = isset($params['page'])&&$params['page'] ? (int)$params['page'] : 1;
        $pageSize = isset($params['page_size'])&&$params['page_size'] ? (int)$params['page_size'] : 10;
        $type = isset($params['user_type'])&&$params['user_type'] ? (int)$params['user_type'] : 0;
        $isAuth = isset($params['is_auth'])&&$params['is_auth'] ? (int)$params['is_auth'] : 0;
        $invitationList = $this->doctorInviter->getInvitationList($yyid, $type, $isAuth, $page, $pageSize);

        return $this->response->success($invitationList);
    }


    /**
     * @param InviteDoctorRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(InviteDoctorRequest $request)
    {

        $inviterId = $request->post('user_yyid');
        $member_yyid = $request->post('member_yyid');

        $inviteDoctor = $this->doctorInviter->inviteDoctor($member_yyid, $inviterId);

        return $this->response->success(['id'=>$inviteDoctor]);
    }
}
