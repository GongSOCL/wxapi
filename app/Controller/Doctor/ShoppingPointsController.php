<?php

/**
 * @OA\Post(
 *      path="/doctor/shoppingPoints/index",
 *      tags={"医生"},
 *      summary="消费积分明细",
 *      description="消费积分明细",
 *      operationId="/shoppingPoints/index",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="type", type="integer", description="类型 1：发放，2：使用， 0或不传为全部"),
 *                  @OA\Property(property="page", type="integer", description="页码值"),
 *                  @OA\Property(property="page_size", type="integer", description="每页显示的条数"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="page", type="integer", description="页码值"),
 *                          @OA\Property(property="page_size", type="integer", description="每页显示的条数"),
 *                          @OA\Property(property="total", type="integer", description="总条数"),
 *                          @OA\Property(
 *                              property="list", type="object",
 *                              @OA\Property(
 *                              @OA\Property(property="detail_yyid", type="string", description="明细编号"),
 *                              @OA\Property(property="desc", type="string", description="描述"),
 *                              @OA\Property(property="point", type="integer", description="积分 类型为发放时返回负整数"),
 *                              @OA\Property(property="type", type="integer", description="类型 1：发放，2：使用"),
 *                              @OA\Property(property="status", type="integer", description="状态 1：申请中，2：已到账，3：已兑换"),
 *                              @OA\Property(property="date", type="string", description="时间：2020-12-03"),
 *                              @OA\Property(property="month", type="string", description="月份：12"),
 *                              @OA\Property(property="year", type="string", description="年份：2020"),
 *                         ),
 *                     ),
 *                 ),
 *             ),
 *             example={
 *                  "errcode": 0,
 *                  "errmsg": "Success",
 *                  "data": {
 *                      "list": {
 *                          {
 *                              "date": "2020-12-28 11:27:19",
 *                              "desc": "兑换现金",
 *                              "detail_yyid": "9780324848BC11EB9DD40800275A4B96",
 *                              "point": -200,
 *                              "type": 2,
 *                              "status": 1,
 *                              "year": "2020",
 *                              "month": "12"
 *                          },
 *                          {
 *                              "date": "2020-12-04 11:56:32",
 *                              "desc": "兑换现金",
 *                              "detail_yyid": "B28F00CF35E411EB94325254009FF7CE",
 *                              "point": -60,
 *                              "type": 2,
 *                              "status": 1,
 *                              "year": "2020",
 *                              "month": "12"
 *                          }
 *                      },
 *                      "page": 1,
 *                      "page_size": 2,
 *                      "total": 20
 *
 *                  }
 *             },
 *         ),
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Doctor;

use App\Controller\Controller;
use App\Model\Qa\Users;
use App\Repository\PointDetailRepository;
use App\Request\Doctor\PointsDetailstRequest;
use Hyperf\Utils\Context;

class ShoppingPointsController extends Controller
{
    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(
        PointsDetailstRequest $request,
        PointDetailRepository $detailRepository
    ) {
        $req = $request->validated();

        /** @var Users $user */
        $user = Context::get('user');

        $data = $detailRepository->fetchList(
            $user->uid,
            isset($req['page']) ? $req['page'] : 1,
            isset($req['page_size']) ? $req['page_size'] : 20,
            isset($req['type']) ? (int) $req['type'] : 0,
            false,
            false
        );
        $data['list'] = $data['details'];
        unset($data['details']);

        return $this->response->success($data);
    }
}
