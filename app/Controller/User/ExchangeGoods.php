<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\User;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Request\User\ExchangeGoodsRequest;
use App\Service\Exchange\ExchangeService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Arr;

/**
 * @OA\Post(path="/user/exchange/goods", tags={"个人中心"}, summary="使用积分兑换商品",
 *     description="使用积分兑换商品，错误码说明：5001 消费积分不足，5002 服务积分不足，5003 兑换失败",
 *     operationId="exchangeGoods",
 *     @OA\RequestBody(required=true, description="请求参数",
 *         @OA\MediaType(mediaType="multipart/form-data",
 *             @OA\Schema(type="object",
 *                 @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                 @OA\Property(property="user_token", type="string", description="用户token"),
 *                 @OA\Property(property="goods_yyid", type="string", description="商品编号"),
 *                 @OA\Property(property="goods_num", type="integer", description="商品数量"),
 *                 required={"user_yyid", "user_token", "goods_yyid"}
 *             ),
 *         ),
 *     ),
 *     @OA\Response(response="200", description="请求成功返回",
 *         @OA\MediaType(mediaType="application/json",
 *             @OA\Schema(type="object",
 *                 @OA\Property(property="errcode", type="string", description="错误码"),
 *                 @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                 @OA\Property(property="data", type="object", ),
 *             ), example={"errcode": 0, "errmsg": "Success", "data": {}},
 *         ),
 *     ),
 *     @OA\Response(response="4002", ref="#/components/responses/params_error")
 * )
 * @return \Psr\Http\Message\ResponseInterface
 */
class ExchangeGoods extends Controller
{
    /**
     * @Inject
     * @var ExchangeService
     */
    private $service;

    public function handler(ExchangeGoodsRequest $request): \Psr\Http\Message\ResponseInterface
    {
        $req = $request->validated();
        $goodsYyid = Arr::get($req, 'goods_yyid');
        $goodsNum = (int) Arr::get($req, 'goods_num', 0);
        $user = user();

        $data = $this->service->exchangeGoods($user, $goodsYyid, $goodsNum, DataStatus::WECHAT_AGENT);

        return $this->response->success($data);
    }
}
