<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\User;

use App\Controller\Controller;
use App\Repository\PointDetailRepository;
use App\Request\User\PointDetailsRequest;
use Hyperf\Utils\Arr;

/**
 * @codingStandardsIgnoreStart
 * @OA\Post(path="/user/point/details", tags={"个人中心"}, summary="积分明细", description="积分明细", operationId="pointDetails",
 *     @OA\RequestBody(
 *         required=true,
 *         description="请求参数",
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(type="object",
 *                 @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                 @OA\Property(property="user_token", type="string", description="用户token"),
 *                 @OA\Property(property="type", type="integer", description="类型 1：发放，2：使用， 0或不传为全部"),
 *                 @OA\Property(property="page", type="integer", description="页码值"),
 *                 @OA\Property(property="page_size", type="integer", description="每页显示的条数"),
 *                 @OA\Property(property="fetchService", type="bool", description="是否获取服务积分"),
 *                 required={"user_yyid", "user_token"}
 *             ),
 *         ),
 *     ),
 *     @OA\Response(response="200", description="请求成功返回",
 *         @OA\MediaType(mediaType="application/json",
 *             @OA\Schema(type="object",
 *                 @OA\Property(property="errcode", type="string", description="错误码"),
 *                 @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                 @OA\Property(property="data", type="object",
 *                     @OA\Property(property="page", type="integer", description="页码值"),
 *                     @OA\Property(property="page_size", type="integer", description="每页显示的条数"),
 *                     @OA\Property(property="total", type="integer", description="总条数"),
 *                     @OA\Property(property="details", type="object",
 *                         @OA\Property(
 *                             @OA\Property(property="detail_yyid", type="string", description="明细编号"),
 *                             @OA\Property(property="desc", type="string", description="描述"),
 *                             @OA\Property(property="point", type="integer", description="积分 类型为发放时返回负整数"),
 *                             @OA\Property(property="type", type="integer", description="类型 1：发放，2：使用"),
 *                             @OA\Property(property="status", type="integer", description="状态 1：申请中，2：已到账，3：已兑换"),
 *                             @OA\Property(property="date", type="string", description="时间：2020-12-03"),
 *                             @OA\Property(property="month", type="string", description="月份：12"),
 *                             @OA\Property(property="year_month", type="string", description="带年的月份：202012"),
 *                         ),
 *                     ),
 *                 ),
 *             ),
 *             example={"errcode": 0, "errmsg": "Success", "data": {"page": 1, "page_size": 20, "total": 2, "details": {{"detail_yyid": "DBB0498ED9F211E98C17525400115EAa", "desc": "好友注册奖励积分", "point": 500, "type": 1, "status": 0, "date": "2020-12-03", "month": "12", "year_month": "202012"}, {"detail_yyid": "DBB0498ED9F211E98C17525400115EAb", "desc": "兑换现金", "point": 300, "type": 2, "status": 0, "date": "2020-12-03", "month": "12", "year_month": "202012"}}}},
 *         ),
 *     ),
 *     @OA\Response(response="4002", ref="#/components/responses/params_error")
 * )
 * @codingStandardsIgnoreEnd
 * @return \Psr\Http\Message\ResponseInterface
 */
class PointDetails extends Controller
{
    public function handler(
        PointDetailsRequest $request,
        PointDetailRepository $detailRepository
    ): \Psr\Http\Message\ResponseInterface {
        $req = $request->validated();

        $user = user();

        $data = $detailRepository->fetchList(
            $user->uid,
            Arr::get($req, 'page', 1),
            Arr::get($req, 'page_size', 20),
            Arr::get($req, 'type', 0),
            Arr::get($req, 'fetchService', false),
            true
        );

        return $this->response->success($data);
    }
}
