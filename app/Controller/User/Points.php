<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\User;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Repository\PointsExchangeRepository;
use App\Repository\UserAssetRepository;
use App\Request\User\PointsRequest;
use App\Service\Points\PointsService;

/**
 * @codingStandardsIgnoreStart
 * @OA\Post(path="/user/points", tags={"个人中心"}, summary="我的积分", description="我的消费积分页数据", operationId="points",
 *     @OA\RequestBody(required=true, description="请求参数",
 *         @OA\MediaType(mediaType="multipart/form-data",
 *             @OA\Schema(type="object",
 *                 @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                 @OA\Property(property="user_token", type="string", description="用户token"),
 *                 required={"user_yyid", "user_token"}
 *             ),
 *         ),
 *     ),
 *     @OA\Response(response="200", description="请求成功返回",
 *         @OA\MediaType(mediaType="application/json",
 *             @OA\Schema(type="object",
 *                 @OA\Property(property="errcode", type="string", description="错误码"),
 *                 @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                 @OA\Property(property="data", type="object",
 *                     @OA\Property(property="money_point", type="string", description="我的消费积分"),
 *                     @OA\Property(property="goods", type="object",
 *                         @OA\Property(
 *                             @OA\Property(property="goods_yyid", type="string", description="商品编号"),
 *                             @OA\Property(property="goods_name", type="string", description="商品名称"),
 *                             @OA\Property(property="goods_pic", type="string", description="商品icon图片"),
 *                             @OA\Property(property="desc", type="string", description="商品描述", example="每100的服务积分可兑换10个消费积分"),
 *                             @OA\Property(property="money_point", type="string", description="需要的消费积分"),
 *                             @OA\Property(property="service_point", type="string", description="需要的服务积分"),
 *                         ),
 *                     ),
 *                 ),
 *             ),
 *             example={"errcode": 0,
 *     "errmsg": "Success",
 *     "data": {
 *     "money_point": 0,
 *      "goods":
 *     {
 *     {
 *     "goods_yyid": "D97C834C8EB011E7A54A5254009FF7CE",
 *      "goods_name": "兑换现金",
 *      "goods_pic": "http://coupons.quanduogo.com/ico.png",
 *      "desc": "每100的服务积分可兑换10个消费积分",
 *      "money_point": 10, "service_point": 100},
 *      {"goods_yyid": "D97C834C8EB011E7A54A5254009FF72E",
 *      "goods_name": "视频观看",
 *      "goods_pic": "http://coupons.quanduogo.com/ico.png",
 *      "desc": "每100的服务积分可兑换10个消费积分",
 *      "money_point": 6,
 *      "service_point": 60}}}},
 *         ),
 *     ),
 *     @OA\Response(response="4002", ref="#/components/responses/params_error")
 * )
 * @codingStandardsIgnoreEnd
 * @return \Psr\Http\Message\ResponseInterface
 */
class Points extends Controller
{
    public function handler(
        PointsRequest $request,
        UserAssetRepository $assetRepository,
        PointsExchangeRepository $exchangeRepository
    ): \Psr\Http\Message\ResponseInterface {
        $request->validated();
        $user = Helper::getLoginUser();
        //通过uid获取用户的消费积分值
        [$servicePoint, $moneyPoint] = PointsService::getUserPoints();

        //获取积分兑换列表
        $goods = $exchangeRepository->getGoodsList($user->uid, 1, 100, true);

        $resp = [
            'goods' => $goods['list'],
            'money_point' => $servicePoint,
            'service_point' => $moneyPoint,
        ];

        return $this->response->success($resp);
    }
}
