<?php


namespace App\Controller\User;

use App\Model\Qa\Users;
use App\Model\Qa\WechatUser;
use App\Request\User\AuthRequest;
use App\Service\User\UserService;
use Psr\Http\Message\ResponseInterface;

class AuthController extends \App\Controller\Controller
{
    /**
     *
     * @OA\Post (
     *      path="/agent/auth",
     *      tags={"用户"},
     *      summary="代表手机号微信登陆",
     *      description="代表手机号微信登陆",
     *      operationId="AgentMobileWechatAuth",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="mobile", type="string", description="手机号"),
     *                  @OA\Property(property="openid", type="string", description="openid"),
     *                  required={"mobile", "openid"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function auth(AuthRequest $request): ResponseInterface
    {
        /**
         * @var Users $user
         * @var WechatUser $wechat
         */
        [$user, $wechat]  = UserService::authByMobileAndWechat(
            (string)$request->post('mobile'),
            (string)$request->post('openid')
        );
        return $this->response->success([
            'uid' => $user->uid,
            'wechat_id' => $wechat->id
        ]);
    }
}
