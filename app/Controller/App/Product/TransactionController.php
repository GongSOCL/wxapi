<?php
declare(strict_types=1);

namespace App\Controller\App\Product;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Transaction\MsgDailyTransactionRequest;
use App\Service\Common\ServeService;
use App\Service\Transaction\TransactionService;
use DateTime;
use Exception;
use Psr\Http\Message\ResponseInterface;

class TransactionController extends Controller
{

    /**
     *
     * @OA\Get (
     *      path="/app/transaction/products",
     *      tags={"app-流向"},
     *      summary="流向服务产品列表(服务月报)",
     *      description="流向服务产品列表(服务月报)",
     *      operationId="AppTransactionProductList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="服务产品列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="服务产品id"),
     *                                  @OA\Property(property="name", type="string", description="服务产品名称"),
     *                                  @OA\Property(property="last_serve_month", type="string", description="最后服务月份"),
     *                                  @OA\Property(
     *                                      property="current_month",
     *                                      type="object",
     *                                      description="当月数据",
     *                                      @OA\Property(property="target", type="integer", description="潜力"),
     *                                      @OA\Property(
     *                                          property="mom",
     *                                          type="number",
     *                                          format="float",
     *                                          description="环比"),
     *                                      @OA\Property(
     *                                          property="yoy",
     *                                          type="number",
     *                                          format="float",
     *                                          description="同比"),
     *                                      @OA\Property(property="total", type="integer", description="实际")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="last_three_month",
     *                                      type="object",
     *                                      description="过去3月内数据",
     *                                      @OA\Property(property="target", type="integer", description="潜力"),
     *                                      @OA\Property(
     *                                          property="mom",
     *                                          type="number",
     *                                          format="float",
     *                                          description="环比"),
     *                                      @OA\Property(
     *                                          property="yoy",
     *                                          type="number",
     *                                          format="float",
     *                                          description="同比"),
     *                                      @OA\Property(
     *                                          property="total",
     *                                          type="integer",
     *                                          description="实际"),
     *                                      @OA\Property(
     *                                          property="title",
     *                                          type="string",
     *                                          example="12月-02月数据",
     *                                          description="标题")
     *                                  ),
     *                              )
     *                          ),
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function serveProductTransactionList(): ResponseInterface
    {
        $list = TransactionService::getServeProductTransactionSummary();

        return $this->response->success([
            'list' => $list
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/transaction/products/{id}",
     *      tags={"app-流向"},
     *      summary="产品流向统计数据",
     *      description="产品流向统计数据",
     *      operationId="AppTransactionProductInfo",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="产品id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="产品流向统计数据",
     *                              @OA\Property(property="name", type="string", description="服务产品名称"),
     *                              @OA\Property(property="last_serve_month", type="string", description="最后服务月份"),
     *                              @OA\Property(
     *                                  property="current_month",
     *                                  type="object",
     *                                  description="当月数据",
     *                                  @OA\Property(property="target", type="integer", description="潜力"),
     *                                  @OA\Property(property="mom", type="number", format="float", description="环比"),
     *                                  @OA\Property(property="yoy", type="number", format="float", description="同比"),
     *                                  @OA\Property(property="total", type="integer", description="实际")
     *                               ),
     *                               @OA\Property(
     *                                      property="last_three_month",
     *                                      type="object",
     *                                      description="过去3月内数据",
     *                                      @OA\Property(property="target", type="integer", description="潜力"),
     *                                      @OA\Property(
     *                                          property="mom",
     *                                          type="number",
     *                                          format="float",
     *                                          description="环比"),
     *                                      @OA\Property(
     *                                          property="yoy",
     *                                          type="number",
     *                                          format="float",
     *                                          description="同比"),
     *                                      @OA\Property(property="total", type="integer", description="实际"),
     *                                      @OA\Property(
     *                                          property="title",
     *                                          type="string",
     *                                          example="12月-02月数据",
     *                                          description="标题")
     *                               ),
     *                              @OA\Property(
     *                                  property="chart",
     *                                  type="object",
     *                                  description="图表数据(过去6个月数据)",
     *                                  @OA\Property(
     *                                      property="rows",
     *                                      type="array",
     *                                      description="数据列",
     *                                      @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(
     *                                              property="date",
     *                                              type="string",
     *                                              example="19/11",
     *                                              description="日期"),
     *                                          @OA\Property(property="num", type="integer", description="数量")
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function productTransactionDetail($id): ResponseInterface
    {
        $id = (int) $id;
        $info = TransactionService::getProductTransactionDetail($id);
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/transaction/products/{id}/hospital-rank",
     *      tags={"app-流向"},
     *      summary="产品流向医院排名",
     *      description="产品流向医院排名",
     *      operationId="AppTransactionProductHospitalRank",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="产品id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页,默认1",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量,默认10",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="医院排名列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="医院id"),
     *                                  @OA\Property(property="name", type="string", description="医院名称"),
     *                                  @OA\Property(property="sum", type="integer", description="总流向"),
     *                                  @OA\Property(property="rate", type="number", description="排名"),
     *                              )
     *
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function hospitalRank($id): ResponseInterface
    {
        $id = (int)$id;
        $current = (int)$this->request->query('current', 1);
        $limit = (int)$this->request->query('limit', 10);

        [$page, $list] = TransactionService::getRankHospitals($id, $current, $limit);
        return $this->response->success([
            'list' => $list,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/transaction/products/{id}/month",
     *      tags={"app-流向"},
     *      summary="流向月份列表",
     *      description="流向月份列表",
     *      operationId="AppTransactionProductMonthLists",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="产品id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="hospital_id",
     *          in="query",
     *          description="医院id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="流向月份列表",
     *                              @OA\Items(
     *                                  @OA\Property(
     *                                      property="month",
     *                                      type="string",
     *                                      example="2021-01",
     *                                      description="月份"),
     *                                  @OA\Property(property="is_trend", type="integer", description="是否结算"),
     *                              )
     *
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function months($id): ResponseInterface
    {
        $id = (int)$id;
        $list = TransactionService::monthList($id);
        return $this->response->success([
            'list' => $list
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/transaction/products/{id}/details",
     *      tags={"app-流向"},
     *      summary="月度流向详情",
     *      description="月度流向详情",
     *      operationId="AppTransactionProductMonthDetails",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="产品id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="hospital_id",
     *          in="query",
     *          description="医院id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="month",
     *          in="query",
     *          description="月度",
     *          required=false,
     *          example="2021-01",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="流向月份列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="month", type="string", example="01", description="月份"),
     *                                  @OA\Property(property="total", type="integer", description="总计"),
     *                                  @OA\Property(
     *                                      property="details",
     *                                      type="array",
     *                                      description="月度详情",
     *                                      @OA\Items(
     *                                          @OA\Property(
     *                                              property="month_date",
     *                                              type="string",
     *                                              example="07月26日",
     *                                              description="月份"),
     *                                          @OA\Property(
     *                                              property="hospital_name",
     *                                              type="string",
     *                                              description="医院名称"),
     *                                          @OA\Property(property="total", type="integer", description="流向数量")
     *                                      )
     *                                  ),
     *                              )
     *
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function details($id): ResponseInterface
    {
        $id = (int)$id;
        $hospitalId = (int)$this->request->query('hospital_id', 0);
        $month = (string)$this->request->query('month');
        if ($month) {
            $month = DateTime::createFromFormat("Y-m", $month)->format('Y-m');
        }
        [$list, $product, $hospital] = TransactionService::getDetails($id, $hospitalId, $month);

        return $this->response->success([
            'list' => $list,
            'product' => $product,
            'hospital' => $hospital
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/transaction/products/{id}/hospitals",
     *      tags={"app-流向"},
     *      summary="流向产品涉及服务医院搜索",
     *      description="流向产品涉及服务医院搜索",
     *      operationId="AppTransactionProductHospitalSearch",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="产品id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="医院搜索列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="医院id"),
     *                                  @OA\Property(property="name", type="string", description="医院名称"),
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function hospitalSearch($id): ResponseInterface
    {
        $id = (int)$id;
        $keywords = (string)$this->request->query('keywords', '');
        $list = TransactionService::getTransHospitals($id, $keywords);
        return $this->response->success([
              'list' => $list
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/transaction/msg-daily",
     *      tags={"app-流向"},
     *      summary="用户消息流向详情",
     *      description="用户消息流向详情",
     *      operationId="AppDailyMsgTransaction",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="msg_date",
     *          in="query",
     *          description="消息日期, 格式是Y-m-d如2022-01-25",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="order_date",
     *          in="query",
     *          description="流向日期, 格式是Y-m-d如2022-01-25",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="msg_id",
     *          in="query",
     *          description="来源消息id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="流向列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="product_id", type="integer", description="产品id"),
     *                                  @OA\Property(property="name", type="string", description="产品名称"),
     *                                  @OA\Property(
     *                                      property="date",
     *                                      type="string",
     *                                      description="流向日期,格式是Y-m-d如2022-01-25"),
     *                                  @OA\Property(
     *                                      property="detail",
     *                                      type="array",
     *                                      @OA\Items(
     *                                          @OA\Property(property="hospital", type="string", description="医院名称"),
     *                                          @OA\Property(property="order_num", type="integer", description="流向数量")
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function dailyTransaction(MsgDailyTransactionRequest $request): ResponseInterface
    {
        $msgDate = (string)$request->query('msg_date');
        $orderDate = (string)$request->query('order_date');
        $msgId = (int)$request->query('msg_id');
        $msgDate = Helper::formatInputDate($msgDate, 'Y-m-d');
        $orderDate = Helper::formatInputDate($orderDate, 'Y-m-d');
        $list = TransactionService::getDailyMsgTransaction(
            $msgDate,
            $orderDate,
            $msgId
        );

        return $this->response->success([
            'list' => $list
        ]);
    }
}
