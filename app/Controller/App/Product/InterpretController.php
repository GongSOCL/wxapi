<?php
declare(strict_types=1);

namespace App\Controller\App\Product;

use App\Controller\Controller;
use App\Service\Common\InterpretService;
use App\Service\User\UserService;
use Exception;
use Psr\Http\Message\ResponseInterface;

class InterpretController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/interprets/{id}",
     *      tags={"app-药品"},
     *      summary="文献详情",
     *      description="文献详情",
     *      operationId="AppInterpretDetail",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="文献id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="产品文献详情",
     *                              @OA\Property(property="title", type="string", description="文献标题"),
     *                              @OA\Property(property="sub_title", type="string", description="文献作者"),
     *                              @OA\Property(property="content", type="string", description="文献内容"),
     *                              @OA\Property(property="read_num", type="integer", description="阅读数量")
     *                          ),
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function info($id): ResponseInterface
    {
        $id = (int) $id;
        $info = InterpretService::info($id);
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/interprets/{id}/comments",
     *      tags={"app-药品"},
     *      summary="文献评论列表",
     *      description="文献评论列表",
     *      operationId="AppInterpretCommentList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="文献id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="total", type="integer", description="评论总数量"),
     *                          @OA\Property(
     *                              property="comments",
     *                              type="array",
     *                              description="评论列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="评论id"),
     *                                  @OA\Property(property="comment", type="string", description="评论内容"),
     *                                  @OA\Property(property="avatar", type="string", description="评论人头像"),
     *                                  @OA\Property(property="name", type="string", description="评论人名称"),
     *                                  @OA\Property(property="comment_time", type="string", description="评论时间"),
     *                                  @OA\Property(property="is_delete", type="integer", description="是否可删除:0否 1是"),
     *                                  @OA\Property(
     *                                      property="children",
     *                                      type="array",
     *                                      description="子评论",
     *                                      @OA\Items(
     *                                          @OA\Property(property="comment", type="string", description="评论内容"),
     *                                          @OA\Property(property="id", type="integer", description="评论id"),
     *                                          @OA\Property(property="avatar", type="string", description="评论人头像"),
     *                                          @OA\Property(property="name", type="string", description="评论人名称"),
     *                                          @OA\Property(
     *                                              property="comment_time",
     *                                              type="string",
     *                                              description="评论时间"),
     *                                          @OA\Property(
     *                                              property="is_delete",
     *                                              type="integer",
     *                                              description="是否可删除:0否 1是"),
     *                                      )
     *                                  )
     *                              )
     *                          ),
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function comment($id): ResponseInterface
    {
        UserService::checkMobileBind();

        [$total, $comments] = InterpretService::getInterpretComments($id);
        return $this->response->success([
            'total' => $total,
            'comments' => $comments
        ]);
    }

    /**
     *
     * @OA\Put (
     *      path="/app/interprets/{id}/view",
     *      tags={"app-药品"},
     *      summary="浏览文献",
     *      description="浏览文献",
     *      operationId="AppInterpretView",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="文献id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function view($id): ResponseInterface
    {
        InterpretService::viewInterpret($id);

        return $this->response->success([]);
    }
}
