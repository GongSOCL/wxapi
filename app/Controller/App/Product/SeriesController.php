<?php
declare(strict_types=1);

namespace App\Controller\App\Product;

use App\Controller\Controller;
use App\Service\Common\ProductService;
use Exception;
use Psr\Http\Message\ResponseInterface;

class SeriesController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/series/{id}",
     *      tags={"app-药品"},
     *      summary="系列药品详情",
     *      description="系列药品详情",
     *      operationId="AppSeriesInfo",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="药品系列id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="系列药品详情",
     *                              @OA\Property(property="approveal", type="strimng", description="批准文号"),
     *                              @OA\Property(property="type", type="string", description="产品类别"),
     *                              @OA\Property(property="manufacturer", type="string", description="生产企业"),
     *                              @OA\Property(property="sub_series_name", type="string", description="系列名副名称"),
     *                              @OA\Property(property="name", type="string", description="系列名"),
     *                              @OA\Property(property="series_name_en", type="string", description="子系列名"),
     *                              @OA\Property(property="pic", type="string", description="系列药品图片"),
     *                              @OA\Property(property="list_pic", type="string", description="系列药品缩略图(小)"),
     *                              @OA\Property(property="desc", type="string", description="系列药品简介"),
     *                              @OA\Property(property="series_direct", type="string", description="产品应用"),
     *                              @OA\Property(property="market_analyze", type="string", description="市场分析"),
     *                              @OA\Property(property="investment_scope", type="string", description="招商范围"),
     *                              @OA\Property(property="zone_price", type="string", description="区域价格"),
     *                              @OA\Property(property="platform_policy", type="string", description="平台政策"),
     *                              @OA\Property(property="settlement_rules", type="string", description="结算细则")
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function info($id): ResponseInterface
    {
        $id = (int) $id;
        $info = ProductService::getSeriesInfo($id);
        return $this->response->success([
            'info' => $info
        ]);
    }
}
