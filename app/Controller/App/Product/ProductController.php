<?php
declare(strict_types=1);

namespace App\Controller\App\Product;

use App\Controller\Controller;
use App\Service\Common\ProductService;
use Exception;
use Psr\Http\Message\ResponseInterface;

class ProductController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/products",
     *      tags={"app-药品"},
     *      summary="首页药品列表",
     *      description="首页药品列表",
     *      operationId="AppProductList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="产品列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="药品id"),
     *                                  @OA\Property(property="name", type="string", description="药品名称"),
     *                                  @OA\Property(property="intro", type="string", description="药品简介"),
     *                                  @OA\Property(property="series_id", type="integer", description="系列id"),
     *                                  @OA\Property(property="pic", type="string", description="图片url"),
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function list(): ResponseInterface
    {
        $list = ProductService::getAppProduct();
        return $this->response->success([
            'list' => $list
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/products/{id}/manual",
     *      tags={"app-药品"},
     *      summary="产品说明书",
     *      description="产品说明书",
     *      operationId="AppProductManual",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="药品id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="product_name", type="string", description="产品名称"),
     *                          @OA\Property(
     *                              property="manuals",
     *                              type="array",
     *                              description="产品说明书",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="name", type="string", description="项目名称"),
     *                                  @OA\Property(property="content", type="string", description="项目内容")
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function manual($id): ResponseInterface
    {
        [$name, $manuals] = ProductService::getProductManual($id);
        return $this->response->success([
            'product_name' => $name,
            'manuals' => $manuals
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/products/{id}/interpret",
     *      tags={"app-药品"},
     *      summary="产品文献列表",
     *      description="产品文献列表",
     *      operationId="AppProductInterpretList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="药品id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页码: 默认1",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页数量，默认10",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="产品文献列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="文献id"),
     *                                  @OA\Property(property="title", type="string", description="文献标题"),
     *                                  @OA\Property(property="create_time", type="string", description="创建时间"),
     *                                  @OA\Property(property="read_num", type="integer", description="阅读数量")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function interpretList($id): ResponseInterface
    {
        $current = (int) $this->request->query('current', 1);
        $limit = (int) $this->request->query('limit');
        $id = (int) $id;
        [$page, $data] = ProductService::getProductInterprets($id, $current, $limit);

        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }
}
