<?php
declare(strict_types=1);

namespace App\Controller\App\Product;

use App\Controller\Controller;
use App\Service\Common\AdaptionService;
use Exception;
use Psr\Http\Message\ResponseInterface;

class AdaptionController extends Controller
{

    /**
     *
     * @OA\Get (
     *      path="/app/adaptions/field",
     *      tags={"app-治疗领域及适应症"},
     *      summary="治疗领域列表",
     *      description="治疗领域列表",
     *      operationId="AppFieldList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="治疗领域列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="治疗领域id"),
     *                                  @OA\Property(property="name", type="string", description="治疗领域名称")
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function fieldList(): ResponseInterface
    {
        [$page, $data] = AdaptionService::listFields(
            (string) $this->request->query('keywords'),
            (int) $this->request->query('current', 1),
            (int) $this->request->query('limit', 10)
        );
        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/adaptions/adaption",
     *      tags={"app-治疗领域及适应症"},
     *      summary="适应症列表",
     *      description="适应症列表",
     *      operationId="AppAdaptionList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="适应症列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="适应症id"),
     *                                  @OA\Property(property="name", type="string", description="适应症名称")
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function adaptionList()
    {
        [$page, $data] = AdaptionService::listAdaption(
            (string) $this->request->query('keywords'),
            (int) $this->request->query('current', 1),
            (int) $this->request->query('limit', 10)
        );

        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }
}
