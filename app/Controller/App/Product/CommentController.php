<?php
declare(strict_types=1);

namespace App\Controller\App\Product;

use App\Controller\Controller;
use App\Request\App\Product\AddCommentRequest;
use App\Service\Common\CommentService;
use Exception;
use Psr\Http\Message\ResponseInterface;

class CommentController extends Controller
{
    /**
     *
     * @OA\Delete (
     *      path="/app/comments/{id}",
     *      tags={"app-药品"},
     *      summary="删除评论",
     *      description="删除评论",
     *      operationId="AppCommentDelete",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="评论id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function delete($id): ResponseInterface
    {
        $id = (int) $id;
        CommentService::deleteComment($id);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Put (
     *      path="/app/comments",
     *      tags={"app-药品"},
     *      summary="添加评论",
     *      description="添加评论",
     *      operationId="AppCommentAdd",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="resource_id", type="integer", description="资源id, 这里指文献id"),
     *                  @OA\Property(property="parent_comment_id", type="integer", description="父级评论id,默认0"),
     *                  @OA\Property(property="comment", type="string", description="评论内容"),
     *                  required={"resource_id", "comment"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="新加评论id")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function add(AddCommentRequest $request): ResponseInterface
    {
        $resourceId = (int) $request->post('resource_id');
        $parentCommentId = (int) $request->post('parent_comment_id', 0);
        $comment = (string) $request->post('comment');
        $cm = CommentService::addInterceptComment($resourceId, $comment, $parentCommentId);

        return $this->response->success([
            'id' => $cm->id
        ]);
    }
}
