<?php

/**
 *
 * @OA\Post (
 *      path="/app/workgroup/vote/create",
 *      tags={"app-代表的工作群组"},
 *      summary="发起投票",
 *      description="发起投票",
 *      operationId="/app/workgroup/vote/create",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="tax",
 *          in="query",
 *          description="贡献比例",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="vote_id", type="integer", description="投票id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/workgroup/vote/detail",
 *      tags={"app-代表的工作群组"},
 *      summary="投票详情",
 *      description="投票详情",
 *      operationId="/app/workgroup/vote/detail",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="vote_id",
 *          in="query",
 *          description="投票id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="agree_rate", type="string", description="同意比例"),
 *                          @OA\Property(property="disagree_rate", type="string", description="不同意比例"),
 *                          @OA\Property(property="abstain_rate", type="string", description="未投票比例")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/workgroup/vote",
 *      tags={"app-代表的工作群组"},
 *      summary="投票",
 *      description="投票",
 *      operationId="/app/workgroup/vote",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="vote_id",
 *          in="query",
 *          description="投票id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="agree",
 *          in="query",
 *          description="是否同意 1:同意,2:不同意",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="vote_detail_yyid", type="string", description="投票详情编号"),
 *                          @OA\Property(property="member_count", type="integer", description="总人数"),
 *                          @OA\Property(property="vote_count", type="integer", description="已投票人数"),
 *                          @OA\Property(
 *                              property="vote_status",
 *                              type="integer",
 *                              description="投票状态 0表示进行中  1表示已通过
 * 2表示未通过  3表示已过期  4表示手动关闭")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/workgroup/vote/cancel",
 *      tags={"app-代表的工作群组"},
 *      summary="终止投票",
 *      description="终止投票",
 *      operationId="/app/workgroup/vote/cancel",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="vote_id",
 *          in="query",
 *          description="投票id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="vote_id", type="integer", description="投票编号")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Group\Work;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Group\Work\VoteCancelRequest;
use App\Request\App\Group\Work\VoteCreateRequest;
use App\Request\App\Group\Work\VoteDetailRequest;
use App\Request\App\Group\Work\VoteRequest;
use App\Service\Group\Work\VoteGroupService;
use Hyperf\Di\Annotation\Inject;

class VoteGroupController extends Controller
{
    /**
     * @Inject()
     * @var VoteGroupService
     */
    private $voteGroupService;

    /**
     * @param VoteCreateRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(VoteCreateRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $tax = $request->input('tax');

        $data = $this->voteGroupService->createVote($userId, $groupId, $tax);
        return $this->response->success($data);
    }

    /**
     * @param VoteDetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function detail(VoteDetailRequest $request)
    {
        $groupId = $request->input('group_id');
        $voteId = $request->input('vote_id');

        $data = $this->voteGroupService->getVoteDetail($groupId, $voteId);
        return $this->response->success($data);
    }

    /**
     * @param VoteRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function vote(VoteRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $voteId = $request->input('vote_id');
        $agree = $request->input('agree');

        $result = $this->voteGroupService->memberVote($userId, $groupId, $voteId, $agree);
        return $this->response->success($result);
    }

    /**
     * @param VoteCancelRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function cancel(VoteCancelRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $voteId = $request->input('vote_id');

        $result = $this->voteGroupService->cancelVote($userId, $voteId);
        return $this->response->success($result);
    }
}
