<?php

/**
 *
 * @OA\Get (
 *      path="/app/workgroup/search",
 *      tags={"app-代表的工作群组"},
 *      summary="查找群组（群组列表）",
 *      description="查找群组（群组列表）",
 *      operationId="/app/workgroup/search",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="key_word",
 *          in="query",
 *          description="关键字",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="当前页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面容量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              description="群组信息",
 *                              @OA\Property(property="group_id", type="integer", description="群组id"),
 *                              @OA\Property(property="group_name", type="string", description="群组名称"),
 *                              @OA\Property(property="notice", type="string", description="群公告"),
 *                              @OA\Property(property="series_id", type="integer", description="药品id"),
 *                              @OA\Property(property="series_name", type="string", description="药品名称")
 *                          )
 *                      ),
 *                      @OA\Property(property="total", type="integer", description="总数量"),
 *                      @OA\Property(property="page", type="integer", description="当前页码"),
 *                      @OA\Property(property="page_size", type="integer", description="页面容量")
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/workgroup/detail",
 *      tags={"app-代表的工作群组"},
 *      summary="群组详情",
 *      description="群组详情",
 *      operationId="/app/workgroup/detail",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="is_leader", type="integer", description="是否群主 0：否，1：是"),
 *                          @OA\Property(property="group_id", type="string", description="群组编号"),
 *                          @OA\Property(property="group_name", type="string", description="群组名称"),
 *                          @OA\Property(property="series_id", type="string", description="负责的产品编号"),
 *                          @OA\Property(property="series_name", type="string", description="负责的产品名"),
 *                          @OA\Property(property="leader_name", type="string", description="群组长"),
 *                          @OA\Property(property="notice", type="string", description="群公告"),
 *                          @OA\Property(property="current_tax_rate", type="string", description="当前贡献消费积分设定"),
 *                          @OA\Property(
 *                              property="vote_status",
 *                              type="integer",
 *                              description="投票状态，状态为0时方可进行投票
 * -1:投票尚未发起,0:进行中,1:已通过,
 *     2:未通过,3:已过期,4:手动关闭"),
 *                          @OA\Property(property="has_voted", type="integer", description="是否已投票 0：否，1：是，-1：没有投票"),
 *                          @OA\Property(property="vote_id", type="string", description="最近一次投票编号"),
 *                          @OA\Property(property="vote_tax_rate", type="string", description="最近一次投票的积分"),
 *                          @OA\Property(property="join_num", type="integer", description="待加入列表人数")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/workgroup/create",
 *      tags={"app-代表的工作群组"},
 *      summary="创建群组",
 *      description="创建群组",
 *      operationId="/app/workgroup/create",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_name",
 *          in="query",
 *          description="群组名称",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/workgroup/rename",
 *      tags={"app-代表的工作群组"},
 *      summary="群组重命名",
 *      description="群组重命名",
 *      operationId="/app/workgroup/rename",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_name",
 *          in="query",
 *          description="群组名称",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/workgroup/notice/change",
 *      tags={"app-代表的工作群组"},
 *      summary="修改群公告",
 *      description="修改群公告",
 *      operationId="/app/workgroup/notice/change",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="content",
 *          in="query",
 *          description="群公告",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/workgroup/series",
 *      tags={"app-代表的工作群组"},
 *      summary="群产品列表",
 *      description="群产品列表",
 *      operationId="/app/workgroup/series",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="series_id", type="string", description="产品系列编号"),
 *                          @OA\Property(property="name", type="string", description="名称"),
 *                          @OA\Property(property="selected", type="integer", description="是否选中 0：否，1：是"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/workgroup/series/change",
 *      tags={"app-代表的工作群组"},
 *      summary="修改群产品",
 *      description="修改群产品",
 *      operationId="/app/workgroup/series/change",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="series_id",
 *          in="query",
 *          description="产品id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="series_id", type="integer", description="产品系列编号")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/workgroup/newLeader",
 *      tags={"app-代表的工作群组"},
 *      summary="修改群主",
 *      description="修改群主",
 *      operationId="/app/workgroup/newLeader",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="member_id",
 *          in="query",
 *          description="群组用户的member_id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组编号"),
 *                          @OA\Property(property="leader_id", type="integer", description="新群组长编号"),
 *                          @OA\Property(property="old_leader_id", type="integer", description="旧群组长编号")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/workgroup/delete",
 *      tags={"app-代表的工作群组"},
 *      summary="解散群组",
 *      description="解散群组",
 *      operationId="/app/workgroup/delete",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组编号")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/workgroup/qrcode",
 *      tags={"app-代表的工作群组"},
 *      summary="群组二维码",
 *      description="群组二维码",
 *      operationId="/app/workgroup/qrcode",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="url", type="string", description="二维码地址")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Group\Work;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Group\Work\ContentRequest;
use App\Request\App\Group\Work\CreateRequest;
use App\Request\App\Group\Work\DetailRequest;
use App\Request\App\Group\Work\NewLeaderRequest;
use App\Request\App\Group\Work\RenameRequest;
use App\Request\App\Group\Work\SearchRequest;
use App\Request\App\Group\Work\SeriesChangeRequest;
use App\Service\Group\Work\WrokGroupService;
use Hyperf\Di\Annotation\Inject;

class WorkGroupController extends Controller
{
    /**
     * @Inject()
     * @var WrokGroupService
     */
    private $wrokGroupService;

    /**
     * @param SearchRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function search(SearchRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $userYyid = Helper::getLoginUser()->yyid;
        $keyWord = $request->input('key_word');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);

        $data = $this->wrokGroupService->getGroupList($userId, $userYyid, $keyWord, $page, $pageSize);
        return $this->response->success($data);
    }

    /**
     * @param DetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function detail(DetailRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');

        $data = $this->wrokGroupService->getGroupDetail($userId, $groupId);
        return $this->response->success($data);
    }

    /**
     * @param CreateRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(CreateRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupName = trim($request->input('group_name'), ' ');

        $result = $this->wrokGroupService->createGroup($userId, $groupName);
        return $this->response->success($result);
    }

    /**
     * @param RenameRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function rename(RenameRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $groupName = trim($request->input('group_name'), ' ');

        $result = $this->wrokGroupService->renameGroup($userId, $groupId, $groupName);
        return $this->response->success($result);
    }

    /**
     * @param ContentRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function notice(ContentRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $content = trim($request->input('content'), ' ');

        $result = $this->wrokGroupService->changeNotice($userId, $groupId, $content);
        return $this->response->success($result);
    }

    /**
     * @param DetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function series(DetailRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');

        $result = $this->wrokGroupService->getSeries($userId, $groupId);
        return $this->response->success($result);
    }

    /**
     * @param SeriesChangeRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function changeSeries(SeriesChangeRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $seriesId = $request->input('series_id');

        $result = $this->wrokGroupService->changeSeries($userId, $groupId, $seriesId);
        return $this->response->success($result);
    }

    /**
     * @param NewLeaderRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function newLeader(NewLeaderRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $userYyid = Helper::getLoginUser()->yyid;
        $groupId = $request->input('group_id');
        $memberId = $request->input('member_id');

        $result = $this->wrokGroupService->choseNewLeader($userId, $userYyid, $groupId, $memberId);
        return $this->response->success($result);
    }

    /**
     * @param DetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(DetailRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');

        $result = $this->wrokGroupService->removeGroup($userId, $groupId);
        return $this->response->success($result);
    }

    /**
     * @param DetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function qrcode(DetailRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');

        $result = $this->wrokGroupService->getQrcode($userId, $groupId);
        return $this->response->success($result);
    }
}
