<?php

/**
 *
 * @OA\Get (
 *      path="/app/workgroup/member/list",
 *      tags={"app-代表的工作群组"},
 *      summary="群组成员列表",
 *      description="群组成员列表",
 *      operationId="/app/workgroup/member/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="name", type="string", description="成员名称"),
 *                          @OA\Property(property="headimgurl", type="string", description="成员头像"),
 *                          @OA\Property(property="realname", type="string", description="成员真实名称"),
 *                          @OA\Property(property="user_id", type="integer", description="成员user_id"),
 *                          @OA\Property(property="member_id", type="integer", description="成员编号"),
 *                          @OA\Property(property="hospital_num", type="integer", description="服务的医院数量"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/workgroup/join/list",
 *      tags={"app-代表的工作群组"},
 *      summary="待加入成员列表",
 *      description="待加入成员列表",
 *      operationId="/app/workgroup/join/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="name", type="string", description="成员名称"),
 *                          @OA\Property(property="headimgurl", type="string", description="成员头像"),
 *                          @OA\Property(property="realname", type="string", description="成员真实名称"),
 *                          @OA\Property(property="user_id", type="integer", description="成员user_id"),
 *                          @OA\Property(property="member_id", type="integer", description="成员编号"),
 *                          @OA\Property(property="user_unionid", type="string", description="成员unionid")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/workgroup/agents/list",
 *      tags={"app-代表的工作群组"},
 *      summary="我的所有代表列表",
 *      description="我的所有代表列表",
 *      operationId="/app/workgroup/agents/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="name", type="string", description="成员名称"),
 *                          @OA\Property(property="headimgurl", type="string", description="成员头像"),
 *                          @OA\Property(property="realname", type="string", description="成员真实名称"),
 *                          @OA\Property(property="user_id", type="integer", description="成员user_id"),
 *                          @OA\Property(property="user_unionid", type="string", description="成员unionid")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/workgroup/member/add",
 *      tags={"app-代表的工作群组"},
 *      summary="添加成员",
 *      description="添加成员",
 *      operationId="/app/workgroup/member/add",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组yyid",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="add_user_unionids",
 *          in="query",
 *          description="需要添加的用户unionid，我的所有代表接口返回的user_unionid，多个用户之间以英文逗号分割",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object"
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/workgroup/join/approval",
 *      tags={"app-代表的工作群组"},
 *      summary="批准/拒绝待加入成员",
 *      description="批准/拒绝待加入成员",
 *      operationId="/app/workgroup/join/approval",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="member_ids",
 *          in="query",
 *          description="需要同意的用户memberid，待加入列表接口返回的member_id，多个用户之间以英文逗号分割",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="approve",
 *          in="query",
 *          description="是否通过 1：通过，2：拒绝",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object"
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/workgroup/member/delete",
 *      tags={"app-代表的工作群组"},
 *      summary="删除群成员",
 *      description="删除群成员",
 *      operationId="/app/workgroup/member/delete",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="member_ids",
 *          in="query",
 *          description="需要删除的用户memberid，群成员列表接口返回的member_id，多个用户之间以英文逗号分割",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object"
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Group\Work;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Group\Work\AddMemberRequest;
use App\Request\App\Group\Work\ApproveRequest;
use App\Request\App\Group\Work\DelMemberRequest;
use App\Request\App\Group\Work\DetailRequest;
use App\Service\Group\Work\WrokGroupMemberService;
use Hyperf\Di\Annotation\Inject;

class WorkGroupMemberController extends Controller
{
    /**
     * @Inject()
     * @var WrokGroupMemberService
     */
    private $groupMemberService;

    /**
     * @param DetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list(DetailRequest $request)
    {
        $groupId = $request->input('group_id');

        $data = $this->groupMemberService->getMemberList($groupId);
        return $this->response->success($data);
    }

    /**
     * @param DetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function joinList(DetailRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');

        $data = $this->groupMemberService->getJoinList($userId, $groupId);
        return $this->response->success($data);
    }

    /**
     * @param DetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function myAgent(DetailRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $userYyid = Helper::getLoginUser()->yyid;
        $groupId = $request->input('group_id');

        $result = $this->groupMemberService->getMyagentList($userId, $userYyid, $groupId);
        return $this->response->success($result);
    }

    /**
     * @param AddMemberRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function add(AddMemberRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $userYyid = Helper::getLoginUser()->yyid;
        $groupId = $request->input('group_id');
        $addUserUnionids = explode(',', $request->input('add_user_unionids'));

        $result = $this->groupMemberService->addMember($userId, $userYyid, $groupId, $addUserUnionids);
        return $this->response->success($result);
    }

    /**
     * @param ApproveRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function approval(ApproveRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $memberIds = explode(',', $request->input('member_ids'));
        $approve = $request->input('approve');

        $result = $this->groupMemberService->approval($userId, $groupId, $memberIds, $approve);
        return $this->response->success($result);
    }

    /**
     * @param DelMemberRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(DelMemberRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $memberIds = explode(',', $request->input('member_ids'));

        $result = $this->groupMemberService->removeMember($userId, $groupId, $memberIds);
        return $this->response->success($result);
    }
}
