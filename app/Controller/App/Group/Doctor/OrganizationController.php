<?php

/**
 *
 * @OA\Get (
 *      path="/app/organization/getNavigate",
 *      tags={"app-组织架构下医生"},
 *      summary="获取用户的导航",
 *      description="获取用户的导航",
 *      operationId="/app/organization/getNavigate",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="department_id", type="integer", description="部门编号"),
 *                          @OA\Property(property="department_name", type="string", description="部门名称"),
 *                          @OA\Property(property="level", type="integer", description="部门等级"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/organization/getTopDepartment",
 *      tags={"app-组织架构下医生"},
 *      summary="获取用户的当前所处的顶级部门",
 *      description="获取用户的当前所处的顶级部门",
 *      operationId="/app/organization/getTopDepartment",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="department_id",
 *          in="query",
 *          description="导航中最后一级部门id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="department_id", type="integer", description="部门编号"),
 *                          @OA\Property(property="department_name", type="string", description="部门名称"),
 *                          @OA\Property(property="level", type="integer", description="部门等级"),
 *                          @OA\Property(property="agent_count", type="integer", description="代表数量"),
 *                          @OA\Property(property="subscribe_count", type="integer", description="关注医生数量"),
 *                          @OA\Property(property="unsubscribe_count", type="integer", description="未关注医生数量"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/organization/getNextDepartment",
 *      tags={"app-组织架构下医生"},
 *      summary="根据部门id获取用户的下级部门",
 *      description="根据部门id获取用户的下级部门",
 *      operationId="/app/organization/getNextDepartment",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="department_id",
 *          in="query",
 *          description="部门id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="department_id", type="integer", description="部门编号"),
 *                          @OA\Property(property="department_name", type="string", description="部门名称"),
 *                          @OA\Property(property="level", type="integer", description="部门等级"),
 *                          @OA\Property(property="agent_count", type="integer", description="代表数量"),
 *                          @OA\Property(property="subscribe_count", type="integer", description="关注医生数量"),
 *                          @OA\Property(property="unsubscribe_count", type="integer", description="未关注医生数量"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/organization/getDepartmentAgent",
 *      tags={"app-组织架构下医生"},
 *      summary="根据部门id获取当前部门下的代表",
 *      description="根据部门id获取当前部门下的代表",
 *      operationId="/app/organization/getDepartmentAgent",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="department_id",
 *          in="query",
 *          description="部门id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="uid", type="integer", description="代表id"),
 *                          @OA\Property(property="name", type="string", description="用户名"),
 *                          @OA\Property(property="truename", type="string", description="用户真名"),
 *                          @OA\Property(property="title", type="string", description="职位"),
 *                          @OA\Property(property="headimgurl", type="string", description="头像"),
 *                          @OA\Property(property="is_leader", type="integer", description="1-主管 0-不是主管"),
 *                          @OA\Property(property="subscribe_count", type="integer", description="关注医生数量"),
 *                          @OA\Property(property="unsubscribe_count", type="integer", description="未关注医生数量"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/organization/getDoctorByAgent",
 *      tags={"app-组织架构下医生"},
 *      summary="根据代表id获取绑定的医生",
 *      description="根据代表id获取绑定的医生",
 *      operationId="/app/organization/getDoctorByAgent",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="uid",
 *          in="query",
 *          description="代表id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页码列表数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="列表总量"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页码列表数"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="r_id", type="integer", description="rid"),
 *                              @OA\Property(property="doctor_id", type="integer", description="医生id"),
 *                              @OA\Property(property="nickname", type="string", description="昵称"),
 *                              @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                              @OA\Property(property="headimgurl", type="string", description="头像"),
 *                              @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                              @OA\Property(property="depart_name", type="string", description="部门名称"),
 *                              @OA\Property(property="is_subscribed", type="integer", description="是否关注 1-是 0-否"),
 *                          ),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/organization/searchDoctor",
 *      tags={"app-组织架构下医生"},
 *      summary="搜索医生",
 *      description="搜索医生",
 *      operationId="/app/organization/searchDoctor",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="keyword",
 *          in="query",
 *          description="关键词",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页码列表数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="r_id", type="integer", description="rid"),
 *                          @OA\Property(property="doctor_id", type="integer", description="医生id"),
 *                          @OA\Property(property="nickname", type="string", description="昵称"),
 *                          @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                          @OA\Property(property="headimgurl", type="string", description="头像"),
 *                          @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                          @OA\Property(property="depart_name", type="string", description="部门名称"),
 *                          @OA\Property(property="is_subscribed", type="integer", description="是否关注 1-是 0-否"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/organization/searchRecord",
 *      tags={"app-组织架构下医生"},
 *      summary="搜索记录",
 *      description="搜索记录",
 *      operationId="/app/organization/searchRecord",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Group\Doctor;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Group\Organzation\GetDoctorRequest;
use App\Request\App\Group\Organzation\SearchDoctorRequest;
use App\Request\App\Group\Organzation\TopDepartmentRequest;
use App\Service\Group\Doctor\OrganizationService;
use Hyperf\Di\Annotation\Inject;

class OrganizationController extends Controller
{
    /**
     * @Inject()
     * @var OrganizationService
     */
    private $organizationService;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getNavigate()
    {
        $userId = Helper::getLoginUser()->uid;
        $data = $this->organizationService->getNavigate($userId);
        return $this->response->success($data);
    }

    /**
     * @param TopDepartmentRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getTopDepartment(TopDepartmentRequest $request)
    {
        $departId = $request->input('department_id');
        $userId = Helper::getLoginUser()->uid;
        $data = $this->organizationService->getTopDepartment($departId, $userId);
        return $this->response->success($data);
    }

    /**
     * @param TopDepartmentRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getNextDepartment(TopDepartmentRequest $request)
    {
        $departId = $request->input('department_id');
        $userId = Helper::getLoginUser()->uid;
        $data = $this->organizationService->getNextDepartment($departId, $userId);
        return $this->response->success($data);
    }

    /**
     * @param TopDepartmentRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getDepartmentAgent(TopDepartmentRequest $request)
    {
        $departId = $request->input('department_id');
        $userId = Helper::getLoginUser()->uid;
        $data = $this->organizationService->getDepartmentAgent($departId, $userId);
        return $this->response->success($data);
    }

    /**
     * @param GetDoctorRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getDoctorByAgent(GetDoctorRequest $request)
    {
        $uid = $request->input('uid');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);
        $data = $this->organizationService->getDoctorByAgent($uid, $page, $pageSize);
        return $this->response->success($data);
    }

    /**
     * @param SearchDoctorRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function searchDoctor(SearchDoctorRequest $request)
    {
        $keyword = $request->input('keyword');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);
        $userId = Helper::getLoginUser()->uid;
        $data = $this->organizationService->searchDoctor($userId, $keyword, $page, $pageSize);
        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function searchRecord()
    {
        $userId = Helper::getLoginUser()->uid;
        $data = $this->organizationService->searchRecord($userId);
        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function levelReset()
    {
        $this->organizationService->levelReset();
        return $this->response->success([]);
    }
}
