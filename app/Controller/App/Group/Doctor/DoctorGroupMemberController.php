<?php

/**
 *
 * @OA\Get (
 *      path="/app/doctorgroup/member/list",
 *      tags={"app-代表的医生群组"},
 *      summary="群组成员列表（带搜索）",
 *      description="群组成员列表（带搜索）",
 *      operationId="/app/doctorgroup/member/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="keyword",
 *          in="query",
 *          description="搜索医生姓名",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id 不传获取默认我的医生组",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码 默认1",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="列表数量 默认10",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="总量"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面列表数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="r_id", type="integer", description="好友关系id"),
 *                              @OA\Property(property="nid", type="integer", description="医生库id"),
 *                              @OA\Property(property="user_unionid", type="string", description="医生unionid"),
 *                              @OA\Property(property="nickname", type="string", description="昵称"),
 *                              @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                              @OA\Property(property="headimgurl", type="string", description="头像"),
 *                              @OA\Property(property="hospital_name", type="string", description="所在医院"),
 *                              @OA\Property(property="depart_name", type="string", description="所在科室"),
 *                              @OA\Property(property="is_subscribe", type="integer", description="是1否0关注"),
 *                              @OA\Property(property="group_count", type="integer", description="医生所在群组数量"),
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/doctorgroup/join/add",
 *      tags={"app-代表的医生群组"},
 *      summary="从医生底层库加入到我的医生组",
 *      description="从医生底层库加入到我的医生组",
 *      operationId="/app/doctorgroup/join/add",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="n_ids",
 *          in="query",
 *          description="待加入列表的n_id,多个使用英文逗号间隔",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群id 暂不分组传0",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="has_missing_info", type="integer", description="选择的医生里是否有信息不完整的 1-是 0-否"),
 *                          @OA\Property(
 *                              property="missing_list",
 *                              type="object",
 *                              description="缺失信息医生名单",
 *                              @OA\Property(property="nid", type="integer", description="医生库id"),
 *                              @OA\Property(property="true_name", type="string", description="医生姓名"),
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/doctorgroup/member/add",
 *      tags={"app-代表的医生群组"},
 *      summary="从我的医生默认组加入具体某一群组",
 *      description="从我的医生默认组加入具体某一群组",
 *      operationId="/app/doctorgroup/member/add",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="r_ids",
 *          in="query",
 *          description="列表r_id 多个通过英文逗号相连",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组id"),
 *                          @OA\Property(property="insertIds", type="object", description="新增的id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/doctorgroup/member/delete",
 *      tags={"app-代表的医生群组"},
 *      summary="从群组删除成员（不是真的删除，相当于移入我的医生）",
 *      description="从群组删除成员（不是真的删除，相当于移入我的医生）",
 *      operationId="/app/doctorgroup/member/delete",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组yyid",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="member_ids",
 *          in="query",
 *          description="列表member_id 多个通过英文逗号相连",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组yyid"),
 *                          @OA\Property(property="member_ids", type="object", description="删除的医生id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/doctorgroup/member/info",
 *      tags={"app-代表的医生群组"},
 *      summary="成员医生的个人详情",
 *      description="成员医生的个人详情",
 *      operationId="/app/doctorgroup/member/info",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="n_id",
 *          in="query",
 *          description="医生列表nid",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="id", type="integer", description="组员信息id"),
 *                          @OA\Property(property="headimgurl", type="string", description="头像"),
 *                          @OA\Property(property="nickname", type="string", description="昵称"),
 *                          @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                          @OA\Property(property="gender", type="integer", description="性别 0表示未知 1表示男 2表示女"),
 *                          @OA\Property(property="mobile_num", type="string", description="手机号码"),
 *                          @OA\Property(property="hospital_id", type="integer", description="医院id"),
 *                          @OA\Property(property="hospital_yyid", type="string", description="医院yyid"),
 *                          @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                          @OA\Property(property="depart_id", type="string", description="科室id"),
 *                          @OA\Property(property="depart_name", type="string", description="科室名称"),
 *                          @OA\Property(property="position", type="string", description="级别/职务"),
 *                          @OA\Property(property="job_title", type="string", description="职称"),
 *                          @OA\Property(property="group_yyid", type="string", description="分组 yyid"),
 *                          @OA\Property(property="group_name", type="string", description="分组名"),
 *                          @OA\Property(property="field_id", type="integer", description="擅长领域 id"),
 *                          @OA\Property(property="field_name", type="string", description="擅长领域名称"),
 *                          @OA\Property(property="clinic_type", type="integer", description="门诊类型 1：专家，2：普通"),
 *                          @OA\Property(property="clinic_rota", type="string", description="门诊值班情况 原样返回")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/doctorgroup/member/checkSame",
 *      tags={"app-代表的医生群组"},
 *      summary="判断新增的医生是否同名同医院",
 *      description="判断新增的医生是否同名同医院",
 *      operationId="/app/doctorgroup/member/checkSame",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="true_name",
 *          in="query",
 *          description="真实姓名",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="hospital_id",
 *          in="query",
 *          description="医院id",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/doctorgroup/member/update",
 *      tags={"app-代表的医生群组"},
 *      summary="新增或更新医生的个人详情",
 *      description="新增或更新医生的个人详情",
 *      operationId="/app/doctorgroup/member/update",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="nid",
 *          in="query",
 *          description="信息nid",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="true_name",
 *          in="query",
 *          description="真实姓名",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="gender",
 *          in="query",
 *          description="性别 0表示未知 1表示男 2表示女",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="mobile_num",
 *          in="query",
 *          description="手机号码",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="hospital_id",
 *          in="query",
 *          description="医院id",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="depart_id",
 *          in="query",
 *          description="科室id",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="position",
 *          in="query",
 *          description="级别/职务",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="job_title",
 *          in="query",
 *          description="职称",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="field_id",
 *          in="query",
 *          description="擅长领域id",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="kol_level",
 *          in="query",
 *          description="kol等级 1-全国性kol 2-区域kol 3-处方医生 0-其他",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="clinic_rota",
 *          in="query",
 *          description="门诊值班情况",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/doctorgroup/member/deleteForAll",
 *      tags={"app-代表的医生群组"},
 *      summary="从我的医生删除，真实删除",
 *      description="从我的医生删除，真实删除",
 *      operationId="/app/doctorgroup/member/deleteForAll",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="r_id",
 *          in="query",
 *          description="列表r_id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="r_id", type="object", description="删除的医生id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Group\Doctor;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Group\Work\DoctorMemberRequest;
use App\Request\App\Group\Work\JoinAddRequest;
use App\Request\App\Group\Work\MemberAddRequest;
use App\Request\App\Group\Work\MemberDelRequest;
use App\Request\App\Group\Work\MemberInfoRequest;
use App\Request\App\Group\Work\MemberUpdateRequest;
use App\Service\Group\Doctor\DoctorGroupMemberService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;

class DoctorGroupMemberController extends Controller
{
    /**
     * @Inject()
     * @var DoctorGroupMemberService
     */
    private $groupMemberService;

    /**
     * @param DoctorMemberRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function members(DoctorMemberRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $keyword = $request->input('keyword', '');
        $groupId = $request->input('group_id') ?? '';
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);

        $result = $this->groupMemberService->members($keyword, $userId, $groupId, $page, $pageSize);
        return $this->response->success($result);
    }

    /**
     * @param JoinAddRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function add(JoinAddRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $nIds = explode(',', trim($request->input('n_ids'), ' '));
        $groupId = $request->input('group_id', 0);

        $result = $this->groupMemberService->joinAdd($userId, $nIds, $groupId);
        return $this->response->success($result);
    }

    /**
     * @param MemberAddRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function addMember(MemberAddRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $rIds = explode(',', $request->input('r_ids'));

        $result = $this->groupMemberService->addMember($userId, $groupId, $rIds);
        return $this->response->success($result);
    }

    /**
     * @param MemberDelRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(MemberDelRequest $request)
    {
        $groupId = $request->input('group_id');
        $memberIds = explode(',', $request->input('member_ids'));

        $result = $this->groupMemberService->deleteMember($groupId, $memberIds);
        return $this->response->success($result);
    }

    /**
     * @param MemberDelRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function change(MemberDelRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $memberIds = explode(',', $request->input('member_ids'));

        $result = $this->groupMemberService->changeMember($userId, $groupId, $memberIds);
        return $this->response->success($result);
    }

    /**
     * @param MemberInfoRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function info(MemberInfoRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $nId = $request->input('n_id');

        $result = $this->groupMemberService->getMemberInfo($userId, $nId);
        return $this->response->success($result);
    }

    /**
     * @param RequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function checkSame(RequestInterface $request)
    {
        $trueName = $request->input('true_name');
        $hospitalId = $request->input('hospital_id');

        $this->groupMemberService->checkSame($trueName, $hospitalId);
        return $this->response->success([]);
    }

    /**
     * @param MemberUpdateRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update(MemberUpdateRequest $request)
    {
        $infoId = $request->input('nid', 0);
        $trueName = $request->input('true_name');
        $gender = $request->input('gender');
        $mobile = $request->input('mobile_num');
        $hospitalId = $request->input('hospital_id');
        $departId = $request->input('depart_id');
        $position = $request->input('position');
        $jobTitle = $request->input('job_title');
        $fieldId = $request->input('field_id');
        $kolLevel = $request->input('kol_level');
        $clinicRota = $request->input('clinic_rota');

        $this->groupMemberService->updateMemberInfo(
            $infoId,
            $trueName,
            $gender,
            $mobile,
            $hospitalId,
            $departId,
            $position,
            $jobTitle,
            $fieldId,
            $kolLevel,
            $clinicRota
        );
        return $this->response->success([]);
    }

    /**
     * @param MemberInfoRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deleteForAll(MemberInfoRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $rId = $request->input('r_id');

        $result = $this->groupMemberService->deleteForAll($userId, $rId);
        return $this->response->success($result);
    }
}
