<?php

/**
 *
 * @OA\Get (
 *      path="/app/doctorgroup/static/departments",
 *      tags={"app-代表的医生群组"},
 *      summary="固定部门分组列表",
 *      description="固定部门分组列表",
 *      operationId="/app/doctorgroup/static/departments",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="depart_id", type="integer", description="部门id"),
 *                          @OA\Property(property="name", type="string", description="部门名称"),
 *                          @OA\Property(property="img", type="string", description="图片"),
 *                          @OA\Property(property="count", type="integer", description="成员人数")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/doctorgroup/static/members",
 *      tags={"app-代表的医生群组"},
 *      summary="固定部门分组成员列表",
 *      description="固定部门分组成员列表",
 *      operationId="/app/doctorgroup/static/members",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="depart_id",
 *          in="query",
 *          description="部门id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码 默认1",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="列表数量 默认10",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="总量"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面列表数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="s_id", type="integer", description="关系id"),
 *                              @OA\Property(property="nid", type="integer", description="医生库id"),
 *                              @OA\Property(property="nickname", type="string", description="昵称"),
 *                              @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                              @OA\Property(property="headimgurl", type="string", description="头像"),
 *                              @OA\Property(property="hospital_name", type="string", description="所在医院"),
 *                              @OA\Property(property="depart_name", type="string", description="所在科室"),
 *                              @OA\Property(property="is_subscribe", type="integer", description="是1否0关注"),
 *                              @OA\Property(property="group_count", type="integer", description="医生所在群组数量"),
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/doctorgroup/static/autoGroup",
 *      tags={"app-代表的医生群组"},
 *      summary="自动分组",
 *      description="自动分组",
 *      operationId="/app/doctorgroup/static/autoGroup",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息")
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/doctorgroup/inGroup",
 *      tags={"app-代表的医生群组"},
 *      summary="医生当前所在分组",
 *      description="医生当前所在分组",
 *      operationId="/app/doctorgroup/inGroup",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="n_id",
 *          in="query",
 *          description="医生id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="id", type="integer", description="id"),
 *                          @OA\Property(property="group_name", type="string", description="群组名称"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Group\Doctor;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Group\Work\MemberInfoRequest;
use App\Request\App\Group\Work\StaticMemberRequest;
use App\Service\Group\Doctor\DoctorGroupStaticService;
use Hyperf\Di\Annotation\Inject;

class DoctorGroupStaticController extends Controller
{
    /**
     * @Inject()
     * @var DoctorGroupStaticService
     */
    private $doctorStaticService;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function departments()
    {
        $userYyid = Helper::getLoginUser()->yyid;
        $userId = Helper::getLoginUser()->uid;

        $data = $this->doctorStaticService->departments($userYyid, $userId);
        return $this->response->success($data);
    }

    /**
     * @param StaticMemberRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function members(StaticMemberRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $departId = $request->input('depart_id');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);

        $data = $this->doctorStaticService->members($userId, $departId, $page, $pageSize);
        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function autoGroup()
    {
        $userId = Helper::getLoginUser()->uid;

        $data = $this->doctorStaticService->autoGroup($userId);
        return $this->response->success($data);
    }

    /**
     * @param MemberInfoRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function inGroup(MemberInfoRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $nId = $request->input('n_id');

        $data = $this->doctorStaticService->inGroup($userId, $nId);
        return $this->response->success($data);
    }
}
