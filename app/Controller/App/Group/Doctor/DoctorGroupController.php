<?php

/**
 *
 * @OA\Get (
 *      path="/app/doctorgroup/info",
 *      tags={"app-代表的医生群组"},
 *      summary="默认组医生数量",
 *      description="默认组医生数量",
 *      operationId="/app/doctorgroup/info",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="id"),
 *                          @OA\Property(property="group_name", type="string", description="组名"),
 *                          @OA\Property(property="group_num", type="integer", description="数量"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/doctorgroup/list",
 *      tags={"app-代表的医生群组"},
 *      summary="群组列表（不包括默认组）",
 *      description="群组列表（不包括默认组）",
 *      operationId="/app/doctorgroup/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组id"),
 *                          @OA\Property(property="group_name", type="string", description="群组名称"),
 *                          @OA\Property(property="group_num", type="integer", description="成员人数")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/doctorgroup/create",
 *      tags={"app-代表的医生群组"},
 *      summary="创建群组",
 *      description="创建群组",
 *      operationId="/app/doctorgroup/create",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_name",
 *          in="query",
 *          description="群组名称",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="id", type="integer", description="id"),
 *                          @OA\Property(property="group_name", type="string", description="新建的群组名称")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/doctorgroup/delete",
 *      tags={"app-代表的医生群组"},
 *      summary="删除群组",
 *      description="删除群组",
 *      operationId="/app/doctorgroup/delete",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/doctorgroup/rename",
 *      tags={"app-代表的医生群组"},
 *      summary="群组重命名",
 *      description="群组重命名",
 *      operationId="/app/doctorgroup/rename",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="group_id",
 *          in="query",
 *          description="群组id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="group_name",
 *          in="query",
 *          description="群组名称",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="group_id", type="integer", description="群组id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/doctorgroup/preset-invite",
 *      tags={"app-代表的医生群组"},
 *      summary="定向邀请医生二维码",
 *      description="定向邀请医生二维码",
 *      operationId="/app/doctorgroup/preset-invite",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="nid",
 *          in="query",
 *          description="列表里的nid",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="url", type="string", description="二维码链接")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/doctorgroup/hospitals",
 *      tags={"app-代表的医生群组"},
 *      summary="我服务的医院",
 *      description="我服务的医院",
 *      operationId="/app/doctorgroup/hospitals",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="hospital_count", type="integer", description="医院数量"),
 *                          @OA\Property(property="doctor_count", type="integer", description="医生数量"),
 *                          @OA\Property(property="total", type="integer", description="总数量"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面容量"),
 *                          @OA\Property(
 *                              property="hospital_list",
 *                              type="object",
 *                              @OA\Property(property="hospital_id", type="integer", description="医院id"),
 *                              @OA\Property(property="hospital_yyid", type="string", description="医院yyid"),
 *                              @OA\Property(property="hospital_name", type="string", description="医院名称")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/doctorgroup/doctors",
 *      tags={"app-代表的医生群组"},
 *      summary="我服务的医院下的医生（可搜索）",
 *      description="我服务的医院下的医生（可搜索）",
 *      operationId="/app/doctorgroup/doctors",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="doctor_name",
 *          in="query",
 *          description="医生姓名",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="hospital_yyid",
 *          in="query",
 *          description="医院yyid",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="总数量"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面数量"),
 *                          @OA\Property(
 *                              property="hospital_list",
 *                              type="object",
 *                              @OA\Property(property="nid", type="integer", description="医生库id"),
 *                              @OA\Property(property="openid", type="string", description="医生openid"),
 *                              @OA\Property(property="nickname", type="string", description="医生昵称"),
 *                              @OA\Property(property="true_name", type="string", description="医生真名"),
 *                              @OA\Property(property="headimgurl", type="string", description="头像"),
 *                              @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                              @OA\Property(property="depart_name", type="string", description="部门名称"),
 *                              @OA\Property(property="is_subscribed", type="integer", description="是否关注 0-否 1-是"),
 *                              @OA\Property(property="is_verify", type="integer", description="是否认证 0-否 1-是"),
 *                              @OA\Property(property="is_in", type="integer", description="是否已经在我的群组 0-否 1-是"),
 *                              @OA\Property(property="info_missing", type="integer", description="是否缺失信息 0-否 1-是")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Group\Doctor;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Group\Work\CreateRequest;
use App\Request\App\Group\Work\DetailRequest;
use App\Request\App\Group\Work\OrgDoctorSearchRequest;
use App\Request\App\Group\Work\PresetUpdateRequest;
use App\Request\App\Group\Work\RenameRequest;
use App\Service\Group\Doctor\DoctorGroupService;
use Hyperf\Di\Annotation\Inject;

class DoctorGroupController extends Controller
{
    /**
     * @Inject()
     * @var DoctorGroupService
     */
    private $doctorGroupService;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function info()
    {
        $userId = Helper::getLoginUser()->uid;

        $data = $this->doctorGroupService->getDoctorGroupInfo($userId);
        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list()
    {
        $userId = Helper::getLoginUser()->uid;

        $data = $this->doctorGroupService->getDoctorGroupList($userId);
        return $this->response->success($data);
    }

    /**
     * @param CreateRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(CreateRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupName = trim($request->input('group_name'), ' ');

        $result = $this->doctorGroupService->createGroup($userId, $groupName);
        return $this->response->success($result);
    }

    /**
     * @param DetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(DetailRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');

        $result = $this->doctorGroupService->removeGroup($userId, $groupId);
        return $this->response->success($result);
    }

    /**
     * @param RenameRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function rename(RenameRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $groupId = $request->input('group_id');
        $groupName = trim($request->input('group_name'), ' ');

        $result = $this->doctorGroupService->renameGroup($userId, $groupId, $groupName);
        return $this->response->success($result);
    }

    public function doctorQrCode()
    {
        $url = $this->doctorGroupService->getDoctorQrcodeUri();
        return $this->response->success([
            'url' => $url
        ]);
    }

    /**
     * @param PresetUpdateRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function presetInvite(PresetUpdateRequest $request)
    {
        $nid = $request->input('nid');

        $res = $this->doctorGroupService->presetInvite($nid
        );
        return $this->response->success($res);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function hospitals(OrgDoctorSearchRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $yyid = Helper::getLoginUser()->yyid;
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);

        $result = $this->doctorGroupService->hospitals($userId, $yyid, $page, $pageSize);
        return $this->response->success($result);
    }

    /**
     * @param OrgDoctorSearchRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doctors(OrgDoctorSearchRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $yyid = Helper::getLoginUser()->yyid;
        $doctorName = $request->input('doctor_name');
        $hospitalYyid = $request->input('hospital_yyid');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);

        $result = $this->doctorGroupService->doctors($userId, $yyid, $doctorName, $hospitalYyid, $page, $pageSize);
        return $this->response->success($result);
    }
}
