<?php
/**
 *
 * @OA\Get (
 *      path="/app/report/visit",
 *      tags={"app-报告"},
 *      summary="拜访报告",
 *      description="拜访报告",
 *      operationId="/app/report/visit",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="month", type="integer", description="月份"),
 *                          @OA\Property(
 *                              property="id",
 *                              type="integer",
 *                              description="Timestamp for previous time user accessed the site."),
 *                          @OA\Property(property="ins_class", type="integer", description="机构级别"),
 *                          @OA\Property(property="of_tgt_ins", type="string", description="目标机构数量"),
 *                          @OA\Property(property="of_ins_visited", type="integer", description="拜访机构数量"),
 *                          @OA\Property(property="coverage", type="integer", description="拜访率"),
 *                          @OA\Property(property="of_calls", type="integer", description="拜访数"),
 *                          @OA\Property(property="frequency", type="string", description="拜访频率"),
 *                          @OA\Property(property="base_num", type="integer", description="base地已列名机构数量"),
 *                          @OA\Property(property="base_standard_num", type="string", description="base地已列名机构拜访达标数量"),
 *                          @OA\Property(property="base_rage", type="integer", description="base地已列名达标率"),
 *                          @OA\Property(property="vacant_days", type="integer", description="空岗天数"),
 *                          @OA\Property(property="dif", type="integer", description="DFI"),
 *                          @OA\Property(property="dif_percent", type="string", description="DFI比例"),
 *                          @OA\Property(property="working_hours", type="string", description="空岗天数/DFI")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/report/sales",
 *      tags={"app-报告"},
 *      summary="BI报告",
 *      description="BI报告",
 *      operationId="/app/report/sales",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="month", type="integer", description="月份"),
 *                          @OA\Property(
 *                              property="id",
 *                              type="integer",
 *                              description="Timestamp for previous time user accessed the site."),
 *                          @OA\Property(property="month_target", type="integer", description="月目标"),
 *                          @OA\Property(property="month_sales", type="string", description="月销量"),
 *                          @OA\Property(property="month_ach", type="integer", description="月达成率"),
 *                          @OA\Property(property="ly_sales", type="integer", description="去年4-12月均销量"),
 *                          @OA\Property(property="month_gr", type="integer", description="月增长"),
 *                          @OA\Property(property="qtd_target", type="integer", description="季度目标"),
 *                          @OA\Property(property="qtd_sales", type="string", description="季度销量"),
 *                          @OA\Property(property="qtd_ach", type="integer", description="季度达成率"),
 *                          @OA\Property(property="qtd_ly_sales", type="integer", description="去年4-12月季度销量"),
 *                          @OA\Property(property="qtd_gr", type="integer", description="季度增长"),
 *                          @OA\Property(property="ytd_target", type="integer", description="年目标"),
 *                          @OA\Property(property="ytd_sales", type="string", description="年销量"),
 *                          @OA\Property(property="ytd_ach", type="integer", description="年达成率"),
 *                          @OA\Property(property="ytd_ly_sales", type="integer", description="去年4-12月年销量"),
 *                          @OA\Property(property="ytd_gr", type="integer", description="年增长")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/report/sendSalesMail",
 *      tags={"app-报告"},
 *      summary="导出报告发送到指定邮箱",
 *      description="导出报告发送到指定邮箱",
 *      operationId="/app/report/sendSalesMail",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="email",
 *          in="query",
 *          description="邮箱地址",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object"
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/report/sendCheckoutMail",
 *      tags={"app-报告"},
 *      summary="导出拜访明细报告发送到指定邮箱",
 *      description="导出拜访明细报告发送到指定邮箱",
 *      operationId="/app/report/sendCheckoutMail",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="email",
 *          in="query",
 *          description="邮箱地址",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object"
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/report/departs",
 *      tags={"app-报告"},
 *      summary="医生拜访报告科室选项",
 *      description="医生拜访报告科室选项",
 *      operationId="/app/report/departs",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object"
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/report/doctorVisit",
 *      tags={"app-报告"},
 *      summary="医生拜访报告汇总",
 *      description="医生拜访报告汇总",
 *      operationId="/app/report/doctorVisit",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="department",
 *          in="query",
 *          description="部门名称",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object"
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);

namespace App\Controller\App\Report;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\Report\AppSendMailRequest;
use App\Request\Report\DepartRequest;
use App\Service\Report\ReportService;
use Hyperf\Di\Annotation\Inject;

class ReportController extends Controller
{
    /**
     * @Inject()
     * @var ReportService
     */
    private $reportService;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function visit()
    {
        $yyid = Helper::getLoginUser()->yyid;

        $data = $this->reportService->getVisitReport($yyid);

        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function sales()
    {
        $yyid = Helper::getLoginUser()->yyid;

        $data = $this->reportService->getSalesReport($yyid);

        return $this->response->success($data);
    }

    /**
     * @param AppSendMailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function sendSalesMail(AppSendMailRequest $request)
    {
        $yyid = Helper::getLoginUser()->yyid;
        $email = $request->input('email');
        $res = $this->reportService->sendMail($yyid, $email);

        return $this->response->success($res);
    }

    /**
     * @param AppSendMailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function sendCheckoutMail(AppSendMailRequest $request)
    {
        $yyid = Helper::getLoginUser()->yyid;
        $email = $request->input('email');
        $res = $this->reportService->sendCheckoutMail($yyid, $email);

        return $this->response->success($res);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function departs()
    {
        $userId = Helper::getLoginUser()->uid;
        $res = $this->reportService->getDeparts($userId);

        return $this->response->success($res);
    }

    /**
     * @param DepartRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doctorVisit(DepartRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $department = $request->input('department');
        $res = $this->reportService->doctorVisit($userId, $department);

        return $this->response->success($res);
    }
}
