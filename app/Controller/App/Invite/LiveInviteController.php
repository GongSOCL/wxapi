<?php

/**
 *
 * @OA\Get (
 *      path="/app/live-invite/lives",
 *      tags={"app-直播邀请"},
 *      summary="直播列表",
 *      description="直播列表",
 *      operationId="/app/live-invite/lives",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="live_name",
 *          in="query",
 *          description="直播名称",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面列表数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="消息列表总数"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="id", type="integer", description="id"),
 *                              @OA\Property(property="title", type="string", description="标题"),
 *                              @OA\Property(property="live_state", type="integer", description="直播状态 1直播中2未开始3已结束有回顾"),
 *                              @OA\Property(property="thumbnail_img", type="string", description="图片"),
 *                              @OA\Property(property="start_time", type="string", description="开始时间")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/live-invite/doctors",
 *      tags={"app-直播邀请"},
 *      summary="医生列表",
 *      description="医生列表",
 *      operationId="/app/live-invite/doctors",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="doctor_name",
 *          in="query",
 *          description="医生真名",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="live_id",
 *          in="query",
 *          description="直播id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面列表数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="消息列表总数"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="id", type="integer", description="id"),
 *                              @OA\Property(property="nickname", type="string", description="昵称"),
 *                              @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                              @OA\Property(property="headimgurl", type="string", description="头像"),
 *                              @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                              @OA\Property(property="depart_name", type="string", description="部门名称"),
 *                              @OA\Property(property="position", type="string", description="职称"),
 *                              @OA\Property(property="is_subscribe", type="integer", description="0-未关注1-已关注"),
 *                              @OA\Property(property="is_invited", type="integer", description="0-未邀请过1-已邀请过")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/live-invite/invite-list",
 *      tags={"app-直播邀请"},
 *      summary="邀请列表",
 *      description="邀请列表",
 *      operationId="/app/live-invite/invite-list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面列表数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="消息列表总数"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="invite_id", type="integer", description="邀请id"),
 *                              @OA\Property(property="live_id", type="integer", description="直播id"),
 *                              @OA\Property(property="live_title", type="string", description="直播标题"),
 *                              @OA\Property(property="live_img", type="string", description="直播封面"),
 *                              @OA\Property(property="live_start_time", type="string", description="开始时间"),
 *                              @OA\Property(property="count", type="integer", description="详情数量")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/live-invite/detail",
 *      tags={"app-直播邀请"},
 *      summary="邀请列表",
 *      description="邀请列表",
 *      operationId="/app/live-invite/detail",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="invite_id",
 *          in="query",
 *          description="邀请id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面列表数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="消息列表总数"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="id", type="integer", description="id"),
 *                              @OA\Property(property="nickname", type="string", description="昵称"),
 *                              @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                              @OA\Property(property="headimgurl", type="string", description="头像"),
 *                              @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                              @OA\Property(property="depart_name", type="string", description="部门名称"),
 *                              @OA\Property(property="position", type="string", description="职称"),
 *                              @OA\Property(property="is_read", type="integer", description="0-未查看1-已查看")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/live-invite/send",
 *      tags={"app-直播邀请"},
 *      summary="邀请",
 *      description="邀请",
 *      operationId="/app/live-invite/send",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="live_id",
 *          in="query",
 *          description="直播id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="doctor_id[]",
 *          in="query",
 *          description="医生id数组【】",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="marks",
 *          in="query",
 *          description="个性化文字",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);

namespace App\Controller\App\Invite;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\LiveInvite\DetailRequest;
use App\Request\App\LiveInvite\GetDoctorsRequest;
use App\Request\App\LiveInvite\GetListRequest;
use App\Request\App\LiveInvite\GetLivesRequest;
use App\Request\App\LiveInvite\SendRequest;
use App\Service\Invite\LiveInviteService;
use Hyperf\Di\Annotation\Inject;

class LiveInviteController extends Controller
{
    /**
     * @Inject()
     * @var LiveInviteService
     */
    private $liveInviteService;

    /**
     * @param GetLivesRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function lives(GetLivesRequest $request)
    {
        $liveName = $request->input('live_name');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);
        $data = $this->liveInviteService->lives($liveName, $page, $pageSize);

        return $this->response->success($data);
    }

    /**
     * @param GetDoctorsRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doctors(GetDoctorsRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $doctorName = $request->input('doctor_name');
        $liveId = $request->input('live_id');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);
        $data = $this->liveInviteService->doctors($userId, $doctorName, $liveId, $page, $pageSize);

        return $this->response->success($data);
    }

    /**
     * @param GetListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function inviteList(GetListRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);
        $data = $this->liveInviteService->inviteList($userId, $page, $pageSize);

        return $this->response->success($data);
    }

    /**
     * @param DetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function detail(DetailRequest $request)
    {
        $inviteId = $request->input('invite_id');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);
        $data = $this->liveInviteService->detail($inviteId, $page, $pageSize);

        return $this->response->success($data);
    }

    /**
     * @param SendRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function send(SendRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $liveId = $request->input('live_id');
        $doctorId = $request->input('doctor_id');
        $marks = $request->input('marks');
        $data = $this->liveInviteService->send($userId, $liveId, $doctorId, $marks);

        return $this->response->success($data);
    }
}
