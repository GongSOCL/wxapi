<?php

/**
 *
 * @OA\Get (
 *      path="/app/invite/list",
 *      tags={"app-我的邀请"},
 *      summary="我的邀请列表",
 *      description="我的邀请列表",
 *      operationId="/app/doctorgroup/info",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="r_id", type="integer", description="邀请id"),
 *                          @OA\Property(property="user_id", type="integer", description="用户id 未注册时为0"),
 *                          @OA\Property(property="nickname", type="string", description="昵称"),
 *                          @OA\Property(property="headimgurl", type="string", description="头像"),
 *                          @OA\Property(property="created_at", type="string", description="邀请时间"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);

namespace App\Controller\App\Invite;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Service\Invite\InviteService;
use Hyperf\Di\Annotation\Inject;

class InviteController extends Controller
{
    /**
     * @Inject()
     * @var InviteService
     */
    private $inviteService;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list()
    {
        $userId = Helper::getLoginUser()->uid;

        $data = $this->inviteService->getInviteList($userId);

        return $this->response->success($data);
    }
}
