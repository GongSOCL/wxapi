<?php

/**
 *
 * @OA\Get (
 *      path="/app/favorites/list",
 *      tags={"app-收藏夹"},
 *      summary="收藏夹列表",
 *      description="收藏夹列表",
 *      operationId="/app/favorites/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="fav_id", type="integer", description="收藏夹id"),
 *                          @OA\Property(property="title", type="string", description="标题"),
 *                          @OA\Property(property="count", type="integer", description="资源数量")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/favorites/detail",
 *      tags={"app-收藏夹"},
 *      summary="收藏夹详情",
 *      description="收藏夹详情",
 *      operationId="/app/favorites/detail",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="fav_id",
 *          in="query",
 *          description="收藏夹id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="fav_id", type="integer", description="收藏夹id"),
 *                          @OA\Property(property="title", type="string", description="标题"),
 *                          @OA\Property(property="count", type="integer", description="资源数量"),
 *                          @OA\Property(property="list", type="object",
 *                              @OA\Property(property="detail_id", type="integer", description="详情id"),
 *                              @OA\Property(property="resource_id", type="integer", description="资源id"),
 *                              @OA\Property(
 *                                  property="resource_type",
 *                                  type="integer",
 *                                  description="资源类型 1 表示视频
 * 2 表示 文献  3表示公告 4 直播视频 5大咖系列
 *     9最新解读"),
 *                              @OA\Property(property="title", type="string", description="资源标题"),
 *                              @OA\Property(property="author", type="string", description="作者"),
 *                              @OA\Property(property="img", type="string", description="图片"),
 *                              @OA\Property(property="sub_title", type="string", description="副标题"),
 *                              @OA\Property(property="tags", type="string", description="标签"),
 *                              @OA\Property(property="created_at", type="string", description="创建时间")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/favorites/add",
 *      tags={"app-收藏夹"},
 *      summary="添加收藏夹",
 *      description="添加收藏夹",
 *      operationId="/app/favorites/add",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="title",
 *          in="query",
 *          description="收藏夹标题",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="fav_id", type="integer", description="收藏夹id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/favorites/delete",
 *      tags={"app-收藏夹"},
 *      summary="删除收藏夹",
 *      description="删除收藏夹",
 *      operationId="/app/favorites/delete",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="fav_id",
 *          in="query",
 *          description="收藏夹id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="fav_id", type="integer", description="收藏夹id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/favorites/rename",
 *      tags={"app-收藏夹"},
 *      summary="重命名收藏夹",
 *      description="重命名收藏夹",
 *      operationId="/app/favorites/rename",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="fav_id",
 *          in="query",
 *          description="收藏夹id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="title",
 *          in="query",
 *          description="收藏夹标题",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="fav_id", type="integer", description="收藏夹id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/favorites/resource/add",
 *      tags={"app-收藏夹"},
 *      summary="向收藏夹添加资源",
 *      description="向收藏夹添加资源",
 *      operationId="/app/favorites/resource/add",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="fav_id",
 *          in="query",
 *          description="收藏夹id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="resource_id",
 *          in="query",
 *          description="资源id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="resource_type",
 *          in="query",
 *          description="资源类型1 表示视频  2 表示 文献  3表示公告 4 直播视频 5大咖系列  9最新解读",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="fav_id", type="integer", description="收藏夹id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Put (
 *      path="/app/favorites/resource/delete",
 *      tags={"app-收藏夹"},
 *      summary="收藏夹删除资源",
 *      description="收藏夹删除资源",
 *      operationId="/app/favorites/resource/delete",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="fav_id",
 *          in="query",
 *          description="收藏夹id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="resource_id",
 *          in="query",
 *          description="资源id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="resource_type",
 *          in="query",
 *          description="资源类型1 表示视频  2 表示 文献  3表示公告 4 直播视频 5大咖系列  9最新解读",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="fav_id", type="integer", description="收藏夹id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/favorites/checkResource",
 *      tags={"app-收藏夹"},
 *      summary="获取该资源的收藏夹id",
 *      description="获取该资源的收藏夹id",
 *      operationId="/app/favorites/checkResource",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="resource_id",
 *          in="query",
 *          description="资源id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="resource_type",
 *          in="query",
 *          description="资源类型1 表示视频  2 表示 文献  3表示公告 4 直播视频 5大咖系列  9最新解读",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="fav_id", type="integer", description="收藏夹id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Favorites;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Favorites\CheckResourceRequest;
use App\Request\App\Favorites\FavAddRequest;
use App\Request\App\Favorites\FavDetailRequest;
use App\Request\App\Favorites\FavRenameRequest;
use App\Request\App\Favorites\ResourceAddRequest;
use App\Request\App\Favorites\ResourceDelRequest;
use App\Service\Favorites\FavoritesRpcService;
use Grpc\Favorites\AddFavoritesReply;
use Grpc\Favorites\AddFavoritesRequest;
use Grpc\Favorites\AddResourceReply;
use Grpc\Favorites\AddResourceRequest;
use Grpc\Favorites\DeleteFavoritesReply;
use Grpc\Favorites\DeleteFavoritesRequest;
use Grpc\Favorites\DeleteResourceReply;
use Grpc\Favorites\DeleteResourceRequest;
use Grpc\Favorites\FavoritesDetailCollection;
use Grpc\Favorites\FavoritesDetailItem;
use Grpc\Favorites\FavoritesDetailReply;
use Grpc\Favorites\FavoritesDetailRequest;
use Grpc\Favorites\FavoritesListItem;
use Grpc\Favorites\FavoritesListReply;
use Grpc\Favorites\FavoritesListRequest;
use Grpc\Favorites\NameFavoritesReply;
use Grpc\Favorites\NameFavoritesRequest;
use Grpc\Favorites\ResourceInFavoritesReply;
use Grpc\Favorites\ResourceInFavoritesRequest;
use Hyperf\Di\Annotation\Inject;

class FavoritesController extends Controller
{
    /**
     * @Inject()
     * @var FavoritesRpcService
     */
    private $favoritesRpcService;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list()
    {
        $userId = Helper::getLoginUser()->uid;

        $favRequest = new FavoritesListRequest();
        $favRequest->setUserId($userId);
        $favRequest->setUserType(DataStatus::WECHAT_AGENT);

        /** @var FavoritesListReply $reply */
        $reply = $this->favoritesRpcService->getFavoritesList($favRequest);

        $list = [];
        /** @var FavoritesListItem $val */
        foreach ($reply->getList()->getIterator() as $val) {
            $list[] = [
                'fav_id' => $val->getFavId(),
                'title' => $val->getTitle(),
                'count' => $val->getCount()
            ];
        }

        return $this->response->success($list);
    }

    /**
     * @param FavDetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function detail(FavDetailRequest $request)
    {
        $favId = $request->input('fav_id');

        $favRequest = new FavoritesDetailRequest();
        $favRequest->setFavId($favId);

        /** @var FavoritesDetailReply  $reply */
        $reply = $this->favoritesRpcService->getFavoritesDetail($favRequest);

        $data = [];
        $data['fav_id'] = $reply->getFavId();
        $data['title'] = $reply->getTitle();
        $data['count'] = $reply->getCount();

        if ($reply->getMap()->count() !== 0) {
            /** @var FavoritesDetailCollection $v */
            foreach ($reply->getMap()->getIterator() as $v) {
                /** @var FavoritesDetailItem $value */
                foreach ($v->getList()->getIterator() as $key => $value) {
                    $data['list'][$key] = [
                        'detail_id' => $value->getDetailId(),
                        'resource_id' => $value->getResourceId(),
                        'resource_type' => $value->getResourceType(),
                        'title' => $value->getTitle(),
                        'author' => $value->getAuthor(),
                        'img' => $value->getImg(),
                        'sub_title' => $value->getSubTitle(),
                        'tags' => $value->getTags(),
                        'created_at' => $value->getCreatedAt()
                    ];
                }
            }
        } else {
            $data['list'] = [];
        }

        return $this->response->success($data);
    }

    /**
     * @param FavAddRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function add(FavAddRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $title = $request->input('title');

        $favRequest = new AddFavoritesRequest();
        $favRequest->setUserId($userId);
        $favRequest->setUserType(DataStatus::WECHAT_AGENT);
        $favRequest->setTitle($title);

        /** @var AddFavoritesReply  $reply */
        $reply = $this->favoritesRpcService->addFavorites($favRequest);

        return $this->response->success(['fav_id' => $reply->getFavId()]);
    }

    /**
     * @param FavDetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(FavDetailRequest $request)
    {
        $favId = $request->input('fav_id');

        $favRequest = new DeleteFavoritesRequest();
        $favRequest->setFavId($favId);

        /** @var DeleteFavoritesReply  $reply */
        $reply = $this->favoritesRpcService->deleteFavorites($favRequest);

        return $this->response->success(['fav_id' => $reply->getFavId()]);
    }

    /**
     * @param FavRenameRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function rename(FavRenameRequest $request)
    {
        $favId = $request->input('fav_id');
        $title = $request->input('title');

        $favRequest = new NameFavoritesRequest();
        $favRequest->setFavId($favId);
        $favRequest->setTitle($title);

        /** @var NameFavoritesReply  $reply */
        $reply = $this->favoritesRpcService->renameFavorites($favRequest);

        return $this->response->success(['fav_id' => $reply->getFavId()]);
    }

    /**
     * @param ResourceAddRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function resourceAdd(ResourceAddRequest $request)
    {
        $favId = $request->input('fav_id');
        $resourceId = $request->input('resource_id');
        $resourceType = $request->input('resource_type');

        $favRequest = new AddResourceRequest();
        $favRequest->setFavId($favId);
        $favRequest->setResourceId($resourceId);
        $favRequest->setResourceType($resourceType);

        /** @var AddResourceReply  $reply */
        $reply = $this->favoritesRpcService->addResource($favRequest);

        return $this->response->success(['fav_id' => $reply->getFavId()]);
    }

    /**
     * @param ResourceDelRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function resourceDelete(ResourceDelRequest $request)
    {
        $favId = $request->input('fav_id');
        $resourceId = $request->input('resource_id');
        $resourceType = $request->input('resource_type');

        $favRequest = new DeleteResourceRequest();
        $favRequest->setFavId($favId);
        $favRequest->setResourceId($resourceId);
        $favRequest->setResourceType($resourceType);

        /** @var DeleteResourceReply  $reply */
        $reply = $this->favoritesRpcService->deleteResource($favRequest);

        return $this->response->success(['fav_id' => $reply->getFavId()]);
    }

    /**
     * @param CheckResourceRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function checkResource(CheckResourceRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $resourceId = $request->input('resource_id');
        $resourceType = $request->input('resource_type');

        $favRequest = new ResourceInFavoritesRequest();
        $favRequest->setUserId($userId);
        $favRequest->setUserType(DataStatus::WECHAT_AGENT);
        $favRequest->setResourceId($resourceId);
        $favRequest->setResourceType($resourceType);

        /** @var ResourceInFavoritesReply  $reply */
        $reply = $this->favoritesRpcService->getResourceInFavorites($favRequest);

        return $this->response->success(['fav_id' => $reply->getFavId()]);
    }
}
