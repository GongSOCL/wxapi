<?php


namespace App\Controller\App\Serve;

use App\Controller\Controller;
use App\Request\App\Serve\GetServeSeriesRequest;
use App\Request\App\Serve\InfoByHospitalRequest;
use App\Request\App\Serve\ServeApplyRequest;
use App\Service\Common\ProductService;
use App\Service\Common\ServeService;
use Psr\Http\Message\ResponseInterface;

class ServeController extends Controller
{
    /**
     *
     * @OA\Post(
     *      path="/app/serve/apply",
     *      tags={"app-医院"},
     *      summary="申请服务医院",
     *      description="申请服务医院",
     *      operationId="AppServeApply",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="series_id", type="integer", description="系列药品id"),
     *                  @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                  required={"series_id", "hospital_id"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ServeApplyRequest $request
     * @return ResponseInterface
     */
    public function apply(ServeApplyRequest $request): ResponseInterface
    {
        $hospitalId = (int) $request->post('hospital_id');
        $seriesId = (int) $request->post('series_id');

        $serve = ServeService::apply($hospitalId, $seriesId);

        return $this->response->success($serve);
    }

    /**
     *
     * @OA\Post(
     *      path="/app/serve/{id}/remove",
     *      tags={"app-医院"},
     *      summary="解绑申请服务",
     *      description="解绑申请服务",
     *      operationId="AppServeRemove",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="产品服务id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function remove($id): ResponseInterface
    {
        $id = (int) $id;
        ServeService::remove($id);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/serve/info-by-hospital",
     *      tags={"app-医院"},
     *      summary="获取医院对应的服务列表及详情",
     *      description="获取医院对应的服务列表及详情",
     *      operationId="AppServeInfoByHospital",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="hospital_id",
     *          in="query",
     *          description="医院id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="医院信息",
     *                              @OA\Property(property="hospital_name", type="string", description="医院名称"),
     *                              @OA\Property(property="hospital_level", type="string", description="医院等级"),
     *                              @OA\Property(property="hospital_addr", type="string", description="医院地理位置"),
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="医院产品列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="产品服务id"),
     *                                  @OA\Property(property="product_id", type="integer", description="产品id"),
     *                                  @OA\Property(property="name", type="string", description="服务产品名称"),
     *                                  @OA\Property(
     *                                      property="status",
     *                                      type="array",
     *                                      description="服务产品状态",
     *                                      @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(
     *                                              property="status",
     *                                              type="integer",
     *                                                  description="服务状态:只有状态是1可以解绑"),
     *                                          @OA\Property(property="desc", type="string", description="按钮文字"),
     *                                      )
     *                                  ),
     *                                  @OA\Property(property="star", type="integer", description="服务产品评级")
     *                              )
     *                          ),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param InfoByHospitalRequest $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function detailByHospital(InfoByHospitalRequest $request): ResponseInterface
    {
        $hospitalId = (int) $request->query('hospital_id');
        [$info, $detail] = ServeService::getServeProductDetailList($hospitalId);

        return $this->response->success([
            'info' => $info,
            'list' => $detail
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/serve/serve-product/{id}",
     *      tags={"app-医院"},
     *      summary="获取服务产品详情",
     *      description="获取服务产品详情",
     *      operationId="AppServeProductDetail",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="产品服务id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="overview",
     *                              type="object",
     *                              description="历史数据",
     *                              @OA\Property(property="total", type="integer", description="总计"),
     *                              @OA\Property(property="max", type="integer", description="单月最高"),
     *                              @OA\Property(property="avg", type="number", format="float", description="月平均"),
     *                              @OA\Property(property="month", type="string", example="2020-02", description="实际月份")
     *                          ),
     *                          @OA\Property(
     *                              property="chart",
     *                              type="object",
     *                              description="图表数据(过去6个月数据)",
     *                              @OA\Property(
     *                                  property="rows",
     *                                  type="array",
     *                                  description="数据行",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(
     *                                          property="date",
     *                                          type="string",
     *                                          example="19/11",
     *                                          description="日期"),
     *                                      @OA\Property(property="num", type="integer",  description="实际数量")
     *                                  )
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="current_month",
     *                              type="object",
     *                              description="当月数据",
     *                              @OA\Property(property="target", type="integer", description="潜力"),
     *                              @OA\Property(property="mom", type="number", format="float", description="环比"),
     *                              @OA\Property(property="yoy", type="number", format="float", description="同比"),
     *                              @OA\Property(property="total", type="integer", description="实际")
     *                          ),
     *                          @OA\Property(
     *                              property="last_three_month",
     *                              type="object",
     *                              description="过去3月内数据",
     *                              @OA\Property(property="target", type="integer", description="潜力"),
     *                              @OA\Property(property="mom", type="number", format="float", description="环比"),
     *                              @OA\Property(property="yoy", type="number", format="float", description="同比"),
     *                              @OA\Property(property="total", type="integer", description="实际"),
     *                              @OA\Property(property="title", type="string", example="12月-02月数据", description="标题")
     *                          ),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getServeProductDetail($id): ResponseInterface
    {
        $id = (int) $id;
        [$overviewData, $chartData, $lastMonthData, $lastThreeMonthData] = ServeService::getServeProductStatistics($id);

        return $this->response->success([
            'overview' => $overviewData,
            'chart' => $chartData,
            'current_month' => $lastMonthData,
            'last_three_month' => $lastThreeMonthData
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/serve/series",
     *      tags={"app-医院"},
     *      summary="获取服务系列药品列表",
     *      description="获取服务系列药品列表",
     *      operationId="AppServeSeriesList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="hospital_id",
     *          in="query",
     *          description="医院id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                              @OA\Property(
     *                                  property="list",
     *                                  type="array",
     *                                  description="药品列表",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="id", type="integer", description="系列药品id"),
     *                                      @OA\Property(property="name", type="string", description="系列药品名称")
     *                                  )
     *                              )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     * @throws \Exception
     */
    public function getServeSeries(GetServeSeriesRequest $request): ResponseInterface
    {
        $list = ProductService::getUserServeDrugSeries(
            (int) $request->query('hospital_id' , 0)
        );
        return $this->response->success([
            'list' => $list
        ]);
    }
}
