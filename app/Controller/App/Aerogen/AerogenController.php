<?php

/**
 *
 * @OA\Get (
 *      path="/app/aerogen/doctor/list",
 *      tags={"app-优云邀请"},
 *      summary="医生列表",
 *      description="医生列表",
 *      operationId="/app/aerogen/doctor/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="r_id", type="integer", description="好友关系id"),
 *                          @OA\Property(property="user_unionid", type="string", description="医生unionid"),
 *                          @OA\Property(property="nickname", type="string", description="昵称"),
 *                          @OA\Property(property="true_name", type="string", description="真实姓名"),
 *                          @OA\Property(property="headimgurl", type="string", description="头像"),
 *                          @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                          @OA\Property(property="link_doctor_id", type="integer", description="关联的医生id")
 *                      )
 *                  )
 *              )
 *      )
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/aerogen/drugstore/list",
 *      tags={"app-优云邀请"},
 *      summary="药店列表",
 *      description="药店列表",
 *      operationId="/app/aerogen/drugstore/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="drugstore_id", type="integer", description="药店id"),
 *                          @OA\Property(property="drugstore_name", type="string", description="药店名称")
 *                      )
 *                  )
 *              )
 *      )
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/aerogen/product/list",
 *      tags={"app-优云邀请"},
 *      summary="产品列表",
 *      description="产品列表",
 *      operationId="/app/aerogen/product/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="drug_id", type="integer", description="产品id"),
 *                          @OA\Property(property="drug_name", type="string", description="产品名称")
 *                      )
 *                  )
 *              )
 *      )
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/aerogen/share/info",
 *      tags={"app-优云邀请"},
 *      summary="分享信息",
 *      description="分享信息",
 *      operationId="/app/aerogen/share/info",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="title", type="integer", description="标题"),
 *                          @OA\Property(property="describe", type="string", description="描述"),
 *                          @OA\Property(property="img", type="string", description="图片"),
 *                          @OA\Property(property="url", type="string", description="地址")
 *                      )
 *                  )
 *              )
 *      )
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/aerogen/qrcode/doctor",
 *      tags={"app-优云邀请"},
 *      summary="医生二维码",
 *      description="医生二维码",
 *      operationId="/app/aerogen/qrcode/doctor",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="link_doctor_id",
 *          in="query",
 *          description="关联的医生id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="url", type="string", description="二维码地址")
 *                      )
 *                  )
 *              )
 *      )
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/aerogen/qrcode/drugstore",
 *      tags={"app-优云邀请"},
 *      summary="药店二维码",
 *      description="药店二维码",
 *      operationId="/app/aerogen/qrcode/drugstore",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="url", type="string", description="页面地址")
 *                      )
 *                  )
 *              )
 *      )
 * )
 */

declare(strict_types=1);

namespace App\Controller\App\Aerogen;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Aerogen\DoctorQrcodeRequest;
use App\Service\Aerogen\AerogenService;
use Hyperf\Di\Annotation\Inject;

class AerogenController extends Controller
{
    /**
     * @Inject()
     * @var AerogenService
     */
    private $aerogenService;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doctorList()
    {
        $agentYyid = Helper::getLoginUser()->yyid;
        $data = $this->aerogenService->getDoctorListByAgentYyid($agentYyid);

        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function drugstoreList()
    {
        $agentYyid = Helper::getLoginUser()->yyid;
        $data = $this->aerogenService->getDrugstoreListByAgentYyid($agentYyid);

        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function productList()
    {
        $agentYyid = Helper::getLoginUser()->yyid;
        $data = $this->aerogenService->getProductListByAgentYyid($agentYyid);

        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function shareInfo()
    {
        $agentYyid = Helper::getLoginUser()->yyid;
        $data = $this->aerogenService->getShareInfo($agentYyid);

        return $this->response->success($data);
    }

    /**
     * @param DoctorQrcodeRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function doctorQrcode(DoctorQrcodeRequest $request)
    {
        $agentYyid = Helper::getLoginUser()->yyid;
        $linkDoctorid = $request->input('link_doctor_id');
        $data = $this->aerogenService->getDoctorQrcode($agentYyid, $linkDoctorid);

        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function drugstoreQrcode()
    {
        $data = $this->aerogenService->getDrugstoreQrcode();

        return $this->response->success($data);
    }
}
