<?php

/**
 *
 * @OA\Get (
 *      path="/app/morning/getType",
 *      tags={"app-早安分享"},
 *      summary="图片类型",
 *      description="图片类型",
 *      operationId="/app/morning/getType",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="type_id", type="integer", description="类型id"),
 *                          @OA\Property(property="type", type="string", description="类型")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/morning/getDepartment",
 *      tags={"app-早安分享"},
 *      summary="图片部门",
 *      description="图片部门",
 *      operationId="/app/morning/getDepartment",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="department_id", type="integer", description="部门id"),
 *                          @OA\Property(property="department", type="string", description="部门")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/morning/picList",
 *      tags={"app-早安分享"},
 *      summary="图片列表",
 *      description="图片列表",
 *      operationId="/app/morning/picList",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="title",
 *          in="query",
 *          description="标题 不传默认全部",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="department_id",
 *          in="query",
 *          description="部门id 不传默认全部",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type_id",
 *          in="query",
 *          description="类型id 不传默认全部",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码 默认1",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面item数量 默认10",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="count", type="string", description="总数量"),
 *                          @OA\Property(property="page", type="string", description="页码"),
 *                          @OA\Property(property="page_size", type="string", description="页面item数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="id", type="integer", description="图片id"),
 *                              @OA\Property(property="title", type="string", description="标题"),
 *                              @OA\Property(property="department", type="string", description="部门"),
 *                              @OA\Property(property="type", type="string", description="类型"),
 *                              @OA\Property(property="img", type="string", description="图片链接"),
 *                              @OA\Property(property="count", type="integer", description="图片点击总次数"),
 *                              @OA\Property(property="time", type="string", description="图片停留总时长"),
 *                              @OA\Property(property="is_share", type="integer", description="我是否分享过 0-否 1-是"),
 *                              @OA\Property(property="my_count", type="integer", description="我的这张图片被点击次数")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/morning/picDetail",
 *      tags={"app-早安分享"},
 *      summary="图片详情",
 *      description="图片详情",
 *      operationId="/app/morning/picDetail",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="id",
 *          in="query",
 *          description="图片id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="id", type="integer", description="图片id"),
 *                          @OA\Property(property="title", type="string", description="标题"),
 *                          @OA\Property(property="department", type="string", description="部门"),
 *                          @OA\Property(property="type", type="string", description="类型"),
 *                          @OA\Property(property="img", type="string", description="图片链接"),
 *                          @OA\Property(property="count", type="integer", description="图片点击总次数"),
 *                          @OA\Property(property="time", type="string", description="图片停留总时长"),
 *                          @OA\Property(property="is_share", type="integer", description="我是否分享过 0-否 1-是"),
 *                          @OA\Property(property="my_count", type="integer", description="我的这张图片被点击次数")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/morning/countRank",
 *      tags={"app-早安分享"},
 *      summary="图片点击次数排行",
 *      description="图片点击次数排行",
 *      operationId="/app/morning/countRank",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="rank_type",
 *          in="query",
 *          description="排行类型 1-全部图片排行 2-仅统计我分享过的图片 默认1",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          in="query",
 *          description="排行榜的limit 默认前10",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="title", type="string", description="标题"),
 *                          @OA\Property(property="department", type="string", description="部门"),
 *                          @OA\Property(property="type", type="string", description="类型"),
 *                          @OA\Property(property="img", type="string", description="图片链接"),
 *                          @OA\Property(property="count", type="integer", description="图片点击次数")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/morning/timeRank",
 *      tags={"app-早安分享"},
 *      summary="图片停留时间排行",
 *      description="图片停留时间排行",
 *      operationId="/app/morning/timeRank",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="rank_type",
 *          in="query",
 *          description="排行类型 1-全部图片排行 2-仅统计我分享过的图片 默认1",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          in="query",
 *          description="排行榜的limit 默认前10",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="title", type="string", description="标题"),
 *                          @OA\Property(property="department", type="string", description="部门"),
 *                          @OA\Property(property="type", type="string", description="类型"),
 *                          @OA\Property(property="img", type="string", description="图片链接"),
 *                          @OA\Property(property="time", type="integer", description="图片停留时间")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);

namespace App\Controller\App\MorningShare;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\MorningShare\PicDetailRequest;
use App\Request\App\MorningShare\PicListRequest;
use App\Request\App\MorningShare\RankRequest;
use App\Service\MorningShare\MorningShareService;
use Hyperf\Di\Annotation\Inject;

class MorningShareController extends Controller
{
    /**
     * @Inject()
     * @var MorningShareService
     */
    private $shareService;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getType()
    {
        $data = $this->shareService->getType();

        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getDepartment()
    {
        $data = $this->shareService->getDepartment();

        return $this->response->success($data);
    }

    /**
     * @param PicListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function picList(PicListRequest $request)
    {
        $id = Helper::getLoginUser()->uid;
        $title = $request->input('title', '');
        $typeId = $request->input('type_id', '');
        $departmentId = $request->input('department_id', '');
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);
        $data = $this->shareService->getMonpic($id, $title, $typeId, $departmentId, $page, $pageSize);

        return $this->response->success($data);
    }

    /**
     * @param PicDetailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function picDetail(PicDetailRequest $request)
    {
        $id = Helper::getLoginUser()->uid;
        $picId = $request->input('id');
        $data = $this->shareService->getDetailById($id, $picId);

        return $this->response->success($data);
    }

    /**
     * @param RankRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function countRank(RankRequest $request)
    {
        $id = Helper::getLoginUser()->uid;
        $rankType = $request->input('rank_type', 1);
        $limit = $request->input('limit', DataStatus::PAGE_SIZE);

        $data = $this->shareService->getCountRank($id, $rankType, $limit);

        return $this->response->success($data);
    }

    /**
     * @param RankRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function timeRank(RankRequest $request)
    {
        $id = Helper::getLoginUser()->uid;
        $rankType = $request->input('rank_type');
        $limit = $request->input('limit');

        $data = $this->shareService->getTimeRank($id, $rankType, $limit);

        return $this->response->success($data);
    }
}
