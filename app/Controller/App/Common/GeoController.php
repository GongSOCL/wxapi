<?php
declare(strict_types=1);


namespace App\Controller\App\Common;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Request\Common\ReverseGeoRequests;
use App\Service\Common\GeoService;
use GuzzleHttp\Exception\GuzzleException;
use Hyperf\Utils\Str;
use Psr\Http\Message\ResponseInterface;

/**
 * 地理服务.
 * Class GeoController
 * @package App\Controller\Common
 */
class GeoController extends Controller
{
    /**
     *
     * @OA\Get  (
     *      path="/app/geo",
     *      tags={"app-地理位置"},
     *      summary="根据经纬度获取地理位置",
     *      description="地理位置",
     *      operationId="AppGeoDesc",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="纬度",
     *         in="query",
     *         name="lat",
     *         required=true,
     *         @OA\Schema(
     *              type="number",
     *              format="float"
     *          )
     *      ),
     *      @OA\Parameter(
     *         description="经度",
     *         in="query",
     *         name="lng",
     *         required=true,
     *         @OA\Schema(
     *              type="number",
     *              format="float"
     *          )
     *      ),
     *      @OA\Parameter(
     *         description="坐标体系: 默认gps 高德gcj-02",
     *         in="query",
     *         name="coordinate_type",
     *         required=false,
     *         @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="desc", type="string", description="详细地址"),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ReverseGeoRequests $requests
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function desc(ReverseGeoRequests $requests): ResponseInterface
    {
        $coordinateType = $requests->query('coordinate_type', DataStatus::COORDINATE_GPS);
        $location = GeoService::reverseGeo(
            $requests->query('lng'),
            $requests->query('lat'),
            $coordinateType
        );

        return $this->response->success([
           'desc' => $location
        ]);
    }
}
