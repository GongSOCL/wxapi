<?php


namespace App\Controller\App\Common;

use App\Controller\Controller;
use App\Request\App\Common\SmsVerifyRequest;
use App\Service\Common\MsgCallback;
use App\Service\Common\SmsService;
use Hyperf\Utils\ApplicationContext;
use Youyao\Framework\Components\Msg\MessageRpcClient;

class SmsController extends Controller
{

    /**
     *
     * @OA\Post(
     *      path="/app/sms/verify",
     *      tags={"app-公用"},
     *      summary="发送验证码短信",
     *      description="发送验证码短信",
     *      operationId="AppSendSms",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="mobile", type="string", description="手机号"),
     *                  @OA\Property(property="in_checking", type="integer", description="1-审核用账号 0或者不传-审核专用"),
     *                  required={"mobile"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function sendVerify(SmsVerifyRequest $request)
    {
        $mobile = (string) $request->post('mobile');
        $check = $request->input('in_checking');

        SmsService::sendVerify($mobile, $check);
        return $this->response->success([]);
    }
}
