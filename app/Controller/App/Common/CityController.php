<?php


namespace App\Controller\App\Common;

use App\Controller\Controller;
use App\Model\Qa\Region;
use App\Service\Common\CityService;
use Exception;
use Psr\Http\Message\ResponseInterface;

class CityController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/region/provinces",
     *      tags={"app-公用"},
     *      summary="获取省份列表",
     *      description="获取省份列表",
     *      operationId="AppProvinceList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="省份列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="省份id"),
     *                                  @OA\Property(property="name", type="string", description="省份名称")
     *                              )
     *                          ),
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function province(): ResponseInterface
    {
        $countryId = (int) env('CHINA_COUNTRY_ID', 1);
        $list = CityService::getNextRegion($countryId, Region::LEVEL_COUNTRY);

        return $this->response->success([
            'list' => $list
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/region/provinces/{id}/city",
     *      tags={"app-公用"},
     *      summary="获取省份下市级列表",
     *      description="获取省份下市级列表",
     *      operationId="AppProvinceCityList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="选择省份id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="市级列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="市级id"),
     *                                  @OA\Property(property="name", type="string", description="市级名称")
     *                              )
     *                          ),
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function city($id): ResponseInterface
    {
        $list = CityService::getNextRegion($id, Region::LEVEL_PROVINCE);

        return $this->response->success([
            'list' => $list
        ]);
    }
}
