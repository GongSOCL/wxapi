<?php

/**
 *
 * @OA\Get (
 *      path="/app/message/list",
 *      tags={"app-站内消息"},
 *      summary="消息列表",
 *      description="消息列表",
 *      operationId="/app/message/list",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="total", type="integer", description="消息列表总数"),
 *                          @OA\Property(property="unread_total", type="integer", description="未读消息数量"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="id", type="integer", description="id"),
 *                              @OA\Property(property="title", type="string", description="标题"),
 *                              @OA\Property(property="sub_title", type="string", description="副标题"),
 *                              @OA\Property(
 *                                  property="type",
 *                                  type="integer",
 *                                  description="消息的类型 1 医院的推荐  2表示平台求助 3表示平台消息公告
 *                                  4 数据更新提醒  5 系统回复用户的消息   6表示平台消息公告微信展示
 *                                  7日流向更新 8投票提醒 9获得积分 10消耗积分  11 问卷邀请  12 邀请加入群组 13签到消息"),
 *                              @OA\Property(property="link", type="string", description="链接"),
 *                              @OA\Property(property="paper_yyid", type="string", description="问卷yyid"),
 *                              @OA\Property(property="created_at", type="string", description="时间"),
 *                              @OA\Property(property="is_read", type="integer", description="是否已读0未读1已读")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/message/latest",
 *      tags={"app-站内消息"},
 *      summary="最近的一条消息",
 *      description="最近的一条消息",
 *      operationId="/app/message/latest",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="id", type="integer", description="id"),
 *                          @OA\Property(property="title", type="string", description="标题"),
 *                          @OA\Property(property="sub_title", type="string", description="副标题"),
 *                          @OA\Property(property="type", type="integer", description="消息的类型 1 医院的推荐  2表示平台求助 3表示平台消息公告
 *                                  4 数据更新提醒  5 系统回复用户的消息   6表示平台消息公告微信展示
 *                                  7日流向更新 8投票提醒 9获得积分 10消耗积分  11 问卷邀请  12 邀请加入群组 13签到消息"),
 *                          @OA\Property(property="body", type="string", description="消息内容"),
 *                          @OA\Property(property="link", type="string", description="链接"),
 *                          @OA\Property(property="paper_yyid", type="string", description="问卷yyid"),
 *                          @OA\Property(property="created_at", type="string", description="时间")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/message/detail",
 *      tags={"app-站内消息"},
 *      summary="消息详情",
 *      description="消息详情",
 *      operationId="/app/message/detail",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="msg_id",
 *          in="query",
 *          description="消息id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="platform",
 *          in="query",
 *          description="阅读消息平台类型。1代表公众号，2代表app，3医生公众号,4医生app,5代表小程序,6医生小程序",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="id", type="integer", description="id"),
 *                          @OA\Property(property="title", type="string", description="标题"),
 *                          @OA\Property(property="sub_title", type="string", description="副标题"),
 *                          @OA\Property(property="type", type="integer", description="消息的类型 1 医院的推荐  2表示平台求助 3表示平台消息公告
 *                                  4 数据更新提醒  5 系统回复用户的消息   6表示平台消息公告微信展示
 *                                  7日流向更新 8投票提醒 9获得积分 10消耗积分  11 问卷邀请  12 邀请加入群组 13签到消息"),
 *                          @OA\Property(property="body", type="string", description="消息内容"),
 *                          @OA\Property(property="link", type="string", description="链接"),
 *                          @OA\Property(property="paper_yyid", type="string", description="问卷yyid"),
 *                          @OA\Property(property="created_at", type="string", description="时间")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/message/readAll",
 *      tags={"app-站内消息"},
 *      summary="全部已读",
 *      description="全部已读",
 *      operationId="/app/message/readAll",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="platform",
 *          in="query",
 *          description="阅读消息平台类型。1代表公众号，2代表app，3医生公众号,4医生app,5代表小程序,6医生小程序",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="unread_count", type="integer", description="未读变已读数量")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Msg;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Msg\MessageLatestRequest;
use App\Request\App\Msg\MessageListRequest;
use App\Request\App\Msg\MessageReadAllRequest;
use App\Service\Msg\MsgRpcService;
use Grpc\InsideMsg\InsideMsgListCollection;
use Grpc\InsideMsg\InsideMsgListItem;
use Grpc\InsideMsg\InsideMsgListReply;
use Grpc\InsideMsg\InsideMsgListRequest;
use Grpc\InsideMsg\LatestOneRequest;
use Grpc\InsideMsg\OneMsgDetailReply;
use Grpc\InsideMsg\OneMsgDetailRequest;
use Grpc\InsideMsg\ReadAllMsgReply;
use Grpc\InsideMsg\ReadAllMsgRequest;
use Hyperf\Di\Annotation\Inject;

class MsgController extends Controller
{
    /**
     * @Inject()
     * @var MsgRpcService
     */
    private $msgRpcService;

    /**
     * @param MessageListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list(MessageListRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $userType = DataStatus::USER_TYPE_AGENT;
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', DataStatus::PAGE_SIZE);

        $msgRequest = new InsideMsgListRequest();
        $msgRequest->setUserId($userId);
        $msgRequest->setUserType($userType);
        $msgRequest->setPage($page);
        $msgRequest->setPageSize($pageSize);

        /** @var InsideMsgListReply $reply */
        $reply = $this->msgRpcService->fetchInsideMsgList($msgRequest);

        $data = [];
        $data['total'] = $reply->getTotal();
        $data['unread_total'] = $reply->getUnreadTotal();
        $data['page'] = $reply->getPage();
        $data['page_size'] = $reply->getPageSize();

        /** @var InsideMsgListCollection $v */
        if ($reply->getMap()->count() !== 0) {
            foreach ($reply->getMap()->getIterator() as $v) {
                /** @var InsideMsgListItem $value */
                foreach ($v->getList()->getIterator() as $key => $value) {
                    $data['list'][$key] = [
                        'id' => $value->getId(),
                        'title' => $value->getTitle(),
                        'sub_title' => $value->getSubTitle(),
                        'type' => $value->getType(),
                        'link' => $value->getLink(),
                        'paper_yyid' => $value->getPaperYyid(),
                        'created_at' => $value->getCreatedAt(),
                        'is_read' => $value->getIsRead()
                    ];
                }
            }
        } else {
            $data['list'] = [];
        }

        return $this->response->success($data);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function latest()
    {
        $userId = Helper::getLoginUser()->uid;
        $userType = DataStatus::USER_TYPE_AGENT;

        $msgRequest = new LatestOneRequest();
        $msgRequest->setUserId($userId);
        $msgRequest->setUserType($userType);

        /** @var OneMsgDetailReply $reply */
        $reply = $this->msgRpcService->fetchLatestOne($msgRequest);

        $data = [];
        if ($reply->getId()) {
            $data['id'] = $reply->getId();
            $data['title'] = $reply->getTitle();
            $data['sub_title'] = $reply->getSubTitle();
            $data['type'] = $reply->getType();
            $data['body'] = $reply->getBody();
            $data['link'] = $reply->getLink();
            $data['paper_yyid'] = $reply->getPaperYyid();
            $data['created_at'] = $reply->getCreatedAt();
        }

        return $this->response->success($data);
    }

    /**
     * @param MessageLatestRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function detail(MessageLatestRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $userType = DataStatus::USER_TYPE_AGENT;
        $msgId = $request->input('msg_id');
        $platform = $request->input('platform');

        $msgRequest = new OneMsgDetailRequest();
        $msgRequest->setMsgId($msgId);
        $msgRequest->setPlatform($platform);
        $msgRequest->setUserId($userId);
        $msgRequest->setUserType($userType);

        /** @var OneMsgDetailReply $reply */
        $reply = $this->msgRpcService->fetchOneMsgDetail($msgRequest);

        $data = [];
        $data['id'] = $reply->getId();
        $data['title'] = $reply->getTitle();
        $data['sub_title'] = $reply->getSubTitle();
        $data['type'] = $reply->getType();
        $data['body'] = $reply->getBody();
        $data['link'] = $reply->getLink();
        $data['paper_yyid'] = $reply->getPaperYyid();
        $data['created_at'] = $reply->getCreatedAt();

        return $this->response->success($data);
    }

    /**
     * @param MessageReadAllRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function readAll(MessageReadAllRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $userType = DataStatus::USER_TYPE_AGENT;
        $platform = $request->input('platform');

        $msgRequest = new ReadAllMsgRequest();
        $msgRequest->setPlatform($platform);
        $msgRequest->setUserId($userId);
        $msgRequest->setUserType($userType);

        /** @var ReadAllMsgReply $reply */
        $reply = $this->msgRpcService->readAllMsg($msgRequest);

        return $this->response->success(['unread_count' => $reply->getUnreadCount()]);
    }
}
