<?php


namespace App\Controller\App\Hospital;

use App\Controller\Controller;
use App\Model\Qa\YouyaoHospital;
use App\Request\App\Hospital\HospitalInfoRequest;
use App\Service\Common\HospitalService;
use Exception;
use Psr\Http\Message\ResponseInterface;

class HospitalController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/hospitals/search",
     *      tags={"app-医院"},
     *      summary="搜索医院列表",
     *      description="搜索医院列表",
     *      operationId="AppHospitalSearch",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="series_id",
     *          in="query",
     *          description="系列药品id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="province_id",
     *          in="query",
     *          description="省份id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="city_id",
     *          in="query",
     *          description="市id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="level",
     *          in="query",
     *          description="医院等级: 三级8 二级 4 一级2 未定级 1,值为选择项总和(sum(2^n))",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="transaction",
     *          in="query",
     *          description="进药状态: 2未进药 1已进药,值为选择项总和(sum(2^n))",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="can_apply",
     *          in="query",
     *          description="是否只过滤可申请医院:0否 1是",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="省份列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="医院id"),
     *                                  @OA\Property(property="name", type="string", description="医院名称"),
     *                                  @OA\Property(property="level", type="string", description="医院等级"),
     *                                  @OA\Property(property="transaction", type="string", description="进药状态"),
     *                                  @OA\Property(property="can_apply", type="integer", description="是否可申请: 0否 1是")
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function search(): ResponseInterface
    {
        $provinceId = (int) $this->request->query('province_id', 0);
        $cityId = (int) $this->request->query('city_id', 0);
        //提取等级
        $level = (int) $this->request->query('level');
        $seriesId = (int) $this->request->query('series_id', 0);
        $canApply = (int) $this->request->query('can_apply', 0);
        $transaction = (int) $this->request->query('transaction', 0);
        $keywords = (string) $this->request->query('keywords');
        $current = (int) $this->request->query('current', 1);
        $limit = (int) $this->request->query('limit', 10);
        //提取等级
        $extractLevels = self::extractLevelFromQuery($level);

        //提取进药状态
        $extractTransaction = self::extractTransaction($transaction);
        //搜索医院
        [$page, $data] = HospitalService::searchHospitals(
            $provinceId,
            $cityId,
            $extractLevels,
            $current,
            $limit,
            $seriesId,
            $extractTransaction,
            $keywords,
            boolval($canApply)
        );

        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    // 提取是否进药
    private static function extractTransaction($transaction): int
    {
        $trs = [];
        if ($transaction == 0) {
            return $transaction;
        }

        $map = [
            YouyaoHospital::HOSPITAL_TRANSACTION_YES,
            YouyaoHospital::HOSPITAL_TRANSACTION_NO
        ];
        foreach ($map as $v) {
            if (($transaction & $v) == $v) {
                $trs[] = $v;
            }
        }
        if (empty(array_diff($map, $trs))) {
            return 0;
        }

        return array_pop($trs);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/hospitals/{id}",
     *      tags={"app-医院"},
     *      summary="医院详情",
     *      description="医院详情",
     *      operationId="AppHospitalInfo",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="医院id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="series_id",
     *          in="query",
     *          description="系列药品id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="医院详情",
     *                              @OA\Property(property="name", type="string", description="医院名称"),
     *                              @OA\Property(property="level", type="string", description="医院等级"),
     *                              @OA\Property(property="can_apply", type="integer", description="是否可申请: 0否 1是"),
     *                              @OA\Property(property="addr", type="string", description="医院地址"),
     *                              @OA\Property(property="hospital_dean", type="string", description="主管院长"),
     *                              @OA\Property(property="bed_num", type="string", description="床位总数"),
     *                              @OA\Property(property="import_department", type="string", description="特色专科")
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function info($id, HospitalInfoRequest $request): ResponseInterface
    {
        $id = (int) $id;
        $seriesId = (int) $request->query('series_id', 0);
        $detail = HospitalService::getHospitalDrugDetail($id, $seriesId);

        return $this->response->success([
            'info' => $detail
        ]);
    }

    /**
     *
     * @OA\Get(
     *      path="/app/hospitals/serve",
     *      tags={"app-医院"},
     *      summary="服务医院列表",
     *      description="服务医院列表",
     *      operationId="AppHospitalServe",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *         description="搜索关键字",
     *         in="query",
     *         name="keywords",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *          name="province_id",
     *          in="query",
     *          description="省份id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="city_id",
     *          in="query",
     *          description="市id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="level",
     *          in="query",
     *          description="医院等级: 三级8 二级 4 一级2 未定级 1,值为选择项总和(sum(2^n))",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="page",
     *          in="query",
     *          description="页码",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="page_size",
     *          in="query",
     *          description="页面数量",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="服务医院列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="医院id"),
     *                                  @OA\Property(property="name", type="string", description="服务医院名称"),
     *                                  @OA\Property(property="org_type", type="integer", description="组织类型: 1 表格医院
     * 2表示药店   3表示经销商")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function serve(): ResponseInterface
    {
        $keywords = (string) $this->request->query('keywords');
        $province = (int)$this->request->query('province_id', 0);
        $city = (int)$this->request->query('city_id', 0);
        $level = (int)$this->request->query('level', 0);
        $page = $this->request->input('page', 1);
        $pageSize = $this->request->input('page_size', 10);
        //提取等级
        $extractLevels = self::extractLevelFromQuery($level);
        $hospitals = HospitalService::getUserServeHospitals(
            $province,
            $city,
            $extractLevels,
            $keywords,
            $page,
            $pageSize
        );

        return $this->response->success($hospitals);
    }

    //提取医院等级
    private function extractLevelFromQuery(int $levelId): array
    {
        $levels = [];
        if ($levelId == 0) {
            return $levels;
        }

        $map = YouyaoHospital::getAllLevels();

        foreach ($map as $v) {
            $pow = 1 << $v;
            if (($levelId & $pow) == $pow) {
                $levels[] = $v;
            }
        }

        return $levels;
    }
}
