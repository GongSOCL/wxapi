<?php
declare(strict_types=1);

namespace App\Controller\App\Hospital;

use App\Controller\Controller;
use App\Request\App\Hospital\AddServeDepartsRequest;
use App\Service\Common\DepartmentService;
use Exception;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\Validation\ValidationException;
use Psr\Http\Message\ResponseInterface;

class DepartmentController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/departments",
     *      tags={"app-医院"},
     *      summary="科室列表",
     *      description="科室列表",
     *      operationId="AppDepartmentList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="科室列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="科室id"),
     *                                  @OA\Property(property="name", type="string", description="科室名称")
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function list(): ResponseInterface
    {
        [$page, $data] = DepartmentService::listSystemDepartsWithPage(
            (string) $this->request->query('keywords'),
            (int) $this->request->query('current', 1),
            (int) $this->request->query('limit', 10)
        );

        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Put(
     *      path="/app/departments/serve",
     *      tags={"app-医院"},
     *      summary="用户添加服务科室",
     *      description="用户添加服务科室",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                  @OA\Property(
     *                      property="depart_ids",
     *                      type="array",
     *                      description="科室id",
     *                      @OA\Items(type="integer")
     *                 ),
     *                 required={"hospital_id", "depart_ids"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AddServeDepartsRequest $request
     * @return ResponseInterface
     */
    public function add(AddServeDepartsRequest $request): ResponseInterface
    {
        $hospitalId = $request->post('hospital_id');
        $departIds = $request->post('depart_ids');

        DepartmentService::addServe($hospitalId, $departIds);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/departments/serve",
     *      tags={"app-医院"},
     *      summary="用户医院下服务科室列表",
     *      description="用户医院下服务科室列表",
     *      operationId="AppServeDepartmentList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="hospital_id",
     *          in="query",
     *          description="医院id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="科室列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="科室id"),
     *                                  @OA\Property(property="name", type="string", description="科室名称")
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function getServeDepartmentList(): ResponseInterface
    {
        $hospitalId = $this->request->query('hospital_id', 0);
        $keywords = $this->request->query('keywords', "");
        $list = DepartmentService::getServeHospitalDeparts($hospitalId, $keywords);

        return $this->response->success([
            'list' => $list
        ]);
    }
}
