<?php

/**
 *
 * @OA\Post (
 *      path="/app/feedback/add",
 *      tags={"app-用户反馈"},
 *      summary="用户反馈",
 *      description="用户反馈",
 *      operationId="/app/feedback/add",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="platform",
 *          in="query",
 *          description="平台类型1-代表公众号 2-代表APP 3-医生公众号 4-医生APP",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="content",
 *          in="query",
 *          description="反馈描述",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="picurl",
 *          in="query",
 *          description="上传图片地址",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="contact_information",
 *          in="query",
 *          description="联系方式",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="feed_id", type="integer", description="反馈id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);

namespace App\Controller\App\Feedback;

use App\Constants\DataStatus;
use App\Helper\Helper;
use App\Request\App\Feedback\FeedbackAddRequest;
use App\Service\Feedback\FeedbackRpcService;
use Grpc\Feedback\AddFeedbackReply;
use Grpc\Feedback\AddFeedbackRequest;
use Hyperf\Di\Annotation\Inject;
use Youyao\Framework\AbstractAction;

class FeedbackController extends AbstractAction
{
    /**
     * @Inject()
     * @var FeedbackRpcService
     */
    private $feedbackRpcService;

    /**
     * @param FeedbackAddRequest $request
     * @return array
     */
    public function add(FeedbackAddRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $platform = $request->input('platform');
        $content = $request->input('content');
        $picurl = $request->input('picurl') ? $request->input('picurl') : '';
        $contact = $request->input('contact_information');

        $feedbackRequest = new AddFeedbackRequest();
        $feedbackRequest->setUserId($userId);
        $feedbackRequest->setUserType(DataStatus::USER_TYPE_AGENT);
        $feedbackRequest->setPlatform($platform);
        $feedbackRequest->setContent($content);
        $feedbackRequest->setPicurl($picurl);
        $feedbackRequest->setContactInformation($contact);

        /** @var AddFeedbackReply $reply */
        $reply = $this->feedbackRpcService->addFeedback($feedbackRequest);

        return $this->success([
            'feed_id' => $reply->getLiveId()
        ]);
    }
}
