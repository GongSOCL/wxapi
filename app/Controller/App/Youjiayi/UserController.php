<?php
declare(strict_types=1);
namespace App\Controller\App\Youjiayi;

use App\Controller\Controller;
use App\Service\Doctor\DoctorInfoRpcService;
use App\Service\Doctor\DoctorInfoService;
use Psr\Http\Message\ResponseInterface;

class UserController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/youjiayi/users",
     *      tags={"app-优佳医用户"},
     *      summary="优佳医用户搜索",
     *      description="优佳医用户搜索",
     *      operationId="AppYoujiayiUserSearch",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="搜索关键字",
     *         in="query",
     *         name="keywords",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *      @OA\Parameter(
     *         description="返回数量限制,默认10条",
     *         in="query",
     *         name="limit",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="用户列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="用户id"),
     *                                  @OA\Property(property="name", type="string", description="用户名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function search(): ResponseInterface
    {
        $keywords= (string) $this->request->query('keywords', '');
        $limit = (int) $this->request->query('limit', 10);
        $list = DoctorInfoService::searchDoctors($keywords, $limit);

        return $this->response->success([
            'list' => $list
        ]);
    }
}
