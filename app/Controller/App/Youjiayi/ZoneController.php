<?php
declare(strict_types=1);
namespace App\Controller\App\Youjiayi;

use App\Controller\Controller;
use App\Service\Meeting\MeetingService;
use Psr\Http\Message\ResponseInterface;

class ZoneController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/youjiayi/zone",
     *      tags={"app-优佳医大区"},
     *      summary="优佳医大区列表",
     *      description="优佳医大区列表",
     *      operationId="AppYoujiayiZoneList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="大区列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="大区id"),
     *                                  @OA\Property(property="name", type="string", description="大区名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function list(): ResponseInterface
    {
        $list = MeetingService::listAreas();

        return $this->response->success([
            'list' => $list
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/youjiayi/zone/{id}/province",
     *      tags={"app-优佳医大区"},
     *      summary="优佳医大区下省份列表",
     *      description="优佳医大区下省份列表",
     *      operationId="AppYoujiayiZoneProvinceList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="省份列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="省份id"),
     *                                  @OA\Property(property="name", type="string", description="省份名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function listProvince($id): ResponseInterface
    {
        $list = MeetingService::listProvince($id);
        return $this->response->success([
            'list' => $list
        ]);
    }
}
