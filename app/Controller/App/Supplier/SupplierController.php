<?php

/**
 *
 * @OA\Get (
 *      path="/app/supplier/getMemberRole",
 *      tags={"app-供应商"},
 *      summary="判断当前用户身份",
 *      description="判断当前用户是公司供应商组长还是成员还是个人供应商",
 *      operationId="/app/supplier/getMemberRole",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(
 *                              property="role",
 *                              type="integer",
 *                              description="1-供应商组长 2-供应商成员 4-代表 5-未认证未知身份"),
 *                          @OA\Property(property="supplier_id", type="integer", description="供应商id 0表示不是供应商"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/supplier/getGroupList",
 *      tags={"app-供应商"},
 *      summary="供应商群组成员列表",
 *      description="供应商群组成员列表",
 *      operationId="/app/supplier/getGroupList",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面列表数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="count", type="integer", description="总数量"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面item数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="member_id", type="integer", description="列表成员id"),
 *                              @OA\Property(property="true_name", type="string", description="姓名"),
 *                              @OA\Property(property="heading", type="string", description="头像"),
 *                              @OA\Property(property="is_leader", type="integer", description="0-组员 1-组长"),
 *                              @OA\Property(property="hospital_count", type="integer", description="代理医院数量")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/supplier/getJoinList",
 *      tags={"app-供应商"},
 *      summary="待加入列表",
 *      description="待加入列表",
 *      operationId="/app/supplier/getJoinList",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="页码",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page_size",
 *          in="query",
 *          description="页面列表数量",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="count", type="integer", description="总数量"),
 *                          @OA\Property(property="page", type="integer", description="页码"),
 *                          @OA\Property(property="page_size", type="integer", description="页面item数量"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="member_id", type="integer", description="列表成员id"),
 *                              @OA\Property(property="true_name", type="string", description="姓名"),
 *                              @OA\Property(property="heading", type="string", description="头像"),
 *                              @OA\Property(property="join_time", type="string", description="申请时间")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/supplier/join",
 *      tags={"app-供应商"},
 *      summary="从待加入列表加入群组",
 *      description="从待加入列表加入群组",
 *      operationId="/app/supplier/join",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="member_id",
 *          in="query",
 *          description="接口返回的member_id",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="member_id", type="integer", description="成员id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/supplier/removeMember",
 *      tags={"app-供应商"},
 *      summary="移出群组或退出群组",
 *      description="移出群组或退出群组",
 *      operationId="/app/supplier/removeMember",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="member_id",
 *          in="query",
 *          description="接口返回的member_id",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="member_id", type="integer", description="成员id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/supplier/dismissGroup",
 *      tags={"app-供应商"},
 *      summary="解散群组",
 *      description="解散群组",
 *      operationId="/app/supplier/dismissGroup",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="res", type="integer", description="1-解散成功")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/supplier/getLeaderCode",
 *      tags={"app-供应商"},
 *      summary="获取邀请人id",
 *      description="获取邀请人id",
 *      operationId="/app/supplier/getLeaderCode",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="supplier_id", type="integer", description="邀请者所在供应商id"),
 *                          @OA\Property(property="inviate_aid", type="integer", description="邀请者id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/supplier/addJoinList",
 *      tags={"app-供应商"},
 *      summary="加入待加入列表",
 *      description="加入待加入列表",
 *      operationId="/app/supplier/addJoinList",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="supplier_id",
 *          in="query",
 *          description="供应商id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="inviate_aid",
 *          in="query",
 *          description="邀请人id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="member_id", type="integer", description="成员id"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/supplier/getMemberDetail",
 *      tags={"app-供应商"},
 *      summary="成员当前绑定状态",
 *      description="成员当前绑定状态",
 *      operationId="/app/supplier/getMemberDetail",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="member_id",
 *          in="query",
 *          description="接口返回的member_id",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="true_name", type="string", description="姓名"),
 *                          @OA\Property(property="heading", type="string", description="头像"),
 *                          @OA\Property(property="supplier_title", type="string", description="供应商名称"),
 *                          @OA\Property(
 *                              property="data",
 *                              type="object",
 *                              @OA\Property(property="bind_id", type="integer", description="绑定id"),
 *                              @OA\Property(property="product_name", type="string", description="产品名称"),
 *                              @OA\Property(property="hospital_name", type="string", description="医院名称")
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/supplier/unbindMember",
 *      tags={"app-供应商"},
 *      summary="成员解除药品和医院绑定",
 *      description="成员解除药品和医院绑定",
 *      operationId="/app/supplier/unbindMember",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="bind_id",
 *          in="query",
 *          description="接口返回的bind_id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object"
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/supplier/hospitalProductForBind",
 *      tags={"app-供应商"},
 *      summary="当前可绑定的医院和药品",
 *      description="当前可绑定的医院和药品",
 *      operationId="/app/supplier/hospitalProductForBind",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="memberid",
 *          in="query",
 *          description="成员id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="keyword",
 *          in="query",
 *          description="关键词",
 *          required=false,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="hospital_id", type="integer", description="医院id"),
 *                          @OA\Property(property="hospital_name", type="string", description="医院名称"),
 *                          @OA\Property(property="product_id", type="integer", description="产品id"),
 *                          @OA\Property(property="product_name", type="string", description="产品名称")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/supplier/bindMember",
 *      tags={"app-供应商"},
 *      summary="成员绑定药品和医院",
 *      description="成员绑定药品和医院",
 *      operationId="/app/supplier/bindMember",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="product_id",
 *          in="query",
 *          description="接口返回的product_id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="hospital_id",
 *          in="query",
 *          description="接口返回的hospital_id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="member_id",
 *          in="query",
 *          description="接口返回的member_id",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="bind_id", type="integer", description="绑定信息id")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Supplier;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Supplier\AddJoinListRequest;
use App\Request\App\Supplier\BindMemberRequest;
use App\Request\App\Supplier\HospitalProductForBindRequest;
use App\Request\App\Supplier\MemberListRequest;
use App\Request\App\Supplier\RemoveMemberRequest;
use App\Request\App\Supplier\UnbindMemberRequest;
use App\Service\Supplier\SupplierService;
use Hyperf\Di\Annotation\Inject;

class SupplierController extends Controller
{
    /**
     * @Inject()
     * @var SupplierService
     */
    private $supplierService;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getMemberRole()
    {
        $userYyid = Helper::getLoginUser()->yyid;
        $data = $this->supplierService->getMemberRole($userYyid);

        return $this->response->success($data);
    }

    /**
     * @param MemberListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getGroupList(MemberListRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);
        $data = $this->supplierService->getGroupList($userId, $page, $pageSize);

        return $this->response->success($data);
    }

    /**
     * @param MemberListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getJoinList(MemberListRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $page = $request->input('page', 1);
        $pageSize = $request->input('page_size', 10);
        $data = $this->supplierService->getJoinList($userId, $page, $pageSize);

        return $this->response->success($data);
    }

    /**
     * @param RemoveMemberRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function join(RemoveMemberRequest $request)
    {
        $memberId = $request->input('member_id');
        $this->supplierService->join($memberId);
        return $this->response->success(['member_id' => $memberId]);
    }

    /**
     * @param RemoveMemberRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function removeMember(RemoveMemberRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $memberId = $request->input('member_id', 0);
        $this->supplierService->removeMember($userId, $memberId);
        return $this->response->success(['member_id' => $memberId]);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function dismissGroup()
    {
        $userId = Helper::getLoginUser()->uid;
        $this->supplierService->dismissGroup($userId);
        return $this->response->success([]);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getLeaderCode()
    {
        $userYyid = Helper::getLoginUser()->yyid;
        $userId = Helper::getLoginUser()->uid;
        $data = $this->supplierService->getLeaderCode($userYyid, $userId);

        return $this->response->success($data);
    }

    /**
     * @param AddJoinListRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function addJoinList(AddJoinListRequest $request)
    {
        $userYyid = Helper::getLoginUser()->yyid;
        $userId = Helper::getLoginUser()->uid;
        $supplierId = $request->input('supplier_id');
        $aid = $request->input('invite_aid');

        $data = $this->supplierService->addJoinList($userYyid, $userId, $supplierId, $aid);

        return $this->response->success($data);
    }

    /**
     * @param RemoveMemberRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getMemberDetail(RemoveMemberRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $memberId = $request->input('member_id', 0);
        $data = $this->supplierService->getMemberDetail($userId, $memberId);

        return $this->response->success($data);
    }

    /**
     * @param UnbindMemberRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function unbindMember(UnbindMemberRequest $request)
    {
        $userYyid = Helper::getLoginUser()->yyid;
        $userId = Helper::getLoginUser()->uid;
        $bindId = $request->input('bind_id');
        $data = $this->supplierService->unbindMember($bindId, $userId, $userYyid);

        return $this->response->success($data);
    }

    /**
     * @param HospitalProductForBindRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function hospitalProductForBind(HospitalProductForBindRequest $request)
    {
        $keyword = $request->input('keyword');
        $memberId = $request->input('memberId');
        $data = $this->supplierService->hospitalProductForBind($memberId, $keyword);

        return $this->response->success($data);
    }

    /**
     * @param BindMemberRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function bindMember(BindMemberRequest $request)
    {
        $userYyid = Helper::getLoginUser()->yyid;
        $userId = Helper::getLoginUser()->uid;
        $memberId = $request->input('member_id', 0);
        $productId = $request->input('product_id');
        $hospitalId = $request->input('hospital_id');
        $this->supplierService->bindMember($userId, $memberId, $productId, $hospitalId, $userYyid);

        return $this->response->success([]);
    }
}
