<?php

/**
 *
 * @OA\Get (
 *      path="/app/supplier/getReport",
 *      tags={"app-供应商"},
 *      summary="获取报表",
 *      description="获取报表",
 *      operationId="/app/supplier/getReport",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="supplier_id",
 *          in="query",
 *          description="供应商id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date",
 *          in="query",
 *          description="月份格式（例：2022-06）",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data1",
 *                          type="object",
 *                          description="管理员",
 *                          @OA\Property(property="total", type="integer", description="总计"),
 *                          @OA\Property(property="h", type="integer", description="医院拜访"),
 *                          @OA\Property(property="s", type="integer", description="药店拜访"),
 *                          @OA\Property(property="b", type="integer", description="商业拜访"),
 *                          @OA\Property(property="e", type="integer", description="其他拜访"),
 *                          @OA\Property(
 *                              property="list",
 *                              type="object",
 *                              @OA\Property(property="true_name", type="integer", description="姓名"),
 *                              @OA\Property(property="h", type="integer", description="医院拜访"),
 *                              @OA\Property(property="s", type="integer", description="药店拜访"),
 *                              @OA\Property(property="b", type="integer", description="商业拜访"),
 *                              @OA\Property(property="e", type="integer", description="其他拜访")
 *                          )
 *                      ),
 *                      @OA\Property(
 *                          property="data2",
 *                          type="object",
 *                          description="成员和个人供应商",
 *                          @OA\Property(property="true_name", type="integer", description="姓名"),
 *                          @OA\Property(property="h", type="integer", description="医院拜访"),
 *                          @OA\Property(property="s", type="integer", description="药店拜访"),
 *                          @OA\Property(property="b", type="integer", description="商业拜访"),
 *                          @OA\Property(property="e", type="integer", description="其他拜访"),
 *                          @OA\Property(property="type_1", type="integer", description="面对面拜访"),
 *                          @OA\Property(property="type_2", type="integer", description="线上拜访"),
 *                          @OA\Property(property="type_3", type="integer", description="内部会议"),
 *                          @OA\Property(property="type_4", type="integer", description="外部会议"),
 *                          @OA\Property(property="type_5", type="integer", description="行政类工作")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Get (
 *      path="/app/supplier/exportReport",
 *      tags={"app-供应商"},
 *      summary="获取报表发送邮件",
 *      description="获取报表发送邮件",
 *      operationId="/app/supplier/exportReport",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="supplier_id",
 *          in="query",
 *          description="供应商id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date",
 *          in="query",
 *          description="月份格式（例：2022-06）",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="email",
 *          in="query",
 *          description="邮箱",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="res", type="string", description="邮件发送成功"),
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Supplier;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Supplier\ExportRequest;
use App\Request\App\Supplier\ReportRequest;
use App\Service\Supplier\ReportService;
use Hyperf\Di\Annotation\Inject;

class ReportController extends Controller
{
    /**
     * @Inject()
     * @var ReportService
     */
    private $reportService;

    /**
     * @param ReportRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getReport(ReportRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $supplierId = $request->input('supplier_id');
        $date = $request->input('date');
        $data = $this->reportService->getReport($supplierId, $userId, $date);
        return $this->response->success($data);
    }

    /**
     * @param ReportRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function exportReport(ExportRequest $request)
    {
        $userId = Helper::getLoginUser()->uid;
        $supplierId = $request->input('supplier_id');
        $date = $request->input('date');
        $email = $request->input('email');
        $data = $this->reportService->exportReport($supplierId, $userId, $date, $email);
        return $this->response->success($data);
    }
}
