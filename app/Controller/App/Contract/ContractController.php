<?php

namespace App\Controller\App\Contract;

use App\Controller\Controller;
use App\Model\Qa\OaUserSign;
use App\Request\App\Contract\GetContractTplRequest;
use App\Request\App\Contract\ListContractRequest;
use App\Request\App\Contract\MailContractRequest;
use App\Request\App\Contract\SignContractRequest;
use App\Service\Contract\ContractService;
use App\Service\Contract\Items\ContractGroupService;
use Psr\Http\Message\ResponseInterface;

class ContractController extends Controller
{
    /**
     *
     * @OA\Get(
     *      path="/app/contracts/group",
     *      tags={"app-合同"},
     *      summary="合同列表",
     *      description="合同列表",
     *      operationId="AppContractList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="状态: 0待签署 1生效中 2已到期,默认0",
     *         in="query",
     *         name="status",
     *         required=true,
     *         @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="msg_id",
     *          in="query",
     *          description="消息id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="sign_count", type="integer", description="待签署总数量"),
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                           @OA\Property(
     *                             property="groups",
     *                             type="array",
     *                             description="合同组列表",
     *                             @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="组id"),
     *                                  @OA\Property(property="name", type="string", description="合同组名称"),
     *                                  @OA\Property(
     *                                      property="pdf_generated",
     *                                      type="integer",
     *                                      description="合同pdf是否已经生成: 1是 0否"),
     *                                  @OA\Property(
     *                                      property="contracts",
     *                                      type="array",
     *                                      description="合同列表",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="合同id"),
     *                                          @OA\Property(property="name", type="string", description="合同名称"),
     *                                           @OA\Property(property="no", type="string", description="合同编号"),
     *                                          @OA\Property(
     *                                              property="start",
     *                                              type="string",
     *                                              description="合同生效开始时间", example="2022/01/01"),
     *                                          @OA\Property(
     *                                              property="end",
     *                                              type="string",
     *                                              description="合同生效结束时间", example="2022/12/31"),
     *                                          @OA\Property(property="pic", type="string", description="合同缩略图url")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="is_signed",
     *                                      type="integer",
     *                                      description="是否已签署: 0未签署 1已签署")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function list(ListContractRequest $request): ResponseInterface
    {
        [$page, $groups, $signCount] = ContractService::contractGroupList(
            (int)$request->query('status', 0),
            (int)$request->query('current', 1),
            (int)$request->query('limit', 10),
            (int)$request->query('msg_id', 0)
        );

        return $this->response->success([
            'sign_count' => $signCount,
            'groups' => $groups,
            'page' => $page
        ]);
    }


    /**
     *
     * @OA\Post  (
     *      path="app/contracts/group/{id}/start",
     *      tags={"app-合同"},
     *      summary="开始签署合同",
     *      description="开始签署合同",
     *      operationId="ContractStartSign",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="合同组id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="next_contract_id", type="integer", description="第一份合同id"),
     *                          @OA\Property(property="request_id", type="string", description="请求id,后面答题需要带上"),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function start($id): ResponseInterface
    {
        [$requestId, $nextId] = ContractService::startContractSign((int)$id);
        return $this->response->success([
            'next_contract_id' => $nextId,
            'request_id' => $requestId
        ]);
    }

    /**
     *
     * @OA\Get  (
     *      path="app/contracts/group/{id}/tpl",
     *      tags={"app-合同"},
     *      summary="获取合同模板html",
     *      description="获取合同html模板",
     *      operationId="GetContractTplWithNext",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="合同组id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="contract_id",
     *          in="query",
     *          description="合同id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="request_id",
     *          in="query",
     *          description="请求id",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="content", type="string", description="合同内容"),
     *                          @OA\Property(property="next_contract_id", type="integer", description="下一份合同id")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function getGroupContractTpl($id, GetContractTplRequest $request): ResponseInterface
    {
        /** @var OaUserSign $contract */
        /** @var OaUserSign $next */
        [$content, $nextId] = ContractService::getContractTpl(
            (int)$id,
            (int)$request->query('contract_id'),
            (string)$request->query('request_id')
        );

        return $this->response->success([
            'content' => $content,
            'next_contract_id' => $nextId
        ]);
    }

    /**
     *
     * @OA\Get  (
     *      path="app/contracts/{id}/pdf",
     *      tags={"app-合同"},
     *      summary="获取签署后的合同pdf",
     *      description="获取签署后的合同pdf",
     *      operationId="GetContractPdf",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="合同id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="pdf", type="string", description="合同pdf链接")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function getContractPdf($id): ResponseInterface
    {
        $link = ContractService::getContractSignedPdf((int)$id);
        return $this->response->success([
            'pdf' => $link
        ]);
    }

    /**
     *
     * @OA\Get  (
     *      path="app/contracts/{id}/tpl",
     *      tags={"app-合同"},
     *      summary="获取合同模板详情",
     *      description="获取合同模板详情",
     *      operationId="GetContractOriginTpl",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="合同id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="content", type="string", description="合同模板详情html")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param integer $id
     * @return ResponseInterface
     */
    public function getContractTpl($id): ResponseInterface
    {
        $content = ContractService::getContractTplInfo((int)$id);
        return $this->response->success([
            'content' => $content
        ]);
    }

    /**
     *
     * @OA\Post  (
     *      path="app/contracts/group/{id}/sign",
     *      tags={"app-合同"},
     *      summary="签署合同",
     *      description="签署合同",
     *      operationId="ContractSign",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="合同组id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="sign_img", type="string", description="图片base64数据"),
     *                     @OA\Property(property="request_id", type="string", description="请求id,后面答题需要带上"),
     *                     @OA\Property(property="code", type="string", description="验证码"),
     *                     required={"sign_img", "request_id", "code"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="content", type="string", description="合同模板详情html")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param integer $id
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sign($id, SignContractRequest $request): ResponseInterface
    {
        $img = $request->post('sign_img');
        ContractService::signContract(
            (int)$id,
            $img,
            (string)$request->post('request_id'),
            (string)$request->post('code')
        );
        return $this->response->success([]);
    }


    /**
     *
     * @OA\Post  (
     *      path="app/contracts/group/{id}/send-mail",
     *      tags={"app-合同"},
     *      summary="发送模板组合同pdf到邮箱",
     *      description="发送模板组合同pdf到邮箱",
     *      operationId="ContractSign",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="合同组id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="mail", type="string", description="接收合同pdf邮箱地址"),
     *                     required={"mail"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param integer $id
     * @return ResponseInterface
     */
    public function sendMail($id, MailContractRequest $request): ResponseInterface
    {
        ContractService::sendContractWithMail(
            (int)$id,
            (string)$request->post('mail')
        );
        return $this->response->success();
    }
}
