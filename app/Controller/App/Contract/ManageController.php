<?php

namespace App\Controller\App\Contract;

use App\Constants\ErrorCode;
use App\Controller\Controller;
use App\Exception\BusinessException;
use App\Request\App\Contract\AddContractGroupRequest;
use App\Request\App\Contract\AddYouyaoAttachmentContractRequest;
use App\Request\App\Contract\AddYouyaoMasterContractRequest;
use App\Request\App\Contract\AddYouyaoUserAgreeContractRequest;
use App\Request\App\Contract\ContractCallbackRequest;
use App\Service\Contract\ManageService;
use Psr\Http\Message\ResponseInterface;

class ManageController extends Controller
{
    /**
     *
     * @OA\Post  (
     *      path="/app/contract-manage/generate-html",
     *      tags={"app-合同"},
     *      summary="请求生成合同组html模板",
     *      description="请求生成合同组html模板",
     *      operationId="GenerateContractHtmlTpl",
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="group_id", type="integer", description="合同组id"),
     *                     required={"group_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function generateHTML(): ResponseInterface
    {
        $groupId = (int)$this->request->post('group_id');
        if ($groupId <= 0) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "合同组id不能小于0");
        }
        ManageService::generateHtml($groupId);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post  (
     *      path="/app/contract-manage/cb-tpl-generated",
     *      tags={"app-合同"},
     *      summary="完成单份合同模板生成回调",
     *      description="完成单份合同模板生成回调",
     *      operationId="GenerateContractHtmlTplCallback",
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="id", type="integer", description="合同id"),
     *                     @OA\Property(property="link", type="string", description="生成模板html文件链接"),
     *                     @OA\Property(property="is_success", type="int", description="生成是否成功"),
     *                     required={"id", "link", "is_success"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function tplGenerated(ContractCallbackRequest $request): ResponseInterface
    {
        ManageService::htmlGenerated(
            (int)$request->post('id'),
            (int)$request->post('is_success') == 1,
            (string)$request->post('link')
        );
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post  (
     *      path="/app/contract-manage/cb-pdf-generated",
     *      tags={"app-合同"},
     *      summary="完成单份合同签名pdf生成回调",
     *      description="完成单份合同签名pdf生成回调",
     *      operationId="GenerateContractHtmlTplCallback",
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="id", type="integer", description="合同id"),
     *                     @OA\Property(property="link", type="string", description="生成模板html文件链接"),
     *                     @OA\Property(property="is_success", type="int", description="生成是否成功"),
     *                     required={"id", "link", "is_success"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param ContractCallbackRequest $request
     * @return ResponseInterface
     */
    public function pdfGenerated(ContractCallbackRequest $request): ResponseInterface
    {
        ManageService::pdfGenerated(
            (int)$request->post('id'),
            (int)$request->post('is_success') == 1,
            (string)$request->post('link')
        );
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post  (
     *      path="/app/contract-manage/add-group",
     *      tags={"app-合同"},
     *      summary="添加合同组-仅测试",
     *      description="添加合同组-仅测试",
     *      operationId="AddContractGroup",
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="start", type="string", description="合同组开始时间"),
     *                     @OA\Property(property="end", type="string", description="合同组结束时间"),
     *                     @OA\Property(property="sign_end", type="string", description="合同组签署过期日期,不传时和合同组结束时间相同"),
     *                     @OA\Property(property="user_id", type="int", description="分配用户id"),
     *                     @OA\Property(property="title", type="string", description="合同组标题"),
     *                     required={"start", "end", "user_id", "title"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="创建合同组id")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AddContractGroupRequest $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function addContractGroup(AddContractGroupRequest $request): ResponseInterface
    {
        $start = (new \DateTime($request->post('start')))->format('Y-m-d');
        $end = (new \DateTime($request->post('end')))->format('Y-m-d');
        $signEnd = (string) $request->post('sign_end', '');
        if ($signEnd) {
            $signEnd = (new \DateTime($signEnd))->format('Y-m-d');
        }
        $group = ManageService::addContractGroup(
            (string)$request->post('title'),
            (int)$request->post('user_id'),
            $start,
            $end,
            $signEnd
        );
        return $this->response->success([
            'id' => $group->id
        ]);
    }

    /**
     *
     * @OA\Post  (
     *      path="/app/contract-manage/add-youyao-master",
     *      tags={"app-合同"},
     *      summary="添加主合同-仅测试",
     *      description="添加主合同-仅测试",
     *      operationId="AddYouyaoMasterContract",
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="group_id", type="integer", description="合同组id"),
     *                     @OA\Property(property="title", type="int", description="合同标题"),
     *                     @OA\Property(property="is_old", type="int", description="是否旧合同"),
     *                     required={"group_id", "title"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="创建合同组id")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AddYouyaoMasterContractRequest $request
     * @return ResponseInterface
     */
    public function addYouyaoMasterContract(AddYouyaoMasterContractRequest $request): ResponseInterface
    {
        $contract = ManageService::addYouyaoMaster(
            $request->post('group_id'),
            (string)$request->post('title'),
            $request->post('is_old', 0) == 1
        );
        return $this->response->success([
            'id' => $contract->id
        ]);
    }

    /**
     *
     * @OA\Post  (
     *      path="/app/contract-manage/add-youyao-attachment",
     *      tags={"app-合同"},
     *      summary="添加附件合同-仅测试",
     *      description="添加附件合同-仅测试",
     *      operationId="AddYouyaoAttachmentContract",
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="group_id", type="integer", description="合同组id"),
     *                     @OA\Property(property="title", type="string", description="合同标题"),
     *                     @OA\Property(property="is_old", type="int", description="是否旧合同"),
     *                     @OA\Property(property="percent", type="integer", description="达成率"),
     *                     @OA\Property(
     *                          property="settlement_date",
     *                          type="string",
     *                          description="结束日期: 非必填,默认当年最后一天格式2022-12-30"),
     *                     @OA\Property(
     *                          property="serves",
     *                          type="array",
     *                          description="服务列表",
     *                          @OA\Items(
     *                              type="object",
     *                              @OA\Property(property="serve_id", type="integer", description="服务id,申请服务后返回"),
     *                              @OA\Property(property="carryon_num", type="integer", description="存量"),
     *                              @OA\Property(
     *                                  property="target",
     *                                  type="array",
     *                                  description="指标",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="month", type="string", description="月份,格式是2022-04"),
     *                                      @OA\Property(property="num", type="integer", description="指标数量")
     *                                  )
     *                              )
     *                          )
     *                      ),
     *                     required={"group_id", "title", "serves", "percent"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="创建合同组id"),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AddYouyaoAttachmentContractRequest $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function addYouyaoAttachment(AddYouyaoAttachmentContractRequest $request): ResponseInterface
    {
        $serves = $this->request->post('serves');
        foreach ($serves as $k => $v) {
            foreach ($v['target'] as $kk => $kv) {
                $serves[$k]['target'][$kk]['month'] = (\DateTime::createFromFormat('Y-m', $kv['month']))
                    ->format('Y-m');
            }
        }
        $settlementDate = (string) $request->post('settlement_date', '');
        if ($settlementDate) {
            $settlementDate = (new \DateTime($settlementDate))->format('Y-m-d');
        }
        $feeUnit = $request->post('fee_unit');
        $feeUnit = bcadd($feeUnit, 0.0, 2);
        $contract = ManageService::addYouyaoAttachment(
            $request->post('group_id'),
            (string)$request->post('title'),
            $serves,
            $feeUnit,
            (int)$request->post('percent'),
            $settlementDate,
            $request->post('is_old', 0) == 1
        );
        return $this->response->success([
            'id' => $contract->id
        ]);
    }

    /**
     *
     * @OA\Post  (
     *      path="/app/contract-manage/add-youyao-agree",
     *      tags={"app-合同"},
     *      summary="添加知情同意书-仅测试",
     *      description="添加知情同意书-仅测试",
     *      operationId="AddUserAgreeContract",
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="group_id", type="integer", description="合同组id"),
     *                     @OA\Property(property="title", type="int", description="合同标题"),
     *                     @OA\Property(property="is_old", type="int", description="是否旧合同"),
     *                     required={"group_id", "title"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="创建合同组id")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AddYouyaoUserAgreeContractRequest $request
     * @return ResponseInterface
     */
    public function addUserAgree(AddYouyaoUserAgreeContractRequest $request): ResponseInterface
    {
        $contract = ManageService::addUserAgree(
            (int)$request->post('group_id'),
            (string)$request->post('title'),
            $request->post('is_old', 0) == 1
        );
        return $this->response->success([
            'id' => $contract->id
        ]);
    }
}
