<?php
declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\App\Learning;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Group\Work\JoinAddRequest;
use App\Request\App\Learning\KnowledgeRequest;
use App\Request\App\Learning\LearningRecordRequest;
use App\Service\Learning\LearningService;
use Hyperf\Di\Annotation\Inject;

class LearningController extends Controller
{
    /**
     * @Inject()
     * @var LearningService
     */
    private $learningService;

    /**
     * @Inject()
     * @var LearningRecordRequest
     */
    private $learningRecordRequest;


    /**
     *
     * @OA\Get (
     *      path="/learning/knowledge",
     *      tags={"学习计划"},
     *      summary="知识库列表",
     *      description="知识库列表",
     *      operationId="knowledgeList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="path",
     *          description="知识库名称关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="pid",
     *          in="path",
     *          description="上级文件目录id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="id",     type="integer", description="id"),
     *                          @OA\Property(property="pid",    type="integer", description="父类id"),
     *                          @OA\Property(property="title",  type="string", description="文件名称"),
     *                          @OA\Property(property="type",   type="integer", description="文件类型1目录 2文件"),
     *                          @OA\Property(property="file_type",   type="integer", description="文件类型1视频 2文档"),
     *                          @OA\Property(property="file_address",   type="string", description="文件地址"),
     *                          @OA\Property(property="introduce",      type="string", description="文件描述"),
     *                          @OA\Property(property="sort",   type="integer", description="排序"),
     *                          @OA\Property(property="created_at",   type="string", description="创建日期"),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 知识库列表 return array
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function knowledgeList()
    {
        $keywords   = $this->request->query('keywords', '');
        $pid   = $this->request->query('pid', '');
        $data       = $this->learningService->getKnowledgeList($keywords, $pid);
        return $this->response->success($data);
    }



    /**
     * @OA\Get (
     *      path="/learning/knowledge/{id:\d+}",
     *      tags={"学习计划"},
     *      summary="知识库详情列表",
     *      description="知识库详情列表",
     *      operationId="knowledgeInfo",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="文件id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="type",
     *          in="path",
     *          description="文件类型1目录2文件",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="learn_id",
     *          in="path",
     *          description="计划id【当在学习页 点击资料学习时传且type=2】",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(
     *                              @OA\Property(
    *                                   property="info",
    *                                   type="object",
    *                                   description="列表",
    *                                       @OA\Property(property="id",     type="integer", description="id"),
    *                                       @OA\Property(property="pid",    type="integer", description="父类id"),
    *                                       @OA\Property(property="title",  type="string", description="文件名称"),
    *                                       @OA\Property(property="type",   type="integer", description="文件类型1目录 2文件"),
    *                                       @OA\Property(
     *                                          property="file_type",
     *                                          type="integer",
     *                                          description="文件类型1视频 2文档"),
    *                                       @OA\Property(property="file_address",   type="string", description="文件地址"),
    *                                       @OA\Property(property="introduce",      type="string", description="文件描述"),
    *                                       @OA\Property(property="sort",   type="integer", description="排序"),
    *                                       @OA\Property(property="created_at",   type="string", description="创建日期"),
    *                                       @OA\Property(
     *                                          property="video_pass_time",
     *                                          type="integer",
     *                                          description="视频通过时长"),
    *                                       @OA\Property(
     *                                          property="video_total_time",
     *                                          type="integer",
     *                                          description="视频总时长"),
    *                                   )
    *                               ),
     *                          @OA\Property(property="read_time",     type="integer", description="累计学习时间"),
     *                          @OA\Property(property="read",     type="integer", description="已读时间"),
     *                          @OA\Property(
     *                              property="page",
     *                              type="integer",
     *                              description="页码 当type=2【视频】时 该值为0。接收id时回传大于等于0"),
     *                          @OA\Property(property="m_status",    type="integer", description="资料状态1未开始2进行中3已完成"),
     *                  )
     *              )
     *          )

     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 知识库详情
     * @param JoinAddRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function knowledgeInfo($id, KnowledgeRequest $request)
    {
        $id     = (int)$id;
        $type   = (int) $this->request->query('type', '');
        $learn_id   = (int) $this->request->query('learn_id', '');
        $result = $this->learningService->getKnowledgeInfo($id, $type, $learn_id);
        return $this->response->success($result);
    }



    /**
     *
     * @OA\Get (
     *      path="/learning/plan",
     *      tags={"学习计划"},
     *      summary="学习时长",
     *      description="学习时长",
     *      operationId="learningPlan",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="to_day_learn_minute", type="integer", description="今日学习时间"),
     *                          @OA\Property(property="learn_tota_minute", type="integer", description="累计总时间"),
     *                          @OA\Property(property="continuit_days", type="integer", description="连续学习天数"),
     *                          @OA\Property(property="finsh_num", type="integer", description="完成数")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function learningPlan()
    {
        $result = $this->learningService->getLearningPlan();
        return $this->response->success($result);
    }


    /**
     * 学习时长相关
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getLearningPlan()
    {
        $result = $this->learningService->getLearningPlan();
        return $this->response->success($result);
    }






    /**
     *
     * @OA\Post (
     *      path="/learning/plan/read/record",
     *      tags={"学习计划"},
     *      summary="记录信息",
     *      description="记录信息",
     *      operationId="learningPlanReadRecord",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="type",
     *          in="path",
     *          description="文件类型1视频2文件",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="plan_id",
     *          in="path",
     *          description="计划id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="mid",
     *          in="path",
     *          description="资料id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="learn_time",
     *          in="path",
     *          description="时长",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="page",
     *          in="path",
     *          description="页码",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="status",
     *          in="path",
     *          description="当前视频/文档是否完成 1完成【当资料学习完成时】 2阅读中",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="状态",
     *                          @OA\Property(property="res", type="bool", description="状态")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 记录信息
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function learningPlanReadRecord(LearningRecordRequest $learningRecordRequest)
    {
        $type       = (int) $learningRecordRequest->input('type', 0);
        $plan_id    = (int) $learningRecordRequest->input('plan_id', 0);
        $mid        = (int) $learningRecordRequest->input('mid', 0);
        $learn_time     = (int) $learningRecordRequest->input('learn_time', 0);
        $page           = (int) $learningRecordRequest->input('page', 0);
        $status         = (int) $learningRecordRequest->input('status', 0);

//        var_dump($type.'-'.$plan_id.'-'.$mid.'-'.$learn_time.'-'.$page.'-'.$status);
        $result = $this->learningService->setLearningPlanReadRecord($type, $plan_id, $mid, $learn_time, $page, $status);
        return $this->response->success(['res'=>$result]);
    }






    /**
     *
     * @OA\Get (
     *      path="/learning/plan/{id:\d+}",
     *      tags={"学习计划"},
     *      summary="计划详情列表",
     *      description="计划详情列表",
     *      operationId="planInfo",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="计划id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(
     *                              @OA\Property(
     *                                   property="data",
     *                                   type="object",
     *                                   description="资料信息",
     *                                       @OA\Property(property="id",     type="integer", description="资料id"),
     *                                       @OA\Property(property="l_id",    type="integer", description="计划id"),
     *                                       @OA\Property(property="title",  type="string", description="文件名称"),
     *                                       @OA\Property(
     *                                          property="type",
     *                                          type="integer",
     *                                          description="知识库类型1目录 2文件"),
     *                                       @OA\Property(
     *                                          property="file_address",
     *                                          type="string",
     *                                          description="文件地址"),
     *                                       @OA\Property(
     *                                          property="introduce",
     *                                          type="string",
     *                                          description="文件描述"),
     *                                       @OA\Property(
     *                                          property="sort",
     *                                          type="integer",
     *                                          description="排序"),
     *                                       @OA\Property(
     *                                          property="file_type",
     *                                          type="integer",
     *                                          description="文件类型1视频2文档"),
     *                                       @OA\Property(
     *                                          property="status",
     *                                          type="integer",
     *                                          description="资料1未开始 2学习中 3完成"),
     *                                       @OA\Property(
     *                                          property="video_total_time",
     *                                          type="integer",
     *                                          description="视频总时长"),
     *                                       @OA\Property(
     *                                          property="video_pass_time",
     *                                          type="integer",
     *                                          description="视频通过时长"),
     *                                   ),
     *                                @OA\Property(
     *                                   property="plan",
     *                                   type="object",
     *                                   description="计划",
     *                                       @OA\Property(property="describe",     type="string", description="描述"),
     *                                       @OA\Property(property="dt_on",    type="string", description="开始时间"),
     *                                       @OA\Property(property="dt_off",  type="string", description="结束时间"),
     *                                       @OA\Property(
     *                                          property="date_type",
     *                                          type="integer",
     *                                          description="时间类型 1固定时长字段开始 2固定时长手动 3固定时间 4无限时"),
     *                                       @OA\Property(
     *                                          property="name",
     *                                          type="string",
     *                                          description="计划名"),
     *                                       @OA\Property(
     *                                          property="check_status",
     *                                          type="integer",
     *                                          description="计划状态"),
     *                                       @OA\Property(
     *                                          property="speed",
     *                                          type="string",
     *                                          description="计划进度"),
     *                                       @OA\Property(
     *                                          property="exam_id",
     *                                          type="string",
     *                                          description="考试id【若存在则有考试】"),
     *                                       @OA\Property(property="exam_name",   type="string", description="考试名称"),
     *                                   ),
     *                               ),
     *                          @OA\Property(property="speed",     type="integer", description="百分比"),
     *                          @OA\Property(property="total_tima",    type="integer", description="累计总时长"),
     *                          @OA\Property(property="today_learn_minute",    type="integer", description="今日学习时长"),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function planInfo($id)
    {
        $id     = (int)$id;
        $result = $this->learningService->getPlanInfo($id);
        return $this->response->success($result);
    }






    /**
     *
     * @OA\Get (
     *      path="/learning/plan/list/dt",
     *      tags={"学习计划"},
     *      summary="计划列表",
     *      description="计划列表",
     *      operationId="planList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="status",
     *          in="path",
     *          description="筛选状态【空全部 1进行中 2未开始 3完成 4过期】",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(property="id",     type="integer", description="id"),
     *                          @OA\Property(property="name",    type="integer", description="计划名"),
     *                          @OA\Property(property="dt_on",    type="integer", description="时间"),
     *                          @OA\Property(property="dt_off",  type="string", description="时间"),
     *                          @OA\Property(property="date_type",   type="integer", description="时间类型"),
     *                          @OA\Property(property="describe",   type="string", description="描述"),
     *                          @OA\Property(property="status",   type="integer", description="是否阅读中-对资料"),
     *                          @OA\Property(property="check_status",   type="integer", description="1进行中2未开始3完成4过期"),
     *                          @OA\Property(property="speed",   type="string", description="进度【百分】"),
     *                          @OA\Property(property="aexam_id",   type="string", description="考试id【默认0-无考试】"),
     *                          @OA\Property(property="exam_name",   type="string", description="考试名称】"),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function planList()
    {
        $st   = (int) $this->request->query('status', '');
        $result = $this->learningService->getPlanList($st, '');
        return $this->response->success($result);
    }






    /**
     *
     * @OA\Get (
     *      path="/learning/keep",
     *      tags={"学习计划"},
     *      summary="继续学习",
     *      description="继续学习",
     *      operationId="keepLearn",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="plan_id",
     *          in="path",
     *          description="当前所属学习计划的id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          description="列表",
     *                          @OA\Property(
     *                              @OA\Property(
     *                                   property="info",
     *                                   type="object",
     *                                   description="列表",
     *                                       @OA\Property(property="id",     type="integer", description="id"),
     *                                       @OA\Property(property="pid",    type="integer", description="父类id"),
     *                                       @OA\Property(property="title",  type="string", description="文件名称"),
     *                                       @OA\Property(
     *                                          property="type",
     *                                          type="integer",
     *                                          description="知识库类型1目录 2文件"),
     *                                       @OA\Property(
     *                                          property="file_type",
     *                                          type="integer",
     *                                          description="文件类型1视频2文档"),
     *                                       @OA\Property(property="file_address",   type="string", description="文件地址"),
     *                                       @OA\Property(property="introduce",      type="string", description="文件描述"),
     *                                       @OA\Property(property="sort",   type="integer", description="排序"),
     *                                       @OA\Property(property="created_at",   type="integer", description="创建时间"),
     *                                   ),
     *                                @OA\Property(
     *                                   property="speed",
     *                                   type="object",
     *                                   description="资料进度详情",
     *                                       @OA\Property(
     *                                          property="learn_time",
     *                                          type="integer",
     *                                          description="阅读时间"),
     *                                       @OA\Property(property="page",   type="integer", description="页码"),
     *                                       @OA\Property(property="type",   type="integer", description="类型"),
     *                                   )
     *                               ),
     *                          @OA\Property(property="status",    type="integer", description="该资料是否完成，1学习中 3 完成"),
     *                  )
     *              )
     *          )

     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * 继续学习
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function keepLearn()
    {
        $plan_id   = (int) $this->request->query('plan_id', 0);
        $result = $this->learningService->getKeepLearnInfo($plan_id);
        return $this->response->success($result);
    }
}
