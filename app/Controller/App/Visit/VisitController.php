<?php
declare(strict_types=1);


namespace App\Controller\App\Visit;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Controller\Controller;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\AgentVisit;
use App\Request\App\Visit\FakeHospitalPositionRequest;
use App\Request\App\Visits\AddToListRequest;
use App\Request\App\Visits\AgentVisiDoctorListRequest;
use App\Request\App\Visits\AgentVisitListRequest;
use App\Request\App\Visits\CheckGeoRequest;
use App\Request\App\Visits\CheckInRequest;
use App\Request\App\Visits\CheckOutRequest;
use App\Request\App\Visits\GetDepartsRequest;
use App\Request\App\Visits\GetVisitConfigRequest;
use App\Request\Visit\AgentVisiDoctorAddRequest;
use App\Service\Visit\VisitService;
use Exception;
use Psr\Http\Message\ResponseInterface;

class VisitController extends Controller
{

    /**
     *
     * @OA\Post (
     *      path="/app/visits",
     *      tags={"app-代表拜访"},
     *      summary="拜访列表",
     *      description="代表拜访",
     *      operationId="AppAgentVisitList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="check_in_start_at", type="string", description="搜索开始时间"),
     *                  @OA\Property(property="check_in_end_at", type="string", description="搜索结束时间"),
     *                  @OA\Property(property="check_out_start_at", type="string", description="搜索开始时间"),
     *                  @OA\Property(property="check_out_end_at", type="string", description="搜索结束时间"),
     *                  @OA\Property(property="create_start_at", type="string", description="签入创建搜索开始时间"),
     *                  @OA\Property(property="create_end_at", type="string", description="签入创建搜索时间"),
     *                  @OA\Property(property="hospital_id", type="integer", description="搜索医院id"),
     *                  @OA\Property(
     *                      property="office_id",
     *                      type="array",
     *                      description="搜索科室id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="check_type", type="integer", description="拜访类型: 0所有 1签入 2签出"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 0所有 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                  @OA\Property(property="current", type="integer", description="当前页: 默认1"),
     *                  @OA\Property(property="limit", type="integer", description="每页显示的数量:默认10"),
     *                  required={}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="拜访记录列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="拜访id"),
     *                                  @OA\Property(property="date", type="string", description="拜访时间"),
     *                                  @OA\Property(
     *                                      property="check_type",
     *                                      type="integer",
     *                                      description="记录类型: 1签入 2签出"),
     *                                  @OA\Property(property="type", type="string", description="拜访类型"),
     *                                  @OA\Property(property="hopital", type="string", description="医院名称"),
     *                                  @OA\Property(property="state", type="integer", description="是否有效的拜访0有效1无效"),
     *                                  @OA\Property(
     *                                      property="state_comment",
     *                                       type="string",
     *                                      description="关于是否有效的拜访的说明"),
     *                                  @OA\Property(
     *                                      property="departs",
     *                                      type="array",
     *                                      description="科室名称列表",
     *                                      @OA\Items(type="string")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="doctors",
     *                                      type="array",
     *                                      description="医生列表",
     *                                      @OA\Items(type="string")
     *                                  )
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AgentVisitListRequest $request
     * @return ResponseInterface
     * @throws Exception
     */
    public function list(AgentVisitListRequest $request): ResponseInterface
    {
        //获取并格式化时间字段
        $checkInStartAt = $request->post('check_in_start_at', '');
        $checkInEndAt = $request->post('check_in_end_at', '');
        $checkOutStartAt = $request->post('check_out_start_at', '');
        $checkOutEndAt = $request->post('check_out_end_at', '');
        $createStartAt = $request->post('create_start_at', '');
        $createEndAt = $request->post('create_end_at', '');
        $formatStr = "Y-m-d 00:00:00";
        $endFormatStr = "Y-m-d 23:59:59";
        $departIds = $request->post('office_id', []);
        if ($checkInStartAt) {
            $checkInStartAt = Helper::formatInputDate($checkInStartAt, $formatStr);
        }
        if ($checkInEndAt) {
            $checkInEndAt = Helper::formatInputDate($checkInEndAt, $endFormatStr);
        }
        if ($checkOutStartAt) {
            $checkOutStartAt = Helper::formatInputDate($checkOutStartAt, $formatStr);
        }
        if ($checkOutEndAt) {
            $checkOutEndAt = Helper::formatInputDate($checkOutEndAt, $endFormatStr);
        }
        if ($checkInStartAt && $checkInEndAt) {
            [$checkInStartAt, $checkInEndAt] = Helper::exchangeDate($checkInStartAt, $checkInEndAt);
        }
        if ($checkOutEndAt && $checkOutStartAt) {
            [$checkOutStartAt, $checkOutEndAt] = Helper::exchangeDate($checkOutStartAt, $checkOutEndAt);
        }
        if ($createStartAt) {
            $createStartAt = Helper::formatInputDate($createStartAt, $formatStr);
        }
        if ($createEndAt) {
            $createEndAt = Helper::formatInputDate($createEndAt, $endFormatStr);
        }
        $checkType = $request->post('check_type', 0);
        $visitType = $request->post('visit_type', 0);
        [$page, $data] = VisitService::list(
            $checkInStartAt,
            $checkInEndAt,
            $checkOutStartAt,
            $checkOutEndAt,
            $checkType,
            $visitType,
            intval($request->post('hospital_id', 0)),
            $departIds,
            intval($request->post('current', 1)),
            intval($request->post('limit', 10)),
            $createStartAt,
            $createEndAt
        );
        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/visits/new-check-out-info/{id}",
     *      tags={"app-代表拜访"},
     *      summary="签出新增时获取拜访记录详情",
     *      description="代表拜访",
     *      operationId="AppAgentVisitNewCheckOut",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="访问记录id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="详情",
     *                              @OA\Property(property="id", type="integer", description="上传id"),
     *                              @OA\Property(property="is_supplement", type="integer", description="是否补签: 1是 0否"),
     *                              @OA\Property(
     *                                  property="hospital",
     *                                  type="object",
     *                                  description="医院信息",
     *                                  @OA\Property(property="id", type="integer", description="医院id"),
     *                                  @OA\Property(property="name", type="string", description="医院名称"),
     *                                  @OA\Property(
     *                                      property="org_type",
     *                                      type="integer",
     *                                      description="1 表格医院  2表示药店   3表示经销商")
     *                              ),
     *                              @OA\Property(
     *                                  property="visit_type",
     *                                  type="integer",
     *                                  description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function newCheckOutInfo($id): ResponseInterface
    {
        $info = VisitService::getNewCheckOutInfo($id);
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/visits/{id}",
     *      tags={"app-代表拜访"},
     *      summary="签入或签出修改时获取拜访记录详情",
     *      description="签入或签出修改时获取拜访记录详情",
     *      operationId="AppAgentVisitInfo",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="访问记录id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="详情",
     *                              @OA\Property(property="id", type="integer", description="上传id"),
     *                              @OA\Property(
     *                                  property="hospital",
     *                                  type="object",
     *                                  description="医院信息",
     *                                  @OA\Property(property="id", type="integer", description="医院id"),
     *                                  @OA\Property(property="name", type="string", description="医院名称"),
     *                                  @OA\Property(
     *                                      property="org_type",
     *                                      type="integer",
     *                                      description="1 表格医院  2表示药店   3表示经销商")
     *                              ),
     *                              @OA\Property(
     *                                  property="visit_type",
     *                                  type="integer",
     *                                  description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                              @OA\Property(property="visit_status", type="integer", description="1已签入 2已签出"),
     *                              @OA\Property(
     *                                  property="is_collaborative",
     *                                  type="integer",
     *                                  description="是否协访: 0未选择 1是 2否"),
     *                              @OA\Property(
     *                                  property="departs",
     *                                  type="array",
     *                                  description="科室列表",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="id", type="integer", description="科室id"),
     *                                      @OA\Property(property="name", type="string", description="科室名称")
     *                                  )
     *                              ),
     *                              @OA\Property(
     *                                  property="doctors",
     *                                  type="array",
     *                                  description="医生列表",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="id", type="integer", description="医生信息表id"),
     *                                      @OA\Property(property="name", type="string", description="医生名称")
     *                                  )
     *                              ),
     *                              @OA\Property(
     *                                  property="series_ids",
     *                                  type="array",
     *                                  description="产品id列表",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="id", type="integer", description="药品id"),
     *                                      @OA\Property(property="name", type="string", description="药品名称")
     *                                  )
     *                              ),
     *                              @OA\Property(property="result", type="string", description="拜访结果"),
     *                              @OA\Property(property="next", type="string", description="后续跟进事宜"),
     *                              @OA\Property(
     *                                  property="pics",
     *                                  type="array",
     *                                  description="图片列表:已签入时为签入图片列表 已签出时为签出图片列表",
     *                                  @OA\Items(
     *                                      type="object",
     *                                      @OA\Property(property="id", type="integer", description="图上上传id"),
     *                                      @OA\Property(property="name", type="string", description="上传图片名称"),
     *                                      @OA\Property(property="url", type="string", description="上传图片url"),
     *                                  )
     *                              ),
     *                              @OA\Property(property="is_supplement", type="integer", description="是否补签: 1是 0否"),
     *                              @OA\Property(property="lat", type="number", description="纬度"),
     *                              @OA\Property(property="lng", type="number", description="经度"),
     *                              @OA\Property(property="is_fake_geo", type="integer", description="是否随机位置, 0否 1是 默认0"),
     *                              @OA\Property(
     *                                  property="coordinate_type",
     *                                  type="string",
     *                                  description="坐标体系: 默认gps 高德gcj-02"),
     *                              @OA\Property(property="position", type="string", description="位置描述"),
     *                              @OA\Property(
     *                                  property="geo_opt",
     *                                  type="integer",
     *                                  description="超出范围选项id,默认传0表示未超出范围"),
     *                              @OA\Property(property="geo_comment", type="string", description="超出选项范围输入内容"),
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function info($id): ResponseInterface
    {
        $info = VisitService::info($id);
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Put(
     *      path="/app/visits/check-in",
     *      tags={"app-代表拜访"},
     *      summary="代表拜访签入",
     *      description="代表拜访签入",
     *      operationId="AppAgentVisitCheckIn",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                  @OA\Property(
     *                      property="pics",
     *                      type="array",
     *                      description="上传图片id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="lat", type="number", format="float", description="纬度"),
     *                  @OA\Property(property="lng", type="number", format="float", description="经度"),
     *                  @OA\Property(property="coordinate_type", type="string", description="坐标体系: 默认gps 高德gcj-02"),
     *                  @OA\Property(property="is_fake_geo", type="integer", description="是否随机位置, 0否 1是 默认0"),
     *                  @OA\Property(property="position", type="string", description="位置描述"),
     *                  @OA\Property(property="geo_opt", type="integer", description="地理位置超出选项id,当调用检查接口返回未通过时需要"),
     *                  @OA\Property(property="geo_comment", type="string", description="地理位置超出填空内容，当调用检查接口返回未通过时需要"),
     *                  @OA\Property(property="special_id", type="integer", description="异常签入选项id, 默认不传为0"),
     *                  @OA\Property(
     *                      property="special_comment",
     *                      type="string",
     *                      description="异常签入选项备注,当speical_id不为0时必传"),
     *                  @OA\Property(property="check_in_time", type="string", description="签入时间, 补签才传，否则不传"),
     *                  required={
     *                      "hospital_id",
     *                      "visit_type",
     *                      "lat",
     *                      "lng"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="新加拜访记录id")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param CheckInRequest $request
     * @return ResponseInterface
     * @throws Exception
     */
    public function checkIn(CheckInRequest $request): ResponseInterface
    {
        $coordinateType = $request->post('coordinate_type', DataStatus::COORDINATE_GPS);
        list($pics, $visitType, $hospitalId) = $this->checkInParamValidate($request);
        $checkInTime = (string) $request->post('check_in_time', '');
        if ($checkInTime) {
            $checkInTime = Helper::formatInputDate($checkInTime);
            if ($checkInTime > date('Y-m-d H:i:s')) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '补签时间不能晚于当前时间');
            }
        }

        $visit = VisitService::checkIn(
            $visitType,
            $hospitalId,
            $pics,
            $request->post('lat'),
            $request->post('lng'),
            $request->post('position'),
            (int)$request->post('geo_opt', 0),
            $request->post('geo_comment', ""),
            $coordinateType,
            (int)$request->post('special_id', 0),
            (string)$request->post('special_comment', ''),
            $checkInTime,
            $request->post('is_fake_geo', 0) == 1
        );

        return $this->response->success([
            'id' => $visit->id
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/visits/check-geo",
     *      tags={"app-代表拜访"},
     *      summary="代表拜访地理位置范围检查",
     *      description="代表拜访地理位置范围检查",
     *      operationId="AppAgentVisitCheckGeo",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                  @OA\Property(property="lat", type="number", format="float",  description="纬度"),
     *                  @OA\Property(property="lng", type="number", format="float",  description="经度"),
     *                  @OA\Property(property="coordinate_type", type="string",  description="坐标系统:默认gps"),
     *                  required={"hospital_id", "lat", "lng"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="result", type="integer", description="检查结果: 1在范围内 0不在范围内")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param CheckGeoRequest $request
     * @return ResponseInterface
     */
    public function checkGeo(CheckGeoRequest $request): ResponseInterface
    {
        $coordinateType = $request->post('coordinate_type', 'gps');
        $checkResult = VisitService::checkGeo(
            (int)$request->post('hospital_id'),
            $request->post('lat'),
            $request->post('lng'),
            $coordinateType
        );

        return $this->response->success([
            'result' => $checkResult ? 1 : 0
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/visits/config",
     *      tags={"app-代表拜访"},
     *      summary="获取拜访配置",
     *      description="获取拜访配置",
     *      operationId="GetAgentVisitConfig",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="配置类型：  1 拜访目的, 2超出范围选项 3. 异常拜访原因",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="config",
     *                              type="array",
     *                              description="配置列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="配置选项id"),
     *                                  @OA\Property(property="name", type="string", description="配置选项名称"),
     *                                  @OA\Property(
     *                                      property="is_extra_input",
     *                                      type="integer",
     *                                      description="是否需要额外输入内容")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetVisitConfigRequest $request
     * @return ResponseInterface
     */
    public function config(GetVisitConfigRequest $request): ResponseInterface
    {
        $config = VisitService::getVisitConfig((int)$request->query('type'));
        return $this->response->success([
            'config' => $config
        ]);
    }

    /**
     *
     * @OA\Put (
     *      path="/app/visits/check-out",
     *      tags={"app-代表拜访"},
     *      summary="代表拜访记录签出",
     *      description="代表拜访记录签出",
     *      operationId="AppAgentVisitCheckOut",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="check_in_id", type="integer", description="签入id"),
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                  @OA\Property(
     *                      property="depart_ids",
     *                      type="array",
     *                      description="科室id列表",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="doctor_ids",
     *                      type="array",
     *                      description="医生id列表",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="series_ids",
     *                      type="array",
     *                      description="产品id列表",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="result", type="string", description="拜访结果"),
     *                  @OA\Property(property="next", type="string", description="后续跟进事宜"),
     *                  @OA\Property(
     *                      property="pics",
     *                      type="array",
     *                      description="上传图片id列表 多个id使用逗号连接",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="lat", type="number", format="float", description="纬度"),
     *                  @OA\Property(property="lng", type="number", format="float", description="经度"),
     *                  @OA\Property(property="coordinate_type", type="string", description="坐标体系: 默认gps 高德gcj-02"),
     *                  @OA\Property(property="is_fake_geo", type="integer", description="是否随机位置, 0否 1是 默认0"),
     *                  @OA\Property(property="position", type="string", description="位置描述"),
     *                  @OA\Property(property="geo_opt", type="integer", description="超出范围选项id,默认传0表示未超出范围"),
     *                  @OA\Property(property="geo_comment", type="string", description="超出选项范围输入内容"),
     *                  @OA\Property(property="is_collaborative", type="integer", description="是否协议: 0未选择 1是 2否"),
     *                  @OA\Property(property="check_out_time", type="string", description="签出时间, 补签才传，否则不传或传空"),
     *                  required={
     *                      "hospital_id",
     *                      "visit_type",
     *                      "pics",
     *                      "lat",
     *                      "lng",
     *                      "result",
     *                      "next"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param CheckOutRequest $request
     * @return ResponseInterface
     * @throws Exception
     */
    public function checkOut(CheckOutRequest $request): ResponseInterface
    {
        $id = (int)$request->post('check_in_id');
        $coordinateType = (string) $request->post('coordinate_type', DataStatus::COORDINATE_GPS);
        // 获取并校验参数
        list($departIds, $seriesIds, $picIds, $result, $next, $doctorIds) = $this->checkOutParamValidate($request);
        //获取补签时间
        $checkOutTime = (string) $request->post('check_out_time', '');
        if ($checkOutTime) {
            $checkOutTime = Helper::formatInputDate($checkOutTime);
            if ($checkOutTime > date('Y-m-d H:i:s')) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, '补签时间不能晚于当前时间');
            }
        }

        // 签出
        VisitService::checkOut(
            $id,
            $request->post('hospital_id'),
            $request->post('visit_type'),
            $departIds,
            $seriesIds,
            $result,
            $next,
            $request->post('lat', 0),
            $request->post('lng', 0),
            $request->post('position', ''),
            $picIds,
            (int) $request->post('geo_opt', 0),
            (string) $request->post('geo_comment', ''),
            $doctorIds,
            $coordinateType,
            (int)$request->post('is_collaborative', 0),
            $checkOutTime,
            $request->post('is_fake_geo', 0) == 1
        );

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/agent/visits/check-in/{id}",
     *      tags={"app-代表拜访"},
     *      summary="代表拜访签入编辑",
     *      description="代表拜访签入编辑",
     *      operationId="AgentVisitCheckInEdit",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="访问记录id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                  @OA\Property(
     *                      property="pics",
     *                      type="array",
     *                      description="上传图片id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="lat", type="number", format="float", description="纬度"),
     *                  @OA\Property(property="lng", type="number", format="float", description="经度"),
     *                  @OA\Property(property="coordinate_type", type="string", description="坐标体系: 默认gps 高德gcj-02"),
     *                  @OA\Property(property="is_fake_geo", type="integer", description="是否随机位置, 0否 1是 默认0"),
     *                  @OA\Property(property="position", type="string", description="位置描述"),
     *                  @OA\Property(property="geo_opt", type="integer", description="地理位置超出选项id,当调用检查接口返回未通过时需要"),
     *                  @OA\Property(property="geo_comment", type="string", description="地理位置超出填空内容，当调用检查接口返回未通过时需要"),
     *                  required={
     *                      "hospital_id",
     *                      "visit_type",
     *                      "lat",
     *                      "lng"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param CheckInRequest $request
     * @return ResponseInterface
     * @throws Exception
     */
    public function editCheckIn($id, CheckInRequest $request): ResponseInterface
    {
        $coordinateType = $request->post('coordinate_type', DataStatus::COORDINATE_GPS);
        list($pics, $visitType, $hospitalId) = $this->checkInParamValidate($request);

        VisitService::editCheckIn(
            $id,
            $visitType,
            $hospitalId,
            $pics,
            $request->post('lng', 0),
            $request->post('lat', 0),
            $request->post('position', ""),
            (int)$request->post('geo_opt', 0),
            $request->post('geo_comment', ''),
            $coordinateType,
            $request->post('is_fake_geo', 0) == 1
        );

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/app/visits/check-out/{id}",
     *      tags={"app-代表拜访"},
     *      summary="代表拜访签出编辑",
     *      description="代表拜访签出编辑",
     *      operationId="AgentVisitCheckOutEdit",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="访问记录id",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="visit_type", type="integer", description="拜访类型id: 1当面拜访 2线上拜访 3内部会议
     * 4外部会议 5 行政类工作"),
     *                   @OA\Property(
     *                      property="depart_ids",
     *                      type="array",
     *                      description="科室id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="doctor_ids",
     *                      type="array",
     *                      description="医生id列表",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="series_ids",
     *                      type="array",
     *                      description="产品id列表",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="result", type="string", description="拜访结果"),
     *                  @OA\Property(property="next", type="string", description="后续跟进事宜"),
     *                  @OA\Property(
     *                      property="pics",
     *                      type="array",
     *                      description="上传图片id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="lat",type="number", format="float", description="纬度"),
     *                  @OA\Property(property="lng", type="number", format="float", description="经度"),
     *                  @OA\Property(property="coordinate_type", type="string", description="坐标体系: 默认gps 高德gcj-02"),
     *                  @OA\Property(property="is_fake_geo", type="integer", description="是否随机位置, 0否 1是 默认0"),
     *                  @OA\Property(property="position", type="string", description="位置描述"),
     *                  @OA\Property(property="geo_opt", type="integer", description="地理位置超出选项id,当调用检查接口返回未通过时需要"),
     *                  @OA\Property(property="geo_comment", type="string", description="地理位置超出填空内容，当调用检查接口返回未通过时需要"),
     *                  @OA\Property(property="is_collaborative", type="integer", description="是否协访: 0未选择 1是 2否"),
     *                  required={
     *                      "hospital_id",
     *                      "visit_type",
     *                      "lat",
     *                      "lng"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param CheckOutRequest $request
     * @return ResponseInterface
     * @throws Exception
     */
    public function editCheckOut($id, CheckOutRequest $request): ResponseInterface
    {
        $coordinateType = $request->post('coordinate_type', DataStatus::COORDINATE_GPS);
        list($departIds, $seriesIds, $picIds, $result, $next, $doctorIds) = $this->checkOutParamValidate($request);

        // 签出
        VisitService::editCheckOut(
            $id,
            $request->post('hospital_id'),
            $request->post('visit_type'),
            $departIds,
            $seriesIds,
            $result,
            $next,
            $request->post('lat', 0),
            $request->post('lng', 0),
            $request->post('position', ''),
            $picIds,
            (int)$request->post('geo_opt', 0),
            $request->post('geo_comment', ''),
            $doctorIds,
            $coordinateType,
            (int)$request->post('is_collaborative', 0),
            $request->post('is_fake_geo', 0) == 1
        );
        return $this->response->success([]);
    }

    /**
     * @param CheckOutRequest $request
     * @return array
     */
    private function checkOutParamValidate(CheckOutRequest $request): array
    {
        // 获取并校验参数
        $departIds = $request->post('depart_ids', []);
        $seriesIds = $request->post('series_ids', []);
        $picIds = $request->post('pics', []);
        $doctorIds = $request->post('doctor_ids', []);

        $result = $request->post('result', '');
        $next = $request->post('next', '');

        return array($departIds, $seriesIds, $picIds, $result, $next, $doctorIds);
    }

    /**
     * @param CheckInRequest $request
     * @return array
     */
    private function checkInParamValidate(CheckInRequest $request): array
    {
        $pics = $request->post('pics', []);
        $visitType = $request->post('visit_type');
        $hospitalId = $request->post('hospital_id', 0);
        if ($hospitalId == 0 && in_array($visitType, [
                AgentVisit::VISIT_TYPE_TO_FACE,
                AgentVisit::VISIT_TYPE_ONLINE
            ])) {
            throw new BusinessException(ErrorCode::CHECKIN_HOSPITAL_EMPTY_INVALID);
        }

        return [$pics, $visitType, $hospitalId];
    }


    /**
     *
     * @OA\Get (
     *      path="/app/visits/doctors",
     *      tags={"app-代表拜访"},
     *      summary="获取代表群组医生",
     *      description="获取代表群组医生",
     *      operationId="AgentVisitDoctorList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="拜访医院id",
     *         in="query",
     *         name="hospital_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="部门id",
     *         in="query",
     *         name="depart_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Parameter(
     *         description="搜索关键字",
     *         in="query",
     *         name="keywords",
     *         required=false,
     *         @OA\Schema(type="string")
     *      ),
     *     @OA\Parameter(
     *         description="页码",
     *         in="query",
     *         name="page",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="页面数量",
     *         in="query",
     *         name="page_size",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="page", type="integer", description="页码"),
     *                          @OA\Property(property="page_size", type="integer", description="页面数量"),
     *                          @OA\Property(property="total", type="integer", description="医生数量"),
     *                          @OA\Property(
     *                              property="list",
     *                              type="object",
     *                              @OA\Property(property="id", type="integer", description="医生id"),
     *                              @OA\Property(property="name", type="string", description="医生名"),
     *                              @OA\Property(property="head_img_url", type="string", description="头像地址"),
     *                              @OA\Property(property="hospital_name", type="string", description="医院名称"),
     *                              @OA\Property(property="depart_name", type="string", description="部门名称")
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AgentVisiDoctorListRequest $request
     * @return ResponseInterface
     */
    public function getDoctors(AgentVisiDoctorListRequest $request): ResponseInterface
    {
        $hospitalId = (int)$request->query('hospital_id');
        $departId = (int)$request->query('depart_id', 0);
        $keywords = (string)$request->query('keywords', '');
        $page = (int)$request->query('page', 1);
        $pageSize = (int)$request->query('page_size', 10);
        $resp = VisitService::getGroupDoctors($hospitalId, $departId, $keywords, $page, $pageSize);

        return $this->response->success($resp);
    }

    /**
     *
     * @OA\Put (
     *      path="/app/visits/doctors",
     *      tags={"app-代表拜访"},
     *      summary="添加代表群组医生",
     *      description="添加代表群组医生",
     *      operationId="AppAgentVisitDoctorAdd",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="check", type="boolean", description="是否判断，有相同姓名医生存在，默认true"),
     *                  @OA\Property(property="name", type="string", description="姓名"),
     *                  @OA\Property(property="sex", type="integer", description="性别: 1男 2女"),
     *                  @OA\Property(property="mobile", type="string", description="手机号"),
     *                  @OA\Property(property="position", type="string", description="级别"),
     *                  @OA\Property(property="job_title", type="string", description="职称"),
     *                  @OA\Property(property="field_id", type="integer", description="擅长领域id"),
     *                  @OA\Property(property="hospital_id", type="integer", description="拜访医院id"),
     *                  @OA\Property(property="depart_id", type="integer", description="部门id"),
     *                  @OA\Property(property="clinic_rota", type="integer", description="门诊时间"),
     *                  required={
     *                      "hospital_id",
     *                      "depart_id",
     *                      "mobile",
     *                      "name"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      @OA\Property(
     *                          property="id",
     *                          type="integer",
     *                          description="新加医生记录id"
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AgentVisiDoctorAddRequest $request
     * @return ResponseInterface
     */
    public function addDoctor(AgentVisiDoctorAddRequest $request): ResponseInterface
    {
        $doctor = VisitService::addGroupDoctors(
            (boolean) $request->post('check', true),
            (string)$request->post('name'),
            (int)$request->post('hospital_id'),
            (int)$request->post('depart_id'),
            (string)$request->post('mobile'),
            (int)$request->post('sex', 1),
            (string)$request->post('position', ''),
            (string)$request->post('job_title', ''),
            (int)$request->post('field_id', 0),
            (string)$request->post('clinic_rota')
        );

        return $this->response->success([
            'id' => $doctor->id
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/visits/check-in-config",
     *      tags={"app-代表拜访"},
     *      summary="获取签入个人配置",
     *      description="获取签入个人配置",
     *      operationId="AppAgentVisitCheckInUserConfig",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="is_supplement", type="integer", description="是否允许补签")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function checkInConfig(): ResponseInterface
    {
        $user = Helper::getLoginUser();
        $isImplement = VisitService::checkUserValidCheckInSupplement($user);
        return $this->response->success([
            'is_supplement' => $isImplement ? 1 : 0
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/visits/departs",
     *      tags={"app-代表拜访"},
     *      summary="获取所有医生所在部门",
     *      description="获取所有医生所在部门",
     *      operationId="departs",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *         description="拜访医院id",
     *         in="query",
     *         name="hospital_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="depart_id", type="integer", description="部门id"),
     *                          @OA\Property(property="depart_name", type="string", description="部门名称")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function departs(GetDepartsRequest $request)
    {
        $hospitalId = $request->input('hospital_id');
        $userId = Helper::getLoginUser()->uid;
        $data = VisitService::getDoctorDeparts($userId, $hospitalId);

        return $this->response->success($data);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/visits/select-list",
     *      tags={"app-代表拜访"},
     *      summary="获取选择医生列表",
     *      description="获取选择医生列表",
     *      operationId="select-list",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="医生id"),
     *                          @OA\Property(property="name", type="string", description="医生名"),
     *                          @OA\Property(property="head_img_url", type="string", description="头像地址"),
     *                          @OA\Property(property="hospital_name", type="string", description="医院名称"),
     *                          @OA\Property(property="depart_name", type="string", description="部门名称")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function selectList()
    {
        $userId = Helper::getLoginUser()->uid;

        $data = VisitService::selectList($userId);

        return $this->response->success($data);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/visits/add-select-list",
     *      tags={"app-代表拜访"},
     *      summary="加入选择医生列表",
     *      description="加入选择医生列表",
     *      operationId="add-select-list",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="医生id数组【】",
     *         in="query",
     *         name="doctor_ids",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function addSelectList(AddToListRequest $request)
    {
        $doctorIds = $request->input('doctor_ids', []);
        $userId = Helper::getLoginUser()->uid;

        VisitService::addSelectList($doctorIds, $userId);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/visits/delete-select-list",
     *      tags={"app-代表拜访"},
     *      summary="从选择列表删除",
     *      description="从选择列表删除",
     *      operationId="delete-select-list",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="医生id数组【】",
     *         in="query",
     *         name="doctor_ids",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function deleteSelectList(AddToListRequest $request)
    {
        $doctorIds = $request->input('doctor_ids', []);
        $userId = Helper::getLoginUser()->uid;

        VisitService::deleteSelectList($doctorIds, $userId);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/visits/clear-select-list",
     *      tags={"app-代表拜访"},
     *      summary="清空选择列表",
     *      description="清空选择列表",
     *      operationId="clear-select-list",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function clearSelectList()
    {
        $userId = Helper::getLoginUser()->uid;

        VisitService::clearSelectList($userId);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/visits/count",
     *      tags={"app-代表拜访"},
     *      summary="列表中选择医生数量",
     *      description="列表中选择医生数量",
     *      operationId="count",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="count", type="integer", description="数量")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function doctorCount()
    {
        $userId = Helper::getLoginUser()->uid;

        $data = VisitService::doctorCount($userId);

        return $this->response->success(['count' => $data]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/visits/fake-position",
     *      tags={"app-代表拜访"},
     *      summary="生成拜访医院随机地理位置坐标",
     *      description="生成拜访医院随机地理位置坐标",
     *      operationId="AppAgentVisitGenerateFakePosition",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="访问记录id, hospital_id和id必须至少传一个,优先使用id",
     *         in="query",
     *         name="id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="需要生成随机位置的医院id, hospital_id和id必须至少传一个,优先使用id",
     *         in="query",
     *         name="hospital_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="geo",
     *                              type="object",
     *                              description="详情",
     *                              @OA\Property(property="lat", type="number", format="float", description="纬度"),
     *                              @OA\Property(property="lng", type="number", format="float", description="经度"),
     *                              @OA\Property(property="pos", type="string", format="float", description="坐标位置"),
     *                              @OA\Property(
     *                                  property="coordinate_type",
     *                                  type="string",
     *                                  description="坐标体系: 默认gps 火星坐标gcj-02"),
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function generateGeoPosition(FakeHospitalPositionRequest $request): ResponseInterface
    {
        $geo = VisitService::generateHospitalRandomGeoPos(
            (int) $request->query('hospital_id', 0),
            (int) $request->query('id', 0)
        );
        return $this->response->success([
            'geo' => $geo ? $geo : new \stdClass()
        ]);
    }
}
