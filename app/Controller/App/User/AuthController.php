<?php
declare(strict_types=1);

namespace App\Controller\App\User;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Helper\Jwt;
use App\Request\App\Login\CheckRequest;
use App\Request\App\Login\IosBindRequest;
use App\Request\App\Login\LoginByIosRequest;
use App\Request\App\Login\LoginForCheckRequest;
use App\Request\App\Login\WechatBindRequest;
use App\Request\App\User\AuthByWechatRequest;
use App\Request\App\User\BindWechatMobileRequest;
use App\Request\App\Login\LoginByPhoneRequest;
use App\Service\Login\BindService;
use App\Service\Login\LoginService;
use App\Service\User\UserService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Arr;
use Hyperf\Utils\Context;
use Psr\Http\Message\ResponseInterface;

class AuthController extends Controller
{
    /**
     * @Inject()
     * @var Jwt
     */
    protected $jwt;

    /**
     *
     * @OA\Post (
     *      path="/app/auth/phone",
     *      tags={"app-登陆"},
     *      summary="手机验证码登陆",
     *      description="手机验证码登陆",
     *      operationId="AppAuthPhone",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="mobile", type="string", description="手机号"),
     *                  @OA\Property(property="code", type="string", description="验证码"),
     *                  required={"mobile", "code"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(
 *                              property="auth",
 *                              type="object",
 *                              @OA\Property(property="auth_status", type="integer", description="登陆状态:1登陆成功 2需要绑定手机号"),
 *                              @OA\Property(property="token", type="string", description="登陆token, auth_status=1时返回")
 *                          )
     *                  )
 *                  )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function appPhone(LoginByPhoneRequest $request)
    {
        $phone = $request->input('mobile');
        $code = $request->input('code');

        $ip = Helper::ip($request);

        $authInfo = LoginService::authByPhone($phone, $code, $ip);

        return $this->response->success([
            'auth' => $authInfo
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/ios/authInfo",
     *      tags={"app-登陆"},
     *      summary="苹果登录拉取苹果用户信息",
     *      description="苹果登录拉取苹果用户信息",
     *      operationId="AppIosAuthInfo",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="iosid", type="string", description="苹果用户唯一标识符"),
     *                  @OA\Property(property="familyName", type="string", description="姓"),
     *                  required={"iosid"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="授权信息生成的id"),
     *                      )
     *                  )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function appIosAuthInfo(LoginByIosRequest $request)
    {
        $iosid = $request->input('iosid');
        $familyName = $request->input('familyName');

        $id = LoginService::appIosAuthInfo($iosid, $familyName);

        return $this->response->success(['id' => $id]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/auth/ios",
     *      tags={"app-登陆"},
     *      summary="苹果登录",
     *      description="苹果登录",
     *      operationId="AppAuthIos",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="iosid", type="string", description="苹果用户唯一标识符"),
     *                  @OA\Property(property="familyName", type="string", description="姓"),
     *                  required={"iosid"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="auth",
     *                              type="object",
     *                              @OA\Property(
     *                                  property="auth_status",
     *                                  type="integer",
     *                                  description="登陆状态:1登陆成功 2需要绑定手机号"),
     *                              @OA\Property(
     *                                  property="token",
     *                                  type="string",
     *                                  description="登陆token, auth_status=1时返回")
     *                          )
     *                      )
     *                  )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function appIos(LoginByIosRequest $request)
    {
        $iosid = $request->input('iosid');
        $familyName = $request->input('familyName');

        $ip = Helper::ip($request);

        $authInfo = LoginService::loginByIos($iosid, $familyName, $ip);

        return $this->response->success([
            'auth' => $authInfo
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/auth/wechat",
     *      tags={"app-登陆"},
     *      summary="微信登陆",
     *      description="微信登陆",
     *      operationId="AppWechatAuth",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="openid", type="string", description="微信openid"),
     *                  required={"openid"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="auth",
     *                              type="object",
     *                              @OA\Property(
     *                                  property="auth_status",
     *                                  type="integer",
     *                                  description="登陆状态:1登陆成功 2需要绑定手机号"),
     *                              @OA\Property(
     *                                  property="token",
     *                                  type="string",
     *                                  description="登陆token, auth_status=1时返回")
     *                          )
     * )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function appWechatAuth(AuthByWechatRequest $request): ResponseInterface
    {
        $openid = $request->input('openid');
        $ip = Helper::ip($request);
        $authInfo = LoginService::authByWechat($openid, $ip);
//        $authInfo = UserService::authByWechat($request->post('openid'));

        return $this->response->success([
            'auth' => $authInfo
        ]);
    }

    /**
     *
     * @OA\Put (
     *      path="/app/auth/wechat-bind-mobile",
     *      tags={"app-登陆"},
     *      summary="微信绑定手机号登陆",
     *      description="微信绑定手机号登陆",
     *      operationId="AppWechatBindMobile",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="mobile", type="string", description="手机号"),
     *                  @OA\Property(property="code", type="string", description="验证码"),
     *                  @OA\Property(property="token", type="string", description="用户token"),
     *                  @OA\Property(property="unionid", type="string", description="微信unionid 微信第三方登录绑定手机号时传值"),
     *                  @OA\Property(property="iosid", type="string", description="苹果id ios第三方登录绑定手机号时传值"),
     *                  required={"mobile", "code"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="auth",
     *                              type="object",
     *                              @OA\Property(
     *                                  property="auth_status",
     *                                  type="integer",
     *                                  description="登陆状态:1登陆成功 2需要绑定手机号"),
     *                              @OA\Property(
     *                                  property="token",
     *                                  type="string",
     *                                  description="登陆token, auth_status=1时返回")
     *                          )
     * )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function bindWechatMobile(BindWechatMobileRequest $request): ResponseInterface
    {
        $token = $request->input('token', '');
        $mobile = $request->input('mobile');
        $code = $request->input('code');
        $unionid = $request->input('unionid');
        $iosid = $request->input('iosid');

        if ($token) {
            $id = Arr::get($this->jwt->decode(substr($token, 7)), 'sub');
        } else {
            $id = 0;
        }

        $ip = Helper::ip($request);

        $auth = BindService::bindPhoneNumber($id, $mobile, $code, $unionid, $iosid, $ip);

//        $auth = UserService::bindWechatMobile(
//            (string)$request->post('mobile'),
//            (string)$request->post('code'),
//        );

        return $this->response->success([
            'auth' => $auth
        ]);
    }

    /**
     *
     * @OA\Post  (
     *      path="/app/auth/logout",
     *      tags={"app-登陆"},
     *      summary="退出登陆",
     *      description="退出登陆",
     *      operationId="AppWechatLogout",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function logout(): ResponseInterface
    {
        UserService::appLogout();
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/auth/wechatBind",
     *      tags={"app-登陆"},
     *      summary="用户绑定或解绑微信",
     *      description="用户绑定或解绑微信",
     *      operationId="AppAuthWechatBind",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="unionid", type="string", description="微信unionid，解绑的时候可以不传"),
     *                  required={}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="status", type="integer", description="1-绑定成功0-解绑成功"),
     *                      )
     *                  )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function wechatBind(WechatBindRequest $request)
    {
        $user = context::get('user');
        $unionid = $request->input('unionid');
        $status = BindService::bindwechat($user, $unionid);

        return $this->response->success(['status' => $status]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/auth/iosBind",
     *      tags={"app-登陆"},
     *      summary="用户绑定或解绑ios",
     *      description="用户绑定或解绑ios",
     *      operationId="AppAuthIosBind",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="iosid", type="string", description="苹果iosid，解绑的时候可以不传"),
     *                  required={}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="status", type="integer", description="1-绑定成功0-解绑成功"),
     *                      )
     *                  )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function iosBind(IosBindRequest $request)
    {
        $id = Helper::getLoginUser()->uid;
        $iosid = $request->input('iosid');
        $status = BindService::bindIos($id, $iosid);

        return $this->response->success(['status' => $status]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/auth/deleteAccount",
     *      tags={"app-登陆"},
     *      summary="注销账号",
     *      description="注销账号",
     *      operationId="AppAuthDeleteAccount",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="message", type="integer", description="注销成功"),
     *                      )
     *                  )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function deleteAccount()
    {
        $user = context::get('user');

        $msg = BindService::deleteAccount($user);

        return $this->response->success(['message' => $msg]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/auth/bindState/{unionid}",
     *      tags={"app-登陆"},
     *      summary="判断公众号当前微信和手机的绑定状态",
     *      description="判断公众号当前微信和手机的绑定状态",
     *      operationId="AppAuthBindState",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="auth",
     *                              type="object",
     *                              @OA\Property(
     *
     *                                  property="auth_status",
     *
     *                                  type="integer",
     *
     *                                  description="登陆状态:1登陆成功 2需要绑定手机号"),
     *                              @OA\Property(
     *
     *                                  property="token",
     *
     *                                  type="string",
     *
     *                                  description="登陆token, auth_status=1时返回")
     *                          )
     * )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function bindState($unionid)
    {
        $auth = BindService::checkBindState($unionid);

        return $this->response->success(['auth' => $auth]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/auth/check-status",
     *      tags={"app-登陆"},
     *      summary="获取审核状态",
     *      description="获取审核状态",
     *      operationId="checkStatus",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="version", type="string", description="版本号"),
     *                  @OA\Property(
     *                      property="channel",
     *                      type="integer",
     *                      description="渠道id 1-应用宝 2-360应用市场
     * 3-华为应用商店 4-小米应用商店 5-vivo应用商店
     *     6-oppo应用商店 7-苹果应用商店"),
     *                  required={"version","channel"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="status", type="integer", description="1审核中2审核已通过"),
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */

    /**
     * @param CheckRequest $request
     * @return ResponseInterface
     */
    public function getStatus(CheckRequest $request)
    {
        $version = $request->input('version');
        $channel = $request->input('channel');

        $res = BindService::getLoginForCheckStatus($version, $channel);

        return $this->response->success($res);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/auth/login-for-check",
     *      tags={"app-登陆"},
     *      summary="审核白名单登录",
     *      description="审核白名单登录",
     *      operationId="loginForCheck",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="phone", type="integer", description="账号（手机号）"),
     *                  @OA\Property(property="password", type="integer", description="密码"),
     *                  required={"phone","password"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="auth_status", type="integer", description="登陆状态:1登陆成功 2需要绑定手机号"),
     *                          @OA\Property(property="token", type="string", description="登陆token, auth_status=1时返回")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */

    /**
     * @param LoginForCheckRequest $request
     * @return ResponseInterface
     */
    public function loginForCheck(LoginForCheckRequest $request)
    {
        $phone = $request->input('phone');
        $password = $request->input('password');

        $data = BindService::loginForCheck($phone, $password);

        return $this->response->success($data);
    }
}
