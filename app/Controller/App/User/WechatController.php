<?php
declare(strict_types=1);

namespace App\Controller\App\User;

use App\Constants\Auth;
use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Controller\Controller;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Model\Qa\Users;
use App\Request\App\Wechat\GetJsSignRequest;
use App\Request\App\Wechat\GetOauthRedirectUrlRequest;
use App\Request\App\Wechat\GetOauthUserInfoRequest;
use App\Request\App\Wechat\RecordWechatRequest;
use App\Request\Wechat\OauthRedirectUrlRequest;
use App\Request\Wechat\OauthUserInfoRequest;
use App\Request\Wechat\WechatJsSignatureGetRequest;
use App\Service\User\UserService;
use App\Service\Wechat\WechatBusiness;
use App\Service\Wechat\WechatService;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Coroutine;
use Psr\Http\Message\ResponseInterface;
use Psr\SimpleCache\InvalidArgumentException;

class WechatController extends Controller
{
    /**
     *
     * @OA\Put (
     *      path="/app/wechat/record",
     *      tags={"app-微信"},
     *      summary="记录/更新微信信息",
     *      description="记录/更新微信信息",
     *      operationId="RecordAppWechat",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="openid", type="string", description="微信openid"),
     *                  @OA\Property(property="nickname", type="string", description="微信呢称"),
     *                  @OA\Property(property="sex", type="integer", description="微信性别,1 为男性，2 为女性，默认0"),
     *                  @OA\Property(property="province", type="string", description="省份"),
     *                  @OA\Property(property="city", type="string", description="城市"),
     *                  @OA\Property(property="country", type="string", description="国家"),
     *                  @OA\Property(property="headimgurl", type="string", description="头像url"),
     *                  @OA\Property(property="unionid", type="string", description="unionid"),
     *                  required={"openid", "nickname", "headimgurl", "unionid"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function record(RecordWechatRequest $request): ResponseInterface
    {
        $openId = (string) $request->post('openid');
        $nickname = (string) $request->post('nickname');
        $sex = (int) $request->post('sex', 0);
        $province = (string) $request->post('province');
        $city = (string) $request->post('city');
        $country = (string) $request->post('country');
        $headImgUrl = (string) $request->post('headimgurl');
        $unionId = (string) $request->post('unionid');
        $roleType = Helper::getAuthRole();
        $wechatType = Helper::getWechatTypeByRole($roleType);

        UserService::recordWechatInfo(
            $wechatType,
            $openId,
            $nickname,
            $headImgUrl,
            $unionId,
            $sex,
            $country,
            $province,
            $city
        );
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/app/wechat/get-oauth-redirect-url",
     *      tags={"app-微信"},
     *      summary="获取网页授权跳转地址",
     *      description="获取网页授权跳转地址",
     *      operationId="appWechatGetOauthRedirectUrl",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(
     *                      property="is_silent",
     *                      type="int",
     *                      description="是否静默授权: 1是(snsapi_base)
     * 0 否(snsapi_userinfo)"),
     *                  @OA\Property(property="state", type="string", description="微信oauth state参数"),
     *                  @OA\Property(property="redirect", type="string", description="完成授权后的跳转地址"),
     *                  required={"redirect"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="url", type="string", description="微信跳转地址，客户端直接使用该地址进行跳转")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetOauthRedirectUrlRequest $request
     * @return ResponseInterface
     */
    public function getOauthRedirectUrl(GetOauthRedirectUrlRequest $request): ResponseInterface
    {
        $wechatType = self::checkWechat();
        $url = ApplicationContext::getContainer()
            ->get(WechatBusiness::class)
            ->getOauthRedirectUrl(
                $request->post('redirect'),
                $request->post('is_silent', 1),
                $request->post('state'),
                $wechatType
            );
        return $this->response->success([
            'url' => $url
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/app/wechat/get-oauth-user-info",
     *      tags={"app-微信"},
     *      summary="获取网页授权用户信息",
     *      description="获取网页授权用户信息",
     *      operationId="appWechatGetOauthUserInfo",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="state", type="string", description="完成授权后url中带的state参数"),
     *                  @OA\Property(property="code", type="string", description="完成授权后url中带的code参数"),
     *                  required={"state", "code"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="openid", type="string", description="当前登陆用户的openid参数"),
     *                          @OA\Property(property="unionid", type="string", description="当前登陆用户的unionid参数")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetOauthUserInfoRequest $request
     * @return ResponseInterface
     */
    public function getOauthUserInfo(GetOauthUserInfoRequest $request): ResponseInterface
    {
        $wechatType = self::checkWechat();

        $userInfo = ApplicationContext::getContainer()
            ->get(WechatBusiness::class)->getOauthWechatInfo($wechatType);
        $raw = $userInfo->getRaw();
        if ($raw) { //添加api表
            Coroutine::create(function () use ($raw, $wechatType) {
                WechatService::processOauthRawMsg($raw, $wechatType);
            });

            Helper::getLogger()->debug("get_oauth_raw_info", [
                'type' => $request->post('type'),
                'raw' => $raw
            ]);
        }
        return $this->response->success([
            'openid' => $userInfo->getId(),
            'unionid' => $raw['unionid']
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/app/wechat/sign",
     *      tags={"app-微信"},
     *      summary="获取jsapi config参数",
     *      description="获取jsapi config参数",
     *      operationId="appWechatGetWechatJsSignature",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="url", type="string", description="当前页面url"),
     *                  @OA\Property(property="apis", type="array", description="api列表", @OA\Items(type="string")),
     *                  required={"url"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetJsSignRequest $request
     * @return ResponseInterface
     * @throws InvalidArgumentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWechatJsSignature(GetJsSignRequest $request): ResponseInterface
    {
        $wechatType = self::checkWechat();
        $debug = !Helper::isOnline();
        $apis = $request->post('apis', []);
        $config = ApplicationContext::getContainer()
            ->get(WechatBusiness::class)
            ->getWechatJsSignature(
                $request->post('url'),
                $apis,
                $debug,
                $wechatType
            );
        return $this->response->success($config);
    }

    private static function checkWechat(): int
    {
        $role = Helper::getAuthRole();
        $wechatType = Helper::getWechatTypeByRole($role);
        if ($wechatType != DataStatus::WECHAT_AGENT) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "目前仅公众号支持该功能");
        }

        return $wechatType;
    }
}
