<?php
declare(strict_types=1);

namespace App\Controller\App\User;

use App\Constants\Auth;
use App\Constants\ErrorCode;
use App\Controller\Controller;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Request\App\Point\ExchangeGoodsRequest;
use App\Request\App\Point\GetUserPointRequest;
use App\Service\Points\PointsService;
use App\Service\User\UserService;
use Psr\Http\Message\ResponseInterface;

class PointController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/points/user-total",
     *
     *      tags={"app-用户"},
     *      summary="用户积分汇总信息",
     *      description="用户积分汇总信息",
     *      operationId="AppUserTotalPoint",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="group_id",
     *          in="query",
     *          description="积分活动id, 默认传0表示平台默认活动",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),    
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="积分数据",
     *                              @OA\Property(
     *                                  property="service_point",
     *                                  type="number",
     *                                  format="float",
     *                                  description="服务积分"),
     *                              @OA\Property(
     *                                  property="money_point",
     *                                  type="number",
     *                                  format="float",
     *                                  description="消费积分")
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function getUserPoint(GetUserPointRequest $request): ResponseInterface
    {
        [$servicePoint, $moneyPoint] = PointsService::getUserPoints(
            (int) $request->query('group_id', 0)
        );

        return $this->response->success([
            'info' => [
                'service_point' => $servicePoint,
                'money_point' => $moneyPoint
            ]
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/points/goods",
     *      tags={"app-用户"},
     *      summary="积分可兑换商品列表",
     *      description="积分可兑换商品列表",
     *      operationId="AppPointGoodsList",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="group_id",
     *          in="query",
     *          description="活动活动分组id,默认传0表示平台默认活动",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="积分可兑换商品列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="yyid", type="string", description="商品yyid"),
     *                                  @OA\Property(property="name", type="string", description="商品名称"),
     *                                  @OA\Property(
     *                                      property="cost_money_point",
     *                                      type="number",
     *                                      format="float",
     *                                      description="兑换一份需要消耗消费积分数量"
     *                                  ),
     *                                  @OA\Property(
     *                                      property="cost_service_point",
     *                                      type="number",
     *                                      format="float",
     *                                      description="兑换一份需要消耗服务积分数量"
     *                                  ),
     *                                  @OA\Property(property="desc", type="string", description="描述"),
     *                                  @OA\Property(property="pic", type="string", description="图片url"),
     *                                  @OA\Property(property="is_unlimited", type="integer", description="是否无限制兑换"),
     *                                  @OA\Property(property="rest", type="integer", description="剩余可兑换数量"),
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function getGoodsLists(): ResponseInterface
    {
        $current = (int) $this->request->query('current', 1);
        $limit = (int) $this->request->query('limit', 10);
        $isAgentApp = Helper::getAuthRole() == Auth::AUTH_ROLE_YOU_YAO_APP;
        [$page, $data] = PointsService::getExchangeGoodsList(
            $current, 
            $limit, 
            $isAgentApp,
            (int) $this->request->query('group_id', 0)
        );

        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/points/goods/info",
     *      tags={"app-用户"},
     *      summary="兑换商品详情",
     *      description="兑换商品详情",
     *      operationId="AppPointGoodsInfo",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="yyid",
     *          in="query",
     *          description="商品yyid",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="商品详情",
     *                              @OA\Property(property="yyid", type="string", description="商品yyid"),
     *                              @OA\Property(property="name", type="string", description="商品名称"),
     *                              @OA\Property(
     *                                      property="cost_money_point",
     *                                      type="number",
     *                                      format="float",
     *                                      description="兑换一份需要消耗消费积分数量"
     *                              ),
     *                              @OA\Property(
     *                                      property="cost_service_point",
     *                                      type="number",
     *                                      format="float",
     *                                      description="兑换一份需要消耗服务积分数量"
     *                               ),
     *                               @OA\Property(property="desc", type="string", description="描述"),
     *                               @OA\Property(property="pic", type="string", description="图片url"),
     *                               @OA\Property(property="is_unlimited", type="integer", description="是否无限制兑换"),
     *                               @OA\Property(property="rest", type="integer", description="剩余可兑换数量"),
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function goodsInfo(): ResponseInterface
    {
        $yyid = (string) $this->request->query('yyid');
        $info = PointsService::getGoodsDetails($yyid);

        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/points",
     *      tags={"app-用户"},
     *      summary="用户积分明细",
     *      description="用户积分明细",
     *      operationId="AppUserPointDetails",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="group_id",
     *          in="query",
     *          description="积分分组活动id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="type",
     *          in="query",
     *          description="类型: 0全部 1发放 2使用",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="point_type",
     *          in="query",
     *          description="积分类型: 1服务积分 2消费积分",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="积分详情",
     *                              @OA\Items(
     *                                  @OA\Property(property="desc", type="string", description="明细描述"),
     *                                  @OA\Property(
     *                                      property="points",
     *                                      type="string",
     *                                      example="+100",
     *                                      description="积分变更数量"),
     *                                  @OA\Property(
     *                                      property="status",
     *                                      type="string",
     *                                      example="已兑换",
     *                                      description="状态描述"),
     *                                  @OA\Property(
     *                                      property="date",
     *                                      type="string",
     *                                      example="2021-01-01",
     *                                      description="创建时间"),
     *                              )
     *                          )
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function details(): ResponseInterface
    {
        $current = (int) $this->request->query('current', 1);
        $limit = (int) $this->request->query('limit', 10);
        $type = (int) $this->request->query('type', 0);
        $pointType = (int) $this->request->query('point_type', 1);
        [$page, $data] = PointsService::getUserPointDetails(
            $current, 
            $limit, 
            $type, 
            $pointType,
            (int) $this->request->query('group_id', 0)
        );

        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Put (
     *      path="/app/points/exchange",
     *      tags={"app-用户"},
     *      summary="用户积分兑换商品",
     *      description="用户积分兑换商品",
     *      operationId="AppUserPointExchange",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="goods_yyid", type="string", description="商品yyid"),
     *                  @OA\Property(property="num", type="integer", description="兑换份数"),
     *                  required={"goods_yyid", "num"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function exchange(ExchangeGoodsRequest $request): ResponseInterface
    {
        $wechat = UserService::getAppLoginUserWechat();
        if (!$wechat) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "微信登陆信息不存在");
        }

        PointsService::appExchangeGoods(
            (string) $request->post('goods_yyid'),
            (int)$request->post('num'),
            $wechat
        );
        return $this->response->success([]);
    }
}
