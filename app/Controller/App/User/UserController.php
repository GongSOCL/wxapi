<?php
declare(strict_types=1);

namespace App\Controller\App\User;

use App\Controller\Controller;
use App\Model\Qa\Agent;
use App\Request\App\User\HelpRequest;
use App\Request\App\User\UpdateUserAvatarRequest;
use App\Request\App\User\UpdateUserMobileRequest;
use App\Request\App\User\UpdateUserNameRequest;
use App\Request\App\User\VerifyRequest;
use App\Request\User\GetPharmacyDoctorQrcodeRequest;
use App\Service\Login\LoginService;
use App\Service\User\UserService;
use Exception;
use Hyperf\HttpServer\Contract\RequestInterface;
use PhpParser\Node\Stmt\Foreach_;
use Psr\Http\Message\ResponseInterface;

class UserController extends Controller
{
    private static function extractDirection($direction): array
    {
        $list = [];
        if (!$direction) {
            return $list;
        }

        $map = [
            Agent::DIRECTION_HOSPITAL_LISTING,
            Agent::DIRECTION_MARKETING_ACADEMIC
        ];
        foreach ($map as $v) {
            if (($direction & $v) == $v) {
                $list[] = $v;
            }
        }

        return $list;
    }

    /**
     *
     * @OA\Get (
     *      path="/app/user",
     *      tags={"app-用户"},
     *      summary="获取用户信息",
     *      description="获取用户信息",
     *      operationId="AppUserInfo",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="用户id"),
     *                          @OA\Property(property="name", type="string", description="用户名"),
     *                          @OA\Property(property="wechat_nickname", type="string", description="微信呢称"),
     *                          @OA\Property(property="avatar", type="string", description="用户头像"),
     *                          @OA\Property(property="mobile", type="string", description="手机号"),
     *                          @OA\Property(property="is_verified", type="integer", description="是否已认证: 0否 1是"),
     *                          @OA\Property(
     *                              property="verify_status",
     *                              type="integer",
     *                              description="1未认证 2认证中 3已认证 4已拒绝"),
     *                          @OA\Property(property="reject_reason", type="string", description="认证拒绝原因"),
     *                          @OA\Property(property="is_bind_wechat", type="integer", description="是否绑定微信1-是0-否"),
     *                          @OA\Property(property="is_bind_ios", type="integer", description="是否绑定ios账号1-是0-否"),
     *                          @OA\Property(property="true_name", type="string", description="真实姓名"),
     *                          @OA\Property(property="name_change_times", type="integer", description="修改昵称次数"),
     *                          @OA\Property(property="headimg_change_times", type="integer", description="修改头像次数"),
     *                          @OA\Property(
     *                              property="is_upload_idcard",
     *                              type="integer",
     *                              description="是否上传身份证 0未传 1已传 2过期"),
     *                          @OA\Property(property="is_alert", type="integer", description="是否提醒过修改昵称头像 1-提醒过 0-没有")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function info(): ResponseInterface
    {
        $info = UserService::getUserInfo();

        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Post  (
     *      path="/app/user/verify",
     *      tags={"app-用户"},
     *      summary="用户认证申请",
     *      description="用户认证申请",
     *      operationId="AppUserVerifyApply",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="name", type="string", description="真实姓名"),
     *                  @OA\Property(
     *                      property="hospital_ids",
     *                      type="array",
     *                      description="曾服务医院id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(property="province_id", type="integer", description="可服务城市"),
     *                  @OA\Property(property="company_id", type="integer", description="服务公司"),
     *                  @OA\Property(property="other_company_name", type="string", description="当选择其它时输入的公司名"),
     *                  @OA\Property(property="working_age", type="integer", description="推广年限"),
     *                  @OA\Property(
     *                      property="department_ids",
     *                      type="array",
     *                      description="熟悉科室id",
     *                      @OA\Items(type="integer")
     *                  ),
     *                  @OA\Property(
     *                      property="serve_company",
     *                      type="array",
     *                      description="近期服务公司",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="company_id", type="integer", description="公司id"),
     *                          @OA\Property(
     *                              property="field_id",
     *                              type="array",
     *                              description="领域id",
     *                              @OA\Items(type="integer")
     *                          ),
     *                          @OA\Property(
     *                              property="adaption_id",
     *                              type="array",
     *                              description="适应症id",
     *                              @OA\Items(type="integer")
     *                          ),
     *                          @OA\Property(property="product_name", type="string", description="产品名"),
     *                      )
     *                  ),
     *                  @OA\Property(
     *                      property="direction",
     *                      type="integer",
     *                      description="擅长服务方向: 1医院列名服务
     * 2学术营销服务, 值为相应选择项的总和"),
     *                  @OA\Property(property="supplier_id", type="integer", description="邀请人所在供应商的id"),
     *                  @OA\Property(property="invite_aid", type="integer", description="邀请人的id"),
     *                  @OA\Property(property="submitted_role_type",
     *                      type="integer",
     *                      description="用户提交角色类型 1-优锐私人医院代理商 2-优锐商务部代理商 3-优驿代理商 4-优驿个人兼职代表"),
     *                  required={"name", "province_id", "company_id", "working_age", "department_ids"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function addVerify(VerifyRequest $request): ResponseInterface
    {
        $trueName = (string) $request->post('name');
        $hospitalIds = $request->post('hospital_ids', []);
        $provinceId = (int) $request->post('province_id');
        $companyId = (int) $request->post('company_id');
        $otherCompanyName = (string) $request->post('other_company_name', '');
        $workingAge = (int) $request->post('working_age');
        $departmentIds = $request->post('department_ids', []);
        $serveCompany = $request->post('serve_company', []);
        $direction = $request->post('direction', 0);
        $supplierId = $request->post('supplier_id', 0);
        $inviteAid = $request->post('invite_aid', 0);
        $roleType = $request->post('submitted_role_type', 0);
        $extractDirection = self::extractDirection($direction);
        UserService::addVerify(
            $trueName,
            $provinceId,
            $companyId,
            $workingAge,
            $departmentIds,
            $hospitalIds,
            $serveCompany,
            $extractDirection,
            $otherCompanyName,
            $supplierId,
            $inviteAid,
            $roleType
        );

        return $this->response->success([]);
    }


    /**
     *
     * @OA\Get (
     *      path="/app/user/verify",
     *      tags={"app-用户"},
     *      summary="获取用户认证信息",
     *      description="获取用户认证信息",
     *      operationId="AppUserVerifyInfo",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="认证信息",
     *                              @OA\Property(property="true_name", type="string", description="真实姓名"),
     *                              @OA\Property(
     *                                  property="serve_hospitals",
     *                                  type="array",
     *                                  description="可服务医院",
     *                                  @OA\Items(
     *                                      @OA\Property(property="id", type="integer", description="医院id"),
     *                                      @OA\Property(property="name", type="string", description="医院名称")
     *                                  )
     *                              ),
     *                              @OA\Property(
     *                                  property="city",
     *                                  type="object",
     *                                  description="可服务城市",
     *                                  @OA\Property(property="id", type="integer", description="城市id"),
     *                                  @OA\Property(property="name", type="string", description="城市名称")
     *                             ),
     *                             @OA\Property(
     *                                  property="company",
     *                                  type="object",
     *                                  description="公司",
     *                                  @OA\Property(property="id", type="integer", description="公司id"),
     *                                  @OA\Property(property="name", type="string", description="公司名称")
     *                             ),
     *                             @OA\Property(
     *                                  property="serve_company",
     *                                  type="array",
     *                                  description="近期服务过的公司",
     *                                  @OA\Items(
     *                                      @OA\Property(property="id", type="integer", description="服务记录id"),
     *                                      @OA\Property(property="company_name", type="string", description="服务公司名称"),
     *                                      @OA\Property(
     *                                          property="field",
     *                                          type="array",
     *                                          description="治疗领域",
     *                                          @OA\Items(type="string")
     *                                      ),
     *                                      @OA\Property(
     *                                          property="adaption",
     *                                          type="array",
     *                                          description="适应症",
     *                                          @OA\Items(type="string")
     *                                      ),
     *                                      @OA\Property(property="product", type="string", description="产品名称"),
     *                                  )
     *                             ),
     *                             @OA\Property(
     *                                  property="department",
     *                                  type="object",
     *                                  description="熟悉科室",
     *                                  @OA\Property(property="id", type="integer", description="科室id"),
     *                                  @OA\Property(property="name", type="string", description="科室名称")
     *                             ),
     *                             @OA\Property(
     *                                  property="direction",
     *                                  type="array",
     *                                  description="擅长的服务方向",
     *                                  @OA\Items(type="string")
     *                             ),
     *                              @OA\Property(
     *                                  property="last_refuse_reasion",
     *                                  type="array",
     *                                  description="上一次认证被拒绝的理由",
     *                                  @OA\Items(type="string")
     *                             )
     *                          ),
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function verifyDetail(): ResponseInterface
    {
        $info = UserService::getVerifyInfo();
        return $this->response->success([
            'info' => $info
        ]);
    }


    /**
     *
     * @OA\Post (
     *      path="/app/user/avatar",
     *      tags={"app-用户"},
     *      summary="更新用户头像",
     *      description="更新用户头像",
     *      operationId="AppUserUpdateAvatar",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="avatar_id", type="integer", description="头像id"),
     *                  required={"avatar_id"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function updateAvatar(UpdateUserAvatarRequest $request): ResponseInterface
    {
        UserService::updateUserAvatar(
            (int) $request->post('avatar_id')
        );
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/user/mobile",
     *      tags={"app-用户"},
     *      summary="更新用户手机号(登陆帐号)",
     *      description="更新用户手机号(登陆帐号)",
     *      operationId="AppUserUpdateMobile",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="mobile", type="string", description="手机号"),
     *                  @OA\Property(property="code", type="string", description="验证码"),
     *                  required={"mobile", "code"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function updateMobile(UpdateUserMobileRequest $request): ResponseInterface
    {
        UserService::updateUserMobileAccount(
            (string) $request->post('mobile'),
            (string) $request->post('code')
        );
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/user/name",
     *      tags={"app-用户"},
     *      summary="更新用户呢称",
     *      description="更新用户呢称",
     *      operationId="AppUserUpdateName",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="name", type="string", description="用户呢称"),
     *                  required={"name"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function updateName(UpdateUserNameRequest $request): ResponseInterface
    {
        UserService::updateUserName(
            (string) $request->post('name')
        );
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/user/check-bind-mobile",
     *      tags={"app-用户"},
     *      summary="检查用户是否已经绑定手机号",
     *      description="检查用户是否已经绑定手机号",
     *      operationId="AppUserCheckBindMobile",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="status", type="string", description="1-已绑定 0-未绑定"),
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function checkMobileBind(): ResponseInterface
    {
        $isBind = LoginService::checkMobileBind();

        return $this->response->success(['status' => $isBind]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/user/help",
     *      tags={"app-用户"},
     *      summary="求助",
     *      description="求助",
     *      operationId="AppUserSubmitHelp",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="content", type="string", description="提交内容"),
     *                  @OA\Property(property="type", type="integer", description="求助类型 1文献资料 2 视频 3学术问题 4流向纠错"),
     *                  @OA\Property(property="mail", type="string", description="邮箱"),
     *                  required={"content", "type"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws Exception
     */
    public function help(HelpRequest $request): ResponseInterface
    {
        UserService::addHelp(
            (int)$request->post('type'),
            (string)$request->post('content'),
            (string)$request->post('email')
        );

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/user/qrcode",
     *      tags={"app-用户"},
     *      summary="用户个人二维码",
     *      description="用户个人二维码",
     *      operationId="/app/user/qrcode",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="url", type="string", description="二维码地址")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function qrcode()
    {
        return $this->response->success((new UserService)->getQrcode());
    }

    /**
     *
     * @OA\Post (
     *      path="/app/user/alert",
     *      tags={"app-用户"},
     *      summary="提醒用户修改昵称头像",
     *      description="提醒用户修改昵称头像",
     *      operationId="/app/user/alert",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function alert()
    {
        UserService::alertChange();
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/user/pharmacy-invite",
     *      tags={"app-用户"},
     *      summary="pharmacy小程序跳转链接",
     *      description="pharmacy小程序跳转链接",
     *      operationId="AppUserPharmacyInviteLink",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="link", type="string", description="药店小程序跳转药店urllink链接"),
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function getPharmacyAgentInvite(): ResponseInterface
    {
        $link = UserService::getPharmacyAgentInviteInfo();
        return $this->response->success([
            'link' => $link
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/user/pharmacy/doctor-qr",
     *      tags={"app-用户"},
     *      summary="pharmacy小程序生成医生二维码",
     *      description="pharmacy小程序生成医生二维码",
     *      operationId="AppUserPharmacyDoctorQr",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Parameter(
     *         description="医生ID",
     *         in="query",
     *         name="doctor_id",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="link", type="string", description="pharmacy小程序生成医生二维码链接")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetPharmacyDoctorQrcodeRequest $request
     * @return ResponseInterface
     */
    public function getPharmacyDoctorQrcode(GetPharmacyDoctorQrcodeRequest $request)
    {
        $doctorId = $request->input('doctor_id');
        $res = UserService::getPharmacyDoctorQrcode($doctorId);
        return $this->response->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/user/product",
     *      tags={"app-用户"},
     *      summary="用户代理的产品",
     *      description="用户代理的产品",
     *      operationId="product",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="string", description="产品id"),
     *                          @OA\Property(property="series_name", type="string", description="产品名称")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetPharmacyDoctorQrcodeRequest $request
     * @return ResponseInterface
     */
    public function getProduct()
    {
        $res = UserService::getServiceProduct();
        return $this->response->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/user/user-verify-state",
     *      tags={"app-用户"},
     *      summary="获取登陆用户注册和认证状态",
     *      description="获取登陆用户注册和认证状态",
     *      operationId="user-verify-state",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="has_mobile", type="integer", description="0-没手机号 1-有手机号"),
     *                          @OA\Property(
     *                              property="verify_state",
     *                              type="integer",
     *                              description="认证状态
     *                              0表示未填写未认证
     *                              2表示已经填写已认证
     *                              3表示申请认证中
     *                              4表示拒绝认证")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetPharmacyDoctorQrcodeRequest $request
     * @return ResponseInterface
     *
     * @return ResponseInterface
     */
    public function getUserVerifyState()
    {
        $res = UserService::getUserVerifyState();
        return $this->response->success($res);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/user/pharmacy-sales-record-link",
     *      tags={"app-用户"},
     *      summary="pharmacy小程序销售数据录入链接",
     *      description="pharmacy小程序销售数据录入链接",
     *      operationId="GetAppUserPharmacySalesRecordLink",
     *     @OA\Parameter(ref="#/components/parameters/app-version"),
     *     @OA\Parameter(ref="#/components/parameters/app-platform"),
     *     @OA\Parameter(ref="#/components/parameters/device-id"),
     *     @OA\Parameter(ref="#/components/parameters/Authorization"),
     *     @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="sales_link", type="string", description="药店小程序销售数据录入跳转链接"),
     *                          @OA\Property(property="topic_link", type="string", description="药店小程序陈列及培训录入跳转链接")
     *                      )
     *                  )
     *              )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    public function getPharmacySalesRecordLink(): ResponseInterface
    {
        [$salesLink, $topicLink] = UserService::getPharmacySalesAndTopicRecordLink();

        return $this->response->success([
            'sales_link' => $salesLink,
            'topic_link' => $topicLink
        ]);
    }
}
