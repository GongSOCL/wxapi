<?php

namespace App\Controller\App\User;

use App\Controller\Controller;
use App\Request\Fake\FakeAuthRequest;
use App\Request\Fake\GetCodeRequest;
use App\Service\User\FakeService;
use Psr\Http\Message\ResponseInterface;

/**
 * 伪登陆
 */
class FakeController extends Controller
{

    /**
     *
     * @OA\Get  (
     *      path="/app/fake/code",
     *      tags={"app-伪登陆"},
     *      summary="伪登陆获取code",
     *      description="伪登陆获取code",
     *      operationId="AppFakeLoginCode",
     *      @OA\Parameter(
     *          name="uid",
     *          in="query",
     *          description="用户uid[加密后]",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="code", type="string", description="一次性临时code，用于伪登陆"),
     *                      )
     *                  )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function getToken(GetCodeRequest $request): ResponseInterface
    {
        $uid = (string)$request->query("uid");
        $token = FakeService::getToken($uid);
        return $this->response->success([
            'code' => $token
        ]);
    }

    /**
     *
     * @OA\Post  (
     *      path="/app/fake/auth",
     *      tags={"app-伪登陆"},
     *      summary="公众号伪登陆",
     *      description="公众号伪登陆",
     *      operationId="AppFakeAuth",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="code", type="string", description="登陆一次性code"),
     *                  required={"code"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="token", type="string", description="用户登陆token"),
     *                          @OA\Property(property="openid", type="string", description="用户openid"),
     *                          @OA\Property(property="unionid", type="string", description="用户微信unionid"),
     *                      )
     *                  )
     *           )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @throws \Exception
     */
    public function auth(FakeAuthRequest $request): ResponseInterface
    {
        $code = (string)$request->post("code");
        $data = FakeService::auth($code);
        return $this->response->success($data);
    }
}
