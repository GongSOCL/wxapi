<?php

/**
 *
 * @OA\Post (
 *      path="/app/user/idcard/upload",
 *      tags={"app-用户"},
 *      summary="上传身份证",
 *      description="上传身份证",
 *      operationId="/app/user/idcard/upload",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="photo",
 *          in="query",
 *          description="图片",
 *          required=true,
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="file_name", type="string", description="文件名称"),
 *                          @OA\Property(property="user_info", type="object",
 *                              @OA\Property(property="name", type="string", description="用户真实姓名（当上传证件为反面时该参数不存在）"),
 *                              @OA\Property(property="sex", type="string", description="用户性别（当上传证件为反面时该参数不存在）"),
 *                              @OA\Property(property="ethnicity", type="string", description="用户民族（当上传证件为反面时该参数不存在）"),
 *                              @OA\Property(
 *                                  property="birthDate",
 *                                  type="string",
 *                                  description="用户出生日期（当上传证件为反面时该参数不存在）"),
 *                              @OA\Property(property="address", type="string", description="用户家庭住址（当上传证件为反面时该参数不存在）"),
 *                              @OA\Property(
 *                                  property="idNumber",
 *                                  type="string",
 *                                   description="用户身份证号码（当上传证件为反面时该参数不存在）"),
 *                              @OA\Property(
 *                                  property="issueAuthority",
 *                                  type="string",
 *                                  description="用户所属户籍机构（当上传证件为正面时该参数不存在）"),
 *                              @OA\Property(
 *                                  property="validPeriod",
 *                                  type="string",
 *                                  description="用户证件有效期（当上传证件为正面时该参数不存在）"),
 *                          )
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/user/idcard",
 *      tags={"app-用户"},
 *      summary="身份证信息提交",
 *      description="身份证信息提交",
 *      operationId="/app/user/idcard",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="idcard_front",
 *          in="query",
 *          description="身份证正面",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="idcard_back",
 *          in="query",
 *          description="身份证反面",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *              name="name",
 *              in="query",
 *              description="用户真实姓名",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *          @OA\Parameter(
 *              name="sex",
 *              in="query",
 *              description="用户性别",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *          @OA\Parameter(
 *              name="ethnicity",
 *              in="query",
 *              description="用户民族",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *          @OA\Parameter(
 *              name="birthDate",
 *              in="query",
 *              description="用户出生日期",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *          @OA\Parameter(
 *              name="address",
 *              in="query",
 *              description="用户家庭住址",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *          @OA\Parameter(
 *              name="idNumber",
 *              in="query",
 *              description="用户身份证号码",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *          @OA\Parameter(
 *              name="issueAuthority",
 *              in="query",
 *              description="用户所属户籍机构",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *          @OA\Parameter(
 *              name="validPeriod",
 *              in="query",
 *              description="用户证件有效期",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *          @OA\Parameter(
 *              name="mobile",
 *              in="query",
 *              description="手机号",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *          @OA\Parameter(
 *              name="is_upload_idcard",
 *              in="query",
 *              description="身份证是否过期",
 *              required=true,
 *              @OA\Schema(
 *                  type="string"
 *              )
 *          ),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="yyid", type="string", description="用户yyid")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);

namespace App\Controller\App\User;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\User\IdcardRequest;
use App\Request\App\User\IdcardUploadRequest;
use App\Service\Doctor\UserIdentifyService;
use Hyperf\Di\Annotation\Inject;

class IdcardController extends Controller
{
    /**
     * @Inject()
     * @var UserIdentifyService
     */
    private $userIdentify;

    /**
     * @param IdcardUploadRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function upload(IdcardUploadRequest $request)
    {
        $yyid = Helper::getLoginUser()->yyid;
//        $yyid = 'A572E86745C011ECA7E1EE3A3A073612';

        $file = $request->file('photo');

        $fileName = $this->userIdentify->uploadIdcard($yyid, $file);

        return $this->response->success($fileName);
    }

    /**
     * @param IdcardRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function idcard(IdcardRequest $request)
    {
        $front  = $request->input('idcard_front');
        $back   = $request->input('idcard_back');
        $name   = $request->input('name');
        $sex    = $request->input('sex');
        $ethnicity  = $request->input('ethnicity');
        $birthDate  = $request->input('birthDate');
        $address    = $request->input('address');
        $idNumber   = $request->input('idNumber');
        $issueAuthority = $request->input('issueAuthority');
        $validPeriod    = $request->input('validPeriod');
        $mobile         = $request->input('mobile');
        $is_upload_idcard         = $request->input('is_upload_idcard');

        $data = $this->userIdentify->idcardSubmit(
            $front,
            $back,
            $name,
            $sex,
            $ethnicity,
            $birthDate,
            $address,
            $idNumber,
            $issueAuthority,
            $validPeriod,
            $mobile,
            $is_upload_idcard
        );
        return $this->response->success($data);
    }
}
