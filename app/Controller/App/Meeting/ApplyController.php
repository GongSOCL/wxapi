<?php

namespace App\Controller\App\Meeting;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\App\Meeting\AddApplyRequest;
use App\Request\App\Meeting\EditApplyRequest;
use App\Request\App\Meeting\ListApplyRequest;
use App\Service\Meeting\MeetingService;
use App\Service\Meeting\LiveTypeRpcService;
use Psr\Http\Message\ResponseInterface;

class ApplyController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/meeting/apply",
     *      tags={"app-会议申请"},
     *      summary="会议申请列表",
     *      description="会议申请列表",
     *      operationId="AppMeetingApplyList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="大区id",
     *         in="query",
     *         name="zone_id",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="状态: 0所有 1待审核 2已通过 3已拒绝",
     *         in="query",
     *         name="status",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="搜索范围: 1所有申请  2个人申请",
     *         in="query",
     *         name="scope",
     *         required=false,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页码:默认1",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量: 默认10",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="拜访记录列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="会议申请id"),
     *                                  @OA\Property(property="name", type="string", description="会议名称"),
     *                                  @OA\Property(property="applicant_id", type="integer", description="申请人id"),
     *                                  @OA\Property(property="applicant_name", type="string", description="申请人姓名"),
     *                                  @OA\Property(property="apply_time", type="string", description="申请时间"),
     *                                  @OA\Property(property="start", type="string", description="第一场会议开始时间"),
     *                                  @OA\Property(property="end", type="string", description="最后一场会议结束时间"),
     *                                  @OA\Property(
     *                                      property="status",
     *                                      type="integer",
     *                                      description="状态: 1审核中 2审核通过 3已拒绝"),
     *                                  @OA\Property(
     *                                      property="zone",
     *                                      type="object",
     *                                      description="区域",
 *                                          @OA\Property(property="id", type="integer", description="区域id"),
 *                                          @OA\Property(property="name", type="string", description="区域名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="type",
     *                                      type="object",
     *                                      description="项目分类",
     *                                          @OA\Property(property="id", type="integer", description="项目分类id"),
     *                                          @OA\Property(property="name", type="string", description="项目分类名称")
     *                                  )
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function list(ListApplyRequest $request): ResponseInterface
    {
        [$page, $data] = MeetingService::listApply(
            (int) $request->query('zone_id', 0),
            intval($request->query('scope', 1)) == 2,
            (int) $request->query('status', 0),
            (int) $request->query('current', 1),
            (int) $request->query('limit', 10)
        );
        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/meeting/apply",
     *      tags={"app-会议申请"},
     *      summary="申请会议添加",
     *      description="申请会议添加",
     *      operationId="AppMeetingApplyAdd",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="mobile", type="string", description="申请人手机号"),
     *                  @OA\Property(property="manager_id", type="integer", description="管理员id"),
     *                  @OA\Property(property="compere_id", type="integer", description="主持人id"),
     *                  @OA\Property(property="type", type="integer", description="项目类型id"),
     *                  @OA\Property(property="sub_type", type="integer", description="项目名称id"),
     *                  @OA\Property(property="category", type="integer", description="项目分类id"),
     *                  @OA\Property(property="product_id", type="integer", description="药品id"),
     *                  @OA\Property(property="name", type="string", description="会议名称"),
     *                  @OA\Property(property="range", type="integer", description="区域类型: 1区域 2中央"),
     *                  @OA\Property(
     *                      property="time",
     *                      type="array",
     *                      description="会议时间",
     *                      @OA\Items(
     *                           @OA\Property(property="start", type="string", description="开始时间"),
     *                          @OA\Property(property="end", type="string", description="结束时间")
     *                      )
     *                  ),
     *                  @OA\Property(property="zone_id", type="integer", description="大区id"),
     *                  @OA\Property(property="province_id", type="integer", description="大区下省份id"),
     *                  @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                  @OA\Property(property="depart_id", type="integer", description="科室id"),
     *                  @OA\Property(
     *                      property="chairman",
     *                      type="object",
     *                      description="主席讲者",
     *                      @OA\Property(property="name", type="string", description="主席名称"),
     *                      @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                      @OA\Property(property="depart_id", type="integer", description="科室id"),
     *                      @OA\Property(property="level_id", type="integer", description="级别: 1全国KOL 2省市KOL 3区域KOL")
     *                  ),
     *                  @OA\Property(
     *                      property="speaker",
     *                      type="array",
     *                      description="讲者列表",
     *                      @OA\Items(
     *                           @OA\Property(property="name", type="string", description="主席名称"),
     *                          @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                          @OA\Property(property="depart_id", type="integer", description="科室id"),
     *                          @OA\Property(
     *                              property="level_id",
     *                              type="integer",
     *                              description="级别: 1全国KOL 2省市KOL 3区域KOL"),
     *                          @OA\Property(property="project", type="string", description="讲者课题")
     *                      )
     *                  ),
     *                  required={
     *                      "mobile",
     *                      "manager_id",
     *                      "compere_id",
     *                      "type",
     *                      "sub_type",
     *                      "category",
     *                      "product_id",
     *                      "name",
     *                      "time",
     *                      "zone_id",
     *                      "province_id",
     *                      "hospital_id",
     *                      "depart_id",
     *                      "speaker",
     *                      "range"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="id", type="integer", description="会议申请id")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param AddApplyRequest $request
     * @return ResponseInterface
     */
    public function add(AddApplyRequest $request): ResponseInterface
    {
        $times = [];
        foreach ($request->post('time') as $time) {
            $times[] = [
                'start' => Helper::formatInputDate($time['start']),
                'end' => Helper::formatInputDate($time['end'])
            ];
        }
        $apply = MeetingService::addApply(
            (string) $request->post('mobile'),
            (int) $request->post('manager_id'),
            (int) $request->post('compere_id'),
            (int) $request->post('type'),
            (int) $request->post('sub_type'),
            (int) $request->post('category'),
            (int) $request->post('product_id'),
            (string) $request->post('name'),
            (int) $request->post('range'),
            $times,
            (int) $request->post('zone_id'),
            (int) $request->post('province_id'),
            (int) $request->post('hospital_id'),
            (int) $request->post('depart_id'),
            $request->post('speaker'),
            $request->post('chairman', [])
        );
        return $this->response->success([
            'id' => $apply->id
        ]);
    }

    /**
     *
     * @OA\Put (
     *      path="/app/meeting/apply/{id}",
     *      tags={"app-会议申请"},
     *      summary="申请会议编辑",
     *      description="申请会议编辑",
     *      operationId="AppMeetingApplyEdit",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="manager_id", type="integer", description="管理员id"),
     *                  @OA\Property(property="compere_id", type="integer", description="主持人id"),
     *                  @OA\Property(property="mobile", type="string", description="申请人手机号"),
     *                  @OA\Property(property="type", type="integer", description="项目类型id"),
     *                  @OA\Property(property="sub_type", type="integer", description="项目名称id"),
     *                  @OA\Property(property="category", type="integer", description="项目分类id"),
     *                  @OA\Property(property="product_id", type="integer", description="药品id"),
     *                  @OA\Property(property="name", type="string", description="会议名称"),
     *                  @OA\Property(property="range", type="integer", description="区域类型: 1区域 2中央"),
     *                  @OA\Property(
     *                      property="time",
     *                      type="array",
     *                      description="会议时间",
     *                      @OA\Items(
     *                           @OA\Property(property="start", type="string", description="开始时间"),
     *                           @OA\Property(property="end", type="string", description="结束时间")
     *                      )
     *                  ),
     *                  @OA\Property(property="zone_id", type="integer", description="大区id"),
     *                  @OA\Property(property="province_id", type="integer", description="大区下省份id"),
     *                  @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                  @OA\Property(property="depart_id", type="integer", description="科室id"),
     *                  @OA\Property(
     *                      property="chairman",
     *                      type="object",
     *                      description="主席讲者",
     *                      @OA\Property(property="name", type="string", description="主席名称"),
     *                      @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                      @OA\Property(property="depart_id", type="integer", description="科室id"),
     *                      @OA\Property(property="level_id", type="integer", description="级别: 1全国KOL 2省市KOL 3区域KOL")
     *                  ),
     *                  @OA\Property(
     *                      property="speaker",
     *                      type="array",
     *                      description="讲者列表",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer", description="讲者记录id: 新加id为0"),
     *                           @OA\Property(property="name", type="string", description="讲者名称"),
     *                          @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                          @OA\Property(property="depart_id", type="integer", description="科室id"),
     *                          @OA\Property(
     *                              property="level_id",
     *                              type="integer",
     *                              description="级别: 1全国KOL 2省市KOL 3区域KOL"),
     *                          @OA\Property(property="project", type="string", description="讲者课题")
     *                      )
     *                  ),
     *                  required={
     *                      "manager_id",
     *                      "compere_id",
     *                      "type",
     *                      "sub_type",
     *                      "category",
     *                      "product_id",
     *                      "name",
     *                      "time",
     *                      "zone_id",
     *                      "province_id",
     *                      "hospital_id",
     *                      "depart_id",
     *                      "speaker",
     *                      "range"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param EditApplyRequest $request
     * @return ResponseInterface
     */
    public function edit($id, EditApplyRequest $request): ResponseInterface
    {
        $times = [];
        foreach ($request->post('time') as $time) {
            $times[] = [
                'id' => isset($time['id']) ? (int) $time['id'] : 0,
                'start' => Helper::formatInputDate($time['start']),
                'end' => Helper::formatInputDate($time['end'])
            ];
        }
        MeetingService::editApply(
            (int)$id,
            (string) $request->post('mobile'),
            (int) $request->post('manager_id'),
            (int) $request->post('compere_id'),
            (int) $request->post('type'),
            (int) $request->post('sub_type'),
            (int) $request->post('category'),
            (int) $request->post('product_id'),
            (string) $request->post('name'),
            (int) $request->post('range'),
            $times,
            (int) $request->post('zone_id'),
            (int) $request->post('province_id'),
            (int) $request->post('hospital_id'),
            (int) $request->post('depart_id'),
            $request->post('speaker'),
            $request->post('chairman', [])
        );

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/meeting/apply/{id}",
     *      tags={"app-会议申请"},
     *      summary="会议申请详情",
     *      description="会议申请详情",
     *      operationId="AppMeetingApplyDetail",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="申请id",
     *         in="path",
     *         name="scope",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="拜访记录列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="会议申请id"),
     *                                  @OA\Property(property="name", type="string", description="会议名称"),
     *                                  @OA\Property(property="applicant_id", type="integer", description="申请人id"),
     *                                  @OA\Property(property="applicant_name", type="string", description="申请人姓名"),
     *                                  @OA\Property(property="is_editable", type="integer", description="is editable"),
     *                                  @OA\Property(property="is_cancelable", type="integer", description="是否可取消"),
     *                                  @OA\Property(
     *                                      property="status",
     *                                      type="integer",
     *                                      description="状态: 1审核中 2审核通过 3已拒绝"),
     *                                  @OA\Property(property="reason", type="string", description="reject reason"),
     *                                  @OA\Property(
     *                                      property="manager",
     *                                      type="object",
     *                                      description="管理员",
     *                                          @OA\Property(property="id", type="integer", description="管理员id"),
     *                                          @OA\Property(property="name", type="string", description="管理员名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="compere",
     *                                      type="object",
     *                                      description="主持人信息",
     *                                          @OA\Property(property="id", type="integer", description="主持人id"),
     *                                          @OA\Property(property="name", type="string", description="主持人名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="type",
     *                                      type="object",
     *                                      description="项目类型",
     *                                          @OA\Property(property="id", type="integer", description="项目类型id"),
     *                                          @OA\Property(property="name", type="string", description="项目类型名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="sub_type",
     *                                      type="object",
     *                                      description="项目名称",
     *                                          @OA\Property(property="id", type="integer", description="项目名称id"),
     *                                          @OA\Property(property="name", type="string", description="项目名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="category",
     *                                      type="object",
     *                                      description="项目分类",
     *                                          @OA\Property(property="id", type="integer", description="项目分类id"),
     *                                          @OA\Property(property="name", type="string", description="项目分类名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="range",
     *                                      type="object",
     *                                      description="区域范围",
     *                                          @OA\Property(property="id", type="integer", description="区域id"),
     *                                          @OA\Property(property="name", type="string", description="区域名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="product",
     *                                      type="object",
     *                                      description="产品",
     *                                          @OA\Property(property="id", type="integer", description="产品id"),
     *                                          @OA\Property(property="name", type="string", description="产品名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="time",
     *                                      type="array",
     *                                      description="会议时间",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="时间记录id"),
     *                                          @OA\Property(
     *                                              property="start",
     *                                              type="string",
     *                                              description="时间记录id,新加id为0"),
     *                                          @OA\Property(property="end", type="string", description="结束时间")
     *                                      )
     *                                  ),
     *                                  @OA\Property(
     *                                      property="zone",
     *                                      type="object",
     *                                      description="大区",
     *                                          @OA\Property(property="id", type="integer", description="大区id"),
     *                                          @OA\Property(property="name", type="string", description="大区名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="province",
     *                                      type="object",
     *                                      description="省份",
     *                                          @OA\Property(property="id", type="integer", description="省份id"),
     *                                          @OA\Property(property="name", type="string", description="省份名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="hospital",
     *                                      type="object",
     *                                      description="医院",
     *                                          @OA\Property(property="id", type="integer", description="医院id"),
     *                                          @OA\Property(property="name", type="string", description="医院名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="depart",
     *                                      type="object",
     *                                      description="科室",
     *                                          @OA\Property(property="id", type="integer", description="科室id"),
     *                                          @OA\Property(property="name", type="string", description="科室名称")
     *                                  ),
     *                                  @OA\Property(
     *                                      property="chairman",
     *                                      type="object",
     *                                      description="主席讲者,可能为空",
     *                                      @OA\Property(property="id", type="integer", description="人员id"),
     *                                      @OA\Property(property="name", type="string", description="主席名称"),
     *                                      @OA\Property(
     *                                          property="hospital",
     *                                          type="object",
     *                                          description="医院",
     *                                          @OA\Property(property="id", type="integer", description="医院id"),
     *                                          @OA\Property(property="name", type="string", description="医院名称"),
     *                                      ),
     *                                      @OA\Property(
     *                                          property="depart",
     *                                          type="object",
     *                                          description="科室",
     *                                          @OA\Property(property="id", type="integer", description="科室id"),
     *                                          @OA\Property(property="name", type="string", description="科室名称"),
     *                                      ),
     *                                      @OA\Property(
     *                                          property="level_id",
     *                                          type="integer",
     *                                          description="级别: 1全国KOL 2省市KOL 3区域KOL")
     *                              ),
     *                              @OA\Property(
     *                                  property="speaker",
     *                                  type="array",
     *                                  description="讲者列表",
     *                                  @OA\Items(
     *                                      @OA\Property(property="id", type="integer", description="讲者记录id: 新加id为0"),
     *                                      @OA\Property(property="name", type="string", description="讲者名称"),
     *                                      @OA\Property(
     *                                          property="hospital",
     *                                          type="object",
     *                                          description="医院",
     *                                          @OA\Property(property="id", type="integer", description="医院id"),
     *                                          @OA\Property(property="name", type="string", description="医院名称"),
     *                                      ),
     *                                      @OA\Property(
     *                                          property="depart",
     *                                          type="object",
     *                                          description="科室",
     *                                          @OA\Property(property="id", type="integer", description="科室id"),
     *                                          @OA\Property(property="name", type="string", description="科室名称"),
     *                                      ),
     *                                      @OA\Property(
     *                                          property="level_id",
     *                                          type="integer",
     *                                          description="级别: 1全国KOL 2省市KOL 3区域KOL"),
     *                                      @OA\Property(property="project", type="string", description="讲者课题")
     *                                  )
     *                              )
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function info($id): ResponseInterface
    {
        $info = MeetingService::info((int) $id);
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/meeting/apply/types",
     *      tags={"app-会议申请"},
     *      summary="会议申请类型列表",
     *      description="会议申请类型列表",
     *      operationId="AppMeetingApplyTypeList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="父id,默认是0",
     *         in="query",
     *         name="pid",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *     @OA\Parameter(
     *         description="类型: 1. 项目类型 2项目名称 3项目分类",
     *         in="query",
     *         name="type",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="类型列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="类型id"),
     *                                  @OA\Property(property="name", type="string", description="类型名称")
     *                              )
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function getSubTypes(): ResponseInterface
    {
        $pid = (int) $this->request->query('pid', 0);
        $type = (int) $this->request->query('type', 1);
        if ($type <= 0) {
            $type = 1;
        }
        if ($pid < 0) {
            $pid = 0;
        }
        $list = MeetingService::listSubTypes($pid, $type);
        return $this->response->success([
            'list' => $list
        ]);
    }

    /**
     *
     * @OA\Delete  (
     *      path="/app/meeting/apply/{id}",
     *      tags={"app-会议申请"},
     *      summary="取消会议申请",
     *      description="取消会议申请",
     *      operationId="AppMeetingApplyCancel",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *         description="申请id",
     *         in="path",
     *         name="scope",
     *         required=true,
     *         @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function cancel($id): ResponseInterface
    {
        MeetingService::cancel($id);
        return $this->response->success([]);
    }
}
