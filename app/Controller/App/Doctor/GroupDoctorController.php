<?php
declare(strict_types=1);

namespace App\Controller\App\Doctor;

use App\Controller\Controller;
use App\Request\Doctor\EditDoctorInfoRequest;
use App\Request\Doctor\MergeDoctorRequest;
use App\Request\Doctor\SearchValidMergeDoctorRequest;
use App\Service\Doctor\InfoService;
use Psr\Http\Message\ResponseInterface;

class GroupDoctorController extends Controller
{
    /**
     *
     * @OA\Delete  (
     *      path="/app/group-doctors/{id}",
     *      tags={"app-群组医生"},
     *      summary="删除群组医生",
     *      description="删除群组医生",
     *      operationId="AppDeleteGroupDoctor",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="群组医生id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function delete($id): ResponseInterface
    {
        $id = (int) $id;
        InfoService::deleteDoctor($id);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get  (
     *      path="/app/group-doctors/{id}",
     *      tags={"app-群组医生"},
     *      summary="群组医生详情",
     *      description="群组医生详情",
     *      operationId="AppInfoGroupDoctor",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="群组医生id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="详细信息",
     *                              @OA\Property(property="name", type="string", description="姓名"),
     *                              @OA\Property(property="sex", type="integer", description="性别: 1男 2女"),
     *                              @OA\Property(property="mobile", type="string", description="联系电话"),
     *                              @OA\Property(
     *                                  property="hospital",
     *                                  type="object",
     *                                  description="医院信息",
     *                                  @OA\Property(property="id", type="integer", description="关联医院id"),
     *                                  @OA\Property(property="name", type="string", description="关联医院名称")
     *                              ),
     *                              @OA\Property(
     *                                  property="depart",
     *                                  type="object",
     *                                  description="科室信息",
     *                                  @OA\Property(property="id", type="integer", description="关联科室id"),
     *                                  @OA\Property(property="name", type="string", description="关联科室名称")
     *                              ),
     *                              @OA\Property(property="job_title", type="string", description="级别"),
     *                              @OA\Property(property="position", type="string", description="职称"),
     *                              @OA\Property(
     *                                  property="field",
     *                                  type="object",
     *                                  description="擅长领域",
     *                                  @OA\Property(property="id", type="integer", description="擅长领域id"),
     *                                  @OA\Property(property="name", type="string", description="擅长领域名称")
     *                              ),
     *                              @OA\Property(property="field_id", type="integer", description="擅长领域"),
     *                              @OA\Property(property="clinic_type", type="integer", description="门诊类型:0未知 1专家2普通"),
     *                              @OA\Property(property="clinic_rota", type="string", description="门诊值班情况 原样返回")
     *                          ),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function info($id): ResponseInterface
    {
        $id = (int) $id;
        $info = InfoService::getInfo($id);
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/group-doctors/{id}",
     *      tags={"app-群组医生"},
     *      summary="更新群组医生详情",
     *      description="更新群组医生详情",
     *      operationId="AppEditGroupDoctor",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="群组医生id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="name", type="string", description="姓名"),
     *                  @OA\Property(property="sex", type="integer", description="性别: 1男 2女"),
     *                  @OA\Property(property="mobile", type="string", description="联系电话"),
     *                  @OA\Property(property="hospital_id", type="integer", description="医院id"),
     *                  @OA\Property(property="depart_id", type="integer", description="科室id"),
     *                  @OA\Property(property="job_title", type="string", description="级别"),
     *                  @OA\Property(property="position", type="string", description="职称"),
     *                  @OA\Property(property="field_id", type="integer", description="擅长领域"),
     *                  @OA\Property(property="clinic_rota", type="string", description="门诊值班情况 原样返回"),
     *                  required={
     *                      "name",
     *                      "mobile",
     *                      "hospital_id",
     *                      "depart_id"
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param EditDoctorInfoRequest $request
     * @return ResponseInterface
     */
    public function edit($id, EditDoctorInfoRequest $request): ResponseInterface
    {
        InfoService::editDoctorInfo(
            $id,
            (string) $request->post('name'),
            (string) $request->post('mobile'),
            (int) $request->post('hospital_id'),
            (int) $request->post('depart_id'),
            (int) $request->post('sex', 0),
            (string) $request->post('job_title', ''),
            (string) $request->post('position', ''),
            (int) $request->post('field_id', 0),
            (string) $request->post('clinic_rota', '')
        );
        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get  (
     *      path="/app/group-doctors/{id}/valid-merge",
     *      tags={"app-群组医生"},
     *      summary="根据手机号查找可合并医生",
     *      description="根据手机号查找可合并医生",
     *      operationId="ValidMergeGroupDoctor",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="医生id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="mobile",
     *          in="query",
     *          description="手机号",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="详细信息",
     *                              @OA\Property(property="nid", type="integer", description="被合并的医生id"),
     *                              @OA\Property(property="name", type="string", description="姓名"),
     *                              @OA\Property(property="sex", type="integer", description="性别: 1男 2女"),
     *                              @OA\Property(property="mobile", type="string", description="联系电话"),
     *                              @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                              @OA\Property(
     *                                  property="hospital",
     *                                  type="object",
     *                                  description="医院信息",
     *                                  @OA\Property(property="id", type="integer", description="关联医院id"),
     *                                  @OA\Property(property="name", type="string", description="关联医院名称")
     *                              ),
     *                              @OA\Property(
     *                                  property="depart",
     *                                  type="object",
     *                                  description="科室信息",
     *                                  @OA\Property(property="id", type="integer", description="关联科室id"),
     *                                  @OA\Property(property="name", type="string", description="关联科室名称")
     *                              ),
     *                              @OA\Property(property="job_title", type="string", description="级别"),
     *                              @OA\Property(property="position", type="string", description="职称"),
     *                              @OA\Property(property="field_id", type="integer", description="擅长领域"),
     *                              @OA\Property(
     *                                  property="field",
     *                                  type="object",
     *                                  description="擅长领域",
     *                                  @OA\Property(property="id", type="integer", description="擅长领域id"),
     *                                  @OA\Property(property="name", type="string", description="擅长领域名称")
     *                              ),
     *                              @OA\Property(property="clinic_type", type="integer", description="门诊类型:0未知 1专家2普通"),
     *                              @OA\Property(property="clinic_rota", type="string", description="门诊值班情况 原样返回")
     *                          ),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param SearchValidMergeDoctorRequest $request
     * @return ResponseInterface
     */
    public function validMerge($id, SearchValidMergeDoctorRequest $request): ResponseInterface
    {
        $id = (int) $id;
        $info = InfoService::getValidDoctorToMerge($id, (string) $request->query('mobile'));
        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Post (
     *      path="/app/group-doctors/{id}/merge/{mergeId}",
     *      tags={"app-群组医生"},
     *      summary="合并医生",
     *      description="合并医生信息",
     *      operationId="MergeGroupDoctor",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="医生id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="mergeId",
     *          in="path",
     *          description="待合并医生id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="agree",
     *          in="path",
     *          description="1-确认合并 2-不合并",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     */
    /**
     * @param $id
     * @param $mergeId
     * @param MergeDoctorRequest $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function merge($id, $mergeId, MergeDoctorRequest $request): ResponseInterface
    {
        $id = (int) $id;
        $mergeId = (int) $mergeId;
        $agree = $request->input('agree');
        InfoService::mergeDoctor($id, $mergeId, $agree);
        return $this->response->success([]);
    }
}
