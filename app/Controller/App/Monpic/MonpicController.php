<?php

/**
 *
 * @OA\Get (
 *      path="/app/monpic",
 *      tags={"app-弹图"},
 *      summary="代表产品弹图（力蜚能）",
 *      description="代表产品弹图（力蜚能）",
 *      operationId="/app/monpic",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="type",
 *          in="query",
 *          description="1-签入弹图 2-签出弹图 默认1 可以不传",
 *          required=false,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="pic_id", type="string", description="图片id"),
 *                          @OA\Property(property="url", type="string", description="图片地址"),
 *                          @OA\Property(property="date", type="string", description="当天日期")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post (
 *      path="/app/monpic/timeCount",
 *      tags={"app-弹图"},
 *      summary="力蜚能弹图用户停留时长统计",
 *      description="从图片弹出开始每1秒调用一次",
 *      operationId="/app/monpic/timeCount",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Parameter(ref="#/components/parameters/Authorization"),
 *     @OA\Parameter(
 *          name="pic_id",
 *          in="query",
 *          description="获取弹图接口返回的pic_id",
 *          required=true,
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date",
 *          in="query",
 *          description="获取弹图接口返回的date",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Response(
 *          response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="count", type="integer", description="停留秒数")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);

namespace App\Controller\App\Monpic;

use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\Monpic\GetMonpicRequest;
use App\Request\Monpic\TimeRequest;
use App\Service\Monpic\MonpicService;
use Hyperf\Di\Annotation\Inject;

class MonpicController extends Controller
{
    /**
     * @Inject()
     * @var MonpicService
     */
    private $MonpicService;

    /**
     * @param GetMonpicRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \App\Exception\BusinessException
     */
    public function getPic(GetMonpicRequest $request)
    {
        $yyid = Helper::getLoginUser()->yyid;
        $type = $request->input('type', '1');
        $data = $this->MonpicService->getMonpic($yyid, $type);

        return $this->response->success($data);
    }

    /**
     * @param TimeRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function timeCount(TimeRequest $request)
    {
        $uid = Helper::getLoginUser()->uid;
        $picId = $request->input('pic_id');
        $date = $request->input('date');
        $data = $this->MonpicService->setMonpicCount($uid, $picId, $date);

        return $this->response->success(['count' => $data]);
    }
}
