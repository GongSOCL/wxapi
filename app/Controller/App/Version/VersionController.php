<?php

/**
 *
 * @OA\Get (
 *      path="/app/version",
 *      tags={"app-版本信息"},
 *      summary="最新版本信息",
 *      description="最新版本信息",
 *      operationId="//app/version",
 *     @OA\Parameter(ref="#/components/parameters/app-version"),
 *     @OA\Parameter(ref="#/components/parameters/app-platform"),
 *     @OA\Parameter(ref="#/components/parameters/device-id"),
 *     @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="id", type="integer", description="id"),
 *                          @OA\Property(property="app_version", type="string", description="版本号"),
 *                          @OA\Property(property="is_forced_update", type="integer", description="是否强制更新0否1是"),
 *                          @OA\Property(property="version_info", type="string", description="版本信息"),
 *                          @OA\Property(property="os_type", type="string", description="操作系统"),
 *                          @OA\Property(property="download_url", type="string", description="下载链接")
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

declare(strict_types=1);

namespace App\Controller\App\Version;

use App\Service\Version\VersionService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Youyao\Framework\AbstractAction;

class VersionController extends AbstractAction
{
    /**
     * @Inject()
     * @var VersionService
     */
    private $versionService;

    /**
     * @return array
     */
    public function version(RequestInterface $request)
    {
        $osType = $request->header('app-platform');
        $data = $this->versionService->getVersion($osType);
        return $this->success($data);
    }
}
