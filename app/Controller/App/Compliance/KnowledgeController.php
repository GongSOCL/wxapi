<?php


namespace App\Controller\App\Compliance;

use App\Controller\Controller;
use App\Request\Compliance\Knowledge\SearchKnowledgeRequest;
use App\Request\Compliance\Knowledge\TreeViewKnowledgeRequest;
use App\Service\Compliance\KnowledgeService;
use Psr\Http\Message\ResponseInterface;

class KnowledgeController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="/app/knowledge/top-dirs",
     *      tags={"app-合规-知识库"},
     *      summary="顶级目录列表",
     *      description="顶级目录列表",
     *      operationId="AppKnowledgeTopDirList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                           @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="顶级目录列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="目录id"),
     *                                  @OA\Property(property="name", type="string", description="目录名")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function listTopDir(): ResponseInterface
    {
        $topDirs = KnowledgeService::topDirs();
        return $this->response->success([
            'list' => $topDirs
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/knowledge/search",
     *      tags={"app-合规-知识库"},
     *      summary="搜索",
     *      description="搜索",
     *      operationId="AppKnowledgeSearch",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                               property="list",
     *                              type="array",
     *                              description="符合规则知识库文件列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                   @OA\Property(property="id", type="string", description="文件yyid"),
     *                                   @OA\Property(property="name", type="string", description="文件名")
     *                              )
     *                          )
     *                      ),
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function search(): ResponseInterface
    {
        $keywords = (string) $this->request->query('keywords');
        $knowledge = KnowledgeService::search($keywords, true);
        return $this->response->success([
            'list' => $knowledge
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="app/knowledge",
     *      tags={"app-合规-知识库"},
     *      summary="知识库树状列表",
     *      description="知识库树状列表",
     *      operationId="AppKnowledgeTreeList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="dir_id",
     *          in="query",
     *          description="顶级目录id",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                               property="list",
     *                              type="array",
     *                              description="知识库树状列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="节点id"),
     *                                  @OA\Property(property="name", type="string", description="节点名称"),
     *                                  @OA\Property(
     *                                      property="type",
     *                                      type="integer",
     *                                      description="节点类型： 1目录 2知识库资源文件"
     *                                  ),
     *                                  @OA\Property(
     *                                      property="children",
     *                                      type="array",
     *                                      description="子节点",
     *                                      @OA\Items(
     *                                          type="object",
     *                                          @OA\Property(property="id", type="integer", description="节点id"),
     *                                          @OA\Property(property="name", type="string", description="节点名称"),
     *                                          @OA\Property(
     *                                              property="type",
     *                                              type="integer",
     *                                              description="节点类型： 1目录 2知识库资源文件"
     *                                          ),
     *                                          @OA\Property(
     *                                              property="children",
     *                                              type="array",
     *                                              description="子节点",
     *                                              @OA\Items(
     *                                                  type="object",
     *                                                  @OA\Property(
     *                                                      property="id",
     *                                                      type="integer",
     *                                                      description="节点id"),
     *                                                  @OA\Property(
     *                                                      property="name",
     *                                                      type="string",
     *                                                      description="节点名称"),
     *                                                  @OA\Property(
     *                                                      property="type",
     *                                                      type="integer",
     *                                                      description="节点类型： 1目录 2知识库资源文件"
     *                                                  )
     *                                          )
     *                                          )
     *                                      )
     *                                  )
     *                              )
     *                          )
     *                      ),
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function list(): ResponseInterface
    {
        $dirId = (int) $this->request->query('dir_id', 0);
        $view = KnowledgeService::treeView($dirId, true);

        return $this->response->success([
            'list' => $view
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="app/knowledge/{id}",
     *      tags={"app-合规-知识库"},
     *      summary="知识库资源详情",
     *      description="知识库资源文件详情",
     *      operationId="AppKnowledgeInfo",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="资源文件id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="知识库文件详情",
     *                              @OA\Property(property="title", type="string", description="标题"),
     *                              @OA\Property(property="intro", type="string", description="简介"),
     *                              @OA\Property(property="type", type="integer", description="类型: 1视频 2文档"),
     *                              @OA\Property(property="link", type="string", description="链接"),
     *                              @OA\Property(
     *                                  property="create_time",
     *                                  type="string",
     *                                  example="2021-10-28 00:00:00", description="创建时间")
     *                          )
     *                      )
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function info($id): ResponseInterface
    {
        $id = (int) $id;
        $info = KnowledgeService::infoKnowledge($id);

        return $this->response->success([
            'info' => $info
        ]);
    }
}
