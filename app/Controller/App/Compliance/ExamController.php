<?php
declare(strict_types=1);

namespace App\Controller\App\Compliance;

use App\Controller\Controller;
use App\Request\App\Exam\AgreeRequest;
use App\Request\App\Exam\AnswerExamRequest;
use App\Request\App\Exam\FinishExamRequest;
use App\Service\Compliance\ExamService;
use Psr\Http\Message\ResponseInterface;

class ExamController extends Controller
{
    /**
     *
     * @OA\Get (
     *      path="app/exams",
     *      tags={"app-合规-考试"},
     *      summary="考试列表",
     *      description="考试列表",
     *      operationId="AppExamList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="status",
     *          in="query",
     *          description="搜索状态: 0全部 1进行中 2已通过 3未通过 4已过期",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页码:默认1",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量: 默认10",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                               property="list",
     *                              type="array",
     *                              description="考试列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="考试id"),
     *                                  @OA\Property(property="name", type="string", description="考试名称"),
     *                                  @OA\Property(
     *                                      property="is_time_unlimited",
     *                                      type="integer",
     *                                      description="是否不限时: 0否 1是"),
     *                                  @OA\Property(
     *                                      property="start",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试开始时间"),
     *                                  @OA\Property(
     *                                      property="end",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试结束时间"),
     *                                  @OA\Property(
     *                                      property="status",
     *                                      type="integer",
     *                                      description="考试状态:0未开始 1进行中 2未通过 3已通过 4已过期 5未通过，不允许再考"),
     *                                  @OA\Property(
     *                                      property="pass_time",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试通过时间: 当状态是3时"),
     *                                  @OA\Property(
     *                                      property="exam_num",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试次数"),
     *                                  @OA\Property(
     *                                      property="exam_time",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试时长"),
     *                                  @OA\Property(
     *                                      property="date_type",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="计划时间类型【判断是否存在】"),
     *                                  @OA\Property(
     *                                      property="dt_off",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="计划时间2【判断是否存在】"),
     *                                  @OA\Property(
     *                                      property="dt_on",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="计划时间1【判断是否存在】"),
     *                                  @OA\Property(
     *                                      property="exam_type",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="是否关联计划 1原始考试
     * 2固定时长内完成学习计划和考试3
     *     完成学习计划后plan_days天内完成考试4
     *     学习计划结束后plan_days开启并完成考试"),
     *                                  @OA\Property(
     *                                      property="plan_status",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="是否完成计划 默认1进行中 6计划完成【开始考试时判断此值，判断是否存在】"),
     *                                  @OA\Property(
     *                                      property="plan_name",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="计划名称【判断是否存在】"),
     *                              )
     *                          )
     *                      )
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @return ResponseInterface
     */
    public function list(): ResponseInterface
    {
        $keywords = (string)$this->request->query('keywords', '');
        $status = (int) $this->request->query('status', 0);
        $current = (int) $this->request->query('current', 1);
        $limit = (int) $this->request->query('limit', 20);
        [$page, $data] = ExamService::list($keywords, $status, $current, $limit);

        return $this->response->success([
            'list' => $data,
            'page' => $page
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="app/exams/{id}",
     *      tags={"app-合规-考试"},
     *      summary="考试详情",
     *      description="考试详情",
     *      operationId="AppExamInfo",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="考试详情",
     *                              @OA\Property(property="name", type="string", description="考试名称"),
     *                              @OA\Property(
     *                                  property="status",
     *                                  type="integer",
     *                                  description="考试状态:0未开始 1进行中 2未通过 3已通过 4已过期"
     *                              ),
     *                              @OA\Property(property="desc", type="string", description="考试说明"),
     *                              @OA\Property(property="is_review", type="integer", description="是否可以回顾: 0否 1是"),
     *                              @OA\Property(
     *                                  property="last_time",
     *                                  type="string",
     *                                  example="2021-10-01 00:00:00",
     *                                  description="最后一次考试时间:is_review==1时显示"
     *                              ),
     *                              @OA\Property(
     *                                      property="with_agreement",
     *                                      type="integer",
     *                                      description="考试是否附带协议: 0否 1是"),
     *                              @OA\Property(
     *                                      property="is_sign",
     *                                      type="integer",
     *                                      description="是否可以签署协议: 0否 1是"),
     *                              @OA\Property(
     *                                      property="sign_time",
     *                                      type="string",
     *                                      description="协议签署时间"),
     *                              @OA\Property(
     *                                      property="is_time_unlimited",
     *                                      type="integer",
     *                                      description="是否不限时: 0否 1是"),
     *                              @OA\Property(
     *                                      property="start",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试开始时间"),
     *                              @OA\Property(
     *                                      property="end",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="考试结束时间"),
     *                                  @OA\Property(
     *                                      property="date_type",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="计划时间类型"),
     *                                  @OA\Property(
     *                                      property="dt_off",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="计划时间2"),
     *                                  @OA\Property(
     *                                      property="dt_on",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="计划时间1"),
     *                                  @OA\Property(
     *                                      property="exam_type",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="是否关联计划 1原始考试2固定时长内完成学习计划和考试
     * 3完成学习计划后plan_days天内完成考试
     *     4学习计划结束后plan_days开启并完成考试"),
     *                                  @OA\Property(
     *                                      property="plan_status",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="是否完成计划 默认1进行中 6计划完成【开始考试时判断此值】"),
     *                                  @OA\Property(
     *                                      property="plan_name",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="计划名称】"),
     *                                  @OA\Property(
     *                                      property="plan_id",
     *                                      type="string",
     *                                      example="2021-10-01",
     *                                      description="计划ID】"),
     *                          )
     *                      )
     *
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function info($id): ResponseInterface
    {
        $id = (int)$id;
        $info = ExamService::getExamInfo($id);

        return $this->response->success([
            'info' => $info
        ]);
    }

    /**
     *
     * @OA\Post  (
     *      path="app/exams/{id}/start",
     *      tags={"app-合规-考试"},
     *      summary="开始考试",
     *      description="开始考试",
     *      operationId="AppExamStart",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="is_sub", type="string", description="第一题题目id"),
     *                          @OA\Property(property="next_question_id", type="integer", description="第一题题目id"),
     *                          @OA\Property(property="total", type="integer", description="总题目id"),
     *                          @OA\Property(property="request_id", type="string", description="请求id,后面答题需要带上"),
     *                          @OA\Property(property="pre_item_id", type="string", description="上一题id"),
     *                          @OA\Property(property="use_timen", type="string", description="考试时长"),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function start($id): ResponseInterface
    {
        $id = (int) $id;
        [$nextQuestionId, $total, $requestId, $useTimen, $preItemId, $is_sub] = ExamService::startExam($id);
        return $this->response->success([
            'pre_item_id' => $preItemId,
            'next_question_id' => $nextQuestionId,
            'total' => $total,
            'request_id' => $requestId,
            'use_timen' => $useTimen,
            'is_sub' => $is_sub
        ]);
    }

    /**
     *
     * @OA\Get  (
     *      path="app/exams/{id}/question",
     *      tags={"app-合规-考试"},
     *      summary="获取考试试题",
     *      description="获取考试试题",
     *      operationId="AppExamQuestionGet",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="question_id",
     *          in="query",
     *          description="试题id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="request_id",
     *          in="query",
     *          description="请求id, start接口返回的",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="item_info", type="string", description="题目已选择答案"),
     *                          @OA\Property(property="s_num", type="integer", description="题目序号"),
     *                          @OA\Property(property="pre_item_id", type="string", description="上一题"),
     *                          @OA\Property(property="question_id", type="integer", description="题目id"),
     *                          @OA\Property(property="name", type="string", description="考试题目"),
     *                          @OA\Property(
     *                              property="type",
     *                              type="object",
     *                              description="题目类型",
     *                              @OA\Property(property="id", type="integer", description="类型id: 1单选 2多选 3输入"),
     *                              @OA\Property(property="desc", type="string", description="类型描述: 如单选\多选\输入"),
     *                          ),
     *                          @OA\Property(
     *                              property="options",
     *                              type="array",
     *                              description="题目选项",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="选项id"),
     *                                  @OA\Property(property="name", type="string", description="选项标题")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function getQuestion($id): ResponseInterface
    {
        $id = (int)$id;
        $questionId = (int)$this->request->query('question_id');
        $requestId = (string)$this->request->query('request_id');
        $info = ExamService::getQuestion(
            $id,
            $questionId,
            $requestId
        );

        return $this->response->success($info);
    }

    /**
     *
     * @OA\Post   (
     *      path="app/exams/{id}/answer",
     *      tags={"app-合规-考试"},
     *      summary="考试回答题目",
     *      description="考试回答题目",
     *      operationId="AppExamQuestionAnswer",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="question_id", type="integer", description="试题id"),
     *                     @OA\Property(property="request_id", type="string", description="请求id"),
     *                     @OA\Property(
     *                          property="option_ids",
     *                          type="array",
     *                          description="选中答案选项id",
     *                          @OA\Items(type="integer")
     *                     ),
     *                     @OA\Property(
     *                          property="content",
     *                          type="string",
     *                          description="文本类型题目输入内容",
     *                     ),
     *                     required={"question_id", "request_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="result",
     *                              type="array",
     *                              description="答题结果",
     *                              @OA\Items(
     *                                  @OA\Property(
     *                                      property="is_question_pass",
     *                                      type="integer",
     *                                      description="该题目是否通过: 0否 1是"),
     *                                  @OA\Property(
     *                                      property="is_last",
     *                                      type="integer",
     *                                      description="是否最后一题: 0否 1是"),
     *                                  @OA\Property(
     *                                      property="next_question_id",
     *                                      type="integer",
     *                                      description="下一题题目id"),
     *                                  @OA\Property(
     *                                      property="is_explain",
     *                                      type="integer",
     *                                      description="是否答案解析: 0否 1是"),
     *                                  @OA\Property(
     *                                      property="explain",
     *                                      type="string",
     *                                      description="答案解析"),
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function answer($id, AnswerExamRequest $request): ResponseInterface
    {
        $id = (int)$id;
        $info = ExamService::answerExam(
            $id,
            (int)$request->post('question_id'),
            (string)$request->post('request_id'),
            $request->post('option_ids', []),
            (string)$request->post('content')
        );
//
        return $this->response->success([
            'result' => $info
        ]);
    }

    /**
     *
     * @OA\Post   (
     *      path="app/exams/{id}/submit",
     *      tags={"app-合规-考试"},
     *      summary="考试交卷",
     *      description="考试交卷",
     *      operationId="AppExamQuestionSubmit",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="request_id", type="string", description="请求id"),
     *                     required={"request_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="result",
     *                              type="object",
     *                              description="答题结果",
     *                                  @OA\Property(
     *                                      property="with_agreement",
     *                                      type="integer",
     *                                      description="是否包含协议: 0否 1是"),
     *                                  @OA\Property(
     *                                      property="is_exam_pass",
     *                                      type="integer",
     *                                      description="考试是否通过: 0否 1是"),
     *                                  @OA\Property(property="last_time", type="string", description="最后提交时间"),
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @param FinishExamRequest $request
     * @return ResponseInterface
     */
    public function finish($id, FinishExamRequest $request): ResponseInterface
    {
        $id = (int)$id;
        $info = ExamService::finishExam($id, (string)$request->post('request_id'));

        return $this->response->success([
            'result' => $info
        ]);
    }

    /**
     *
     * @OA\Get  (
     *      path="app/exams/{id}/review",
     *      tags={"app-合规-考试"},
     *      summary="试题回顾",
     *      description="试题回顾",
     *      operationId="AppExamReview",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="request_id",
     *          in="query",
     *          description="答题请求id,刚刚完成考试后需要带上",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="回顾列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="题目id"),
     *                                  @OA\Property(property="name", type="string", description="题目名称"),
     *                                  @OA\Property(
     *                                      property="type",
     *                                      type="object",
     *                                      description="题目类型",
     *                                      @OA\Property(
     *                                          property="id",
     *                                           type="integer",
     *                                           description="类型id: 1单选 2多选 3输入"),
     *                                      @OA\Property(
     *                                          property="desc",
     *                                           type="string",
     *                                           description="类型描述: 如单选\多选\输入"),
     *                                  ),
     *                                  @OA\Property(
     *                                      property="options",
     *                                      type="array",
     *                                      description="题目选项",
     *                                      @OA\Items(
     *                                          @OA\Property(property="id", type="integer", description="选项id"),
     *                                          @OA\Property(property="name", type="string", description="选项内容"),
     *                                          @OA\Property(property="is_choosed", type="integer", description="是否勾选"),
     *                                          @OA\Property(property="is_right", type="integer", description="答案是否正确")
     *                                      )
     *                                  ),
     *                                  @OA\Property(property="is_right", type="integer", description="当前题目是否答对了"),
     *                                  @OA\Property(property="explain", type="string", description="答案解析"),
     *                                  @OA\Property(property="content", type="string", description="文本类型输入内容"),
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function review($id): ResponseInterface
    {
        $id =(int)$id;
        $requestId = (string)$this->request->query('request_id', '');
        $reviews = ExamService::review($id, $requestId);

        return $this->response->success([
            'list' => $reviews
        ]);
    }

    /**
     *
     * @OA\Get  (
     *      path="app/exams/{id}/agreement",
     *      tags={"app-合规-考试"},
     *      summary="获取考试协议内容",
     *      description="获取考试协议内容",
     *      operationId="AppExamGetAgreement",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="content", type="string", description="考试协议内容")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function getAgreement($id): ResponseInterface
    {
        $id = (int)$id;
        $agreement = ExamService::getExamAgreement($id);

        return $this->response->success([
            'content' => $agreement->agreement_content
        ]);
    }

    /**
     *
     * @OA\Post   (
     *      path="app/exams/{id}/agreement",
     *      tags={"app-合规-考试"},
     *      summary="签署考试协议",
     *      description="签署考试协议",
     *      operationId="AppExamAgreeAgreement",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="sign_img", type="string", description="图片base64数据"),
     *                     required={"sign_img"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="pdf", type="string", description="签名图片url"),
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function agree($id, AgreeRequest $request): ResponseInterface
    {
        $id = (int)$id;
        $img = (string)$request->post('sign_img');
        $agree = ExamService::agree($id, $img);

        return $this->response->success([
            'pdf' => $agree->agree_pdf
        ]);
    }

    /**
     *
     * @OA\Get  (
     *      path="app/exams/{id}/agree-view",
     *      tags={"app-合规-考试"},
     *      summary="获取签署后的协议pdf",
     *      description="获取签署后的协议pdf",
     *      operationId="AppExamGetAgreePdf",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="考试id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="pdf", type="string", description="签署后的协议pdf链接")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param $id
     * @return ResponseInterface
     */
    public function viewAgreement($id): ResponseInterface
    {
        $id = (int)$id;
        $userAgree = ExamService::getExamAgree($id);

        return $this->response->success([
            'pdf' => $userAgree->agree_pdf
        ]);
    }
}
