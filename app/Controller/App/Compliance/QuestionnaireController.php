<?php

declare(strict_types=1);

namespace App\Controller\App\Compliance;

use App\Controller\Controller;
use App\Request\App\Questionnaire\InviteRequest;
use App\Request\App\Questionnaire\NoticeAnswerRequest;
use App\Service\Compliance\QuestionnaireRefService;
use Psr\Http\Message\ResponseInterface;

class QuestionnaireController extends Controller
{
    /**
     *
     * @OA\Post(
     *      path="/app/questionnaires/{id}/re-invite",
     *      tags={"app-合规-问卷"},
     *      summary="重新邀请",
     *      description="问卷",
     *      operationId="AppQuestionnaireReInvite",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                 type="object",
     *                 @OA\Property(property="invite_id", type="integer", description="邀请记录id"),
     *                 required={"invite_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object"
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     *
     * @param $id
     * @param NoticeAnswerRequest $request
     * @return ResponseInterface
     */
    public function reInvite($id, NoticeAnswerRequest $request): ResponseInterface
    {
        $id = (int)$id;
        $inviteId = (int)$request->post('invite_id');
        QuestionnaireRefService::appReInviteDoctor($id, $inviteId);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Post(
     *      path="/app/questionnaires/{id}/notice-answer",
     *      tags={"app-合规-问卷"},
     *      summary="提醒作答",
     *      description="问卷",
     *      operationId="AppQuestionnaireNoticeAnswer",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                 type="object",
     *                 @OA\Property(property="invite_id", type="integer", description="邀请记录id"),
     *                 required={"invite_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(property="data",type="object")
     *                  )
     *          )
     *      ),
     *     @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     *
     * @param $id
     * @param NoticeAnswerRequest $request
     * @return ResponseInterface
     */
    public function noticeAnswer($id, NoticeAnswerRequest $request): ResponseInterface
    {
        $id = (int)$id;
        $inviteId = (int)$request->post('invite_id');
        QuestionnaireRefService::appNoticeToAnswer($id, $inviteId);

        return $this->response->success([]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/questionnaires",
     *      tags={"app-合规-问卷"},
     *      summary="问卷列表",
     *      description="问卷",
     *      operationId="AppQuestionnaireList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="status",
     *          in="query",
     *          description="试卷状态: 1进行中 2已结束",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="问卷列表",
     *                              @OA\Items(
     *                                  @OA\Property(property="id", type="integer", description="问卷id"),
     *                                  @OA\Property(property="name", type="string", description="问卷名称"),
     *                                  @OA\Property(property="invited", type="integer", description="已经邀请人数"),
     *                                  @OA\Property(property="rest_invite", type="integer", description="可用邀请名额"),
     *                                  @OA\Property(property="invite_quota", type="integer", description="邀请总量"),
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *     @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     * @return ResponseInterface
     */
    public function list(): ResponseInterface
    {
        $status = (int) $this->request->query('status', 1);
        $result = QuestionnaireRefService::appGetAgentsPapers(
            $status == 1
        );
        return $this->response->success([
            'list' => $result
        ]);
    }

    /**
     *
     * @OA\Get (
     *      path="/app/questionnaires/{id}/invitees",
     *      tags={"app-合规-问卷"},
     *      summary="问卷受邀请人列表",
     *      description="问卷",
     *      operationId="AppQuestionnaireInviteesList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="问卷id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="invite_more", type="integer", description="是否可以邀请更多: 0不可 1可以"),
     *                          @OA\Property(property="paper_name", type="string", description="问卷名称"),
     *                          @OA\Property(
     *                              property="statistical",
     *                              type="object",
     *                              description="统计数据",
     *                              @OA\Property(property="invite_quota", type="integer", description="总计"),
     *                              @OA\Property(property="answered", type="integer", description="已回答"),
     *                              @OA\Property(property="invalid", type="integer", description="已作废"),
     *                              @OA\Property(property="rest_invite", type="integer", description="可邀请")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="邀请人员列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(property="id", type="integer", description="受邀记录id"),
     *                                  @OA\Property(property="wechat_name", type="string", description="受邀请人微信呢称"),
     *                                  @OA\Property(
     *                                      property="status",
     *                                      type="integer",
     *                                      description="受邀请人回答状态: 1 未作答 2 已作答 3 已作废"),
     *                                  @OA\Property(property="invitee_openid", type="string", description="受邀请人openid")
     *                              )
     *                          )
     *                      )
     *                  )
     *          )
     *      ),
     *     @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     * @param $id
     * @return ResponseInterface
     */
    public function inviteeList($id): ResponseInterface
    {
        $id = (int) $id;
        $result = QuestionnaireRefService::getAppAgentInvitees($id);

        return $this->response->success($result);
    }

    /**
     *
     * @OA\Put (
     *      path="/app/questionnaires/{id}/invite",
     *      tags={"app-合规-问卷"},
     *      summary="邀请医生",
     *      description="问卷",
     *      operationId="AppQuestionnaireInvite",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="问卷id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                     type="object",
     *                     @OA\Property(property="doctor_id", type="integer", description="医生id"),
     *                     required={"doctor_id"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     * @param $id
     * @param InviteRequest $request
     * @return ResponseInterface
     */
    public function invite($id, InviteRequest $request): ResponseInterface
    {
        $id = (int)$id;
        $doctorId = (int) $request->post('doctor_id');
        QuestionnaireRefService::appInviteDoctor($id, $doctorId);

        return $this->response->success([]);
    }


    /**
     *
     * @OA\Get (
     *      path="/app/questionnaires/{id}/doctors",
     *      tags={"app-合规-问卷"},
     *      summary="问卷可邀请医生列表",
     *      description="问卷",
     *      operationId="AppQuestionnaireDoctorList",
     *      @OA\Parameter(ref="#/components/parameters/app-version"),
     *      @OA\Parameter(ref="#/components/parameters/app-platform"),
     *      @OA\Parameter(ref="#/components/parameters/device-id"),
     *      @OA\Parameter(ref="#/components/parameters/Authorization"),
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="问卷id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="keywords",
     *          in="query",
     *          description="医生微信名搜索关键字",
     *          required=false,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="current",
     *          in="query",
     *          description="当前页，默认1",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          in="query",
     *          description="每页显示数量,默认10",
     *          required=false,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="page",
     *                              type="object",
     *                              description="分页数据",
     *                              @OA\Property(property="total", type="integer", description="总数量"),
     *                              @OA\Property(property="current", type="integer", description="当前页数"),
     *                              @OA\Property(property="pages", type="integer", description="总页数"),
     *                              @OA\Property(property="limit", type="integer", description="每页显示数量")
     *                          ),
     *                          @OA\Property(
     *                              property="list",
     *                              type="array",
     *                              description="医生列表",
     *                              @OA\Items(
     *                                  type="object",
     *                                  @OA\Property(
     *                                      property="doctor_id",
     *                                      type="integer",
     *                                      description="doctor_openid表医生id"),
     *                                  @OA\Property(
     *                                      property="is_followed",
     *                                      type="integer",
     *                                      description="是否已关注: 0未关注 1已关注"),
     *                                  @OA\Property(property="name", type="string", description="微信名"),
     *                                  @OA\Property(property="openid", type="string", description="微信open-id"),
     *                                  @OA\Property(
     *                                      property="status",
     *                                      type="integer",
     *                                      description="状态: 0未关注 1已关注未邀请 2已邀请, 只有状态是1的记录可以发送邀请"
     *                                  ),
     *                              )
     *                          ),
     *
     *                      )
     *                  )
     *          )
     *      ),
     *     @OA\Response(response="4002",ref="#/components/responses/params_error")
     *  )
     * @param $id
     * @return ResponseInterface
     */
    public function doctorList($id): ResponseInterface
    {
        $id = (int) $id;
        $keywords = (string) $this->request->query('keywords');
        $current = (int) $this->request->query('current', 1);
        $limit = (int) $this->request->query('limit', 10);

        [$page, $data] = QuestionnaireRefService::getAppPaperAvailableWechatDoctor($id, $keywords, $current, $limit);

        return $this->response->success([
            'page' => $page,
            'list' => $data
        ]);
    }
}
