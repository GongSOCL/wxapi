<?php
declare(strict_types=1);

namespace App\Controller;

use App\Helper\Helper;
use Hyperf\HttpMessage\Cookie\Cookie;
use Hyperf\HttpMessage\Exception\HttpException;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use Hyperf\HttpServer\Response as HttpResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * Class Response
 * 参考: "heartide/response"库并修改返回形式
 *
 * @package App\Controller\Response
 */
class Response extends HttpResponse
{
    /**
     * Response constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $response = $container->get(ResponseInterface::class);
        $requestId = Helper::getRequestId();
        if ($requestId) {
            $response = $response->withHeader('x-request-id', $requestId);
        }

        return $this->response = $response;
    }

    /**
     * @param array $data
     * @return PsrResponseInterface
     */
    public function success(array $data = []): PsrResponseInterface
    {
        return $this->response->json([
            'errcode' => 0,
            'errmsg' => "Success",
            'data' => $data,
        ]);
    }

    /**
     * @param string $message
     * @param int    $code
     * @return PsrResponseInterface
     */
    public function fail(string $message = '', int $code = 1): PsrResponseInterface
    {
        return $this->response->json([
            'errcode' => $code,
            'errmsg' => $message,
            'data' => []
        ]);
    }

    /**
     * @param Cookie $cookie
     * @return $this
     */
    public function cookie(Cookie $cookie)
    {
        $response = $this->response()->withCookie($cookie);
        Context::set(PsrResponseInterface::class, $response);
        return $this;
    }

    /**
     * @param HttpException $throwable
     * @return PsrResponseInterface
     */
    public function handleException(HttpException $throwable): PsrResponseInterface
    {
        return $this->response()
            ->withAddedHeader('Server', 'youyao')
            ->withStatus($throwable->getStatusCode())
            ->withBody(new SwooleStream($throwable->getMessage()));
    }

    /**
     * @return PsrResponseInterface
     */
    public function response(): PsrResponseInterface
    {
        return Context::get(PsrResponseInterface::class);
    }

    public function object2Array($object): array
    {
        if ($object) {
            return $object->toArray();
        }
        return [];
    }
}
