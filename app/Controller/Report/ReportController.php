<?php
/**
 *
 * @OA\Post(
 *      path="/report/visit",
 *      tags={"报告"},
 *      summary="拜访报告",
 *      description="拜访报告",
 *      operationId="/report/visit",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="month", type="integer", description="月份"),
 *                          @OA\Property(
 *                              property="id",
 *                              type="integer",
 *                              description="Timestamp for previous time user accessed the site."
 *                          ),
 *                          @OA\Property(property="ins_class", type="integer", description="机构级别"),
 *                          @OA\Property(property="of_tgt_ins", type="string", description="目标机构数量"),
 *                          @OA\Property(property="of_ins_visited", type="integer", description="拜访机构数量"),
 *                          @OA\Property(property="coverage", type="integer", description="拜访率"),
 *                          @OA\Property(property="of_calls", type="integer", description="拜访数"),
 *                          @OA\Property(property="frequency", type="string", description="拜访频率"),
 *                          @OA\Property(property="base_num", type="integer", description="base地已列名机构数量"),
 *                          @OA\Property(property="base_standard_num", type="string", description="base地已列名机构拜访达标数量"),
 *                          @OA\Property(property="base_rage", type="integer", description="base地已列名达标率"),
 *                          @OA\Property(property="vacant_days", type="integer", description="空岗天数"),
 *                          @OA\Property(property="dif", type="integer", description="DFI"),
 *                          @OA\Property(property="dif_percent", type="string", description="DFI比例"),
 *                          @OA\Property(property="working_hours", type="string", description="空岗天数/DFI"),
 *                      ),
 *                      example={
 *                          "errcode":0,
 *                          "errmsg":"Success",
 *                          "data":{
 *                              "month": "4",
                                "report": {
                                    "产品1": {
                                        "三级": {
                                            "id": 3,
                                            "product_id": "3",
                                            "ins_class": "三级",
                                            "of_tgt_ins": null,
                                            "of_ins_visited": null,
                                            "coverage": null,
                                            "of_calls": null,
                                            "frequency": null,
                                            "base_num": null,
                                            "base_standard_num": null,
                                            "base_rage": null,
                                            "vacant_days": null,
                                            "dif": null,
                                            "dif_percent": null,
                                            "working_hours": null,
                                            "product_name_cn": "E"
                                        },
                                        "其他": {
                                            "id": 4,
                                            "product_id": "3",
                                            "ins_class": null,
                                            "of_tgt_ins": null,
                                            "of_ins_visited": null,
                                            "coverage": null,
                                            "of_calls": null,
                                            "frequency": null,
                                            "base_num": null,
                                            "base_standard_num": null,
                                            "base_rage": null,
                                            "vacant_days": null,
                                            "dif": null,
                                            "dif_percent": null,
                                            "working_hours": null,
                                            "product_name_cn": "E"
                                        }
                                    },
                                    "产品3": {
                                        "二级": {
                                            "id": 5,
                                            "product_id": "5",
                                            "ins_class": "二级",
                                            "of_tgt_ins": null,
                                            "of_ins_visited": null,
                                            "coverage": null,
                                            "of_calls": null,
                                            "frequency": null,
                                            "base_num": null,
                                            "base_standard_num": null,
                                            "base_rage": null,
                                            "vacant_days": null,
                                            "dif": null,
                                            "dif_percent": null,
                                            "working_hours": null,
                                            "product_name_cn": "T"
                                        }
                                    },
                                    "产品2": {
                                        "一级": {
                                            "id": 6,
                                            "product_id": "6",
                                            "ins_class": "一级",
                                            "of_tgt_ins": null,
                                            "of_ins_visited": null,
                                            "coverage": null,
                                            "of_calls": null,
                                            "frequency": null,
                                            "base_num": null,
                                            "base_standard_num": null,
                                            "base_rage": null,
                                            "vacant_days": null,
                                            "dif": null,
                                            "dif_percent": null,
                                            "working_hours": null,
                                            "product_name_cn": "Y"
                                        }
                                    }
                                }
 *                          }
 *                      }
 *                  )
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/report/sales",
 *      tags={"报告"},
 *      summary="BI报告",
 *      description="BI报告",
 *      operationId="/report/sales",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  required={"user_token","user_yyid"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *                          @OA\Property(property="month", type="integer", description="月份"),
 *                          @OA\Property(
 *                              property="id",
 *                              type="integer",
 *                              description="Timestamp for previous time user accessed the site."
 *                          ),
 *                          @OA\Property(property="month_target", type="integer", description="月目标"),
 *                          @OA\Property(property="month_sales", type="string", description="月销量"),
 *                          @OA\Property(property="month_ach", type="integer", description="月达成率"),
 *                          @OA\Property(property="ly_sales", type="integer", description="去年4-12月均销量"),
 *                          @OA\Property(property="month_gr", type="integer", description="月增长"),
 *                          @OA\Property(property="qtd_target", type="integer", description="季度目标"),
 *                          @OA\Property(property="qtd_sales", type="string", description="季度销量"),
 *                          @OA\Property(property="qtd_ach", type="integer", description="季度达成率"),
 *                          @OA\Property(property="qtd_ly_sales", type="integer", description="去年4-12月季度销量"),
 *                          @OA\Property(property="qtd_gr", type="integer", description="季度增长"),
 *                          @OA\Property(property="ytd_target", type="integer", description="年目标"),
 *                          @OA\Property(property="ytd_sales", type="string", description="年销量"),
 *                          @OA\Property(property="ytd_ach", type="integer", description="年达成率"),
 *                          @OA\Property(property="ytd_ly_sales", type="integer", description="去年4-12月年销量"),
 *                          @OA\Property(property="ytd_gr", type="integer", description="年增长")
 *                      ),
 *                      example={
 *                          "errcode":0,
 *                          "errmsg":"Success",
 *                          "data":{
 *                             "saleNum": {
"month": "4",
"report": {
"产品1": {
"sku1": {
"id": 3,
"month": "4",
"staff_code": "1",
"product_id": "1",
"sku_id": "11",
"month_target": null,
"month_sales": null,
"month_ach": null,
"ly_sales": null,
"month_gr": null,
"qtd_target": null,
"qtd_sales": null,
"qtd_ach": null,
"qtd_ly_sales": null,
"qtd_gr": null,
"ytd_target": null,
"ytd_sales": null,
"ytd_ach": null,
"ytd_ly_sales": null,
"ytd_gr": null,
"product_name_cn": "Q",
"sku_name_cn": "Q11"
}
}
}
},
"saleValue": {
"month": "4",
"report": {
"产品1": {
"id": 3,
"month": "4",
"staff_code": "1",
"product_id": "1",
"month_target": null,
"month_sales": null,
"month_ach": null,
"ly_sales": null,
"month_gr": null,
"qtd_target": null,
"qtd_sales": null,
"qtd_ach": null,
"qtd_ly_sales": null,
"qtd_gr": null,
"ytd_target": null,
"ytd_sales": null,
"ytd_ach": null,
"ytd_ly_sales": null,
"ytd_gr": null,
"product_name_cn": "Q"
},
"产品2": {
"id": 6,
"month": "4",
"staff_code": "1",
"product_id": "2",
"month_target": null,
"month_sales": null,
"month_ach": null,
"ly_sales": null,
"month_gr": null,
"qtd_target": null,
"qtd_sales": null,
"qtd_ach": null,
"qtd_ly_sales": null,
"qtd_gr": null,
"ytd_target": null,
"ytd_sales": null,
"ytd_ach": null,
"ytd_ly_sales": null,
"ytd_gr": null,
"product_name_cn": "E"
}
}
}
 *                          }
 *                      }
 *                  )
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/report/sendSalesMail",
 *      tags={"报告"},
 *      summary="导出报告发送到指定邮箱",
 *      description="导出报告发送到指定邮箱",
 *      operationId="/report/sendSalesMail",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="email", type="string", description="用户email"),
 *                  required={"user_token","user_yyid","email"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *
 *                          ),
 *                      ),
 *                      example={
 *                          "errcode":0,
 *                          "errmsg":"Success",
 *                          "data":{
 *                              "id": "邮件发送成功",
 *                          }
 *                      }
 *                  )
 *          )
 *      ),
 *      @OA\Response(response="4002",ref="#/components/responses/params_error")
 * )
 */

/**
 *
 * @OA\Post(
 *      path="/report/sendCheckoutMail",
 *      tags={"报告"},
 *      summary="导出拜访明细报告发送到指定邮箱",
 *      description="导出拜访明细报告发送到指定邮箱",
 *      operationId="/report/sendCheckoutMail",
 *      @OA\RequestBody(
 *          required=true,
 *          description="请求参数",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  type="object",
 *                  @OA\Property(property="user_token", type="string", description="用户token"),
 *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *                  @OA\Property(property="email", type="string", description="用户email"),
 *                  required={"user_token","user_yyid","email"}
 *              )
 *          )
 *      ),
 *      @OA\Response(
 *           response="200",
 *          description="请求成功返回",
 *          @OA\MediaType(
 *                  mediaType="application/json",
 *                  @OA\Schema(
 *                      type="object",
 *                      @OA\Property(property="errcode", type="string", description="错误码"),
 *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
 *                      @OA\Property(
 *                          property="data",
 *                          type="object",
 *
 *                          ),
 *                      ),
 *                      example={
 *                          "errcode":0,
 *                          "errmsg":"Success",
 *                          "data":{
 *                              "id": "邮件发送成功",
 *                          }
 *                      }
 *                  )
 *          )
 *      )
 * )
 */

declare(strict_types=1);

namespace App\Controller\Report;

use App\Controller\Controller;
use App\Request\Report\ReportRequest;
use App\Request\Report\SendMailRequest;
use App\Service\Report\ReportService;
use Hyperf\Di\Annotation\Inject;

class ReportController extends Controller
{
    /**
     * @Inject()
     * @var ReportService
     */
    private $reportService;

    /**
     * @param ReportRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function visit(ReportRequest $request)
    {
        $yyid = $request->input('user_yyid');

        $data = $this->reportService->getVisitReport($yyid);

        return $this->response->success($data);
    }

    /**
     * @param ReportRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function sales(ReportRequest $request)
    {
        $yyid = $request->input('user_yyid');

        $data = $this->reportService->getSalesReport($yyid);

        return $this->response->success($data);
    }

    /**
     * @param SendMailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function sendSalesMail(SendMailRequest $request)
    {
        $yyid = $request->input('user_yyid');
        $email = $request->input('email');
        $res = $this->reportService->sendMail($yyid, $email);

        return $this->response->success($res);
    }

    /**
     * @param SendMailRequest $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function sendCheckoutMail(SendMailRequest $request)
    {
        $yyid = $request->input('user_yyid');
        $email = $request->input('email');
        $res = $this->reportService->sendCheckoutMail($yyid, $email);

        return $this->response->success($res);
    }
}
