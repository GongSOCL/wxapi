<?php
declare(strict_types=1);
namespace App\Controller\Wechat;

use App\Constants\DataStatus;
use App\Controller\Controller;
use App\Helper\Helper;
use App\Request\Doctor\QrcodeRequest;
use App\Request\Wechat\GetAccessTokenRequest;
use App\Request\Wechat\GetWechatUserInfoRequest;
use App\Request\Wechat\OauthRedirectUrlRequest;
use App\Request\Wechat\OauthUserInfoRequest;
use App\Request\Wechat\WechatJsSignatureGetRequest;
use App\Request\Wechat\WechatQrcodeRequest;
use App\Service\Wechat\WechatBusiness;
use App\Service\Wechat\WechatService;
use EasyWeChat\Kernel\Exceptions\InvalidConfigException;
use GuzzleHttp\Exception\GuzzleException;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Coroutine;
use Psr\Http\Message\ResponseInterface;
use Hyperf\Di\Annotation\Inject;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\InvalidArgumentException;
use function foo\func;

class WechatController extends Controller
{
    /**
     * @Inject
     * @var WechatBusiness
     */
    private $business;

    /**
     *
     * @OA\Post(
     *      path="/wechat/serve/{type}",
     *      tags={"微信公众号"},
     *      summary="公众号回调入口",
     *      description="获取网页授权跳转地址",
     *      operationId="wechatServe",
     *      @OA\RequestBody(
     *          required=false,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *             mediaType="application/plain"
     *          )
     *      )
     * )
     * @param $type
     * @return false|ResponseInterface|string
     */
    public function serve($type)
    {
        Helper::getLogger()->info('wechat.serve.start', ['request' => $this->request->all()]);
        try {
            return $this->business->serve($type)->getContent();
        } catch (\Exception $e) {
            Helper::getLogger()->error('wechat.serve.exception', [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                    'wechat_type' => $type
                ]);
            return $this->response->raw("success");
        }
    }

    /**
     *
     * @OA\Post(
     *      path="/wechat/get-oauth-redirect-url",
     *      tags={"微信公众号"},
     *      summary="获取网页授权跳转地址",
     *      description="获取网页授权跳转地址",
     *      operationId="wechatGetOauthRedirectUrl",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="type", ref="#/components/schemas/wechat_type"),
     *                  @OA\Property(
     *                      property="is_silent",
     *                      type="int",
     *                      description="是否静默授权: 1是(snsapi_base) 0 否(snsapi_userinfo)"
     *                     ),
     *                  @OA\Property(property="state", type="string", description="微信oauth state参数"),
     *                  @OA\Property(property="redirect", type="string", description="完成授权后的跳转地址"),
     *                  required={"type", "redirect"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="url", type="string", description="微信跳转地址，客户端直接使用该地址进行跳转")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param OauthRedirectUrlRequest $request
     * @return ResponseInterface
     */
    public function getOauthRedirectUrl(OauthRedirectUrlRequest $request): ResponseInterface
    {
        $url = $this->business->getOauthRedirectUrl(
            $request->post('redirect'),
            $request->post('is_silent', 1),
            $request->post('state'),
            $request->post('type')
        );
        return $this->response->success([
            'url' => $url
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/wechat/get-oauth-user-info",
     *      tags={"微信公众号"},
     *      summary="获取网页授权用户信息",
     *      description="获取网页授权用户信息",
     *      operationId="wechatGetOauthUserInfo",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="type",  ref="#/components/schemas/wechat_type"),
     *                  @OA\Property(property="state", type="string", description="完成授权后url中带的state参数"),
     *                  @OA\Property(property="code", type="string", description="完成授权后url中带的code参数"),
     *                  required={"type", "state", "code"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="openid", type="string", description="当前登陆用户的openid参数")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param OauthUserInfoRequest $request
     * @return ResponseInterface
     */
    public function getOauthUserInfo(OauthUserInfoRequest $request): ResponseInterface
    {
        $type = $request->post('type');
        $userInfo = $this->business->getOauthWechatInfo($type);
        $raw = $userInfo->getRaw();
        if ($raw) { //添加api表
            Coroutine::create(function () use ($raw, $type) {
                WechatService::processOauthRawMsg($raw, $type);
            });

            Helper::getLogger()->debug("get_oauth_raw_info", [
                'type' => $request->post('type'),
                'raw' => $raw
            ]);
        }
        return $this->response->success([
            'openid' => $userInfo->getId(),
        ]);
    }


    /**
     *
     * @OA\Post(
     *      path="/wechat/signpage/get",
     *      tags={"微信公众号"},
     *      summary="获取jsapi config参数",
     *      description="获取jsapi config参数",
     *      operationId="wechatGetWechatJsSignature",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="type",  ref="#/components/schemas/wechat_type"),
     *                  @OA\Property(property="url", type="string", description="当前页面url"),
     *                  @OA\Property(property="apis", type="array", description="api列表", @OA\Items(type="string")),
     *                  required={"type", "url"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param WechatJsSignatureGetRequest $request
     * @return ResponseInterface
     * @throws InvalidArgumentException|GuzzleException
     */
    public function getWechatJsSignature(WechatJsSignatureGetRequest $request): ResponseInterface
    {
        $debug = Helper::isOnline() ? false : true;
        $apis = Helper::getArrayRequestParams($request, 'apis');
        Helper::getLogger()->info("business_object", ['is_null' => $this->business]);
        $config = $this->business->getWechatJsSignature(
            $request->post('url'),
            $apis,
            $debug,
            $request->post('type', DataStatus::WECHAT_AGENT)
        );
        return $this->response->success($config);
    }

    /**
     *
     * @OA\Post(
     *      path="/wechat/info",
     *      tags={"微信公众号"},
     *      summary="根据openid获取微信用户信息",
     *      description="根据openid获取微信信息",
     *      operationId="wechatGetWechatUserInfo",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  allOf={
     *                      @OA\Schema(ref="#/components/schemas/loginAuthenticate"),
     *                      @OA\Schema(
     *                          type="object",
     *                          @OA\Property(property="wechat_type", ref="#/components/schemas/wechat_type"),
     *                          @OA\Property(property="openid", type="integer", description="用户openid"),
     *                          required={"wechat_type", "openid"}
     *                      )
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="user", type="object", description="微信用户信息")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetWechatUserInfoRequest $request
     * @return ResponseInterface
     * @throws InvalidConfigException
     */
    public function getWechatUserInfo(GetWechatUserInfoRequest $request): ResponseInterface
    {
        $openId = $request->post('openid');
        $info = $this->business->getWechatInfo($openId, $request->post('wechat_type'));

        return $this->response->success([
            'user' => $info
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/wechat/get-temporary-qrcode",
     *      tags={"微信公众号"},
     *      summary="生成微信临时二维码",
     *      description="生成微信临时二维码",
     *      operationId="wechatGetTemporaryQrcode",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  allOf={
     *                      @OA\Schema(ref="#/components/schemas/loginAuthenticate"),
     *                      @OA\Schema(
     *                          type="object",
     *                          @OA\Property(property="wechat_type", ref="#/components/schemas/wechat_type"),
     *                          @OA\Property(property="sense_str", type="integer", description="二维码字符串"),
     *                          required={"sense_str", "wechat_type"}
     *                      )
     *                  }
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="url", type="string", description="生成的临时二维码url")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param QrcodeRequest $request
     * @return ResponseInterface
     */
    public function getTemporaryQrcode(QrcodeRequest $request): ResponseInterface
    {
        $wechatType = $request->post('wechat_type', DataStatus::WECHAT_DOCTOR);
        $senseStr = $request->post('sense_str');
        $url = $this->business->getQrcode($senseStr, $wechatType);
        return $this->response->success([
            'url' => $url
        ]);
    }

    /**
     *
     * @OA\Post(
     *      path="/wechat/get-access-token",
     *      tags={"微信公众号"},
     *      summary="获取服务器access_token",
     *      description="获取服务器access_token",
     *      operationId="wechatGetServerAccessToken",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(ref="#/components/schemas/loginAuthenticate"),
     *              @OA\Schema(
     *                 type="object",
     *                 @OA\Property(property="wechat_type", ref="#/components/schemas/wechat_type"),
     *                 required={"wechat_type"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(property="token", type="string", description="请求微信号的access_token参数")
     *                      )
     *                  )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param GetAccessTokenRequest $request
     * @return ResponseInterface
     */
    public function getServerAccessToken(GetAccessTokenRequest $request): ResponseInterface
    {
        $token = $this->business->getAccessToken($request->post('wechat_type'));
        return $this->response->success([
            'token' => $token
        ]);
    }
}
