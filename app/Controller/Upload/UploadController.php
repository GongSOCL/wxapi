<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Controller\Upload;

use App\Controller\Controller;
use App\Request\Upload\RecordUploadRequests;
use App\Request\Upload\UploadTokenRequests;
use App\Service\Upload\UploadService;
use Grpc\Upload\UPLOAD_SCOPE;
use Psr\Http\Message\ResponseInterface;

class UploadController extends Controller
{

    /**
     * @param string $url
     * @param object $file
     * @param $id
     * @return bool|string
     */
    public function upload(string $url, object $file, $id)
    {
        $extension = $file->getExtension();
        $dir = $url . $id . '/';
        $fileName = $id . '_' . time() . rand(10000000, 99999999) . '.' . $extension;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $path = $dir.$fileName;
        $file->moveTo($path);
        if (!$file->isMoved()) {
            return false;
        }
        return $fileName;
    }

    /**
     *
     * @OA\Post(
     *      path="/upload/token",
     *      tags={"上传"},
     *      summary="获取七牛上传token",
     *      description="上传",
     *      operationId="UploadToken",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="type", type="string", description="bucket类型 1: static bucket 2 user
     * bucket"),
     *                  @OA\Property(property="business", type="string",description="业务类型描述，单个英文词，如问卷的questionnaire"),
     *                  required={"user_yyid", "user_token"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                           @OA\Property(
     *                             property="config",
     *                             type="object",
     *                             description="上传配置",
     *                              @OA\Property(property="path", type="string", description="上传路径前缀,需要拼接文件名作为上传key"),
     *                              @OA\Property(property="token", type="token", description="七牛上传token"),
     *                              @OA\Property(
     *                                  property="expire_time",
     *                                  type="string",
     *                                   description="token过期时间,默认一时间过期"),
     *                              @OA\Property(property="bucket", type="string", description="上传bucket名"),
     *                              @OA\Property(
     *                                  property="base_uri",
     *                                   type="string",
     *                                   description="七牛cdn地址,拼接上传返回的key为文件的访问url"),
     *                              @OA\Property(property="upload_uri", type="string", description="七牛上传文件地址,sdk上传地址")
     *                          ),
     *
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param UploadTokenRequests $requests
     * @return ResponseInterface
     */
    public function token(UploadTokenRequests $requests): ResponseInterface
    {
        $config = UploadService::getConfig(
            $requests->post('type'),
            UPLOAD_SCOPE::SCOPE_AGENT,
            $requests->post('business')
        );
        //获取七牛上传token
        return $this->response->success([
            'config' => $config
        ]);
    }

    /**
     *
     * @OA\Put(
     *      path="/upload",
     *      tags={"上传"},
     *      summary="记录上传七牛文件",
     *      description="上传",
     *      operationId="UploadAdd",
     *      @OA\RequestBody(
     *          required=true,
     *          description="请求参数",
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="user_yyid", type="string", description="用户yyid"),
     *                  @OA\Property(property="user_token", type="string", description="用户token"),
     *                  @OA\Property(property="bucket", type="string", description="bucket名称,config返回的bucket字段"),
     *                  @OA\Property(property="key", type="string",description="上传七牛后返回key"),
     *                  @OA\Property(property="name", type="string", description="上传文件名称"),
     *                  @OA\Property(property="url", type="string", description="上传后文件的访问全路径url"),
     *                  required={"user_yyid", "user_token", "bucket", "key", "url", "name"}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *           response="200",
     *          description="请求成功返回",
     *          @OA\MediaType(
     *                  mediaType="application/json",
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="errcode", type="string", description="错误码"),
     *                      @OA\Property(property="errmsg", type="string", description="错误信息"),
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                           @OA\Property(
     *                              property="info",
     *                              type="object",
     *                              description="文件信息",
     *                               @OA\Property(property="id", type="integer", description="文件id"),
     *                               @OA\Property(property="name", type="string", description="文件名"),
     *                               @OA\Property(property="url", type="string", description="文件url"),
     *                          )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response="4002",ref="#/components/responses/params_error")
     * )
     * @param RecordUploadRequests $requests
     * @return ResponseInterface
     */
    public function record(RecordUploadRequests $requests): ResponseInterface
    {
        $res = UploadService::addUpload(
            $requests->post('bucket'),
            $requests->post('key'),
            $requests->post('name'),
            $requests->post('url')
        );
        return $this->response->success([
            'info' => $res
        ]);
    }
}
