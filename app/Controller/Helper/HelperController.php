<?php
declare(strict_types=1);
namespace App\Controller\Helper;

use App\Controller\Controller;
use App\Request\Helper\SyncSingleHospitalRequest;
use App\Service\Common\HospitalService;
use Psr\Http\Message\ResponseInterface;

class HelperController extends Controller
{
    public function syncSingleHospital(SyncSingleHospitalRequest $request): ResponseInterface
    {
        $hospitalId = (int) $request->query('hospital_id');
        HospitalService::syncSingleHospitalEs($hospitalId);
        return $this->response->success([]);
    }

    public function syncAllHospital(): ResponseInterface
    {
        HospitalService::syncAllHospitalEs();
        return $this->response->success([]);
    }
}