<?php
declare(strict_types=1);
namespace App\Exception;

use App\Constants\ErrorCode;
use App\Helper\Helper;
use Throwable;

/**
 * 定义微信调用的异常，可以记录日志，线上环境返回统一的错误码
 * Class WechatException
 * @package App\Exception
 */
class WechatException extends BusinessException
{
    public $api;
    public $data;
    public $errCode;
    public $errMessage;

    public static function newFromCode($code, $message, $api, $data = []): WechatException
    {

        if (Helper::isOnline()) {
            $o = new self(ErrorCode::WECHAT_BUSINESS_EXCEPTIN);
        } else {
            $o = new self($code, $message);
        }
        $o->api = $api;
        $o->data = $data;
        $o->errCode= $code;
        $o->errMessage = $message;

        return $o;
    }
}
