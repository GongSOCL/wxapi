<?php

declare(strict_types=1);

namespace App\Exception\Handler;

use App\Constants\ErrorCode;
use App\Helper\Helper;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Utils\Codec\Json;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Hyperf\Validation\ValidationException;

class ValidationExceptionHandler extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        if ($throwable instanceof ValidationException) {
            Helper::getLogger()->error("validation_exception", [
                'trace' => $throwable->getTrace(),
                'keys' => $throwable->validator->errors()->keys(),
                'errors' => $throwable->validator->errors()->getMessages()
            ]);
        }
        $this->stopPropagation();
        /** @var \Hyperf\Validation\ValidationException $throwable */
        $msg = $throwable->validator->errors()->first();

        if (!$response->hasHeader('content-type')) {
            $response = $response->withAddedHeader('content-type', 'application/json; charset=utf-8');
        }
        $body = Json::encode([
            'errcode' => ErrorCode::ERROR_CODE_PARAMETERS_ERROR,
            'errmsg' => $msg,
            'data' => []
        ]);
        return $response->withHeader('Server', 'Youyao')
            ->withStatus(200)   //目前要想让前端正确的打印出错误消息，需要暂时返回200
            ->withBody(new SwooleStream($body));
    }

    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof ValidationException;
    }
}
