<?php

declare(strict_types=1);

namespace App\Exception\Handler;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Helper\Helper;
use Elasticsearch\Endpoints\Cat\Help;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class AppExceptionHandler extends ExceptionHandler
{
    /**
     * @var StdoutLoggerInterface
     */
    protected $logger;

    public function __construct(StdoutLoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->logger->error(
            sprintf(
                '%s[%s] in %s',
                $throwable->getMessage(),
                $throwable->getLine(),
                $throwable->getFile()
            )
        );
        $this->logger->error($throwable->getTraceAsString());
        $response = $response->withHeader('Server', 'Youyao')
            ->withStatus(200)   //前端好像会拦截其它code，报错会出问题
            ->withHeader('Content-Type', 'application/json');
        $errCode = $throwable->getCode();
        $errCode = $errCode ?  $errCode : ErrorCode::SERVER_ERROR;
        if (Helper::isOnline()) {
            $data = json_encode([
                'errcode' => $errCode,
                'errmsg' => ErrorCode::getMessage(ErrorCode::SERVER_ERROR),
                'data' => []
            ], JSON_UNESCAPED_UNICODE);
        } else {
            $data = json_encode([
                'errcode' => $errCode,
                'errmsg' => $throwable->getMessage(),
                'data' => []
            ], JSON_UNESCAPED_UNICODE);
        }
        $this->stopPropagation();
        return $response->withBody(new SwooleStream($data));
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }
}
