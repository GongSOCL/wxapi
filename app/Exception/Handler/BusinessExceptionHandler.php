<?php

declare(strict_types=1);

namespace App\Exception\Handler;

use App\Exception\BusinessException;
use App\Helper\Helper;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Utils\Codec\Json;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class BusinessExceptionHandler extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        Helper::getLogger()->error("business_exception_handler", [
            'message' => $throwable->getMessage(),
            'trace' => $throwable->getTrace(),
            'code' => $throwable->getCode()
        ]);

        $this->stopPropagation();
        /** @var \Hyperf\Validation\ValidationException $throwable */

        if (!$response->hasHeader('content-type')) {
            $response = $response->withAddedHeader('content-type', 'application/json; charset=utf-8');
        }
        $body = Json::encode([
            'errcode' => $throwable->getCode(),
            'errmsg' => $throwable->getMessage(),
            'data' => []
        ]);
        return $response->withHeader('Server', 'Youyao')
            ->withStatus(200)
            ->withBody(new SwooleStream($body));
    }

    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof BusinessException;
    }
}
