<?php
declare(strict_types=1);
namespace App\Exception\Handler;

use App\Exception\WechatException;
use App\Helper\Helper;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Psr\Http\Message\ResponseInterface;
use Hyperf\Di\Annotation\Inject;
use Psr\Log\LoggerInterface;

class WechatExceptionHandler extends ExceptionHandler
{
    /**
     * @Inject
     * @var LoggerInterface
     */
    private $logger;

    public function handle(\Throwable $throwable, ResponseInterface $response)
    {
        if ($throwable instanceof WechatException) {
            Helper::getLogger()->error("微信接口调用异常", [
                'api' => $throwable->api,
                'data' => $throwable->data,
                'errcode' => $throwable->errCode,
                'errmsg' => $throwable->errMessage
            ]);
        }
    }

    public function isValid(\Throwable $throwable): bool
    {
        return $throwable instanceof WechatException;
    }
}
