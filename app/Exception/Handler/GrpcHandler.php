<?php
declare(strict_types=1);
namespace App\Exception\Handler;

use Hyperf\Grpc\StatusCode;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\ExceptionHandler\Formatter\FormatterInterface;
use Hyperf\GrpcServer\Exception\GrpcException;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Youyao\Framework\ErrCode;

class GrpcHandler extends ExceptionHandler
{
    /**
     * @var StdoutLoggerInterface
     */
    protected $logger;

    /**
     * @var FormatterInterface
     */
    protected $formatter;

    public function __construct(StdoutLoggerInterface $logger, FormatterInterface $formatter)
    {
        $this->logger = $logger;
        $this->formatter = $formatter;
    }

    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();
        if ($throwable instanceof GrpcException) {
            $this->logger->debug($this->formatter->format($throwable));
        } else {
            $this->logger->warning($this->formatter->format($throwable));
        }

        return $this->transferToResponse(
            $throwable->getCode() ?? ErrCode::GrpcServerErr,
            $throwable->getMessage(),
            $response
        );
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }

    /**
     * Transfer the non-standard response content to a standard response object.
     */
    protected function transferToResponse($code, string $message, ResponseInterface $response): ResponseInterface
    {
        $code = intval($code);
        if ($code == 0) {
            $code = StatusCode::INTERNAL;
        }
        return $response->withAddedHeader('Content-Type', 'application/grpc')
            ->withAddedHeader('trailer', 'grpc-status, grpc-message')
            ->withStatus(StatusCode::HTTP_CODE_MAPPING[$code] ?? 500)->withTrailer('grpc-status', (string) $code)
            ->withTrailer('grpc-message', (string) $message);
    }
}
