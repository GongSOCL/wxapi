<?php

declare(strict_types=1);

namespace App\Exception;

use App\Constants\Code\ErrorCodeDoctor;
use Throwable;

class DoctorException extends BusinessException
{
    public function __construct(int $code = 0, string $message = null, Throwable $previous = null)
    {
        if (is_null($message)) {
            $message = ErrorCodeDoctor::getMessage($code);
        }

        parent::__construct($code, $message, $previous);
    }
}
