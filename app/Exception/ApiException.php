<?php
declare(strict_types=1);
namespace App\Exception;

use App\Constants\ErrorCode;

/**
 * 内部项目之间调用异常(code和message均需要往外透传)
 * Class ApiException
 * @package App\Exception
 */
class ApiException extends BusinessException
{
    public function __construct(string $message, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($code, $message, $previous);
    }
}
