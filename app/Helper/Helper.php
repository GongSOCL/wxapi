<?php
declare(strict_types=1);
namespace App\Helper;

use App\Constants\Auth;
use App\Constants\DataStatus;
use App\Constants\Device;
use App\Kernel\Uuid\Uuid;
use Google\Protobuf\Internal\RepeatedField;
use Grpc\Pharmacy\Common\ShareLinkToken;
use Grpc\Questionnaire\QuestionnaireInviteShare;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\Qa\Users;
use Hashids\Hashids;
use Hyperf\AsyncQueue\Driver\DriverFactory;
use Hyperf\AsyncQueue\Driver\DriverInterface;
use Hyperf\Database\Query\Expression;
use Hyperf\DbConnection\Db;
use Hyperf\Guzzle\ClientFactory;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Task\Task;
use Hyperf\Task\TaskExecutor;
use Hyperf\Redis\Redis;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use const http\Client\Curl\AUTH_ANY;

class Helper
{
    public static function isOnline(): bool
    {
        return env('APP_ENV') == DataStatus::APP_ENV_PROD;
    }

    public static function getLogger($name = 'default'): \Psr\Log\LoggerInterface
    {
        return ApplicationContext::getContainer()->get(LoggerFactory::class)->get($name);
    }

    public static function getRedisCache(): \Hyperf\Redis\Redis
    {
        return ApplicationContext::getContainer()->get(\Hyperf\Redis\Redis::class);
    }

    public static function getHttpClient($uri, $timeout = 5): \GuzzleHttp\Client
    {
        return ApplicationContext::getContainer()->get(ClientFactory::class)
            ->create([
                'base_uri' => $uri,
                'timeout' => $timeout
            ]);
    }

    public static function getRequestId($default = '')
    {
        $requestId = Context::get('x-request-id');
        if (!$requestId) {
            $requestIds = ApplicationContext::getContainer()->get(RequestInterface::class)->getHeader('x-request-id');
            if (!empty($requestIds) && isset($requestIds[0])) {
                $requestId = $requestIds[0];
                Context::set('x-request-id', $requestId);
            }
        }

        return $requestId ?? $default;
    }

    /**
     * 根据id生成hashid
     * @param $id
     * @param string $salt
     * @param string $minLength
     * @return string
     */
    public static function generateHashId($id, $salt = "", $minLength = ""): string
    {
        if (!$salt) {
            $salt = env('HASH_ID_SALT');
        }
        if (!$minLength) {
            $minLength = env('HASH_ID_MIN_LENGTH');
        }
        $hash = new Hashids($salt, $minLength);
        return $hash->encode($id);
    }

    /**
     * 根据hashId解码原始值
     * @param string $hashId
     * @param string $salt
     * @param string $minLength
     * @return mixed|string
     */
    public static function decodeHashId(string $hashId, $salt = "", $minLength = "")
    {
        if (!$salt) {
            $salt = env('HASH_ID_SALT');
        }
        if (!$minLength) {
            $minLength = env('HASH_ID_MIN_LENGTH');
        }
        $hash = new Hashids($salt, $minLength);
        $hashDecode = $hash->decode($hashId);
        return isset($hashDecode[0]) && $hashDecode[0] ? $hashDecode[0] : '';
    }

    public static function createPaperShareKey(
        $paperId,
        $invitorId = 0,
        $invitorType = 0,
        $inviteeId = 0,
        $inviteeType = 0,
        $inviteId = 0
    ): string {
        $share = new QuestionnaireInviteShare();
        $share->setPaperId($paperId);
        $share->setInvitorId($invitorId);
        $share->setInvitorType($invitorType);
        $share->setInviteeId($inviteeId);
        $share->setInviteeType($inviteeType);
        $share->setInviteId($inviteId);

        return self::encryptShareKey($share);
    }

    private static function encryptShareKey(QuestionnaireInviteShare $share): string
    {
        $str = $share->serializeToString();
        [$method, $passphrase, $iv] = self::getPaperEncryptElem();
        if (strlen($str) % 16) {
            $data = str_pad($str, strlen($str) + 16 - strlen($str) % 16, "\0");
        }

        $res = base64_encode(openssl_encrypt($share->serializeToString(), $method, $passphrase, 0, $iv));
        return strtr($res, '+/', '-_');
    }

    private static function getPaperEncryptElem(): array
    {
        $method = env("PAPER_ENC_METHOD", 'aes-256-cbc');
        $iv = "LtRdHdS:x7Kyg?^n";
        $passphrase = env('PAPER_ENC_PASSPHRASE', '^/!%~I#:2v!5\k&@');
        return [$method, $passphrase, $iv];
    }

    /**
     * 问卷分享key解码
     * @param $key
     * @return QuestionnaireInviteShare
     * @throws \Exception
     */
    public static function decodeShareKey($key): QuestionnaireInviteShare
    {
        [$method, $passphrase, $iv] = self::getPaperEncryptElem();
        $key = strtr($key, '-_', '+/');
        $raw = openssl_decrypt(base64_decode($key), $method, $passphrase, 0, $iv);

        $share = new QuestionnaireInviteShare();
        $share->mergeFromString($raw);

        return $share;
    }

    public static function getQueue($name = 'default'): DriverInterface
    {
        return ApplicationContext::getContainer()->get(DriverFactory::class)->get($name);
    }

    public static function executeTask(callable $call, array $params = [])
    {
        return ApplicationContext::getContainer()
            ->get(TaskExecutor::class)
            ->execute(new Task($call, $params));
    }

    public static function diff(array $old, array $new): array
    {
        $delete = array_diff($old, $new);
        $add = array_diff($new, $old);
        return [$delete, $add];
    }

    /**
     * 获取登陆用户
     *
     * @return Users
     */
    public static function getLoginUser(): Users
    {
        $user = context::get('user');
        if (!($user && $user instanceof Users)) {
            throw new BusinessException(ErrorCode::ERROR_CODE_AUTHENTICATION_FAILED);
        }

        return $user;
    }

    public static function tryGetLoginUser(): ?Users
    {
        $user = context::get('user');
        return $user && $user instanceof Users ? $user : null;
    }

    /**
     * 根据平台保存角色
     * @param int $device
     */
    public static function setAuthRoleFromDevice($device = Device::PLATFORM_AGENT_WECHAT)
    {
        switch ($device) {
            case Device::PLATFORM_ANDROID:
            case Device::PLATFORM_IOS:
                $platform = Auth::AUTH_ROLE_YOU_YAO_APP;
                break;
            case Device::PLATFORM_MINI_PROGRAM:
                $platform = Auth::AUTH_ROLE_YOU_YAO_MINI_PROGRAM;
                break;
            default:
                $platform = Auth::AUTH_ROLE_YOU_YAO_AGENT;
                break;
        }
        Context::set("auth_role", $platform);
    }

    /**
     * 获取会话平台角色
     * @return int|mixed|null
     */
    public static function getAuthRole()
    {
        $platform = Context::get('auth_role');
        if (!$platform) {
            $platform = Auth::AUTH_ROLE_YOU_YAO_AGENT;
        }
        return $platform;
    }

    /**
     * 根据角色获取微信类型
     * @param int $authRole
     * @return int
     */
    public static function getWechatTypeByRole($authRole = Auth::AUTH_ROLE_YOU_YAO_AGENT): int
    {
        switch ($authRole) {
            case Auth::AUTH_ROLE_YOU_YAO_APP:
                return DataStatus::WECHAT_APP;
            case Auth::AUTH_ROLE_YOU_YAO_MINI_PROGRAM:
                return DataStatus::WECHAT_YOU_YAO_MINI_PROGRAM;
            default:
                return DataStatus::WECHAT_AGENT;
        }
    }

    public static function formatInputDate($dateStr, $format = 'Y-m-d H:i:s'): string
    {
        return (new \DateTime($dateStr))->format($format);
    }

    public static function exchangeDate($d1, $d2): array
    {
        if ($d2 < $d1) {
            return [$d2, $d1];
        } else {
            return [$d1, $d2];
        }
    }

    public static function getRedis($name = 'default'): Redis
    {
        return ApplicationContext::getContainer()->get(RedisFactory::class)->get($name);
    }

    public static function getArrayRequestParams($request, $paramsName): array
    {
        $data = [];
        $item = $request->post($paramsName);
        if (!$item) {
            $item = [];
        }
        if (is_array($item)) {
            return $item;
        }
        if (is_string($item)) {
            $data = json_decode($item, true);
        }
        return $data;
    }

    /**
     * 提取app请求头
     * @return array
     */
    public static function extractAppHeaders(): array
    {
        $req = ApplicationContext::getContainer()
            ->get(RequestInterface::class);

        $appVersion = $req->header('app-version', '');
        $appPlatform = $req->header('app-platform', '');
        $deviceId = $req->header('device-id', '');
        return [$appVersion, $appPlatform, $deviceId];
    }

    public static function getAuthToken(): string
    {
        $container = ApplicationContext::getContainer();
        $req = $container->get(RequestInterface::class);
        $token = (string)$req->header('Authorization');
        return $token ? substr($token, 7) : '';
    }

    public static function yyidGenerator(): Expression
    {
        return Db::raw("UPPER(REPLACE(UUID(), '-', ''))");
    }

    /**
     * 生成n位随机数
     * @param $length
     * @return int
     * @throws \Exception
     */
    public static function generateNInteger($length): int
    {
        return random_int(10 **($length - 1), (10 ** $length) -1);
    }

    public static function generateYYID()
    {
        return ApplicationContext::getContainer()
            ->get(Uuid::class)
            ->generate();
    }

    public static function buildYouyaoUrl($path, $params = []): string
    {
        $baseUrl = "";
        switch (env('APP_ENV')) {
            case DataStatus::APP_ENV_STAGING:
                $baseUrl = "http://wx.qa2.youyao99.com";
                break;
            case DataStatus::APP_ENV_PROD:
                $baseUrl = "http://wx.youyao99.com";
                break;
            default:
                $baseUrl = "http://wx.qa.youyao99.com";
                break;
        }

        $url = rtrim($baseUrl, '/') . '/'. ltrim($path, '/');
        if ($params) {
            $url .= '?' . http_build_query($params);
        }
        return $url;
    }

    public static function getIp(RequestInterface $request = null)
    {
        if (!$request) {
            $request = ApplicationContext::getContainer()
                ->get(RequestInterface::class);
        }

        $ip = '';
        $res = $request->getHeaders();
        if (isset($res['x-real-ip'])) {
            $ip =  $res['x-real-ip'][0];
        } elseif (isset($res['x-forwarded-for'])) {
            $ip = $res['x-forwarded-for'][0];
        } else {
            $ip = $res['remote_addr']  ?? '';
        }

        return $ip;
    }

    /*网络图片转为base64编码*/
    public static function imgtobase64($img = '')
    {
        $imageInfo = getimagesize($img);
        return 'data:' . $imageInfo['mime'] . ';base64,' . chunk_split(base64_encode(file_get_contents($img)));
    }

    public static function getUserAgent(RequestInterface $request = null)
    {
        if (!$request) {
            $request = ApplicationContext::getContainer()
                ->get(RequestInterface::class);
        }
        $resp = $request->getHeader('user-agent');
        if (is_array($resp) && !empty($resp)) {
            return $resp[0];
        }
        return (string)$resp;
    }

    /**
     * @param $request
     * @return mixed
     */
    public static function ip($request)
    {
        $res = $request->getHeaders();
        if (isset($res['x-real-ip'])) {
            return $res['x-real-ip'][0];
        } elseif (isset($res['x-forwarded-for'])) {
            return $res['x-forwarded-for'][0];
        } else {
            return $res['remote_addr'];
        }
    }

    /**
     * 生成随机字符串
     * @param $len
     * @param null $chars
     * @return string
     */
    public static function getRandomString($len, $chars = null)
    {
        if (is_null($chars)) {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";
        }
        $str = '';
        for ($i = 0; $i < $len; $i++) {
            $str .= $chars[mt_rand(0, strlen($chars))];
        }
        return $str;
    }

    //二维数组排序
    public static function arraySort($arr, $keys, $type = 'asc')
    {
        $keysvalue = $new_array = array();
        foreach ($arr as $k => $v) {
            $keysvalue[$k] = $v[$keys];
        }
        if ($type == 'asc') {
            asort($keysvalue);
        } else {
            arsort($keysvalue);
        }
        reset($keysvalue);
        foreach ($keysvalue as $k => $v) {
            $new_array[$k] = $arr[$k];
        }
        return $new_array;
    }

    public static function timeFormat($time)
    {
        if ($time<60) {
            return $time . '秒';
        }

        $min = floor($time / 60);

        return $min . '分' . ($time - $min * 60) . '秒';
    }

    /**
     * @param $uri
     * @param $timeout
     * @return resource
     */
    public static function downloadFileToTmp($uri, $timeout = 30)
    {
        $client = ApplicationContext::getContainer()->get(ClientFactory::class)
            ->create([
                'timeout' => $timeout
            ]);
        $resp = $client->get($uri, [
            'stream' => true
        ]);

        if ($resp->getStatusCode() != 200) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "文件下载失败");
        }

        $tmp = tmpfile();
        $body = $resp->getBody();
        while (!$body->eof()) {
            fwrite($tmp, $body->read(4096));
        }
        rewind($tmp);

        return $tmp;
    }

    /**
     * aes加密
     * @param $str
     * @param $password
     * @param $iv
     * @param $method
     * @return string
     */
    public static function aesEnc(
        $str,
        $password,
        $iv = DataStatus::DEFAULT_AES_IV,
        $method = DataStatus::DEFAULT_AES_METHOD
    ): string {
        $res= base64_encode(openssl_encrypt($str, $method, $password, 0, $iv));
        return strtr($res, '+/', '-_');
    }

    /**
     * aes解密
     * @return string|bool
     */
    public static function aesDec(
        $str,
        $password,
        $iv = DataStatus::DEFAULT_AES_IV,
        $method = DataStatus::DEFAULT_AES_METHOD
    ) {
        return openssl_decrypt(base64_decode($str), $method, $password, 0, $iv);
    }

    public static function buildYoujiayiUrl($path, $params = []): string
    {
        $baseUrl = "";
        switch (env('APP_ENV')) {
            case DataStatus::APP_ENV_DEV:
                $baseUrl = "https://doctor.yyimgs.com/uniapp";
                break;
            case DataStatus::APP_ENV_STAGING:
                $baseUrl = "https://doctor.qa2.youyao99.com/uniapp";
                break;
            default:
                $baseUrl = "https://doctor.youyao99.com/uniapp";
                break;
        }

        $url = rtrim($baseUrl, '/') . '/'. ltrim($path, '/');
        if ($params) {
            $url .= '?' . http_build_query($params);
        }
        return $url;
    }

    public static function encyptUserId($userId)
    {
        $pwd = env('USER_ENC_KEY', 'D&m}+BS%:g+ywQY_B1eZ9#<`OkSW=[,Y');
        return self::aesEnc(strval($userId), $pwd);
    }

    public static function decryptUserId($userToken): int
    {
        try {
            $pwd = env('USER_ENC_KEY', 'D&m}+BS%:g+ywQY_B1eZ9#<`OkSW=[,Y');
            $res = self::aesDec($userToken, $pwd);
            if (false == $res) {
                throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法请求");
            }
            return (int) $res;
        } catch (\Exception $e) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法请求");
        }
    }

    public static function mapRepeatedField(RepeatedField $field, callable $call, $initial = [])
    {
        $data = $initial;
        foreach ($field as $item) {
            $res = $call($item);
            $data[] = $res;
        }
        return $data;
    }

    public static function genShareToken(ShareLinkToken $token): string
    {
        $str = $token->serializeToString();
        [$method, $passphrase, $iv] = self::getShareElem();
        if (strlen($str) % 16) {
            $data = str_pad($str, strlen($str) + 16 - strlen($str) % 16, "\0");
        }

        $res = base64_encode(openssl_encrypt($str, $method, $passphrase, 0, $iv));
        return strtr($res, '+/', '-_');
    }

    private static function getShareElem(): array
    {
        $method = env("SHARE_ENC_METHOD", 'aes-256-cbc');
        $iv = "jr2v7*T|}/D[86y=";
        $passphrase = env('SHARE_ENC_PASSPHRASE', '*v-ic}}\+1Kl0|_O');
        return [$method, $passphrase, $iv];
    }

    public static function getPharmacyMiniAppInfo(): array
    {
        $pharmacyUserName = env('PHARMACY_MINIAPP_USERNAME');
        if (!$pharmacyUserName) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '小程序未配置');
        }
        $envVersion = env('PHARMACY_MINIAPP_ENV_VERSION');
        if ($envVersion) {
            return [$pharmacyUserName, $envVersion];
        }
        switch (env('APP_ENV')) {
            case 'prod':
                return [$pharmacyUserName, 'release'];
            case 'staging':
                return [$pharmacyUserName, 'trial'];
            default:
                return [$pharmacyUserName, 'develop'];
        }
    }

    public static function n_img_base_64($img) {
        $imageInfo = getimagesize($img);
        return 'data:' . $imageInfo['mime'] . ';base64,' . chunk_split(base64_encode(file_get_contents($img)));
    }

    public static function getDistanceGeoPoint($lng, $lat, $deg = 0, $distance = 100): array
    {
        // 参考https://www.jianshu.com/p/1d71ec4367d4
        if ($deg < 0 || $deg > 360) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, '角度最多360度');
        }
        $arc = 6371.393*1000;
        $alpha = deg2rad($deg);
        $lng1 = $lng + $distance * sin($alpha)/($arc *  cos($lat)* (2*pi()/360));
        $lat1 =  $lat + $distance * cos($alpha) / ($arc * (2 * pi()/360));

        return [$lng1, $lat1];
    }
}
