<?php
declare(strict_types=1);
namespace App\Helper;

use Hyperf\Utils\Coroutine;
use Monolog\Processor\ProcessorInterface;

class LogRequestIdProcessor implements ProcessorInterface
{

    public function __invoke(array $record)
    {
        if (Coroutine::inCoroutine()) {
            $requestId = Helper::getRequestId();
            if ($requestId) {
                $record['extra']['request_id'] = $requestId;
            }
            $record['extra']['request_id'] = uniqid();
        }

        return $record;
    }
}
