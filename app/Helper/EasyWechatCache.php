<?php
declare(strict_types=1);
namespace App\Helper;

use GuzzleHttp\Client;
use Hyperf\Guzzle\ClientFactory;
use Hyperf\Utils\ApplicationContext;
use Psr\SimpleCache\CacheInterface;

class EasyWechatCache implements CacheInterface
{
    private $wechatIpUrl = 'https://api.weixin.qq.com';

    private $cachePrefix = 'c:';

    private function getCache(): CacheInterface
    {
        return ApplicationContext::getContainer()->get(CacheInterface::class);
    }

    private function getHttpClient(): Client
    {
        ApplicationContext::getContainer()->get(ClientFactory::class);
        return (new ClientFactory(ApplicationContext::getContainer()))->create([
            'base_uri' => $this->wechatIpUrl,
            'timeout' => 5
        ]);
    }

    public function get($key, $default = null)
    {
        $cache = $this->getCache();
        $result = $cache->get($key);
        Helper::getLogger()->info('easywechat.cache.get', [
            'key' => $key,
            'default' => $default,
            'result' => $result
        ]);
        //微信获取access_token需要重新设置
        if (substr($key, 0, 30) == 'easywechat.kernel.access_token' && $result && isset($result['access_token'])) {
            $accessToken = $result['access_token'];
            $ipRequest = $this->requestIp($accessToken);
            if (!$ipRequest) {
                $cache->delete($key);
                return $default;
            }
            return $result;
        }

        return $result ? $result : $default;
    }

    /**
     * 请求ip地址接口验证access_token是否正确
     *
     * @param $accessToken
     * @return bool
     */
    private function requestIp($accessToken): bool
    {
        $logger = Helper::getLogger();
        try {
            $result = json_decode($this->getHttpClient()->get('/cgi-bin/get_api_domain_ip', [
                'query' => ['access_token' => $accessToken]
            ])->getBody()->getContents(), true);


            $logger->info('easywechat_cache_get_ip_finish', [
                'access_token' => $accessToken,
                'result' => $result
            ]);
            return !(isset($result['errcode']) && $result['errcode'] != 0);
        } catch (\Exception $e) {
            $logger->error('easywechat_cache_get_ip_exception', [
                'access_token' => $accessToken,
                'msg' => $e->getMessage()
            ]);
            return false;
        }
    }

    public function set($key, $value, $ttl = null)
    {
        Helper::getLogger()->info('easywechat.cache.set', [
            'key' => $key,
            'val' => $value,
            'ttl' => $ttl
        ]);
        return $this->getCache()->set($key, $value, $ttl);
    }

    public function delete($key)
    {
        return $this->getCache()->delete($key);
    }

    public function clear()
    {
        return $this->getCache()->clear();
    }

    public function getMultiple($keys, $default = null)
    {
        return $this->getCache()->getMultiple($keys, $default);
    }

    public function setMultiple($values, $ttl = null)
    {
        return $this->getCache()->setMultiple($values, $ttl);
    }

    public function deleteMultiple($keys)
    {
        return $this->getCache()->deleteMultiple($keys);
    }

    public function has($key)
    {
        $result = $this->getCache()->has($key);
        Helper::getLogger()->info('easywechatCache::has', [
            'key' => $key,
            'result' => $result
        ]);
        return $result;
    }
}
