<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/admin-api.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Helper;

use App\Model\Qa\Users;
use Firebase\JWT\JWT as JwtSdk;
use Firebase\JWT\Key;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Arr;
use Youyao\Framework\Logger\LoggerApp;

class Jwt
{
    /**
     * @Inject
     * @var ConfigInterface
     */
    private $config;

    public function generate(Users $user, $expire = "")
    {
        if (!$expire) {
            $expire =  $this->config->get('jwt.app.token_expires');
        }
        $payload = [
            'iss' => 'youyao',
            'aud' => 'youyao-employees',
            'iat' => time(),
            'exp' => time() + $expire,
            'nbf' => time(),
            'sub' => $user->getAttribute('uid'),
        ];
        try {
            return JwtSdk::encode(
                $payload,
                $this->config->get('jwt.app.key'),
                $this->config->get('jwt.app.alg')
            );
        } catch (\Exception $e) {
            Helper::getLogger()->error($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    public function decode(string $token)
    {
        try {
            return (array) JwtSdk::decode(
                $token,
                new Key(
                    $this->config->get('jwt.app.key'),
                    $this->config->get('jwt.app.alg')
                )
            );
        } catch (\Exception $e) {
            Helper::getLogger()->error($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    public function logout(string $token)
    {
        $payload = $this->decode($token);
        if (!$payload) {
            return;
        }

        $expire = Arr::get($payload, 'exp');
        if ($expire) {
            $redis = ApplicationContext::getContainer()
                ->get(RedisFactory::class)
                ->get('default');
            $key = sprintf("youyao:user:logout:token:%s", md5($token));
            $redis->set($key, 1);
            $redis->expireAt($key, $expire);
        }
    }

    public function isLogout($token): bool
    {
        $redis = ApplicationContext::getContainer()
            ->get(RedisFactory::class)
            ->get('default');
        $key = sprintf("youyao:user:logout:token:%s", md5($token));
        $exists = $redis->exists($key);
        return (bool) $exists;
    }
}
