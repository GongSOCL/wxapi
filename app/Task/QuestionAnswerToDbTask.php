<?php

declare(strict_types=1);

namespace App\Task;

use App\Helper\Helper;
use App\Model\Qa\PaperUserAnswer;

class QuestionAnswerToDbTask
{
    public static function handle($dataKey): bool
    {
        $redis = Helper::getRedisCache();
        if (!$redis->exists($dataKey)) {
            return true;
        }

        while (true) {
            $dataItem = $redis->rPop($dataKey);
            if (!$dataItem) {
                return true;
            }
            try {
                $data = json_decode($dataItem, true);
                if ($data) {
                    //TODO:还缺少一个content
                    $data['answers'] = json_encode($data['answers'], JSON_UNESCAPED_UNICODE);
                    $data['created_time'] = $data['created_time'] = date('Y-m-d H:i:s');
                    PaperUserAnswer::create($data);
                }
            } catch (\Exception $e) {
                //仅记录错误，如果有必要，可以拖动恢复
                Helper::getLogger()->error("user_answer_to_db_fail", [
                    'msg' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                    'data' => $dataItem,
                    'key' => $dataKey
                ]);
                return true;
            }
        }
    }
}
