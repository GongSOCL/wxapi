<?php
/**
 * @OA\Schema(
 *     schema="loginAuthenticate",
 *      @OA\Property(property="user_yyid", type="string", description="用户yyid"),
 *      @OA\Property(property="user_token", type="string", description="用户token"),
 *     required={"user_yyid", "user_token"}
 * )
 *
 * @OA\Schema(
 *   schema="wechat_type",
 *   title="wechat_type",
 *   type="int",
 *   description="公众号类型： 1代表公众号 2医生公众号",
 *   enum={1, 2},
 *   default=1
 * )
 */
