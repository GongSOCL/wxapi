<?php

/**
 * @OA\Parameter(
 *     name="app-version",
 *     in="header",
 *     required=true,
 *     description="app应用版本, app传值，其它不传",
 *     @OA\Schema(
 *          type="string"
 *      )
 * ),
 * @OA\Parameter(
 *     name="app-platform",
 *     in="header",
 *     description="平台: 1android 2ios 3小程序 0公众号",
 *     required=true,
 *     @OA\Schema(
 *          type="integer"
 *      )
 * ),
 * @OA\Parameter(
 *     name="device-id",
 *     in="header",
 *     description="设备硬件标识符, app_platform等于1或者2时必传,后期可用于设备消息推送",
 *     required=false,
 *     @OA\Schema(
 *          type="string"
 *      )
 * ),
 * @OA\Parameter(
 *     name="Authorization",
 *     in="header",
 *     description="登陆接口返回token,格式为'Bearer <token>'",
 *     required=true,
 *     @OA\Schema(
 *          type="string"
 *      )
 * ),
 */
