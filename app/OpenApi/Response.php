<?php

/**
 * @OA\Response(
 *    response="params_error",
 *    description="请求失败返回",
 *    @OA\MediaType(
 *        mediaType="application/json",
 *        @OA\Schema(
 *            type="object",
 *            @OA\Property(property="errcode", type="integer", description="错误码"),
 *            @OA\Property(property="errmsg", type="string", description="错误信息"),
 *            @OA\Property(property="data", type="object"),
 *        ),
 *        example={"errcode": 4002, "errmsg": "Parameters error", "data": {}},
 *    ),
 * ),
 */
