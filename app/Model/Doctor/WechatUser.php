<?php

declare (strict_types=1);
namespace App\Model\Doctor;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property string $openid openid
 * @property string $app_openid appopenid
 * @property string $unionid
 * @property string $nickname 微信昵称
 * @property string $headimgurl 头像
 * @property int $sex 性别
 * @property string $country 国家
 * @property string $province 省份
 * @property string $city 城市
 * @property int $subscribe 是否关注0否1是
 * @property string $subscribe_time 关注时间
 * @property string $remark
 * @property string $subscribe_scene 关注场景
 * @property string $qr_scene 扫码场景
 * @property string $qr_scene_str 扫码场景
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class WechatUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wechat_user';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'doctor';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'sex' => 'integer',
        'subscribe' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
