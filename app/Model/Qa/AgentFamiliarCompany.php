<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $user_yyid 人员的编号 对应t_user的yyid
 * @property string $company_name 药品的名称
 * @property string $company_adaptation 适应症
 * @property string $company_territory 治疗领域
 * @property string $drug_name
 * @property int $status 状态，0为删除，1为有效
 * @property string $created_time
 * @property string $modify_time
 */
class AgentFamiliarCompany extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_agent_familiar_company';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
