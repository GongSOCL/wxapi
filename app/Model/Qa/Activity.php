<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $name
 * @property string $event_key
 * @property string $start_time 事件的开始时间
 * @property string $end_time 事件的结束时间，空为无限
 * @property float $money_point
 * @property float $service_point
 * @property int $exist_special 是否存在特例 0表示不存在 1 表示存在
 * @property int $num_pre_day 每人每天可获取的次数
 * @property int $num_total 每人可获取的总次数
 * @property string $created_user_id 创建者的编号
 * @property int $status 状态 0表示未发布    1表示发布
 * @property string $created_time 数据创建时间
 * @property string $modify_time
 */
class Activity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_activity';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'money_point' => 'float',
        'service_point' => 'float',
        'exist_special' => 'integer',
        'num_pre_day' => 'integer',
        'num_total' => 'integer',
        'status' => 'integer'
    ];
}
