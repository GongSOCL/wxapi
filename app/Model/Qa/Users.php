<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;
use Hyperf\Database\Model\Relations\BelongsTo;

/**
 * @property int $uid Primary Key: Unique user ID.
 * @property string $yyid UNIQUE Key: uuid
 * @property string $name nick name.
 * @property string $true_name 真实姓名，认证时填写
 * @property string $pass User’s password md5 (hashed).
 * @property string $mail User’s e-mail address.
 * @property int $mail_verified status is active(1) or blocked(0)
 * @property string $company_yyid 医药代表绑定一家公司
 * @property string $region_yyid
 * @property string $mobile_num user accont mobile num
 * @property int $mobile_verified status is active(1) or blocked(0)
 * @property string $wechat
 * @property string $weibo 新浪微博
 * @property string $tengqq
 * @property string $tel_num
 * @property string $client_ip register ip address
 * @property string $last_client_ip last sign-in ip address
 * @property int $access Timestamp for previous time user accessed the site.
 * @property int $login Timestamp for user’s last login.
 * @property string $picture picture hashkey
 * @property int $v_status Whether the user is active(1) or blocked(0) or reject(2).
 * @property string $avatar_id app头像上传id
 * @property int $v_date verified date
 * @property int $user_type 用户的类型 1表示未认证或解除认证的用户 2 表示已经通过认证的用户
 * @property int $is_agent 0表示无  1表示已经填写信息未认证  2 表示已经填写已认证 3表示申请认证中 4表示拒绝认证的
 * @property int $is_doctor 0表示无  1表示已经填写信息未认证  2 表示已经填写已认证 3表示取消认证的
 * @property string $train_account 金牌培训的账户
 * @property int $submitted_role_type
 * @property int $real_role_type
 * @property string $event 记录注册的来源
 * @property int $created Timestamp for when record was created.
 * @property string $update_date
 *
 * @property Company $company
 */
class Users extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uid'             => 'integer',
        'mail_verified'   => 'integer',
        'mobile_verified' => 'integer',
        'access'          => 'integer',
        'login'           => 'integer',
        'v_status'        => 'integer',
        'v_date'          => 'integer',
        'user_type'       => 'integer',
        'is_agent'        => 'integer',
        'is_doctor'       => 'integer',
        'created'         => 'integer',
        'submitted_role_type' => 'integer',
        'real_role_type' => 'integer',
    ];

    protected $primaryKey = 'uid';

    public $timestamps = false;

    const AGENT_VERIFY_NO = 1;
    const AGENT_VERIFY_OK = 2;
    const AGENT_VERIFY_PROCESSING = 3;
    const AGENT_VERIFY_REFUSED = 4;

    const IS_BIND_WECHAT_1 = 1; //  已绑定微信
    const IS_BIND_WECHAT_0 = 0; //  未绑定微信

    const IS_BIND_IOS_1 = 1; //  已绑定苹果账户
    const IS_BIND_IOS_0 = 0; //  未绑定苹果账户

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_yyid', 'yyid')
            ->where('status', DataStatus::REGULAR);
    }

    public function isVerified(): bool
    {
        return $this->is_agent == self::AGENT_VERIFY_OK;
    }
}
