<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id Primary Key: Unique doctor ID.
 * @property string $yyid UNIQUE Key: uuid
 * @property string $u_yyid user uuid, 所属用户 的编号
 * @property string $name 姓名
 * @property string $job_title 职称
 * @property string $position 职务
 * @property string $degree 学位
 * @property string $expertise_ids 医生擅长
 * @property string $hospital_name 医院名称
 * @property string $h_yyid hospital uuid, 所属医院 的编号
 * @property string $region_yyid 地区编号 对应t_region 的yyid
 * @property string $address 详细地址
 * @property string $depart_name 科室名称
 * @property int $status 是否认证。1为已认证，0为未认证
 * @property string $agreement_time
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class Doctor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_doctor';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
