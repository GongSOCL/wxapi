<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $name 题库名称
 * @property int $single_count 内涵题目数量
 * @property int $multi_count
 * @property int $status 状态 0删除   1有效
 * @property string $created_time
 * @property string $modify_time
 */
class ComplianceQuestion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_question';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'single_count' => 'integer',
        'multi_count' => 'integer',
        'status' => 'integer'
    ];

    /**
     * 返回所有的question_items
     * @return \Hyperf\Database\Model\Relations\HasMany
     */
    public function questionItems()
    {
        return self::hasMany(ComplianceQuestion::class, 'question_yyid', 'yyid')
            ->where('status', DataStatus::REGULAR);
    }

    public function whereValid()
    {
        return $this->where('status', DataStatus::REGULAR);
    }
}
