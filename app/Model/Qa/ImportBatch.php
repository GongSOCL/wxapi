<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $dealer_yyid 供应商的编号对应T_dealer的ID
 * @property string $file_md5 文件的校验码
 * @property string $file_path 文件上传到服务器的路途
 * @property int $total_num 总记录条数
 * @property string $create_user_id 创建数据的管理员编号
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class ImportBatch extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_import_batch';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'total_num' => 'integer'];
}
