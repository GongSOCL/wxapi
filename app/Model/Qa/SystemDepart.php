<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $name 科室名称
 * @property int $status 状态: 0删除 1正确
 * @property string $created_time
 * @property string $modify_time
 */
class SystemDepart extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_system_depart';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'status', 'created_time', 'modify_time'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
