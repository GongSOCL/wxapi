<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $pid 组织回显id
 * @property int $uid 用户id
 * @property int $dir_id 文件目录id
 * @property int $status 状态 1正常 2全选
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property int $is_deleted 0未删除1已删除
 */
class NewKnowledgeUserPower extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_knowledge_user_power';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pid' => 'integer',
        'uid' => 'integer',
        'dir_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'is_deleted' => 'integer'
    ];
}
