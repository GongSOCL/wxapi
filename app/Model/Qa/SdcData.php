<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $dealer_yyid 供应商编号
 * @property string $data_yyid 数据编号
 * @property string $order_date 制单日期
 * @property string $order_month 制单月份
 * @property string $drug_yyid
 * @property string $product_yyid 产品编号   对应 t_drug_product 的yyid
 * @property string $series_yyid 系列的编号    对应 t_drug_series 的 yyid
 * @property string $pro_no 商品编号
 * @property string $pro_name 商品名称
 * @property string $customer_id 客户编码
 * @property string $customer_name 客户名称
 * @property string $hospital_yyid
 * @property int $order_num 订单数量
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class SdcData extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_sdc_data';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'order_num' => 'integer'];
}
