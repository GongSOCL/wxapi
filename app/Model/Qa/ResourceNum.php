<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $resource_yyid 资源的编号
 * @property int $type 资源的类型 1 表示视频   2 表示文献  3 表示新闻公告  4 收藏夹  5 表示药品详情页  6 表示产品手册    7表示回放详情页  8表示直播列表页
 * @property int $read_num 阅读数
 * @property int $share_num 分享次数
 * @property int $share_click_num 分享链接被点击的次数
 * @property int $fav_num 收藏的数量
 * @property int $comment_num 评论数
 */
class ResourceNum extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_resource_num';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'read_num' => 'integer',
        'share_num' => 'integer',
        'share_click_num' => 'integer',
        'fav_num' => 'integer',
        'comment_num' => 'integer'
    ];

    public $timestamps = false;

    const TYPE_VIDEO = 1;
    const TYPE_INTERPRET = 2;
}
