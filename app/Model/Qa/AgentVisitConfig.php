<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $config_name 配置项
 * @property int $config_type 配置类型： 1 拜访目的,后续可以加
 * @property int $visit_id 拜访id, 0表示通用类型
 * @property int $is_extra_input 是否需要额外输入框
 * @property int $config_order 排序
 * @property int $status 状态: 0删除 1正确
 * @property string $created_time
 * @property string $modify_time
 */
class AgentVisitConfig extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_agent_visit_config';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'config_name',
        'config_type',
        'visit_id',
        'is_extra_input',
        'config_order',
        'status',
        'created_time',
        'modify_time'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'             => 'integer',
        'config_type'    => 'integer',
        'visit_id'       => 'integer',
        'is_extra_input' => 'integer',
        'config_order'   => 'integer',
        'status'         => 'integer'
    ];


    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    //拜访目的
    const CONFIG_TYPE_AIM = 1;
}
