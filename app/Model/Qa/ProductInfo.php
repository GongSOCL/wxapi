<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $series_id 药品系列id
 * @property int $info_type 配置类型: 1产品应用 2 市场分析 3招商范围 4 区域价格 5平台政策 6结算细则
 * @property string $content 配置项内容
 * @property int $status 状态: 0删除 1正确
 * @property string $created_time
 * @property string $modify_time
 */
class ProductInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_product_info';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'series_id' => 'integer', 'info_type' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    const INFO_TYPE_DIRECTION = 1;
    const INFO_TYPE_MARKET_ANALYZE = 2;
    const INFO_TYPE_INVESTMENT_SCOPE = 3;
    const INFO_TYPE_ZONE_PRICE = 4;
    const INFO_TYPE_PLATFORM_POLICY = 5;
    const INFO_TYPE_SETTLEMENT_RULES = 6;
}
