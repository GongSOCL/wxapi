<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $exam_start
 * @property string $exam_end
 * @property string $name
 * @property string $introduce
 * @property int $publish_answer
 * @property int $wrong_remind
 * @property string $test_paper_yyid
 * @property string $agreement_yyid
 * @property int $pass_condition
 * @property int $status
 * @property string $created_time
 * @property string $modify_time
 * @property string $platform  所属平台
 * @property int $service_point 服务积分
 * @property int $money_point 消费积分
 */
class ComplianceExam extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_exam';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'publish_answer' => 'integer',
        'wrong_remind' => 'integer',
        'pass_condition' => 'integer',
        'status' => 'integer'
    ];

    //优药平台
    const PLATFORM_YOUYAO = 'youyao';
    //优佳医平台
    const PLATFORM_YOUJIAYI = 'youjiayi';
    //otc平台
    const PLATFORM_OTC = "otc";
    //pharmacy平台
    const PLATFORM_PHARMACY = "pharmacy";
}
