<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $paper_id 问卷id
 * @property int $wechat_id 微信id
 * @property string $wechat_open_id 微信openid
 * @property int $invite_id 邀请记录id,对应wechat_invitee表
 * @property int $question_id 问题id
 * @property string $answers 问题回答答案: json存储
 * @property int $status 0 表示删除 1 表示正常
 * @property string $content
 * @property string $created_time
 * @property string $modify_time
 */
class PaperUserAnswer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_paper_user_answers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'paper_id',
        'wechat_id',
        'wechat_open_id',
        'invite_id',
        'question_id',
        'answers',
        'status',
        'created_time',
        'modify_time'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'paper_id' => 'integer',
        'wechat_id' => 'integer',
        'invite_id' => 'integer',
        'question_id' => 'integer',
        'status' => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
