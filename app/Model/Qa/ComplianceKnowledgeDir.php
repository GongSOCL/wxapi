<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $dir_name 目录名称
 * @property string $p_yyid 父级目录yyid
 * @property int $status 状态，0-删除，1-有效
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class ComplianceKnowledgeDir extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_knowledge_dir';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'yyid', 'dir_name', 'p_yyid', 'status', 'created_time', 'modify_time'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];
}
