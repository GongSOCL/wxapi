<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property int $user_id 用户编号
 * @property int $company_id 公司编号
 * @property int $user_sign_id 签约编号
 * @property int $hospital_id 医院的编号
 * @property int $reps_serve_scope_s_id 申请编号
 * @property int $carryonnum 存量
 * @property int $targetnum 指标
 * @property int $createtime
 * @property string $start 合同医院指标生效开始时间
 * @property string $end 合同医院指标生效开始时间
 * @property int $updatetime
 * @property int $deletetime
 */
class OaUserSignHospital extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oa_user_sign_hospital';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'company_id',
        'user_sign_id',
        'hospital_id',
        'reps_serve_scope_s_id',
        'carryonnum',
        'targetnum',
        'createtime',
        'updatetime',
        'deletetime'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'company_id' => 'integer',
        'user_sign_id' => 'integer',
        'hospital_id' => 'integer',
        'reps_serve_scope_s_id' => 'integer',
        'carryonnum' => 'integer',
        'targetnum' => 'integer',
        'createtime' => 'integer',
        'updatetime' => 'integer',
        'deletetime' => 'integer'
    ];
}
