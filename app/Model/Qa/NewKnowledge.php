<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $pid 父级菜单id
 * @property string $title 名称
 * @property int $file_type 文件类型1视频2文档
 * @property string $file_address 地址
 * @property string $introduce 简介
 * @property int $type 类型 1：目录，2：文件
 * @property int $sort 排序，值越大越靠前
 * @property int $status 状态 1：显示，0：隐藏
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property int $is_deleted 0未删除1已删除
 */
class NewKnowledge extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_knowledge';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pid' => 'integer',
        'file_type' => 'integer',
        'type' => 'integer',
        'sort' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'is_deleted' => 'integer'
    ];
}
