<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id 
 * @property string $name 名称
 * @property int $rank 排序
 * @property int $status 是否显示。1为显示，0为隐藏
 * @property \Carbon\Carbon $created_at 数据的创建时间
 * @property \Carbon\Carbon $updated_at 数据的修改时间
 */
class NewGroupStaticDepartment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_group_static_department';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'rank' => 'integer', 'status' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}