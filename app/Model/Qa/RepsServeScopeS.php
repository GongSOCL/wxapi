<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;
use Hyperf\Database\Model\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $user_yyid 医药代表的编号
 * @property string $series_yyid 系列的编号
 * @property string $hospital_yyid 医院的编号
 * @property string $admin_id 后台审核管理员编号
 * @property string $agreed_time 后台审核时间
 * @property int $status 关系解除4、审批超时3、审批通过 1 、审批不通过2、审批中0     解除申请中5    解除申请未通过 6
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 *
 * @property Users $users
 * @property YouyaoHospital $hospital
 * @property DrugSeries $series
 */
class RepsServeScopeS extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_serve_scope_s';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'yyid',
        'user_yyid',
        'series_yyid',
        'hospital_yyid',
        'admin_id',
        'agreed_time',
        'status',
        'created_time',
        'modify_time'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    public function users(): BelongsTo
    {
        return $this->belongsTo(Users::class, 'user_yyid', 'yyid');
    }

    public function hospital(): BelongsTo
    {
        return $this->belongsTo(YouyaoHospital::class, 'hospital_yyid', 'yyid');
    }

    public function series(): BelongsTo
    {
        return $this->belongsTo(DrugSeries::class, 'series_yyid', 'yyid')
            ->where('status', DataStatus::REGULAR);
    }
}
