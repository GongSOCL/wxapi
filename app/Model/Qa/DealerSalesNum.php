<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $company_yyid 公司yyid 对应t_company和t_user的company_yyid
 * @property string $month 月份
 * @property string $staff_code 工号
 * @property string $staff_name 姓名
 * @property string $hospital_id 医院id
 * @property string $name_cn 医院名称
 * @property string $product_id 产品id
 * @property string $product_name_cn 产品名称
 * @property string $sku_id skuid
 * @property string $sku_name_cn sku名称
 * @property string $month_target 月目标
 * @property string $month_sales 月销量
 * @property string $month_ach 月达成率
 * @property string $ly_sales 去年4-12月均销量
 * @property string $month_gr 月增长
 * @property string $qtd_target 季度目标
 * @property string $qtd_sales 季度销量
 * @property string $qtd_ach 季度达成率
 * @property string $qtd_ly_sales 去年4-12月季度销量
 * @property string $qtd_gr 季度增长
 * @property string $ytd_target 年目标
 * @property string $ytd_sales 年销量
 * @property string $ytd_ach 年达成率
 * @property string $ytd_ly_sales 去年4-12月年销量
 * @property string $ytd_gr 年增长
 * @property int $status 2表示删除 1 表示通过
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerSalesNum extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_sales_num';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];
}
