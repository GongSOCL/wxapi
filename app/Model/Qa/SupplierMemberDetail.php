<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $supplier_id 供应商id
 * @property int $member_id t_supplier_member的id
 * @property int $user_id 用户id
 * @property int $hospital_id 医院id
 * @property int $product_id 医院id
 * @property int $type 1-页面申请 2-组长分配
 * @property int $status 只有type=1时有效1-申请中 2-已通过 3已拒绝 4已解除
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class SupplierMemberDetail extends Model
{
    const STATUS_1 = 1;  //申请中
    const STATUS_2 = 2;  //已通过
    const STATUS_3 = 3;  //已拒绝
    const STATUS_4 = 4;  //已解除

    const TYPE_1 = 1;  //组长申请
    const TYPE_2 = 2;  //组长分配

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_supplier_member_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'supplier_id' => 'integer',
        'member_id' => 'integer',
        'user_id' => 'integer',
        'hospital_id' => 'integer',
        'product_id' => 'integer',
        'type' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
