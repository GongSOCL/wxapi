<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $resource_yyid 资源yyid
 * @property int $admin_id 备注的人yyid
 * @property int $type 类型 1 用户 2医院
 * @property string $content 备注内容
 * @property int $status 1 显示 2删除
 * @property string $created_time
 * @property string $modify_time
 */
class RefuseRemark extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_refuse_remark';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'admin_id' => 'integer', 'type' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
