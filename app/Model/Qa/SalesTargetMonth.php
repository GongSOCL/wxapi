<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $hospital_yyid 对应hospital的yyid
 * @property string $month
 * @property int $target 指标值
 * @property string $drug_yyid 对应series的yyid
 * @property string $area_name
 */
class SalesTargetMonth extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sales_target_month';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'target' => 'integer'];
}
