<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $title 消息的标题
 * @property string $sub_title 消息的简述
 * @property string $content 消息的内容
 * @property string $link 链接地址
 * @property int $msg_type 消息的类型 1 医院的推荐
 * 2表示平台求助 3表示平台消息公告 4 数据更新提醒
 * 5 系统回复用户的消息
 * 6表示平台消息公告微信展示
 * 7日流向更新
 * 8投票提醒
 * 9获得积分
 * 10消耗积分
 * 11 问卷邀请
 * 12 邀请加入群组
 * @property int $send_type 发送的类型 1 表示全部用户的消息  2 表示单个用户的消息
 * @property int $is_feedback 是否答复 (仅对于平台求助)
 * @property string $start_time 消息的开始生效时间
 * @property string $end_time 消息的结束生效时间
 * @property int $status 消息的状态  0表示未审核通过  1 表示审核通过对外可以查看了
 * @property string $create_user_id 数据的创建者编号
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class Msg extends Model
{

    //消息的类型
    const MSG_TYPE_HOSPITAL_RECOMMEND = 1;              //1 医院的推荐
    const MSG_TYPE_ASK_PLATFORM_HELP = 2;               //2表示平台求助
    const MSG_TYPE_PLATFORM_ANNOUNCEMENT = 3;           //3表示平台消息公告
    const MSG_TYPE_DATA_UPDATED = 4;                    //4 数据更新提醒
    const MSG_TYPE_REPLY_USER = 5;                      //5 系统回复用户的消息
    const MSG_TYPE_WECHAT_PLATFORM_ANNOUNCEMENT = 6;    //6表示平台消息公告微信展示
    const MSG_FLOW_DATE_UPDATED = 7;                    //7日流向更新
    const MSG_VOTE_NOTICE = 8;                          //8投票提醒
    const MSG_REWARD_POINT = 9;                         //9获得积分
    const MSG_CONSUMPUTION_POINT = 10;                  //10消耗积分
    const MSG_QUESTIONNAIRE_INVITE = 11;                //11 问卷邀请
    const MSG_INVITE_TO_GROUP = 12;                     //12 邀请加入群组

    const MSG_SUPPLIER_GROUP = 20;                     //20 供应商群组消息
    const MSG_SUPPLIER_APPLY = 21;                     //21 供应商审核消息

    //发送类型
    const SEND_TYPE_ALL = 1;    //全部发送
    const SEND_TYPE_USER = 2;   //单个用户发送

    //审核状态
    const STATUS_AUDITING = 0; //审核未通过
    const STATUS_AUDITED  = 1; //通过

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_msg';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'msg_type' => 'integer',
        'send_type' => 'integer',
        'is_feedback' => 'integer',
        'status' => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
