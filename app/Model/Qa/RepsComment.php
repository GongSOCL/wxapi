<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $resource_yyid 资源的编号
 * @property string $user_yyid 用户编号
 * @property string $content 评论的内容详情
 * @property string $comment_pyyid 父级评论的编号
 * @property int $type 1 表示视频  2 表示 文献  3 表示 新闻  4 表示收藏夹 5直播视频  7 优药直播 8最新解读
 * @property int $status 是否被删除  1 表示正常  0表示删除
 * @property string $created_time 创建时间
 * @property string $modify_time 修改时间
 */
class RepsComment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_comment';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'type' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
