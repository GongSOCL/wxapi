<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $group_id 分组的id
 * @property int $vote_id 投票的编号
 * @property int $user_id 投票人id
 * @property int $status 1表示赞同 2表示不赞同
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class NewGroupVoteDetail extends Model
{
    const STATUS_AGREE = 1;
    const STATUS_DISAGREE = 2;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_group_vote_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'group_id' => 'integer',
        'vote_id' => 'integer',
        'user_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
