<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $user_id 用户id
 * @property int $platform 0-h5 1-安卓 2-ios 3-微信小程序
 * @property string $login_time 登录时间
 * @property string $login_ip 登录ip
 * @property int $is_first_login 1-第一次登录 0-非第一次
 * @property \Carbon\Carbon $created_at 操作时间
 */
class UserLoginLog extends Model
{
    const IS_FIRST_LOGIN = 1; // 第一次登录
    const NOT_FIRST_LOGIN = 0; // 非第一次登录

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_user_login_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'platform' => 'integer',
        'is_first_login' => 'integer',
        'created_at' => 'datetime'
    ];
}
