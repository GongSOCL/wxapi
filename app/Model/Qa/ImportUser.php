<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid 导入yyid
 * @property int $company_id 导入公司id
 * @property int $import_id 导入id, 当前最新生效记录此id为0
 * @property string $user_id 导入用户原始id
 * @property string $name 导入用户名称
 * @property string $user_unique 导入用户唯一标识,后期用作其它类型导入转换，如钉钉的userid
 * @property string $unionid 钉钉用户唯一标识
 * @property string $avatar 用户头像
 * @property string $mobile 钉钉通讯录手机号
 * @property string $title 员工title
 * @property string $email 员工email
 * @property string $org_email 员工企业邮箱
 * @property string $extension 扩展信息字段
 * @property int $is_leader 是否部门主管
 * @property string $nickname 用户呢称
 * @property string $employment_date 入职日期
 * @property string $job_no 员工编号
 * @property string $account_uid 关系优药帐户id
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class ImportUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_import_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'yyid',
        'company_id',
        'import_id',
        'user_id',
        'name',
        'user_unique',
        'unionid',
        'avatar',
        'mobile',
        'job_number',
        'title',
        'email',
        'org_email',
        'dept_id_list',
        'extension',
        'is_leader',
        'nickname',
        'employment_date',
        'job_no',
        'created_at',
        'updated_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_id' => 'integer',
        'import_id' => 'integer',
        'is_leader' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
