<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $meeting_id 会议id
 * @property int $user_type 人员类型: 1主席２讲者
 * @property string $name 用户名
 * @property int $hospital_id 医院id
 * @property int $depart_id 科室id
 * @property int $level_id 级别id
 * @property string $project 讲者课题
 * @property int $status 是否有效 0否 1是
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class MeetingParticipator extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_meeting_participator';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'meeting_id',
        'user_type',
        'name',
        'hospital_id',
        'depart_id',
        'level_id',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'meeting_id' => 'integer',
        'user_type' => 'integer',
        'hospital_id' => 'integer',
        'depart_id' => 'integer',
        'level_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    const TYPE_CHAIRMAN = 1;
    const TYPE_SPEAKER = 2;
}
