<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $inviter_yyid 邀请者的yyid
 * @property string $invitee_yyid 被邀请者的yyid
 * @property string $group_yyid 群组yyid
 * @property int $is_subscribe 是否已关注 0表示未关注 1表示已关注
 * @property int $user_type 0表示未知  1表示代表 2表示医生
 * @property int $status 状态  0表示已删除  1表示正常 2表示待加入  3表示拒绝
 * @property \Carbon\Carbon $created_at
 */
class RepsDoctorInviter extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_doctor_inviter';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_subscribe' => 'integer',
        'user_type' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime'
    ];

    const ALREADY_SUBSCRIBE = 0; //未关注
    const NOT_SUBSCRIBE = 1; //已关注

    const USER_TYPE_UNKNOW = 0;  //未知
    const USER_TYPE_AGENT = 1;  //代表
    const USER_TYPE_DOCTOR = 2;  //医生

    const STATUS_DELETE = 0; //已删除
    const STATUS_COMMON = 1; //正常
    const STATUS_JOINING = 2; //待加入
    const STATUS_REFUSE = 3; //拒绝
}
