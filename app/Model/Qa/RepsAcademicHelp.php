<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $user_yyid 求助者yyid
 * @property int $type 求助类型 1文献资料 2 视频 3学术问题 4流向纠错
 * @property string $content 需求描述／问题描述
 * @property string $email 电子邮箱
 * @property string $ask_time 求助时间
 * @property int $status 求助状态 0答复中 1 已答复
 * @property int $feedback_user_id 答复者id
 * @property string $feedback_content 答复内容
 * @property string $feedback_time 答复时间
 * @property int $feedback_is_read 答复是否已读
 * @property string $created_time
 * @property string $modify_time
 */
class RepsAcademicHelp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_academic_help';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'status' => 'integer',
        'feedback_user_id' => 'integer',
        'feedback_is_read' => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
