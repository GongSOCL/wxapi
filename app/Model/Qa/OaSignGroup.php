<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;
use Hyperf\Utils\Collection;

/**
 * @property int $id
 * @property string $title 合同打包标题
 * @property int $user_id 合同组所属人员id
 * @property string $start 合同组开始日期
 * @property string $end 合同组结束日期
 * @property string $sign_end 合同签署过期时间
 * @property int $state 合同组状态: 0 待发布 1待生成 2待签署 3已签署 4生成失败
 * @property int $status 状态: 0
 * @property string $createtime
 * @property string $updatetime
 * @property string $deletetime
 *
 * @property-read Collection|[]OaUserSign $contracts
 */
class OaSignGroup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oa_sign_group';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'state' => 'integer',
        'user_info_id' => 'integer',
        'status' => 'integer'
    ];

    const CREATED_AT =  'createtime';
    const UPDATED_AT = 'updatetime';

    //合同组状态: 0 待发布 1待生成 2待签署 3已签署 4生成失败
    const STATE_NEW = 0;
    const STATE_TO_GENERATE_HTML = 1;
    const STATE_TO_SIGN = 2;
    const STATE_SIGNED = 3;
    const STATE_GENERATE_FAILED = 4;
    const STATE_PDF_GENERATED = 5;

    public function isValidToGenerate(): bool
    {
        return in_array($this->state, [
            self::STATE_NEW,
            self::STATE_TO_GENERATE_HTML,
            self::STATE_GENERATE_FAILED
        ]);
    }

    public function isValidToSign(): bool
    {
        return $this->state == self::STATE_TO_SIGN;
    }

    public function isPdfGenerated(): bool
    {
        return $this->state == self::STATE_PDF_GENERATED;
    }

    /**
     * @return \Hyperf\Database\Model\Relations\HasMany
     */
    public function contracts()
    {
        return $this->hasMany(OaUserSign::class, 'group_id', 'id');
    }

    public function isSigned(): bool
    {
        return $this->state == self::STATE_SIGNED;
    }
}
