<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $owner_yyid 创建者的yyid 对应 t_user的yyid
 * @property string $leader_yyid 所有者的用户编号
 * @property string $group_yyid 分组的编号对应agent_doctor_group的yyid 空表示未分组
 * @property string $user_openid 微信的openid
 * @property string $user_yyid 用户编号 当用户注册成功后更新
 * @property string $inviter_yyid 邀请者的编号
 * @property int $is_subscribe 是否已关注 0表示未关注 1表示已关注
 * @property int $user_type 0表示未知  1表示代表 2表示医生
 * @property int $wechat_type 公众号类型: 1代表公众号 2医生公众号
 * @property int $status 状态  0表示已删除  1表示正常 2表示待加入  3表示拒绝
 * @property string $created_time
 * @property string $modify_time
 */
class GroupMember extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_group_member';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_subscribe' => 'integer',
        'user_type' => 'integer',
        'status' => 'integer'
    ];

    const CREATED_AT =  'created_time';
    const UPDATED_AT = 'modify_time';

    const USER_TYPE_ALL = -1;
    const USER_TYPE_UNKONW = 0;      //未知组
    const USER_TYPE_WORKGROUP = 1;   //代表工作组
    const USER_TYPE_DOCTOR = 2;      //医生组

    const STATUS_DELETE = 0;        //已删除
    const STATUS_NORMAL = 1;        //正常
    const STATUS_JOIN = 2;          //待加入
    const STATUS_REJECT = 3;        //已拒绝

    const NOT_SUBSCRIBE = 0;       //未关注
    const IS_SUBSCRIBE = 1;        //已关注
}
