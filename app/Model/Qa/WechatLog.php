<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $ToUserName 开发者微信号
 * @property string $FromUserName 发送方帐号（一个OpenID）
 * @property string $Event 事件类型，subscribe(订阅)、unsubscribe(取消订阅)
 * @property string $EventKey 事件KEY值，与自定义菜单接口中KEY值对应
 * @property string $MsgType 消息类型
 * @property int $MsgId 消息id
 * @property string $MediaId 媒体id，可以调用多媒体文件下载接口拉取数据
 * @property string $PicUrl 图片链接
 * @property string $Format 语音格式，如amr，speex等
 * @property string $Recognition 语音识别结果，UTF8编码
 * @property string $ThumbMediaId 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
 * @property string $Title 消息标题
 * @property string $Description 消息描述
 * @property string $Url 消息链接
 * @property string $Content 文本消息内容
 * @property string $Ticket 二维码的ticket，可用来换取二维码图片
 * @property int $wechat_type 公众号类型: 1代表公众号 2医生公众号
 * @property int $CreateTime 数据创建时间
 * @property string $created_time 数据创建时间
 */
class WechatLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_wechat_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'MsgId' => 'integer', 'wechat_type' => 'integer', 'CreateTime' => 'integer'];

    const UPDATED_AT = null;
    const CREATED_AT = 'created_time';
}
