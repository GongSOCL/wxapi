<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $user_id 用户编号
 * @property string $last_page_name 上一个访问的页面
 * @property string $now_page_name 当前访问的页面
 * @property string $browser 浏览器类型
 * @property string $os 操作系统
 * @property string $screen 分辨率
 * @property string $cookie_id 全局会话标识 不清COOkie都存在
 * @property string $trace_id 单次会话标识 关闭浏览器就不存在了
 * @property string $ip
 * @property string $url
 * @property string $params 请求的参数
 * @property string $date 日期
 * @property string $id_type 特定下的数据编号
 * @property string $http_user_agent http_user_agent
 * @property string $is_pretend 1 表示是伪登录的  0表示非伪登录的
 * @property string $datetime 时间
 * @property int $is_user
 * @property string $created_time
 */
class TUserTongji extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_user_tongji';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'is_user' => 'integer'];
}
