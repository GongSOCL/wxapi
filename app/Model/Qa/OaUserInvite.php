<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id 手机号码
 * @property string $mobile 手机号码
 * @property string $truename 真实名称
 * @property string $id_card 身份证号码
 * @property string $eno 员工编号
 * @property int $service_provice
 * @property int $service_city
 * @property int $province 省
 * @property string $birthday
 * @property int $city 市
 * @property int $area 区
 * @property string $address 地址
 * @property int $user_id 用户编号
 * @property int $admin_id 管理员编号
 * @property int $company_id 公司
 * @property int $admin_company_id 录入人员公司编号
 * @property string $mastercontractno
 * @property string $slavercontractno
 * @property string $sign_date
 * @property string $valid_date
 * @property string $desc 备注
 * @property string $status 状态:1=审批中,2=审批通过,3=解除中,4=已解除
 * @property int $createtime
 * @property int $updatetime
 * @property int $deletetime
 */
class OaUserInvite extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oa_user_invite';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'mobile',
        'truename',
        'id_card',
        'eno',
        'service_provice',
        'service_city',
        'province',
        'birthday',
        'city',
        'area',
        'address',
        'user_id',
        'admin_id',
        'company_id',
        'admin_company_id',
        'mastercontractno',
        'slavercontractno',
        'sign_date',
        'valid_date',
        'desc',
        'status',
        'createtime',
        'updatetime',
        'deletetime'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service_provice' => 'integer',
        'service_city' => 'integer',
        'province' => 'integer',
        'city' => 'integer',
        'area' => 'integer',
        'user_id' => 'integer',
        'admin_id' => 'integer',
        'company_id' => 'integer',
        'admin_company_id' => 'integer',
        'createtime' => 'integer',
        'updatetime' => 'integer',
        'deletetime' => 'integer'
    ];

    public $timestamps = false;
}
