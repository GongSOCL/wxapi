<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $hospital_yyid
 * @property string $series_yyid
 * @property string $transaction_month
 * @property string $transaction_date
 * @property string $created_time
 */
class HospitalTransactionStatus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_hospital_transaction_status';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer'];
}
