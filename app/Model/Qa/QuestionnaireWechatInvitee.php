<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;
use App\Model\Qa\Users;

/**
 * @property int $id
 * @property int $paper_id 问卷id
 * @property int $invitor_id 邀请者id
 * @property string $wechat_open_id 邀请者微信openid
 * @property string $wechat_union_id 邀请者微信unionid
 * @property int $invitee_type 受邀请人员类型: 1代表 2医生
 * @property string $last_invite_time 最后邀请时间
 * @property string $last_expire_time 最后邀请时间
 * @property int $invite_status 邀请状态: 1新建 2已邀请 3已开始答题 4已完成答题 5已重新邀请
 * @property int $status 邀请记录状态: 1正常 0已删除
 * @property string $created_time
 * @property string $modify_time
 *
 * @property QuestionnairePaper|null $paper
 */
class QuestionnaireWechatInvitee extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_questionnaire_wechat_invitee';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'paper_id' => 'integer',
        'invitor_id' => 'integer',
        'invitee_type' => 'integer',
        'invite_status' => 'integer',
        'status' => 'integer'
    ];

    //invite_status
    const INVITE_STATUS_NEW = 1;        //新建
    const INVITE_STATUS_INVITED = 2;    //已邀请
    const INVITE_STATUS_START = 3;      //已开始答题
    const INVITE_STATUS_FINISHED = 4;   //已完成答题
    const INVITE_STATUS_REINVITED = 5;  //已重新邀请

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    /**
     * 问卷关联模型
     * @return \Hyperf\Database\Model\Relations\HasOne
     */
    public function paper()
    {
        return $this->hasOne(QuestionnairePaper::class, 'id', 'paper_id');
    }

    /**
     * 邀请者关联模型
     * @return \Hyperf\Database\Model\Relations\HasOne
     */
    public function invitor()
    {
        return $this->hasOne(Users::class, 'uid', 'invitor_id');
    }

    /**
     * 是否已答题
     * @return bool
     */
    public function isFinished()
    {
        return $this->invite_status == self::INVITE_STATUS_FINISHED;
    }

    /**
     * 目前仅允许
     * @return bool
     */
    public function isStatusValidToAnswer()
    {
        return in_array($this->invite_status, [
            self::INVITE_STATUS_START,
            self::INVITE_STATUS_INVITED,
            self::INVITE_STATUS_REINVITED,
        ]);
    }

    /**
     * 判断邀请状态是否可以重新邀请参与问卷
     * @return bool
     */
    public function isStatusValidToReInvited()
    {
        return in_array($this->invite_status, [
            self::INVITE_STATUS_INVITED,
            self::INVITE_STATUS_REINVITED,
            self::INVITE_STATUS_START
        ]);
    }

    /**
     * 当前状态是否允许发起提醒答题通知
     *
     * @return bool
     */
    public function isStatusValidToNotice()
    {
        return in_array($this->invite_status, [
            self::INVITE_STATUS_INVITED,
            self::INVITE_STATUS_START,
            self::INVITE_STATUS_REINVITED
        ]);
    }

    /**
     * 邀请记录是否过期
     * @return bool
     */
    public function isExpired($now = '')
    {
        if (!$now) {
            $now = date("Y-m-d H:i:s");
        }
        return $this->last_expire_time < $now;
    }

    /**
     * 是否可以作为计数标准
     * 已经结束的
     * 新建/已邀请/已开始回答/已重新邀请且末过期
     * @param $now
     * @return bool
     */
    public function isValidToCount($now = '')
    {
        if (!$now) {
            $now = date("Y-m-d H:i:s");
        }
        return $this->invite_status == self::INVITE_STATUS_FINISHED || (
                in_array($this->invite_status, [
                    self::INVITE_STATUS_NEW,
                    self::INVITE_STATUS_START,
                    self::INVITE_STATUS_INVITED,
                    self::INVITE_STATUS_REINVITED
                ]) && $this->last_expire_time > $now
            );
    }

    public function isNewInviteRecord()
    {
        return $this->invite_status == self::INVITE_STATUS_NEW;
    }
}
