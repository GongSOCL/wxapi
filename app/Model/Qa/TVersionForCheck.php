<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $app_version app版本号
 * @property string $channel 渠道 channel表id
 * @property string $phone 账号
 * @property string $psd 密码
 * @property int $user_id 用户id
 * @property int $created_id
 * @property int $status 1审核中2审核已通过
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class TVersionForCheck extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_version_for_check';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'created_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
