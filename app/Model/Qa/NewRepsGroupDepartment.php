<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id 
 * @property int $group_department_id 群组科室id
 * @property int $department_id 正常科室id
 * @property int $status 1-正常 2-失效
 * @property \Carbon\Carbon $created_at 数据的创建时间
 * @property \Carbon\Carbon $updated_at 数据的修改时间
 */
class NewRepsGroupDepartment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_reps_group_department';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'group_department_id' => 'integer', 'department_id' => 'integer', 'status' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}