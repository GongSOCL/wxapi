<?php

declare (strict_types=1);

namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $dealer_yyid
 * @property string $staff_code 工号
 * @property string $staff_name 姓名
 * @property string $phone_number 手机号
 * @property int $status 2表示禁用 1 表示通过
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerAgentPhone extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_agent_phone';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];
}
