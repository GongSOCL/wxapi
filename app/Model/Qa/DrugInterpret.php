<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $product_yyid
 * @property string $drug_yyid
 * @property string $series_yyid
 * @property string $title
 * @property string $sub_title
 * @property string $content
 * @property string $interpret_image
 * @property int $rank
 * @property int $status
 * @property string $created_time
 * @property string $modify_time
 */
class DrugInterpret extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_drug_interpret';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'rank' => 'integer', 'status' => 'integer'];
}
