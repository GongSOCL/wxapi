<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $user_yyid 医药代表的编号
 * @property string $series_yyid 系列的编号
 * @property string $hospital_yyid 医院的编号
 * @property string $admin_id 后台审核管理员编号
 * @property string $agreed_time 后台审核时间
 * @property int $status 关系解除4、审批超时3、审批通过 1 、审批不通过2、审批中0     解除申请中5    解除申请未通过 6
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class RepsServeScope extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_serve_scope_s';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'yyid',
        'user_yyid',
        'series_yyid',
        'hospital_yyid',
        'admin_id',
        'agreed_time',
        'status',
        'created_time',
        'modify_time'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const STATUS_WAIT = 0;          //申请服务审核中
    const STATUS_ACCESS = 1;        //审核成功
    const STATUS_FAILED = 2;        //审核失败
    const STATUS_DELAY = 3;         //审核超时
    const STATUS_REMOVE = 4;        //解除关系
    const STATUS_REMOVE_APPLY = 5;  //解除服务申请中
    const STATUS_REMOVE_FAILED = 6; //解除服务失败
}
