<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id Primary Key: Unique  ID.
 * @property string $yyid UNIQUE Key: uuid
 * @property int $sort
 * @property int $is_series 1 表示系列  2 表示单品
 * @property string $series_yyid 系列的编号  对应t_drug_series 表的yyid
 * @property string $name 商品名称
 * @property string $c_name 通用名称
 * @property string $e_name 英文名称
 * @property string $py_name 汉语拼音
 * @property string $list_pic 系列列表图片
 * @property string $detail_pic 系列详情图片
 * @property int $type 药品类型，1：处方药  2:非处方药 3：器械
 * @property int $medical_insurance 医保属性, 1:yes，0:no
 * @property float $one_treatment_num 单个疗程的使用量保留一位小数
 * @property int $level_1_num
 * @property float $level_1_point
 * @property int $level_2_num
 * @property float $level_2_point
 * @property int $level_3_num
 * @property float $level_3_point
 * @property int $level_4_num
 * @property float $level_4_point
 * @property float $level_0_point
 * @property int $status 是否显示。1为显示，0为隐藏
 * @property int $is_show 是否展示
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DrugProduct extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_drug_product';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sort' => 'integer',
        'is_series' => 'integer',
        'type' => 'integer',
        'medical_insurance' => 'integer',
        'one_treatment_num' => 'float',
        'level_1_num' => 'integer',
        'level_1_point' => 'float',
        'level_2_num' => 'integer',
        'level_2_point' => 'float',
        'level_3_num' => 'integer',
        'level_3_point' => 'float',
        'level_4_num' => 'integer',
        'level_4_point' => 'float',
        'level_0_point' => 'float',
        'status' => 'integer',
        'is_show' => 'integer'
    ];
}
