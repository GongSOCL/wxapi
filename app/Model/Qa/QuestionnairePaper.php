<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;
use Hyperf\Database\Model\Relations\HasMany;
use Hyperf\Utils\Collection;

/**
 * @property int $id
 * @property string $yyid 问卷yyid
 * @property string $name 问卷名称
 * @property int $paper_type 问卷类型: 1普通问卷 2逻辑问卷
 * @property int $wrong_tips 是否答错提示
 * @property string $publish_time 问卷发布时间
 * @property string $expire_time 问卷结束时间时间
 * @property int $total_quota 总发放作答数
 * @property int $used_quote 已分配数量
 * @property int $finished_quota 已经完成配额
 * @property int $status 问卷状态: 1新建 2已发布 3已删除
 * @property string $platform 发布平台
 * @property int $start_bg_id 首页背景图upload_id
 * @property int $tail_bg_id 尾页背景图upload_id
 * @property int $answer_bg_id 答题页背景图upload_id
 * @property string $intro 问卷简介
 * @property string $cash 可获得现金
 * @property int $points 可获得积分
 * @property int $is_invite_answer 是否邀请作答: 1是 0否
 * @property int $invite_expire_hour 邀请失效时间
 * @property string $created_time
 * @property string $modify_time
 *
 * @property null|Collection|PaperQuestionItem[] logicQuestions
 */
class QuestionnairePaper extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_questionnaire_paper';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                 => 'integer',
        'wrong_tips'         => 'integer',
        'total_quota'        => 'integer',
        'finished_quota'     => 'integer',
        'status'             => 'integer',
        'cash'               => 'float',
        'points'             => 'integer',
        'is_invite_answer'   => 'integer',
        'invite_expire_hour' => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';


    //status状态
    const STATUS_NEW= 1;           //新建
    const STATUS_PUBLISHED = 2;     //已发布
    const STATUS_DELETED = 3;       //已删除

    //is_invite_answer是否邀请作答
    const INVITE_ANSWER_YES = 1;
    const INVITE_ANSWER_NO  = 0;

    //wrong_tips是否错误提示
    const WRONG_TIPS_YES = 1;
    const WRONG_TIPS_NO = 0;

    const PAPER_TYPE_NORMAL = 1;
    const PAPER_TYPE_LOGIC = 2;

    const PLATFORM_YOUYAO = 'youyao';
    const PLATFORM_YOUJIAYI = 'youjiayi';
    const PLATFORM_OTC = 'otc';

    /**
     * 是否已发布
     *
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->status == self::STATUS_PUBLISHED;
    }

    /**
     * 微信邀请记录关联模型
     * @return HasMany
     */
    public function wechatInvitees()
    {
        return $this->hasMany(QuestionnaireWechatInvitee::class, 'paper_id', 'id')
            ->where('status', DataStatus::REGULAR);
    }

    /**
     * @return HasMany
     */
    public function invitee()
    {
        return $this->hasMany(QuestionnairePaperInvitee::class, 'paper_id', 'id')
            ->where('status', DataStatus::REGULAR);
    }

    /**
     * 是否开放邀请
     * @return bool
     */
    public function isInviteToAnswer()
    {
        return $this->is_invite_answer == self::INVITE_ANSWER_YES;
    }

    /**
     * 问卷关联模型
     * @return HasMany
     */
    public function testPapers(): HasMany
    {
        return self::hasMany(QuestionnaireTestPaper::class, 'questionnaire_paper_id', 'id')
            ->where('status', DataStatus::REGULAR);
    }

    public function isLogicPaper(): bool
    {
        return $this->paper_type == self::PAPER_TYPE_LOGIC;
    }

    public function logicQuestions(): ?HasMany
    {
        if ($this->paper_type == self::PAPER_TYPE_LOGIC) {
            return $this->hasMany(PaperQuestionItem::class, 'paper_id', 'id')
                ->where("status", DataStatus::REGULAR);
        }
        return null;
    }

    public function isExpired(): bool
    {
        if (!$this->expire_time) {
            return false;
        }
        $now = date('Y-m-d H:i:s');
        return $this->expire_time < $now;
    }
}
