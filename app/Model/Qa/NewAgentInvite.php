<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $user_openid
 * @property string $user_unionid 被邀请者的微信unionid
 * @property int $inviter_id 邀请者的编号
 * @property int $is_subscribe 是否已关注 0表示未关注 1表示已关注
 * @property int $user_type 0表示未知  1表示代表 2表示医生
 * @property int $status 状态 0表示已删除 1表示正常
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class NewAgentInvite extends Model
{
    const AGENT = 1;        //代表
    const DOCTOR = 2;      //医生

    const DELETE = 0;        //已删除
    const NORMAL = 1;        //正常
    const DOCTOR_JOIN = 2;        //正常

    const IS_SUBSCRIBE = 1;
    const NOT_SUBSCRIBE = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_agent_invite';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'inviter_id' => 'integer',
        'is_subscribe' => 'integer',
        'user_type' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
