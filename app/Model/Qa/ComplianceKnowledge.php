<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $dir_yyid 对应t_knowledge_dir目录yyid
 * @property string $title 标题
 * @property string $introduce 简介
 * @property string $file_address 文件地址
 * @property int $file_type 文件类型，1-视频，2-文档
 * @property int $status
 * @property string $created_time
 * @property string $modify_time
 */
class ComplianceKnowledge extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_knowledge';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'yyid',
        'dir_yyid',
        'title',
        'introduce',
        'file_address',
        'file_type',
        'status',
        'created_time',
        'modify_time'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'file_type' => 'integer', 'status' => 'integer'];
}
