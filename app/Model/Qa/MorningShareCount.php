<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $user_id 点击人优佳医用户id
 * @property int $platform 点击来源平台 3医生公众号 4医生APP 5医生小程序
 * @property int $pic_id 图片id
 * @property int $share_user_id 分享者id
 * @property int $share_platform 分享者平台 1代表公众号 2代表APP 6代表小程序
 * @property \Carbon\Carbon $created_at 时间
 */
class MorningShareCount extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_morning_share_count';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'platform' => 'integer',
        'pic_id' => 'integer',
        'share_user_id' => 'integer',
        'share_platform' => 'integer',
        'created_at' => 'datetime'
    ];
}
