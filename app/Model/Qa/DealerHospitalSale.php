<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $company_yyid 公司yyid 对应t_company和t_user的company_yyid
 * @property string $type 类型（数量 金额）
 * @property string $month 月份
 * @property string $staff_code 工号
 * @property string $hospital_id 医院id
 * @property string $product_id 产品id
 * @property string $sku_id skuid
 * @property string $month_target 月目标
 * @property string $month_sales 月销量
 * @property string $month_ach 月达成率
 * @property string $ly_sales 去年4-12月均销量
 * @property string $month_gr 月增长
 * @property string $eff_call_num 有效拜访数量
 * @property int $status 2表示删除 1 表示通过
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerHospitalSale extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_hospital_sales';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];
}
