<?php

declare(strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $leader_yyid 代表的编号对应user的yyid
 * @property string $group_name 分组的名称
 * @property int $group_type 分组的类 型1表示 代表   2表示医生分组
 * @property string $created_user_yyid
 * @property int $status 状态 0表示删除  1表示正常
 * @property string $created_time
 * @property string $modify_time
 */
class Group extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_group';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'group_type' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    const GROUP_TYPE_REPS = 1;       //工作群
    const GROUP_TYPE_DOCTOR = 2;     //医生群

    const STATUS_DELETE = 0;        //已删除
    const STATUS_NORMAL = 1;        //正常
}
