<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $company_yyid 公司yyid 对应t_company和t_user的company_yyid
 * @property int $activity_id 活动ID
 * @property string $telephone 手机号码
 * @property string $staff_code 员工编号
 * @property string $activity_type_code 活动类型编号
 * @property string $activity_type_name 活动类型名称
 * @property string $activity_dt 活动日期
 * @property string $check_in_datetime 签入时间
 * @property string $check_out_datetime 签出时间
 * @property string $activity_duration 活动时常(单位:小时)
 * @property string $inscode 机构编号
 * @property string $insname_cn 机构名称
 * @property string $customer_code 医生编号
 * @property string $customer_name 医生姓名
 * @property string $flag 是否有效GPS
 * @property int $status 2表示删除 1 表示通过
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerCheckoutDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_checkout_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'activity_id' => 'integer', 'status' => 'integer'];
}
