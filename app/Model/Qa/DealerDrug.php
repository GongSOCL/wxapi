<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $dealer_yyid 供应商的编号对应T_dealer的ID
 * @property string $drug_yyid 药品的编号对应T_drug的yyID
 * @property string $pro_no 供应商药品的编号
 * @property string $drug_name 供应商的药品名称
 * @property string $product_yyid 产品编号   对应 t_drug_product 的yyid
 * @property string $series_yyid 系列的编号    对应 t_drug_series 的 yyid
 * @property int $status -1 表示删除 0表示禁用 1 表示正常
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerDrug extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_drug';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];
}
