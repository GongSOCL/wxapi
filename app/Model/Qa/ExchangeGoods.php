<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $goods_name 兑换的商品的名称
 * @property string $goods_pic 商品的图片
 * @property int $total_num 兑换的商品的总数量空表示无限
 * @property float $money_point 需要的消费积分
 * @property float $service_point 需要的服务积分
 * @property string $start_time 开始时间
 * @property string $end_time 结束时间，空为无限
 * @property int $goods_type 类型 1表示 现金 2表示实物  3表示虚拟
 * @property string $created_user_id 创建者的编号
 * @property string $desc 描述
 * @property string $user_detail_desc 显示在用户积分明细里的描述
 * @property int $status 状态 0表示未发布    1表示发布
 * @property string $created_time
 * @property string $modify_time
 */
class ExchangeGoods extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_exchange_goods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'total_num' => 'integer',
        'money_point' => 'float',
        'service_point' => 'float',
        'goods_type' => 'integer',
        'status' => 'integer'
    ];

    const CREATED_AT = 'created_time';

    const UPDATED_AT = 'modify_time';
}
