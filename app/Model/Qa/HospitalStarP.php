<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $hospital_yyid 医院编号 对应t_hospital表的id字段
 * @property string $product_yyid 产品编号
 * @property int $total_month 累计月份
 * @property int $total 累计数量
 * @property int $num 当月的数量
 * @property string $avg 平均
 * @property string $avg_except_0 非0月的平均
 * @property int $0_month 0数月
 * @property int $0_nearby 最近连续0月数
 * @property int $upper_month 连续上升月数
 * @property int $down_month 连续下降月数
 * @property string $stdevp 标准差
 * @property string $percent 标准差/平均值
 * @property string $month 月份
 * @property int $star
 * @property int $star_up 上涨次数
 * @property int $star_down 下降次数
 * @property int $up_100 连续大于100次数
 * @property int $up_50 连续大于50次数
 * @property int $up_10 连续大于10次数
 * @property int $below_10 连续小于10次数
 * @property string $created_time 数据的创建时间
 */
class HospitalStarP extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_hospital_star_p';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'total_month' => 'integer',
        'total' => 'integer',
        'num' => 'integer',
        '0_month' => 'integer',
        '0_nearby' => 'integer',
        'upper_month' => 'integer',
        'down_month' => 'integer',
        'star' => 'integer',
        'star_up' => 'integer',
        'star_down' => 'integer',
        'up_100' => 'integer',
        'up_50' => 'integer',
        'up_10' => 'integer',
        'below_10' => 'integer'
    ];
}
