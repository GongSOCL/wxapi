<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $exam_yyid 对应考试yyid
 * @property string $name 人员类型名称
 * @property string $product_yyid 产品yyid
 * @property string $user_yyid 人员yyid
 * @property int $status 0-删除，1-有效
 * @property string $created_time
 * @property string $modify_time
 */
class ComplianceExamUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_exam_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];
}
