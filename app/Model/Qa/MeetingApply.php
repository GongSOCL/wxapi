<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $name 会议名称
 * @property int $applicant_id 申请人id, 优药用户id
 * @property string $applicant_mobile 申请人手机号
 * @property int $manager_id 会议管理员id, 优佳医用户id
 * @property int $compere_id 会议主持人id
 * @property int $meeting_type 项目类型
 * @property int $meeting_sub_type 项目名称
 * @property int $meeting_category 项目分类
 * @property int $state 申请状态: 1新建待审核 2审核通过  3审核拒绝
 * @property int $zone_id 大区id
 * @property int $province_id 省份id
 * @property int $hospital_id 医院id
 * @property int $depart_id 部门id
 * @property int $series_id 系列药品id
 * @property int $chairman_id 会议主席id
 * @property int $range_type 1区域 2中央
 * @property string $reject_reason 审核拒绝原因
 * @property int $status 是否有效 0否 1是
 * @property string $start 第一场会议开始时间，用于排序
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class MeetingApply extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_meeting_apply';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'applicant_id',
        'applicant_mobile',
        'manager_id',
        'compere_id',
        'meeting_type',
        'meeting_sub_type',
        'state',
        'zone_id',
        'province_id',
        'hospital_id',
        'depart_id',
        'series_id',
        'chairman_id',
        'reject_reason',
        'status',
        'created_at',
        'updated_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'applicant_id' => 'integer',
        'manager_id' => 'integer',
        'compere_id' => 'integer',
        'meeting_type' => 'integer',
        'meeting_sub_type' => 'integer',
        'state' => 'integer',
        'zone_id' => 'integer',
        'province_id' => 'integer',
        'hospital_id' => 'integer',
        'depart_id' => 'integer',
        'series_id' => 'integer',
        'chairman_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    const STATE_NEW = 1;
    const STATE_SUCCESS = 2;
    const STATE_FAIL = 3;
    const STATE_CANCEL = 4;

    const CATEGORY_AREA = 1;
    const CATEGORY_CENTER = 2;

    public function isValidToEdit(): bool
    {
        return in_array($this->state, [
            self::STATE_NEW,
            self::STATE_FAIL,
            self::STATE_SUCCESS
        ]);
    }

    public function isValidToEditAll(): bool
    {
        return in_array($this->state, [
           self::STATE_NEW,
            self::STATE_FAIL
        ]);
    }

    public function isCanceled(): bool
    {
        return $this->state == self::STATE_CANCEL;
    }
}
