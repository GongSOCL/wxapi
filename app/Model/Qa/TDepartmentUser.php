<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $company_id 公司id
 * @property string $name 用户名,平台人员
 * @property string $avatar 用户头像
 * @property int $dept_id 平台部门id
 * @property string $import_dept_uniq 导入部门uniq
 * @property int $from_type 添加来源: 1继承自组织绑定 2手动绑定
 * @property int $auto_import_id 绑定部门时最后导入id
 * @property string $user_uniq 用户唯一标识符
 * @property string $mobile 人员手机号
 * @property string $mail 人员邮箱
 * @property string $job_no 员工工号
 * @property int $account_uid 关联优药用户uid
 * @property int $is_leader 是否部门负责人: 0否 1是
 * @property string $employment_date 入职日期
 * @property int $status 记录状态: 1正常 0已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $title 员工title
 */
class TDepartmentUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_department_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_id' => 'integer',
        'dept_id' => 'integer'
        , 'from_type' => 'integer',
        'auto_import_id' => 'integer',
        'account_uid' => 'integer',
        'is_leader' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
