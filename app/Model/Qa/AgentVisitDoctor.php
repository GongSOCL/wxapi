<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $visit_id 拜访记录id
 * @property int $doctor_id 医生关联id,关联: t_doctor_openid_userinfo
 * @property int $status 状态: 0删除 1正确
 * @property string $created_time
 * @property string $modify_time
 */
class AgentVisitDoctor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_agent_visit_doctor';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'visit_id', 'doctor_id', 'status', 'created_time', 'modify_time'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'visit_id' => 'integer', 'doctor_id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
