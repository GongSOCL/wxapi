<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $dealer_yyid 供应商的编号对应T_dealer的ID
 * @property string $hospital_no 供应商提供的医院编号
 * @property string $hospital_name 供应商提供的医院名称
 * @property string $hospital_yyid 供应商的编号对应T_hospital的hID
 * @property int $status -1 表示删除 0表示禁用 1 表示正常
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerHospital extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_hospital';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'yyid',
        'dealer_yyid',
        'hospital_no',
        'hospital_name',
        'hospital_yyid',
        'status',
        'created_time',
        'modify_time'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
