<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id Primary Key: Unique hospital ID.
 * @property string $yyid UNIQUE Key: uuid
 * @property string $name 名称
 * @property string $tel_num 联系电话
 * @property string $address 公司地址
 * @property string $introduction 公司介绍
 * @property string $contacter 联系人
 * @property string $email 邮件地址
 * @property int $flag 是否显示。1为后台创建，2表示前台创建
 * @property string $create_user_id 数据的创建者编号
 * @property int $status 是否显示。1为显示，0为隐藏
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class Company extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_company';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'flag' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
