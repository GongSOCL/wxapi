<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $dealer_yyid 数据供应商的编号对应T_dealer表的IDp字段
 * @property string $batch_yyid 数据导入的批次号对应T_import_batch表的ID字段
 * @property string $batch_list_yyid 数据导入的记录号对应T_import_batch_list表的ID字段
 * @property string $company_yyid 人员所属的组织编号 对应t_company表的cid
 * @property string $region_yyid 地区编号 对应t_region表的id字段
 * @property string $user_yyid 医药代表编号 对应t_agent表的id字段
 * @property string $hospital_yyid 医院编号 对应t_hospital表的id字段
 * @property string $drug_yyid 指定单品的编号  对应t_drug_sku表的yyid字段
 * @property string $product_yyid 指定产品的编号  对应t_drug_product表的yyid字段
 * @property string $series_yyid 指定产品的编号  对应t_drug_series表的yyid字段
 * @property string $transaction_month
 * @property string $transaction_date 数据流向时间
 * @property int $num 数量
 * @property float $point 单个流向对应的积分
 * @property int $status -1 表示删除   1 表示正常
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class RepsTransactionP extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_transaction_p';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'num' => 'integer', 'point' => 'float', 'status' => 'integer'];
}
