<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $drug_yyid 药品的编号
 * @property string $hospital_yyid 医院的编号
 * @property int $track_type 开发进度,0-未知，1-临采，2-已提单，3-已上会，4-药事会通过，5-正式采购
 * @property int $status 0-无效，1-有效
 * @property string $user_yyid 操作用户编号
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class RepsHospitalTrack extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_hospital_track';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'track_type' => 'integer', 'status' => 'integer'];
}
