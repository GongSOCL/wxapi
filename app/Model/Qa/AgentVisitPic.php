<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $visit_id 拜访记录id
 * @property int $pic_type 图片类型: 1签入 2签出
 * @property int $upload_id 上传id,关联upload_qiniu
 * @property int $status 状态: 0删除 1正确
 * @property string $created_time
 * @property string $modify_time
 */
class AgentVisitPic extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_agent_visit_pic';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'visit_id', 'pic_type', 'upload_id', 'status', 'created_time', 'modify_time'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'        => 'integer',
        'visit_id'  => 'integer',
        'pic_type'  => 'integer',
        'upload_id' => 'integer',
        'status'    => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    // 签入图片
    const PIC_TYPE_CHECK_IN = 1;
    // 签出图片
    const PIC_TYPE_CHECK_OUT = 2;

    // 签出
    const CHECK_OUT = 2;
    // 签入
    const CHECK_IN = 1;
}
