<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $visit_id 拜访id
 * @property int $user_id 代表id
 * @property string $ua 请求ua
 * @property string $ip 请求ip地址
 * @property string $operate_type 操作类型
 * @property float $amap_latitude 高德坐标纬度
 * @property float $amap_longitude 高德坐标经度
 * @property string $device_type 请求设备描述
 * @property int $status 0 表示删除 1 表示正常
 * @property string $created_time
 * @property string $modify_time
 */
class AgentVisitLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_agent_visit_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'visit_id' => 'integer',
        'user_id' => 'integer',
        'amap_latitude' => 'float',
        'amap_longitude' => 'float',
        'status' => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
