<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $company_yyid 公司编号 对应t_company表的id
 * @property string $month 年月 例如 201706
 * @property float $point 当月积分
 * @property int $youyao_rank 当月全局排名
 * @property float $last_point 上个月积分
 * @property int $last_youyao_rank 上个月全局排名
 * @property float $this_year_point 今年的积分汇总
 * @property float $total_point 累计获取积分值
 * @property int $status -1 表示删除  1 表示正常
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class CompanyMonthPoint extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_company_month_point';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'point' => 'float',
        'youyao_rank' => 'integer',
        'last_point' => 'float',
        'last_youyao_rank' => 'integer',
        'this_year_point' => 'float',
        'total_point' => 'float',
        'status' => 'integer'
    ];
}
