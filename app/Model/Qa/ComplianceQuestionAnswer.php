<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $question_item_yyid 问卷题目的编号 对应t_question_item的yyid
 * @property string $name 选项名称
 * @property int $is_right 是否为正确选项,0-错误，1-正确
 * @property int $sort 排序越小越靠前
 * @property int $status 0 表示删除 1 表示正常
 * @property string $created_time
 * @property string $modify_time
 */
class ComplianceQuestionAnswer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_question_answer';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'is_right' => 'integer', 'sort' => 'integer', 'status' => 'integer'];
}
