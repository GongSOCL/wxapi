<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $group_id 分组的id
 * @property int $up_yyid 发起者编号
 * @property float $from_tax 目前的贡献积分
 * @property float $to_tax 目标的贡献积分
 * @property string $start_time
 * @property string $end_time
 * @property int $status 0表示进行中  1表示已通过 2表示未通过  3表示已过期  4表示手动关闭 一般投票期为7天
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class NewGroupVote extends Model
{
    const STATUS_PROCESSING = 0; // 进行中
    const STATUS_PASSED = 1; // 已通过
    const STATUS_FAILED = 2; // 未通过
    const STATUS_EXPIRED = 3; // 已过期
    const STATUS_CLOSED = 4; // 手动关闭
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_group_vote';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'group_id' => 'integer',
        'up_yyid' => 'integer',
        'from_tax' => 'float',
        'to_tax' => 'float',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
