<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $user_id 代表id
 * @property int $live_invite_id live_invite表id
 * @property int $doctor_info_id new_doctor_openid_userinfo表id
 * @property \Carbon\Carbon $created_at
 */
class LiveInviteHistory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'live_invite_history';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'live_invite_id' => 'integer',
        'doctor_info_id' => 'integer',
        'created_at' => 'datetime'
    ];
}
