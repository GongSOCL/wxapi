<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $user_yyid
 * @property string $name 收藏夹的名称
 * @property int $is_sys 0表示非系统的可以删除  1表示固定项无法删除
 * @property int $status 1 表示正常 0表示逻辑删除
 * @property int $is_shared  1表示已经分享 0表示未分享
 * @property string $p_fav_yyid 收藏别人的收藏夹的名称的yyid
 * @property string $created_time
 * @property string $modify_time
 */
class RepsFav extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_fav';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'is_sys' => 'integer', 'status' => 'integer', 'is_shared' => 'integer'];
}
