<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id Primary Key: Unique  ID.
 * @property string $yyid UNIQUE Key: uuid
 * @property string $u_yyid UNIQUE Key: uuid
 * @property int $sort
 * @property string $warn
 * @property string $name 商品名称
 * @property string $c_name 通用名称
 * @property string $e_name 英文名称
 * @property string $py_name 汉语拼音
 * @property int $type 药品类型，1：处方药
 * @property string $product_yyid 产品编号   对应 t_drug_product 的yyid
 * @property string $series_yyid 系列的编号    对应 t_drug_series 的 yyid
 * @property string $approval_id 批准文号/审批编号,如: 批准文号：国药准字Zxxxxx
 * @property string $specs 规格
 * @property string $dosage_form 剂型
 * @property string $indication 适应症
 * @property string $ingredients 成份
 * @property string $shape 性状
 * @property string $usage 用法用量
 * @property string $adverse_reaction 不良反应
 * @property string $taboo 禁忌
 * @property string $attentions 注意事项
 * @property string $attentions_pw 孕妇及哺乳期妇女用药
 * @property string $attentions_ch 儿童用药
 * @property string $attentions_oa 老年用药
 * @property string $overdose 药物过量
 * @property string $chinical_trial 临床试验
 * @property string $toxicology 药理毒理
 * @property string $pharmacokinetics 药代动力学
 * @property string $storage_conditions 贮藏
 * @property string $package 包装
 * @property string $period_validity 有效期
 * @property string $performance_standards 执行标准
 * @property int $price 价格，人民币，元
 * @property string $preparation 制剂
 * @property string $adaptation_department 适应科室
 * @property string $therapeutic_field 治疗领域
 * @property string $product_advantage 产品优势
 * @property float $one_treatment_num 单个疗程的使用量保留一位小数
 * @property string $channel 渠道
 * @property string $business_type 业务类型
 * @property int $medical_insurance 医保属性, 1:yes，0:no
 * @property string $competitive_products 市场竞品
 * @property string $manufacturer 生产企业/生产厂家
 * @property string $l_info
 * @property string $v_info
 * @property int $imgs_num 上传图片数量
 * @property int $status 是否显示。1为显示，0为隐藏
 * @property int $created Timestamp for when record was created.
 * @property string $update_date
 */
class DrugSku extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_drug_sku';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sort' => 'integer',
        'type' => 'integer',
        'price' => 'integer',
        'one_treatment_num' => 'float',
        'medical_insurance' => 'integer',
        'imgs_num' => 'integer',
        'status' => 'integer',
        'created' => 'integer'];
}
