<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $group_id 分组的编号对应new_group的id
 * @property string $user_unionid 微信的unionid
 * @property int $inviter_id 邀请者的id
 * @property int $status 状态  0表示已删除  1表示正常 2表示待加入  3表示拒绝
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class NewGroupMember extends Model
{
    const DELETE = 0;        //已删除
    const NORMAL = 1;        //正常
    const JOIN = 2;          //待加入
    const REJECT = 3;        //已拒绝

    const WORK = 1;       //工作群
    const DOCTOR = 2;     //医生群
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_group_member';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'group_id' => 'integer',
        'inviter_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
