<?php

declare (strict_types=1);
namespace App\Model\Qa;

/**
 * @property int $id
 * @property int $test_id 考试id
 * @property int $exam_id 考试id
 * @property int $item_id 对应题目id
 * @property int $question_bank_id 题库id
 * @property string $fraction 题目分数【若题库每题的平均分】
 * @property int $item_num 每个题库分配的题目数量【当题库 随机题库题目】
 * @property int $type 1 题库出题 2题库出题随机出题 3随机出题随机题目
 * @property int $status 0-删除，1-有效
 * @property int $is_deleted 0未删除1已删除
 * @property int $created_id
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
use App\Model\Model;

class TComplianceTestExamFraction extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_test_exam_fraction';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'test_id' => 'integer',
        'exam_id' => 'integer',
        'item_id' => 'integer',
        'question_bank_id' => 'integer',
        'item_num' => 'integer',
        'type' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
