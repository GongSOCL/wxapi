<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $user_yyid t_users用户yyid
 * @property string $idcard_front_location 身份证正面存储地址
 * @property string $idcard_back_location 身份证反面存储地址
 * @property string $realname 姓名
 * @property string $id_code 身份证号
 * @property int $gender 性别
 * @property string $nationality 民族
 * @property string $birth_date 生日
 * @property string $address 地址
 * @property string $due_date 身份证到期时间
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class UserIdentity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_user_identity';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'gender' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
