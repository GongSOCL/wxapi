<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $user_id 用户id
 * @property string $ios_id apple_id
 * @property int $status 操作状态1绑定0解绑
 * @property \Carbon\Carbon $created_at 操作时间
 */
class UserBindIosLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_user_bind_ios_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'user_id' => 'integer', 'status' => 'integer', 'created_at' => 'datetime'];
}
