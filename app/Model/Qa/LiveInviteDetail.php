<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $live_invite_id live_invite表id
 * @property int $doctor_info_id new_doctor_openid_userinfo表id
 * @property string $doctor_openid 被邀请的医生openid
 * @property string $doctor_unionid 被邀请的医生unionid
 * @property int $is_read 状态 0表示未读  1表示已读
 * @property int $status 状态 0表示删除  1表示正常
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class LiveInviteDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'live_invite_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'live_invite_id' => 'integer',
        'doctor_info_id' => 'integer',
        'is_read' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
