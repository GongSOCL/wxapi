<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $product_yyid 产品编号   对应 t_drug_product 的yyid
 * @property string $series_yyid 系列的编号    对应 t_drug_series 的 yyid
 * @property string $name 分类名称
 * @property string $content 分类内容
 * @property int $manual_rank 手册顺序
 * @property int $recom_rank 推荐位顺序
 * @property int $status 是否显示 1显示  2隐藏
 * @property int $is_recom 是否推荐 1是 2否
 * @property string $created_time
 * @property string $modify_time
 */
class DrugManual extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_drug_manual';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'manual_rank' => 'integer',
        'recom_rank' => 'integer',
        'status' => 'integer',
        'is_recom' => 'integer'
    ];
}
