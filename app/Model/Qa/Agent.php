<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $aid Primary Key: Unique  ID.
 * @property string $yyid UNIQUE Key: uuid
 * @property string $u_yyid 所属会员ID
 * @property string $truename 真实姓名
 * @property string $serve_direction 服务的方向 逗号分隔   1 表示 医院列名服务 2 表示营销学术服务
 * @property string $familiar_offices 熟悉的科室 内 1 外 2 妇3  儿 4
 * @property string $address
 * @property string $employer_company
 * @property string $expertise 擅长领域
 * @property string $identitycard 身份证号码
 * @property int $points 积分
 * @property string $birthday 出生日期
 * @property int $city 城市
 * @property int $work 工作
 * @property int $education 学历
 * @property int $status 是否认证。1为已认证，0为未认证
 * @property string $agreement_time
 * @property int $created Timestamp for when record was created.
 * @property string $update_date
 * @property string $expertise_ids 擅长领域的ID 数组多个用逗号分隔
 * @property string $extension_product
 * @property string $cover_hospital
 * @property string $working_age 工龄
 * @property string $service_region 可服务区域
 */
class Agent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_agent';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'aid' => 'integer',
        'points' => 'integer',
        'city' => 'integer',
        'work' => 'integer',
        'education' => 'integer',
        'status' => 'integer',
        'created' => 'integer'
    ];

    public $timestamps = false;


    const DIRECTION_HOSPITAL_LISTING = 1;
    const DIRECTION_MARKETING_ACADEMIC = 2;
}
