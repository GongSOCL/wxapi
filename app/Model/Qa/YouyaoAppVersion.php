<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $app_version app版本号
 * @property int $is_forced_update 是否强制更新0否1是
 * @property string $version_info 版本说明
 * @property string $os_type 操作系统android 或 ios
 * @property string $download_url 下载链接
 * @property int $created_id
 * @property int $status 1有效2无效
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class YouyaoAppVersion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_youyao_app_version';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_forced_update' => 'integer',
        'created_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
