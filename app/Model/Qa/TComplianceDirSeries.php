<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $dir_id 目录id，关联
 * @property int $series_id 产品id
 * @property int $status 记录状态:1有效 0无效
 * @property string $created_time
 * @property string $modify_time
 */
class TComplianceDirSeries extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_dir_series';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'dir_id' => 'integer', 'series_id' => 'integer', 'status' => 'integer'];
}
