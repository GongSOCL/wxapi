<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id 
 * @property int $activity_id 活动id
 * @property string $event_key 活动key
 * @property int $user_id 代表id
 * @property int $nid 医生id
 * @property \Carbon\Carbon $created_at 数据的创建时间
 */
class NewInviteQrcode extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_invite_qrcode';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'activity_id' => 'integer', 'user_id' => 'integer', 'nid' => 'integer', 'created_at' => 'datetime'];

    public $timestamps = false;
}