<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $bucket 上传bucket
 * @property string $file_name 上传文件名
 * @property string $file_key 文件Key，删除时需要使用到
 * @property string $url 文件url
 * @property int $status 状态: 1新建 2 预删除 3已删除
 * @property int $storage_type 存储类型: 1七牛 2 minio 3oss
 * @property string $created_time
 * @property string $modify_time
 */
class UploadQiniu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_upload_qiniu';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'bucket', 'file_name', 'file_key', 'url', 'status', 'created_time', 'modify_time'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const STATUS_NORMAL = 1;        //正常状态
    const STATUS_PRE_DELETE = 2;    //预删除状态
    const STATUS_DELETED = 3;       //已经删除云端文件

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
    
    const STORAGE_TYPE_QINIU = 1;
    const STORAGE_TYPE_MINIO = 2;
    const STORAGE_TYPE_OSS = 3;

    public function isValid(): bool
    {
        return $this->status == self::STATUS_NORMAL;
    }

    public function isPreDelete(): bool
    {
        return $this->status == self::STATUS_PRE_DELETE;
    }

    public function isDeleted(): bool
    {
        return $this->status == self::STATUS_DELETED;
    }
}
