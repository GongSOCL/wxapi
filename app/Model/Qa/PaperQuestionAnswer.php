<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;

/**
 * @property int $id
 * @property int $question_id 问卷题目id: 对应t_paper_question_items
 * @property string $name 问题的答案
 * @property int $sort 排序越小越靠前
 * @property int $is_right 是否正确 0否 1是
 * @property int $status 0 表示删除 1 表示正常
 * @property string $created_time
 * @property string $modify_time
 */
class PaperQuestionAnswer extends Model
{
    const CREATED_AT = 'created_time';

    const UPDATED_AT = 'modify_time';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_paper_question_answer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'question_id', 'name', 'sort', 'status', 'created_time', 'modify_time'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'question_id' => 'integer', 'sort' => 'integer', 'status' => 'integer'];
}
