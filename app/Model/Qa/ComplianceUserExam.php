<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $user_yyid
 * @property string $openid
 * @property string $exam_yyid
 * @property string $pass_time
 * @property int $status
 * @property string $created_time
 * @property string $modify_time
 */
class ComplianceUserExam extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_compliance_user_exam';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    public function isPassed(): bool
    {
        return $this->status == DataStatus::REGULAR;
    }
}
