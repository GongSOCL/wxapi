<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $company_id 公司名称
 * @property string $name 用户名,平台人员
 * @property int $dept_id 平台部门id
 * @property string $import_dept_uniq 导入部门uniq
 * @property int $from_type 添加来源: 1继承自组织绑定 2手动绑定
 * @property int $auto_import_id 绑定部门时最后导入id
 * @property string $user_uniq 导入用户唯一标识符
 * @property string $mobile 人员手机号
 * @property string $mail 人员邮箱
 * @property string $job_no 员工工号
 * @property string $account_uid 关系优药帐户id
 * @property string $employment_date 入职日期
 * @property int $status 记录状态: 1正常 0已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class DepartmentUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_department_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'dept_id',
        'from_type',
        'auto_import_id',
        'mobile',
        'mail',
        'job_no',
        'employment_date',
        'status',
        'created_at',
        'updated_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'dept_id' => 'integer',
        'from_type' => 'integer',
        'auto_import_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
