<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $company_yyid 公司yyid 对应t_company和t_user的company_yyid
 * @property string $dealer_yyid 供应商yyid 对应t_dealer的yyid
 * @property string $period
 * @property string $ta_id
 * @property string $ta_name_cn
 * @property string $ta_name_en
 * @property string $brand_id 品牌id
 * @property string $brand_name_cn 品牌名称
 * @property string $brand_name_en 品牌名称
 * @property string $keybrand
 * @property string $product_id 产品id
 * @property string $product_name_cn 产品名称
 * @property string $product_name_en 产品名称
 * @property string $generic_name
 * @property string $short_name_en 简称
 * @property string $specification 规范
 * @property string $sku_id skuid 对应t_dealer_drug的pro_no
 * @property string $sku_name_cn sku名称
 * @property string $sku_name_en sku名称
 * @property string $series_yyid series yyid
 * @property string $unit_cn 单位
 * @property string $unit_en 单位
 * @property string $manufacturer 制造商
 * @property string $import
 * @property string $excel_file_name excel临时文件名 用于区分每次的数据属于哪个excel文件
 * @property int $status 2表示删除 1 表示通过
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerDrugC extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_drug_c';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
