<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $l_id 计划id
 * @property int $m_id 资料id
 * @property int $user_id 分配用户id
 * @property string $user_name 分配用户名称
 * @property string $user_phone 分配用户手机号
 * @property int $user_department_id 分配用户部门
 * @property int $lr_st 当有一个读了默认修改计划时不可对此人修改资料 1计划进行中
 * @property int $status 状态 1正常，2已开始【修改该计划下所有lr_st=1】 3学习完成
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class LearningMaterialsDistribution extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'learning_materials_distribution';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'l_id' => 'integer',
        'm_id' => 'integer',
        'user_id' => 'integer',
        'user_department_id' => 'integer',
        'lr_st' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
