<?php

declare (strict_types=1);

namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $series_name 系列名称
 * @property string $sub_series_name
 * @property string $series_name_en 系列英文名称
 * @property int $sort 排序
 * @property string $list_pic 系列列表图片
 * @property string $detail_pic 系列详情图片
 * @property string $manufacturer 生产商
 * @property string $type
 * @property string $approval_id 批准文号/审批编号,如: 批准文号：国药准字Zxxxxx
 * @property string $desc
 * @property int $children_num 子产品的数量
 * @property int $status
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DrugSeries extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_drug_series';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'yyid',
        'series_name',
        'sub_series_name',
        'series_name_en',
        'sort',
        'list_pic',
        'detail_pic',
        'manufacturer',
        'type',
        'approval_id',
        'desc',
        'children_num',
        'status',
        'created_time',
        'modify_time'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'sort' => 'integer', 'children_num' => 'integer', 'status' => 'integer'];
}
