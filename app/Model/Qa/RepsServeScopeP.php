<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $user_yyid 医药代表的编号
 * @property string $product_yyid 产品的编号
 * @property string $series_yyid 产品的编号
 * @property string $hospital_yyid 医院的编号
 * @property string $admin_id 后台审核管理员编号
 * @property string $agreed_time 后台审核时间
 * @property int $status 关系解除4、审批超时3、审批通过 1 、审批不通过2、审批中0     解除申请中5    解除申请未通过 6
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class RepsServeScopeP extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_serve_scope_p';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];
}
