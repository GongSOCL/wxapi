<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $supplier_id 供应商id
 * @property int $is_leader 0-不是组长 1-是组长
 * @property int $user_id 用户id
 * @property int $pid 群组组长id
 * @property string $u_yyid 所属会员ID
 * @property int $status 0-待加入 1-正常 2-退出
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class SupplierMember extends Model
{
    const STATUS_0 = 0;
    const STATUS_1 = 1;
    const STATUS_2 = 2;

    const IS_LEADER = 1;
    const NOT_LEADER = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_supplier_member';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'supplier_id' => 'integer',
        'is_leader' => 'integer',
        'user_id' => 'integer',
        'pid' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
