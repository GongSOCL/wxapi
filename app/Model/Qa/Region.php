<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id 地区自增ID
 * @property string $yyid
 * @property string $region_name 地区的名称
 * @property string $pinyin 地区的拼音，如果是英文的话直接显示英文字母
 * @property string $short_pinyin 地区首字母组成，用于快速搜索用，如果是英文的话直接显示英文字母
 * @property string $pid_yyid
 * @property int $pid 地区父ID   国家是0
 * @property string $zip 邮政编码
 * @property int $level 地区的层级，国家为一级 省为二级   市为三级  地区为四级
 * @property int $status 状态 1表示 正常  0 表示禁用  -1 表示逻辑删除
 */
class Region extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_region';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'pid' => 'integer', 'level' => 'integer', 'status' => 'integer'];

    const LEVEL_COUNTRY = 1;
    const LEVEL_PROVINCE = 2;
    const LEVEL_CITY = 3;
    const LEVEL_DISTRICT = 4;
}
