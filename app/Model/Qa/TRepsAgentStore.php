<?php

declare (strict_types=1);
namespace App\Model\Qa;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property int $user_id 医药代表的id（t_users）
 * @property int $hospital_id 医院的id（t_youyao_hospital）
 * @property int $admin_id 后台审核管理员编号
 * @property string $agreed_time 后台审核时间
 * @property int $status 关系解除4、审批超时3、审批通过 1 、审批不通过2、审批中0     解除申请中5    解除申请未通过 6
 * @property int $sort 排序权重
 * @property \Carbon\Carbon $created_at 数据的创建时间
 * @property \Carbon\Carbon $updated_at 数据的修改时间
 */
class TRepsAgentStore extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_agent_store';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
     protected $casts = [
         'id' => 'integer',
         'user_id' => 'integer',
         'hospital_id' => 'integer',
         'admin_id' => 'integer',
         'status' => 'integer',
         'created_at' => 'datetime',
         'updated_at' => 'datetime'
     ];
}