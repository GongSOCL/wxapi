<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;
use Hyperf\Database\Model\Relations\BelongsTo;
use Hyperf\Database\Model\Relations\HasMany;
use Hyperf\Utils\Collection;

/**
 * @property int $id
 * @property int $paper_id 问卷id
 * @property string $name 题目的内容
 * @property int $sort 题目的排序  排序越小越靠前
 * @property int $type 题目类型 1表示单选题 2表示多选题  3表示填写题
 * @property int $status 0 表示删除 1 表示正常
 * @property int $is_skip 是否允许跳过, 0必做 1允许跳过
 * @property string $created_time
 * @property string $modify_time
 *
 * @property QuestionnairePaper $paper
 * @property Collection|Rule[] $questionRules
 */
class PaperQuestionItem extends Model
{
    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    //单选
    const QUESTION_TYPE_SINGLE = 1;
    //多选
    const QUESTION_TYPE_MULTI = 2;
    //文本
    const QUESTION_TYPE_TEXT = 3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_paper_question_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'paper_id' => 'integer',
        'sort' => 'integer',
        'type' => 'integer',
        'status' => 'integer',
        'is_skip' => 'integer',
    ];

    /**
     * 是否带选项题目类型.
     */
    public function isWithOptions(): bool
    {
        return in_array($this->type, [
            self::QUESTION_TYPE_SINGLE,
            self::QUESTION_TYPE_MULTI,
        ]);
    }

    public function paper(): BelongsTo
    {
        return $this->belongsTo(QuestionnairePaper::class, 'paper_id', 'id');
    }

    public function answer(): HasMany
    {
        return $this->hasMany(PaperQuestionAnswer::class, 'question_id', 'id')
            ->where('status', DataStatus::REGULAR);
    }

    public function isMultiType(): bool
    {
        return $this->type == self::QUESTION_TYPE_MULTI;
    }
}
