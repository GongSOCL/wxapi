<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $uid Primary Key: Unique user ID.
 * @property string $yyid UNIQUE Key: uuid
 * @property string $sid access token 码
 * @property string $client_ip ip地址
 * @property int $status 是否显示。1为显示，0为隐藏
 * @property int $created unix时间戳
 * @property string $login_from
 */
class UserSession extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_user_session';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['uid' => 'integer', 'status' => 'integer', 'created' => 'integer'];
}
