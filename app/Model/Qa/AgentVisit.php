<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Constants\DataStatus;
use App\Model\Model;
use Hyperf\Database\Model\Relations\HasOne;
use Hyperf\Utils\Collection;

/**
 * @property int $id
 * @property int $user_id 代表id
 * @property int $hospital_id 医院id
 * @property int $visit_type 拜访类型
 * @property string $visit_result 拜访结果
 * @property string $next_visit_purpose 后续跟进事项
 * @property string $check_in_time 签入时间
 * @property string $check_out_time 签出时间
 * @property int $check_status 签到状态: 1签出 2签入
 * @property int $is_collaborative 是否协访: 0否 1是
 * @property int $check_in_geo 签入地理位置id
 * @property int $check_out_geo 签出地理位置id
 * @property int $check_in_geo_opt 签入地理位置超出原因
 * @property string $check_in_geo_comment 签入地理位置超出额外输入
 * @property int $check_out_geo_opt 签出地理位置超出原因,对应visit_config id
 * @property string $check_out_geo_comment 签出地理位置超出说明
 * @property int $check_in_geo_over 签入是否超出范围
 * @property int $check_out_geo_over 签出范围是否超出范围
 * @property int $visit_state  是否有效的拜访0有效1无效
 * @property string $visit_state_comment 关于是否有效的拜访的说明
 * @property int $visit_special_id 异常原因id: 0表示非异常
 * @property string visit_special_comment 异常原因备注
 * @property int $status 状态: 0删除 1正确
 * @property string $created_time
 * @property string $modify_time
 * @property int $is_supplement 是否补签 0否 1是
 *
 * @property-read AgentVisitGeo $checkInGeo
 * @property-read AgentVisitGeo $checkOutGeo
 * @property-read YouyaoHospital $hospital
 * @property-read Collection $visitDeparts
 */
class AgentVisit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_agent_visit';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'hospital_id',
        'visit_type',
        'visit_result',
        'next_visit_purpose',
        'check_in_time',
        'check_out_time',
        'check_status',
        'check_in_geo',
        'check_out_geo',
        'check_in_geo_opt',
        'check_in_geo_comment',
        'check_out_geo_opt',
        'check_out_geo_comment',
        'check_in_geo_over',
        'check_out_geo_over',
        'status',
        'created_time',
        'modify_time'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                 => 'integer',
        'user_id'            => 'integer',
        'hospital_id'        => 'integer',
        'visit_type'         => 'integer',
        'check_status'       => 'integer',
        'check_in_geo'       => 'integer',
        'check_out_geo'      => 'integer',
        'check_in_geo_opt'   => 'integer',
        'check_out_geo_opt'  => 'integer',
        'check_in_geo_over'  => 'integer',
        'check_out_geo_over' => 'integer',
        'status'             => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';


    // 当面拜访
    const VISIT_TYPE_TO_FACE = 1;
    // 线上拜访
    const VISIT_TYPE_ONLINE = 2;
    // 内部会议
    const VISIT_TYPE_INTERNAL_MEETING = 3;
    // 外部会议
    const VISIT_TYPE_EXTERNAL_MEETING = 4;
    // 行政类工作
    const VISIT_TYPE_ADMINISTRATIVE_WORK = 5;


    // 已签入
    const CHECK_STATUS_CHECK_IN = 1;
    // 已签出
    const CHECK_STATUS_CHECK_OUT = 2;

    public function getVisitTypeDesc(): string
    {
        switch ($this->visit_type) {
            case self::VISIT_TYPE_TO_FACE:
                return "当面拜访";
                break;
            case self::VISIT_TYPE_ONLINE:
                return "线上拜访";
                break;
            case self::VISIT_TYPE_INTERNAL_MEETING:
                return "内部会议";
                break;
            case self::VISIT_TYPE_EXTERNAL_MEETING:
                return "外部会议";
            case self::VISIT_TYPE_ADMINISTRATIVE_WORK:
                return "行政类工作";
            default:
                return "其它拜访方式";
                break;
        }
    }

    public function isCheckIn(): bool
    {
        return self::CHECK_STATUS_CHECK_IN == $this->check_status;
    }

    public function isCheckOut(): bool
    {
        return self::CHECK_STATUS_CHECK_OUT == $this->check_status;
    }

    public function visitDeparts(): \Hyperf\Database\Model\Relations\HasMany
    {
        return $this->hasMany(AgentVisitDepart::class, 'visit_id', 'id')
            ->where('status', DataStatus::REGULAR);
    }

    public function checkInGeo(): ?HasOne
    {
        if ($this->check_in_geo > 0) {
            return $this->hasOne(AgentVisitGeo::class, 'id', 'check_in_geo')
                ->where('status', DataStatus::REGULAR);
        } else {
            return null;
        }
    }

    public function checkOutGeo(): ?HasOne
    {
        if ($this->check_out_geo > 0) {
            return $this->hasOne(AgentVisitGeo::class, 'id', 'check_out_geo')
                ->where('status', DataStatus::REGULAR);
        } else {
            return null;
        }
    }

    public function hospital(): HasOne
    {
        return $this->hasOne(YouyaoHospital::class, 'id', 'hospital_id')
            ->where('status', DataStatus::REGULAR);
    }

    /**
     * 是否补签
     * @return bool
     */
    public function isSupplement(): bool
    {
        return $this->is_supplement == 1;
    }
}
