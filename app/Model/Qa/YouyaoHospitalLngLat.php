<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;
/**
 * @property int $id 
 * @property int $hospital_id 
 * @property string $addr 
 * @property string $lat 
 * @property string $lng 
 * @property float $distance 范围多少米
 * @property string $status 状态:0=禁用,1=正常
 * @property int $admin_id 
 * @property int $createtime 
 * @property int $updatetime 
 * @property int $deletetime 
 */
class YouyaoHospitalLngLat extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_youyao_hospital_lng_lat';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'hospital_id' => 'integer', 'distance' => 'float', 'admin_id' => 'integer', 'createtime' => 'integer', 'updatetime' => 'integer', 'deletetime' => 'integer'];
}