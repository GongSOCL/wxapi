<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id 
 * @property int $group_department_id 群组科室id
 * @property int $department_id 正常科室id
 * @property int $user_id 代表id
 * @property int $new_doctor_id 医生库nid
 * @property int $status 状态  0表示已删除  1表示正常
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 */
class NewGroupDepartMember extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_group_depart_member';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'group_department_id' => 'integer', 'department_id' => 'integer', 'user_id' => 'integer', 'new_doctor_id' => 'integer', 'status' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}