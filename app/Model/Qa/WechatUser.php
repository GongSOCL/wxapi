<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Qa\Users;
use App\Model\Model;

/**
 * @property int $id
 * @property string $u_yyid
 * @property string $openid
 * @property string $unionid
 * @property string $nickname 微信昵称
 * @property string $headimgurl 头像
 * @property int $sex 性别
 * @property string $country 国家
 * @property string $province 省份
 * @property string $city 城市
 * @property string $subscribe
 * @property string $subscribe_time
 * @property string $remark
 * @property string $subscribe_scene
 * @property string $qr_scene
 * @property string $qr_scene_str
 * @property int    $wechat_type
 * @property \Carbon\Carbon $created_at
 *
 * @property Users|null  $user
 */
class WechatUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wechat_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'sex' => 'integer', 'created_at' => 'datetime'];

    const UPDATED_AT = '';

    public $timestamps = false;

    /**
     * @return \Hyperf\Database\Model\Relations\HasOne
     */
    public function user(): \Hyperf\Database\Model\Relations\HasOne
    {
        return $this->hasOne(Users::class, 'yyid', 'u_yyid')
            ->where('v_status', 1);
    }
}
