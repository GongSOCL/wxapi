<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $type 1-公司供应商 2-个人供应商
 * @property string $title 供应商名称
 * @property int $company_id 公司id
 * @property string $admin_name 公司供应商管理员姓名或者个人供应商姓名
 * @property string $admin_phone 公司供应商管理员手机或者个人供应商手机
 * @property string $hospital 医院
 * @property string $product 药品 t_drug_series表id
 * @property string $area 区域
 * @property string $start_time 协议开始时间
 * @property string $end_time 协议结束时间
 * @property string $pdf_url 协议上传地址
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class Supplier extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_supplier';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'company_id' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
