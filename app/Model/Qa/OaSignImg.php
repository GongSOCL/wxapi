<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $sign_group_id 合同组id
 * @property int $user_id 代表id
 * @property int $img_type 签名图片类型, 1代表姓名
 * @property string $img_link 签名图片base64数据文件链接
 * @property string $file_hash 签名文件链接md5值
 * @property string $createtime
 * @property string $updatetime
 * @property string $deletetime
 */
class OaSignImg extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oa_sign_img';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sign_group_id' => 'integer',
        'user_id' => 'integer',
        'img_type' => 'integer'
    ];

    const CREATED_AT = 'createtime';
    const UPDATED_AT = 'updatetime';

    //签名图片
    const TYPE_NAME = 1;
}
