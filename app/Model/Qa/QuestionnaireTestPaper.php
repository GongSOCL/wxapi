<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $questionnaire_paper_id 问卷id
 * @property int $test_paper_id 试卷id,关联t_compliance_test_paper
 * @property int $status 记录状态: 1有效 0已删除
 * @property string $created_time
 * @property string $modify_time
 */
class QuestionnaireTestPaper extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_questionnaire_test_papers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'questionnaire_paper_id' => 'integer',
        'test_paper_id' => 'integer',
        'status' => 'integer'
    ];
}
