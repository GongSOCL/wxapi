<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $activity_id 活动id
 * @property string $name
 * @property string $event_key
 * @property int $s_type 特例的类型 1 表示地域 2表示 角色  3表示产品  4表示医院  5表示特定数据
 * @property string $s_value 特例的值
 * @property string $s_table_name 对应数据库时表的名称i当且仅当特定数值时有效
 * @property string $start_time 事件的开始时间
 * @property string $end_time 事件的结束时间，空为无限
 * @property float $money_point 消费积分
 * @property float $service_point 服务积分
 * @property int $num_pre_day 每人每天可获取的次数
 * @property int $num_total 每人可获取的总次数
 * @property string $point_desc 积分获取说明
 * @property string $created_user_id 创建者的编号
 * @property int $status 状态 0表示未发布    1表示发布
 * @property string $created_time 数据创建时间
 * @property string $modify_time
 */
class ActivitySpecial extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_activity_special';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'activity_id' => 'integer',
        's_type' => 'integer',
        'money_point' => 'float',
        'service_point' => 'float',
        'num_pre_day' => 'integer',
        'num_total' => 'integer',
        'status' => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
