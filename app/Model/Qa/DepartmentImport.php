<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/sdc-crawler.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $company_id 公司id
 * @property int $import_type 导入类型： 1钉钉 2 excel
 * @property int $import_state 导入状态: 1新建 2 导入成功 3导入失
 * @property int $is_active 是否激活: 默认0
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class DepartmentImport extends Model
{
    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    //钉钉导入
    const IMPORT_TYPE_DINGTALK = 1;

    //excel导入
    const IMPORT_TYPE_EXCEL = 2;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_department_import';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'company_id', 'import_type', 'created_at', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_id' => 'integer',
        'import_type' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    const IMPORT_STATE_NEW = 1;
    const IMPORT_STATE_SUCCESS = 2;
    const IMPORT_STATE_FAIL = 3;

    public function isImportFinished(): bool
    {
        return in_array($this->import_state, [self::IMPORT_STATE_SUCCESS, self::IMPORT_STATE_FAIL]);
    }

    public function isImportByDingtalk(): bool
    {
        return $this->import_type == self::IMPORT_TYPE_DINGTALK;
    }

    public function isImportByExcel(): bool
    {
        return $this->import_type == self::IMPORT_TYPE_EXCEL;
    }
}
