<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property float $longitude 经度
 * @property float $latitude 纬度
 * @property float $amap_longitude 高德经度
 * @property float $amap_latitude 高德纬度
 * @property string $origin_coordinate_type 原始坐标类型
 * @property string $sign_pos 签到位置，由经纬度转换后的位置
 * @property int $status 状态: 0删除 1正确
 * @property string $created_time
 * @property string $modify_time
 * @property int $is_fake_geo 是否随机生成gps坐标
 */
class AgentVisitGeo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_agent_visit_geo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'longitude',
        'latitude',
        'amap_longitude',
        'amap_latitude',
        'sign_pos',
        'status',
        'created_time',
        'modify_time'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'             => 'integer',
        'longitude'      => 'float',
        'latitude'       => 'float',
        'amap_longitude' => 'float',
        'amap_latitude'  => 'float',
        'status'         => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
