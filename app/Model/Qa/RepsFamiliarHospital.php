<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $rep_yyid
 * @property string $hospital_yyid
 * @property int $status 1表示正常 0表示删除
 * @property string $created_time
 * @property string $modify_time
 */
class RepsFamiliarHospital extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_familiar_hospitals';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
