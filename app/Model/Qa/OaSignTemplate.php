<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $templatename 模板名称
 * @property string $desc 模板描述
 * @property string $tpl_alias 合同模板别名
 * @property string $link 模板链接
 * @property int $admin_id
 * @property string $content 模板参数说明
 * @property int $createtime
 * @property int $updatetime
 * @property int $deletetime
 */
class OaSignTemplate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oa_sign_template';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'admin_id' => 'integer',
        'createtime' => 'integer',
        'updatetime' => 'integer',
        'deletetime' => 'integer'
    ];
}
