<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $openid
 * @property string $nickname 微信昵称
 * @property string $headimgurl 头像
 * @property string $language
 * @property int $sex 性别
 * @property string $country 国家
 * @property string $province 省份
 * @property string $city 城市
 * @property string $privilege
 * @property string $scope
 * @property string $redirct_url
 * @property string $data_md5
 * @property string $created_time
 */
class WechatUserSnsapi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wechat_user_snsapi';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'sex' => 'integer'];

    public $timestamps = false;

    const CREATED_AT =  'created_time';
}
