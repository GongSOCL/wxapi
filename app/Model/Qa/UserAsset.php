<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $uid 用户id
 * @property float $money_point 消费积分
 * @property float $service_point 服务积分
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class UserAsset extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_user_assets';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'uid' => 'integer',
        'money_point' => 'float',
        'service_point' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
