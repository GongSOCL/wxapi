<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $company_yyid 公司yyid 对应t_company和t_user的company_yyid
 * @property string $dealer_yyid 供应商yyid 对应t_dealer的yyid
 * @property string $hospital_no 供应商提供的医院编号
 * @property string $hospital_name_cn 供应商提供的医院名称
 * @property string $hospital_name_en 供应商提供的医院名称
 * @property string $hospital_name_complete 供应商提供的完整医院名称
 * @property string $hospital_yyid
 * @property string $tier 等级（一级 二级 三级 未评级）
 * @property string $tier_en 等级（T1 T2 T3 Undefined）
 * @property string $class
 * @property string $class_en
 * @property string $type 类型 医院、药店、诊所等
 * @property string $type_en 类型英文 Hospital
 * @property string $ownership
 * @property string $ownership_en
 * @property string $city_id 城市id
 * @property string $postalcode
 * @property string $noofbeds
 * @property string $tel_no 手机号
 * @property string $address 地址
 * @property string $comments
 * @property int $status 2表示禁用 1 表示通过
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerHospitalC extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_hospital_c';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'company_yyid',
        'dealer_yyid',
        'hospital_no',
        'hospital_name_cn',
        'hospital_name_en',
        'hospital_name_complete',
        'hospital_yyid',
        'tier',
        'tier_en',
        'class',
        'class_en',
        'type',
        'type_en',
        'ownership',
        'ownership_en',
        'city_id',
        'postalcode',
        'noofbeds',
        'tel_no',
        'address',
        'comments',
        'status',
        'created_time',
        'modify_time'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    /**
     * 这个是个不完整的类型
     * @return int
     */
    public function getHospitalType(): int
    {
        if ($this->type && trim($this->type) == "医院") {
            return 1;
        } else {
            return 2;
        }
    }
}
