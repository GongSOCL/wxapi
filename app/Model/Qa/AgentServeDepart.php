<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $user_id 代表id
 * @property int $hospital_id 医院id
 * @property int $depart_id 科室id
 * @property int $status 状态: 0删除 1正确
 * @property string $created_time
 * @property string $modify_time
 */
class AgentServeDepart extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_agent_serve_depart';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'hospital_id', 'depart_id', 'status', 'created_time', 'modify_time'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'user_id'     => 'integer',
        'hospital_id' => 'integer',
        'depart_id'   => 'integer',
        'status'      => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
