<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $leader_id 组长id
 * @property string $group_name 分组的名称
 * @property int $group_type 分组的类 型1表示 代表   2表示医生分组
 * @property string $notice 群公告
 * @property int $created_id 创建者id
 * @property int $series_id 产品系列的名称
 * @property float $tax_point
 * @property int $status 状态 0表示删除  1表示正常
 * @property \Carbon\Carbon $created_at
 * @property string $modify_at
 */
class NewGroup extends Model
{
    const WORK = 1;       //工作群
    const DOCTOR = 2;     //医生群

    const DELETE = 0;        //已删除
    const NORMAL = 1;        //正常

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_group';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'leader_id' => 'integer',
        'group_type' => 'integer',
        'created_id' => 'integer',
        'series_id' => 'integer',
        'tax_point' => 'float',
        'status' => 'integer',
        'created_at' => 'datetime'
    ];
}
