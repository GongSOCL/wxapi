<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $hospital_yyid 医院编号 对应t_hospital表的id字段
 * @property string $product_yyid 产品编号
 * @property string $month 年月 例如 201706
 * @property int $num 医药代表按医院的当月销量
 * @property string $created_time 数据的创建时间
 */
class HospitalMonthTransP extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_hospital_month_trans_p';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'num' => 'integer'];
}
