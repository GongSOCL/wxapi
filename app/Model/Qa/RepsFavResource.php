<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $fav_yyid
 * @property string $resource_yyid 资源编号
 * @property int $resource_type 1 表示视频  2 表示 文献  3表示公告 4 直播视频  9最新解读
 * @property int $status 1 表示正常 0表示逻辑删除
 * @property string $created_time
 * @property string $modify_time
 */
class RepsFavResource extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_fav_resource';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'resource_type' => 'integer', 'status' => 'integer'];
}
