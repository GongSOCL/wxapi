<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $hospital_yyid 医院yyid
 * @property string $company_yyid 公司id
 * @property string $dealer_yyid
 * @property string $hospital_no 供应商提供的医院编号
 * @property string $hospital_name_cn 供应商提供的医院名称
 * @property string $tier 等级（一级 二级 三级 未评级）
 * @property string $province 省
 * @property string $city 市
 * @property float $longitude 经度
 * @property float $latitude 纬度
 * @property float $range 范围
 * @property string $excel_file_name 上传文件名
 * @property int $status 2表示删除 1 表示通过
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerHospitalGps extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_hospital_gps';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'yyid',
        'dealer_yyid',
        'hospital_no',
        'hospital_name_cn',
        'tier',
        'province',
        'city',
        'longitude',
        'latitude',
        'range',
        'status',
        'created_time',
        'modify_time'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'longitude' => 'float',
        'latitude' => 'float',
        'range' => 'float',
        'status' => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
