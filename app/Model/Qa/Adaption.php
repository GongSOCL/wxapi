<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $name 领域名称
 * @property string $name_en 领域名称英文
 * @property int $parent_id 父领域id
 * @property int $status 状态: 0删除 1正确
 * @property string $created_time
 * @property string $modify_time
 */
class Adaption extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_adaption';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'parent_id' => 'integer', 'status' => 'integer'];

    const CREATED_AT =  'created_time';
    const UPDATED_AT = 'modify_time';
}
