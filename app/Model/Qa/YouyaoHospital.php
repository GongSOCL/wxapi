<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $company_yyid 公司对应的yyid
 * @property string $pro 省
 * @property string $pro_yyid 省的编号
 * @property string $city 市或区
 * @property string $city_yyid 市或区编号
 * @property string $hospital_name 医院名称
 * @property string $alias_name 医院的别名
 * @property string $pinyin 拼音
 * @property string $short_pinyin 拼音的首字母
 * @property string $hospital_md5 医院名称的唯一校验码
 * @property string $addr 地址
 * @property string $tel 电话
 * @property string $level 等级
 * @property string $org_code 组织机构码
 * @property int $org_type 1 表格医院  2表示药店   3表示经销商   org_code  0 表示抓取过 1 表示有结果的 2表示确认过的 3 表示跳过
 * @property string $industry
 * @property string $p_department 重要科室
 * @property string $method
 * @property string $fax 传真
 * @property string $email 邮箱
 * @property string $url 网站地址
 * @property string $kn_department 我熟悉的科室  到时要拆分出来  一个医药代表一个熟悉的
 * @property string $hos_dean 主管院长
 * @property string $annual_income 医院年收入
 * @property string $use_rate 药占比
 * @property int $bed_num 病床数量
 * @property int $operation_num 年手术量
 * @property int $outpatient_num 年门诊量
 * @property string $create_user_id 数据的创建者编号
 * @property int $pid 关联的医院的编号
 * @property string $old_hospital_name 医院名称更新之前的名称
 * @property string $old_alias_name
 * @property string $old_addr
 * @property int $status 状态 1表示 正常  0 表示禁用  -1 表示逻辑删除 2表示申请中 3 表示申请不通过
 * @property string $created_time
 * @property string $modify_time
 */
class YouyaoHospital extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_youyao_hospital';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'org_type' => 'integer',
        'bed_num' => 'integer',
        'operation_num' => 'integer',
        'outpatient_num' => 'integer',
        'pid' => 'integer',
        'status' => 'integer'
    ];

    const HOSPITAL_NORMAL = 1;
    const HOSPITAL_FORBID = 0;
    const HOSPITAL_DELETED = -1;
    const HOSPITAL_APPLYING = 2;
    const HOSPITAL_REFUSE = 3;

    // 组织类型是医院
    const ORG_TYPE_HOSPITAL = 1;
    // 组织类型是药店
    const ORG_TYPE_DRUGSTORE = 2;

    //医院等级
    const HOSPITAL_LEVEL_THREE = 3;
    const HOSPITAL_LEVEL_TWO = 2;
    const HOSPITAL_LEVEL_ONE = 1;
    const HOSPITAL_LEVEL_UNKNOWN = 0;

    //进药状态
    const HOSPITAL_TRANSACTION_YES = 1;
    const HOSPITAL_TRANSACTION_NO =  2;

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    public function isOrgHospital(): bool
    {
        return $this->org_type == self::ORG_TYPE_HOSPITAL;
    }

    public static function getAllLevels(): array
    {
        return [
            YouyaoHospital::HOSPITAL_LEVEL_UNKNOWN,
            YouyaoHospital::HOSPITAL_LEVEL_ONE,
            YouyaoHospital::HOSPITAL_LEVEL_TWO,
            YouyaoHospital::HOSPITAL_LEVEL_THREE
        ];
    }
}
