<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $plan_id 计划id
 * @property int $user_id 用户id
 * @property string $one_day 计划开始当天短信
 * @property string $last_day 计划最后一天
 * @property string $node_one 节点1
 * @property string $node_two 节点2
 * @property string $exam_start 考试开始时间-需要带计划id（为手动开始考试记录时间）
 * @property int $status 状态 1：正常，0：禁用
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class LearningPlanMsgNode extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'learning_plan_msg_node';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'plan_id' => 'integer',
        'user_id' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
