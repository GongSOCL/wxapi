<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $department_id 梦想科室id
 * @property int $type_id 梦想类型id
 * @property string $title 标题
 * @property string $img 图片
 * @property int $admin_id 管理员编号
 * @property int $status 状态 1：正常，2：禁用
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class MorningShare extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_morning_share';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'department_id' => 'integer',
        'type_id' => 'integer',
        'admin_id' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
