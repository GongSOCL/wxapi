<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $nid
 * @property string $openid 微信的openid
 * @property string $unionid 微信的unionid
 * @property string $nickname 微信昵称
 * @property string $headimgurl 微信头像
 * @property string $true_name 真实姓名
 * @property int $gender 1表示男 2表示女
 * @property string $mobile_num 手机号码
 * @property string $hospital_yyid 医院编号
 * @property string $hospital_name 医院名称
 * @property int $depart_id 科室id
 * @property string $depart_name 科室名称
 * @property string $job_title 职称
 * @property string $position 职务
 * @property int $field_id 擅长领域id
 * @property int $clinic_type 门诊类型 0：未知，1：专家，2：普通
 * @property string $clinic_rota 门诊值班情况 json 数据
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class NewDoctorUserinfo extends Model
{
    public $primaryKey = 'nid';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_doctor_userinfo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'gender' => 'integer',
        'depart_id' => 'integer',
        'field_id' => 'integer',
        'clinic_type' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
