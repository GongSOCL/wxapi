<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $user_id 用户编号
 * @property string $title 合同标题，默认从模板那边继承过来
 * @property int $group_id 组id,关联oa_sign_group表
 * @property string $contractno 合同编号
 * @property string $startdate 合同开始时间,承继自合同组
 * @property string $enddate 合同结束时间,继承自合同组
 * @property string $type 合同的类型:slaver=附件合同,master=主合同,other=其他文件
 * @property int $template_id 模板编号
 * @property int $admin_id
 * @property int $company_id 公司编号
 * @property string $link 生成用户合同html link,不带签名
 * @property string $pdf_link 用户签署pdf link
 * @property string $signdate 签约时间
 * @property int $state 合同状态: 1新建待生成合同html 2已生成html待签署 3已签署 待生成pdf 4已生成pdf
 * @property int $createtime
 * @property int $updatetime
 * @property int $deletetime
 */
class OaUserSign extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oa_user_sign';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'group_id' => 'integer',
        'template_id' => 'integer',
        'admin_id' => 'integer',
        'company_id' => 'integer',
        'state' => 'integer',
        'createtime' => 'integer',
        'updatetime' => 'integer',
        'deletetime' => 'integer'
    ];

    public $timestamps = false;

    //合同状态: 1新建待生成合同html 2已生成html待签署 3已签署 待生成pdf 4已生成pdf
    const STATE_TO_GENERATE_HTML = 1;
    const STATE_TO_SIGN = 2;
    const STATE_SIGNED = 3;
    const STATE_PDF_GENERATED = 4;

    const TYPE_MASTER = "master";
    const TYPE_SLAVE = "slaver";
    const TYPE_OTHER = "other";

    public function isValidToGenerateHtmlTpl(): bool
    {
        return $this->state == self::STATE_TO_GENERATE_HTML;
    }

    public function isValidToSign(): bool
    {
        return $this->state == self::STATE_TO_SIGN;
    }

    public function isSigned(): bool
    {
        return $this->state == self::STATE_SIGNED;
    }

    public function isPdfGenerated(): bool
    {
        return $this->state == self::STATE_PDF_GENERATED;
    }

    public function template()
    {
    }
}
