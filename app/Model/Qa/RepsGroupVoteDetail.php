<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $group_yyid 分组的名称
 * @property string $vote_yyid 投票的编号
 * @property string $user_yyid 目前的贡献积分
 * @property int $status 1表示赞同 2表示不赞同
 * @property string $created_time
 * @property string $modify_time
 */
class RepsGroupVoteDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_repsgroup_vote_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];

    const STATUS_AGREE = 1;
    const STATUS_DISAGREE = 2;

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
