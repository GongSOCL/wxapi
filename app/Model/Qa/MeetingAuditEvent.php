<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $meeting_id 会议id
 * @property int $user_id 触发变更人员id
 * @property int $platform 人员所属平台: 1wxapi 2admin
 * @property int $event_type 事件类型: 1新建会议 2审核成功 3审核失败 4审核失败再次修改提交
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class MeetingAuditEvent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_meeting_audit_event';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'meeting_id', 'user_id', 'platform', 'event_type', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'meeting_id' => 'integer',
        'user_id' => 'integer',
        'platform' => 'integer',
        'event_type' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    const EVENT_TYPE_NEW = 1;
    const EVENT_TYPE_AUDIT_SUCCESS = 2;
    const EVENT_TYPE_AUDIT_FAILED = 3;
    const EVENT_TYPE_CANCEL = 4;
    const EVENT_TYPE_RE_NEW = 5;

    const PLATFORM_WXAPI = 1;
    const PLATFORM_ADMIN = 2;
}
