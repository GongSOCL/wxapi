<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id 
 * @property string $company_yyid 公司yyid 对应t_company和t_user的company_yyid
 * @property string $month 月份
 * @property string $staff_code 员工编号
 * @property int $user_id 用户id
 * @property string $department 部门
 * @property string $targethospitals 目标机构数量
 * @property string $targetcustoms 目标客户数量
 * @property string $importantdeptcustoms 重点科室的客户数
 * @property string $qrcodeactive Qrcode激活数
 * @property string $qrcodeactivepercentage Qrcode激活占比
 * @property string $hospitalpercentage 客户>0的医院比例
 * @property string $callcustoms 已拜访客户数量
 * @property string $custompercentage 客户覆盖率
 * @property string $callfrequency 客户拜访频率
 * @property int $status 2表示删除 1 表示通过 
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class TDealerDoctorVisitDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_doctor_visit_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'user_id' => 'integer', 'status' => 'integer'];
}