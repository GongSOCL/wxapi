<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $group_yyid 分组的名称
 * @property string $leader_yyid 发起者编号
 * @property float $from_tax 目前的贡献积分
 * @property float $to_tax 目标的贡献积分
 * @property string $start_time
 * @property string $end_time
 * @property int $status 0表示进行中  1表示已通过 2表示未通过  3表示已过期  4表示手动关闭 一般投票期为7天
 * @property string $created_time
 * @property string $modify_time
 */
class RepsGroupVote extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_repsgroup_vote';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'from_tax' => 'float', 'to_tax' => 'float', 'status' => 'integer'];

    const STATUS_PROCESSING = 0; // 进行中
    const STATUS_PASSED = 1; // 已通过
    const STATUS_FAILED = 2; // 未通过
    const STATUS_EXPIRED = 3; // 已过期
    const STATUS_CLOSED = 4; // 手动关闭

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';
}
