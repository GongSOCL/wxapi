<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $openid 微信openid
 * @property int $uid 用户id
 * @property int $activity_id t_activity 表 id
 * @property int $goods_id exchange_goods id
 * @property int $goods_num 商品数量
 * @property float $point 积分
 * @property int $type 类型 1：发放，2：使用
 * @property int $status 状态 1：申请中，2：已到账，3：已兑换
 * @property string $desc 说明
 * @property \Carbon\Carbon $created_at 创建时间
 */
class ShoppingPointsDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_user_money_point_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'uid' => 'integer',
        'activity_id' => 'integer',
        'goods_id' => 'integer',
        'goods_num' => 'integer',
        'point' => 'float',
        'type' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime'
    ];

    //追加字段
    protected $appends = ['year','month'];

    //防问器获取月份和年份
    public function getYearAttribute($value)
    {
        return explode('-', $this->attributes['date'])[0];
    }

    public function getMonthAttribute($value)
    {
        return explode('-', $this->attributes['date'])[1];
    }

    const ACTIVITY_ID_REGISTER = 1; //注册行为
    const ACTIVITY_ID_EXTENSION = 2; //推广行为
    const ACTIVITY_ID_EXCHANGE = 3; //兑换行为（数据库未定，暂时使用）

    const TYPE_GET_POINTS = 1;  //发放积分
    const TYPE_USE_POINTS = 2;  //消耗积分

    const STATUS_APPLITYING = 1; //申请中
    const STATUS_ALREADY_RECEIVED = 2; //已到账
    const STATUS_ALREADY_EXCHANGE = 3; //已兑换
}
