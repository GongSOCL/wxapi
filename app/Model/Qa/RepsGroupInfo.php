<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $group_yyid 对应t_group的yyid
 * @property string $notice 群公告
 * @property string $leader_yyid 群组的用户编号对应user_yyid
 * @property string $series_yyid 产品系列的名称
 * @property float $tax_point
 * @property int $status
 * @property string $created_time
 * @property string $modify_time
 */
class RepsGroupInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_repsgroup_info';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'tax_point' => 'float', 'status' => 'integer'];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    const STATUS_DELETE = 0;        //已删除
    const STATUS_NORMAL = 1;        //正常
}
