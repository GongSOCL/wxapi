<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $user_id 代表id
 * @property int $new_doctor_id new_doctor_openid_userinfo表id
 * @property int $status 0表示删除   1表示正常   2待加入
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class NewDoctorOpenidUserinfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'new_doctor_openid_userinfo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'new_doctor_id' => 'integer',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
