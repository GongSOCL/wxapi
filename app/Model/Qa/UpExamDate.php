<?php

declare (strict_types=1);
namespace App\Model\Qa;

/**
 * @property int $id
 * @property int $exam_id 对应考试id
 * @property string $up_exam_start 考试开始时间
 * @property string $up_exam_end 考试结束时间
 * @property int $status 0-删除，1-有效
 * @property int $is_deleted 0未删除1已删除
 * @property int $created_id
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
use App\Model\Model;

class UpExamDate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'up_exam_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'exam_id' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
