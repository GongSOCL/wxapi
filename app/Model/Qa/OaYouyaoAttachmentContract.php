<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $sign_id 合同主表id,关联oa_user_sign
 * @property string $startdate 合同开始时间
 * @property string $enddate 合同结束时间
 * @property string $settlement_date 结算日期
 * @property int $percent 达成率指标
 * @property string $fee_unit 单盒咨询费
 * @property string $createtime
 * @property string $updatetime
 * @property string $deletetime
 */
class OaYouyaoAttachmentContract extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oa_youyao_attachment_contract';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'sign_id',
        'startdate',
        'enddate',
        'percent',
        'createtime',
        'updatetime',
        'deletetime'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'sign_id' => 'integer', 'percent' => 'integer'];

    const CREATED_AT = 'createtime';
    const UPDATED_AT = 'updatetime';
}
