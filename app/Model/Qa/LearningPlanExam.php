<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $plan_id 计划id
 * @property int $exam_id 用户id
 * @property int $status 状态 1：正常，0：禁用
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class LearningPlanExam extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'learning_plan_exams';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'plan_id' => 'integer',
        'exam_id' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
