<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $name 名称
 * @property string $h_yyid hospital uuid, 所属医院
 * @property int $rank 排序
 * @property int $status 是否显示。1为显示，0为隐藏
 * @property string $spider_url 数据来源
 * @property string $hospital_url parent url
 * @property string $intro 科室介绍
 * @property int $created Timestamp for when record was created.
 * @property string $update_date
 */
class Department extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_department';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'rank' => 'integer', 'status' => 'integer', 'created' => 'integer'];

    const   DEPARTMENT_SHOW = 1; //显示
    const   DEPARTMENT_HIDE = 0; //隐藏
}
