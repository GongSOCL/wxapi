<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $user_yyid 用户的编号  t_users yyid
 * @property string $msg_yyid 消息编号的编号  对应 t_msg yyid
 * @property int $member_id group_member 表的 id
 * @property int $is_read 是否已读的编号
 * @property string $read_time 读取的时间
 * @property int $is_deleted 是否删除
 * @property string $deleted_time 删除时间
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class RepsMsg extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_reps_msg';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'member_id' => 'integer', 'is_read' => 'integer', 'is_deleted' => 'integer'];


    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    const STALL_NOT_READ = 0; //该信息未读

    const IS_DELETED = 1; //已删除
    const NOT_DELETED = 0; //未删除
}
