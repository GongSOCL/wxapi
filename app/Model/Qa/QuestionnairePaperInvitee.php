<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;
use App\Model\Qa\Users;

/**
 * @property int $id
 * @property int $paper_id 问卷id
 * @property int $invitee_id 受邀请人员id
 * @property int $invitee_type 受邀请人员类型: 1代表 2医生
 * @property int $invite_quota 分配额度
 * @property int $invited 已用额度
 * @property int $status 是否有效
 * @property string $created_time
 * @property string $modify_time
 */
class QuestionnairePaperInvitee extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_questionnaire_paper_invitee';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    //invitee_type受邀请者类型
    const INVITEE_TYPE_AGENT = 1;
    const INVITEE_TYPE_DOCTOR = 2;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'paper_id' => 'integer',
        'invitee_id' => 'integer',
        'invitee_type' => 'integer',
        'invite_quota' => 'integer',
        'invited' => 'integer',
        'status' => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    /**
     * 问卷模型
     * @return \Hyperf\Database\Model\Relations\BelongsTo
     */
    public function paper()
    {
        return $this->belongsTo(QuestionnairePaper::class, 'paper_id', 'id');
    }

    /**
     * 邀请者模型
     * @return \Hyperf\Database\Model\Relations\BelongsTo
     */
    public function invitee()
    {
        return $this->belongsTo(Users::class, 'invitee_id', 'uid');
    }
}
