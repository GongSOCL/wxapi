<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $supplier_id 供应商id
 * @property string $service_type 服务类型
 * @property string $title 服务项目名称
 * @property string $unit 单位
 * @property string $money_unit 金额单位
 * @property string $price 单价
 * @property string $standard 服务类型id
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class SupplierDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_supplier_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'supplier_id' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
