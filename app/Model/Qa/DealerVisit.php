<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $dealer_yyid
 * @property string $month 月份
 * @property string $staff_code 工号
 * @property string $staff_name 姓名
 * @property string $product_id 产品id
 * @property string $product_name_cn 产品名称
 * @property string $ins_class 机构级别
 * @property string $of_tgt_ins 目标机构数量
 * @property string $of_ins_visited 拜访机构数量
 * @property string $coverage 拜访率
 * @property string $of_calls 拜访数量
 * @property string $frequency 拜访频率
 * @property int $status 2表示删除 1 表示通过
 * @property string $created_time 数据的创建时间
 * @property string $modify_time 数据的修改时间
 */
class DealerVisit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_dealer_visit';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer'];
}
