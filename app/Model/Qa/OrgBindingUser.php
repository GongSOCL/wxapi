<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $company_id 公司id
 * @property string $ding_user_id 钉钉用户id
 * @property string $bind_mobile 绑定帐户手机号
 * @property int $bind_user_id 计算出的用户id
 * @property int $import_id 导入id
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class OrgBindingUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_org_binding_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'company_id',
        'ding_user_id',
        'bind_mobile',
        'bind_user_id',
        'import_id',
        'created_at',
        'updated_at']
    ;
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_id' => 'integer',
        'bind_user_id' => 'integer',
        'import_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
