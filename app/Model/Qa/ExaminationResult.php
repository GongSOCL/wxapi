<?php

declare (strict_types=1);
namespace App\Model\Qa;

/**
 * @property int $id
 * @property int $exam_id 对应考试id
 * @property string $user_yyid 用户yyid
 * @property string $city 城市
 * @property string $username 用户名
 * @property string $code 员工code
 * @property string $area 所在地区
 * @property string $bu BU
 * @property string $join_time 入职时间
 * @property string $position 职位
 * @property string $phone 手机号
 * @property int $fraction 考试分数 有分数通过规则为按分数 空即是答对N题
 * @property int $up_exam 是否补考【当status=3】【123分别对应补考次数 目前只有一次】 10倍为补考状态，例：11补考通过 12 未通过
 * @property int $status 状态 1正常，2通过，3补考
 * @property int $is_deleted 0未删除1已删除
 * @property int $created_id
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
use App\Model\Model;

class ExaminationResult extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'examination_results';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'exam_id' => 'integer',
        'fraction' => 'integer',
        'up_exam' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
