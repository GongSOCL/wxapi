<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property int $supplier_id 供应商id
 * @property int $hospital_id 终端id
 * @property int $product_id 产品id t_drug_serise表id
 * @property int $is_occupy 0-未被使用 1-被使用
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class SupplierHospitalProduct extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_supplier_hospital_product';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'supplier_id' => 'integer',
        'hospital_id' => 'integer',
        'product_id' => 'integer',
        'is_occupy' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
