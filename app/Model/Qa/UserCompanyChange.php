<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;
/**
 * @property int $id 
 * @property int $user_id 代表id
 * @property int $before_company_id 变更前公司id
 * @property int $after_company_id 变更后公司id
 * @property int $operator_id 操作人id
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class UserCompanyChange extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_company_change';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'user_id' => 'integer', 'before_company_id' => 'integer', 'after_company_id' => 'integer', 'operator_id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}