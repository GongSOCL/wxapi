<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid UNIQUE Key: uuid
 * @property string $dealer_yyid 数据供应商的编号对应T_dealer表的IDp字段
 * @property string $batch_yyid 数据导入的批次号对应T_import_batch表的ID字段
 * @property string $batch_number 导入的文件里面 的序号
 * @property string $product_name 导入的文件里面 的产品名称
 * @property string $specs 导入的文件里面 的规格
 * @property int $num 导入的文件里面 的 数量
 * @property string $hospital_name 导入的文件里面 的医院名称
 * @property string $transaction_date 导入的文件里面 的交易日期
 * @property string $product_no 导入的文件里面 的产品ID
 * @property string $hospital_no 导入的文件里面 的医院ID
 * @property string $product_batch 导入的文件里面 的产品批次
 * @property string $created_time 数据的创建时间
 */
class ImportBatchList extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_import_batch_list';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'num' => 'integer'];
}
