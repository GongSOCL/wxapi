<?php

declare (strict_types=1);
namespace App\Model\Qa;

use App\Model\Model;

/**
 * @property int $id
 * @property string $yyid
 * @property string $user_yyid 代表的用户编号 对应t_user的yyid值
 * @property string $openid 微信的openid
 * @property int    $wechat_type 公众号类型: 1代表公众号 2医生公众号
 * @property string $nickname 微信昵称
 * @property string $headimgurl 微信头像
 * @property string $true_name 真实姓名
 * @property int $gender 1表示男 2表示女
 * @property string $mobile_num 手机号码
 * @property string $hospital_yyid 医院编号
 * @property string $hospital_name 医院名称
 * @property int $depart_id 科室id
 * @property string $depart_name 科室名称
 * @property string $job_title 职称
 * @property string $position 职务
 * @property int $field_id 擅长领域id
 * @property int $clinic_type 门诊类型 0：未知，1：专家，2：普通
 * @property string $clinic_rota 门诊值班情况 json 数据
 * @property int $status 0表示删除   1表示正常
 * @property string $created_time
 * @property string $modify_time
 */
class DoctorOpenidUserinfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_doctor_openid_userinfo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'gender' => 'integer',
        'depart_id' => 'integer',
        'field_id' => 'integer',
        'clinic_type' => 'integer',
        'status' => 'integer'
    ];

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modify_time';

    const STATUS_COMMON = 1;  //正常
    const STATUS_DELETED = 0; //删除
}
