<?php

declare (strict_types=1);
namespace App\Model\Core;

use Hyperf\DbConnection\Model\Model;
/**
 * @property int $id 
 * @property string $province 省份
 * @property string $area 大区
 * @property int $area_id 大区id
 * @property \Carbon\Carbon $created_at 更新时间
 */
class ProvinceArea extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'province_area';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'core';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'area_id' => 'integer', 'created_at' => 'datetime'];
}