<?php

declare (strict_types=1);
namespace App\Model\Core;

use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 直播表主键id
 * @property int $meeting_id 会议id
 * @property int $applicant_id 申请人id, 优药用户id
 * @property string $applicant_mobile 申请人手机号
 * @property int $manager_id 会议管理员id, 优佳医用户id
 * @property int $compere_id 会议主持人id, 优佳医用户id
 * @property int $live_type 项目类型
 * @property int $live_sub_type 项目名称
 * @property int $live_category 项目分类
 * @property string $title 直播标题
 * @property int $area_id 大区
 * @property int $province 省份
 * @property int $location 1-中央2-区域
 * @property int $hospital_id 医院id
 * @property string $hospital_no 医院编号
 * @property string $hospital 医院
 * @property int $depart 科室
 * @property int $product_id 药品id t_product表
 * @property string $start_date 开播月份
 * @property string $start_time 直播开始时间
 * @property string $end_time 直播结束时间
 * @property int $live_state 直播状态1直播中2未开始3已结束有回顾4已结束无回顾
 * @property string $pullflow_url 拉流地址
 * @property string $weizan_url 微赞观看地址
 * @property string $cover_img 直播封面
 * @property string $thumbnail_img 直播封面缩略图
 * @property string $running_text_up 跑马灯文字上方
 * @property string $running_text_down 跑马灯文字下方
 * @property int $signup_sheet 报名表id
 * @property string $approval_no 审批编号
 * @property int $allow_comment 1可以评论2关闭评论
 * @property int $allow_share 1可以分享2关闭分享
 * @property string $share_title 分享标题
 * @property string $share_desc 分享简介
 * @property string $share_img 分享封面
 * @property int $allow_review 1打开直播回顾2关闭直播回顾
 * @property string $review_url 回顾地址
 * @property int $like 点赞
 * @property int $collected 收藏
 * @property int $comment 评论
 * @property int $share 分享
 * @property int $signup 报名数
 * @property int $subscribe 订阅数
 * @property int $read_init 阅读/观看数量初始化数值
 * @property int $read_step_count 阶段观看数
 * @property int $read_pv_multiple pv倍数
 * @property int $read_uv 真实阅读/观看数量
 * @property int $read_step_uv 当前倍数观看数
 * @property int $rank 排序 越大越靠前
 * @property int $is_recommend 1默认推荐 2默认不推荐
 * @property int $is_top 0不置顶1置顶
 * @property int $is_available 观看需要登录0不需要1需要
 * @property int $is_out 是否为外部直播 1内部 2外部
 * @property string $platform 0默认全部1代表公众号2代表APP3医生公众号4医生APP
 * @property int $status 1上架2下架
 * @property int $is_deleted 0未删除1已删除
 * @property \Carbon\Carbon $created_at 创建时间
 * @property \Carbon\Carbon $updated_at 更新时间
 */
class Live extends Model
{
    const LIVE_STATE_1 = 1;
    const LIVE_STATE_2 = 2;
    const LIVE_STATE_3 = 3;
    const LIVE_STATE_4 = 4;

    const PURE_ONLINE = 1;
    const PURE_OFFLINE = 2;
    const HALF_ONLINE = 3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'live';
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'core';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'meeting_id' => 'integer',
        'applicant_id' => 'integer',
        'manager_id' => 'integer',
        'compere_id' => 'integer',
        'live_type' => 'integer',
        'live_sub_type' => 'integer',
        'live_category' => 'integer',
        'area_id' => 'integer',
        'province' => 'integer',
        'location' => 'integer',
        'hospital_id' => 'integer',
        'depart' => 'integer',
        'product_id' => 'integer',
        'live_state' => 'integer',
        'signup_sheet' => 'integer',
        'allow_comment' => 'integer',
        'allow_share' => 'integer',
        'allow_review' => 'integer',
        'like' => 'integer',
        'collected' => 'integer',
        'comment' => 'integer',
        'share' => 'integer',
        'signup' => 'integer',
        'subscribe' => 'integer',
        'read_init' => 'integer',
        'read_step_count' => 'integer',
        'read_pv_multiple' => 'integer',
        'read_uv' => 'integer',
        'read_step_uv' => 'integer',
        'rank' => 'integer',
        'is_recommend' => 'integer',
        'is_top' => 'integer',
        'is_available' => 'integer',
        'is_out' => 'integer',
        'status' => 'integer',
        'is_deleted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
