<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Middleware;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\Jwt;
use App\Repository\UsersRepository;
use App\Service\User\UserService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Arr;
use Hyperf\Utils\Context;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Youyao\Framework\ErrCode;
use Hyperf\Di\Annotation\Inject;

class AppAuthMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @Inject
     * @var Jwt
     */
    protected $jwt;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $log = Helper::getLogger();
        $req = $this->container->get(RequestInterface::class);
        $token = $req->header('Authorization');
        if (! $token) {
            $log->error('未获取到 Authorization 头信息');
            throw new BusinessException(ErrCode::Unauthorized, '认证失败');
        }

        $token = substr($token, 7);
        if ($this->jwt->isLogout($token)) { //先前已经退出登陆完成了
            throw new BusinessException(ErrCode::Unauthorized, '认证失败');
        }

        $payload = $this->jwt->decode($token);
        $userId = Arr::get($payload, 'sub');
        if (! $userId) {
            $log->error('jwt 获取用户 id 失败');
            throw new BusinessException(ErrCode::Unauthorized, '认证失败');
        }

        $user = UsersRepository::getUserByUid($userId);
        if (!$user) {
            throw new BusinessException(ErrCode::Unauthorized, '认证失败');
        }

        Context::set('user', $user);

        return $handler->handle($request);
    }
}
