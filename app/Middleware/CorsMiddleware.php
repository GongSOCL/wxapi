<?php

declare(strict_types=1);

namespace App\Middleware;

use Hyperf\Utils\Context;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CorsMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = Context::get(ResponseInterface::class);
        $response = $this->addCorsHeader($response);
        Context::set(ResponseInterface::class, $response);

        if ($request->getMethod() == 'OPTIONS') {
            return $response;
        }

        $response = $handler->handle($request);
        return $this->addCorsHeader($response);
    }

    public function addCorsHeader(ResponseInterface $response): ResponseInterface
    {
        if (!$response->hasHeader('Access-Control-Allow-Origin')) {
            $response = $response->withHeader('Access-Control-Allow-Origin', '*');
        }
        if (!$response->hasHeader('Access-Control-Allow-Methods')) {
            $response = $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PUT');
        }

        if (!$response->hasHeader('Access-Control-Allow-Headers')) {
            $response = $response->withHeader(
                'Access-Control-Allow-Headers',
                'SIG,DNT,X-Mx-ReqToken,Keep-Alive,'.
                'User-Agent,X-Requested-With,If-Modified-Since,'.
                'Cache-Control,Content-Type, Accept-Language,'.
                ' Origin, Accept-Encoding, device-id, app-platform,'.
                ' app-version, Authorization'
            );
        }
        return $response;
    }
}
