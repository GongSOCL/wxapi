<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Middleware;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Repository\UsersRepository;
use App\Service\User\UserService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\Context;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AuthMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $req = $this->container->get(RequestInterface::class);
        $userYyid = $req->input('user_yyid');
        $userToken = $req->input('user_token');
        if (! $userYyid || ! $userToken) {
            throw new BusinessException(ErrorCode::ERROR_CODE_AUTHENTICATION_FAILED);
        }
        $userService = $this->container->get(UserService::class);
        $userSession = $userService->verifyUserLogin($userYyid, $userToken);

        if (! $userSession || ! isset($userSession['uid'])) {
            throw new BusinessException(ErrorCode::ERROR_CODE_AUTHENTICATION_FAILED);
        }
        $user = UsersRepository::getUserByUid($userSession['uid']);

        if (! $user) {
            UserService::deleteUserCacheSession($userYyid);
            throw new BusinessException(ErrorCode::ERROR_CODE_AUTHENTICATION_FAILED);
        }
        Context::set('user', $user);

        return $handler->handle($request);
    }
}
