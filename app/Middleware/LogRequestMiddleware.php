<?php

declare(strict_types=1);

namespace App\Middleware;

use App\Helper\Helper;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class LogRequestMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $log = Helper::getLogger();
        $log->info("log_request", [
            'path' => $request->getUri()->getPath(),
            'method' => $request->getMethod(),
            'params' => $request->getParsedBody(),
            'header' => $request->getHeaders()
        ]);
        return $handler->handle($request);
    }
}
