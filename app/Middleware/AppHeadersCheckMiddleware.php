<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Middleware;

use App\Constants\Auth;
use App\Constants\Device;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Helper\Jwt;
use App\Repository\UsersRepository;
use App\Service\User\UserService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Arr;
use Hyperf\Utils\Context;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Youyao\Framework\ErrCode;
use Hyperf\Di\Annotation\Inject;

class AppHeadersCheckMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        [$appVersion, $appPlatform, $deviceId] = Helper::extractAppHeaders();

        //校验参数范围
        $validPlatform = [Device::PLATFORM_ANDROID, Device::PLATFORM_IOS, Device::PLATFORM_AGENT_WECHAT];
        if ($appPlatform == "" || (!in_array($appPlatform, $validPlatform))) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法请求,未定义别平台");
        }

        // app需要传版本号和设备id
        $isApp = in_array($appPlatform, [Device::PLATFORM_ANDROID, Device::PLATFORM_IOS]);
        if ($isApp && ($appVersion == "" || $deviceId == "")) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "非法请求,未定义设备类型");
        }

        Helper::setAuthRoleFromDevice($appPlatform);

        return $handler->handle($request);
    }
}
