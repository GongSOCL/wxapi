<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class DataStatus extends AbstractConstants
{
    const REGULAR = 1;

    const DELETE = 0;

    const USER_TYPE_AGENT = 1;

    const USER_TYPE_DOCTOR = 2;

    const WECHAT_DOCTOR = 2;    //医生公众号

    const WECHAT_AGENT = 1;    //代表公众号

    const WECHAT_APP = 3;       //app

    const WECHAT_YOU_YAO_MINI_PROGRAM = 4;

    //微信模板key
    const WX_SERVICE_STATUS_NOTICE_KEY = 'service_status_notice';              //服务状态提醒

    const WX_REWARD_NOTICE_KEY = 'reward_notice';                     //中奖结果通知提醒

    const WX_CRON_PERFORMANCE_NOTICE_KEY = 'cron_performance_notice';           //定期业绩通知

    const WX_AUDIT_NOTICE_KEY = 'audit_notice';                                 //审核通知

    const WX_REGISTER_SUCCESS_KEY = 'register_success';                         //注册成功通知

    const WX_RECHARGE_SUCCESS_KEY = 'recharge_success';                          //充值成功通知

    const WX_COMPLETE_ACTIVITY_KEY = 'complete_activity';                       //完成活动提醒

    const WX_MEETING_NOTICE_KEY = 'meeting_notice';                             //会议提醒

    const WX_DRUG_FLOW_DIRECTION_NOTICE_KEY = 'drub_flow_direction_notice';     //药品流向提醒

    const WX_QUESTIONNAIRE_KEY = 'questionnaire';                               //问卷
    const WX_AGENT_SERVE_APPLY = 'serve_apply';                                 //申请服务医院代表审核
    const WX_HELP_AUDIT = 'help_audit';
    const WX_AGENT_VERIFY = 'agent_verify';
    const SUPPLIER_GROUP = 'supplier_group';
    const SUPPLIER_APPLY = 'supplier_apply';
    const SUPPLIER_UNBIND = 'supplier_unbind';

    //行为id 来源：t_activity表
    const ACTIVITY_ID_REGISTER = 1;              //注册行为

    const ACTIVITY_ID_PROMOTED = 2;              //推广行为

    const ACTIVITY_ID_EXHIBITION = 3;            //展会行为

    const ACTIVITY_ID_QRCODE_DOCTOR_GROUP = 4;   //医生群组二维码

    const ACTIVITY_ID_QRCODE_AGENT_GROUP = 5;    //代表群组二维码

    const ACTIVITY_ID_QRCODE_AGENT_INVITE = 6;   //代表个人邀请二维码

    const ACTIVITY_ID_QRCODE_DOCTOR_INVITE = 7;  //医生个人邀请二维码

    //身份证照片上传文件公共路径
    const UPLOAD_URI_IDCARD = BASE_PATH . '/storage/upload/idcard/';

    const PAGE_SIZE = 10; //默认列表每页显示数量

    //excel报表存储位置（销售金额）
    const SALES_REPORT_URI = BASE_PATH . '/storage/excel/hospitalSalesReport/';

    const CHECKOUT_DETAIL_URI = BASE_PATH . '/storage/excel/checkoutDetailReport/';

    //供应商报表位置
    const SUPPLIER_REPORT_URI = BASE_PATH . '/storage/excel/supplierReport/';

    const APP_ENV_DEV  ='testing';
    const APP_ENV_STAGING = 'staging';
    const APP_ENV_PROD = 'prod';

    //坐标系统
    const COORDINATE_GPS = 'gps';           //gps
    const COORDINATE_GCJ02 = 'gcj-02';      //gcj-02
    const WECHAT_AEROGEN = 3;    //优云呼吸公众号

    const DRUGSTORE = 3;    //药店

    //消息平台优药公众号平台
    const MESSAGE_PLATFORM_AGENT = 1;
    //消息平台优药app
    const MESSAGE_PLATFORM_APP = 2;

    //力蜚能
    const DRUG_SERIES_NAME = '力蜚能';

    //优锐公司yyid
    const COMPANY_YOURUI = "338EEF6193A011E7BAD8A1F3E2912E36";
    //优药公司yyid
    const COMPANY_YOUYAO = "73BF674CA97D88DF8DF7DBFEB07F44B7";

    const DEFAULT_AES_IV = "LtRdHdS:x7Kyg?^n";
    const DEFAULT_AES_METHOD = "aes-256-cbc";

    /**
     * @Info("不落地的展示搜索记录过期时间")
     */
    const EXPIRE_TIME_WITHOUT_DOWN = 30*24*3600;
}
