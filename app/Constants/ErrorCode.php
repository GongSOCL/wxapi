<?php

declare(strict_types=1);

namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 * @method string getMessage(int $errorCode) static 通过反射获取错误信息
 */
class ErrorCode extends AbstractConstants
{
    /**
     * @message("Success")
     */
    const ERROR_CODE_SUCCESS = 0;

    /**
     * @message("Parameters error")
     */
    const ERROR_CODE_ILLEGAL_REQUEST = 4001;

    /**
     * @message("Parameters error")
     */
    const ERROR_CODE_PARAMETERS_ERROR = 4002;

    /**
     * @message("Authentication failed")
     */
    const ERROR_CODE_AUTHENTICATION_FAILED = 4003;

    /**
     * @message("Query failed")
     */
    const ERROR_CODE_QUERY_FAILED = 4004;

    /**
     * @message("手机号尚未绑定")
     */
    const ERROR_MOBILE_NOT_BIND = 4005;

    /**
     * @Message("Server Error！")
     */
    const SERVER_ERROR = 500;

    /**
     * @message("validate error")
     */
    const VALIDATION_ERROR = 100000;

    /**
     * @message("upload error")
     */
    const UPLOAD_ERROR = 200000;


    //问卷
    /**
     * @message("问卷不存在")
     */
    const QUESTIONNAIRE_NOT_EXISTS = 500001;
    /**
     * @message("您已完成该问卷")
     */
    const QUESTIONNAIRE_FINISHED = 500002;
    /**
     * @message("您尚末被邀请参与该问卷")
     */
    const QUESTIONNAIRE_NOT_INVITED = 500003;
    /**
     * @message("问卷邀请已过期")
     */
    const QUESTIONNAIRE_INVITE_EXPIRED = 500004;
    /**
     * @message("问卷尚末配置完成，请联系管理员")
     */
    const QUESTIONNAIRE_WITHOUT_ITEMS = 500005;
    /**
     * @message("当前问卷状态不允许答题")
     */
    const QUESTIONNAIRE_STATUS_ERROR = 500006;
    /**
     * @message("当前问题不存在")
     */
    const QUESTIONNAIRE_QUESTION_ITEM_NOT_EXISTS = 500007;
    /**
     * @message("当前问题末配置选项")
     */
    const QUESTIONNAIRE_ITEM_WITHOUT_ANSWER = 500009;
    /**
     * @message("当前问题末配置正确选项，请联系管理员")
     */
    const QUESTIONNAIRE_ITEM_WITHOUT_RIGHT_ANSWER = 500010;
    /**
     * @message("请至少选择一个选项")
     */
    const QUESTIONNAIRE_ITEM_WITH_AT_LEAST_ONE_ANSWER = 500011;
    /**
     * @message("关闭邀请问卷功能尚不支持查看数据")
     */
    const QUESTIONNAIRE_INVITE_ANSWER_NO_NOT_SUPPORT = 500012;
    /**
     * @message("您已经邀请过该医生，无法重复邀请")
     */
    const QUESTIONNAIRE_USER_WECHAT_INVITE_EXISTS = 500013;
    /**
     * @message("微信邀请记录尚末过期，无法重新邀请")
     */
    const QUESTIONNAIRE_PAPER_WECHAT_INVITE_NOT_EXPIRE = 500014;
    /**
     * @message("当前记录无法重新发起邀请，请联系管理员")
     */
    const QUESTIONNAIRE_PAPER_WECHAT_NOT_VALID_REINVITED = 500015;
    /**
     * @message("当前记录状态无法通知，请联系管理员")
     */
    const QUESTIONNAIRE_PAPER_WECHAT_NOT_VALID_NOTICE = 500016;

    /**
     * @message("邀请已达限额,无法邀请更多人员参与问卷")
     */
    const QUESTIONNAIRE_INVITE_QUOTE_EXCEED = 500017;


    //微信
    /**
     * @message("用户微信信息不存在")
     */
    const USER_WECHAT_INFO_NOT_EXISTS = 510001;

    /**
     * @message("用户尚末订阅该微信号")
     */
    const USER_NOT_SUBSCRIBE = 510002;

    /**
     * @message("您尚不是他的微信好友")
     */
    const USER_NOT_WECHAT_FRIENDS = 510003;

    /**
     * @message("微信接口异常")
     */
    const WECHAT_HTTP_EXCEPTION = 510004;

    /**
     * @message("微信接口异常")
     */
    const WECHAT_BUSINESS_EXCEPTIN=510005;
    /**
     * @message("试题不存在")
     */
    const QUESTION_NOT_EXISTS = 500028;
    /**
     * @message("问卷已过期")
     */
    const QUESTIONNAIRE_EXPIRED = 500029;
    /**
     * @message("拜访记录产品添加失败")
     */
    const AGENT_VISIT_ADD_SERIES_FAIL = 520001;
    /**
     * @message("拜访记录照片添加失败")
     */
    const AGENT_VISIT_ADD_PICS_FAIL = 520002;
    /**
     * @message("代表拜访记录不存在")
     */
    const AGENT_VISIT_NOT_EXISTS = 520003;
    /**
     * @message("只能编辑自己的拜访记录")
     */
    const AGENT_VISIT_EDIT_SELF_ONLY = 520004;
    /**
     * @message("拜访记录保存产品记录失败,请重试")
     */
    const AGENT_VISIT_EDIT_DRUG_FAIL = 520005;
    /**
     * @message("拜访记录保存图片失败,请重试")
     */
    const AGENT_VISIT_EDIT_PIC_FAIL = 520006;
    /**
     * @message("百度地图接口调用异常")
     */
    const BAIDU_MAP_REVERSE_COORD_ERROR = 521007;
    /**
     * @message("人员关联科室添加失败")
     */
    const VISIT_ADD_DEPART_FAIL = 521008;
    /**
     * @message("超出打卡范围需要选择原因")
     */
    const AGENT_VISIT_OVER_GEO_EMPTY_COMMENT = 521009;
    /**
     * @message("请至少选择一张照片")
     */
    const AGENT_VISIT_AT_LEAST_ONE_PIC = 521010;
    /**
     * @message("医院不存在")
     */
    const HOSPITAL_NOT_EXISTS = 521011;
    /**
     * @message("当前拜访记录不允许签出，请刷新重试")
     */
    const AGENT_VISIT_STATUS_DENY_CHECK_OUT = 521012;
    /**
     * @message("签出不允许修改医院")
     */
    const AGENT_VISIT_CHECK_OUT_DENY_HOSPITAL_MODIFY = 521013;
    /**
     * @message("签出不允许修改拜访类型")
     */
    const AGENT_VISIT_CHECK_OUT_DENY_TYPE_MODIFY = 521014;
    /**
     * @message("拜访记录更新保存失败，请重试")
     */
    const AGENT_VISIT_EDIT_FAIL = 521015;
    /**
     * @message("签出部门保存失败")
     */
    const AGENT_VISIT_EDIT_DEPART_FAIL = 521016;
    /**
     * @message("请至少选择一个部门")
     */
    const AGENT_VISIT_HOSPITAL_AT_LEAST_ONE_DEPART = 521017;
    /**
     * @message("请至少选择一个产品")
     */
    const AGENT_VISIT_AT_LEAST_ONE_DRUG = 521018;
    /**
     * @message("只能编辑自己的拜访记录")
     */
    const AGENT_VISIT_ONLY_EDIT_SELF = 521019;
    /**
     * @message("该类型拜访请选择一个医院")
     */
    const CHECKIN_HOSPITAL_EMPTY_INVALID = 521020;
    /**
     * @message("当前状态不允许修改")
     */
    const AGENT_VISIT_STATUS_DENY_MODIFY = 521021;
    /**
     * @message("拜访记录编辑失败")
     */
    const AGENT_VISIT_GEO_EDIT_FAIL = 521022;
    /**
     * @message("业务异常")
     */
    const BUSINESS_ERROR = 521023;
    /**
     * @message("部门不存在")
     */
    const DEPART_NOT_EXISTS = 521024;
    /**
     * @message("服务商不存在")
     */
    const SUPPLIER_NOT_EXISTS = 521025;
    /**
     * @message("服务商下没有药品")
     */
    const SUPPLIER_DONT_HAVE_PRODUCTS = 521026;
    /**
     * @message("服务商下没有医院")
     */
    const SUPPLIER_DONT_HAVE_HOSPITALS = 521027;
    /**
     * @message("拜访数据为空")
     */
    const EMPTY_SUPPLIER_VISIT_INFO = 521028;
    /**
     * @message("该用户还未通过服务商身份认证，请在认证通过后操作")
     */
    const USER_DONT_VERIFY = 521029;
    /**
     * @message("发送消息认证信息错误")
     */
    const SUPPLIER_VERIFY_DATA_WORING = 521030;
    /**
     * @message("该用户已在其他群中任职，无法进入，请先让其退群后加入")
     */
    const MEMBER_EXIXTS = 521031;
    /**
     * @message("你和该用户公司不一致，无法进群")
     */
    const NOT_SAME_COMPANY = 521032;

    /**
     * @message("版本不存在或渠道错误")
     */
    const VERSION_NOT_EXIST = 521033;

    /**
     * @message("该用户不存在或已注销")
     */
    const DOCTOR_USER_NOT_EXISTS  = 521034;
    /**
     * @message("用户不存在或已注销")
     */
    const USER_NOT_EXISTS  = 521034;

    /**
     * @message("错误活动")
     */
    const WRONG_ACTIVITY  = 521035;

    /**
     * @message("用户还未成为服务商")
     */
    const USER_IS_NOT_SUPPLIER  = 521036;

    /**
     * @message("用户结算标识的服务商不一致")
     */
    const ROLE_TYPE_NOT_SAME  = 521037;
}
