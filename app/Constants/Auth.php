<?php
declare(strict_types=1);

namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class Auth extends AbstractConstants
{
    // 角色-公众号
    const AUTH_ROLE_YOU_YAO_AGENT = 0;
    // 角色 - app
    const AUTH_ROLE_YOU_YAO_APP = 1;
    // 角色 - 小程序
    const AUTH_ROLE_YOU_YAO_MINI_PROGRAM =2;

    //登陆成功
    const AUTH_STATUS_SUCCESS = 1;
    //需要绑定手机号
    const AUTH_STATUS_BIND_MOBILE = 2;
}
