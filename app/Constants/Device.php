<?php
declare(strict_types=1);

namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class Device extends AbstractConstants
{
    // 公众号
    const PLATFORM_AGENT_WECHAT = 0;
    // 安卓设备
    const PLATFORM_ANDROID = 1;
    // ios设备
    const PLATFORM_IOS = 2;
    // 小程序
    const PLATFORM_MINI_PROGRAM = 3;
}
