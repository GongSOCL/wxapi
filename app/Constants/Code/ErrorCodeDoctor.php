<?php

declare(strict_types=1);

namespace App\Constants\Code;

use App\Constants\ErrorCode;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 * @method string getMessage(int $errorCode) static 通过反射获取错误信息
 */
class ErrorCodeDoctor extends ErrorCode
{
    //医生
    /**
     * @message("该医院不存在")
     */
    const DOCTOR_HOSPITAL_NOT_EXISTS = 700001;

    /**
     * @message("该医院不存在该科室")
     */
    const DOCTOR_DEPARTMENT_NOT_EXISTS = 700002;

    /**
     * @message("提交认证资料失败")
     */
    const DOCTOR_FAIL_TO_SUBMIT_QUALIFYINFO = 700003;

    /**
     * @message("该用户不存在或已注销")
     */
    const DOCTOR_USER_NOT_EXISTS  = 700004;

    /**
     * @message("该物品不存在或该物品已兑换完")
     */
    const DOCTOR_GOODS_NOT_REMAIN  = 700005;

    /**
     * @message("该物品库存不足")
     */
    const DOCTOR_GOODS_NOT_ENOUGH  = 700006;

    /**
     * @message("消费积分不足")
     */
    const DOCTOR_SHOPPING_POINTS_NOT_ENOUGH  = 700007;

    /**
     * @message("服务积分不足")
     */
    const DOCTOR_SERVICE_POINTS_NOT_ENOUGH  = 700008;

    /**
     * @message("兑换失败")
     */
    const DOCTOR_FAIL_TO_EXCHANGE_GOODS  = 700009;

    /**
     * @message("信息已提交，正在认证中")
     */
    const DOCTOR_QUALIFYINFO_ALREADY_SUBMIT  = 7000010;

    /**
     * @message("已在邀请列表中，请勿重复邀请")
     */
    const DOCTOR_ALREADY_INVITE  = 7000011;

    /**
     * @message("没有该类型的用户")
     */
    const DOCTOR_INVITE_TYPE_NOT_EXISTS  = 7000012;

    /**
     * @message("邀请失败")
     */
    const FAIL_TO_INVITE_DOCTOR  = 7000013;

    /**
     * @message("缺少上传照片信息")
     */
    const DOCTOR_UPLOAD_INFO_MISS  = 7000014;

    /**
     * @message("已上传照片，请勿重复上传")
     */
    const DOCTOR_IDCARD_ALREADY_UPLOADED = 7000015;



    /**
     * @message("用户不存在或手机号和医院未认证")
     */
    const REPORT_HISPOTAL_PHONE_MISSING = 900001;


    /**
     * @message("找不到该员工工号")
     */
    const NO_STAFF_CODE = 900002;


    /**
     * @message("不合法的类型")
     */
    const ILLEGAL_TYPE = 900003;


    /**
     * @message("暂无可导出的数据")
     */
    const NO_REPORT = 900004;


    /**
     * @message("邮件发送失败")
     */
    const FAIL_TO_SEND_EMAIL = 900005;

    /**
     * @message("报表中存在未知药品")
     */
    const ILLEGAL_DRUG_IN_REPORT = 900006;

    /**
     * @message("平台未收录您的真实姓名")
     */
    const TRUE_NAME_LACK = 900007;

    /**
     * @message("报表中存在未知医院")
     */
    const ILLEGAL_HOSPITAL_IN_REPORT = 900008;

    /**
     * @message("邮件发送失败（队列错误）")
     */
    const FAIL_TO_SEND_EMAIL_QUEUEU = 900009;
}
