<?php

declare(strict_types=1);

namespace App\Constants\Code;

use App\Constants\ErrorCode;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 * @method string getMessage(int $errorCode) static 通过反射获取错误信息
 */
class ErrorCodeGroup extends ErrorCode
{

    /**
     * @message("创建失败")
     */
    const FAIL_TO_CREATE_GROUP = 800001;

    /**
     * @message("已有同名群组，无法创建")
     */
    const GROUP_ALREADY_EXISTS = 800002;

    /**
     * @message("群组不存在")
     */
    const GROUP_NOT_EXISTS = 800003;

    /**
     * @message("重命名失败")
     */
    const FAIL_TO_RENAME_GROUP = 800004;

    /**
     * @message("删除失败")
     */
    const FAIL_TO_DELETE_GROUP = 800005;

    /**
     * @message("添加失败")
     */
    const FAIL_TO_ADD_TO_DEFAULT_GROUP = 800006;

    /**
     * @message("选中的医生已在群中，请刷新页面")
     */
    const DOCTOR_ALREADY_IN_GROUP = 800007;

    /**
     * @message("选中的医生已不在该群中，请刷新页面")
     */
    const DOCTOR_ALREADY_DELETED = 800008;

    /**
     * @message("删除失败")
     */
    const FAIL_TO_DELETED_MEMBER = 800009;

    /**
     * @message("改变群组失败")
     */
    const FAIL_TO_CHANGE_GROUP = 800010;

    /**
     * @message("该意思不存在")
     */
    const MEMBER_NOT_EXISTS = 800011;

    /**
     * @message("只有群主才能修改群公告")
     */
    const ONLY_LEADER_CAN_CHANGE_NOTICE = 800012;

    /**
     * @message("修改群公告失败")
     */
    const FAIL_TO_CHANGE_NOTICE = 800013;

    /**
     * @message("只有群主才能查看产品")
     */
    const ONLY_LEADER_CAN_CHECK_SERIES = 800014;

    /**
     * @message("产品不存在")
     */
    const SERIES_NOT_EXISTS = 800015;

    /**
     * @message("无法变更产品，您的组群已绑定产品")
     */
    const CAN_NOT_CHANGE_SERIES = 800016;

    /**
     * @message("无法变更产品，您有组员已在其他组群代理该产品")
     */
    const MEMBER_ALREADY_IN_SERIES = 800017;

    /**
     * @message("只有群主才能修改群主")
     */
    const ONLY_LEADER_CAN_CHANGE_LEADER = 800018;

    /**
     * @message("组员信息不存在")
     */
    const ON_MEMBER_INFO = 800019;

    /**
     * @message("存在进行中的投票，无法更换组长")
     */
    const CAN_NOT_CHANGE_LEADER = 800020;

    /**
     * @message("群组微信信息不存在")
     */
    const WECHAT_MEMBER_NOT_EXIST = 800021;

    /**
     * @message("只有群主才能解散群")
     */
    const ONLY_LEADER_CAN_DELETE_GROUP = 800022;

    /**
     * @message("只有群主才能查看待加入人员列表")
     */
    const ONLY_LEADER_CAN_CHECK_JOINLIST = 800023;

    /**
     * @message("群组尚末分配产品，无法添加成员")
     */
    const CAN_NOT_ADD_MEMBER = 800024;

    /**
     * @message("选中的医生已有人在该群中，请刷新页面")
     */
    const MEMBER_ALREADY_IN = 800025;

    /**
     * @message("您不是群组成员或管理员，无法添加成员")
     */
    const CAN_NOT_ADD_MEMBER_NOT_IN = 800026;

    /**
     * @message("只有群主才能同意或拒绝")
     */
    const ONLY_LEADER_CAN_APPROVE = 800027;

    /**
     * @message("加入失败，已有人在其他群代理该药品")
     */
    const ALREADY_IN_DRUG_SERIES = 800028;

    /**
     * @message("只有群主才能删除群成员")
     */
    const ONLY_LEADER_CAN_DELETE = 800029;

    /**
     * @message("贡献比例不能超过100")
     */
    const WRONG_TAX = 800030;

    /**
     * @message("当前已存在进行中的投票，无法投票")
     */
    const VOTE_ALREADY_EXIST = 800031;

    /**
     * @message("群主无法投票")
     */
    const LEADER_CAN_NOT_VOTE = 800032;

    /**
     * @message("投票信息不存在或投票已结束")
     */
    const VOTE_NOT_EXIST = 800033;

    /**
     * @message("您不在该群，无法投票")
     */
    const NOT_IN_GROUP_CAN_NOT_VOTE = 800034;

    /**
     * @message("您已经投过票了")
     */
    const ALREADY_VOTED = 800035;

    /**
     * @message("投票失败，请稍后重试")
     */
    const FAIL_TO_VOTE = 800036;

    /**
     * @message("您不是群主，无法中止投票")
     */
    const ONLY_LEADER_CAN_CANCEL = 800037;

    /**
     * @message("该医生在自定义群组中，请先移除群组")
     */
    const IN_SOME_GROUP = 800038;

    /**
     * @message("该医生还未在优佳医授权，无法加入其他群组")
     */
    const CANT_NOT_ADD_GROUP = 800039;

    /**
     * @message("选中的医生已在其他群中，无法加入该群组")
     */
    const ALREADY_IN_OTHER_GROUP = 800040;

    /**
     * @message("手机号已被其他医生占用，请确认手机号是否正确")
     */
    const DOCTOR_PHONE_ALREADY_USED  = 800041;

    /**
     * @message("有相同姓名医生存在，请确认是否添加新医生")
     */
    const REPEAT_DOCTOR  = 800042;

    /**
     * @message("该用户没有组织架构")
     */
    const USER_DONT_HAVE_ORGANIZATION  = 800043;

    /**
     * @message("部门错误")
     */
    const WRONG_USER_DEPARTMENT  = 800044;

    /**
     * @message("该医院下已存在该医生，确认新增？")
     */
    const SAME_HOSPITAL_DOCTOR  = 800045;
}
