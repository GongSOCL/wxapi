<?php


namespace App\Constants;

use Hyperf\Constants\AbstractConstants;

/**
 * 微信模板消息配置
 * Class Wechat
 * @package App\Constants
 */
class Wechat extends AbstractConstants
{
    /**
     * @message("服务提醒")
     */
    const TPL_SERVE_NOTICE = "serve_notice";

    /**
     * @message("注册成功通知")
     */
    const TPL_REGISTER_SUCCESS = "register_success";

    /**
     * @message("流向报告")
     */
    const TPL_TRANSACTION_REPORT = "transaction_report";

    /**
     * @message("问卷")
     */
    const TPL_AGENT_QUESTIONNAIRE = "questionnaire";

    /**
     * @message("签到成功")
     */
    const SIGN_IN_SUCCESS =  "sign_in";

    /**
     * 认证结果通知
     */
    const VERIFY_NOTICE = "verify_notice";

    /**
     * 服务申请通知
     */
    const SERVE_APPLY = 'serve_apply';

    /**
     * @message("积分申请结果通知")
     */
    const POINT_APPLY = "point_apply";

    /**
     * @message("流向提醒")
     */
    const TRANSACTION_NOTICE = "transaction_notice";

    const WECHAT_CHANNEL_WXAPI = "wxapi";
    const WECHAT_CHANNEL_YOUJIAYI = 'youjiayi';

    /**
     * @Message("合同签署提醒")
     */
    const CONTRACT_SIGN_NOTICE = 'contract_sign_notice';

    /**
     * @message("合同签署完成")
     */
    const CONTRACT_GENERATED = 'contract_generated';

    /**
     * @message("服务商审核结果通知")
     */
    const SUPPLIER_GROUP = 'supplier_group';

    /**
     * @message("服务商绑定通知")
     */
    const SUPPLIER_APPLY = 'supplier_apply';

    /**
     * @message("服务商解除绑定通知")
     */
    const SUPPLIER_UNBIND = 'supplier_unbind';

    /**
     * @message("直播邀请通知")
     */
    const LIVE_INVITE = 'live_invite';
}
