<?php

declare(strict_types=1);

namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class Point extends AbstractConstants
{
    //api timeout时间
    const API_REQUEST_TIMEOUT = 15;
    //兑换商品消耗积分URL
    const POINTS_EXCHANGE_GOODS = '/api/point/goods';
    //添加事件积分
    const POINTS_ADD_ACTIVITY = '/api/point/activity';
    // 获取积分明细列表 url
    const POINTS_DETAILS = '/api/point/details';
    //问卷活动event_key
    const QUESTIONNAIRE_EVENT_KEY = 'question_answer';
}
