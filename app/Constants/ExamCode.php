<?php

declare(strict_types=1);
/**
 * This file is part of the youyao/service-core.
 *
 * (c) youyao <info@nuancebiotech.cn>
 * This source file is subject to the license under the project that is bundled.
 */
namespace App\Constants;

use Hyperf\Constants\Annotation\Constants;
use Youyao\Framework\AbstractConstants;

/**
 * @Constants
 */
class ExamCode extends AbstractConstants
{

    /**
     * @Info("优药")
     */
    const YOUYAO = 1;

    /**
     * @Info("优能汇")
     */
    const PHARMACY = 2;

    /**
     * @Info("通过")
     */
    const REGULAR = 1;
    /**
     * @Info("未通过")
     */
    const DELETE = 0;
}
