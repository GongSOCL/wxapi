<?php
declare(strict_types=1);
namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 * 拜访补签状态
 */
class VisitSupplement extends AbstractConstants
{
    /**
     * @message("公司未缓存")
     */
    const STATUS_COMPANY_NONE = 1;

    /**
     * @message("公司允许补签")
     */
    const STATUS_COMPANY_YES = 2;


    /**
     * @message("公司不允许补签")
     */
    const STATUS_COMPANY_NO = 3;

    /**
     * @message("用户补签未缓存")
     */
    const STATUS_USER_NONE = 1;
    /**
     * @message("用户不允许补签")
     */
    const STATUS_USER_YES = 2;
    /**
     * @message("用户允许补签")
     */
    const STATUS_USER_NO = 3;
}
