<?php

declare(strict_types=1);

namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class Smtp extends AbstractConstants
{
    // 字符
    const UNICODE = 'UTF-8';
    //SMTP服务器地址
    const SMTP_SERVER = 'smtp.exmail.qq.com';
    // 允许 TLS 或者ssl协议
    const SMTP_SECURE = 'ssl';
    //SMTP端口
    const SMTP_PORT = 465;
    //SMTP账号
    const SMTP_USER   = 'noreply@nuancepharma.cn';
    //SMTP密码
    const SMTP_PASS   = 'bfgt#sDxk212$';
}
