<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
use App\Model\Qa\Users;
use Hyperf\Utils\Context;

if (! function_exists('user')) {
    /**
     * 获取当前登录用户信息.
     * @return null|Users
     */
    function user()
    {
        return Context::get('user');
    }
}
