<?php


namespace App\Handler\Users;

use App\Model\Qa\Users;
use App\Service\Common\ServeService;
use App\Service\User\UserService;
use App\Service\Wechat\WechatService;
use Google\Protobuf\GPBEmpty;
use Grpc\Wxapi\User\AgentDoctorGetReply;
use Grpc\Wxapi\User\AgentDoctorGetRequest;
use Grpc\Wxapi\User\AgentServeHospitalsReply;
use Grpc\Wxapi\User\AgentSpecifyHospitalsReply;
use Grpc\Wxapi\User\CheckResultReply;
use Grpc\Wxapi\User\RebindUserCompanyRequest;
use Grpc\Wxapi\User\SendUserTemplateMsgRequest;
use Grpc\Wxapi\User\ServeIdRequest;
use Grpc\Wxapi\User\ServeListRequest;
use Grpc\Wxapi\User\ServeListResponse;
use Grpc\Wxapi\User\ServiceAgentsReply;
use Grpc\Wxapi\User\StoreIdRequest;
use Grpc\Wxapi\User\UserIdRequest;
use Grpc\Wxapi\User\UserListReponse;
use Grpc\Wxapi\User\UserListRequest;
use Grpc\Wxapi\User\UserSearchList;
use Grpc\Wxapi\User\UserSearchRequest;
use Grpc\Wxapi\User\UserServeHospitalCheckRequest;
use Grpc\Wxapi\User\UserSvcInterface;
use Grpc\Wxapi\User\UserSvcServerHandler;
use Grpc\Wxapi\User\UserVerifyStatus;
use Grpc\Wxapi\User\VerifyInfoResponse;
use Grpc\Wxapi\User\VerifyRequest;

class UserSrvHandler extends UserSvcServerHandler
{
    /**
     * @inheritDoc
     */
    public function listUser(UserListRequest $request): UserListReponse
    {

        $keywords = $request->getKeywords();
        $companyId = $request->getCompanyId();
        $current = $request->getPage()->getPage();
        $limit = $request->getPage()->getSize();
        $verifyStatus = $request->getVerifyStatus();
        $vStatus = self::transferVerifyAgentStatus($verifyStatus);
        $submittedType = $request->getSubmittedType();
        $realType = $request->getRealType();
        return UserService::listUsersWithPage(
            $companyId,
            $keywords,
            $vStatus,
            $current,
            $limit,
            $submittedType,
            $realType
        );
    }

    private static function transferVerifyAgentStatus(int $vStatus): int
    {
        switch ($vStatus) {
            case UserVerifyStatus::VERIFY_APPLYING:
                return Users::AGENT_VERIFY_PROCESSING;
            case UserVerifyStatus::VERIFY_PASSED:
                return Users::AGENT_VERIFY_OK;
            case UserVerifyStatus::VERIFY_REFUSED:
                return Users::AGENT_VERIFY_REFUSED;
            case UserVerifyStatus::VERIFY_UNKNOW:
                return 0;
            default:
                return -1;
        }
    }


    /**
     * @inheritDoc
     */
    public function verifyPass(VerifyRequest $request): GPBEmpty
    {
        UserService::verifyOk($request->getUid(), $request->getRealRole());
        return new GPBEmpty();
    }

    /**
     * @inheritDoc
     */
    public function verifyRefuse(VerifyRequest $request): GPBEmpty
    {
        UserService::verifyRefused(
            $request->getUid(),
            $request->getReason(),
            $request->getAdminId()
        );

        return new GPBEmpty();
    }

    /**
     * @inheritDoc
     */
    public function userServeList(ServeListRequest $request): ServeListResponse
    {
        $page = $request->getPage();

        return ServeService::getServeList(
            $request->getUid(),
            $request->getHospitalId(),
            $request->getSeriesId(),
            $request->getStart(),
            $request->getEnd(),
            $page->getPage(),
            $page->getSize()
        );
    }

    /**
     * @inheritDoc
     */
    public function auditServeSuccess(ServeIdRequest $request): GPBEmpty
    {
        ServeService::applyOk($request->getServeId());
        return new GPBEmpty();
    }

    /**
     * @inheritDoc
     */
    public function auditServeFail(ServeIdRequest $request): GPBEmpty
    {
        ServeService::applyRefused($request->getServeId());
        return new GPBEmpty();
    }

    /**
     * @inheritDoc
     */
    public function getVerifyInfo(UserIdRequest $request): VerifyInfoResponse
    {
        return UserService::getAdminUserVerifyInfo($request->getUid());
    }

    /**
     * @inheritDoc
     */
    public function auditServeUnbindSuccess(ServeIdRequest $request): GPBEmpty
    {
        ServeService::auditUnbind($request->getServeId(), true);
        return new GPBEmpty();
    }

    /**
     * @inheritDoc
     */
    public function auditServeUnbindFail(ServeIdRequest $request): GPBEmpty
    {
        ServeService::auditUnbind($request->getServeId(), false);
        return new GPBEmpty();
    }

    /**
     * @inheritDoc
     */
    public function verifyCancel(VerifyRequest $request): GPBEmpty
    {
        UserService::verifyCancel($request->getUid());
        return new GPBEmpty();
    }

    /**
     * @inheritDoc
     */
    public function searchUser(UserSearchRequest $request): UserSearchList
    {
        $limit = $request->getLimit();
        if ($limit <= 0) {
            $limit  = 10;
        }

        return UserService::searchUser(
            $request->getKeywords(),
            $limit
        );
    }

    public function getAgentServeHospitalDoctors(AgentDoctorGetRequest $request): AgentDoctorGetReply
    {
        $page = $request->getPage();
        return UserService::getUserServeHospitalDoctors(
            $request->getUserId(),
            $request->getWithWechat(),
            $page->getPage(),
            $page->getSize()
        );
    }

    /**
     * 获取代表服务的医院列表
     *
     * @param  UserIdRequest $request
     * @return AgentServeHospitalsReply
     */
    public function getAgentServeHospitals(UserIdRequest $request): AgentServeHospitalsReply
    {
        return ServeService::getUserServeHospitalByUid($request->getUid());
    }

    /**
     * 检查代表服务医院或药店关系是否存在
     *
     * @param  UserServeHospitalCheckRequest $request
     * @return CheckResultReply
     */
    public function checkUserServeHospital(UserServeHospitalCheckRequest $request): CheckResultReply
    {
        $isServing = ServeService::checkUserServerHospital($request->getUserId(), $request->getHospitalId());
        $reply = new CheckResultReply();
        $reply->setIsPass($isServing);
        return $reply;
    }

    /**
     * 获取代表指定医院列表
     *
     * @param  UserIdRequest $request
     * @return AgentSpecifyHospitalsReply
     */
    public function getAgentSpecifyHospitals(UserIdRequest $request): AgentSpecifyHospitalsReply
    {
        return ServeService::getUserSpecialHospitalByUid(
            $request->getUid(),
            $request->getKeywords()
        );
    }

    /**
     * 检查代表与指定服务医院或药店关系是否存在
     *
     * @param  UserServeHospitalCheckRequest $request
     * @return CheckResultReply
     */
    public function checkUserSpecifyServeHospital(UserServeHospitalCheckRequest $request): CheckResultReply
    {
        $isServing = ServeService::checkUserSpecifyServeHospital(
            $request->getUserId(),
            $request->getHospitalId()
        );
        $reply = new CheckResultReply();
        $reply->setIsPass($isServing);
        return $reply;
    }


    public function sendUserTemplateMsg(SendUserTemplateMsgRequest $request): GPBEmpty
    {
        $params = [];
        foreach ($request->getParams() as $key => $v) {
            $skey = (string) $key;
            $params[$skey] = $v;
        }

        UserService::sendUserWechatTemplateMsg(
            $request->getUserId(),
            $params,
            $request->getTplAlias(),
            $request->getUrl()
        );

        return new GPBEmpty();
    }

    public function getStoreAgents(StoreIdRequest $request): ServiceAgentsReply
    {
        return ServeService::getStoreAgents($request->getStoreId());
    }

    public function rebindUserCompany(RebindUserCompanyRequest $request): GPBEmpty
    {
        UserService::rebindUserCompany(
            $request->getUserId(),
            $request->getNewCompanyId(),
            $request->getAdminUserId()
        );
        return new GPBEmpty();
    }
}
