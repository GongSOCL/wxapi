<?php
declare(strict_types=1);
namespace App\Handler\Company;

use App\Service\Common\CompanyService;
use Grpc\Wxapi\Company\CompanySvcServerHandler;
use Grpc\Wxapi\Company\SearchCompanyReply;
use Grpc\Wxapi\Company\SearchCompanyRequest;

class CompanyHandler extends CompanySvcServerHandler
{
    public function searchLimitCompany(SearchCompanyRequest $request): SearchCompanyReply
    {
        return CompanyService::searchCompanyWithLimit($request->getKeywords(), $request->getLimit());
    }
}