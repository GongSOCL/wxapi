<?php


namespace App\Handler\Visit;

use App\Service\Visit\VisitService;
use Google\Protobuf\GPBEmpty;
use Grpc\Wxapi\Visit\CommonOperateIdRequest;
use Grpc\Wxapi\Visit\CompanyListReply;
use Grpc\Wxapi\Visit\CompanyListRequest;
use Grpc\Wxapi\Visit\GetVisitDistinctResponse;
use Grpc\Wxapi\Visit\SearchUserToAddSupplementReply;
use Grpc\Wxapi\Visit\SearchUserToAddSupplementRequest;
use Grpc\Wxapi\Visit\UserListReply;
use Grpc\Wxapi\Visit\UserListRequest;
use Grpc\Wxapi\Visit\VisitIdRequest;
use Grpc\Wxapi\Visit\VisitSvcInterface;
use Grpc\Wxapi\Visit\VisitSvcServerHandler;

class VisitSrvHandler extends VisitSvcServerHandler
{

    /**
     * @inheritDoc
     */
    public function getVisitDistinct(VisitIdRequest $request): GetVisitDistinctResponse
    {
        return VisitService::getGeoInfo($request->getId());
    }

    public function getSupplementCompanyList(CompanyListRequest $request): CompanyListReply
    {
        return VisitService::getSupplementCompanyList(
            $request->getKeywords(),
            $request->getPage()->getPage(),
            $request->getPage()->getSize()
        );
    }

    public function getSupplementUserList(UserListRequest $request): UserListReply
    {
        return VisitService::getSupplementUserList(
            $request->getKeywords(),
            $request->getPage()->getPage(),
            $request->getPage()->getSize()
        );
    }

    public function addSupplementCompany(CommonOperateIdRequest $request): GPBEmpty
    {
        VisitService::addSupplementCompany($request->getId());
        return new GPBEmpty();
    }

    public function delSupplementCompany(CommonOperateIdRequest $request): GPBEmpty
    {
        VisitService::delSupplementCompany($request->getId());
        return new GPBEmpty();
    }

    public function delSupplementUser(CommonOperateIdRequest $request): GPBEmpty
    {
        VisitService::delSupplementUser($request->getId());
        return new GPBEmpty();
    }

    public function addSupplementUser(CommonOperateIdRequest $request): GPBEmpty
    {
        VisitService::addSupplementUser($request->getId());

        return new GPBEmpty();
    }

    public function searchUserToAddSupplement(SearchUserToAddSupplementRequest $request): SearchUserToAddSupplementReply
    {
        return VisitService::searchUserForAddSupplement(
            $request->getKeywords(),
            $request->getLimit()
        );
    }
}
