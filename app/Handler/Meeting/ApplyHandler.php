<?php

namespace App\Handler\Meeting;

use App\Service\Meeting\MeetingService;
use Google\Protobuf\GPBEmpty;
use Grpc\Wxapi\Meeting\ApplyDetailReply;
use Grpc\Wxapi\Meeting\ApplyIdRequest;
use Grpc\Wxapi\Meeting\ApplyListReply;
use Grpc\Wxapi\Meeting\ApplyListRequest;
use Grpc\Wxapi\Meeting\AuditRequest;
use Grpc\Wxapi\Meeting\MeetingApplySvcServerHandler;
use Grpc\Wxapi\Meeting\SortOrder;

class ApplyHandler extends MeetingApplySvcServerHandler
{

    /**
     * @inheritDoc
     */
    public function listApply(ApplyListRequest $request): ApplyListReply
    {
        $current  = $request->getPage()->getPage();
        if ($current < 1) {
            $current = 1;
        }
        $limit = $request->getPage()->getSize();
        if ($limit <= 0) {
            $limit = 10;
        }
        $sort = $request->getSortMeetingStartTime();
        $sortOrder = $sort == SortOrder::SORT_UNKNOWN ? 0 : (
            $sort == SortOrder::SORT_ASC ? 1 : -1
        );
        return MeetingService::adminListApply(
            $request->getApplicantId(),
            $request->getZoneId(),
            $request->getStatus(),
            $request->getApplyStart(),
            $request->getApplyEnd(),
            $request->getMeetingStart(),
            $request->getMeetingEnd(),
            $current,
            $limit,
            $sortOrder
        );
    }

    /**
     * @inheritDoc
     */
    public function audit(AuditRequest $request): GPBEmpty
    {
        MeetingService::audit(
            $request->getId(),
            $request->getIsPass(),
            $request->getReason()
        );
        return new GPBEmpty();
    }

    /**
     * @inheritDoc
     */
    public function applyDetail(ApplyIdRequest $request): ApplyDetailReply
    {
        return MeetingService::getApplyDetail($request->getId());
    }
}
