<?php
declare(strict_types=1);

namespace App\Handler\Depart;

use App\Helper\Helper;
use App\Repository\SystemDepartRepository;
use Grpc\Wxapi\Depart\DepartIdsRequest;
use Grpc\Wxapi\Depart\DepartReply;
use Grpc\Wxapi\Depart\DepartSvcServerHandler;
use Grpc\Wxapi\Depart\DepartItem;

class DepartHandler extends DepartSvcServerHandler
{
    public function departList(DepartIdsRequest $request): DepartReply
    {
        if (empty($request->getIds())) {
            return [];
        }

        $departIds = Helper::mapRepeatedField($request->getIds(), function ($id) {
            return $id;
        });
        $list =  SystemDepartRepository::getByIds($departIds);
        $departArr = [];
        foreach ($list as $row) {
            $departItem = new DepartItem();
            $departItem->setId($row->id);
            $departItem->setName($row->name);
            $departArr[] = $departItem;
        }
        $reply = new DepartReply();
        return $reply->setDepart($departArr);
    }
}
