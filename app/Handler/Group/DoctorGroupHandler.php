<?php
declare(strict_types=1);
namespace App\Handler\Group;

use App\Service\Group\Doctor\DoctorGroupService;
use Grpc\Wxapi\DoctorGroup\AgentDoctorsListReply;
use Grpc\Wxapi\DoctorGroup\AgentListRely;
use Grpc\Wxapi\DoctorGroup\CheckAgentDoctorRequest;
use Grpc\Wxapi\DoctorGroup\CheckResultReply;
use Grpc\Wxapi\DoctorGroup\DoctorGroupServerHandler;
use Grpc\Wxapi\DoctorGroup\GroupDoctorIdRequest;
use Grpc\Wxapi\DoctorGroup\GroupDoctorInfoReply;
use Grpc\Wxapi\DoctorGroup\GroupDoctorListRequest;
use Grpc\Wxapi\DoctorGroup\QrCodeReply;
use Grpc\Wxapi\DoctorGroup\QrCodeRequest;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Di\Annotation\Inject;

class DoctorGroupHandler extends DoctorGroupServerHandler
{
    /**
     * @Inject
     * @var DoctorGroupService
     */
    private $doctorGroupService;

    /**
     * @inheritDoc
     */
    public function genDoctorGroupQrCode(QrCodeRequest $request): QrCodeReply
    {
        return $this->doctorGroupService->getQrcodeFromToken($request->getToken());
    }

    public function checkAgentOwnerDoctor(CheckAgentDoctorRequest $request): CheckResultReply
    {
        $isOwned = $this->doctorGroupService->checkAgentOwnerDoctorInGroup(
            $request->getAgentId(),
            $request->getDoctorId()
        );
        $reply = new CheckResultReply();
        $reply->setIsPass($isOwned);
        return $reply;
    }

    public function getAgentRelatedWithDoctorInGroup(GroupDoctorIdRequest $request): AgentListRely
    {
        return $this->doctorGroupService->getAgentOwnGroupDoctor($request->getId());
    }

    public function getDoctorServeHospitalRelatedAgents(GroupDoctorIdRequest $request): AgentListRely
    {
        return $this->doctorGroupService->getServingAgentByDoctorRelatedHospital($request->getId());
    }

    public function getGroupDoctors(GroupDoctorListRequest $request): AgentDoctorsListReply
    {
        return $this->doctorGroupService->getAgentGroupDoctors(
            $request->getAgentId(),
            $request->getPage()->getPage(),
            $request->getPage()->getSize(),
            $request->getOnlyWithWechat()
        );
    }

    public function getDoctorInfo(GroupDoctorIdRequest $request): GroupDoctorInfoReply
    {
        return $this->doctorGroupService->getGroupDoctorInfo($request->getId());
    }
}
