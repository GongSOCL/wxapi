<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
namespace App\Command\Wechat;

use App\Constants\DataStatus;
use App\Exception\WechatException;
use App\Service\Wechat\WechatBusiness;
use Hyperf\Command\Annotation\Command;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Arr;
use Hyperf\Utils\Codec\Json;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * @Command
 */
class MenuCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @Inject
     * @var WechatBusiness
     */
    protected $wechat;

    protected $name = 'wechat:menu';

    protected $subCommands = [
        'get',
        'create',
        'delete',
    ];

    public function configure()
    {
        parent::configure();
        $this->addOption('content', 'c', InputOption::VALUE_OPTIONAL, '创建菜单的内容，使用单引号\'包裹');
        $this->addArgument(
            'subcommand',
            InputOption::VALUE_REQUIRED,
            '支持的命令: ' . implode('|', $this->subCommands)
        );
        $this->setDescription('微信菜单命令');
    }

    public function handle()
    {
        try {
            $command = $this->input->getArgument('subcommand');
            if (empty($command)) {
                throw new \InvalidArgumentException('命令不能为空');
            }
            if (! method_exists($this, $command)) {
                throw new \InvalidArgumentException('不支持的命令：' . $command);
            }
            $this->{$command}();

            $this->info('执行成功');
        } catch (\Throwable $exception) {
            $this->error('执行失败： ' . $exception->getMessage());
            $this->error($exception->getTraceAsString());
        }
    }

    protected function get()
    {
        $res = $this->wechat->menuGet(DataStatus::WECHAT_AGENT);

        if (Arr::get($res, 'errcode', 0) !== 0) {
            throw new WechatException($res['errcode'], $res['errmsg']);
        }

        $buttons = Arr::get($res['selfmenu_info']['button']);

        $this->info('当前菜单内容： ' . Json::encode($buttons, 0));
    }

    protected function create()
    {
        $content = $this->input->getOption('content');
        $buttons = Json::decode($content);
        if (! $buttons) {
            throw new \InvalidArgumentException('菜单内容有误');
        }

        $res = $this->wechat->menuCreate(DataStatus::WECHAT_AGENT, $buttons);

        if (Arr::get($res, 'errcode', 0) !== 0) {
            throw new WechatException($res['errcode'], $res['errmsg']);
        }

        $this->info('创建成功： ' . Json::encode($res));
    }

    protected function delete()
    {
        $menuId = $this->input->getOption('id');
        if (! $menuId) {
            $menuId = null;
        }

        $res = $this->wechat->menuDelete(DataStatus::WECHAT_AGENT, $menuId);

        if (Arr::get($res, 'errcode', 0) !== 0) {
            throw new WechatException($res['errcode'], $res['errmsg']);
        }

        $this->info('删除成功： ' . Json::encode($res));
    }
}
