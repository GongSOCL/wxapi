<?php

declare(strict_types=1);

namespace App\Command;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Helper\Helper;
use App\Kernel\Minio\Minio;
use App\Repository\AdaptionRepository;
use App\Repository\UsersRepository;
use App\Service\Compliance\ExamService;
use App\Service\Points\PointsService;
use App\Service\Wechat\WechatBusiness;
use Aws\S3\S3Client;
use Carbon\Carbon;
use Grpc\Msg\MSG_BIZ_TYPE;
use Grpc\Msg\MSG_PRIORITY;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Hyperf\Filesystem\Adapter\S3AdapterFactory;
use Hyperf\Filesystem\FilesystemFactory;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Context;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use Psr\Container\ContainerInterface;
use Swoole\Coroutine\Channel;
use Symfony\Component\Console\Input\InputOption;
use Youyao\Framework\Components\Msg\MessageRpcClient;

/**
 * @Command
 */
class TestCommand extends HyperfCommand
{
    protected $name='test:test';

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function configure()
    {
        parent::configure();
        $this->setDescription('Hyperf Demo Command');
        $this->addOption('path', '', InputOption::VALUE_REQUIRED, '上传路径');
    }

    public function handle()
    {
        $path = $this->input->getOption('path');
        if (!file_exists($path)) {
            throw new BusinessException(ErrorCode::BUSINESS_ERROR, "上传文件不存在");
        }
        $name = basename($path);
        $uploadPath = sprintf("/cmd/%s/%s", date('Ymd'), $name);
        $fs = Minio::getFileSystem();
        $stream = fopen($path, "rb+");
        $fs->writeStream($uploadPath, $stream, []);
        fclose($stream);

        $this->line('Hello Hyperf!', 'info');
    }
}
