<?php

declare(strict_types=1);

namespace App\Command;

use App\Constants\DataStatus;
use App\Constants\ErrorCode;
use App\Constants\Point;
use App\Exception\BusinessException;
use App\Repository\QuestionnairePaperInviteeRepository;
use App\Repository\QuestionnairePaperRepository;
use App\Repository\UsersRepository;
use App\Service\Compliance\QuestionnaireService;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * @Command
 */
class AddQuestionnairePaperAgentCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $name='questionnaire:add-agent';

    public function configure()
    {
        parent::configure();
        $this->addOption('paper_id', '', InputOption::VALUE_REQUIRED, '问卷id');
        $this->addOption('agent_id', '', InputOption::VALUE_REQUIRED, '问卷代表id');
        $this->addOption('quote', '', InputOption::VALUE_REQUIRED, '可邀请人数');
        $this->setDescription('问卷邀请代表');
    }

    public function handle()
    {
        try {
            $paperId = $this->input->getOption('paper_id');
            $uid = $this->input->getOption('agent_id');
            $quote = $this->input->getOption('quote');
            $quote = $quote ? intval($quote) : 0;
            $paper = QuestionnairePaperRepository::getPaperById($paperId);
            if (!$paper) {
                throw new BusinessException(ErrorCode::QUESTIONNAIRE_NOT_EXISTS);
            }
            $invitor = UsersRepository::getUserByUid($uid);
            if (!$invitor) {
                throw new \UnexpectedValueException("代表不存在");
            }
            if ($quote <=0 || $quote > $paper->total_quota) {
                throw new \UnexpectedValueException("限额不正确");
            }
            $usedQuote = QuestionnairePaperInviteeRepository::getPaperUsedQuote($paper);
            $rest = $paper->total_quota - $usedQuote;
            if ($quote > $rest) {
                throw new \UnexpectedValueException("问卷邀请代表限额超过可分配限额");
            }

            QuestionnaireService::addPaperInvitee($paper, $uid, $quote, DataStatus::USER_TYPE_AGENT);
        } catch (BusinessException | \UnexpectedValueException $e) {
            $this->line($e->getMessage(), 'error', 'error');
        } catch (\Exception $e) {
            $this->line($e->getMessage() . $e->getTraceAsString(), 'error', 'error');
        }
    }
}
