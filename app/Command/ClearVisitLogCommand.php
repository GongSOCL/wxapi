<?php

declare(strict_types=1);

namespace App\Command;

use App\Repository\AgentVisitLogRepository;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * @Command
 */
class ClearVisitLogCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('visit:clear-log');
    }

    public function configure()
    {
        $this->addOption('before_day', '', InputOption::VALUE_OPTIONAL, '清除多少天以前的日志');
        parent::configure();
        $this->setDescription('Hyperf Demo Command');
    }

    public function handle()
    {
        $interval = (int)$this->input->getOption('before_day');
        if ($interval <= 0) {
            $interval= 30;
        }
        $end= (new \DateTime())->modify(sprintf("-%d days", $interval))->format('Y-m-d H:i:s');
        AgentVisitLogRepository::clearLog($end);
        $this->line('Hello Hyperf!', 'info');
    }
}
