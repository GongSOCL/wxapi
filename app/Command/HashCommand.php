<?php

declare(strict_types=1);

namespace App\Command;

use App\Exception\BusinessException;
use App\Helper\Helper;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * @Command
 */
class HashCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $name = 'hash:crypt';

    public function configure()
    {
        parent::configure();
        $this->addOption('decode', 'd', InputOption::VALUE_NONE, '是否解码');
        $this->addArgument('value', InputOption::VALUE_REQUIRED, '需要编码/解码字符串');
        $this->setDescription('哈希编码/解码');
    }

    public function handle()
    {
        try {
            $decode = $this->input->getOption('decode');
            $val = $this->input->getArgument('value');
            if ($decode) {
                $result = Helper::decodeHashId($val);
            } else {
                $result = Helper::generateHashId($val);
            }
            $this->line(sprintf("%s [%s] => [%s]", $decode ? 'decode' : 'encode', $val, $result), 'info');
        } catch (BusinessException | \UnexpectedValueException $e) {
            $this->line($e->getMessage(), 'error', 'error');
        } catch (\Exception $e) {
            $this->line($e->getMessage() . $e->getTraceAsString(), 'error', 'error');
        }
    }
}
