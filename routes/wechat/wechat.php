<?php

use App\Controller\Wechat\WechatController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/wechat/', function () {
    //公众号推送
    Router::post('serve/{type}', [WechatController::class, 'serve']);
    //公众号配置回调地址验证，形式是verify/1,2[此数值是DataStatus里面的WECHAT_TYPE]
    Router::get('serve/{type}', [WechatController::class, 'serve']);
    //获取网页授权地址
    Router::post('get-oauth-redirect-url', [WechatController::class, 'getOauthRedirectUrl']);
    //获取网页授权用户信息
    Router::post('get-oauth-user-info', [WechatController::class, 'getOauthUserInfo']);
    //获取网页jsapi config参数
    Router::post('signpage/get', [WechatController::class, 'getWechatJsSignature']);
    //根据openid获取微信用户信息
    Router::post('info', [WechatController::class, 'getWechatUserInfo']);
    //获取微信临时二维码
    Router::post('get-temporary-qrcode', [WechatController::class, 'getTemporaryQrcode'], [
        'middleware' => [AuthMiddleware::class]
    ]);
    //获取微信access-token
    Router::post('get-access-token', [WechatController::class, 'getServerAccessToken']);
});
