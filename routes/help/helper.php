<?php
declare(strict_types=1);

use App\Controller\Helper\HelperController;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/helper', function () {
    //同步单个医院
    Router::get('/sync-single-hospital', [HelperController::class, 'syncSingleHospital']);
    //同步所有医院
    Router::get('/sync-all-hospital', [HelperController::class, 'syncAllHospital']);
});