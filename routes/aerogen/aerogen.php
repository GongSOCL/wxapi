<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */

use App\Controller\Aerogen\AerogenController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

// 优云邀请
Router::addGroup('/aerogen', function () {
    //邀请医生列表
    Router::post('/doctor/list', [AerogenController::class,'doctorList']);
    //邀请药店列表
    Router::post('/drugstore/list', [AerogenController::class,'drugstoreList']);
    //药品列表
    Router::post('/product/list', [AerogenController::class,'productList']);
    //邀请-分享
    Router::post('/share/info', [AerogenController::class,'shareInfo']);
    //医生二维码
    Router::post('/qrcode/doctor', [AerogenController::class,'doctorQrcode']);
    //药店二维码
    Router::post('/qrcode/drugstore', [AerogenController::class,'drugstoreQrcode']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
