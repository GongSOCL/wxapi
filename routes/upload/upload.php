<?php
declare(strict_types=1);

use App\Controller\Upload\UploadController;
use App\Middleware\AuthMiddleware;
use App\Middleware\CorsMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/upload', function () {
    // 获取七牛上传token
    Router::post('/token', [UploadController::class, 'token']);
    // 添加七牛上传的文件记录
    Router::put('', [UploadController::class, 'record']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
