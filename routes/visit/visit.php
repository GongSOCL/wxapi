<?php
declare(strict_types=1);

use App\Controller\Visit\VisitController;
use App\Middleware\AuthMiddleware;
use App\Middleware\CorsMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/agent/visits', function () {
    // 拜访记录列表
    Router::post('', [VisitController::class, 'list']);
    // 签入
    Router::put('/check-in', [VisitController::class, 'checkIn']);
    // 检查地理位置
    Router::post('/check-geo', [VisitController::class, 'checkGeo']);
    //获取拜访配置
    Router::post('/config', [VisitController::class, 'config']);
    //签出
    Router::post('/check-out/{id:\d+}', [VisitController::class, 'checkOut']);
    // 新增签出-拜访记录详情
    Router::post('/new-check-out-info/{id:\d+}', [VisitController::class, 'newCheckOutInfo']);
    // 修改签入或签出-拜访记录详情
    Router::post('/info/{id:\d+}', [VisitController::class, 'info']);
    // 编辑签入
    Router::post('/edit-check-in/{id:\d+}', [VisitController::class, 'editCheckIn']);
    // 编辑签出
    Router::post('/edit-check-out/{id:\d+}', [VisitController::class, 'editCheckOut']);
    // 获取代表医生列表
    Router::post("/doctors", [VisitController::class, 'getDoctors']);
    // 添加医生
    Router::post("/doctors/add", [VisitController::class, 'addDoctor']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
