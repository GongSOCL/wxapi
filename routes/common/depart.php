<?php
declare(strict_types=1);

use App\Controller\Common\DepartmentController;
use App\Middleware\AuthMiddleware;
use App\Middleware\CorsMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/departs', function () {
    // 系统科室列表
    Router::post('', [DepartmentController::class, 'list']);
    // 用户服务科室列表
    Router::post('/serve', [DepartmentController::class, 'serveList']);
    // 添加用户服务科室
    Router::put('/serve', [DepartmentController::class, 'serve']);
}, [
    'middleware' => [
        AuthMiddleware::class
    ]
]);
