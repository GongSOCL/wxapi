<?php

declare(strict_types=1);

use App\Controller\Common\ProductController;
use App\Middleware\AuthMiddleware;
use App\Middleware\CorsMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::post('/series/serve', [ProductController::class, 'serve'], [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
