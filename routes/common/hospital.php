<?php
declare(strict_types=1);

use App\Controller\Common\HospitalController;
use App\Middleware\AuthMiddleware;
use App\Middleware\CorsMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::post('/hospital/serve', [HospitalController::class, 'serve'], [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
