<?php
declare(strict_types=1);

use App\Controller\Common\CallbackController;
use Hyperf\HttpServer\Router\Router;

//消息系统回调
Router::post('/callback/msg', [CallbackController::class, 'msg']);
