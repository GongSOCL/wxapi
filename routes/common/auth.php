<?php
declare(strict_types=1);

use App\Controller\User\AuthController;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/agent/auth', function () {
    //登陆认证
    Router::post('', [AuthController::class, 'auth']);
});
