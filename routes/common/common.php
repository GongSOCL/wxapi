<?php
declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/common/', function () {
    Router::post('hospital/index', [\App\Controller\Common\HospitalController::class, 'index']);
    Router::post('department/index', [\App\Controller\Common\DepartmentController::class, 'index']);
});
