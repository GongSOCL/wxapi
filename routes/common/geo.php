<?php
declare(strict_types=1);

use App\Controller\Common\GeoController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::post('/geo', [GeoController::class, 'desc'], [
    'middleware' => [AuthMiddleware::class]
]);
