<?php
declare(strict_types=1);

use App\Handler\Meeting\ApplyHandler;
use Grpc\Wxapi\Meeting\MeetingApplySvcRegister;

// 注册
MeetingApplySvcRegister::register(ApplyHandler::class);
