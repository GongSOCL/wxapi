<?php
declare(strict_types=1);

use App\Handler\Group\DoctorGroupHandler;
use Grpc\Wxapi\DoctorGroup\DoctorGroupRegister;

//DoctorGroupRegister::register(DoctorGroupHandler::class);

DoctorGroupRegister::registerDoctorGroup_CheckAgentOwnerDoctor([
    DoctorGroupHandler::class,
    'checkAgentOwnerDoctor'
]);

DoctorGroupRegister::registerDoctorGroup_GenDoctorGroupQrCode([
    DoctorGroupHandler::class,
    'genDoctorGroupQrCode'
]);


DoctorGroupRegister::registerDoctorGroup_GetAgentRelatedWithDoctorInGroup([
    DoctorGroupHandler::class,
    'getAgentRelatedWithDoctorInGroup'
]);


DoctorGroupRegister::registerDoctorGroup_GetDoctorServeHospitalRelatedAgents([
    DoctorGroupHandler::class,
    'getDoctorServeHospitalRelatedAgents'
]);


DoctorGroupRegister::registerDoctorGroup_GetGroupDoctors([
    DoctorGroupHandler::class,
    'getGroupDoctors'
]);


DoctorGroupRegister::registerDoctorGroup_GetDoctorInfo([
    DoctorGroupHandler::class,
    'getDoctorInfo'
]);
