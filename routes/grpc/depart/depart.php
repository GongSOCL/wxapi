<?php
declare(strict_types=1);

use Grpc\Wxapi\Depart\DepartSvcRegister;
use App\Handler\Depart\DepartHandler;

DepartSvcRegister::register(DepartHandler::class);
