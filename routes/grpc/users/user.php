<?php
declare(strict_types=1);

use App\Handler\Users\UserSrvHandler;
use Grpc\Wxapi\User\UserSvcRegister;

UserSvcRegister::register(UserSrvHandler::class);
