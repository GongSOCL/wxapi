<?php

declare(strict_types=1);

use App\Handler\Visit\VisitSrvHandler;
use Grpc\Wxapi\Visit\VisitSvcRegister;

VisitSvcRegister::register(VisitSrvHandler::class);
