<?php
declare(strict_types=1);

use App\Handler\Company\CompanyHandler;

\Grpc\Wxapi\Company\CompanySvcRegister::register(CompanyHandler::class);