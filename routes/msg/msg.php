<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
use Hyperf\HttpServer\Router\Router;

// 消息系统
Router::addGroup('/message', function () {
    //消息列表
    Router::post('/index', [\App\Controller\Msg\MsgController::class, 'index']);
    //最新一条消息
    Router::get('/latest', [\App\Controller\Msg\MsgController::class, 'latest']);
    //消息详情
    Router::post('/show', [\App\Controller\Msg\MsgController::class, 'show']);
    //全部已读
    Router::post('/readAll', [\App\Controller\Msg\MsgController::class, 'readAll']);
},
    ['middleware' => [\App\Middleware\AuthMiddleware::class]]);
