<?php
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/user/', function () {
    Router::post('exchange/goods', [\App\Controller\User\ExchangeGoods::class, 'handler']);
    Router::post('points', [\App\Controller\User\Points::class, 'handler']);
    Router::post('point/details', [\App\Controller\User\PointDetails::class, 'handler']);
}, ['middleware' => [\App\Middleware\AuthMiddleware::class]]);
