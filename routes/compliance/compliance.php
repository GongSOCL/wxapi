<?php
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/compliance/', function () {
    Router::post('agree', [\App\Controller\Compliance\ComplianceController::class, 'agree']);
}, ['middleware' => [\App\Middleware\AuthMiddleware::class]]);
