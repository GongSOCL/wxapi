<?php
//问卷
use App\Controller\Compliance\QuestionnaireController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/compliance/questionnaire/', function () {
    //开始问卷
    Router::post('start', [QuestionnaireController::class, 'start']);
    //获取问卷试题
    Router::post('get', [QuestionnaireController::class, 'get']);
    //回答问卷
    Router::post('answer', [QuestionnaireController::class, 'answer']);
    //问卷列表
    Router::post('list', [QuestionnaireController::class, 'list'], [
        'middleware' =>[AuthMiddleware::class]
    ]);
    //问卷受邀请人列表
    Router::post('invitees-list', [QuestionnaireController::class, 'inviteeList'], [
        'middleware' =>[AuthMiddleware::class]
    ]);
    //问卷医生列表
    Router::post('doctor-list', [QuestionnaireController::class, 'doctorList'], [
        'middleware' =>[AuthMiddleware::class]
    ]);
    //邀请答题
    Router::post('invite', [QuestionnaireController::class, 'invite'], [
        'middleware' =>[AuthMiddleware::class]
    ]);
    //重新邀请问卷
    Router::post('reinvite', [QuestionnaireController::class, 'reInvite'], [
        'middleware' =>[AuthMiddleware::class]
    ]);
    //提醒回答问卷
    Router::post('notice-answer', [QuestionnaireController::class, 'noticeAnswer'], [
        'middleware' =>[AuthMiddleware::class]
    ]);
});
