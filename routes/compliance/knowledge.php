<?php
declare(strict_types=1);

use App\Controller\Compliance\KnowledgeController;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/compliance/knowledges', function () {
    //树状列表
    Router::post('', [KnowledgeController::class, 'list']);
    // 顶级目录
    Router::post('/top-dirs', [KnowledgeController::class, 'listTopDir']);
    // 查找
    Router::post('/search', [KnowledgeController::class, 'search']);
}, [
    'middleware' => [
        AuthMiddleware::class
    ]
]);
