<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */

use App\Controller\Doctor\DoctorInfoController;
use Hyperf\HttpServer\Router\Router;

// 医生
Router::addGroup('/doctor', function () {
    //获取医生个人信息
    Router::post('/info/show', [\App\Controller\Doctor\InfoController::class, 'show']);
    //积分兑换的物品列表
    Router::post('/exchange/index', [\App\Controller\Doctor\ExchangeController::class, 'index']);
    //积分兑换
    Router::post('/exchange/create', [\App\Controller\Doctor\ExchangeController::class, 'create']);
    //消费积分明细
    Router::post('/shoppingPoints/index', [\App\Controller\Doctor\ShoppingPointsController::class, 'index']);
    //服务积分明细
    Router::post('/servicePoints/index', [\App\Controller\Doctor\ServicePointsController::class, 'index']);
    //邀请名单列表
    Router::post('/invitation/index', [\App\Controller\Doctor\InvitationController::class, 'index']);
    //邀请医生
    Router::post('/invitation/create', [\App\Controller\Doctor\InvitationController::class, 'create']);
    //医生预留信息
    Router::post('/authentication/show', [\App\Controller\Doctor\QualifyController::class, 'show']);
    //医生认证
    Router::post('/authentication/create', [\App\Controller\Doctor\QualifyController::class, 'create']);
    //医生认证信息
    Router::post('/authentication/index', [\App\Controller\Doctor\QualifyController::class, 'index']);
    //二维码
    Router::post('/qrcode/create', [\App\Controller\Doctor\QrcodeController::class, 'create']);
    //医生上传身份证照片
    Router::post('/idcard/upload', [\App\Controller\Doctor\IdcardController::class, 'upload']);
    //医生上传身份证确认
    Router::post('/idcard/create', [\App\Controller\Doctor\IdcardController::class, 'create']);
    //删除群组医生
    Router::post("/{id:\d+}/delete", [DoctorInfoController::class, 'delete']);
    //群组医生详情
    Router::post("/{id:\d+}/info", [DoctorInfoController::class, 'info']);
    //编辑群组医生
    Router::post('/{id:\d+}/edit', [DoctorInfoController::class, 'edit']);
    //根据手机号查找可合并医生信息
    Router::post('/{id:\d+}/valid-merge', [DoctorInfoController::class, 'validMerge']);
    //根据手机号查找可合并医生信息
    Router::post('/{id:\d+}/merge/{mergeId:\d+}', [DoctorInfoController::class, 'merge']);
},
    ['middleware' => [\App\Middleware\AuthMiddleware::class]]);

Router::addGroup('/user', function () {
    //上传身份证
    Router::post('/idcard/upload', [\App\Controller\Doctor\IdcardController::class, 'upload']);
    //提交身份证信息
    Router::post('/idcard', [\App\Controller\Doctor\IdcardController::class, 'idcard']);
}, [
    'middleware' => [
        \App\Middleware\AuthMiddleware::class
    ]
]);
