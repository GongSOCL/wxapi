<?php
declare(strict_types=1);

use App\Controller\App\User\AuthController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup("/app/auth", function () {
    //绑定或解绑微信
    Router::post('/wechatBind', [AuthController::class, 'wechatBind']);
    //绑定或解绑ios
    Router::post('/iosBind', [AuthController::class, 'iosBind']);
    //注销账户
    Router::post('/deleteAccount', [AuthController::class, 'deleteAccount']);
    //退出登陆
    Router::post("/logout", [AuthController::class, 'logout']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);


Router::addGroup('/app/auth', function () {
    // app手机号登录
    Router::post('/phone', [AuthController::class, 'appPhone']);
    // app苹果登录
    Router::post('/ios', [AuthController::class, 'appIos']);
    // app微信验证登录
    Router::post('/wechat', [AuthController::class, 'appWechatAuth']);
    //绑定或更换手机号
    Router::put("/wechat-bind-mobile", [AuthController::class, 'bindWechatMobile']);
}, [
    'middleware' => [AppHeadersCheckMiddleware::class]
]);

//ios授权信息
Router::post('/app/ios/authInfo', [AuthController::class, 'appIosAuthInfo']);
//判断公众号当前微信和手机的绑定状态
Router::get('/app/auth/bindState/{unionid}', [AuthController::class, 'bindState']);
// 审核后门登录
Router::get('/app/auth/check-status', [AuthController::class, 'getStatus']);
Router::post('/app/auth/login-for-check', [AuthController::class, 'loginForCheck']);
