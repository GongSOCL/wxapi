<?php
declare(strict_types=1);

use App\Controller\App\User\FakeController;
use Hyperf\HttpServer\Router\Router;

Router::addGroup("/app/fake", function () {
    //获取临时token
    Router::get('/code', [FakeController::class, 'getToken']);

    //使用token登陆
    Router::post('/auth', [FakeController::class, 'auth']);
});
