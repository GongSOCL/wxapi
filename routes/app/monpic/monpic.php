<?php
declare(strict_types=1);

use App\Middleware\AppAuthMiddleware;
use App\Controller\App\Monpic\MonpicController;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app', function () {
    // 弹图
    Router::get('/monpic', [MonpicController::class, 'getPic']);
    // 停留时长统计
    Router::post('/monpic/timeCount', [MonpicController::class, 'timeCount']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
