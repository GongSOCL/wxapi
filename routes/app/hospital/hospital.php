<?php
declare(strict_types=1);

use App\Controller\App\Hospital\HospitalController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/hospitals', function () {
    //医院搜索
    Router::get('/search', [HospitalController::class, 'search']);
    //医院药品详情
    Router::get("/{id:\d+}", [HospitalController::class, 'info']);
    //获取用户服务医院列表
    Router::get('/serve', [HospitalController::class, 'serve']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
