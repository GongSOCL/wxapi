<?php
declare(strict_types=1);

use App\Controller\App\Serve\ServeController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/serve', function () {
    //申请服务
    Router::post('/apply', [ServeController::class, 'apply']);
    //解绑服务
    Router::post('/{id:\d+}/remove', [ServeController::class, 'remove']);
    //服务中医院产品列表
    Router::get('/info-by-hospital', [ServeController::class, 'detailByHospital']);
    //服务产品详情
    Router::get('/serve-product/{id:\d+}', [ServeController::class, 'getServeProductDetail']);
    //获取服务系列药品
    Router::get('/series', [ServeController::class, 'getServeSeries']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
