<?php
declare(strict_types=1);

use App\Controller\App\Hospital\DepartmentController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/departments', function () {
    //部门列表
    Router::get('', [DepartmentController::class, 'list']);
    //添加用户服务部门
    Router::put('/serve', [DepartmentController::class, 'add']);
    //用户服务科室列表
    Router::get('/serve', [DepartmentController::class, 'getServeDepartmentList']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
