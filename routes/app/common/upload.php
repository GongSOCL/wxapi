<?php
declare(strict_types=1);


use App\Controller\App\Common\UploadController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/upload', function () {
    // 获取七牛上传token
    Router::post('/token', [UploadController::class, 'token']);
    // 添加七牛上传的文件记录
    Router::put('', [UploadController::class, 'record']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
