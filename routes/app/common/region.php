<?php
declare(strict_types=1);

use App\Controller\App\Common\CityController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/region', function () {
    //获取省份列表
    Router::get("/provinces", [CityController::class, 'province']);
    //获取市省份下的市县列表
    Router::get("/provinces/{id:\d+}/city", [CityController::class, 'city']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
