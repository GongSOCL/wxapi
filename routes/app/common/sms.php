<?php
declare(strict_types=1);

use App\Controller\App\Common\SmsController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/sms', function () {
    //发送短信验证码
    Router::post('/verify', [SmsController::class, 'sendVerify']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
//        AppAuthMiddleware::class
    ]
]);
