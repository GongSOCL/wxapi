<?php
declare(strict_types=1);

use App\Controller\App\Meeting\ApplyController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/meeting/apply', function () {
    //列表
    Router::get('', [ApplyController::class, 'list']);
    //添加申请
    Router::post('', [ApplyController::class, 'add']);
    //编辑申请
    Router::put('/{id:\d+}', [ApplyController::class, 'edit']);
    //详情
    Router::get('/{id:\d+}', [ApplyController::class, 'info']);
    //会议类型
    Router::get('/types', [ApplyController::class, 'getSubTypes']);
    //取消申请
    Router::delete('/{id:\d+}', [ApplyController::class,  'cancel']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
