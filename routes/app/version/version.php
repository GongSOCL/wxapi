<?php

declare(strict_types=1);

use App\Controller\App\Version\VersionController;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/version', function () {
    Router::get('', [VersionController::class, 'version']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class
    ]
]);
