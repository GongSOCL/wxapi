<?php
declare(strict_types=1);

use App\Controller\App\Group\Work\WorkGroupController;
use App\Controller\App\Group\Work\WorkGroupMemberController;
use App\Controller\App\Group\Work\VoteGroupController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

// 代表工作群组
Router::addGroup('/app/workgroup', function () {
    //工作群组搜索
    Router::get('/search', [WorkGroupController::class, 'search']);
    //工作群组详情
    Router::get('/detail', [WorkGroupController::class, 'detail']);
    //创建工作群组
    Router::post('/create', [WorkGroupController::class, 'create']);
    //群组重命名
    Router::put('/rename', [WorkGroupController::class, 'rename']);
    //修改群公告
    Router::put('/notice/change', [WorkGroupController::class, 'notice']);
    //群产品列表
    Router::get('/series', [WorkGroupController::class, 'series']);
    //修改群产品
    Router::put('/series/change', [WorkGroupController::class, 'changeSeries']);
    //设置新群主
    Router::put('/newLeader', [WorkGroupController::class, 'newLeader']);
    //解散群组
    Router::put('/delete', [WorkGroupController::class, 'delete']);
    //群组二维码
    Router::get('/qrcode', [WorkGroupController::class, 'qrcode']);

    //群组成员列表
    Router::get('/member/list', [WorkGroupMemberController::class, 'list']);
    //待加入成员列表
    Router::get('/join/list', [WorkGroupMemberController::class, 'joinList']);
    //我的好友列表
    Router::get('/agents/list', [WorkGroupMemberController::class, 'myAgent']);
    //添加成员
    Router::post('/member/add', [WorkGroupMemberController::class, 'add']);
    //批准/拒绝待加入成员
    Router::put('/join/approval', [WorkGroupMemberController::class, 'approval']);
    //删除群成员
    Router::put('/member/delete', [WorkGroupMemberController::class, 'delete']);

    //发起投票
    Router::post('/vote/create', [VoteGroupController::class, 'create']);
    //投票详情
    Router::get('/vote/detail', [VoteGroupController::class, 'detail']);
    //投票
    Router::post('/vote', [VoteGroupController::class, 'vote']);
    //终止投票
    Router::put('/vote/cancel', [VoteGroupController::class, 'cancel']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
