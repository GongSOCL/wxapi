<?php
declare(strict_types=1);

use App\Controller\App\Doctor\GroupDoctorController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/group-doctors', function () {
    //群组医生详情
    Router::get('/{id:\d+}', [GroupDoctorController::class, 'info']);
    //更新群组医生详情
    Router::post('/{id:\d+}', [GroupDoctorController::class, 'edit']);
    //删除群组医生
    Router::delete('/{id:\d+}', [GroupDoctorController::class, 'delete']);
    //获取待合并医生
    Router::get('/{id:\d+}/valid-merge', [GroupDoctorController::class, 'validMerge']);
    //合并医生
    Router::post('/{id:\d+}/merge/{mergeId:\d+}', [GroupDoctorController::class, 'merge']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
