<?php
declare(strict_types=1);

use App\Controller\App\Group\Doctor\DoctorGroupController;
use App\Controller\App\Group\Doctor\DoctorGroupMemberController;
use App\Controller\App\Group\Doctor\DoctorGroupStaticController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

// 医生群组
Router::addGroup('/app/doctorgroup', function () {
    //我的所有群组和成员详情
    Router::get('/info', [DoctorGroupController::class, 'info']);
    //群组列表
    Router::get('/list', [DoctorGroupController::class, 'list']);
    //创建群组
    Router::post('/create', [DoctorGroupController::class, 'create']);
    //删除群组
    Router::put('/delete', [DoctorGroupController::class, 'delete']);
    //群组重命名
    Router::put('/rename', [DoctorGroupController::class, 'rename']);
    //代表医生群组二维码
    Router::get('/doctor-qrcode', [DoctorGroupController::class, 'doctorQrCode']);
    //定向邀请医生二维码
    Router::post('/preset-invite', [DoctorGroupController::class, 'presetInvite']);
    //我服务的医院
    Router::get('/hospitals', [DoctorGroupController::class, 'hospitals']);
    //我服务的医院下的医生（可搜索）
    Router::get('/doctors', [DoctorGroupController::class, 'doctors']);

    //群组成员列表
    Router::get('/member/list', [DoctorGroupMemberController::class, 'members']);
    //从医生底层库加入到我的医生组
    Router::post('/join/add', [DoctorGroupMemberController::class, 'add']);
    //从我的医生默认组加入具体某一群组
    Router::post('/member/add', [DoctorGroupMemberController::class, 'addMember']);
    //从群组移除成员（相当于移入我的医生）
    Router::put('/member/delete', [DoctorGroupMemberController::class, 'delete']);
    //成员修改群组
//    Router::put('/member/change', [DoctorGroupMemberController::class, 'change']);
    //组员详情
    Router::get('/member/info', [DoctorGroupMemberController::class, 'info']);
    //判断新增的医生是否同名同医院
    Router::post('/member/checkSame', [DoctorGroupMemberController::class, 'checkSame']);
    //新增或修改组员信息
    Router::post('/member/update', [DoctorGroupMemberController::class, 'update']);
    //删除我的医生
    Router::put('/member/deleteForAll', [DoctorGroupMemberController::class, 'deleteForAll']);

    //固定部门分组
    Router::get('/static/departments', [DoctorGroupStaticController::class, 'departments']);
    //固定部门分组列表
    Router::get('/static/members', [DoctorGroupStaticController::class, 'members']);
    //自动分组
    Router::post('/static/autoGroup', [DoctorGroupStaticController::class, 'autoGroup']);
    //医生当前所在分组
    Router::get('/inGroup', [DoctorGroupStaticController::class, 'inGroup']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
