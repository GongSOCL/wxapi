<?php
declare(strict_types=1);

use App\Controller\App\Group\Doctor\OrganizationController;
use Hyperf\HttpServer\Router\Router;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;

Router::get('/app/organization/levelReset', [OrganizationController::class, 'levelReset']);

// 医生群组-组织架构
Router::addGroup('/app/organization', function () {
    // 获取用户的导航
    Router::get('/getNavigate', [OrganizationController::class, 'getNavigate']);
    // 获取用户的当前所处的顶级部门
    Router::post('/getTopDepartment', [OrganizationController::class, 'getTopDepartment']);
    // 根据部门id获取用户的下级部门
    Router::post('/getNextDepartment', [OrganizationController::class, 'getNextDepartment']);
    // 根据部门id获取当前部门下的代表
    Router::post('/getDepartmentAgent', [OrganizationController::class, 'getDepartmentAgent']);
    // 根据代表id获取绑定的医生
    Router::post('/getDoctorByAgent', [OrganizationController::class, 'getDoctorByAgent']);
    // 搜索医生
    Router::post('/searchDoctor', [OrganizationController::class, 'searchDoctor']);
    // 搜索记录
    Router::get('/searchRecord', [OrganizationController::class, 'searchRecord']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
