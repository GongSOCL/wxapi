<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
use Hyperf\HttpServer\Router\Router;
use App\Middleware\AppAuthMiddleware;
use App\Controller\App\Supplier\ReportController;
use App\Middleware\AppHeadersCheckMiddleware;

// 供应商系统
Router::addGroup('/app/supplier', function () {
    //报告
    Router::get('/getReport', [ReportController::class, 'getReport']);
    //报告导出
    Router::get('/exportReport', [ReportController::class, 'exportReport']);
},
    [
        'middleware' => [
            AppHeadersCheckMiddleware::class,
            AppAuthMiddleware::class
        ]
    ]);
