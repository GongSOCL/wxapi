<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
use Hyperf\HttpServer\Router\Router;
use App\Controller\App\Supplier\SupplierController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;

// 供应商系统
Router::addGroup('/app/supplier', function () {
    //判断当前用户是公司组长还是成员还是个人供应商
    Router::get('/getMemberRole', [SupplierController::class, 'getMemberRole']);
    //我的供应商群组成员列表
    Router::get('/getGroupList', [SupplierController::class, 'getGroupList']);
    //待加入列表
    Router::get('/getJoinList', [SupplierController::class, 'getJoinList']);
    //加入
    Router::post('/join', [SupplierController::class, 'join']);
    //移出群组或退出群组
    Router::post('/removeMember', [SupplierController::class, 'removeMember']);
    //解散群组
    Router::get('/dismissGroup', [SupplierController::class, 'dismissGroup']);
    //群长邀请二维码
    Router::get('/getLeaderCode', [SupplierController::class, 'getLeaderCode']);
    //扫码后调用
    Router::post('/addJoinList', [SupplierController::class, 'addJoinList']);
    //成员当前绑定状态
    Router::get('/getMemberDetail', [SupplierController::class, 'getMemberDetail']);
    //成员解除药品和医院绑定
    Router::post('/unbindMember', [SupplierController::class, 'unbindMember']);
    //当前可绑定的药品和医院
    Router::get('/hospitalProductForBind', [SupplierController::class, 'hospitalProductForBind']);
    //成员绑定药品和医院
    Router::post('/bindMember', [SupplierController::class, 'bindMember']);
},
    [
        'middleware' => [
            AppHeadersCheckMiddleware::class,
            AppAuthMiddleware::class
        ]
    ]);
