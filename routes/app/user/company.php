<?php
declare(strict_types=1);

use App\Controller\App\User\CompanyController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/company', function () {
    //公司列表
    Router::get('', [CompanyController::class, 'list']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
