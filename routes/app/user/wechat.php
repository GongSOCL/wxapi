<?php
declare(strict_types=1);

use App\Controller\App\User\WechatController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use App\Middleware\CorsMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/wechat', function () {
    //获取登陆用户信息
    Router::put('/record', [WechatController::class, 'record']);
    //获取oauth跳转地址
    Router::post('/get-oauth-redirect-url', [WechatController::class, 'getOauthRedirectUrl']);
    //获取oauth用户openid
    Router::post('/get-oauth-user-info', [WechatController::class, 'getOauthUserInfo']);
    //获取js签名
    Router::post('/sign', [WechatController::class, 'getWechatJsSignature']);
}, [
    'middleware' => [
        CorsMiddleware::class,
        AppHeadersCheckMiddleware::class
    ]
]);
