<?php
declare(strict_types=1);

use App\Controller\App\User\PointController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/points', function () {
    //用户积分汇总
    Router::get('/user-total', [PointController::class, 'getUserPoint']);
    //积分可兑换商品列表
    Router::get('/goods', [PointController::class, 'getGoodsLists']);
    //积分可兑换商品列表
    Router::get('/goods/info', [PointController::class, 'goodsInfo']);
    //用户积分明细
    Router::get('', [PointController::class, 'details']);
    //积分兑换
    Router::put('/exchange', [PointController::class, 'exchange']);
}, [
    'middleware' => [
        AppAuthMiddleware::class,
        AppHeadersCheckMiddleware::class
    ]
]);
