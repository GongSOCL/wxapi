<?php
declare(strict_types=1);

use App\Controller\App\User\IdcardController;
use App\Controller\App\User\UserController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/user', function () {
    //获取登陆用户信息
    Router::get('', [UserController::class, 'info']);
    //用户认证
    Router::post('/verify', [UserController::class, 'addVerify']);
    //更新用户头像
    Router::post('/avatar', [UserController::class, 'updateAvatar']);
    //更新用户手机号
    Router::post('/mobile', [UserController::class, 'updateMobile']);
    //更新用户呢称
    Router::post('/name', [UserController::class, 'updateName']);
    //获取用户认证信息
    Router::get('/verify', [UserController::class, 'verifyDetail']);
    //检查用户是否已经绑定手机号
    Router::get('/check-bind-mobile', [UserController::class, 'checkMobileBind']);
    //提交纠错信息
    Router::post('/help', [UserController::class, 'help']);
    //获取登陆用户信息
    Router::get('/qrcode', [UserController::class, 'qrcode']);
    //上传身份证
    Router::post('/idcard/upload', [IdcardController::class, 'upload']);
    //提交身份证信息
    Router::post('/idcard', [IdcardController::class, 'idcard']);
    //提醒用户修改昵称头像
    Router::post('/alert', [UserController::class, 'alert']);
    //获取pharmacy跳转信息
    Router::get('/pharmacy-invite', [UserController::class, 'getPharmacyAgentInvite']);
    //代表生成pharmacy的医生二维码
    Router::get('/pharmacy/doctor-qr', [UserController::class, 'getPharmacyDoctorQrcode']);
    //获取用户服务药品
    Router::get('/product', [UserController::class, 'getProduct']);
    //获取登陆用户注册和认证状态
    Router::get('/user-verify-state', [UserController::class, 'getUserVerifyState']);
    //pharmacy小程序销售数据录入链接
    Router::get('/pharmacy-sales-record-link', [UserController::class, 'getPharmacySalesRecordLink']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
