<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
use Hyperf\HttpServer\Router\Router;
use App\Controller\App\Favorites\FavoritesController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;

// 收藏夹
Router::addGroup('/app/favorites', function () {
    //收藏夹列表
    Router::get('/list', [FavoritesController::class, 'list']);
    Router::get('/detail', [FavoritesController::class, 'detail']);
    Router::post('/add', [FavoritesController::class, 'add']);
    Router::put('/delete', [FavoritesController::class, 'delete']);
    Router::put('/rename', [FavoritesController::class, 'rename']);
    Router::get('/checkResource', [FavoritesController::class,'checkResource']);

    Router::post('/resource/add', [FavoritesController::class, 'resourceAdd']);
    Router::put('/resource/delete', [FavoritesController::class, 'resourceDelete']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
