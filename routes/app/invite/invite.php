<?php
declare(strict_types=1);

use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use App\Controller\App\Invite\InviteController;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/invite', function () {
    // 我的邀请列表
    Router::get('/list', [InviteController::class, 'list']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
