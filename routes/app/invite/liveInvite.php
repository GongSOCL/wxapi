<?php
declare(strict_types=1);

use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use App\Controller\App\Invite\LiveInviteController;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/live-invite', function () {
    // 会议列表
    Router::get('/lives', [LiveInviteController::class, 'lives']);
    // 医生搜索
    Router::get('/doctors', [LiveInviteController::class, 'doctors']);
    // 邀请列表
    Router::get('/invite-list', [LiveInviteController::class, 'inviteList']);
    // 邀请详情
    Router::get('/detail', [LiveInviteController::class, 'detail']);
    // 发送邀请
    Router::post('/send', [LiveInviteController::class, 'send']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
