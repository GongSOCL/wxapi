<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */

use App\Controller\App\Aerogen\AerogenController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

// 优云邀请
Router::addGroup('/app/aerogen', function () {
    //邀请医生列表
    Router::get('/doctor/list', [AerogenController::class,'doctorList']);
    //邀请药店列表
    Router::get('/drugstore/list', [AerogenController::class,'drugstoreList']);
    //药品列表
    Router::get('/product/list', [AerogenController::class,'productList']);
    //邀请-分享
    Router::get('/share/info', [AerogenController::class,'shareInfo']);
    //医生二维码
    Router::post('/qrcode/doctor', [AerogenController::class,'doctorQrcode']);
    //药店二维码
    Router::get('/qrcode/drugstore', [AerogenController::class,'drugstoreQrcode']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
