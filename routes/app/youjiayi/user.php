<?php
declare(strict_types=1);

use App\Controller\App\Youjiayi\UserController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/youjiayi/users', function () {
    //搜索用户
    Router::get('', [UserController::class, 'search']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
