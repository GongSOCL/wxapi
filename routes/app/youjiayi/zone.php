<?php
declare(strict_types=1);

use App\Controller\App\Youjiayi\ZoneController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/youjiayi/zone', function () {
    //列表
    Router::get('', [ZoneController::class, 'list']);
    //大区省份列表
    Router::get('/{id:\d+}/province', [ZoneController::class, 'listProvince']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
