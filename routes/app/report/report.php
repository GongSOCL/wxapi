<?php
declare(strict_types=1);

use App\Middleware\AppAuthMiddleware;
use App\Controller\App\Report\ReportController;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/report', function () {
    // 拜访报告展示
    Router::get('/visit', [ReportController::class, 'visit']);
    // 销售报告展示
    Router::get('/sales', [ReportController::class, 'sales']);
    // 销售报告邮件发送
    Router::post('/sendSalesMail', [ReportController::class, 'sendSalesMail']);
    // 医院拜访报告邮件发送
    Router::post('/sendCheckoutMail', [ReportController::class, 'sendCheckoutMail']);
    // 医生拜访部门选项
    Router::get('/departs', [ReportController::class, 'departs']);
    // 医生拜访信息
    Router::post('/doctorVisit', [ReportController::class, 'doctorVisit']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
