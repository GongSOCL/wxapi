<?php
declare(strict_types=1);

use App\Controller\App\Product\AdaptionController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/adaptions', function () {
    //治疗领域列表
    Router::get('/field', [AdaptionController::class, 'fieldList']);

    //适应症列表
    Router::get('/adaption', [AdaptionController::class, 'adaptionList']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
