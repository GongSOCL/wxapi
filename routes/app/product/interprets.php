<?php
declare(strict_types=1);

use App\Controller\App\Product\InterpretController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/interprets', function () {
    //文献详情
    Router::Get("/{id:\d+}", [InterpretController::class, 'info']);
    //查看文献
    Router::put('/{id:\d+}/view', [InterpretController::class, 'view']);
    //文献评论列表
    Router::Get("/{id:\d+}/comments", [InterpretController::class, 'comment']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
