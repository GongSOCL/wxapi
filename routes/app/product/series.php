<?php
declare(strict_types=1);

use App\Controller\App\Product\SeriesController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/series', function () {
    //详情(不需要登陆)
    Router::get("/{id:\d+}", [SeriesController::class, 'info']);
}, [
    'middleware' => [
//        app审核原因产品页面暂时不验证登录（2022-07-07）
//        AppHeadersCheckMiddleware::class,
//        AppAuthMiddleware::class
    ]
]);
