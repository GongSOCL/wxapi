<?php
declare(strict_types=1);

use App\Controller\App\Product\CommentController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/comments', function () {
    //添加文献评论
    Router::put("", [CommentController::class, 'add']);
    //删除评论
    Router::delete("/{id:\d+}", [CommentController::class, 'delete']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
