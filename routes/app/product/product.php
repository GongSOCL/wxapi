<?php

declare(strict_types=1);


use App\Controller\App\Common\UploadController;
use App\Controller\App\Product\ProductController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/products', function () {
    //药品列表
    Router::get("", [ProductController::class, 'list']);
    //产品说明书
    Router::get("/{id:\d+}/manual", [ProductController::class, "manual"]);
    //产品文献列表
    Router::get("/{id:\d+}/interpret", [ProductController::class, "interpretList"]);
}, [
    'middleware' => [
//        app审核原因产品页面暂时不验证登录（2022-07-07）
//        AppHeadersCheckMiddleware::class,
//        AppAuthMiddleware::class
    ]
]);
