<?php
declare(strict_types=1);

use App\Controller\App\Product\TransactionController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/transaction', function () {
    //流向月报
    Router::get('/products', [TransactionController::class, 'serveProductTransactionList']);
    //产品流向图表数据
    Router::get('/products/{id:\d+}', [TransactionController::class, 'productTransactionDetail']);
    //产品流向医院排名
    Router::get('/products/{id:\d+}/hospital-rank', [TransactionController::class, 'hospitalRank']);
    //产品流向详情
    Router::get('/products/{id:\d+}/month', [TransactionController::class, 'months']);
    //产品流向详情
    Router::get('/products/{id:\d+}/details', [TransactionController::class, 'details']);
    //产品流向医院搜索
    Router::get('/products/{id:\d+}/hospitals', [TransactionController::class, 'hospitalSearch']);
    //根据消息查看用户某一天的流向数据
    Router::get('/msg-daily', [TransactionController::class, 'dailyTransaction']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
