<?php
declare(strict_types=1);

use App\Controller\App\Learning\LearningController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/learning', function () {
    // 知识库目录列表
    Router::get('/knowledge', [LearningController::class, 'knowledgeList']);

    // 知识库详情 文件id 文件类型
    Router::get('/knowledge/{id:\d+}', [LearningController::class, 'knowledgeInfo']);

    # 学习时长相关数据 学习首页
    Router::get('/plan', [LearningController::class, 'learningPlan']);

//    Router::get('/plan/list', [LearningController::class, 'getLearningPlan']);

    # 记录信息/学习完成
    Router::post('/plan/read/record', [LearningController::class, 'learningPlanReadRecord']);

    # 计划详情
    Router::get('/plan/{id:\d+}', [LearningController::class, 'planInfo']);

    # 计划列表
    Router::get('/plan/list/dt', [LearningController::class, 'planList']);

    # 继续学习
    Router::get('/plan/keep', [LearningController::class, 'keepLearn']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
