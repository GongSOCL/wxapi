<?php

declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */
use Hyperf\HttpServer\Router\Router;
use App\Controller\App\Msg\MsgController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;

// 消息系统
Router::addGroup('/app/message', function () {
    //消息列表
    Router::get('/list', [MsgController::class, 'list']);
    //最新一条消息
    Router::get('/latest', [MsgController::class, 'latest']);
    //消息详情
    Router::get('/detail', [MsgController::class, 'detail']);
    //全部已读
    Router::get('/readAll', [MsgController::class, 'readAll']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
