<?php
declare(strict_types=1);

use App\Middleware\AppAuthMiddleware;
use App\Controller\App\MorningShare\MorningShareController;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/morning', function () {
    // 类型
    Router::get('/getType', [MorningShareController::class, 'getType']);
    // 科室
    Router::get('/getDepartment', [MorningShareController::class, 'getDepartment']);
    // 图片列表
    Router::post('/picList', [MorningShareController::class, 'picList']);
    // 图片详情
    Router::post('/picDetail', [MorningShareController::class, 'picDetail']);
    // 点击次数排行榜
    Router::post('/countRank', [MorningShareController::class, 'countRank']);
    // 停留时长排行榜
    Router::post('/timeRank', [MorningShareController::class, 'timeRank']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
