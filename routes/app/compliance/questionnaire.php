<?php
//问卷
use App\Controller\App\Compliance\QuestionnaireController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/questionnaires', function () {
    //问卷列表
    Router::get('', [QuestionnaireController::class, 'list']);
    //问卷受邀请人列表
    Router::get('/{id:\d+}/invitees', [QuestionnaireController::class, 'inviteeList']);
    //问卷医生列表
    Router::get('/{id:\d+}/doctors', [QuestionnaireController::class, 'doctorList']);
    //邀请答题
    Router::put('/{id:\d+}/invite', [QuestionnaireController::class, 'invite']);
    //重新邀请问卷
    Router::post('/{id:\d+}/re-invite', [QuestionnaireController::class, 'reInvite']);
    //提醒回答问卷
    Router::post('/{id:\d+}/notice-answer', [QuestionnaireController::class, 'noticeAnswer']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);

//这部分接口需要抽成grpc给各自的系统使用
Router::addGroup('/app/questionnaires/', function () {
    //开始问卷
    Router::post('start', [QuestionnaireController::class, 'start']);
    //获取问卷试题
    Router::post('get', [QuestionnaireController::class, 'get']);
    //回答问卷
    Router::post('answer', [QuestionnaireController::class, 'answer']);
}, [
    'middleware' => [
        //TODO: 如果要启用，需要引入优佳医系统的中间件做一些基础校验
        //AppHeadersCheckMiddleware::class
    ]
]);
