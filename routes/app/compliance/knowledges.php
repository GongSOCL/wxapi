<?php
declare(strict_types=1);

use App\Controller\App\Compliance\KnowledgeController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/knowledge', function () {
    //顶级目录列表
    Router::get('/top-dirs', [KnowledgeController::class, 'listTopDir']);
    //搜索知识库文件
    Router::get('/search', [KnowledgeController::class, 'search']);
    //知识库树状列表
    Router::get('', [KnowledgeController::class, 'list']);
    //知识库文件详情
    Router::get('/{id:\d+}', [KnowledgeController::class, 'info']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
