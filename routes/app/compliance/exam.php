<?php
declare(strict_types=1);

use App\Controller\App\Compliance\ExamController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/exams', function () {
    //考试列表
    Router::get('', [ExamController::class, 'list']);
    //详情
    Router::get('/{id:\d+}', [ExamController::class, 'info']);
    //开始考试
    Router::post('/{id:\d+}/start', [ExamController::class, 'start']);
    //获取试题
    Router::get('/{id:\d+}/question', [ExamController::class, 'getQuestion']);
    //回答题目
    Router::post('/{id:\d+}/answer', [ExamController::class, 'answer']);
    //回答题目
    Router::post('/{id:\d+}/submit', [ExamController::class, 'finish']);
    //考试回顾
    Router::get('/{id:\d+}/review', [ExamController::class, 'review']);
    //获取考试协议
    Router::get('/{id:\d+}/agreement', [ExamController::class, 'getAgreement']);
    //同意考试协议
    Router::post('/{id:\d+}/agreement', [ExamController::class, 'agree']);
    //获取签署后生成的pdf版本
    Router::get('/{id:\d+}/agree-view', [ExamController::class, 'viewAgreement']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
