<?php

declare(strict_types=1);

use App\Controller\App\Feedback\FeedbackController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/feedback', function () {
    Router::post('/add', [FeedbackController::class, 'add']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
