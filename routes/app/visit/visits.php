<?php
declare(strict_types=1);
/**
 * This file is part of Youyao.
 *
 * @link     http://yy-git.youyao99.com/youyao/wxapi
 * @document http://yy-git.youyao99.com/youyao/wxapi
 * @contact  info@nuancebiotech.cn
 * @copyright  © 2020 Nuance All Rights Reserved
 */

use App\Controller\App\Visit\VisitController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/visits', function () {
    //列表
    Router::post('', [VisitController::class, 'list']);
    //签入详情
    Router::get('/new-check-out-info/{id:\d+}', [VisitController::class, 'newCheckOutInfo']);
    //详情
    Router::get('/{id:\d+}', [VisitController::class, 'info']);
    //签入
    Router::put('/check-in', [VisitController::class, 'checkIn']);
    //检查地理位置范围
    Router::post('/check-geo', [VisitController::class, 'checkGeo']);
    //获取配置项
    Router::get('/config', [VisitController::class, 'config']);
    //签出
    Router::put('/check-out', [VisitController::class, 'checkOut']);
    //编辑签入
    Router::post('/check-in/{id:\d+}', [VisitController::class, 'editCheckIn']);
    //编辑签出
    Router::post('/check-out/{id:\d+}', [VisitController::class, 'editCheckOut']);
    //获取关联医生
    Router::get('/doctors', [VisitController::class, 'getDoctors']);
    //添加关联医生
    Router::put('/doctors', [VisitController::class, 'addDoctor']);
    //获取签入配置
    Router::get('/check-in-config', [VisitController::class, 'checkInConfig']);

    //获取所有医生所在部门
    Router::get('/departs', [VisitController::class, 'departs']);

    //获取选择医生列表
    Router::get('/select-list', [VisitController::class, 'selectList']);
    //加入选择医生列表
    Router::post('/add-select-list', [VisitController::class, 'addSelectList']);
    //从选择列表删除
    Router::post('/delete-select-list', [VisitController::class, 'deleteSelectList']);
    //清空选择列表
    Router::get('/clear-select-list', [VisitController::class, 'clearSelectList']);
    //列表中选择医生数量
    Router::get('/count', [VisitController::class, 'doctorCount']);
    // fake position
    Router::get('/fake-position', [VisitController::class, 'generateGeoPosition']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
