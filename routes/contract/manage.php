<?php
declare(strict_types=1);

use App\Controller\App\Contract\ManageController;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/contract-manage', function () {
    //生成合同html
    Router::post('/generate-html', [ManageController::class, 'generateHTML']);
    //单份合同html模板生成结束
    Router::post('/cb-tpl-generated', [ManageController::class, 'tplGenerated']);
    //单份合同签名pdf生成结束
    Router::post('/cb-pdf-generated', [ManageController::class, 'pdfGenerated']);
    //添加合同组
    Router::post('/add-group', [ManageController::class, 'addContractGroup']);
    //添加主合同
    Router::post('/add-youyao-master', [ManageController::class, 'addYouyaoMasterContract']);
    //添加附件合同
    Router::post('/add-youyao-attachment', [ManageController::class, 'addYouyaoAttachment']);
    //添加知情同意书
    Router::post('/add-youyao-agree', [ManageController::class, 'addUserAgree']);
});
