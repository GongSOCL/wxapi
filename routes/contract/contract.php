<?php
declare(strict_types=1);

use App\Controller\App\Contract\ContractController;
use App\Middleware\AppAuthMiddleware;
use App\Middleware\AppHeadersCheckMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/app/contracts', function () {
    //合同列表
    Router::get('/group', [ContractController::class, 'list']);
    //开始签署合同
    Router::post('/group/{id:\d+}/start', [ContractController::class, 'start']);
    //获取合同模板内容及下一份合同
    Router::get('/group/{id:\d+}/tpl', [ContractController::class, 'getGroupContractTpl']);
    //签署合同
    Router::post('/group/{id:\d+}/sign', [ContractController::class, 'sign']);
    //获取签署后的合同pdf
    Router::get('/{id:\d+}/pdf', [ContractController::class, 'getContractPdf']);
    //获取单份合同模板
    Router::get('/{id:\d+}/tpl', [ContractController::class, 'getContractTpl']);
    //发送合同组附件到邮箱
    Router::post('/group/{id:\d+}/send-mail', [ContractController::class, 'sendMail']);
}, [
    'middleware' => [
        AppHeadersCheckMiddleware::class,
        AppAuthMiddleware::class
    ]
]);
