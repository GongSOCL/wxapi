<?php
declare(strict_types=1);

use App\Middleware\AuthMiddleware;
use Hyperf\HttpServer\Router\Router;

Router::addGroup('/report', function () {
    // 拜访报告展示
    Router::post('/visit', [\App\Controller\Report\ReportController::class, 'visit']);
    // 销售报告展示
    Router::post('/sales', [\App\Controller\Report\ReportController::class, 'sales']);
    // 销售报告邮件发送
    Router::post('/sendSalesMail', [\App\Controller\Report\ReportController::class, 'sendSalesMail']);

    // 销售报告邮件发送
    Router::post('/sendCheckoutMail', [\App\Controller\Report\ReportController::class, 'sendCheckoutMail']);
}, [
    'middleware' => [
        AuthMiddleware::class,
    ]
]);
