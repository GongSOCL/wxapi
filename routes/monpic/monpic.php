<?php
declare(strict_types=1);

use App\Controller\Monpic\MonpicController;
use Hyperf\HttpServer\Router\Router;

Router::post(
    '/monpic',
    [MonpicController::class, 'getPic'],
    [
        'middleware' => [\App\Middleware\AuthMiddleware::class]
    ]
);
